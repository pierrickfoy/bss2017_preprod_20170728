TEMPLATE BORDAS SOUTIEN SCOLAIRE :
Directives et informations à l'attention des developpeurs :

Template réalisé avec Bootstrap 3.3.4 | Version jquery 1.11.2 | Version modernizr 2.8.3
L'essentiel de l'intégration respecte la doc bootstrap : http://getbootstrap.com/

Les scripts et feuilles de styles generiques sont appelés du CDN cloudflare : https://cdnjs.com/

Template location : 

/ : Les pages types initiales demandées
/template/*.php : Toutes les sections qui composent les pages intiales.
/css/ : Feuilles de styles, Media query et webfonts
/img/ : visuels du template
/js/ : Les javascripts - le fichier script.js contient les options spécifiques des scripts du site.

--------------------------------------
IMPORTANT
--------------------------------------
Les seuls impératifs, actuellement repérés, lors du developpement est :

- Bien conserver les classes propres à la mise en page responsive Bootstrap

- Bien penser à ajouter la classe "logged-in" dans le BODY des pages lorsque l'utilisateur est connecté à son compte 
  et bien evidement supprimer cette classe entant que visiteur
  
- Rester compatible IE8 "dans les grandes lignes"
--------------------------------------
--------------------------------------