<!doctype html>
<html lang="fr" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<title>BSS Conditions générales d'utilisation</title>
<link rel="canonical" href="[canonical]" />
<meta name="description" content="[description]" />
<meta name="author" content="[author]" />
<meta property="og:locale" content="fr_FR" />
<meta property="og:type" content="website" />
<meta property="og:title" content="[title]" />
<meta property="og:description" content="[description]" />
<meta property="og:url" content="[URL]" />
<meta property="og:site_name" content="[site_name]" />
<meta property="og:image" content="[image]" />
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:description" content="[description]"/>
<meta name="twitter:title" content="[title]"/>
<meta name="twitter:site" content="[site]"/>
<meta name="twitter:domain" content="[domain]"/>
<meta name="twitter:image:src" content="[image]" />
<!--STYLES-->
<?php include 'styles.php';?>
</head>
<body class="page-simple " id="cgu">

<!-- HEADER -->
<header class="bss-header">
  <?php include 'header.php';?>
  <?php include 'menu.php';?>
</header>
<div class="main animsition">
<!-- BREADCRUMB -->
<div class="bss-breadcrumb" >
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h6>
          <ol class="breadcrumb">
            <li id="a" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="index.php" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
            <li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="cgu.php" itemprop="url"> <span itemprop="title">CGU</span> </a></li>
          </ol>
        </h6>
      </div>
    </div>
  </div>
</div>
<div class="bss-section bloc-section-orange bss-ccm">
  <div class="container">
    <h1 id="tagline" class="h1">Conditions générales d'utilisation de Bordas Soutien scolaire</h1>
    <div class="row">
      <div class="col-md-8">
<p>Les éditions BORDAS (ci-après dénommé « BORDAS ») donnent accès, par téléchargement ou consultation en ligne via des abonnements au site <a href="../soutien-scolaire-en-ligne/">http://www.bordas-soutien-scolaire.com/soutien-scolaire-en-ligne/</a> (ci-après dénommé « le Site »), à une offre sécurisée d’accompagnement scolaire en ligne, qui met à la disposition des parents, des élèves et des enseignants (ci-après dénommés « l’Utilisateur ») des contenus numériques interactifs dans différentes matières (ci-après dénommés les « Contenus Numériques »).</p>
<p>L’Utilisateur reconnait prendre connaissance des présentes conditions d’utilisation et les accepter.</p>
<hr>
<h2>I. Equipements requis pour accéder à l’offre</h2>
<p>La configuration minimale requise pour accéder aux contenus numériques est :</p>
<table border="1" cellspacing="0" cellpadding="0" class="table table-bordered">
<tbody>
<tr>
<td style="" valign="top">&nbsp;</td>
<td style="" valign="top">
<p><strong>Paramètres</strong></p>
</td>
<td style="" valign="top">
<p><strong>Configurations</strong></p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p><strong>Ports TCP</strong></p>
</td>
<td style="" valign="top">
<p>&nbsp;</p>
</td>
<td style="" valign="top">
<p>Les ports standards HTTP (80) et HTTPS (443) doivent être accessibles en sortie.</p>
</td>
</tr>
<tr>
<td style="" rowspan="2" valign="top">
<p><strong>Equipement réseau</strong></p>
</td>
<td style="" valign="top">
<p>Bande passante disponible du réseau Wifi par client</p>
</td>
<td style="" valign="top">
<p>Le réseau Wifi 802.11(a, b, g, ou n) utilisé doit supporter au moins 25 connexions simultanées (au moins autant que d’élèves dans une classe).</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Accès à internet</p>
</td>
<td style="" valign="top">
<p>- 400 kbit/s en descendant par utilisateur actif<br> - 100 kbit/s en montant par utilisateur actif</p>
</td>
</tr>
<tr>
<td style="" rowspan="9" valign="top">
<p><strong>Ordinateur</strong></p>
</td>
<td style="" valign="top">
<p>Processeur</p>
</td>
<td style="" valign="top">
<p>Intel Atom (2 GHz)</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Mémoire</p>
</td>
<td style="" valign="top">
<p>2 Go</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Espace disque disponible</p>
</td>
<td style="" valign="top">
<p>4 Go sur le disque dur principal</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Réseau</p>
</td>
<td style="" valign="top">
<p>Wi-Fi 802.11N</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Taille d’écran minimale</p>
</td>
<td style="" valign="top">
<p>9 pouces<br> Recommandé : au moins 10 pouces</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Définition d’écran</p>
</td>
<td style="" valign="top">
<p>1024*768</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Ecouteurs</p>
</td>
<td style="" valign="top">
<p>Recommandé</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Système d’exploitation</p>
</td>
<td style="" valign="top">
<p>Windows XP<br> Windows 7 / 8<br> OS X 10.8.4</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Navigateur internet</p>
</td>
<td style="" valign="top">
<p>Chrome 35.x et versions ultérieures<br> Firefox 31 et versions ultérieures<br>IE 10 et IE11 et versions ultérieures</p>
</td>
</tr>
<tr>
<td style="" rowspan="9" valign="top">
<p><strong>Tablette</strong></p>
</td>
<td style="" valign="top">
<p>Processeur</p>
</td>
<td style="" valign="top">
<p>Processeur à double coeur cadencé à 1 GHz<br> Recommandé : quatre coeurs et cadence de 1.4 GHz ou plus</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Mémoire</p>
</td>
<td style="" valign="top">
<p>1 Go</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Espace disque libre</p>
</td>
<td style="" valign="top">
<p>4 Go</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Réseau</p>
</td>
<td style="" valign="top">
<p>Wi-Fi 802.11N</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Taille de l’écran</p>
</td>
<td style="" valign="top">
<p>7”</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Résolution de l’écran</p>
</td>
<td style="" valign="top">
<p>800*600<br> Recommandé : 1024*768</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Casque / Ecouteurs</p>
</td>
<td style="" valign="top">
<p>Recommandé</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Tablettes Androïd supportées</p>
</td>
<td style="" valign="top">
<p>Android 4.1.2 et ultérieurs</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Tablettes iOS supportées</p>
</td>
<td style="" valign="top">
<p>iOS iPad 2/3/4<br> iPad mini<br> iOS 6.0 et ultérieurs</p>
</td>
</tr>
<tr>
<td style="" rowspan="2" valign="top">
<p><strong>Autres matériels (optionnel)</strong></p>
</td>
<td style="" valign="top">
<p>Souris et clavier</p>
</td>
<td style="" valign="top">
<p>Recommandé : touchpad + souris</p>
</td>
</tr>
<tr>
<td style="" valign="top">
<p>Imprimante</p>
</td>
<td style="" valign="top">
<p>Imprimante réseau noir et blanc</p>
</td>
</tr>
</tbody>
</table>
<p><br>Les coûts de ces équipements et logiciels sont à la charge de l’Utilisateur.</p>
<p>L’Utilisateur doit s’assurer de la configuration minimale requise avant chaque commande de Contenus Numériques sans recours possible contre BORDAS.</p>
<p>Les Contenus Numériques sont fournis en l’état et leurs mises à jour tout comme celles liées notamment aux systèmes d’exploitation sont à la charge exclusive de l’Utilisateur.</p>
<p>Dans le cas où des codes sont nécessaires pour accéder à l’offre, ceux-ci sont confidentiels .Toute perte, vol, détournement ou utilisation non autorisée de ces codes d'accès et leurs conséquences relèvent de la responsabilité de l’Utilisateur.</p><hr>
<h2>II. Conditions d’utilisation de l’offre</h2>
<p>Les logiciels, textes, vidéos, logos, marques et Contenus Numériques intégrés dans l’offre sont des créations pour lesquelles BORDAS est seule titulaire de l’ensemble des droits de propriété intellectuelle et/ou d’exploitation.</p>
<p>Aussi et sauf autorisation spéciale de BORDAS, la reproduction partielle ou totale sur tout support, la location, la revente et la diffusion par tout moyen et notamment sur les réseaux « peer-to-peer », les blogs, les sites web contributifs… des logiciels, textes, vidéos, logos, marques et Contenus Numériques sont strictement interdites et passibles de poursuites judiciaires.</p>
<p>L’Utilisateur peut imprimer tout ou partie des Contenus Numériques sur support papier pour son usage personnel ce qui exclut notamment, toute reproduction à des fins commerciales ou de diffusion en grand nombre, gratuite ou payante.</p><hr>
<h2>III. Garanties et responsabilités</h2>
<p>BORDAS fera ses meilleurs efforts pour sécuriser l'accès, la consultation et l'utilisation des Contenus Numériques conformément aux règles d'usages de l'Internet.</p>
<p>BORDAS ne peut être tenus responsables des interruptions de services dues aux caractéristiques et limites du réseau Internet, notamment dans le cas d’interruption des réseaux d’accès, des performances techniques et des temps de réponse pour consulter ou interroger les Contenus Numériques.</p>
<p>Compte tenu des caractéristiques intrinsèques de l’Internet, BORDAS ne garantit pas l’Utilisateur contre les risques notamment de piratage des données et programmes et de tout dommage subi par les ordinateurs consécutif à l’utilisation du Site. Il appartient à l’Utilisateur de prendre toutes mesures appropriées de nature à protéger ses données et logiciels. Il lui appartient également de veiller à se déconnecter à la fin d’une session.</p><hr>
<h2>IV. Collecte d’informations à caractère personnel</h2>
<p>Les informations concernant l’Utilisateur sont nécessaires au suivi de son apprentissage.</p>
<p>Les fichiers de BORDAS sont déclarés auprès de la Commission Nationale de l’Informatique et des Libertés (CNIL).</p>
<p>Les informations et données concernant l’Utilisateur sont nécessaires à la gestion de sa commande. L’Utilisateur sera susceptible de recevoir des offres de BORDAS et d’autres sociétés du groupe auquel il appartient s’il les a acceptées au moment de sa commande.</p>
<p>Conformément à la loi Informatique et Libertés du 6 janvier 1978 modifiée en 2004, l’Utilisateur dispose d’un droit d’accès, de rectification ou d’opposition aux données personnelles le concernant en s’adressant à la Relation client BORDAS :<br> Tél. : 01 72 36 40 91<br> E-mail : <a href="mailto:soutien-scolaire@bordas.tm.fr">soutien-scolaire@bordas.tm.fr</a></p><hr>
<h2>V. Cookies</h2>
<p>Les seuls cookies utilisés sur le Site sont des cookies techniques à des fins d’authentification pour que la session de l’Utilisateur reste active de page en page et des cookies de mesure d’audience (anonyme) permettant de mémoriser des données de trafic sur le Site.</p>
<p>En supprimant certains cookies, l’Utilisateur accepte de ne plus pouvoir utiliser certaines fonctionnalités qui nécessiteraient d’être identifié sur le Site.</p><hr>
<h2>VII. Loi applicable</h2>
<p>Les présentes conditions générales d’utilisation sont régies par la loi française et tout conflit en relevant devra être résolu conformément au droit français devant le Tribunal du lieu où demeure le défenseur en justice.</p>
			
      </div>
      <div class="col-md-4 hidden-xs hidden-sm">
        <div class=""> 
          <!-- 16:9 aspect ratio -->
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CItcTboz6_k?rel=0"></iframe>
          </div>
        </div>
        <hr>
        <div class="bloc-annonce"><div class="prepend"><b class="caret"></b> publicité <b class="caret"></b></div><a href="#"><img src="http://www.bordas-soutien-scolaire.com/images/bss-wengo-300.gif" class="img-responsive"></a></div>
      </div>
      <div class="col-md-12">
        <hr>
        <p class="text-center"><a class="btn btn-md btn-primary" href="#">Je découvre toutes les formules d'abonnement<i class="icon-angle-right"></i></a></p>
      </div>
    </div>
  </div>
</div>

<!--FOOTER-->
<?php include 'footer.php';?>
<!--SCRIPTS-->
<?php include 'scripts.php';?>
</body>
</html>
