<!doctype html>
<html lang="fr" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<title>BSS Conditions générales de vente</title>
<link rel="canonical" href="[canonical]" />
<meta name="description" content="[description]" />
<meta name="author" content="[author]" />
<meta property="og:locale" content="fr_FR" />
<meta property="og:type" content="website" />
<meta property="og:title" content="[title]" />
<meta property="og:description" content="[description]" />
<meta property="og:url" content="[URL]" />
<meta property="og:site_name" content="[site_name]" />
<meta property="og:image" content="[image]" />
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:description" content="[description]"/>
<meta name="twitter:title" content="[title]"/>
<meta name="twitter:site" content="[site]"/>
<meta name="twitter:domain" content="[domain]"/>
<meta name="twitter:image:src" content="[image]" />
<!--STYLES-->
<?php include 'styles.php';?>
</head>
<body class="page-simple " id="cgv">

<!-- HEADER -->
<header class="bss-header">
  <?php include 'header.php';?>
  <?php include 'menu.php';?>
</header>
<div class="main animsition">
<!-- BREADCRUMB -->
<div class="bss-breadcrumb" >
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h6>
          <ol class="breadcrumb">
            <li id="a" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="index.php" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
            <li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="cgv.php" itemprop="url"> <span itemprop="title">CGV</span> </a></li>
          </ol>
        </h6>
      </div>
    </div>
  </div>
</div>
<div class="bss-section bloc-section-orange bss-ccm">
  <div class="container">
    <h1 id="tagline" class="h1">Conditions générales de vente de Bordas Soutien scolaire</h1>
    <div class="row">
      <div class="col-md-8">
        <p><strong>INTERFORUM</strong><br>
          Société anonyme au capital de 1 729 950 € <br>
          SIREN 612 039 073 R.C.S. Créteil (1995 B 00836), TVA FR60612039073</p>
        <p><strong>Siège social, Services administratifs et commerciaux </strong><br>
          Immeuble PARYSEINE - 3, allée de la Seine 94854 IVRY SUR SEINE CEDEX <br>
          Téléphone : 01.49.59.10.10 - Fax : 01.49.59.10.72 / 01.49.59.10.94</p>
        <p><strong>Comptabilité Clients</strong><br>
          3, Allée de la Seine - 94206 IVRY SUR SEINE CEDEX<br>
          Téléphone : 01.49.59.58 58 - Fax : 01.49.59.58.60</p><hr>
        <h2>Article 1 : Objet du contrat</h2>
        <p>INTERFORUM (ci après dénommé « INTERFORUM ») est lié à la société SEJER (ci après dénommée « SEJER ») par des accords relatifs à la facturation et l'encaissement des ventes directes aux consommateurs au sens de l'article préliminaire Livre 1er du Code de la Consommation. A ce titre, INTERFORUM applique les conditions commerciales définies par SEJER (prix de vente, remises, etc).</p>
        <p>SEJER sous sa marque Bordas Soutien Scolaire est une société anonyme au capital de 9 898 330 Euros, dont le siège social est situé 30 place d'Italie, 75702 Paris Cedex 13 et inscrite au RCS de Paris sous le numéro 393 291 042.</p>
        <p>Les présentes conditions générales de vente complètent les conditions commerciales de SEJER et s'appliquent aux ventes à des consommateurs de contenus numériques immatériels (ci-après dénommés « les Contenus Numériques ») accessibles par téléchargement et consultation en ligne via des abonnements au site Internet http://www.bordas-soutien-scolaire.com/soutien-scolaire-en-ligne/ (ci-après dénommé « le Site »).</p>
        <p>SEJER sous sa marque Bordas Soutien Scolaire donne accès sur le Site à une offre d'accompagnement scolaire en ligne composée de Contenus Numériques interactifs dans différentes matières et destinée aux parents, aux élèves et aux enseignants.</p>
        <h2>Article 2 :  Processus et validation de la commande</h2>
        <p>Le processus de commande des Contenus Numériques est présenté en détail sur le Site. SEJER vous confirmera votre commande par e-mail et vous adressera à l'acceptation du paiement le lien permettant d'accéder aux Contenus Numériques sélectionnés. Cette confirmation vaut acceptation sans réserve des présentes conditions générales de vente.</p>
        <p>Pour commander, vous devez impérativement avoir la capacité juridique de contracter. Pour les personnes mineures, une autorité parentale est nécessaire.</p>
        <p>Il est rappelé que le délai de rétractation de 14 (quatorze) jours francs ouvert au consommateur par l'article L.121-21-8 du Code de la Consommation n'est pas applicable à la vente de produits immatériels.</p><hr>
        <h2>Article 3 : Moyens de paiement</h2>
        <p>Les Contenus Numériques commandés sur le Site sont réglés comptant par une carte bancaire personnelle en cours de validité (carte Bleue, Carte Visa, Mastercard). Si le paiement se fait par des cartes émises par des banques domiciliées hors de France, elles doivent être obligatoirement des cartes bancaires internationales.</p>
        <p>Par les présentes conditions générales de vente, vous acceptez la conservation sécurisée de vos données bancaires pour la durée nécessaire des abonnements.</p>
        <p>Tout paiement sur le Site fait l'objet d'un système de sécurisation par certificat SSL (Secure Sockets Layer) permettant le cryptage de l'information relative à vos coordonnées bancaires.</p><hr>
        <h2>Article 4 : Prix et durée des abonnements</h2>
        <p><strong>4.1</strong> Le paiement des abonnements aux Contenus Numériques s'effectue selon les tarifs et modalités en vigueur sur le Site au jour de l'enregistrement de la commande, SEJER se réservant le droit de les modifier à tout moment. Les prix des abonnements aux Contenus Numériques sont indiqués en Euros toutes taxes comprises. Le coût de la communication pour accéder au Site restant à votre charge.</p>
        <p><strong>4.2</strong> Les durées des différents abonnements aux Contenus Numériques sont fixées dans les offres d'abonnement détaillées sur le Site.</p>
        <p>Conformément à l'article L. 136-1 du Code de la Consommation et pour les abonnements avec tacite reconduction, nous vous informerons par écrit, par lettre nominative ou courrier électronique dédiés, au plus tôt trois mois et au plus tard un mois avant le terme de la période autorisant le rejet de la reconduction, de la possibilité de ne pas reconduire le contrat d'abonnement. Cette information, délivrée dans des termes clairs et compréhensibles, mentionne, dans un encadré apparent, la date limite de résiliation.</p>
        <p>Lorsque cette information ne vous a pas été adressée conformément aux dispositions du premier alinéa, vous pouvez mettre gratuitement un terme à votre contrat d'abonnement, à tout moment à compter de la date de reconduction. Les avances effectuées après la dernière date de reconduction ou, s'agissant des contrats à durée indéterminée, après la date de transformation du contrat initial à durée déterminée, sont dans ce cas remboursées dans un délai de trente jours à compter de la date de résiliation, déduction faite des sommes correspondant, jusqu'à celle-ci, à l'exécution du contrat. A défaut de remboursement dans les conditions prévues ci-dessus, les sommes dues sont productives d'intérêts au taux légal.</p><hr>
        <h2>Article 5 : Modalités techniques d'accès aux contenus numériques</h2>
        <p>Les modalités techniques générales associées à la consultation en ligne ou au téléchargement des Contenus Numériques sont détaillées sur le Site. En cas de difficultés, et notamment dans le cas où vous ne recevez pas le lien de lecture en ligne ou de téléchargement sur l'adresse e-mail associée à votre compte client, vous pouvez contacter la Relation client de SEJER : <br>
          SEJER/BORDAS – Relation clients <br>
          31 avenue Pierre de Coubertin<br>
          75013 PARIS<br>
          Téléphone : 01 72 36 40 91 <br>
          Adresse e-mail : soutien-scolaire@bordas.tm.fr<br>
          Vous devez vous assurer de la configuration minimale requise avant chaque commande de Contenus Numériques sans recours possible contre INTERFORUM et SEJER.</p><hr>
        <h2>Article 6 : Protection des droits d'auteur</h2>
        <p>Les Contenus Numériques ainsi que tous les éléments reproduits sur le Site (notamment textes, commentaires, illustrations, logos et documents iconographiques) sont protégés par le Code de la Propriété Intellectuelle et par les normes internationales applicables.</p>
        <p>Tout échange, revente ou mise à disposition à un tiers des Contenus Numériques est strictement interdit sous réserve des conditions générales d'utilisation du Site et sera considéré comme une violation du droit d'auteur passible de poursuites pénales. Les conditions d'utilisation des Contenus Numériques sont présentées sur les licences d'utilisation de chaque Contenu Numérique.</p>
        <p>Vous vous engagez expressément à garder confidentiel le lien de consultation en ligne ou de téléchargement qui vous sera transmis et à ne pas le communiquer sous quelque forme que ce soit à un tiers.</p><hr>
        <h2>Article 7 : Responsabilité</h2>
        <p><strong>7.1 </strong>Les garanties légales de conformité et des vices cachés sont assurées dans les conditions de l'article L 133-3 du Code de la Consommation.</p>
        <p>INTERFORUM et SEJER ne peuvent être tenu responsables des interruptions de services dues aux caractéristiques et limites du réseau Internet notamment dans le cas d'interruption des réseaux d'accès, des performances techniques et des temps de réponse pour consulter, interroger ou transférer les informations contenues sur le Site ou les Contenus Numériques. Compte tenu des caractéristiques intrinsèques de l'Internet, INTERFORUM et SEJER ne vous garantissent pas contre les risques notamment de détournement, d'intrusion, de contamination et de piratage de vos données, programmes et fichiers. Il vous appartient de prendre toutes mesures appropriées de nature à protéger vos propres données, programmes et fichiers notamment contre les virus informatiques.</p>
        <p><strong>7.2</strong> Vous devez vous assurer de la configuration minimale requise avant toute commande de Contenus Numériques sans recours possible contre SEJER et INTERFORUM. Toutes les mises à jour des Contenus Numériques et notamment des systèmes d'exploitation sont à votre charge exclusive.</p><hr>
        <h2>Article 8 : Protection des données personnelles</h2>
        <p>Les informations et données vous concernant sont nécessaires à la gestion de votre commande. Vous serez susceptibles de recevoir des offres des marques de SEJER si vous les avez acceptées au moment de votre commande. Conformément à la loi Informatique et Libertés du 6 janvier 1978 modifiée en 2004, vous disposez d'un droit d'accès, de rectification ou d'opposition aux données vous concernant en vous adressant à la Relation Client de SEJER (article 5).</p><hr>
        <h2>Article 9 : Applications des conditions générales de vente - opposabilité</h2>
        <p>Toute commande implique votre adhésion entière et sans réserve à ces conditions générales de vente et nonobstant toutes clauses et stipulations contraires.</p>
        <p>Aucune condition particulière ne peut prévaloir sur les présentes conditions générales de vente sauf acceptation écrite et signée par INTERFORUM. Toute condition contraire, non expressément acceptée par INTERFORUM, lui sera donc inopposable quel que soit le moment où elle aura pu être portée à sa connaissance.</p>
        <p>Le fait qu'INTERFORUM ne se prévale pas à un moment donné de l'une quelconque des présentes conditions générales de vente ne peut être interprété comme valant renonciation à se prévaloir ultérieurement de l'une quelconque desdites conditions.</p><hr>
        <h2>Article 10 : Litiges - Attribution de juridiction</h2>
        <p>En cas de contestation du contrat de vente, vous pouvez recourir à une procédure de médiation conventionnelle ou à tout autre mode alternatif des règlements des différends. En cas de procédure judiciaire, les actions engagées le seront devant le Tribunal du lieu où demeure le défenseur en justice.</p>
        <p>Le Droit Français est seul applicable, sous réserve de l'application de la loi du pays dans lesquels les consommateurs ont leur résidence habituelle en cas d'exportation. En cas de traduction des présentes, la version française prédomine.</p>
      </div>
      <div class="col-md-4 hidden-xs hidden-sm">
        <div class=""> 
          <!-- 16:9 aspect ratio -->
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CItcTboz6_k?rel=0"></iframe>
          </div>
        </div>
        <hr>
        <div class="bloc-annonce"><div class="prepend"><b class="caret"></b> publicité <b class="caret"></b></div><a href="#"><img src="http://www.bordas-soutien-scolaire.com/images/bss-wengo-300.gif" class="img-responsive"></a></div>
      </div>
      <div class="col-md-12">
        <hr>
        <p class="text-center"><a class="btn btn-md btn-primary" href="#">Je découvre toutes les formules d'abonnement<i class="icon-angle-right"></i></a></p>
      </div>
    </div>
  </div>
</div>
<!--FOOTER-->
<?php include 'footer.php';?>
<!--SCRIPTS-->
<?php include 'scripts.php';?>
</body>
</html>
