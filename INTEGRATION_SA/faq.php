<!doctype html>
<html lang="fr" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<title>BSS FAQ</title>
<link rel="canonical" href="[canonical]" />
<meta name="description" content="[description]" />
<meta name="author" content="[author]" />
<meta property="og:locale" content="fr_FR" />
<meta property="og:type" content="website" />
<meta property="og:title" content="[title]" />
<meta property="og:description" content="[description]" />
<meta property="og:url" content="[URL]" />
<meta property="og:site_name" content="[site_name]" />
<meta property="og:image" content="[image]" />
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:description" content="[description]"/>
<meta name="twitter:title" content="[title]"/>
<meta name="twitter:site" content="[site]"/>
<meta name="twitter:domain" content="[domain]"/>
<meta name="twitter:image:src" content="[image]" />
<!--STYLES-->
<?php include 'styles.php';?>

</head>
<body class="page-simple " id="faq">

<!-- HEADER -->
<header class="bss-header">
  <?php include 'header.php';?>
  <?php include 'menu.php';?>
</header>
<div class="main animsition">

<!-- BREADCRUMB -->
<div class="bss-breadcrumb" >
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h6>
          <ol class="breadcrumb">
            <li id="a" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="index.php" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
            <li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="faq.php" itemprop="url"> <span itemprop="title">FAQ</span> </a></li>
          </ol>
        </h6>
      </div>
    </div>
  </div>
</div>
<div class="bss-section bloc-section-orange bss-ccm">
  <div class="container">
    <h1 id="tagline" class="h1">FAQ</h1>
    <div class="row">
      <div class="col-md-8">
        <h2 id="subtitle" class="h2">Informations sur l'offre de soutien scolaire en ligne</h2>
        <div class="panel-group" id="accordion-faq-info" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="info-headingOne">
              <h4 class="panel-title"> <a class="collapsed" ole="button" data-toggle="collapse" data-parent="#accordion-faq-info" href="#info-collapseOne" aria-expanded="false" aria-controls="info-collapseOne"> Que propose le service Bordas Soutien Scolaire ? </a> </h4>
            </div>
            <div id="info-collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="info-headingOne">
              <div class="panel-body"> 
                <p>Bordas Soutien scolaire propose une solution de <a href="#">soutien scolaire numérique</a>, afin de permettre aux enfants de réviser et de s'entraîner à la maison, sur ordinateur ou sur tablette, grâce à des cours et des exercices interactifs, des quiz, des vidéos... Ce service est accessible sur <a href="#">simple abonnement</a>.              </p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="info-headingTwo">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-info" href="#info-collapseTwo" aria-expanded="false" aria-controls="info-collapseTwo"> Pourquoi s'inscrire à notre newsletter ? </a> </h4>
            </div>
            <div id="info-collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="info-headingTwo">
              <div class="panel-body"> 
                <p>En vous inscrivant à la newsletter du service de révisions en ligne de <strong>Bordas Soutien scolaire</strong>, vous recevrez des offres privilégiées (réductions, offres découvertes, codes promotionnels, jeux-concours...), et vous serez informé des dernières actualités de la plateforme (ajout de nouveaux contenus, de nouvelles classes, de nouvelles matières, ...).                  </p>
                <p>Vous pouvez vous désinscrire à tout moment en vous identifiant sur le site et en cliquant sur "Mon espace" puis sur "Gérer mon compte". </p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-info" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> A qui s'adresse le service Bordas Soutien Scolaire ?</a> </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body"> 
                <p>Notre solution de <a href="#">soutien scolaire en ligne</a> s'adresse à tous les enfants du CP jusqu'à la Terminale, ainsi qu'aux parents soucieux de suivre le travail de leurs enfants ou de les accompagner dans leurs révisions. Grâce à un système de parcours personnalisé, Bordas Soutien scolaire s'adapte au niveau de chaque enfant.                  </p>
                <p>Vous pouvez, au choix :                  </p>
                <ul>
                  <li>abonner un seul enfant pour le faire travailler sur une matière uniquement ;</li>
                  <li>abonner un seul enfant en lui donnant accès à toutes les matières de l'année ;</li>
                  <li>ou encore abonner tous vos enfants en une seule fois, en leur donnant accès à toutes les matières.</li>
                </ul>
                <p>Retrouvez plus d'informations sur nos offres d'abonnement en <a href="#">cliquant ici</a>. </p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFour">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-info" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour"> Qui rédige les contenus de Bordas Soutien scolaire ?</a> </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
              <div class="panel-body"> Tous les contenus (cours et exercices interactifs, quiz…) sont rédigés par des enseignants en poste. Les enseignants sont sélectionnés par les <a href="#">éditions Bordas</a>, qui valident également tous les contenus créés.</div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFive">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-info" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive"> Les contenus sont-ils conformes au programme ?</a> </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
              <div class="panel-body"> Oui, tous les contenus de notre service de soutien scolaire en ligne respectent les programmes de l'Education nationale et sont mis à jour à chaque changement de programme. Ces contenus sont rédigés par des enseignants en poste et validés par les éditions Bordas.</div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingSix">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-info" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix"> Bordas soutien scolaire propose-t-il des cours en ligne ou des cours à domicile ?</a> </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
              <div class="panel-body">Bordas Soutien scolaire propose à la fois des<strong> cours particuliers</strong>, sur le site <a href="#">www.bordas.com</a>, et un service de <strong>soutien scolaire en ligne</strong>, sur le site <a href="#">www.bordas-soutien-scolaire.com/soutien-scolaire-en-ligne/</a>. </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingSeven">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-info" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven"> J'ai des questions sur les cours à domicile, à qui dois-je les poser ?</a> </h4>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
              <div class="panel-body">Cette FAQ concerne uniquement le service de <a href="#">révisions en ligne</a> de Bordas Soutien scolaire. Pour toute question relative aux cours à domicile, rendez-vous sur le site <a href="#">www.bordas.com</a> et cliquez sur "Nous contacter". </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingEight">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-info" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight"> Qui est Bordas ?</a> </h4>
            </div>
            <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
              <div class="panel-body">Les <a href="#">éditions Bordas</a>, fondées en 1946, sont spécialisées dans l'éducation et l'accompagnement de tous les élèves, depuis la maternelle jusqu'au lycée. Elles publient aussi bien des manuels scolaires que des ouvrages parascolaires.
Soucieuses de trouver de nouvelles solutions pour faire progresser les enfants, elles ont développé un service de <a href="#.">révisions en ligne</a> sous la marque Bordas Soutien scolaire. </div>
            </div>
          </div>
        </div>
        <hr>
        <h2>Mon compte</h2>
        <div class="panel-group" id="accordion-faq-compte" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="compte-headingOne">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-compte" href="#compte-collapseOne" aria-expanded="false" aria-controls="compte-collapseOne"> Je veux changer mon profil.</a> </h4>
            </div>
            <div id="compte-collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="compte-headingOne">
              <div class="panel-body"> Pour mettre à jour vos informations personnelles, identifiez-vous sur notre site et cliquez sur "Mon espace" puis sur "Gérer mon compte".</div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="compte-headingTwo">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-compte" href="#compte-collapseTwo" aria-expanded="false" aria-controls="compte-collapseTwo"> Je veux changer mon mot de passe.</a> </h4>
            </div>
            <div id="compte-collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="compte-headingTwo">
              <div class="panel-body">
                <ul>
                  <li> Pour modifier votre mot de passe d'accès au site internet : pour mettre à jour vos informations personnelles, identifiez-vous sur notre site et cliquez sur "Mon espace" > "Gérer mon compte", puis sur "Modifier le mot de passe".                    </li>
                  <li>Pour modifier votre mot de passe d'accès à la plateforme de révision en ligne : il est impossible de changer ce mot de passe, nous vous conseillons donc de bien conserver l'email de confirmation d'inscription où sont spécifiés votre mot de passe ainsi que votre identifiant. </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="compte-headingThree">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-compte" href="#compte-collapseThree" aria-expanded="false" aria-controls="compte-collapseThree"> Comment mes données personnelles sont-elles protégées ?</a> </h4>
            </div>
            <div id="compte-collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="compte-headingThree">
              <div class="panel-body"> 
                <p>Les informations vous concernant sont nécessaires au suivi de votre commande et de l'apprentissage de votre enfant. Vous ne recevrez des offres des éditions Bordas et de ses partenaires que si vous les avez acceptées.                  </p>
                <p>Conformément à la loi informatique et libertés du 6 janvier 1978, vous disposez d’un droit d’accès, de rectification ou d’opposition aux données personnelles vous concernant. Pour cela, il suffit de vous adresser à notre <a href="#">service Clients</a>. </p>
              </div>
            </div>
          </div>
        </div>
        <hr>
        <h2 id="subtitle" class="h2">Mon abonnement</h2>
        <div class="panel-group" id="accordion-faq-abo" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="abo-headingOne">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-abo" href="#abo-collapseOne" aria-expanded="false" aria-controls="abo-collapseOne"> Quelle formule choisir ? </a> </h4>
            </div>
            <div id="abo-collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="abo-headingOne">
              <div class="panel-body"> 
                <p>Pour 1 enfant vous pouvez choisir la formule Ciblée qui donne accès à une seule matière, ou à la formule Réussite qui donne accès à toutes les matières. Pour plusieurs enfants, vous pouvez choisir la formule Tribu qui donne accès à toutes les matières, pour 2 à 5 enfants.  </p>
                <p>Pour comparer tous les tarifs, <a href="#">cliquez ici</a>.</p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="abo-headingTwo">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-abo" href="#abo-collapseTwo" aria-expanded="false" aria-controls="abo-collapseTwo"> Puis-je abonner plusieurs enfants à Bordas Soutien Scolaire ?</a> </h4>
            </div>
            <div id="abo-collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="abo-headingTwo">
              <div class="panel-body"> Oui, la <a href="#">formule Tribu</a> vous permet d'abonner tous vos enfants (dans la limite de 5 enfants). Chaque enfant pourra accéder à toutes les matières disponibles pour sa classe. </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="abo-headingThree">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-abo" href="#abo-collapseThree" aria-expanded="false" aria-controls="abo-collapseThree"> Je n'arrive pas à m'abonner dans la classe ou la matière de mon choix</a> </h4>
            </div>
            <div id="abo-collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="abo-headingThree">
              <div class="panel-body"> Actuellement, certaines classes ou matières ne sont pas encore disponibles, mais vous pourrez y accéder très prochainement. Pour voir la liste des classes et des matières disponibles, <a href="#">cliquez ici</a>. </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="abo-headingFour">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-abo" href="#abo-collapseFour" aria-expanded="false" aria-controls="abo-collapseFour"> Mon abonnement arrive à expiration, comment le renouveler ?</a> </h4>
            </div>
            <div id="abo-collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="abo-headingFour">
              <div class="panel-body"> 
                <p>Si vous avez choisi un abonnement d'un mois ou de trois mois, vous n'avez rien à faire : votre abonnement sera reconduit automatiquement à la fin de chaque période. Nous vous préviendrons par e-mail un mois avant chaque reconduction.                  </p>
                <p>Si vous avez choisi un abonnement d'un an, il vous suffit de vous réabonner en <a href="#">cliquant ici</a>. </p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="abo-headingFive">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-abo" href="#abo-collapseFive" aria-expanded="false" aria-controls="abo-collapseFive"> A quoi sert mon numéro client ?</a> </h4>
            </div>
            <div id="abo-collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="abo-headingFive">
              <div class="panel-body"> Ce numéro client nous permet d'assurer le suivi de vos commandes et de répondre au mieux à vos demandes. Il n'est pas nécessaire de le renseigner pour créer le compte : il est généré automatiquement. Pour retrouver votre numéro client, identifiez-vous sur notre site et cliquez sur "Mon espace" > "Gérer mon compte".</div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="abo-headingSix">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-abo" href="#abo-collapseSix" aria-expanded="false" aria-controls="abo-collapseSix"> J'ai déjà un compte mais j'ai oublié mon identifiant et mon mot de passe.</a> </h4>
            </div>
            <div id="abo-collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="abo-headingSix">
              <div class="panel-body"> Pour retrouver l'identifiant ou le mot de passe que vous avez créés lors de votre inscription sur notre site, il suffit de nous indiquer l'adresse e-mail saisie lors de l'inscription en <a href="#">cliquant ici</a>. </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="abo-headingSeven">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-abo" href="#abo-collapseSeven" aria-expanded="false" aria-controls="abo-collapseSeven"> Comment mettre à jour mes coordonnées bancaires pour ne pas interrompre mon abonnement ?</a> </h4>
            </div>
            <div id="abo-collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="abo-headingSeven">
              <div class="panel-body"> Pour mettre à jour vos coordonnées bancaires, identifiez-vous sur notre site et cliquez sur "Mon espace" > "Gérer mon abonnement", puis sur "Modifier/supprimer ma CB". </div>
            </div>
          </div>
        </div>
        <hr>
        <h2 id="subtitle" class="h2">Paiement</h2>
        <div class="panel-group" id="accordion-faq-paiement" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="paiement-headingOne">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-paiement" href="#paiement-collapseOne" aria-expanded="false" aria-controls="paiement-collapseOne"> Puis-je payer autrement que par carte bancaire ? (chèque, …) </a> </h4>
            </div>
            <div id="paiement-collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="paiement-headingOne">
              <div class="panel-body"> Non, vous pouvez payer votre abonnement uniquement par carte bancaire (Carte bleue, Visa ou Mastercard). </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="paiement-headingTwo">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-paiement" href="#paiement-collapseTwo" aria-expanded="false" aria-controls="paiement-collapseTwo"> Le paiement est-il sécurisé ?</a> </h4>
            </div>
            <div id="paiement-collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="paiement-headingTwo">
              <div class="panel-body">Oui : tout paiement effectué sur notre site fait l’objet d’un système de sécurisation par certificat SSL (Secure Sockets Layer) permettant le cryptage de l'information relative à vos coordonnées bancaires. </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="paiement-headingThree">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-paiement" href="#paiement-collapseThree" aria-expanded="false" aria-controls="paiement-collapseThree"> Je souhaite une facture ? </a> </h4>
            </div>
            <div id="paiement-collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="paiement-headingThree">
              <div class="panel-body"> Pour recevoir une facture, il vous suffit d'adresser un e-mail à <a href="#">soutien-scolaire@bordas.tm.fr</a> avec vos coordonnées complètes : prénom, nom, adresse postale, et numéro client. </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="paiement-headingFour">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-paiement" href="#paiement-collapseFour" aria-expanded="false" aria-controls="paiement-collapseFour"> Comment mettre à jour mes coordonnées bancaires pour ne pas interrompre mon abonnement ?</a> </h4>
            </div>
            <div id="paiement-collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="paiement-headingFour">
              <div class="panel-body"> Pour mettre à jour vos coordonnées bancaires, identifiez-vous sur notre site et cliquez sur "Mon espace" > "Gérer mon abonnement", puis sur "Modifier/supprimer ma CB".
</div>
            </div>
          </div>
        </div>
        <hr>
        <h2 id="subtitle" class="h2">Offre découverte "1 mois d'accès offert pour 1 ouvrage Bordas acheté"</h2>
        <div class="panel-group" id="accordion-faq-offre" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="offre-headingOne">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-offre" href="#offre-collapseOne" aria-expanded="false" aria-controls="offre-collapseOne"> Puis-je bénéficier de cette offre ?</a> </h4>
            </div>
            <div id="offre-collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="offre-headingOne">
              <div class="panel-body"> Pour pouvoir bénéficier de cette offre, vous devez avoir acheté un ouvrage Bordas parmi les collections indiquées sur la page <a href="#">www.bordas-soutien-scolaire.com/decouverte/</a>. Cette offre est valable une seule fois, elle n'est pas cumulable avec une autre offre en cours.</div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="offre-headingTwo">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-offre" href="#offre-collapseTwo" aria-expanded="false" aria-controls="offre-collapseTwo"> Comment puis-je activer l'offre ?</a> </h4>
            </div>
            <div id="offre-collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="offre-headingTwo">
              <div class="panel-body"> 
                <p>Munissez-vous de l'ouvrage que vous avez acheté et rendez-vous sur <a href="#">www.bordas-soutien-scolaire.com/decouverte/</a> :                  </p>
                <ul>
                  <li>indiquez le code EAN (les 13 chiffres en-dessous du code-barres) qui se trouve sur le verso de l'ouvrage que vous avez acheté ;                    </li>
                  <li>une question vous sera posée afin d'authentifier votre achat ;</li>
                  <li> indiquez vos coordonnées dans le formulaire ;                    </li>
                  <li>enfin, vous recevrez un email avec vos identifiants et le lien pour accéder au service de soutien scolaire en ligne. </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="offre-headingThree">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-offre" href="#offre-collapseThree" aria-expanded="false" aria-controls="offre-collapseThree"> Je ne trouve pas l'ouvrage que j'ai acheté dans la liste. </a> </h4>
            </div>
            <div id="offre-collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="offre-headingThree">
              <div class="panel-body">L'offre d'1 mois d'accès offert est limitée aux ouvrages dont le code EAN (les 13 chiffres en-dessous du code-barres) est indiqué dans la liste précisée sur la page <a href="#">www.bordas-soutien-scolaire.com/decouverte/</a>.</div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="offre-headingFour">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-offre" href="#offre-collapseFour" aria-expanded="false" aria-controls="offre-collapseFour"> Le mot que j'ai indiqué ne fonctionne pas, comment faire ?</a> </h4>
            </div>
            <div id="offre-collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="offre-headingFour">
              <div class="panel-body">Vérifiez que vous renseigné le bon code EAN (les 13 chiffres en-dessous du code-barres) et consultez votre ouvrage afin de répondre à la question posée. Si cela ne fonctionne toujours pas après vérification, vous pouvez contacter notre <a href="#">service Clients</a>.</div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="offre-headingFive">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-offre" href="#offre-collapseFive" aria-expanded="false" aria-controls="offre-collapseFive"> Quand puis-je profiter de cette offre ?</a> </h4>
            </div>
            <div id="offre-collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="offre-headingFive">
              <div class="panel-body"> Cette offre est valable du 1er septembre 2014 au 1er septembre 2015. Vous pouvez utiliser la plateforme Bordas Soutien scolaire pour une durée d'un mois à compter du jour où vous avez activé l'offre. Vous ne pouvez profiter de cette offre qu'une seule fois. </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="offre-headingSix">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-offre" href="#offre-collapseSix" aria-expanded="false" aria-controls="offre-collapseSix"> Autres questions</a> </h4>
            </div>
            <div id="offre-collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="offre-headingSix">
              <div class="panel-body"> <a href="#">soutien-scolaire@bordas.tm.fr</a></div>
            </div>
          </div>
        </div>
        <hr>
        <h2 id="subtitle" class="h2">Accès à la plateforme de soutien scolaire</h2>
        <div class="panel-group" id="accordion-faq-acces" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="acces-headingOne">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-acces" href="#acces-collapseOne" aria-expanded="false" aria-controls="acces-collapseOne"> Quel est le matériel nécessaire pour utiliser ce service ?</a> </h4>
            </div>
            <div id="acces-collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="acces-headingOne">
              <div class="panel-body"> 
                <p>Vous pouvez utiliser le service : </p>
                <ul>
                  <li>sur tout ordinateur utilisant comme navigateur : Chrome version 35.X et versions ultérieures, Internet Explorer 10.0 et versions ultérieures ou Firefox 31 et versions ultérieures  ; </li>
                  <li>sur tout tablette utilisant comme système d'exploitation : Android 4.1.2 minimum ou iOS 6.0 minimum ou Internet Explorer 10.0 et versions ultérieures ; </li>
                  <li>avec un écran de 9 pouces minimum (recommandation) </li>
                </ul>
                <p>Vous retrouverez toutes les configurations minimales requises en consultant les conditions générales d'utilisation.</p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="acces-headingTwo">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-acces" href="#acces-collapseTwo" aria-expanded="false" aria-controls="acces-collapseTwo"> Comment accéder à la plateforme ?</a> </h4>
            </div>
            <div id="acces-collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="acces-headingTwo">
              <div class="panel-body"> 
                <p>Il vous suffit de vous connecter sur www.bordas-soutien-scolaire.com/soutien-scolaire-en-ligne/ avec votre identifiant et votre mot de passe, et de cliquer sur &quot;&quot;Mon espace&quot;&quot; puis sur &quot;&quot;Accéder à la plateforme de soutien scolaire&quot;&quot;. </p>
                <p>Vous pouvez aussi vous connecter directement sur <a href="#">http://bordas-soutien-scolaire.eduplateforme.com</a>. </p>
                <p>Vos identifiants et ceux de vos enfants sont indiqués dans l'e-mail de confirmation de commande qui vous a été envoyé suite à votre abonnement.</p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="acces-headingThree">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-acces" href="#acces-collapseThree" aria-expanded="false" aria-controls="acces-collapseThree"> Puis-je l'utiliser sur tablette ou sur smartphone ? </a> </h4>
            </div>
            <div id="acces-collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="acces-headingThree">
              <div class="panel-body"> 
                <p>Le service de <a href="#">soutien scolaire en ligne</a> est utilisable sur tablette, mais, pour l'instant, il n'est pas possible d'y accéder sur smartphone. </p>
                <p>Vous retrouverez toutes les configurations minimales requises en consultant les <a href="#">conditions générales d'utilisation</a>.</p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="acces-headingFour">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-acces" href="#acces-collapseFour" aria-expanded="false" aria-controls="acces-collapseFour"> Je n'ai pas reçu mes identifiants</a> </h4>
            </div>
            <div id="acces-collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="acces-headingFour">
              <div class="panel-body"> Vous avez dû recevoir un e-mail de confirmation de commande avec vos identifiants et ceux de vos enfants au plus tard 24h après votre inscription. Vérifiez que l'e-mail ne se trouve pas dans votre courrier indésirable. Si vous ne retrouvez pas cet e-mail, envoyez une demande à notre <a href="#">service Clients</a> en précisant les noms, prénoms et adresse e-mail utilisés lors de la commande. </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="acces-headingFive">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-acces" href="#acces-collapseFive" aria-expanded="false" aria-controls="acces-collapseFive"> Quels sont les logiciels nécessaires ?</a> </h4>
            </div>
            <div id="acces-collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="acces-headingFive">
              <div class="panel-body"> 
                <p>Aucun logiciel n'est nécessaire pour utiliser le service Bordas Soutien scolaire, cependant les navigateurs Google Chrome 35.X et versions ultérieures, Internet Explorer 10.0 et versions ultérieures ou Firefox 31 et versions ultérieures sont pour l'instant indispensable pour pouvoir utiliser le service de révisions en ligne. Pour le télécharger, rendez-vous sur <a href="#">cette page</a>. </p>
                <p>Vous retrouverez toutes les configurations minimales requises en consultant les <a href="#">conditions générales d'utilisation</a>.</p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="acces-headingSix">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-acces" href="#acces-collapseSix" aria-expanded="false" aria-controls="acces-collapseSix"> Mes identifiants ne fonctionnent pas</a> </h4>
            </div>
            <div id="acces-collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="acces-headingSix">
              <div class="panel-body"> Vérifiez que les identifiants utilisés sont bien ceux qui figurent dans l'e-mail de confirmation de commande que vous avez reçu lors de votre abonnement, et réessayez. Si le problème persiste, contactez notre <a href="#">service Clients</a>. </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="acces-headingSeven">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-acces" href="#acces-collapseSeven" aria-expanded="false" aria-controls="acces-collapseSeven"> Puis-je accéder au service depuis n'importe quel ordinateur ?</a> </h4>
            </div>
            <div id="acces-collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="acces-headingSeven">
              <div class="panel-body"> Oui, vous pouvez accéder à notre service depuis n'importe quel ordinateur ou tablette correspondant aux <a href="#">configurations minimales requises</a>, avec une simple connexion internet, quel que soit le lieu où vous vous trouvez.</div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="acces-headingEight">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-acces" href="#acces-collapseEight" aria-expanded="false" aria-controls="acces-collapseEight"> J'ai perdu mes identifiants, comment les retrouver ? </a> </h4>
            </div>
            <div id="acces-collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="acces-headingEight">
              <div class="panel-body"> Pour retrouver l'identifiant ou le mot de passe que vous avez créés lors de votre inscription sur notre site, il suffit de nous indiquer l'adresse e-mail saisie lors de l'inscription <a href="#">en cliquant ici</a>.</div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="acces-headingNine">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-acces" href="#acces-collapseNine" aria-expanded="false" aria-controls="acces-collapseNine"> Puis-je l'utiliser sans connexion Internet ?</a> </h4>
            </div>
            <div id="acces-collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="acces-headingNine">
              <div class="panel-body"> 
                <p>Oui : </p>
                <ul>
                  <li> Sur tablette : pour utiliser le mode hors connexion, vous devez préalablement être en ligne sur le compte élève et télécharger les chapitres que vous désirez utiliser hors ligne. Les chapitres téléchargés seront accessibles une fois hors connexion internet. Les chapitres non téléchargés sont grisés, et ne deviendront accessible qu’après rétablissement de la connexion internet. </li>
                  <li>Sur ordinateur PC ou Mac : pour utiliser le mode hors connexion, vous devez préalablement être en ligne sur le compte élève et télécharger les chapitres que vous désirez utiliser hors ligne. Les chapitres téléchargés seront accessibles une fois hors connexion internet, en passant par le même navigateur. Les chapitres non téléchargés sont grisés, et ne deviendront accessibles qu’après rétablissement de la connexion internet. Si vous effacez le cache du navigateur, vous effacerez les chapitres téléchargés.</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="acces-headingTen">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-acces" href="#acces-collapseTen" aria-expanded="false" aria-controls="acces-collapseTen"> Je ne parviens pas à utiliser la plateforme </a> </h4>
            </div>
            <div id="acces-collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="acces-headingTen">
              <div class="panel-body"> 
                <p>Vous pouvez utiliser la plateforme uniquement avec les navigateurs Google Chrome 35.X et versions ultérieures, Internet Explorer 10.0 et versions ultérieures ou Firefox 31 et versions ultérieures . Pour télécharge Google Chrome rendez vous sur <a href="#">cette page</a>.  Pour télécharger Internet Explorer, rendez-vous <a href="#">ici</a>. </p>
                <p>Vous retrouverez toutes les configurations minimales requises en consultant les <a href="#">conditions générales d'utilisation</a>.</p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="acces-headingEleven">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq-acces" href="#acces-collapseEleven" aria-expanded="false" aria-controls="acces-collapseEleven"> Je ne parviens pas à faire mes exercices</a> </h4>
            </div>
            <div id="acces-collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="acces-headingEleven">
              <div class="panel-body"> Etes-vous bien connecté au compte élève ? Si vous êtes sur le compte parents, c'est normal, ce dernier n'est qu'en lecture seule, c'est-à-dire que vous pouvez voir, mais vous ne pouvez pas effectuer les exercices. Afin de faire les exercices vous devez donc vous connecter au compte élève.</div>
            </div>
          </div>
        </div>
        <hr>
      </div>
      <div class="col-md-4 hidden-xs hidden-sm">
        <div class=""> 
          <!-- 16:9 aspect ratio -->
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CItcTboz6_k?rel=0"></iframe>
          </div>
        </div>
        <hr>
        <div class="bloc-annonce"><div class="prepend"><b class="caret"></b> publicité <b class="caret"></b></div><a href="#"><img src="http://www.bordas-soutien-scolaire.com/images/bss-wengo-300.gif" class="img-responsive"></a></div>
      </div>
      <div class="col-md-12">
        <hr>
        <p class="text-center"><a class="btn btn-md btn-primary" href="#">Je découvre toutes les formules d'abonnement<i class="icon-angle-right"></i></a></p>
      </div>
    </div>
  </div>
</div>

<!--FOOTER-->
<?php include 'footer.php';?>
<!--SCRIPTS-->
<?php include 'scripts.php';?>
</body>
</html>
