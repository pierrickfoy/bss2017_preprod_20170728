<?php include ("modal.php");?>

<!--PIED DE SITE-->
<footer class="bss-footer">
<div class="top-footer">
  <div class="container">
    <ul class="row list-inline reassurance text-center">
      <li class="col-xs-6 col-sm-4 col-md-2"><img src="img/reassurance-1946.png" class="img-responsive">
        <p>Bordas : Éditeur scolaire depuis 1946</p>
      </li>
      <li class="col-xs-6 col-sm-4 col-md-2"><img src="img/reassurance-enseignant.png" class="img-responsive">
        <p>Conforme aux programmes de l’Éducation nationale</p>
      </li>
      <li class="col-xs-6 col-sm-4 col-md-2"><img src="img/reassurance-livres.png" class="img-responsive">
        <p>Contenus rédigés par des enseignants</p>
      </li>
      <li class="col-xs-6 col-sm-4 col-md-2"><img src="img/reassurance-famille.png" class="img-responsive">
        <p>Un compte Parent pour suivre ses résultats</p>
      </li>
      <li class="col-xs-6 col-sm-4 col-md-2"><img src="img/reassurance-secure.png" class="img-responsive">
        <p>Paiement sécurisé</p>
        <p><img src="img/icone-paiement.png" class="img-responsive"></p>
      </li>
      <li class="col-xs-6 col-sm-4 col-md-2"><img src="img/reassurance-service-client.png" class="img-responsive">
        <p>Un service Client à votre écoute</p>
      </li>
    </ul>
  </div>
</div>  
<div class="mid-footer">
  <div class="container ">
    <div class="row">
    <div class="col-sm-12 h1 text-center">Besoin d'aide ?</div>
      <div class="col-sm-4 clearfix">
       
          <img src="img/aide-phone.png" class="img-responsive align-left hidden-xs hidden-sm"> 
         
            <ul class="list-unstyled">
              <li><strong>Contactez-nous par téléphone :</strong> </li>
              <li class="phone-number">01 72 36 40 91 </li>
              <li>Du lundi au vendredi, </li>
              <li>de 9h à 13h et de 14h à 18h</li>
            </ul>
         
       
      </div>
      <div class="col-sm-4 clearfix">
   
          <img src="img/aide-mail.png" class="img-responsive align-left hidden-xs hidden-sm"> 
         
            <ul class="list-unstyled">
              <li><strong>Contactez-nous par email :</strong> </li>
              <li><a href="mailto:soutien-scolaire@bordas.tm.fr">soutien-scolaire@bordas.tm.fr</a></li>
            </ul>
       
    
      </div>
      <div class="col-sm-4 clearfix">
    
          <img src="img/aide-faq.png" class="img-responsive align-left hidden-xs hidden-sm">  
        
            <ul class="list-unstyled">
              <li> <strong>Consultez notre FAQ :</strong> </li>
              <li><a href="faq.php">Questions fréquentes</a></li>
            </ul>
        
   
      </div>
    </div>
  </div>
</div> 
<div class="bot-footer">   
  <div class="container ">
    <div class="row">
      <div class="col-sm-6 ">
        <ul class="list-inline">
          <li><a href="mentions-legales.php">Mentions légales & Crédits</a></li>
          <li> <a href="cgu.php">CGU</a></li>
          <li><a href="cgv.php">CGV</a></li>
          <li><a href="sitemap.php">Plan du site</a></li>
        </ul>
      </div>
      <div class="col-sm-6 text-right">
        <ul class="list-inline">
          <li>Copyright 2015</li>
          <li>Bordas Soutien scolaire</li>
          <li><a href="http://www.editions-bordas.fr/" target="_blank" class="logo-bordas">Bordas</a></li>
        </ul>
      </div>
    </div>
  </div>
 </div>  
</footer>
<!--<p class="text-center">
  <button type="button" class="btn btn-default">Default</button>
  <button type="button" class="btn btn-primary">Primary</button>
  <button type="button" class="btn btn-success">Success</button>
  <button type="button" class="btn btn-info">Info</button>
  <button type="button" class="btn btn-warning">Warning</button>
  <button type="button" class="btn btn-danger">Danger</button>
  <button type="button" class="btn btn-link">Link</button>
</p>
-->