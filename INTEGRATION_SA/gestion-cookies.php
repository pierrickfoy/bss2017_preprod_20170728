<!doctype html>
<html lang="fr" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<title>BSS Mentions légales</title>
<link rel="canonical" href="[canonical]" />
<meta name="description" content="[description]" />
<meta name="author" content="[author]" />
<meta property="og:locale" content="fr_FR" />
<meta property="og:type" content="website" />
<meta property="og:title" content="[title]" />
<meta property="og:description" content="[description]" />
<meta property="og:url" content="[URL]" />
<meta property="og:site_name" content="[site_name]" />
<meta property="og:image" content="[image]" />
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:description" content="[description]"/>
<meta name="twitter:title" content="[title]"/>
<meta name="twitter:site" content="[site]"/>
<meta name="twitter:domain" content="[domain]"/>
<meta name="twitter:image:src" content="[image]" />
<!--STYLES-->
<?php include 'styles.php';?>
</head>
<body class="page-simple " id="mentions-legales">

<!-- HEADER -->
<header class="bss-header">
  <?php include 'header.php';?>
  <?php include 'menu.php';?>
</header>
<div class="main animsition"> 
  
  <!-- BREADCRUMB -->
  <div class="bss-breadcrumb" >
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h6>
            <ol class="breadcrumb">
              <li id="a" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="index.php" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
              <li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="gestion-cookies.php" itemprop="url"> <span itemprop="title">Gestion des cookies</span> </a></li>
            </ol>
          </h6>
        </div>
      </div>
    </div>
  </div>
  
  
  <div class="bss-section bloc-section-orange bss-ccm">
  <div class="container">
    <h1 id="tagline" class="h1">Gestion des cookies</h1>
    
    <div class="row">
      <div class="col-md-8">
        <h2>Qu'est ce qu'un cookie ?
        </h2>
        <p>Un cookie est un fichier texte déposé et conservé sur le disque dur de l'internaute, sous réserve de ses choix, par le serveur du site visité ou par un serveur tiers (outil de web analytique , régie publicitaire, partenaires, etc .)
        </p>
        <p>Un cookie permet donc de reconnaître le terminal de l'Utilisateur lorsqu'il revient sur un site web. En effet ce n'est pas l'Utilisateur qui est reconnu mais le terminal depuis lequel il visite un site web.</p>
       <hr> <h2> A quoi servent les cookies émis sur nos Sites ?
        </h2>
        <p>Seul l'émetteur d'un cookie est susceptible de lire ou de modifier des informations qui y sont contenues.
        </p>
        <p>Les cookies que nous émettons sur nos Sites sont utilisés pour reconnaître le terminal de l'Utilisateur lorsqu'il se connecte à l'un de nos Sites afin de :
        </p>
        <ul>
          <li>Optimiser la présentation de nos Sites aux préférences d'affichage de votre terminal (résolution d'affichage, système d'exploitation utilisé, etc.) lors de vos visites selon les matériels et les logiciels de visualisation ou de lecture que comporte votre terminal,
          </li>
          <li>Permettre à l' Utilisateur d'accéder à des espaces réservés et personnels sur nos Sites, tels que son compte personnel sur la base des informations qu'il a transmises au moment de la création de son compte. L' Utilisateur accède par ce bais à des contenus personnalisés ou qui lui sont réservés.            </li>
          <li>Mémoriser des informations relatives à un formulaire que vous avez rempli sur notre Site (accès à votre compte / et à votre abonnement). </li>
          <li>Etablir des statistiques et volumes de fréquentation et d'utilisation de nos Sites,            </li>
          <li>Mettre en place des mesures de sécurité, par exemple lorsqu'il est demandé à l' Utilisateur de se connecter après un certain laps de temps.            </li>
          <li>Adapter les contenus publicitaires de nos Sites aux centres d'intérêt de l' Utilisateur qui résultent des données de sa navigation. </li>
        </ul>
        <h3>Gestion des cookies partenaires :
        </h3>
        <ul>
          <li>Mesure d'audience (Google Analytics) : <a href="https://tools.google.com/dlpage/gaoptout">https://tools.google.com/dlpage/gaoptout </a></li>
        </ul>
        <hr>
        <h2>Quels sont les cookies émis sur nos Sites ?
        </h2>
        <p>Les cookies émis par des tiers sur nos Sites sont de différentes natures. Nous pouvons distinguer notamment :
        </p>
        <ul>
          <li>Les cookies émis par nos partenaires commerciaux tiers qui utilisent des technologies logicielles communément appelées « pixel transparent ». Ces pixels transparents sont des petits graphiques avec un identifiant unique qui ont la même vocation que les cookies, à savoir renseigner le Site sur la navigation de l' Utilisateur à travers des information cryptées contenus dans son terminal. Contrairement aux cookies qui sont stockés sur le disque dur des ordinateurs de l'internaute, les « pixels transparents » sont incrustés de façon invisible sur les pages du Site web et permettent d'informer le Site web des contenus publicitaires les plus appréciés par les internautes. Nous ne lions en aucun cas les informations collectées par les « pixels transparents » à des données personnelles de nos internautes.          </li>
          <li>Du fait d'applications tierces intégrées à nos Sites, certains cookies peuvent être émis par d'autres entités. Ces fonctionnalités permettent le partage de contenus par les utilisateurs de nos Sites , tels que les boutons « Partager » ou « J'amie » de Facebook, ou les boutons « twitter », « Google+ », etc . Le réseau social fournissant une de ces applications est susceptible de vous identifier grâce à ses boutons et à ses propres cookies, même si vous ne les avez pas utilisés lors de votre consultation de nos Sites, du seul fait que vous disposez d'un compte ouvert sur votre terminal auprès du réseau social concerné. Nous n'avons aucun contrôle sur le processus employé par les réseaux sociaux pour collecter ces informations et vous invitons à consulter les politique de confidentialité de ces derniers . </li>
        </ul>
        <h3>Gestion des cookies partenaires :
        </h3>
        <ul>
          <li> Réseau social (Facebook) : <a href="https://www.facebook.com/help/cookies/">https://www.facebook.com/help/cookies/</a> </li>
          <li>Réseau social (Twitter) : <a href="https://support.twitter.com/articles/20170518-utilisation-des-cookies-et-des-technologies-similaires-par-twitter">https://support.twitter.com/articles/20170518-utilisation-des-cookies-et-des-technologies-similaires-par-twitter</a></li>
        </ul><hr>
        <h2>Vos choix concernant les cookies
        </h2>
        <p>Vous disposez de différents moyens pour gérer les cookies . Tout paramétrage que vous pouvez entreprendre sera susceptible de modifier votre navigation sur nos Sites et sur Internet en général et vos conditions d'accès à certains services de nos Sites nécessitant l'utilisation de cookies.
        </p>
        <p>Vous pouvez à tout moment exprimer et modifier vos souhaits en matière de cookies, par les moyens décrits ci-dessous.
        </p>
        <h3>1. Les choix offerts par votre logiciel de navigation
        </h3>
        <p>Vous pouvez configurer votre logiciel de navigation de manière à ce que des cookies soient enregistrés dans votre terminal ou, au contraire, qu'ils soient rejetés, soit systématiquement, soit selon leur émetteur.
        </p>
        <p>Vous pouvez également configurer votre logiciel de navigation de manière à ce que l'acceptation ou le refus des cookies vous soient proposés ponctuellement, avant qu'un cookie soit susceptible d'être enregistré dans votre terminal.          </p>
        <p>Pour plus d'informations, consultez la rubrique « Comment exercer vos choix selon le navigateur que vous utilisez » ci-dessous. </p>
        <h3>2. L'accord sur les cookies
        </h3>
        <p>L'enregistrement d'un cookie dans un terminal est essentiellement subordonné à la volonté de l'utilisateur du terminal, que celui-ci peut exprimer et modifier à tout moment et gratuitement à travers les choix qui lui sont offerts par son logiciel de navigation.          </p>
        <p>Si vous avez accepté dans votre logiciel de navigation l'enregistrement de cookies dans votre terminal, les cookies intégrés dans les pages et contenus que vous avez consultés pourront être stockés temporairement dans un espace dédié de votre terminal. Ils y seront lisibles uniquement par leur émetteur. </p>
        <h3>3. Le refus des cookies
        </h3>
        <p>Si vous refusez l'enregistrement de cookies dans votre terminal, ou si vous supprimez ceux qui y sont enregistrés, vous ne pourrez plus bénéficier d'un certain nombre de fonctionnalités qui sont néanmoins nécessaires pour naviguer dans certains espaces de nos Sites Tel serait le cas si vous tentiez d'accéder à votre compte ou à votre abonnement qui nécessitent de vous identifier. Tel serait également le cas lorsque nous, ou nos prestataires, ne pourrions pas reconnaître, à des fins de compatibilité technique, le type de navigateur utilisé par votre terminal, ses paramètres de langue et d'affichage ou le pays depuis lequel votre terminal semble connecté à Internet.
        </p>
        <p>Le cas échéant, nous déclinons toute responsabilité pour les conséquences liées au fonctionnement dégradé de nos services résultant de l'impossibilité pour nous d'enregistrer ou de consulter les cookies nécessaires à leur fonctionnement et que vous auriez refusés ou supprimés.
        </p>
        <h3>4. Comment exercer vos choix selon le navigateur que vous utilisez</h3>
        <p> Pour la gestion des cookies et de vos choix, la configuration de chaque navigateur est différente. Elle est décrite dans le menu d'aide de votre navigateur, qui vous permettra de savoir de quelle manière modifier vos souhaits en matière de cookies. Voici la procédure à suivre pour que votre navigateur refuse les cookies :</p>
        <ul>
          <li> Si vous utilisez le navigateur Internet Explorer TM
            <ul>
              <li>Dans Internet Explorer, cliquez sur le bouton « Outils », puis sur « Options Internet ».                </li>
              <li> Sous l'onglet Général, sous « Historique de navigation », cliquez sur « Paramètres ».                </li>
              <li> Cliquez sur le bouton « Afficher les fichiers ». </li>
              <li>Cliquez sur l'en-tête de colonne « Nom » pour trier tous les fichiers dans l'ordre alphabétique, puis parcourez la liste jusqu'à ce que vous voyiez des fichiers commençant par le préfixe « Cookie ». (tous les cookies possèdent ce préfixe et contiennent habituellement le nom du site Web qui a créé le cookie).                </li>
              <li> Sélectionnez le ou les cookies concernés et supprimez-les.                </li>
              <li> Fermez la fenêtre qui contient la liste des fichiers, puis cliquez deux fois sur OK pour retourner dans Internet Explorer. </li>
            </ul>
          </li>
        </ul>
        <p><a href="#">En savoir plus </a></p>
        <ul>
          <li>Si vous utilisez le navigateur Firefox TM
            
            o Allez dans l'onglet « Outils » du navigateur puis sélectionnez le menu « Options ».
            <ul>
              <li> Dans la fenêtre qui s'affiche, choisissez « Vie privée » et cliquez sur « Affichez les cookies ».                </li>
              <li> Repérez les fichiers concernés, sélectionnez-les et supprimez-les. </li>
            </ul>
          </li>
        </ul>
        <p><a href="#">En savoir plus</a> </p>
        <ul>
          <li>Si vous utilisez le navigateur Safari TM
            <ul>
              <li> Dans votre navigateur, choisissez le menu « Édition                           > Préférences ».
              </li>
              <li>Cliquez sur « Sécurité ». </li>
              <li>Cliquez sur « Afficher les cookies ».                </li>
              <li>Sélectionnez les cookies concernés et cliquez sur « Effacer » ou sur « Tout effacer ».</li>
              <li> Après avoir supprimé les cookies, cliquez sur « Terminé ». </li>
            </ul>
          </li>
        </ul>
        <p><a href="#">En savoir plus</a></p>
        <ul>
          <li>si vous utilisez le navigateur Google Chrome TM            
            <ul>
              <li>Cliquez sur l'icône du menu « Outils ».                </li>
              <li> Sélectionnez « Options ».                </li>
              <li> Cliquez sur l'onglet « Options avancées » et accédez à la section « Confidentialité ».                </li>
              <li> Cliquez sur le bouton « Afficher les cookies ».                </li>
              <li> Repérez les fichiers concernés, sélectionnez-les et supprimez-les.                </li>
              <li> Cliquez sur « Fermer » pour revenir à votre navigateur. </li>
            </ul>
          </li>
        </ul>
        <p><a href="#">En savoir plus</a></p>
        <ul>
          <li> Les cookies "Flash"© de "Adobe Flash Player"™ "Adobe Flash Player"™ est une application informatique qui permet le développement rapide des contenus dynamiques utilisant le langage informatique "Flash". Flash (et les applications de même type) mémorise les paramètres, les préférences et l'utilisation de ces contenus grâce à une technologie similaire aux cookies. Toutefois, "Adobe Flash Player"™ gère ces informations et vos choix via une interface différente de celle fournie par votre logiciel de navigation. Dans la mesure où votre Terminal serait susceptible de visualiser depuis le Site et les Applications des contenus développés avec le langage Flash, nous vous invitons à accéder à vos outils de gestion des cookies Flash, directement depuis le site <a href="http://www.adobe.com/fr/">http://www.adobe.com/fr/</a>. </li>
        </ul>
        <p>Pour connaître les options offertes par tout autre logiciel de navigation et les modalités de suppression de fichiers cookies stockés dans votre terminal, selon le(s) navigateur(s) installés dans votre terminal, nous vous invitons à consulter le menu d'aide de votre navigateur ainsi que la rubrique « Vos traces » du site de la CNIL (Commission Nationale de l'Informatique & des Libertés
        </p><hr>
        <h2>A quoi servent les cookies accompagnant les contenus publicitaires diffusés par des tiers
        </h2>
        <p>Lorsque vous accédez à un site ou une application contenant des espaces publicitaires ou promotionnelle diffusant une de nos opérations publicitaires ou promotionnelle, cette annonce est susceptible de contenir un cookie.
        </p>
        <p>Ce cookie est susceptible, sous réserve de vos choix, d'être enregistré dans votre Terminal et de nous permettre de reconnaître le navigateur de votre Terminal pendant la durée de validité du cookie concerné. 
        </p>
        <p>Les Cookies intégrés à nos annonces publicitaires ou promotionnelles diffusées par des tiers sont utilisés aux fins décrites ci-dessous, sous réserve de vos choix, qui résultent des paramètres de votre logiciel de navigation utilisé lors de votre visite de nos Sites que vous pouvez exprimer à tout moment.
        </p>
        <p>Les Cookies que nous sommes susceptibles d'émettre nous permettent, si votre Terminal a permis leur enregistrement, selon vos choix : 
        </p>
        <ul>
          <li>de comptabiliser le nombre d'affichages et d'activations de nos contenus publicitaires diffusés sur des sites ou des applications de tiers, d'identifier ces contenus et ces sites/applications, de déterminer le nombre d'utilisateurs ayant cliqué sur chaque contenu,</li>
          <li> de calculer les sommes dues aux acteurs de la chaîne de diffusion publicitaire (agence de communication, régie publicitaire, site/support de diffusion) et d'établir des statistiques,            </li>
          <li> d'adapter la présentation du Site auquel mène un de nos contenus publicitaires ou promotionnelle,            </li>
          <li> selon les préférences d'affichage de votre Terminal (langue utilisée, résolution d'affichage, système d'exploitation utilisé, etc) lors de vos visites sur nos Sites et selon les matériels et les logiciels de visualisation ou de lecture que votre Terminal comporte,            </li>
          <li> selon les données de localisation (longitude et latitude) qui nous sont transmises (ou à nos prestataires) par votre Terminal avec votre accord préalable,            </li>
          <li> de suivre la navigation ultérieure effectuée par votre Terminal sur des sites ou des applications ou sur d'autres contenus publicitaires. </li>
        </ul><hr>
        <h2>Quel est l'intérêt de voir s'afficher des publicités adaptées à votre navigation ?
        </h2>
        <p>Notre objectif est de vous présenter des offres les plus pertinentes possibles. A cette fin, la technologie des cookies permet de déterminer en temps réel quelle offre afficher à un Terminal, en fonction de sa navigation récente au sein de nos Sites. Vous préférez sans doute voir s'afficher des offres de produits et de services qui correspondent à ce qui vous intéresse plutôt que des offres qui n'ont aucun intérêt pour vous.
        </p><hr>
        <h2>Si vous partagez l'utilisation de votre terminal avec d'autres personnes</h2>
        <p> Si votre Terminal est utilisé par plusieurs personnes et lorsqu'un même Terminal dispose de plusieurs logiciels de navigation, nous ne pouvons pas nous assurer de manière certaine que les services et publicités destinés à votre Terminal correspondent bien à votre propre utilisation de ce Terminal et non à celle d'un autre utilisateur de ce Terminal. Le cas échéant, le partage avec d'autres personnes de l'utilisation de votre Terminal et la configuration des paramètres de votre navigateur à l'égard des cookies, relèvent de votre libre choix et de votre responsabilité.
        </p><hr>
        <h2>Données personnelles et informations de navigation
        </h2>
        <p>Nous sommes susceptibles d'adapter nos offres qui vous sont destinées à des informations relatives à la navigation de votre Terminal sur nos Sites ou au sein de sites ou de services édités par des tiers et sur lesquels nous émettons des cookies. 
          Dans la mesure où vous nous avez fourni des données personnelles vous concernant, notamment vos coordonnées électroniques, lors de votre inscription ou de votre accès à l'un de nos services, nous sommes susceptibles, sous réserve de vos choix, d'associer des informations de navigation relatives à votre Terminal, traitées par les cookies que nous émettons, avec vos données personnelles afin de vous adresser, par exemple, des prospections électroniques ou d'afficher sur votre Terminal, des offres personnalisées qui vous sont plus spécifiquement destinées et susceptibles de plus vous intéresser.
        </p>
        <p>Vous pourrez à tout moment nous demander de ne plus recevoir de publicités ou de prospections adaptées aux informations de navigation de votre Terminal, en nous contactant directement et gratuitement, ou au moyen du lien de désinscription inclus dans toute prospection que nous serions susceptible de vous adresser par courrier électronique. Le cas échéant les publicités que vous pourriez continuer à recevoir, sauf opposition de votre part exercée auprès de nous, ne seront plus adaptées à la navigation de votre Terminal.          </p>
        <p>Par ailleurs, dans l'hypothèse où nous envisagerions d'obtenir auprès d'un tiers (prestataire de publicité ciblée, régie publicitaire, etc.), des informations de navigation de votre Terminal que nous pourrions associer aux données personnelles que vous nous avez fournies, nous solliciterions préalablement votre accord explicite avant de procéder à une telle association et de vous adresser des publicités ou des prospections qui en résulteraient. </p>
      </div>
      <div class="col-md-4 hidden-xs hidden-sm">
        <div class=""> 
          <!-- 16:9 aspect ratio -->
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/5LaIfS7P60s?rel=0"></iframe>
          </div>
        </div>
        <hr>
        <div class="bloc-annonce"><a href="#"><img src="http://www.bordas-soutien-scolaire.com/images/bss-wengo-300.gif" class="img-responsive"></a></div>
      </div>
      <div class="col-md-12">
        <hr>
        <p class="text-center"><a class="btn btn-md btn-primary" href="#">Je découvre toutes les formules d'abonnement<i class="icon-angle-right"></i></a></p>
      </div>
    </div>
  </div>
</div>
  
  
  
  
 
<!--FOOTER-->
<?php include 'footer.php';?>
<!--SCRIPTS-->
<?php include 'scripts.php';?>
</body>
</html>
