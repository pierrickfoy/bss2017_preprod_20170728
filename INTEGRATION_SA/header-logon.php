<?php include 'sitemap.php';?>
<div id="totop"></div>
<!-- BLOC UTILISATEUR SUPLEMENTAIRE SI IDENTIFIE-->
<div class="container-fluid top-member">
<div class="container top-member">
  <div class="row">
    <div class="col-md-12 text-right">
      <ul class="list-inline">
        <li><a href="gestion-profil.php">Mon profil</a></li>
        <li>|</li>
        <li><a href="gestion-abonnement.php">Mon abonnement</a></li>
      </ul>
    </div>
  </div>
</div>  
</div>
<!--HEADER FIXE SUR DESKTOP ET ECRAN LARGE - HEADER ABSOLUE SUR MOBILE ET PHABLETTE -->
<div class="container top-header fixed-top">
  <div class="row">
    <div class="col-xs-12 col-sm-4 text-center hidden-xs"> <a href="index.php" class="bss-logo"> <img src="img/logo.png" class="img-responsive"></a> </div>
    <div class="col-xs-12 col-sm-4 no-padding text-center hidden-xs"><a href="index.php" class="bss-slogan ">La réussite à portée de clic !</a></div>
    
    <div class="col-xs-12 col-sm-4 no-padding main-bloc-bss-connexion text-right">
      <div class="logo-mobile visible-xs-block"><a href="index.php" class="bss-logo"> <img src="img/logo.png" class="img-responsive"></a></div>
      <!--<div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      </div>
      <div class="bloc-bss-connexion"> <a class="btn btn-user logout" href="index" ><i class="icon-user"></i><i class="mini-icon icon-cancel"></i></a>
        <div class="account-name hidden-xs">Bonjour, <span>Hélène Gonzalez-Quijano</span></div>
      </div>-->
   
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      </div>  
      <div class="bloc-bss-connexion">  <div class="account-name hidden-xs">Hélène <span>Gonzalez-Quijano</span></div><a class="btn btn-user login collapsed"   href="index"><i class="icon-user"></i><!--<i class="mini-icon icon-cancel"></i>--></a> <a class="btn btn-connexion login  hidden-xs hidden-sm hidden-md collapsed" href="index" >Déconnexion</a> </div>
    </div>
  </div>
</div>
