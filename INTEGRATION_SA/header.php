<?php include 'sitemap.php';?>
<div id="totop"></div>
<!--HEADER FIXE SUR DESKTOP ET ECRAN LARGE - HEADER ABSOLUE SUR MOBILE ET PHABLETTE -->
<div class="container top-header fixed-top">
  <div class="row">
    <div class="col-xs-12  col-sm-4 text-center hidden-xs"> <a href="index.php" class="bss-logo"> <img src="img/logo.png" class="img-responsive"></a> </div>
    <div class="col-xs-12  col-sm-4 no-padding text-center hidden-xs"><a href="index.php" class="bss-slogan ">La réussite à portée de clic !</a></div>
    <div class="col-xs-12 col-sm-4 no-padding main-bloc-bss-connexion text-right">
      <div class="logo-mobile visible-xs-block"><a href="index.php" class="bss-logo"> <img src="img/logo.png" class="img-responsive"></a></div>
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      </div>
      <div class="bloc-bss-connexion"> <a class="btn btn-user login collapsed" role="button" data-toggle="collapse" href="#collapseConnexion" aria-expanded="false" aria-controls="collapseConnexion"><i class="icon-user"></i><!--<i class="mini-icon icon-ok"></i>--></a> <a class="btn btn-connexion login  hidden-xs collapsed" role="button" data-toggle="collapse" href="#collapseConnexion" aria-expanded="false" aria-controls="collapseConnexion"><i class="icon-key"></i> Connexion</a> </div>
    </div>
  </div>
</div>
<!--COLLAPSE CONTENANT LES ELEMENTS DE CONNEXION-->
<div class="collapse bss-connexion" id="collapseConnexion">
  <div class="container">
    <div class="row">
      <div class="col-md-5 col-md-offset-1">
        <div class="bss-connexion">
          <form action="index-logon.php" id="Connexion" title="Connexion">
            <p class="form-titre">Je gère mon compte</p>
            <p class="form-stitre">(abonnement(s), données personnelles)</p>
            <div class="row">
              <div class="form-group col-sm-6">
                <label for="connexion-identifiant">Identifiant</label>
                <input name="connexion-identifiant" type="text" class="form-control" id="connexion-identifiant" form="Connexion" placeholder="Identifiant" title="Identifiant">
              </div>
              <div class="form-group col-sm-6">
                <label for="connexion-mdp">Mot de passe</label>
                <input name="mot-de-passe" type="password" class="form-control" id="connexion-mdp" form="Connexion" placeholder="Mot de passe" title="Mot de passe">
              </div>
            </div>
            <p class="form-texte">
              <button type="submit" class="btn btn-primary btn-fw">Je me connecte à mon compte<i class="icon-angle-right"></i></button>
            </p>
            <p class="form-texte"><a href="mot-de-passe-oublie"  class="">Mot de passe oublié ?</a></p>
          </form>
        </div>
      </div>
      <div class="col-md-5 ">
        <div class="bss-plateforme">
          <p class="form-titre">J'accède à la plateforme <br>
            de Soutien scolaire</p>
          <p class="form-texte"><a href="#"   class="btn btn-primary btn-fw">Je me connecte à la plateforme<i class="icon-angle-right"></i></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
