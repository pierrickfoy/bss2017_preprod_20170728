$(document).ready(function() {
    //POPOVER CARTE BLEU ET TOOLTIPS AU CAS OU
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
    $(function() {
        $('[data-toggle="popover"]').popover()
    })
    $(document).ready(function() {
        var image = '<img src="img/verso-carte.jpg" class="img-responsive"">';
        $('#popover').popover({
            placement: 'top',
            content: image,
            html: true
        });
    });
    //SCROLL SMOOTH
    $(function() {
        $('.ancre').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
    //SmoothScroll du DROPDOWN de la page "Les matières disponibles"
    $(function() {
        $('#accordion').on('shown.bs.collapse', function(e) {
            var offset = $(this).find('.collapse.in').prev('.item-matiere');
            if (offset) {
                $('html,body').animate({
                    scrollTop: $(offset).offset().top - 150
                }, 500);
            }
        });
    });
    //Comportement du menu sur les ecran inferieur à 768 px - Script a ameliorer si possible.
    function bindNavbar() {
        if ($(window).width() > 768) {
            $('.navbar-default .dropdown').on('mouseover', function() {
                $('.dropdown-toggle', this).next('.dropdown-menu').show();
            }).on('mouseout', function() {
                $('.dropdown-toggle', this).next('.dropdown-menu').hide();
            });

            $('.dropdown-toggle').click(function() {
                if ($(this).next('.dropdown-menu').is(':visible')) {
                    window.location = $(this).attr('href');
                }
            });
        }
        if (/Android|iPhone|iPad|iPod/i.test(navigator.userAgent)) {
            $('.navbar-default .dropdown').off('mouseover').off('mouseout');
        } else {
            $('.navbar-default .dropdown').off('mouseover').off('mouseout');
        }

    }

    $(window).resize(function() {
        bindNavbar();
    });

    bindNavbar();

    /*PROPAGATION MENU bssmenu*/
    $(function() {
        window.prettyPrint && prettyPrint()
        $(document).on('click', '.bssmenu .dropdown-menu', function(e) {
            e.stopPropagation()
        })
    })

    //CAROUSEL TEMOIN FULL WIDTH
    var owl = $("#carousel-temoins");
    owl.owlCarousel({
        autoPlay: 5000,
        stopOnHover: true,
        items: 1, //10 items above 1000px browser width
        itemsDesktop: [1000, 1], //5 items between 1000px and 901px
        itemsDesktopSmall: [900, 1], // betweem 900px and 601px
        itemsTablet: [600, 1], //2 items between 600 and 0
        itemsMobile: [400, 1], // itemsMobile disabled - inherit from itemsTablet option
        navigation: true,
        navigationText: [
            "<i class='icon-angle-left'></i>",
            "<i class='icon-angle-right icon-white'></i>"
        ],
    });

});