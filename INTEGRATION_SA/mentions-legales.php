<!doctype html>
<html lang="fr" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<title>BSS Mentions légales</title>
<link rel="canonical" href="[canonical]" />
<meta name="description" content="[description]" />
<meta name="author" content="[author]" />
<meta property="og:locale" content="fr_FR" />
<meta property="og:type" content="website" />
<meta property="og:title" content="[title]" />
<meta property="og:description" content="[description]" />
<meta property="og:url" content="[URL]" />
<meta property="og:site_name" content="[site_name]" />
<meta property="og:image" content="[image]" />
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:description" content="[description]"/>
<meta name="twitter:title" content="[title]"/>
<meta name="twitter:site" content="[site]"/>
<meta name="twitter:domain" content="[domain]"/>
<meta name="twitter:image:src" content="[image]" />
<!--STYLES-->
<?php include 'styles.php';?>
</head>
<body class="page-simple " id="mentions-legales">

<!-- HEADER -->
<header class="bss-header">
  <?php include 'header.php';?>
  <?php include 'menu.php';?>
</header>
<div class="main animsition"> 
  
  <!-- BREADCRUMB -->
  <div class="bss-breadcrumb" >
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h6>
            <ol class="breadcrumb">
              <li id="a" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="index.php" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
              <li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="mentions-legales.php" itemprop="url"> <span itemprop="title">Mentions légales</span> </a></li>
            </ol>
          </h6>
        </div>
      </div>
    </div>
  </div>
  
  
  <div class="bss-section bloc-section-orange bss-ccm">
  <div class="container">
    <h1 id="tagline" class="h1">Mentions légales</h1>
    
    <div class="row">
      <div class="col-md-8">
       <h2 id="subtitle" class="h2">Éditeur du site</h2> 
<p><strong>Éditions BORDAS</strong><br>
  31 avenue Pierre de Coubertin<br>
  75013 PARIS</p>
<p>Une maison d'édition SEJER, SEJER Société Anonyme au capital de 9 898 330 euros<br>
  Dont le siège social est 30 place d'Italie 75702 Paris cedex 13 - RCS Paris B 393 291 042<br>
  TVA intracommunautaire de SEJER : FR 58 393 291 042<br>
  Direction de la publication : Catherine LUCET</p><hr>
<h2>Déclaration CNIL</h2>
<p>Le site <a href="../">www.bordas-soutien-scolaire.com</a> &nbsp;est déclaré auprès de la Commission Nationale de l'Informatique et des Libertés sous le n° 167 20 00.</p>
<p>Nous vous rappelons que vous disposez d'un droit d'accès, de modification, de rectification et de suppression des données qui vous concernent (art. 34 de la loi " Informatique et Libertés " du 6 janvier 1978 modifiée en 2004). Pour exercer ce droit, adressez-vous à :</p>
<p><strong>Éditions BORDAS</strong><br>
  31 avenue Pierre de Coubertin<br>
  75013 PARIS</p>
<p>Tél. : 01 72 36 40 00<br> Fax : 01 72 36 40 10<br> Mail : <a href="mailto:webmestre@bordas.tm.fr">webmestre@bordas.tm.fr</a></p>
<p>Si vous les avez acceptées, vous serez susceptibles de recevoir des offres des Éditions BORDAS et de ses partenaires.</p><hr>
<h2>Responsabilité</h2>
<p>Les Editions BORDAS n’ont qu’une obligation de moyens pour toutes les étapes et fonctionnalités du site. La responsabilité des Éditions BORDAS ne saurait être engagée pour tous les inconvénients ou dommages inhérents à l'utilisation du réseau Internet, notamment une rupture du service, une intrusion extérieure ou la présence de virus informatiques, ou de tout fait qualifié de force majeure, conformément à la jurisprudence.</p><hr>
<h2>Propriété intellectuelle</h2>
<p>Tous les éléments du site <a href="../">www.bordas-soutien-scolaire.com</a>, y compris les documents téléchargeables et consultables en ligne, sont protégés par le droit d'auteur, des marques ou des brevets. Ils sont la propriété exclusive des Éditions BORDAS. En conséquence, vous ne pouvez en aucun cas reproduire, représenter, diffuser, modifier ou concéder tout ou partie de l'un quelconque des éléments du site sous réserve des conditions générales d’utilisation applicable et sans l'accord préalable et express des Éditions BORDAS.</p><hr>
<h2>Création du site</h2>
<ul>
  <li>Charte graphique: <a href="http://www.editions-bordas.fr/" target="_blank">Les Éditions Bordas</a></li>
  <li>Graphisme et webdesign: <a href="http://www.studio-ancalime.com" target="_blank">François Cerret</a></li>
  <li>Développement: <a href="http://www.ecomiz.com/" target="_blank">Écomiz</a></li>
</ul>
      </div>
      <div class="col-md-4 hidden-xs hidden-sm">
        <div class=""> 
          <!-- 16:9 aspect ratio -->
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CItcTboz6_k?rel=0"></iframe>
          </div>
        </div>
        <hr>
        <div class="bloc-annonce"><div class="prepend"><b class="caret"></b> publicité <b class="caret"></b></div><a href="#"><img src="http://www.bordas-soutien-scolaire.com/images/bss-wengo-300.gif" class="img-responsive"></a></div>
      </div>
      <div class="col-md-12">
        <hr>
        <p class="text-center"><a class="btn btn-md btn-primary" href="#">Je découvre toutes les formules d'abonnement<i class="icon-angle-right"></i></a></p>
      </div>
    </div>
  </div>
</div>
  
  
  
  
 
<!--FOOTER-->
<?php include 'footer.php';?>
<!--SCRIPTS-->
<?php include 'scripts.php';?>
</body>
</html>
