<!-- MENU PRINCIPAL -->

<div class="navbar bssmenu navbar-default ">
  <div class="container">
    <div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="index.php">Accueil</a></li>
        <li class="dropdown bssmenu-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Classe<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li> 
              <!-- Classe CP, CE1, CE2, etc -->
              <div class="bssmenu-content">
                <div class="row">
                  <div class="col-sm-4 col-md-2  clearfix">
                    <p class="categorie-classe">Primaire </p>
                    <ul class="liste-classe list-unstyled">
                      <li class="item-classe-col"><a href="page-classe.php" class="item-menu">CP</a></li>
                      <li class="item-classe-col"><a href="page-classe.php" class="item-menu">CE1</a></li>
                      <li class="item-classe-col"><a href="page-classe.php" class="item-menu">CE2</a></li>
                      <li class="item-classe-col"><a href="page-classe.php" class="item-menu">CM1</a></li>
                      <li class="item-classe-col"><a href="page-classe.php" class="item-menu">CM2</a></li>
                    </ul>
                  </div>
                  <div class="col-sm-4 col-md-2 clearfix">
                    <p class="categorie-classe">Collège </p>
                    <ul class="liste-classe list-unstyled">
                      <li class="item-classe-1col"><a href="page-classe.php" class="item-menu">6e</a></li>
                      <li class="item-classe-1col"><a href="page-classe.php" class="item-menu">5e</a></li>
                      <li class="item-classe-1col"><a href="page-classe.php" class="item-menu">4e</a></li>
                      <li class="item-classe-1col"><a href="page-classe.php" class="item-menu">3e</a></li>
                    </ul>
                  </div>
                  <div class="col-sm-4 col-md-2 clearfix">
                    <p class="categorie-classe">Lycée général</p>
                    <ul class="liste-classe list-unstyled">
                      <li class="item-classe-1col"><a href="page-classe.php" class="item-menu">2de</a></li>
                      <li class="item-classe-2col"><a href="page-classe.php" class="item-menu">1re L</a></li>
                      <li class="item-classe-2col"><a href="page-classe.php" class="item-menu">1re ES</a></li>
                      <li class="item-classe-2col"><a href="page-classe.php" class="item-menu">1re S</a></li>
                      <li class="item-classe-2col"><a href="page-classe.php" class="item-menu">Tle L</a></li>
                      <li class="item-classe-2col"><a href="page-classe.php" class="item-menu">Tle ES</a></li>
                      <li class="item-classe-2col"><a href="page-classe.php" class="item-menu">Tle S</a></li>
                    </ul>
                  </div>
                  <div class=" col-sm-8 col-md-4 clearfix">
                    <p class="categorie-classe">Lycée technique</p>
                    <div class=" col-sm-6 col-md-6 no-padding clearfix">
                      <ul class="liste-classe list-unstyled">
                        <li class="item-classe-1col"><a href="page-classe.php" class="item-menu">2de</a></li>
                        <li class="item-classe-2col"><a href="page-classe.php" class="item-menu">1re STD2A</a></li>
                        <li class="item-classe-2col"><a href="page-classe.php" class="item-menu">1re ST2S</a></li>
                        <li class="item-classe-2col"><a href="page-classe.php" class="item-menu">1re STI2D</a></li>
                        <li class="item-classe-2col"><a href="page-classe.php" class="item-menu">1re STMG</a></li>
                        <li class="item-classe-2col"><a href="page-classe.php" class="item-menu">1re STL</a></li>
                      </ul>
                    </div>
                    <div class=" col-sm-6 col-md-6 no-padding clearfix">
                      <ul class="liste-classe list-unstyled">
						<li class="item-classe-2col"><a href="page-classe.php" class="item-menu">Tle STD2A</a></li>
                        <li class="item-classe-1col"><a href="page-classe.php" class="item-menu">Tle ST2S</a></li>
                        <li class="item-classe-2col"><a href="page-classe.php" class="item-menu">Tle STI2D</a></li>
                        <li class="item-classe-2col"><a href="page-classe.php" class="item-menu">Tle STMG</a></li>
                        <li class="item-classe-2col"><a href="page-classe.php" class="item-menu">Tle STL</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class=" col-sm-4 col-md-2 clearfix">
                    <p class="categorie-classe">Lycée professionnel</p>
                    <ul class="liste-classe list-unstyled">
                      <li class="item-classe-1col"><a href="page-classe.php" class="item-menu">2de BAC Pro</a></li>
                      <li class="item-classe-1col"><a href="page-classe.php" class="item-menu">1re BAC Pro</a></li>
                      <li class="item-classe-1col"><a href="page-classe.php" class="item-menu">Tle BAC Pro</a></li>
                    </ul>
                  </div>
                </div>
                <div class="row"> </div>
              </div>
            </li>
          </ul>
        </li>
        <li class="dropdown bssmenu-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Matière<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li> 
              <!-- Matière Français, Maths, etc -->
              <div class="bssmenu-content">
                <div class="row">
                  <div class="col-sm-15 clearfix">
                    <ul class="liste-classe list-unstyled">
                      <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Français</a></li>
					  <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Mathématiques</a></li>
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Anglais</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Hist/Géo</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">SVT</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Phys/Chim</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">SES</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Sciences</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Philo</a></li> -->
                    </ul>
                  </div>
                  <div class="col-sm-15 clearfix">
                    <ul class="liste-classe list-unstyled">
                      <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Histoire-Géographie</a></li>
                      <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Physique-Chimie</a></li>
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Anglais</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Hist/Géo</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">SVT</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Phys/Chim</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">SES</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Sciences</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Philo</a></li> -->
                    </ul>
                  </div>
                  <div class=" col-sm-15 clearfix">
                    <ul class="liste-classe list-unstyled">
                      <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">SVT</a></li>
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Maths</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Anglais</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Hist/Géo</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">SVT</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Phys/Chim</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">SES</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Sciences</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Philo</a></li> -->
                    </ul>
                  </div>
                  <div class=" col-sm-15 clearfix">
                    <ul class="liste-classe list-unstyled">
                      <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Sciences</a></li>
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Maths</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Anglais</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Hist/Géo</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">SVT</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Phys/Chim</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">SES</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Sciences</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Philo</a></li> -->
                    </ul>
                  </div>
                  <div class="col-sm-15 clearfix">
                    <ul class="liste-classe list-unstyled">
                      <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">SES</a></li>
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Maths</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Anglais</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Hist/Géo</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">SVT</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Phys/Chim</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">SES</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Sciences</a></li> -->
                      <!-- <li class="item-matiere-1col"><a href="page-matiere.php" class="item-menu">Philo</a></li> -->
                    </ul>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </li>
        <li> <a href="qui-sommes-nous.php">Qui sommes-nous ?</a> </li>
        <li> <a href="comment-ca-marche.php">Comment ça marche ?</a> </li>
        <li> <a href="comment-sabonner.php">Comment s'abonner ?</a> </li>
      </ul>
    </div>
  </div>
</div>
