<!-- TOUT LES MODALES DU SITE SE TROUVE ICI -->

<!-- Modal Donnez son avis-->
<div class="modal fade" id="avisModal" tabindex="-1" role="dialog" aria-labelledby="avisModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="cbModalLabel">Donnez votre avis</h4>
      </div>
      <div class="modal-body">
        <form action="tunnel-4.php"  class="form-horizontal clearfix" id="form-donner-avis" title="Donnez votre avis">
            <div class="form-group">
              <label for="prenom-avis" class="col-sm-12 control-label">Votre prénom</label>
              <div class="col-sm-12">
                <input name="prenom-avis" type="text" class="form-control" id="prenom-avis" form="form-donner-avis" placeholder="Votre prénom" title="Votre prénom" value="">
              </div>
            </div>
            
            
            <div class="form-group">
              <label for="nom-avis" class="col-sm-12 control-label">1ère lettre de votre nom</label>
              <div class="col-sm-12">
                <input name="nom-avis" type="text" class="form-control" id="nom-avis" form="form-donner-avis" placeholder="1ère lettre de votre nom" title="1ère lettre de votre nom" value="">
              </div>
            </div>
            
            
            
            <div class="form-group  ">
              <label for="motif" class="col-sm-12 control-label">Votre avis</label>
              <div class="col-sm-12">
                <textarea rows="5" class="form-control" form="form-resiliation"></textarea>
              </div> 
           </div>
            
            

<div class="form-group">
              
              <div class="col-sm-12 text-center">
                <button type="submit" class="btn btn-primary ">Poster votre avis<i class="icon-ok "></i></button>
              </div>
            </div>
          
            
        </form>
      </div>
     
    </div>
  </div>
</div>








<!-- Modal Modif coordonnee bancaire -->
<div class="modal fade" id="cbModal" tabindex="-1" role="dialog" aria-labelledby="cbModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="cbModalLabel">Modifier ma CB</h4>
      </div>
      <div class="modal-body">
        <form action="tunnel-4.php"  class="form-horizontal clearfix" id="form-modif-cb" title="Creation compte">
            <div class="form-group">
              <label for="nom-carte-client" class="col-sm-6 control-label">Nom du porteur de la carte</label>
              <div class="col-sm-6">
                <input name="nom-carte-client" type="text" class="form-control" id="nom-carte-client" form="form-modif-cb" placeholder="Nom du porteur de la carte" title="Nom du porteur de la carte" value="Gonzalez-Quijano">
              </div>
            </div>
            <div class="form-group ">
              <label for="type-carte" class="col-sm-6 control-label">Type de carte</label>
              <div class="col-sm-6">
                <label class="radio-inline">
                  <input name="type-carte" type="radio" id="Visa" form="form-modif-cb" title="Carte Visa" value="Visa" checked="checked">
                  <span class="icone-visa">Visa</span> </label>
                <label class="radio-inline">
                  <input name="type-carte" type="radio" id="Mastercard" form="form-modif-cb" title="Mastercard" value="Mastercard">
                <span class="icone-mastercard">Mastercard</span> </label>
                <label class="radio-inline">
                  <input name="type-carte" type="radio" id="CB" form="form-modif-cb" title="Carte Bleue" value="CB">
                <span class="icone-cb">CB</span> </label>
              </div>
            </div>
            <div class="form-group  has-feedback" >
              <label for="numero-carte" class="col-sm-6 control-label">Numéro de carte</label>
              <div class="col-sm-6">
                <input name="numero-carte" type="text" class="form-control" id="numero-carte" form="form-modif-cb" placeholder="Numéro de carte" title="Numéro de carte" value="12345******6789">
                <i class="icon-shield form-control-feedback"></i> <span id="inputSuccess2Status" class="sr-only">(sécurisé)</span> </div>
            </div>
            <div class="form-group">
              <label for="date-expiration-carte" class="col-sm-6 control-label">Date d'expiration</label>
              <div class="col-sm-3">
                <select name="mois-expiration-carte" class="form-control" id="date-expiration-carte" form="form-modif-cb" title="mois-expiration-carte">
                  <option>Mois</option>
                  <option>Janvier</option>
                  <option selected="selected">Février</option>
                  <option>....</option>
                </select>
              </div>
              <div class="col-sm-3">
                <select name="annee-expiration-carte" class="form-control" id="annee-expiration-carte" form="form-modif-cb" title="annee-expiration-carte">
                  <option>Année</option>
                  <option>2015</option>
                  <option selected="selected">2016</option>
                  <option>....</option>
                </select>
              </div>
            </div>
            <div class="form-group  has-feedback" >
              <label for="numero-controle" class="col-sm-6 control-label">Numéro de carte</label>
              <div class="col-sm-3">
                <input name="numero-controle" type="text" class="form-control" id="numero-controle" form="form-modif-cb" placeholder="CVV" title="CVV"> 
                <i class="icon-shield form-control-feedback"></i> <span id="numero-controle-statut" class="sr-only">(sécurisé)</span> </div>
                <div class="col-sm-3">
                 <a id="popover" class="btn btn-primary btn-icon" tabindex="0"  role="button"  data-trigger="focus" rel="popover" data-content="" title="Emplacement du CVV">?</a> 
              </div> 
           </div>

<div class="form-group">
              <div class="col-sm-6"> &nbsp;</div>
              <div class="col-sm-6">
                <button type="submit" class="btn btn-primary btn-fw">Mettre à jour ma CB <i class="icon-ok "></i></button>
              </div>
            </div>
            <hr>
            <div class="well">
            <div class="form-group">
              <div class="col-sm-12">   <a href="#" class="btn btn-danger btn-fw"><i class="icon-cancel"></i> Supprimer ma CB</a> </div>
             
            </div>
              
              <p>En supprimant ma CB, je mets fin à mon abonnement Bordas Soutien scolaire à la fin de ma période d'engagement.</p>
            </div>
        </form>
      </div>
     
    </div>
  </div>
</div>

<!-- Modal Modif resiliation abonnement -->
<div class="modal fade" id="raModal" tabindex="-1" role="dialog" aria-labelledby="raModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="raModalLabel">Résilier mon abonnement</h4>
      </div>
      <div class="modal-body">
        <form action="tunnel-4.php"  class="form-horizontal clearfix" id="form-resiliation" title="Creation compte">
            
            
            <div class="well">
          
              
              <p>Vous pouvez demander à résilier votre abonnement. Cette résiliation sera effective à la fin de votre période d'engagement en cours. Pour ne pas reconduire votre abonnement d'X mois supplémentaire, vous devez le résilier avant le XX/XX/XXXX. Ces dispositions sont précisées dans les <a href="cgv.php">conditions générales de vente</a></p>
            </div>
            
            <div class="form-group  " >
              <label for="motif" class="col-sm-12 control-label">Motif de la résiliation</label>
              <div class="col-sm-12">
                <textarea rows="3" class="form-control" form="form-resiliation"></textarea>
              </div> 
           </div>

<div class="form-group">
              <div class="col-sm-6"> &nbsp;</div>
              <div class="col-sm-6">
                <button type="submit" class="btn btn-primary btn-fw">Je résilie mon abonnement <i class="icon-ok "></i></button>
              </div>
            </div>
                    </form>
      </div>
   
    </div>
  </div>
</div>

