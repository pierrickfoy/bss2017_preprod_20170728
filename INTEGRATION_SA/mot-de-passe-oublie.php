<!doctype html>
<html lang="fr" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<title>BSS Vous avez oublié votre mot de passe ?</title>
<link rel="canonical" href="[canonical]" />
<meta name="description" content="[description]" />
<meta name="author" content="[author]" />
<meta property="og:locale" content="fr_FR" />
<meta property="og:type" content="website" />
<meta property="og:title" content="[title]" />
<meta property="og:description" content="[description]" />
<meta property="og:url" content="[URL]" />
<meta property="og:site_name" content="[site_name]" />
<meta property="og:image" content="[image]" />
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:description" content="[description]"/>
<meta name="twitter:title" content="[title]"/>
<meta name="twitter:site" content="[site]"/>
<meta name="twitter:domain" content="[domain]"/>
<meta name="twitter:image:src" content="[image]" />
<!--STYLES-->
<?php include 'styles.php';?>
</head>
<body class="page-simple " id="authentification">

<!-- HEADER -->
<header class="bss-header">
  <?php include 'header.php';?>
  <?php include 'menu.php';?>
</header>
<div class="main animsition"> 
  
  <!-- BREADCRUMB -->
  <div class="bss-breadcrumb" >
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h6>
            <ol class="breadcrumb">
              <li id="a" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="index.php" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
              <li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="mot-de-passe-oublie.php" itemprop="url"> <span itemprop="title">Vous avez oublié votre mot de passe ?</span> </a></li>
            </ol>
          </h6>
        </div>
      </div>
    </div>
  </div>
  
  
  <div class="bss-section bloc-section-orange bss-ccm">
  <div class="container">
    <h1 id="tagline" class="h1">Vous avez oublié votre mot de passe ?</h1>
    <div class="row">
   
    <div class="col-md-8 col-md-offset-2 ">
    <div class="bss-connexion bloc-bss-1">
          <form action="tunnel-2-adresse.php"  class="form-horizontal clearfix" id="mot-de-passe-oublie" title="mot-de-passe-oublie"> 
            <h2 class="text-center">Entrez l'adresse e-mail associé à votre inscription</h2>
            <div class="form-group">
              <label for="identifiant" class="sr-only">Entrez l'adressse e-mail associé à votre inscription :</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" id="email" form="mot-de-passe-oublie" placeholder="Mon adresse email">
              </div>
             
            </div>
            <div class="form-group">
              
              <div class="col-sm-12">
                <button type="submit" class="btn btn-primary btn-fw">Valider<i class="icon-angle-right"></i></button>
              </div>
            </div>
          </form>
        </div>
     </div>
     </div>
  </div>
</div>
  
  
  
  
 
<!--FOOTER-->
<?php include 'footer.php';?>
<!--SCRIPTS-->
<?php include 'scripts.php';?>
</body>
</html>
