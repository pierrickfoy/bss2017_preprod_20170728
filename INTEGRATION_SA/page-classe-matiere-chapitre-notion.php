<!doctype html>
<html lang="fr" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<title>BSS Page Classe Matiere Chapitre Notion</title>
<link rel="canonical" href="[canonical]" />
<meta name="description" content="[description]" />
<meta name="author" content="[author]" />
<meta property="og:locale" content="fr_FR" />
<meta property="og:type" content="website" />
<meta property="og:title" content="[title]" />
<meta property="og:description" content="[description]" />
<meta property="og:url" content="[URL]" />
<meta property="og:site_name" content="[site_name]" />
<meta property="og:image" content="[image]" />
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:description" content="[description]"/>
<meta name="twitter:title" content="[title]"/>
<meta name="twitter:site" content="[site]"/>
<meta name="twitter:domain" content="[domain]"/>
<meta name="twitter:image:src" content="[image]" />
<!--STYLES-->
<?php include 'styles.php';?>
</head>
<!-- /!\ SI L'ABONNE EST IDENTIFIE AJOUTER LA CLASSE "logged-in" AU BODY /!\ -->
<body class="classe " id="classe-matiere-chapitre-notion">

<!-- HEADER -->
<header class="bss-header">
  <?php include 'header.php';?>
  <?php include 'menu.php';?>
</header>
<div class="main animsition"> 
  <!-- BREADCRUMB -->
  <div class="bss-breadcrumb" >
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h6>
            <ol class="breadcrumb">
              <li id="a" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="index.php" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
              <li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="page-classe.php" itemprop="url"> <span itemprop="title">[classe]</span> </a></li>
              <li id="c" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="d"> <a href="page-classe-matiere.php" itemprop="url"> <span itemprop="title">[Matière : Français]</span> </a></li>
              <li id="d" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="e"> <a href="page-classe-matiere.php" itemprop="url"> <span itemprop="title">[Chapitre : Temps]</span> </a></li>
              <li id="f" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="g"> <span itemprop="title">[Notion : Les fonctions (2de)]</span> </li>
            </ol>
          </h6>
        </div>
      </div>
    </div>
  </div>
  <?php include ("template/section-notion.php");?>
  <?php include ("template/section-sous-notion-disponible.php");?>
  <?php include ("template/section-avantage-abonne.php");?>
  <?php include ("template/section-temoignage.php");?>
  <?php include ("template/section-notion-relatif.php");?>
  <?php include ("template/section-newsletter.php");?>
</div>
</div>
</div>
<!--FOOTER-->
<?php include 'footer.php';?>
<!--SCRIPTS-->
<?php include 'scripts.php';?>
</body>
</html>
