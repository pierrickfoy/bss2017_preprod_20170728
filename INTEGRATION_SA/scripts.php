<!--Jquery
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!--modernizr-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<!--bootstrap
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>

<!--carousel-->
<script src="js/owl.carousel.min.js"></script>
<!--Cookie Policy-->
<script src="js/cookiechoices.js"></script>
<script>

  document.addEventListener('DOMContentLoaded', function(event) {
    cookieChoices.showCookieConsentBar('En utilisant ce site, vous acceptez l\'utilisation des cookies pour ainsi bénéficier de services et offres adaptés. ',
      'Fermer', 'En savoir plus ...', 'mentions-legales.php');
  });
</script>
<!--scripts specifiques pour BSS-->
<script src="js/script.js"></script>

<!--A SUPPRIMER-->
<script src="http://anglehtml.oxygenna.com/assets/js/switcher.min.js"></script>