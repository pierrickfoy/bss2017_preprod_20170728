<div id="style-switcher" class="">
  <div class="switcher-inner text-center">
    <button class="btn btn-switcher" role="menu"><i class="icon icon-docs" aria-hidden="true"></i> </button>
    <h4><span>Maquette HTML <br />
      BSS 2015</span></h4>
    <ul>
      <li><a href="index.php">index</a></li>
      <li><a href="index-logon.php">index-logon</a></li>
      <li>-------------------------</li>
      <li><a href="qui-sommes-nous.php">qui-sommes-nous</a></li>
      <li><a href="comment-ca-marche.php">comment-ca-marche</a></li>
      <li><a href="comment-sabonner.php">comment-sabonner</a></li>
      <li><a href="mentions-legales.php">mentions-legales</a></li>
      <li><a href="cgu.php">cgu</a></li>
      <li><a href="cgv.php">cgv</a></li>
      <li><a href="faq.php">faq</a></li>
      <li>-------------------------</li>
      <li><a href="gestion-abonnement.php">gestion-abonnement</a></li>
      <li><a href="gestion-profil.php">gestion-profil</a></li>
      <li>-------------------------</li>
      <li><a href="actualite.php">actualite-liste</a></li> 
      <li><a href="actualite-single.php">actualite-single</a></li>
      <li>-------------------------</li>
       <li><a href="page-matiere.php">page-matiere</a></li>
      <li><a href="page-classe.php">page-classe</a></li>
      <li><a href="page-classe-matiere.php">page-classe-matiere</a></li>
      <li><a href="page-classe-matiere-chapitre.php">page-classe-matiere-chapitre</a></li>
      <li><a href="page-classe-matiere-chapitre-notion.php">page-classe-matiere-chapitre-notion</a></li>
      <li>-------------------------</li>
      <li><a href="fiche-produit.php">fiche-produit</a></li>
      <li><a href="tunnel-1.php">tunnel-1</a></li>
      <li><a href="tunnel-2.php">tunnel-2</a></li>
      <li><a href="tunnel-2-adresse.php">tunnel-2-adresse</a></li>
      <li><a href="tunnel-3.php">tunnel-3</a></li>
      <li><a href="tunnel-4.php">tunnel-4</a></li>
      <li>-------------------------</li>
      <li><a href="authentification.php">Authentification</a></li>
      <li><a href="mot-de-passe-oublie.php">Mot de passe oublié</a></li>
      <li>-------------------------</li>
      <li><a href="gestion-cookies.php">Gestion des cookies</a></li>
      <li>-------------------------</li>
      <li><a href="http://preprod.soutien-scolaire.lexpress.fr/INTEGRATION_SA/index.php">Page Express</a></li>
      
    </ul>
  </div>
</div>
