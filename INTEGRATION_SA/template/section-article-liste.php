<div class="bss-section bloc-section-orange bss-actualite">
  <div class="container">
    <h1 id="tagline" class="h1">Actualités Bordas Soutien Scolaire</h1>
    <h2 id="tagline" class="h2">Toute l'actualité du service d'entraînement en ligne de Bordas Soutien scolaire.</h2>
    <div id="articles" class="row">
      <div class="item col-sm-6 col-md-4 hidden-xs">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Europan lingues es membres del sam familie. Lor separat existentie es un myth.</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis di</div></div>
        </div>
      </div>
      <div class="item col-sm-6 col-md-4">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Europan lingues. Lor separat existentie es un myth. Por scientie, musi</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dolor sit amet, consectan commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu</div></div>
        </div>
      </div>
      <div class="item col-sm-6 col-md-4">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musi</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dol. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu</div></div>
        </div>
      </div>

      <div class="item col-sm-6 col-md-4">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Europan lbres del sam familie. Lor separat existentie es un myth. Por scientie, musi</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dolor sit amet, consecAenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu</div></div>
        </div>
      </div>
      <div class="item col-sm-6 col-md-4">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Europan liembres del sam familie. Lor separat existentie es un myth. Por scientie, musi</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dolor sit amet, consectet elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu</div></div>
        </div>
      </div>
      <div class="item col-sm-6 col-md-4 hidden-xs">
       <div class="bloc-annonce"><div class="prepend"><b class="caret"></b> publicité <b class="caret"></b></div><a href="#"><img src="http://www.bordas-soutien-scolaire.com/images/bss-wengo-300.gif"  class="img-responsive"></a></div>
      </div>

      <div class="item col-sm-6 col-md-4">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Europan lingues es membres del sam familie. Lor separat existentie es ntie, musi</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibuent montes, nascetur ridiculus mus. Donec qu</div></div>
        </div>
      </div>
      <div class="item col-sm-6 col-md-4">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Europembres del sam familie. Lor separat existentie es un myth. Por scientie, musi</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus </div></div>
        </div>
      </div>
      <div class="item col-sm-6 col-md-4">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Eues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musi</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dolor sit amcing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, naulus mus. Donec qu</div></div>
        </div>
      </div>
      
      <div class="item col-sm-6 col-md-4">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Europan lingues es membres del sam familie. Lor separat existentie es ntie, musi</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibuent montes, nascetur ridiculus mus. Donec qu</div></div>
        </div>
      </div>
      <div class="item col-sm-6 col-md-4">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Europembres del sam familie. Lor separat existentie es un myth. Por scientie, musi</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus </div></div>
        </div>
      </div>
      <div class="item col-sm-6 col-md-4">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Eues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musi</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dolor sit amcing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, naulus mus. Donec qu</div></div>
        </div>
      </div>
      
      <div class="item col-sm-6 col-md-4">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Europan lingues es membres del sam familie. Lor separat existentie es ntie, musi</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibuent montes, nascetur ridiculus mus. Donec qu</div></div>
        </div>
      </div>
      <div class="item col-sm-6 col-md-4">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Europembres del sam familie. Lor separat existentie es un myth. Por scientie, musi</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus </div></div>
        </div>
      </div>
      <div class="item col-sm-6 col-md-4">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Eues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musi</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dolor sit amcing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, naulus mus. Donec qu</div></div>
        </div>
      </div>
      
      <div class="item col-sm-6 col-md-4">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Europan lingues es membres del sam familie. Lor separat existentie es ntie, musi</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibuent montes, nascetur ridiculus mus. Donec qu</div></div>
        </div>
      </div>
      <div class="item col-sm-6 col-md-4">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Europembres del sam familie. Lor separat existentie es un myth. Por scientie, musi</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus </div></div>
        </div>
      </div>
      <div class="item col-sm-6 col-md-4">
        <div class="bloc-bss-actu">
          <div class="visuel-actu"><a href="actualite-single.php"><img src="img/1200x380.jpg" class="img-responsive"></a></div>
          <div class="info-actu "><h2><a href="actualite-single.php">Li Eues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musi</a></h2>
          <div class="tag-actu "><span class="rubrique">RUBRIQUE</span><div class="pull-right">JJ/MM/AAAA</div></div>
          <div class="excerpt">Lorem ipsum dolor sit amcing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, naulus mus. Donec qu</div></div>
        </div>
      </div>
      
    </div>
    <nav class="text-center">
  <ul class="pagination ">
    <li class="disabled">
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li class="active"><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li class="disabled"><a href="#">...</a></li>
    <li><a href="#">12</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>
  </div>
</div>
