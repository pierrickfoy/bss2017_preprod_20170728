<div class="bss-section bloc-section-bleu bss-avantage">
  <div class="container">
    <h1 id="tagline" class="h1">Les avantages abonnés</h1>
    <div class="row">
      <div class="col-sm-4">
        <div class=" bloc-bss-1">
          <div class="icone text-center"><img src="img/icone-cloud.png"></div>
          <div class="h3  text-center">Disponibilité</div>
          <p class="texte  text-center">Accessible en ligne ou hors connexion. Utilisation en mobilité ou à la maison</p>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="bloc-bss-1">
          <div class="icone text-center"><img src="img/icone-multisupport.png"></div>
          <div class="h3  text-center">Multi-supports</div>
          <p class="texte  text-center">Accessible sur tablette, ordinateur portable, ordinateur de bureau</p>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="bloc-bss-1">
          <div class="icone text-center"><img src="img/icone-ilimite.png"></div>
          <div class="h3  text-center">Accès illimité</div>
          <p class="texte  text-center">Toutes les ressources de votre abonnement durant votre engagement</p>
        </div>
      </div>
    </div>
  </div>
</div>
