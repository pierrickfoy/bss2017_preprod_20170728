
<div class="bss-section bloc-section-gris-bleu bss-autres-produits">
  <div class="container">
    <h1  class="h1">Les autres offres disponibles :</h1>
    <div class="row">
    <div class="col-md-3">
      <p class=" h3 text-center">Formule Ciblée</p>
        <p class="text-center"><a class="image-autres-produits" href="#"><img src="img/icone-cible-300-violet.png" class="img-responsive"> </a></p>
        <p class="text-center"><a class="lien-autres-produits" href="#">1 enfant / 1 matière</a></p>
      </div>
      <div class="col-md-3">
      <p class=" h3 text-center">Formule Réussite</p>
        <p class="text-center"><a class="image-autres-produits" href="#"><img src="img/icone-reussite-300-verteau.png" class="img-responsive"> </a></p>
        
        <p class="text-center "><a class="lien-autres-produits" href="#">1 enfant / Multi-matières</a></p>
      </div>
      <div class="col-md-3">
      <p class=" h3 text-center">Formule Tribu</p>
        <p class="text-center"><a class="image-autres-produits" href="#"><img src="img/icone-tribu-300-vert.png" class="img-responsive"> </a></p>
        <p class="text-center"><a class="lien-autres-produits" href="#">2 à 5 enfants / Multi-matières</a></p>
      </div>
       
    </div>
  </div>
</div>
