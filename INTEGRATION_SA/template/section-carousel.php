<!--NOUVELLE SECTION AVEC CAROUSEL ET BLOC-->

<div class="container classe-carousel">
  <div class="row">
    <div class=" col-md-8  ">
      <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
        
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
          <li data-target="#myCarousel" data-slide-to="3"></li>
          <li data-target="#myCarousel" data-slide-to="4"></li>
        </ol>
        
        <div class="carousel-inner" role="listbox">
        
          <div class="item active"  data-slide-number="0"> <a href="#" target="_blank"> <img src="img/montage-classe.png" class="img-responsive" border="0" alt="Titre du slide" title="Titre du slide">
            <div class="container">
              <div class="carousel-caption">
                <h2> Français CE2 : exemple de fiches de cours sur les groupes nominaux</h2>
                <p>Conforme aux programmes - Rédigé par des enseignants</p>
               
              </div>
            </div>
            </a> </div>
        
          <!--LOOP ITEM SLIDER-->
          <div class="item"  > 
          <a href="{url}" target="_blank"> <img src="img/montage-classe.png" class="img-responsive" border="0" alt="Titre du slide" title="Titre du slide">
            <div class="container">
              <div class="carousel-caption">
                <h2>{Titre}</h2>
                <p>{Sous-Titre}</p>
               
              </div>
            </div>
            </a> 
         </div>
            <!--END LOOP ITEM SLIDER-->
            
          <div class="item"  > <a href="#" target="_blank"> <img src="img/montage-classe.png" class="img-responsive" border="0" alt="Titre du slide" title="Titre du slide">
            <div class="container">
              <div class="carousel-caption">
                <h2>Cum sociis natoque penatibus et magnis dis parturient montes.</h2>
                <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>
               
              </div>
            </div>
            </a> </div>
          <div class="item"  > <a href="#" target="_blank"> <img src="img/montage-classe.png" class="img-responsive" border="0" alt="Titre du slide" title="Titre du slide">
            <div class="container">
              <div class="carousel-caption">
                <h2>Lorem ipsum dolor sit amet, consec tetuer adipiscing elit. </h2>
                <p>Aenean commodo ligula eget dolor. Aenean massa.</p>
               
              </div>
            </div>
            </a> </div>
          <div class="item"  > <a href="#" target="_blank"> <img src="img/montage-classe.png" class="img-responsive" border="0" alt="Titre du slide" title="Titre du slide">
            <div class="container">
              <div class="carousel-caption">
                <h2>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</h2>
                <p>Nulla consequat massa quis enim. Donec pede justo.</p>
               
              </div>
            </div>
            </a> </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <i class="icon-angle-left glyphicon-chevron-right"></i> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <i class="icon-angle-right glyphicon-chevron-right"></i> </a> </div>

    </div>
    <div class=" col-md-4  ">
      
      <div class="well prix-appel ">
        <p class="h3"><span class="partir">A&nbsp;partir&nbsp;de</span><span class="prix">3,99&nbsp;€</span>&nbsp;<span class="mois">/&nbsp;mois</span></p>
        <a class="btn btn-lg btn-primary btn-fw btn-abonne" href="#">Je m'abonne &gt;</a></div>
        
        <!--<div class="well prix-appel ">
        <p class="h3"><span class="partir">A&nbsp;partir&nbsp;de</span><span class="prix">3,99&nbsp;€</span>&nbsp;<span class="mois">/&nbsp;mois</span></p></div>
        <p><a class="btn btn-lg btn-primary  btn-fw" href="#">Je m'abonne<i class="icon-angle-right"></i></a></p>-->
        <a class="btn btn-lg  btn-link  btn-fw btn-test" href="#">Tester une leçon<i class="icon-angle-right"></i></a>
        <!--<div class="bss-conformite">
<div class="titre">CONFORME AUX PROGRAMMES</div>
<div class="sous-titre">Conçu par des enseignants</div>
        </div>-->
    </div>
  </div>
</div>
<!--FIN NOUVELLE SECTION AVEC CAROUSEL ET BLOC-->