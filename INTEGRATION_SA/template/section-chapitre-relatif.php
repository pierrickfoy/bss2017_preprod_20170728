<div class="bss-section bloc-section-bleu bss-chapitre">
  <div class="container">
    <p id="tagline" class="h1">Pour revoir tout le programme de [MAtière] en [Classe] <br>
Bordas Soutien scolaire propose également les chapitres suivants :</p>
    <div class="bloc-bss-1 item-relatif">
      <div class="item-relatif-body">
        <h3 class="item-relatif-heading"><a href="#">[CHAPITRE] [CLASSE] [MATIERE]</a></h3>
      </div>
    </div>
    <div class="bloc-bss-1 item-relatif">
      <div class="item-relatif-body">
        <h3 class="item-relatif-heading"><a href="#">[CHAPITRE] [CLASSE] [MATIERE]</a></h3>
      </div>
    </div>
    <div class="bloc-bss-1 item-relatif">
      <div class="item-relatif-body">
        <h3 class="item-relatif-heading"><a href="#">[CHAPITRE] [CLASSE] [MATIERE]</a></h3>
      </div>
    </div>
    <div class="bloc-bss-1 item-relatif">
      <div class="item-relatif-body">
        <h3 class="item-relatif-heading"><a href="#">[CHAPITRE] [CLASSE] [MATIERE]</a></h3>
      </div>
    </div>
    <div class="bloc-bss-1 item-relatif">
      <div class="item-relatif-body">
        <h3 class="item-relatif-heading"><a href="#">[CHAPITRE] [CLASSE] [MATIERE]</a></h3>
      </div>
    </div>
    <div class="bloc-bss-1 item-relatif">
      <div class="item-relatif-body">
        <h3 class="item-relatif-heading"><a href="#">[CHAPITRE] [CLASSE] [MATIERE]</a></h3>
      </div>
    </div>
  </div>
</div>
