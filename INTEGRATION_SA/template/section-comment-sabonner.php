<div class="bss-section  bloc-section-bleu bss-abonnement">
  <div class="container">
    <p id="tagline" class="h1">Comment s'abonner ?</p>
    <h2 id="subtitle" class="h2">Abonnez votre enfant à Bordas Soutien scolaire à partir de 3,99 € par mois.</h2>
    <div class="row">
      <div class="col-sm-4">
        <div class="bloc-bss-formule bloc-bss-violet">
          <form action="tunnel-1.php" class="form-horizontal form-cible">
            <div class="h2  text-center">Formule Ciblée</div>
            <div class="icone text-center"><img src="img/icone-cible-300-violet.png"></div>
             <p class="info  text-center">1 enfant / 1 matière</p>
            <div class="form-group">
              
              <div class="col-md-12">
                <select  name="cible-classe" class="form-control" id="cible-classe" placeholder="Classe" >
                  
                  <option value="Classe" selected="selected">Classe</option>
                  <option value="CP">CP</option>
                  <option value="CE1">CE1</option>
                  <option value="CE2">CE2</option>
                  <option value="CM1">CM1</option>
                  <option value="CM2">CM2</option>
                  <option value="-----------">----------</option>
                  <option value="6e">6e</option>
                  <option value="5e">5e</option>
                  <option value="4e">4e</option>
                  <option value="3e">3e</option>
                  <option value="----------">----------</option>
                  <option value="2de">2de</option>
                  <option value="1re L">1re L</option>
                  <option value="1re ES">1re ES</option>
                  <option value="1re S">1re S</option>
                  <option value="Tle L">Tle L</option>
                  <option value="Tle ES">Tle ES</option>
                  <option value="Tle S">Tle S</option>
                  <option value="----------">----------</option>
                  <option value="1re STD2A">1re STD2A</option>
                  <option value="1re ST2S">1re ST2S</option>
                  <option value="1re STMG">1re STMG</option>
                  <option value="1re STI2D">1re STI2D</option>
                  <option value="1re STL">1re STL</option>
                  <option value="Tle STD2A">Tle STD2A</option>
                  <option value="Tle ST2S">Tle ST2S</option>
                  <option value="Tle STMG">Tle STMG</option>
                  <option value="Tle STI2D">Tle STI2D</option>
                  <option value="Tle STL">Tle STL</option>
                </select>
              </div>
            </div>
            <div class="form-group">
             
              <div class="col-md-12">
                <select  name="cible-matiere" class="form-control" id="cible-matiere" placeholder="Matiere" >
           <option value="Matière" selected="selected">Matière</option>
                  <option value="Français">Français</option>
                  <option value="Maths">Maths</option>
                  <option value="Anglais">Anglais</option>
                  <option value="Hist/Géo">Hist/Géo</option>
                  <option value="SVT">SVT</option>
                  <option value="Phys/Chim">Phys/Chim</option>
                  <option value="SES">SES</option>
                  <option value="Sciences">Sciences</option>
                  <option value="Philo">Philo</option>
                </select>
              </div>
            </div>
            <p class="tarif  text-center"><span>À partir de</span> 3<sup>,99 €</sup><span>/mois</span></p>
            <p class="lead">Avec cette formule, votre enfant se concentre sur la matière dans laquelle il est en difficulté ou souhaite progresser.</p>
            <p class="bouton  text-center">
              <button type="submit" class="btn btn-ciblee  btn-fw ">J'abonne mon enfant<i class="icon-angle-right"></i></button>
            </p>
            <p class="bouton  text-center"><a href="fiche-produit.php" class="btn btn-ciblee-outline btn-fw">+ d'informations</a></p>
          </form>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="bloc-bss-formule bloc-bss-vert-deau">
          <form action="tunnel-1.php" class="form-horizontal form-reussite">
            <div class="h2  text-center">Formule Réussite</div>
            <div class="icone text-center"><img src="img/icone-reussite-300-verteau.png"></div>
            <p class="info  text-center">1 enfant / Multi-matières</p>
            <div class="form-group">
             <div class="col-md-12">
                <select  name="cible-classe" class="form-control" id="cible-classe" placeholder="Classe" >
                  
                  <option value="Classe" selected="selected">Classe</option>
                  <option value="CP">CP</option>
                  <option value="CE1">CE1</option>
                  <option value="CE2">CE2</option>
                  <option value="CM1">CM1</option>
                  <option value="CM2">CM2</option>
                  <option value="-----------">----------</option>
                  <option value="6e">6e</option>
                  <option value="5e">5e</option>
                  <option value="4e">4e</option>
                  <option value="3e">3e</option>
                  <option value="----------">----------</option>
                  <option value="2de">2de</option>
                  <option value="1re L">1re L</option>
                  <option value="1re ES">1re ES</option>
                  <option value="1re S">1re S</option>
                  <option value="Tle L">Tle L</option>
                  <option value="Tle ES">Tle ES</option>
                  <option value="Tle S">Tle S</option>
                  <option value="----------">----------</option>
                  <option value="1re STD2A">1re STD2A</option>
                  <option value="1re ST2S">1re ST2S</option>
                  <option value="1re STMG">1re STMG</option>
                  <option value="1re STI2D">1re STI2D</option>
                  <option value="1re STL">1re STL</option>
                  <option value="Tle STD2A">Tle STD2A</option>
                  <option value="Tle ST2S">Tle ST2S</option>
                  <option value="Tle STMG">Tle STMG</option>
                  <option value="Tle STI2D">Tle STI2D</option>
                  <option value="Tle STL">Tle STL</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12">
                <label for="reussite-matiere" class="col-md-6 control-label hidden-xs hidden-sm hidden-md hidden-lg">&nbsp;</label>
                <p class="form-control-static text-center">Multi-matières</p>
              </div>
            </div>
            <p class="tarif  text-center"><span>À partir de</span> 6<sup>,99 €</sup><span>/mois</span></p>
            <p class="lead">Avec cette formule, votre enfant accède à toutes les matières de sa classe, et s'entraîne selon son rythme et ses besoins.</p>
            <p class="bouton  text-center">
              <button type="submit" class="btn btn-reussite btn-fw">J'abonne mon enfant<i class="icon-angle-right"></i></button>
            </p>
            <p class="bouton  text-center"><a href="fiche-produit.php" class="btn btn-reussite-outline btn-fw">+ d'informations</a></p>
          </form>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="bloc-bss-formule bloc-bss-vert">
          <form action="tunnel-1.php" class="form-horizontal form-tribu">
            <div class="h2  text-center">Formule Tribu</div>
            <div class="icone text-center"><img src="img/icone-tribu-300-vert.png"></div>
            <p class="info  text-center">2 à 5 enfants / Multi-matières</p>
            <div class="form-group">
              <label for="tribu-classe" class="col-md-6  control-label tribu-label">Un de mes enfants est en</label>
              <div class="col-md-6">
                <select  name="tribu-classe" class="form-control" id="tribu-classe" placeholder="Classe" >
                  <option value="CP">CP</option>
                  <option value="CE1">CE1</option>
                  <option value="CE2">CE2</option>
                  <option value="CM1">CM1</option>
                  <option value="CM2">CM2</option>
                  <option value="-----------">----------</option>
                  <option value="6e">6e</option>
                  <option value="5e">5e</option>
                  <option value="4e">4e</option>
                  <option value="3e">3e</option>
                  <option value="----------">----------</option>
                  <option value="2de">2de</option>
                  <option value="1re L">1re L</option>
                  <option value="1re ES">1re ES</option>
                  <option value="1re S">1re S</option>
                  <option value="Tle L">Tle L</option>
                  <option value="Tle ES">Tle ES</option>
                  <option value="Tle S">Tle S</option>
                  <option value="----------">----------</option>
                  <option value="1re STD2A">1re STD2A</option>
                  <option value="1re ST2S">1re ST2S</option>
                  <option value="1re STMG">1re STMG</option>
                  <option value="1re STI2D">1re STI2D</option>
                  <option value="1re STL">1re STL</option>
                  <option value="Tle STD2A">Tle STD2A</option>
                  <option value="Tle ST2S">Tle ST2S</option>
                  <option value="Tle STMG">Tle STMG</option>
                  <option value="Tle STI2D">Tle STI2D</option>
                  <option value="Tle STL">Tle STL</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12">
                <label for="tribu-matiere" class="col-md-6 control-label hidden-xs hidden-sm hidden-md hidden-lg">&nbsp;</label>
                <p class="form-control-static text-center">Multi-matières</p>
              </div>
            </div>
            <p class="tarif  text-center"><span>À partir de </span>9<sup>,99 €</sup><span>/mois</span></p>
            <p class="lead">Cette formule permet d'abonner plusieurs enfants à un tarif avantageux. Chaque enfant peut accéder à toutes les matières.</p>
            <p class="bouton  text-center">
              <button type="submit" class="btn btn-tribu btn-fw">J'abonne mes enfants<i class="icon-angle-right"></i></button>
            </p>
            <p class="bouton  text-center"><a href="fiche-produit.php" class="btn btn-tribu-outline btn-fw">+ d'informations</a></p>
          </form>
        </div>
      </div>
      <div class="col-sm-12">
        
        <hr>
        <p class="h3">En abonnant votre enfant à Bordas Soutien scolaire vous lui donnez accès à :</p>
        <ul>
          <li>Un site conforme au programme et rédigé par des enseignants.</li>
          <li>Un service disponible 24h/24 et 7j/7, en ligne et même hors connexion.</li>
          <li>Des fiches de cours, des exercices interactifs et des ressources multimédia. (vidéos, ...)</li>
          <li>Des parcours d'entraînement personnalisés en fonction des réponses de votre enfant.</li>
          <li>Un espace parent pour suivre les progrès de votre enfant.</li>
          <li>Des corrigés connectés et personnalisés.</li>
        </ul>
      </div>
    </div>
  </div>
</div>
