<div class="bss-section  bloc-section-bleu bss-comparez-tarif" id="compare-tarif">
  <div class="container">
    <h1 id="tagline" class="h1">Comparez tous les tarifs</h1>
    <div class="">
      <div class="col-xs-12 col-sm-offset-4 col-sm-8 text-center">
        <div class="row">
          <div class="col-xs-4 my_planHeader my_plan1">
            <div class="my_planTitle">Formule Ciblée</div>
         
           
          </div>
          <div class="col-xs-4 my_planHeader my_plan2">
            <div class="my_planTitle">Formule Réussite</div>
            
          </div>
          <div class="col-xs-4 my_planHeader my_plan3">
            <div class="my_planTitle">Formule Tribu</div>
            
          </div>
        </div>
      </div>
    </div>
    <div class=" my_featureRow">
      <div class="col-xs-12 col-sm-4 my_feature"> Engagement 1 mois</div>
      <div class="col-xs-12 col-sm-8 text-center">
        <div class="row">
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan1">  <strong>5,99 €</strong>/mois </div>
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan2">  <strong>9,99 €</strong>/mois </div>
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan3">  <strong>14,99 €</strong>/mois </div>
        </div>
      </div>
    </div>
    <div class=" my_featureRow">
      <div class="col-xs-12 col-sm-4 my_feature"> Engagement 3 mois </div>
      <div class="col-xs-12 col-sm-8 text-center">
        <div class="row">
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan1"><strong> 4,99 €</strong>/mois </div>
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan2">  <strong>8,99 €</strong>/mois </div>
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan3">  <strong>12,99 €</strong>/mois </div>
        </div>
      </div>
    </div>
    <div class=" my_featureRow">
      <div class="col-xs-12 col-sm-4 my_feature"> Engagement 1 an</div>
      <div class="col-xs-12 col-sm-8 text-center">
        <div class="row">
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan1"> <strong>3,99 €</strong>/mois </div>
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan2"> <strong>6,99 €</strong>/mois </div>
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan3"> <strong>9,99 €</strong>/mois </div>
        </div>
      </div>
    </div>
  </div>
</div>
