<div class="bss-section bloc-section-gris bss-compatibilite">
  <div class="container">
    <h1 id="tagline" >Compatibilité technique</h1>
    <div class="row">
      <div class="col-md-4">
        <h2 >Système d'exploitation</h2>
        <ul>
          <li>Windows 7 32 bits ou 64 bits ou supérieur</li>
          <li>Mac OS X 10.4 32 bits ou 64 bits ou supérieur</li>
          <li>Android 4.1 ou supérieur</li>
          <li>iOS 7.1.1 ou supérieur</li>
        </ul>
      </div>
      <div class="col-md-4">
        <h2 >Résolution d'écran</h2>
        <ul>
          <li>Minimum : 320x568</li>
          <li>Maximum : 768x1024 ou supérieure</li>
        </ul>
      </div>
      <div class="col-md-4">
        <h2 >Navigateur internet</h2>
        <ul>
          <li>Internet explorer 8 ou supérieur</li>
          <li>Firefox 31.0 ou supérieur</li>
          <li>Google Chrome 35.0 ou supérieur</li>
          <li>Safari 6.1.6 ou supérieur</li>
        </ul>
      </div>
    </div>
  </div>
</div>
