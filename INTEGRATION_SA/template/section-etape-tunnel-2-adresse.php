<div class="bss-section  bloc-section-gris bss-tunnel">
  <div class="container"><form action="tunnel-3.php"  class="form-horizontal clearfix" id="form-adresse" title="form-adresse">
    <div class="row">
      <div class="col-md-8">
        <h1 class="h1">Informations personnelles</h1>
        <h2 >Mon adresse de facturation</h2>
        <div class="alert alert-danger" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Erreur</strong> <br>
          L'Adresse n'est pas valide<br>
          Le code postal n'est pas valide </div>
        <div class="bss-creation bloc-bss-1">
          
            <div class="form-group ">
              <label for="civilite" class="col-sm-6 control-label">Monsieur</label>
              <div class="col-sm-6">
                <p class="form-control-static">Hélène Gonzalez-Quijano</p>
              </div>
            </div>
             <div class="form-group">
              <label for="pays" class="col-sm-6 control-label"><span class="obligatoire">*</span>  Pays</label>
              <div class="col-sm-6">
                <select name="pays" class="form-control" id="pays" form="form-adresse" title="Pays">
                  <option>Choisir</option>
                  <option>France</option>
                  <option>Belgique</option>
                  <option>....</option>
                </select>
              </div>
            </div>
            <div class="form-group has-error" >
              <label for="code-postal" class="col-sm-6 control-label"><span class="obligatoire">*</span> Code postal</label>
              <div class="col-sm-3">
                <input name="code-postal" type="text" class="form-control" id="code-postal" form="form-adresse" placeholder="Code postal" title="Code postal">
              </div>
            </div>
            <div class="form-group">
              <label for="Ville" class="col-sm-6 control-label"><span class="obligatoire">*</span> Ville</label>
              <div class="col-sm-6">
                <input name="Ville" type="text" class="form-control" id="Ville" form="form-adresse" placeholder="Ville" title="Ville">
              </div>
            </div>
             <div class="form-group " >
              <label for="voie" class="col-sm-6 control-label"><span class="obligatoire">*</span> Adresse</label>
              <div class="col-sm-6">
              <div class="row">
              <div class="col-sm-4">
              <input name="Adresse" type="text" class="form-control" id="voie" form="form-adresse" placeholder="Numéro de voie" title="Numéro de voie">
              </div>
              <div class="col-sm-8"><input name="type-voie" type="text" class="form-control" id="type-voie" form="form-adresse" placeholder="Type de voie" title="Type de voie">
               </div>
              </div>
              </div>
          </div>
           
            <div class="form-group " >
              <label for="nom-voie" class="col-sm-6 control-label">&nbsp;</label>
              <div class="col-sm-6">
              
        
                <input name="Adresse" type="text" class="form-control" id="nom-voie" form="form-adresse" placeholder="Nom de la voie" title="Nom de la voie">
              </div>
            </div>
            <div class="form-group " >
              <label for="adresse-complementaire" class="col-sm-6 control-label">Adresse Complémentaire</label>
              <div class="col-sm-6">
                <input name="adresse-complementaire" type="text" class="form-control" id="adresse-complementaire" form="form-adresse" placeholder="Adresse complémentaire" title="Adresse complémentaire">
              </div>
            </div>
            <hr>
      
            <p class="legende"><span class="obligatoire">* <strong>champ obligatoire</strong></span></p>
            
            
           
          
          
        </div>
      </div>
      <div class="col-md-4 bss-step-sidebar">
        <div class="bloc-bss-step-sidebar bloc-bss-1">
          <div class="bloc-bss-produit-titre">Ma Formule</div>
          <ul>
            <li>Formule Tribu</li>
            <li>2 classes / Multi-matières</li>
          </ul>
          <div class="text-right"><a href="tunnel-1.php" class="btn btn-sm btn-default " type="button">Changer de formule</a></div>
        </div>
       
          <div class="bloc-bss-step-sidebar bloc-bss-1">
            <div class="bloc-bss-produit-titre">Prix de mon abonnement :</div>
            <div class="bloc-bss-produit-stitre">Pour une durée de 1 an :</div>
            <div class="bloc-bss-produit-prix"><span>A partir de </span>99<sup>,99 €</sup></div>
            <div class="legende text-center">(soit 99<sup>,99 €</sup> pour l'année)</div>
            <div class="form-group">
              <label for="modification-duree" class="sr-only">Modifier la durée</label>
              <select name="modification-duree" class="form-control" id="modification-duree" form="form-adresse" title="modification-duree">
                <option>Modifier la durée</option>
                <option>Durée 1 mois</option>
                <option>Durée 3 mois</option>
              </select>
            </div>
          </div>
          <div class="bloc-bss-step-sidebar bloc-bss-1">
            <div class="bloc-bss-produit-titre">Mon abonnement pour :</div>
            <div class="form-group">
              <label for="nom-enfant" class="control-panel">Nom de l'enfant</label>
              <input name="nom-enfant" type="text" class="form-control" id="nom-enfant" form="form-adresse" placeholder="Nom de l'enfant" title="Nom de l'enfant" value="Léa">
            </div>
            <label for="matiere" class="control-panel">Matières :</label>
            <ul class="liste-matiere">
              <li>Histoire Géographie</li>
              <li>Mathématiques</li>
            </ul>
            <hr>
          
            <div class="form-group">
              <label for="nom-enfant2" class="control-panel">Nom de l'enfant</label>
              <input name="nom-enfant2" type="text" class="form-control" id="nom-enfant2" form="form-adresse" placeholder="Nom de l'enfant" title="Nom de l'enfant" value="Julien">
            </div>
            <label for="matiere" class="control-panel">Matières :</label>
            <ul class="liste-matiere">
              <li>Histoire Géographie</li>
              <li>Mathématiques</li>
            </ul>
            <button type="submit" class="btn btn-fw  btn-primary"> Continuer <i class="icon-angle-circled-right"></i></button>
          </div>
          
           
        
      </div>
    </div></form>
  </div>
</div>
