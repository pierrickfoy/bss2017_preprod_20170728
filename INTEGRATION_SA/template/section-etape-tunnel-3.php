<div class="bss-section bloc-section-gris bss-tunnel">
  <div class="container">
    <form  action="tunnel-4.php" class="form-horizontal" id="paiement" title="form-code-promo">
      <div class="row">
        <div class="col-md-8">
          <h1 class="h1">Paiement</h1>
          <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>Merci !</strong> Votre code promotionnel à bien été pris en compte ! </div>
          <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>Erreur !</strong> Votre code promotionnel est faux ou périmé.</div>
          <div class="bss-connexion bloc-bss-1">
            <div class="input-group  ">
              <input type="text" class="form-control" id="code-promo" form="paiement" placeholder="Entrez votre code promotionnel">
              <span class="input-group-btn">
              <button class="btn btn-primary" type="submit">Valider</button>
              </span> </div>
            <!-- /input-group -->
            
            <p class="no-margin legende"><a href="#">Où puis-je trouver un code promo ?</a></p>
          </div>
          <hr>
          <h2>Choisissez votre mode de paiement :</h2>
          <div class="radio">
            <label>
              <input name="form-code-promo" type="radio" id="optionsRadios1" form="paiement" value="option1" checked>
              Réglez la totalité de votre abonnement en une seule fois</label>
          </div>
          <div class="radio">
            <label>
              <input name="form-code-promo" type="radio" id="optionsRadios2" form="paiement" value="option2">
              Optez pour un paiement échelonné (prélèvement mensuel)</label>
          </div>
          <hr>
          <h2>Récapitulatif de votre paiement :</h2>
          <ul class="paiement-recap">
            <li>Abonnement d'une durée de <strong>X an à 107,88 €</strong> sans reconduction tacite</li>
            <li>Abonnement valable jusqu'au : <strong  class="bleu">Date du jour + durée d'engagement</strong></li>
            <li><strong>Paiement mensualisé à 99,99€ par mois, </strong></li>
            <li>Prix du premier prélévement : <strong>99,99€</strong></li>
            <li>Prix récurent : <strong>99,99€</strong></li>
            <li>Prochain prélévement : <strong   class="bleu">Date + X mois</strong></li>
          </ul>
          <hr>
          <h2>Formulaire de paiement :</h2>
          <div class="bss-creation bloc-bss-1">
            <div class="form-group has-error">
              <label for="nom-carte-client" class="col-sm-6 control-label">Nom du porteur de la carte</label>
              <div class="col-sm-6">
                <input name="nom-carte-client" type="text" class="form-control" id="nom-carte-client" form="paiement" placeholder="Nom du porteur de la carte" title="Nom du porteur de la carte">
              </div>
            </div>
            <div class="form-group ">
              <label for="type-carte" class="col-sm-6 control-label">Type de carte</label>
              <div class="col-sm-6">
                <label class="radio-inline">
                  <input name="type-carte" type="radio" id="Visa" form="paiement" title="Carte Visa" value="Visa">
                  <span class="icone-visa">Visa</span> </label>
                <label class="radio-inline">
                  <input name="type-carte" type="radio" id="Mastercard" form="paiement" title="Mastercard" value="Mastercard">
                  <span class="icone-mastercard">Mastercard</span> </label>
                <label class="radio-inline">
                  <input name="type-carte" type="radio" id="CB" form="paiement" title="Carte Bleue" value="CB">
                  <span class="icone-cb">CB</span> </label>
              </div>
            </div>
            <div class="form-group  has-feedback" >
              <label for="numero-carte" class="col-sm-6 control-label">Numéro de carte</label>
              <div class="col-sm-6">
                <input name="numero-carte" type="text" class="form-control" id="numero-carte" form="paiement" placeholder="Numéro de carte" title="Numéro de carte">
                <i class="icon-shield form-control-feedback"></i> <span id="inputSuccess2Status" class="sr-only">(sécurisé)</span> </div>
            </div>
            <div class="form-group">
              <label for="date-expiration-carte" class="col-sm-6 control-label">Date d'expiration</label>
              <div class="col-sm-3">
                <select name="mois-expiration-carte" class="form-control" id="date-expiration-carte" form="paiement" title="mois-expiration-carte">
                  <option>Mois</option>
                  <option>Janvier</option>
                  <option>Février</option>
                  <option>....</option>
                </select>
              </div>
              <div class="col-sm-3">
                <select name="annee-expiration-carte" class="form-control" id="annee-expiration-carte" form="paiement" title="annee-expiration-carte">
                  <option>Année</option>
                  <option>2015</option>
                  <option>2016</option>
                  <option>....</option>
                </select>
              </div>
            </div>
            <div class="form-group  has-feedback" >
              <label for="numero-controle" class="col-sm-6 control-label">Numéro de carte</label>
              <div class="col-sm-3">
                <input name="numero-controle" type="text" class="form-control" id="numero-controle" form="paiement" placeholder="CVV" title="CVV">
                <i class="icon-shield form-control-feedback"></i> <span id="numero-controle-statut" class="sr-only">(sécurisé)</span> </div>
              <div class="col-sm-3"> <a id="popover" class="btn btn-primary btn-icon" tabindex="0"  role="button"  data-trigger="focus" rel="popover" data-content="" title="Emplacement du CVV">?</a> </div>
            </div>
            <!-- Modal aideccv-->
            <div class="modal aideccv fade" id="aideccv" tabindex="-1" role="dialog" aria-labelledby="aideccvLabel">
              <div class="modal-dialog " role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Emplacement du cryptogramme visuel</h4>
                  </div>
                  <div class="modal-body"> <img src="img/verso-carte.jpg"  class="img-responsive"/> </div>
                </div>
              </div>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" form="paiement" title="conditions-generales" value="conditions generales">
                J'ai lu et j'accepte les <a href="cgv.php">conditions générales de vente</a> et les <a href="cgu.php">conditions générales d'utilisation</a> </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" form="paiement" title="securite bancaire" value="securite-bancaire">
                J'accepte la conservation sécurisés de mes données bancaires </label>
            </div>
            <hr>
            <div class="well legende">La conservation sécurisée de vos données bancaires est nécessaire pour vous abonner mensuellement. Cela vous permet d’éviter de les saisir à nouveau pour la poursuite de votre abonnement. Nous vous rappelons que vous êtes libre d’interrompre l’abonnement mensuel dans les conditions définies aux conditions générales de ventes.</div>
            <div class="form-group">
              <div class="col-sm-6"> &nbsp;</div>
              <div class="col-sm-6">
                <button type="submit" class="btn btn-primary btn-fw">Valider votre paiement <i class="icon-ok "></i></button>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 bss-step-sidebar">
          <div class="bloc-bss-step-sidebar bloc-bss-gris">
            <div class="bloc-bss-produit-titre">Ma Formule</div>
            <ul class="liste-formule">
              <li>Formule Tribu</li>
              <li>2 classes / Multi-matières</li>
            </ul>
            <div class="text-right"><a href="tunnel-1.php" class="btn btn-sm btn-default " type="button">Changer de formule</a></div>
          </div>
          <div class="bloc-bss-step-sidebar bloc-bss-gris">
            <div class="bloc-bss-produit-titre">Prix de mon abonnement :</div>
            <div class="bloc-bss-produit-prix">99<sup>,99 €/mois</sup></div>
            <div class="legende text-center">(soit 99<sup>,99 €</sup> pour l'année)</div>
            <div class="legende ancien-prix text-center"><strong><s>99<sup>,99 €</sup></s> par mois</strong> (soit <s>99<sup>,99 €</sup></s> pour l'année)</div>
          </div>
          <div class="bloc-bss-step-sidebar bloc-bss-gris">
            <div class="bloc-bss-produit-titre">Mon abonnement pour :</div>
            <label for="liste-enfant" class="control-panel"><strong>Nom de l'enfant</strong></label>
            <ul class="liste-enfant">
              <li>Léa</li>
            </ul>
            <label for="liste-classe" class="control-panel"><strong>Classe :</strong></label>
            <ul class="liste-classe">
              <li>CM1</li>
            </ul>
            <label for="matiere" class="control-panel"><strong>Matières :</strong></label>
            <ul class="liste-matiere">
              <li>Histoire Géographie</li>
              <li>Mathématiques</li>
            </ul>
            <hr>
            <div class="bloc-bss-produit-titre">Mes informations personnelles</div>
            <ul class="recap-info-perso">
              <li><strong>Civilité :</strong> Mme</li>
              <li><strong>Prénom :</strong> Hélène</li>
              <li><strong>Nom :</strong> Gonzalez-Quijano</li>
              <li><strong>Identifiant :</strong> Hélène </li>
              <li><strong>Mot de passe :</strong> motdepassse </li>
            </ul>
          </div>
          <div class="bloc-bss-step-sidebar bloc-bss-gris bloc-bss-bleu">
            <div class="bloc-bss-produit-titre text-center"><i class="icon-shield "></i> Paiement sécurisé</div>
            <div class="text-center"><a role="button" data-toggle="collapse" href="#collapseSecurite" aria-expanded="false" aria-controls="collapseSecurite" class="btn  btn-outline  btn-fw collapsed">En savoir plus</a></div>
            <div class="collapse" id="collapseSecurite">
              <div class="well"> Tout paiement effectué sur notre site fait l’objet d’un système de sécurisation par certificat SSL (Secure Sockets Layer) permettant le cryptage de l'information relative à vos coordonnées bancaires. </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
