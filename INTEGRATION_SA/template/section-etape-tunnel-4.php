<div class="bss-section bloc-section-gris bss-tunnel">
  <div class="container">
    <div class="row">
      <div class="col-md-3"> <img src="img/icone-tribu-300-vert.png" class="img-responsive"> </div>
      <div class="col-md-9">
        <h1 class="h1">Confirmation</h1>
        <h2 class="text-success">Votre abonnement à bien été pris en compte</h2>
        <p class="lead">Nous vous remercions de votre confiance</p>
        <p class="numero-commande bg-success">Numéro de votre commande : <strong>[XXX-XXX-XXX]</strong></p>
        <p class="texte">Vous allez recevoir dans quelques minutes un mail de confirmation de commande.<br>
          À réception de ce mail votre abonnement sera activé et vous pourrez accédez à la plateforme de soutien scolaire en ligne Bordas.<br>
          Nous espérons que ce service répondra à vos attentes.<br>
          Le service client reste à votre écoute pour toute question par mail ou par téléphone. </p>
        <hr>
        <p class="lead"> Vous pouvez retrouver ces informations en téléchargement la confirmation de votre commande <a href="#">au format PDF</a></p>
        <div class="bss-recap-commande bloc-bss-1">
          <h3 class="h3">Information de facturation</h3>
          <ul class="paiement-recap">
            <li class="numero-commande">Numéro de votre commande : <span class="numero-commande"><strong>[XXX-XXX-XXX]</strong></span></li>
            <li class="infos-persos">Vos informations personnelles :<strong> Mme Hélène Gonzalez-Quijano</strong></li>
            <li class="adresse-facturation">Votre adresse :<strong> 25 avenue Pierre de Coubertin 75013 Paris</strong></li>
          </ul>
        </div>
        <div class="bss-recap-commande bloc-bss-1">
          <h3 class="h3">Mode de paiement</h3>
          <ul class="paiement-recap">
            <li class="type-paiement">Type de paiement : <span class="numero-commande"><strong>Carte Bancaire</strong></span></li>
            <li class="mode-prelevement">Mode de prélèvement :<strong> MENSUEL</strong></li>
            <li class="prochain-prevelement">Prochain prélèvement : <strong>le 00/00/0000</strong></li>
          </ul>
        </div>
        <div class="bss-recap-commande bloc-bss-1">
          <h3 class="h3">Votre abonnement</h3>
          <ul class="paiement-recap">
            <li class="type-formule">Votre formule : <span class="numero-commande"><strong>Tribu</strong></span></li>
            <li class="duree-engagement">Votre durée d'engagement :<strong> 1 an</strong></li>
            <li class="nom-enfant-1">Prénom de l'enfant :<strong> Léa</strong></li>
            <li class="classe-enfant-1">Classe : <strong>CM1</strong></li>
            <li class="matiere-enfant-1">Matière(s) : <strong>Mathématiques, Histoire-Géographie</strong></li>
            <li class="separateur"><hr></li>
            <li class="nom-enfant-1">Prénom de l'enfant :<strong> Léa</strong></li>
            <li class="classe-enfant-1">Classe : <strong>CM1</strong></li>
            <li class="matiere-enfant-1">Matière(s) : <strong>Mathématiques, Histoire-Géographie</strong></li><li class="separateur"><hr></li>
            <li class="nom-enfant-1">Prénom de l'enfant :<strong> Julien</strong></li>
            <li class="classe-enfant-1">Classe : <strong>CM1</strong></li>
            <li class="matiere-enfant-1">Matière(s) : <strong>Mathématiques, Histoire-Géographie</strong></li><li class="separateur"><hr></li>
            <li class="nom-enfant-1">Prénom de l'enfant :<strong> Léa</strong></li>
            <li class="classe-enfant-1">Classe : <strong>CM1</strong></li>
            <li class="matiere-enfant-1">Matière(s) : <strong>Mathématiques, Histoire-Géographie</strong></li><li class="separateur"><hr></li>
            <li class="nom-enfant-1">Prénom de l'enfant :<strong> Léa</strong></li>
            <li class="classe-enfant-1">Classe : <strong>CM1</strong></li>
            <li class="matiere-enfant-1">Matière(s) : <strong>Mathématiques, Histoire-Géographie</strong></li><li class="separateur"><hr></li>
            <li class="limite-dispo">Disponible jusqu'au : <strong>00/00/0000</strong></li>
          </ul>
        </div>
     <!--   <hr>
        
        <p class="text-center"> <a class="btn btn-md btn-primary" href="#">Accéder à la plateforme de révision<i class="icon-angle-right"></i></a> </p>-->
     
      </div>
    </div>
  </div>
</div>

