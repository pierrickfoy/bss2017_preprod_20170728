<div class="bss-section bloc-section-gris bss-tunnel">
  <form action="tunnel-2.php"  class="form-horizontal" id="offre-tribu" title="offre tribu">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 col-md-3"> <img src="img/icone-tribu-300-vert.png" class="img-responsive"> </div>
        <div class="col-sm-9 col-md-6">
          <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Merci !</strong> <br>
            [Message] </div>
          <div class="alert alert-info" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Info !</strong><br>
            [Message] </div>
          <div class="alert alert-warning" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Attention !</strong><br>
            [Message] </div>
          <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Erreur !</strong><br>
            [Message] </div>
          <h1 class="h1">Formule Tribu</h1>
          <p class="h2">2 à 5 classes - Multi-matières</p>
          <p class="h3">Je personnalise l'abonnement de mon enfant :</p>
          <div class="form-group">
            <label for="prenom" class="col-sm-6 control-label">Prénom de mon enfant</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" id="prenom" form="offre-tribu" placeholder="Prénom de mon enfant">
            </div>
          </div>
          <div class="form-group">
            <label for="classe" class="col-sm-6 control-label">Je choisis une classe</label>
            <div class="col-sm-6">
              <select name="classe" class="form-control" id="classe" form="offre-tribu">
                <option value="CP">CP</option>
                <option value="CE1">CE1</option>
                <option value="CE2">CE2</option>
                <option value="CM1">CM1</option>
                <option value="CM2">CM2</option>
                <option value="-----------">----------</option>
                <option value="6e">6e</option>
                <option value="5e">5e</option>
                <option value="4e">4e</option>
                <option value="3e">3e</option>
                <option value="----------">----------</option>
                <option value="2de">2de</option>
                <option value="1re L">1re L</option>
                <option value="1re ES">1re ES</option>
                <option value="1re S">1re S</option>
                <option value="Tle L">Tle L</option>
                <option value="Tle ES">Tle ES</option>
                <option value="Tle S">Tle S</option>
                <option value="----------">----------</option>
                <option value="1re STD2A">1re STD2A</option>
                <option value="1re ST2S">1re ST2S</option>
                <option value="1re STMG">1re STMG</option>
                <option value="1re STI2D">1re STI2D</option>
                <option value="1re STL">1re STL</option>
                <option value="Tle STD2A">Tle STD2A</option>
                <option value="Tle ST2S">Tle ST2S</option>
                <option value="Tle STMG">Tle STMG</option>
                <option value="Tle STI2D">Tle STI2D</option>
                <option value="Tle STL">Tle STL</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="matieres" class="col-sm-6 control-label">Matières disponibles</label>
            <div class="col-sm-6">
              <ul class="liste-matiere">
                <li>Français</li>
                <li>Maths</li>
              </ul>
            </div>
          </div>
          <hr>
          <div class="form-group">
            <label for="prenom2" class="col-sm-6 control-label">Prénom de mon enfant :</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" id="prenom2" form="offre-tribu" placeholder="Prénom de mon enfant">
            </div>
          </div>
          <div class="form-group">
            <label for="classe2" class="col-sm-6 control-label">Je choisis une classe</label>
            <div class="col-sm-6">
              <select name="classe2" class="form-control" id="classe2" form="offre-tribu">
                <option value="CP">CP</option>
                <option value="CE1">CE1</option>
                <option value="CE2">CE2</option>
                <option value="CM1">CM1</option>
                <option value="CM2">CM2</option>
                <option value="-----------">----------</option>
                <option value="6e">6e</option>
                <option value="5e">5e</option>
                <option value="4e">4e</option>
                <option value="3e">3e</option>
                <option value="----------">----------</option>
                <option value="2de">2de</option>
                <option value="1re L">1re L</option>
                <option value="1re ES">1re ES</option>
                <option value="1re S">1re S</option>
                <option value="Tle L">Tle L</option>
                <option value="Tle ES">Tle ES</option>
                <option value="Tle S">Tle S</option>
                <option value="----------">----------</option>
                <option value="1re STD2A">1re STD2A</option>
                <option value="1re ST2S">1re ST2S</option>
                <option value="1re STMG">1re STMG</option>
                <option value="1re STI2D">1re STI2D</option>
                <option value="1re STL">1re STL</option>
                <option value="Tle STD2A">Tle STD2A</option>
                <option value="Tle ST2S">Tle ST2S</option>
                <option value="Tle STMG">Tle STMG</option>
                <option value="Tle STI2D">Tle STI2D</option>
                <option value="Tle STL">Tle STL</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="matieres2" class="col-sm-6 control-label">Matières disponibles</label>
            <div class="col-sm-6">
              <ul class="liste-matiere">
                <li>Français</li>
                <li>Maths</li>
              </ul>
            </div>
          </div>
          <hr>
          <p class="text-center">
            <button type="button" class="btn  btn-default "><i class="icon-plus-circled"></i> Ajouter un enfant</button>
          </p>
          <!--<p class="text-center">
            <button type="button" class="btn  btn-primary">Continuer <i class="icon-angle-circled-right"></i></button>
          </p>--> 
        </div>
        <div class="col-sm-9 col-sm-offset-3   col-md-3 col-md-offset-0">
          <div class="bloc-bss-produit bloc-bss-gris">
            <div class="bloc-bss-produit-titre">Durée recommandée :</div>
            <div class="bloc-bss-produit-stitre">Prix pour une période de 1 an</div>
            <div class="bloc-bss-produit-prix">99<sup>,99€/mois TTC</sup></div>
            <div class="bloc-bss-duree-titre">Choisir une durée</div>
            <div class="bloc-bss-selection-duree">
              <div class="radio">
                <label>
                <input name="optionsRadios" type="radio" id="optionsRadios1" form="offre-tribu" value="option1" checked>
                <div class="option">*1 an - 99<sup>,99€</sup>/mois</div>
                <div class="legende">(soit 99<sup>,99€</sup> TTC pour l'année)</div>
                </label>
              </div>
              <div class="radio">
                <label>
                <input name="optionsRadios" type="radio" id="optionsRadios2" form="offre-tribu" value="option2">
                <div class="option">**3 mois - 99<sup>,99€</sup>/mois</div>
                <div class="legende">(soit 99<sup>,99€</sup> TTC pour 3 mois)</div>
                </label>
              </div>
              <div class="radio ">
                <label>
                <input name="optionsRadios" type="radio"  id="optionsRadios3" form="offre-tribu" value="option3">
                <div class="option">***1 mois - 99<sup>,99€</sup>/mois</div>
                </label>
              </div>
            </div>
          </div>
          <p class="legende">* Cet abonnement prend fin après la durée d'1 an. Il n'est pas reconduit tacitement. Pour plus d’informations, voir <a href="cgv.php">les conditions générales de vente.</a></p>
          <p class="legende">** Cet abonnement est reconduit tacitement par période de 3 mois. Il est résiliable à tout moment (après chaque période de 3 mois) et sans justificatif, en vous connectant sur ce site et en vous rendant sur la page « Gérer mon abonnement ». Pour plus d’informations, voir <a href="cgv.php">les conditions générales de vente</a>.</p>
          <p class="legende">*** Cet abonnement est reconduit tacitement chaque mois. Il est résiliable à tout moment et sans justificatif, en vous connectant sur ce site et en vous rendant sur la page « Gérer mon abonnement ». Pour plus d’informations, voir <a href="cgv.php">les conditions générales de vente</a>.</p>
          <!--<p class="text-center">
            <button type="submit" class="btn  btn-primary btn-fw">Je m'abonne </button>
            
          </p>-->
          <p class="text-center">
            <button type="submit" class="btn  btn-primary btn-fw">Continuer <i class="icon-angle-circled-right"></i></button>
          </p>
        </div>
      </div>
    </div>
  </form>
</div>
<div class="bss-section bloc-section-gris bss-fiche-tunnel">
  <form action="tunnel-2.php"  class="form-horizontal" id="offre-cible">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 col-md-3"> <img src="img/icone-cible-300-violet.png" class="img-responsive"> </div>
        <div class="col-sm-9 col-md-6">
          <h1 class="h1">Formule Ciblée</h1>
          <p class="h2">1 classe - 1 matière</p>
          <p class="h3">Je personnalise l'abonnement de mon enfant :</p>
          <div class="form-group">
            <label for="tribu-prenom" class="col-sm-6 control-label">Prénom de mon enfant</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" id="tribu-prenom" form="offre-cible" placeholder="Prénom de mon enfant">
            </div>
          </div>
          <div class="form-group">
            <label for="tribu-classe" class="col-sm-6 control-label">Je choisis une classe</label>
            <div class="col-sm-6">
              <select name="tribu-classe" class="form-control" id="tribu-classe" form="offre-cible" title="tribu-classe">
                <option value="CP">CP</option>
                <option value="CE1">CE1</option>
                <option value="CE2">CE2</option>
                <option value="CM1">CM1</option>
                <option value="CM2">CM2</option>
                <option value="-----------">----------</option>
                <option value="6e">6e</option>
                <option value="5e">5e</option>
                <option value="4e">4e</option>
                <option value="3e">3e</option>
                <option value="----------">----------</option>
                <option value="2de">2de</option>
                <option value="1re L">1re L</option>
                <option value="1re ES">1re ES</option>
                <option value="1re S">1re S</option>
                <option value="Tle L">Tle L</option>
                <option value="Tle ES">Tle ES</option>
                <option value="Tle S">Tle S</option>
                <option value="----------">----------</option>
                <option value="1re STD2A">1re STD2A</option>
                <option value="1re ST2S">1re ST2S</option>
                <option value="1re STMG">1re STMG</option>
                <option value="1re STI2D">1re STI2D</option>
                <option value="1re STL">1re STL</option>
                <option value="Tle STD2A">Tle STD2A</option>
                <option value="Tle ST2S">Tle ST2S</option>
                <option value="Tle STMG">Tle STMG</option>
                <option value="Tle STI2D">Tle STI2D</option>
                <option value="Tle STL">Tle STL</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="tribu-matiere" class="col-sm-6 control-label">Je choisis une matière</label>
            <div class="col-sm-6">
              <select name="tribu-matiere" class="form-control" id="tribu-matiere" form="offre-cible" title="tribu-matiere">
                <option value="Français">Français</option>
                <option value="Maths">Maths</option>
                <option value="Anglais">Anglais</option>
                <option value="Hist/Géo">Hist/Géo</option>
                <option value="SVT">SVT</option>
                <option value="Phys/Chim">Phys/Chim</option>
                <option value="SES">SES</option>
                <option value="Sciences">Sciences</option>
                <option value="Philo">Philo</option>
              </select>
            </div>
          </div>
          <hr>
          <p class="text-center"> 
            <!--<button type="button" class="btn  btn-default "><i class="icon-plus-circled"></i> Ajouter un enfant</button>
            <button type="submit" class="btn  btn-primary">Continuer <i class="icon-angle-circled-right"></i></button>--></p>
        </div>
        <div class="col-sm-9 col-sm-offset-3   col-md-3 col-md-offset-0">
          
          
          <div class="bloc-bss-produit bloc-bss-gris">
            <div class="bloc-bss-produit-titre">Durée recommandée :</div>
            <div class="bloc-bss-produit-stitre">Prix pour une période de 1 an</div>
            <div class="bloc-bss-produit-prix">99<sup>,99€/mois TTC</sup></div>
            <div class="bloc-bss-duree-titre">Choisir une durée</div>
            <div class="bloc-bss-selection-duree">
              <div class="radio">
                <label>
                <input name="optionsRadios" type="radio" id="optionsRadios1" form="offre-cible" value="option1" checked>
                <div class="option">*1 an - 99<sup>,99€</sup>/mois</div>
                <div class="legende">(soit 99<sup>,99€</sup> TTC pour l'année)</div>
                </label>
              </div>
              <div class="radio">
                <label>
                <input name="optionsRadios" type="radio" id="optionsRadios2" form="offre-cible" value="option2">
                <div class="option">**3 mois - 99<sup>,99€</sup>/mois</div>
                <div class="legende">(soit 99<sup>,99€</sup> TTC pour 3 mois)</div>
                </label>
              </div>
              <div class="radio ">
                <label>
                <input name="optionsRadios" type="radio"  id="optionsRadios3" form="offre-cible" value="option3">
                <div class="option">***1 mois - 99<sup>,99€</sup>/mois</div>
                </label>
              </div>
            </div>
          </div>
          <p class="legende">* Cet abonnement prend fin après la durée d'1 an. Il n'est pas reconduit tacitement. Pour plus d’informations, voir <a href="cgv.php">les conditions générales de vente.</a></p>
          <p class="legende">** Cet abonnement est reconduit tacitement par période de 3 mois. Il est résiliable à tout moment (après chaque période de 3 mois) et sans justificatif, en vous connectant sur ce site et en vous rendant sur la page « Gérer mon abonnement ». Pour plus d’informations, voir <a href="cgv.php">les conditions générales de vente</a>.</p>
          <p class="legende">*** Cet abonnement est reconduit tacitement chaque mois. Il est résiliable à tout moment et sans justificatif, en vous connectant sur ce site et en vous rendant sur la page « Gérer mon abonnement ». Pour plus d’informations, voir <a href="cgv.php">les conditions générales de vente</a>.</p>
          <p class="text-center">
            <button type="submit" class="btn  btn-primary btn-fw">Continuer <i class="icon-angle-circled-right"></i></button>
          </p>
        </div>
      </div>
    </div>
  </form>
</div>
<div class="bss-section bloc-section-gris bss-fiche-tunnel">
  <form action="tunnel-2.php"  class="form-horizontal" id="offre-reussite" title="offre-reussite">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 col-md-3"> <img src="img/icone-reussite-300-verteau.png" class="img-responsive"> </div>
        <div class="col-sm-9 col-md-6">
          <h1 class="h1">Formule Réussite</h1>
          <p class="h2">1 classe - Multi-matières</p>
          <p class="h3">Je personnalise l'abonnement de mon enfant :</p>
          <div class="form-group">
            <label for="reussite-prenom" class="col-sm-6 control-label">Prénom de votre enfant</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" id="reussite-prenom" form="offre-reussite" placeholder="Prénom de votre enfant">
            </div>
          </div>
          <div class="form-group">
            <label for="reussite-classe" class="col-sm-6 control-label">Je choisi une classe</label>
            <div class="col-sm-6">
              <select name="reussite-classe" class="form-control" id="reussite-classe" form="offre-reussite" title="reussite-classe">
                <option>CE1</option>
                <option>CE2</option>
                <option>CM1</option>
                <option>CM2</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="reussite-matiere" class="col-sm-6 control-label">Matières disponibles</label>
            <div class="col-sm-6">
              <ul class="liste-matiere">
                <li>Français</li>
                <li>Maths</li>
              </ul>
            </div>
          </div>
          <hr>
          <p class="text-center"> 
            <!--<button type="button" class="btn  btn-default "><i class="icon-plus-circled"></i> Ajouter un enfant</button>
            <button type="submit" class="btn  btn-primary">Continuer <i class="icon-angle-circled-right"></i></button>--></p>
        </div>
        <div class="col-sm-9 col-sm-offset-3   col-md-3 col-md-offset-0">
          
          <div class="bloc-bss-produit bloc-bss-gris">
            <div class="bloc-bss-produit-titre">Durée recommandée :</div>
            <div class="bloc-bss-produit-stitre">Prix pour une période de 1 an</div>
            <div class="bloc-bss-produit-prix">99<sup>,99€/mois TTC</sup></div>
            <div class="bloc-bss-duree-titre">Choisir une durée</div>
            <div class="bloc-bss-selection-duree">
              <div class="radio">
                <label>
                <input name="optionsRadios" type="radio" id="optionsRadios1" form="offre-reussite" value="option1" checked>
                <div class="option">*1 an - 99<sup>,99€</sup>/mois</div>
                <div class="legende">(soit 99<sup>,99€</sup> TTC pour l'année)</div>
                </label>
              </div>
              <div class="radio">
                <label>
                <input name="optionsRadios" type="radio" id="optionsRadios2" form="offre-reussite" value="option2">
                <div class="option">**3 mois - 99<sup>,99€</sup>/mois</div>
                <div class="legende">(soit 99<sup>,99€</sup> TTC pour 3 mois)</div>
                </label>
              </div>
              <div class="radio ">
                <label>
                <input name="optionsRadios" type="radio"  id="optionsRadios3" form="offre-reussite" value="option3">
                <div class="option">***1 mois - 99<sup>,99€</sup>/mois</div>
                </label>
              </div>
            </div>
          </div>
          <p class="legende">* Cet abonnement prend fin après la durée d'1 an. Il n'est pas reconduit tacitement. Pour plus d’informations, voir <a href="cgv.php">les conditions générales de vente.</a></p>
          <p class="legende">** Cet abonnement est reconduit tacitement par période de 3 mois. Il est résiliable à tout moment (après chaque période de 3 mois) et sans justificatif, en vous connectant sur ce site et en vous rendant sur la page « Gérer mon abonnement ». Pour plus d’informations, voir <a href="cgv.php">les conditions générales de vente</a>.</p>
          <p class="legende">*** Cet abonnement est reconduit tacitement chaque mois. Il est résiliable à tout moment et sans justificatif, en vous connectant sur ce site et en vous rendant sur la page « Gérer mon abonnement ». Pour plus d’informations, voir <a href="cgv.php">les conditions générales de vente</a>.</p>
          <p class="text-center">
            <button type="submit" class="btn  btn-primary btn-fw">Continuer <i class="icon-angle-circled-right"></i></button>
          </p>
        </div>
      </div>
    </div>
  </form>
</div>
