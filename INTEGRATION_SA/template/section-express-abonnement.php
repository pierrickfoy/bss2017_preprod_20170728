<div class="bss-section bss-express bloc-section-bleu bss-home-abonnement">
  <div class="container">
    <p id="tagline" class="h1 text-center">Choississez la formule qui vous convient :</p>
   
    <div class="row">
      <div class="col-sm-4">
        <div class="bloc-bss-formule bloc-bss-violet">
          <form action="tunnel-1.php" class="form-horizontal form-cible">
            <div class="h2  text-center">Formule Ciclée</div>
            <div class="icone text-center"><img src="img/icone-cible-300-violet.png"></div>
            <p class="info  text-center">1 enfant</p>
            
            <div class="form-group">
              <div class="col-md-12">
                <label for="tribu-matiere" class="col-md-6 control-label hidden-xs hidden-md hidden-lg">&nbsp;</label>
                <p class="form-control-static text-center">1 classe - 1 matière</p>
              </div>
            </div>
            <p class="tarif  text-center"><span>A partir de</span> 5<sup>,99 €</sup></p>
            <p class="bouton  text-center">
              <button type="submit" class="btn btn-ciblee ">J’ABONNE mon enfant</button>
            </p>
          </form>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="bloc-bss-formule bloc-bss-vert-deau">
          <form action="tunnel-1.php" class="form-horizontal form-reussite">
            <div class="h2  text-center">Formule Réussite</div>
            <div class="icone text-center"><img src="img/icone-reussite-300-verteau.png"></div>
            <p class="info  text-center">1 enfant</p>
            
            <div class="form-group">
              <div class="col-md-12">
                <label for="reussite-matiere" class="col-md-6 control-label hidden-xs hidden-md hidden-lg">&nbsp;</label>
                <p class="form-control-static text-center">1 classe - 1 matière</p>
              </div>
            </div>
            <p class="tarif  text-center"><span>A partir de</span> 9<sup>,99 €</sup></p>
            <p class="bouton  text-center">
              <button type="submit" class="btn btn-reussite ">J’ABONNE mon enfant</button>
            </p>
          </form>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="bloc-bss-formule bloc-bss-vert">
          <form action="tunnel-1.php" class="form-horizontal form-tribu">
            <div class="h2  text-center">Formule Tribu</div>
            <div class="icone text-center"><img src="img/icone-tribu-300-vert.png"></div>
            <p class="info  text-center">de 2 à 5 enfants</p>
            
            <div class="form-group">
              <div class="col-md-12">
                <label for="tribu-matiere" class="col-md-6 control-label hidden-xs hidden-md hidden-lg">&nbsp;</label>
                <p class="form-control-static text-center">5 classes - Plusieurs matières</p>
              </div>
            </div>
            <p class="tarif  text-center"><span>A partir de</span> 14<sup>,99 €</sup></p>
            <p class="bouton  text-center">
              <button type="submit" class="btn btn-tribu ">J’ABONNE mes enfants</button>
            </p>
          </form>
        </div>
      </div>
    </div>
    
  </div>
</div>
