<div class="bss-section bloc-section-gris bss-fiche-produit">
  <form action="tunnel-2.php"  class="form-horizontal" id="form-fiche-produit" title="form-fiche-produit">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 col-md-3"> <img src="img/icone-cible-300-violet.png" class="img-responsive"> 
         <p class="text-center"> <a class="btn  btn-info btn-fw ancre" href="#test-demo"><i class="icon-desktop"></i> Tester le service </a></p>  <p class="text-center"><a class="btn  btn-fw btn-default ancre" href="#voir-video"><i class="icon-play-circled"></i> Voir la vidéo démo </a></p></div>
        <div class="col-sm-9 col-md-6">
          <h1 class="h1">Formule Ciblée</h1>
          <p class="h2">1 enfant / 1 matière</p>
          <p class="lead">Avec cette formule, votre enfant se concentre sur la matière dans laquelle il est en difficulté ou souhaite progresser.</p>
          <p class="h3">Les matières disponibles pour chaque classe :</p>
          <div class="form-group">
            <label for="classe" class="col-sm-6 control-label">Je choisis une classe</label>
            <div class="col-sm-6">
              <select  name="classe" class="form-control" id="classe" form="form-fiche-produit" placeholder="Classe" >
                <option value="CP">CP</option>
                <option value="CE1">CE1</option>
                <option value="CE2">CE2</option>
                <option value="CM1">CM1</option>
                <option value="CM2">CM2</option>
                <option value="-----------">----------</option>
                <option value="6e">6e</option>
                <option value="5e">5e</option>
                <option value="4e">4e</option>
                <option value="3e">3e</option>
                <option value="----------">----------</option>
                <option value="2de">2de</option>
                <option value="1re L">1re L</option>
                <option value="1re ES">1re ES</option>
                <option value="1re S">1re S</option>
                <option value="Tle L">Tle L</option>
                <option value="Tle ES">Tle ES</option>
                <option value="Tle S">Tle S</option>
                <option value="----------">----------</option>
                <option value="1re STD2A">1re STD2A</option>
                <option value="1re ST2S">1re ST2S</option>
                <option value="1re STMG">1re STMG</option>
                <option value="1re STI2D">1re STI2D</option>
                <option value="1re STL">1re STL</option>
                <option value="Tle STD2A">Tle STD2A</option>
                <option value="Tle ST2S">Tle ST2S</option>
                <option value="Tle STMG">Tle STMG</option>
                <option value="Tle STI2D">Tle STI2D</option>
                <option value="Tle STL">Tle STL</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="matiere" class="col-sm-6 control-label">Je choisis une matière</label>
            <div class="col-sm-6">
              <select  name="matiere" class="form-control" id="matiere" form="form-fiche-produit" placeholder="Matiere" >
                <option value="Français">Français</option>
                <option value="Maths">Maths</option>
                <option value="Anglais">Anglais</option>
                <option value="Hist/Géo">Hist/Géo</option>
                <option value="SVT">SVT</option>
                <option value="Phys/Chim">Phys/Chim</option>
                <option value="SES">SES</option>
                <option value="Sciences">Sciences</option>
                <option value="Philo">Philo</option>
              </select>
            </div>
          </div>
          <p class="text">En se réveillant un matin après des rêves agités, Gregor Samsa se retrouva, dans son lit, métamorphosé en un monstrueux insecte. Il était sur le dos, un dos aussi dur qu’une carapace, et, en relevant.</p>
          <p class="text">Quanta autem vis amicitiae sit, ex hoc intellegi maxime potest, quod ex infinita societate generis humani, quam conciliavit ipsa natura, ita contracta res est et adducta in angustum ut omnis caritas aut inter duos aut inter paucos iungeretur.</p>

<p class="text">Et olim licet otiosae sint tribus pacataeque centuriae et nulla suffragiorum certamina set Pompiliani redierit securitas temporis, per omnes tamen quotquot sunt partes terrarum, ut domina suscipitur et regina et ubique patrum reverenda cum auctoritate canities populique Romani nomen circumspectum et verecundum.</p>

<p class="text">Inter quos Paulus eminebat notarius ortus in Hispania, glabro quidam sub vultu latens, odorandi vias periculorum occultas perquam sagax. is in Brittanniam missus ut militares quosdam perduceret ausos conspirasse Magnentio, cum reniti non possent, iussa licentius supergressus fluminis modo fortunis conplurium sese repentinus infudit et ferebatur per strages multiplices ac ruinas, vinculis membra ingenuorum adfligens et quosdam obterens manicis, crimina scilicet multa consarcinando a veritate longe discreta. unde admissum est facinus impium, quod Constanti tempus nota inusserat sempiterna.</p>
         
        </div>
        <div class="col-sm-9 col-sm-offset-3   col-md-3 col-md-offset-0">
          <div class="bloc-bss-produit bloc-bss-gris">
            <div class="bloc-bss-produit-titre">Durée recommandée :</div>
            <div class="bloc-bss-produit-stitre">Prix pour une période de 1 an</div>
            <div class="bloc-bss-produit-prix">99<sup>,99€/mois TTC</sup></div>
            <div class="bloc-bss-duree-titre">Choisir une durée</div>
            <div class="bloc-bss-selection-duree">
              <div class="radio">
                <label>
                <input name="optionsRadios" type="radio" id="optionsRadios1" form="form-fiche-produit" value="option1" checked>
                <div class="option">*1 an - 99<sup>,99€</sup>/mois</div>
                <div class="legende">(soit 99<sup>,99€</sup> TTC pour l'année)</div>
                </label>
              </div>
              <div class="radio">
                <label>
                <input name="optionsRadios" type="radio" id="optionsRadios2" form="form-fiche-produit" value="option2">
                <div class="option">**3 mois - 99<sup>,99€</sup>/mois</div>
                <div class="legende">(soit 99<sup>,99€</sup> TTC pour 3 mois)</div>
                </label>
              </div>
              <div class="radio ">
                <label>
                <input name="optionsRadios" type="radio"  id="optionsRadios3" form="form-fiche-produit" value="option3">
                <div class="option">***1 mois - 99<sup>,99€</sup>/mois</div>
                </label>
              </div>
            </div>
          </div>
          <p class="legende">* Cet abonnement prend fin après la durée d'1 an. Il n'est pas reconduit tacitement. Pour plus d’informations, voir <a href="cgv.php">les conditions générales de vente.</a></p>
          <p class="legende">** Cet abonnement est reconduit tacitement par période de 3 mois. Il est résiliable à tout moment (après chaque période de 3 mois) et sans justificatif, en vous connectant sur ce site et en vous rendant sur la page « Gérer mon abonnement ». Pour plus d’informations, voir <a href="cgv.php">les conditions générales de vente</a>.</p>
          <p class="legende">*** Cet abonnement est reconduit tacitement chaque mois. Il est résiliable à tout moment et sans justificatif, en vous connectant sur ce site et en vous rendant sur la page « Gérer mon abonnement ». Pour plus d’informations, voir <a href="cgv.php">les conditions générales de vente</a>.</p>
          <!--<p class="text-center">
            <button type="submit" class="btn  btn-primary btn-fw">Je m'abonne </button>
            
          </p>--><p class="text-center">
            <button type="submit" class="btn  btn-primary btn-fw">Je m'abonne <i class="icon-angle-right"></i></button>
          </p>
          
          <!-- <div class="bloc-bss-produit">
            <div class="bloc-bss-produit-titre">Durée recommandée :</div>
            <div class="bloc-bss-produit-stitre">Prix pour une période de 1 an</div>
            <div class="bloc-bss-produit-prix">99<sup>,99€/mois TTC</sup></div>
            <div class="bloc-bss-duree-titre">Choisir une durée</div>
            <div class="bloc-bss-selection-duree">
              <div class="radio">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                  1 an 99<sup>,99€/mois TTC</sup> (1)<br>
                  (soit 99,00€ pour l'année) </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                  1 an 99<sup>,99€/mois TTC</sup> (1)<br>
                  (soit 99<sup>,99€/mois TTC</sup> pour l'année) </label>
              </div>
              <div class="radio disabled">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
                  1 an 99<sup>,99€/mois TTC</sup> (1)<br>
                  (soit 99<sup>,99€/mois TTC</sup> pour l'année) </label>
              </div>
            </div>
          </div>
          <p class="legende">(1) Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pro</p>
          <p class="text-center"> <a class="btn  btn-primary" href="#">Je m'abonne</a></p>
           <div class="bloc-bss-produit">
            <div class="bloc-bss-produit-titre">Durée recommandée :</div>
            <div class="bloc-bss-produit-stitre">Prix pour une période de 1 an</div>
            <div class="bloc-bss-produit-prix">99<sup>,99€/mois TTC</sup></div>
            <div class="bloc-bss-duree-titre">Choisir une durée</div>
            <div class="bloc-bss-selection-duree">
              <div class="radio">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                  1 an 99<sup>,99€/mois TTC</sup> (1)<br>
                  (soit 99,00€ pour l'année) </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                  1 an 99<sup>,99€/mois TTC</sup> (1)<br>
                  (soit 99<sup>,99€/mois TTC</sup> pour l'année) </label>
              </div>
              <div class="radio disabled">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
                  1 an 99<sup>,99€/mois TTC</sup> (1)<br>
                  (soit 99<sup>,99€/mois TTC</sup> pour l'année) </label>
              </div>
            </div>
          </div>
          <p class="legende">(1) Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pro</p>
          <p class="text-center"> <a class="btn  btn-primary" href="#">Je m'abonne</a></p>--> 
        </div>
      </div>
    </div>
  </form>
</div>
