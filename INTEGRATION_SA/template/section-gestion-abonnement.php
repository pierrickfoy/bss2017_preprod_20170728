<div class="bss-section bloc-section-bleu bss-gestion-abonnement">
  <div class="container">
    <div class="row">
    <div class="col-md-12">
      
      <h1 class="h1">Gérer mon abonnement</h1>
      <h2 class="">Abonnement(s) en cours :</h2></div>
      <hr>
      <div id="abo-en-cours" class="abo-en-cours clearfix">
        <div class="col-sm-2 col-md-2"> <img src="img/icone-tribu-300-vert.png" class="img-responsive"> </div>
        <div class="col-sm-10 col-md-7">
          <h3 class="h3">Formule Tribu <span>valable jusqu'au 00/00/0000</span></h3>
          <p class="nombre-enfant"><strong>X enfants</strong></p>
          <ul class="abonnement-recap">
            <li class="enfant-recap">Julien révise en CM2</li>
            <li class="enfant-recap">Léa révise en CM1</li>
            <li class="enfant-recap">Mathilde révise CE2</li>
          </ul>
          <div class="panel-group" id="gestion-abonnement-1" role="tablist" aria-multiselectable="true">
            <div class="menu-panel "> <a role="button" data-toggle="collapse" data-parent="#gestion-abonnement-1" href="#details-abo" aria-expanded="true" aria-controls="details-abo" class="btn btn-default collapsed"> <i class="icon-search"></i> Voir les détails </a> <a role="button" data-toggle="collapse" data-parent="#gestion-abonnement-1" href="#modifier-abo" aria-expanded="false" aria-controls="modifier-abo" class="btn btn-default collapsed"> <i class="icon-pencil"></i> Modifier </a></div>
            <div class="panel ">
              <div id="details-abo" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                <div class="bloc-bss-1">
                  <p class="h4">Offre d'abonnement :</p>
                  <ul>
                  <li>Formule : <strong>Tribu</strong></li>
                    <li>Date de début : <strong>le 00/00/0000</strong> </li>
                    <li>Date de fin : <strong>le 00/00/0000</strong> avec tacite reconduction</li>
                    <li>Numéro de commande : <strong>[XXX-XXX-XXX]</strong></li>
                    <li>Prix TTC : <strong>[XX, XX] /€ </strong>pour les trois premiers mois puis<strong> [XX, XX] € </strong>tous les 3 mois.</li>
                    <li>Prochain prélèvement : <strong>le 00/00/0000</strong></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="panel ">
              <div id="modifier-abo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="bloc-bss-1">
                  <p class="h4">Modifier l'abonnement</p>
                  <form class="form-horizontal" id="modif-abonnement">
                    <div class="form-group">
                      <label for="modif-enfant" class="col-sm-6 control-label">Je modifie l'abonnement de </label>
                      <div class="col-sm-6">
                        <select name="modif-enfant" class="form-control" id="modif-enfant" placeholder="enfant">
                          <option value="Français">Léa</option>
                          <option value="Maths">Julien</option>
                        </select>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="modif-classe" class="col-sm-6 control-label">Classe</label>
                      <div class="col-sm-6">
                        <select name="modif-classe" class="form-control" id="modif-classe" placeholder="Classe">
                          <option value="CP">CP</option>
                          <option value="CE1">CE1</option>
                          <option value="CE2">CE2</option>
                          <option value="CM1">CM1</option>
                          <option value="CM2">CM2</option>
                          <option value="-----------">----------</option>
                          <option value="6e">6e</option>
                          <option value="5e">5e</option>
                          <option value="4e">4e</option>
                          <option value="3e">3e</option>
                          <option value="----------">----------</option>
                          <option value="2de">2de</option>
                          <option value="1re L">1re L</option>
                          <option value="1re ES">1re ES</option>
                          <option value="1re S">1re S</option>
                          <option value="Tle L">Tle L</option>
                          <option value="Tle ES">Tle ES</option>
                          <option value="Tle S">Tle S</option>
                          <option value="----------">----------</option>
                          <option value="1re STD2A">1re STD2A</option>
                          <option value="1re ST2S">1re ST2S</option>
                          <option value="1re STMG">1re STMG</option>
                          <option value="1re STI2D">1re STI2D</option>
                          <option value="1re STL">1re STL</option>
                          <option value="Tle STD2A">Tle STD2A</option>
                          <option value="Tle ST2S">Tle ST2S</option>
                          <option value="Tle STMG">Tle STMG</option>
                          <option value="Tle STI2D">Tle STI2D</option>
                          <option value="Tle STL">Tle STL</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="modif-matiere" class="col-sm-6 control-label">Matière(s)</label>
                     
<div class="col-sm-6">
                          <p class="form-control-static">Multi-matières</p>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-6 hidden-xs"></div>
                     
<div class="col-sm-6">
                           <button type="submit" class="btn btn-primary btn-fw ">Je valide <i class="icon-ok"></i></button>
                      </div>
                    </div>
                    <p class="bouton  text-center">
                     
                    </p>
                    <ul><li><a href="#" data-toggle="modal" data-target="#cbModal">Modifier mes coordonnées bancaires</a> </li>
                    <li><a href="#" data-toggle="modal" data-target="#raModal">Résilier mon abonnement</a></li>
                  </ul></form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-offset-2 col-sm-10 col-md-offset-0 col-md-3">
          <div class="bloc-bss-step-sidebar bloc-bss-1">
            <div class="bloc-bss-produit-titre text-center">Accéder à la plateforme</div>
            <div class="text-center"><a type="button" class="btn   btn-primary ">Connexion<i class="icon-angle-right"></i></a></div>
          </div>
        </div>
      </div>
      <hr>
      <div id="abo-en-cours" class="abo-en-cours clearfix">
        <div class="col-sm-2 col-md-2"> <img src="img/icone-reussite-300-verteau.png" class="img-responsive"> </div>
        <div class="col-sm-10 col-md-7">
          <h3 class="h3">Formule Réussite <span>valable jusqu'au 00/00/0000</span></h3>
          <!--<p class="nombre-enfant"><strong>X enfants</strong></p>-->
          <ul class="abonnement-recap">
            <li class="enfant-recap">Julien révise en CM2</li>
          </ul>
          <div class="panel-group" id="gestion-abonnement-3" role="tablist" aria-multiselectable="true">
            <div class="menu-panel "> <a role="button" data-toggle="collapse" data-parent="#gestion-abonnement-3" href="#details-abo-3" aria-expanded="true" aria-controls="details-abo" class="btn btn-default collapsed"> <i class="icon-search"></i> Voir les détails </a> <a role="button" data-toggle="collapse" data-parent="#gestion-abonnement-3" href="#modifier-abo-3" aria-expanded="false" aria-controls="modifier-abo" class="btn btn-default collapsed"> <i class="icon-pencil"></i> Modifier </a></div>
            <div class="panel ">
              <div id="details-abo-3" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                <div class="bloc-bss-1">
                  <p class="h4">Offre d'abonnement :</p>
                  <ul>
                  <li>Formule : <strong>Réussite</strong></li>
                    <li>Date de début : <strong>le 00/00/0000</strong> </li>
                    <li>Date de fin : <strong>le 00/00/0000</strong> avec tacite reconduction</li>
                    <li>Numéro de commande : <strong>[XXX-XXX-XXX]</strong></li>
                   <li>Prix TTC : <strong>[XX, XX] /€ </strong>pour les trois premiers mois puis<strong> [XX, XX] € </strong>tous les 3 mois.</li>
                    <li>Prochain prélèvement : <strong>le 00/00/0000</strong></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="panel ">
              <div id="modifier-abo-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="bloc-bss-1">
                  <p class="h4">Modifier l'abonnement</p>
                  <form class="form-horizontal" id="modif-abonnement-3">
                    <div class="form-group">
                      <label for="modif-enfant" class="col-sm-6 control-label">Je modifie l'abonnement de </label>
                      <div class="col-sm-6">
                            <p class="form-control-static">Léa</p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="modif-classe" class="col-sm-6 control-label">Classe</label>
                      <div class="col-sm-6">
                        <select name="modif-classe" class="form-control" id="modif-classe" placeholder="Classe">
                          <option value="CP">CP</option>
                          <option value="CE1">CE1</option>
                          <option value="CE2">CE2</option>
                          <option value="CM1">CM1</option>
                          <option value="CM2">CM2</option>
                          <option value="-----------">----------</option>
                          <option value="6e">6e</option>
                          <option value="5e">5e</option>
                          <option value="4e">4e</option>
                          <option value="3e">3e</option>
                          <option value="----------">----------</option>
                          <option value="2de">2de</option>
                          <option value="1re L">1re L</option>
                          <option value="1re ES">1re ES</option>
                          <option value="1re S">1re S</option>
                          <option value="Tle L">Tle L</option>
                          <option value="Tle ES">Tle ES</option>
                          <option value="Tle S">Tle S</option>
                          <option value="----------">----------</option>
                          <option value="1re STD2A">1re STD2A</option>
                          <option value="1re ST2S">1re ST2S</option>
                          <option value="1re STMG">1re STMG</option>
                          <option value="1re STI2D">1re STI2D</option>
                          <option value="1re STL">1re STL</option>
                          <option value="Tle STD2A">Tle STD2A</option>
                          <option value="Tle ST2S">Tle ST2S</option>
                          <option value="Tle STMG">Tle STMG</option>
                          <option value="Tle STI2D">Tle STI2D</option>
                          <option value="Tle STL">Tle STL</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="modif-matiere" class="col-sm-6 control-label">Matière(s)</label>
                      
                      <div class="col-sm-6">
                        <p class="form-control-static">Multi-matières</p>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="col-sm-6 hidden-xs"></div>
                     
<div class="col-sm-6">
                           <button type="submit" class="btn btn-primary btn-fw ">Je valide <i class="icon-ok"></i></button>
                      </div>
                    </div>
                 <ul><li><a href="#" data-toggle="modal" data-target="#cbModal">Modifier mes coordonnées bancaires</a> </li>
                    <li><a href="#" data-toggle="modal" data-target="#raModal">Résilier mon abonnement</a></li>
                  </ul>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-offset-2 col-sm-10 col-md-offset-0 col-md-3">
          <div class="bloc-bss-step-sidebar bloc-bss-1">
            <div class="bloc-bss-produit-titre text-center">Accéder à la plateforme</div>
            <div class="text-center"><a type="button" class="btn   btn-primary ">Connexion<i class="icon-angle-right"></i></a></div>
          </div>
        </div>
      </div>
      <hr>
      <div id="abo-en-cours" class="abo-en-cours clearfix">
        <div class="col-sm-2 col-md-2"> <img src="img/icone-cible-300-violet.png" class="img-responsive"> </div>
        <div class="col-sm-10 col-md-7">
          <h3 class="h3">Formule Ciblée <span>valable jusqu'au 00/00/0000</span></h3>
          <!--<p class="nombre-enfant"><strong>X enfants</strong></p>-->
          <ul class="abonnement-recap">
            <li class="enfant-recap">Julien révise en Français - CM2</li>
          </ul>
          <div class="panel-group" id="gestion-abonnement-2" role="tablist" aria-multiselectable="true">
            <div class="menu-panel "> <a role="button" data-toggle="collapse" data-parent="#gestion-abonnement-2" href="#details-abo-2" aria-expanded="true" aria-controls="details-abo" class="btn btn-default collapsed"> <i class="icon-search"></i> Voir les détails </a> <a role="button" data-toggle="collapse" data-parent="#gestion-abonnement-2" href="#modifier-abo-2" aria-expanded="false" aria-controls="modifier-abo" class="btn btn-default collapsed"> <i class="icon-pencil"></i> Modifier </a></div>
            <div class="panel ">
              <div id="details-abo-2" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                <div class="bloc-bss-1">
                  <p class="h4">Offre d'abonnement :</p>
                  <ul>
                  <li>Formule : <strong>Ciblée</strong></li>
                    <li>Date de début : <strong>le 00/00/0000</strong> </li>
                    <li>Date de fin : <strong>le 00/00/0000</strong> avec tacite reconduction</li>
                    <li>Numéro de commande : <strong>[XXX-XXX-XXX]</strong></li>
                    <li>Prix TTC : <strong>[XX, XX] /€ </strong>pour les trois premiers mois puis<strong> [XX, XX] € </strong>tous les 3 mois.</li>
                    <li>Prochain prélèvement : <strong>le 00/00/0000</strong></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="panel ">
              <div id="modifier-abo-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="bloc-bss-1">
                  <p class="h4">Modifier l'abonnement</p>
                  <form class="form-horizontal" id="modif-abonnement-3">
                    <div class="form-group">
                      <label for="modif-enfant" class="col-sm-6 control-label">Je modifie l'abonnement de </label>
                      <div class="col-sm-6">
                        <p class="form-control-static">Léa</p>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="modif-classe" class="col-sm-6 control-label">Classe</label>
                      <div class="col-sm-6">
                        <select name="modif-classe" class="form-control" id="modif-classe" placeholder="Classe">
                          <option value="CP">CP</option>
                          <option value="CE1">CE1</option>
                          <option value="CE2">CE2</option>
                          <option value="CM1">CM1</option>
                          <option value="CM2">CM2</option>
                          <option value="-----------">----------</option>
                          <option value="6e">6e</option>
                          <option value="5e">5e</option>
                          <option value="4e">4e</option>
                          <option value="3e">3e</option>
                          <option value="----------">----------</option>
                          <option value="2de">2de</option>
                          <option value="1re L">1re L</option>
                          <option value="1re ES">1re ES</option>
                          <option value="1re S">1re S</option>
                          <option value="Tle L">Tle L</option>
                          <option value="Tle ES">Tle ES</option>
                          <option value="Tle S">Tle S</option>
                          <option value="----------">----------</option>
                          <option value="1re STD2A">1re STD2A</option>
                          <option value="1re ST2S">1re ST2S</option>
                          <option value="1re STMG">1re STMG</option>
                          <option value="1re STI2D">1re STI2D</option>
                          <option value="1re STL">1re STL</option>
                          <option value="Tle STD2A">Tle STD2A</option>
                          <option value="Tle ST2S">Tle ST2S</option>
                          <option value="Tle STMG">Tle STMG</option>
                          <option value="Tle STI2D">Tle STI2D</option>
                          <option value="Tle STL">Tle STL</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="modif-matiere" class="col-sm-6 control-label">Matière</label>
                      <div class="col-sm-6">
                        <select name="modif-matiere" class="form-control" id="modif-matiere" placeholder="Matiere">
                          <option value="Français">Français</option>
                          <option value="Maths">Maths</option>
                          <option value="Anglais">Anglais</option>
                          <option value="Hist/Géo">Hist/Géo</option>
                          <option value="SVT">SVT</option>
                          <option value="Phys/Chim">Phys/Chim</option>
                          <option value="SES">SES</option>
                          <option value="Sciences">Sciences</option>
                          <option value="Philo">Philo</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-6 hidden-xs"></div>
                     
<div class="col-sm-6">
                           <button type="submit" class="btn btn-primary btn-fw ">Je valide <i class="icon-ok"></i></button>
                      </div>
                    </div>
                   <ul><li><a href="#" data-toggle="modal" data-target="#cbModal">Modifier mes coordonnées bancaires</a> </li>
                    <li><a href="#" data-toggle="modal" data-target="#raModal">Résilier mon abonnement</a></li>
                  </ul>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-offset-2 col-sm-10 col-md-offset-0 col-md-3">
          <div class="bloc-bss-step-sidebar bloc-bss-1">
            <div class="bloc-bss-produit-titre text-center">Accéder à la plateforme</div>
            <div class="text-center"><a type="button" class="btn  btn-primary ">Connexion<i class="icon-angle-right"></i></a></div>
          </div>
        </div>
      </div>
      <hr>
      <h2 class="">Abonnement(s) terminé(s) :</h2>
      <hr>
      <div id="abo-en-cours" class="abo-en-cours clearfix">
        <div class="col-sm-2 col-md-2"> <img src="img/icone-cible-300-violet.png" class="img-responsive"> </div>
        <div class="col-sm-10 col-md-7">
          <h3 class="h3">Formule Ciblée <span>valable jusqu'au 00/00/0000</span></h3>
         <!-- <p class="nombre-enfant"><strong>X enfants</strong></p>-->
          <ul class="abonnement-recap">
            <li class="enfant-recap">Julien révise en Français - CM2</li>
          </ul>
          <p><a href="#details-abo-2" class="btn btn-primary "> <i class="icon-arrows-cw"></i> Je me réabonne </a> </p>
        </div>
      </div>
      <hr>
      <div id="abo-en-cours" class="abo-en-cours clearfix">
        <div class="col-sm-2 col-md-2"> <img src="img/icone-reussite-300-verteau.png" class="img-responsive"> </div>
        <div class="col-sm-10 col-md-7">
          <h3 class="h3">Formule Réussite <span>valable jusqu'au 00/00/0000</span></h3>
          <!--<p class="nombre-enfant"><strong>X enfants</strong></p>-->
          <ul class="abonnement-recap">
            <li class="enfant-recap">Julien révise en CM2</li>
          </ul>
          <p><a href="#details-abo-2" class="btn btn-primary "> <i class="icon-arrows-cw"></i> Je me réabonne </a> </p>
        </div>
      </div>
      <hr>
      <div id="abo-en-cours" class="abo-en-cours clearfix">
        <div class="col-sm-2 col-md-2"> <img src="img/icone-tribu-300-vert.png" class="img-responsive"> </div>
        <div class="col-sm-10 col-md-7">
          <h3 class="h3">Formule Tribu <span>valable jusqu'au 00/00/0000</span></h3>
          <!--<p class="nombre-enfant"><strong>X enfants</strong></p>-->
          <ul class="abonnement-recap">
            <li class="enfant-recap">Julien révise en CM2</li>
            <li class="enfant-recap">Léa révise en CM1</li>
            <li class="enfant-recap">Mathilde révise CE2</li>
          </ul>
          <p><a href="#details-abo-2" class="btn btn-primary "> <i class="icon-arrows-cw"></i> Je me réabonne </a> </p>
        </div>
      </div>
    </div>
  </div>

</div>



