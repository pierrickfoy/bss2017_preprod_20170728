<div class="bss-section bloc-section-bleu bss-gestion-abonnement">
  <div class="container">
    <div class="row">
    <div class="col-md-12">
      <h1 class="h1">Gérer mon profil</h1>
      <h2 class="">Vos informations personnelles</h2></div>
      <div class="col-md-8 col-md-offset-2 ">
        <form class="form-horizontal clearfix" id="gestion-profil" title="Gérer mon profil">
        <div class="bloc-bss-1 clearfix">
          <div class="form-group">
            <label for="identifiant" class="col-sm-6 control-label">Vous êtes identifié en tant que : </label>
            <div class="col-sm-6">
              <p class="form-control-static ">Hélène Gonzalez-Quijano</p>
            </div>
          </div>
          <div class="form-group ">
            <label for="numero-client" class="col-sm-6 control-label">Numéro client</label>
            <div class="col-sm-6">
              <p class="form-control-static ">999-999-999</p>
            </div>
          </div>
          <hr>
          <h4>Mes coordonnées</h4>
          <div class="form-group">
            <label for="civilite" class="col-sm-6 control-label">Civilité</label>
            <div class="col-sm-6">
              <label class="radio-inline">
                <input name="civilite" type="radio" id="Mme" form="gestion-profil" value="Mme" checked="checked">
                Mme </label>
              <label class="radio-inline">
                <input name="civilite" type="radio" id="Mr" form="gestion-profil" value="Mr">
                Mr </label>
            </div>
          </div>
          <div class="form-group ">
            <label for="votre-nom" class="col-sm-6 control-label">Votre nom</label>
            <div class="col-sm-6">
              <input name="votre-nom" type="text" class="form-control" id="votre-nom" form="gestion-profil" placeholder="Votre nom" title="Votre nom" value="Gonzalez-Quijano">
            </div>
          </div>
          <div class="form-group">
            <label for="votre-prenom" class="col-sm-6 control-label">Votre prénom</label>
            <div class="col-sm-6">
              <input name="votre-prenom" type="text" class="form-control" id="votre-prenom" form="gestion-profil" placeholder="Votre prénom" title="Votre prénom" value="Hélène">
            </div>
          </div>
          <div class="form-group">
            <label for="votre-email" class="col-sm-6 control-label">Email</label>
            <div class="col-sm-6">
              <input name="votre-email" type="text" class="form-control" id="votre-email" form="gestion-profil" placeholder="Votre email" title="Votre email" value="HGONZALEZ-QUIJANO@sejer.fr">
            </div>
          </div>
          <div class="form-group">
            <label for="confirmation-email" class="col-sm-6 control-label">Confirmation email</label>
            <div class="col-sm-6">
              <input name="confirmation-email" type="text" class="form-control" id="confirmation-email" form="gestion-profil" placeholder="Confirmation email" title="Confirmation email" value="HGONZALEZ-QUIJANO@sejer.fr">
            </div>
          </div>
          <hr>
          <h4>Modifier votre mot de passe</h4>
          <div class="form-group">
            <label for="ancien-mot-de-passe" class="col-sm-6 control-label">Mot de passe actuel</label>
            <div class="col-sm-6">
              <input name="ancien-mot-de-passe" type="password" class="form-control" id="ancien-mot-de-passe" form="gestion-profil" placeholder="Mot de passe actuel" title="Mot de passe" value="motdepasse">
            </div>
          </div>
          <div class="form-group">
            <label for="nouveau-mot-de-passe" class="col-sm-6 control-label">Nouveau mot de passe</label>
            <div class="col-sm-6">
              <input name="nouveau-mot-de-passe" type="password" class="form-control" id="nouveau-mot-de-passe" form="gestion-profil" placeholder="Nouveau mot de passe" title="Mot de passe">
            </div>
          </div>
          <div class="form-group">
            <label for="confirm-nouveau-mot-de-passe" class="col-sm-6 control-label">Confirmation du nouveau mot de passe</label>
            <div class="col-sm-6">
              <input name="confirm-nouveau-mot-de-passe" type="password" class="form-control" id="confirm-nouveau-mot-de-passe" form="gestion-profil" placeholder="Confirmation du mot de passe" title="Confirmation du nouveau mot de passe">
            </div>
          </div>
          <hr>
          <div class=" text-center ">
            <button type="cancel" class="btn btn-danger">Annuler</button>
            <button type="submit" class="btn btn-primary">Modifier <i class="icon-ok"></i></button>
          </div>
          <hr>
          </div>
         <div class="bloc-bss-1 clearfix">
          <h4>Votre adresse</h4>
          <div class="form-group">
            <label for="pays" class="col-sm-6 control-label">Pays</label>
            <div class="col-sm-6">
              <select name="pays" class="form-control" id="pays" form="gestion-profil" title="Pays">
                <option>Choisir</option>
                <option selected="selected">France</option>
                <option>Belgique</option>
                <option>....</option>
              </select>
            </div>
          </div>
          <div class="form-group " >
            <label for="code-postal" class="col-sm-6 control-label">Code postal</label>
            <div class="col-sm-3">
              <input name="code-postal" type="text" class="form-control" id="code-postal" form="gestion-profil" placeholder="Code postal" title="Code postal" value="75013 ">
            </div>
          </div>
          <div class="form-group">
            <label for="Ville" class="col-sm-6 control-label">Ville</label>
            <div class="col-sm-6">
              <input name="Ville" type="text" class="form-control" id="Ville" form="gestion-profil" placeholder="Ville" title="Ville" value="Paris">
            </div>
          </div>
           <div class="form-group " >
              <label for="voie" class="col-sm-6 control-label">Adresse</label>
              <div class="col-sm-6">
              <div class="row">
              <div class="col-sm-4">
              <input name="Adresse" type="text" class="form-control" id="voie" form="gestion-profil" placeholder="Numéro de voie" title="Numéro de voie" value="25">
              </div>
              <div class="col-sm-8"><input name="type-voie" type="text" class="form-control" id="type-voie" form="gestion-profil" placeholder="Type de voie" title="Type de voie" value="avenue">
               </div>
              </div>
              </div>
          </div>
           
            <div class="form-group " >
              <label for="nom-voie" class="col-sm-6 control-label">&nbsp;</label>
              <div class="col-sm-6">
              
        
                <input name="Adresse" type="text" class="form-control" id="nom-voie" form="gestion-profil" placeholder="Nom de la voie" title="Nom de la voie" value="Pierre de Coubertin">
              </div>
            </div>
          <div class="form-group " >
            <label for="adresse-complementaire" class="col-sm-6 control-label">Adresse complémentaire</label>
            <div class="col-sm-6">
              <input name="adresse-complementaire" type="text" class="form-control" id="adresse-complementaire" form="gestion-profil" placeholder="Adresse complémentaire" title="Adresse complémentaire">
            </div>
          </div>
          
          
          
          <hr>
          <div class="col-sm-12 text-center ">
            <button type="cancel" class="btn btn-danger">Annuler</button>
            <button type="submit" class="btn btn-primary">Modifier <i class="icon-ok"></i></button>
          </div>
         </div>
        </form>
      </div>
    </div>
  </div>
</div>
