<div class="bss-section bloc-section-gris bss-actualite">
  <div class="container"> 
    <!--<h1 id="tagline" class="h1">Autres</h1>-->
    <div id="articles" class="row">
      <div class="col-sm-6 col-md-8">
        <div class="row">
          <div class="item col-sm-12 col-md-6">
            <div class="bloc-bss-actu home">
              <div class="visuel-actu"><a href="actualite.php"><img src="img/logo-bss.jpg" class="img-responsive" border="0"></a></div>
              <div class="info-actu ">
                <h3 class="titre-actu"><a href="actualite.php">L’actualité du soutien scolaire</a> </h3>
                <div class="excerpt">Lorem ipsum dol. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu</div>
                <hr class="separator">
                <div class="toute-actu text-center"><a href="actualite.php" class="btn  btn-fw btn-primary  ">Lire toute l'actualité<i class="icon-angle-right"></i> </a></div>
              </div>
            </div>
          </div>
          <div class="item  col-sm-12 col-md-6">
            <div class="bloc-bss-actu home">
              <div class="visuel-actu"><a href="actualite-single.php"><img src="img/750x400.jpg" class="img-responsive"></a></div>
                <div class="info-actu ">
              <h3 class="titre-actu"><a href="actualite-single.php">Vous cherchez un professeur pour des cours particuliers ?</a></h3>
              <div class="excerpt">Bordas Soutien scolaire vous propose également un service de cours à domicile assuré par des enseignants de l'Éducation Nationale, disponibles partout en France.</div>
              <hr class="separator">
              <div class="toute-actu text-center"><a href="#" class="btn  btn-fw btn-primary  ">Trouver un professeur<i class="icon-angle-right"></i> </a></div>  </div>
            </div>
          </div>
        </div>
      </div>
    
  <div class="item  col-sm-6 col-md-3">
       <div class="bloc-annonce"><div class="prepend"><b class="caret"></b> publicité <b class="caret"></b></div><a href="#"><img src="http://www.bordas-soutien-scolaire.com/images/bss-wengo-300.gif"  class="img-responsive"></a></div>
      </div></div>
  </div>
</div>
</div>
