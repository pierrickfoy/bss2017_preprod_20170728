
<div class="bss-section  bloc-section-gris bss-home-qsn">
  <div class="container">
    <p id="tagline" class="h1">Qui sommes-nous ?</p>
    <h2 id="subtitle" class="h2">Un programme d'entraînement personnalisé </h2>
    <div class="row">
    
   
      <div class="col-sm-3">
        <div class=" bloc-bss-1">
          <div class="icone text-center"><img src="img/icone-multisupport.png"></div>
       
          <p class="texte  text-center"><strong>Site de soutien scolaire</strong> pour s’entraîner sur ordinateur et tablette du CP à la Terminale</p>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="bloc-bss-1">
          <div class="icone text-center"><img src="img/icone-smiley.png"></div>
          <p class="texte text-center"><strong>Révisions interactives</strong> pour retrouver motivation et plaisir d’apprendre</p>
         
        </div>
      </div>
      <div class="col-sm-3">
        <div class="bloc-bss-1">
          <div class="icone text-center"><img src="img/icone-24.png"></div>
          <p class="texte  text-center"><strong>Service disponible 24h/24 et 7j/7</strong>, avec ou sans connexion Internet</p>
          
        </div>
      </div>
      <div class="col-sm-3">
        <div class="bloc-bss-1">
          <div class="icone text-center"><img src="img/icone-enseignant.png"></div>
          <p class="texte  text-center"><strong>Conçu par des enseignants</strong> et <strong>conforme aux programmes</strong> de l’Éducation nationale</p>
          
        </div>
      </div>

    
    
    
      <!--<div class="col-sm-6 col-md-4">
        <div class="media">
          <div class="media-left"> <img src="http://placehold.it/80x80"> </div>
          <div class="media-body">
            <div class="h3 ">La garantie de contenus de qualité</div>
            <p class="">Les causes et les remèdes aux difficultés scolaires des enfants et adolescents.</p>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="media">
          <div class="media-left"> <img src="http://placehold.it/80x80"> </div>
          <div class="media-body">
            <div class="h3 ">La garantie de contenus de qualité</div>
            <p class="">Les causes et les remèdes aux difficultés scolaires des enfants et adolescents.</p>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="media">
          <div class="media-left"> <img src="http://placehold.it/80x80"> </div>
          <div class="media-body">
            <div class="h3 ">La garantie de contenus de qualité</div>
            <p class="">Les causes et les remèdes aux difficultés scolaires des enfants et adolescents.</p>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="media">
          <div class="media-left"> <img src="http://placehold.it/80x80"> </div>
          <div class="media-body">
            <div class="h3 ">La garantie de contenus de qualité</div>
            <p class="">Les causes et les remèdes aux difficultés scolaires des enfants et adolescents.</p>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="media">
          <div class="media-left"> <img src="http://placehold.it/80x80"> </div>
          <div class="media-body">
            <div class="h3 ">La garantie de contenus de qualité</div>
            <p class="">Les causes et les remèdes aux difficultés scolaires des enfants et adolescents.</p>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="media">
          <div class="media-left"> <img src="http://placehold.it/80x80"> </div>
          <div class="media-body">
            <div class="h3 ">La garantie de contenus de qualité</div>
            <p class="">Les causes et les remèdes aux difficultés scolaires des enfants et adolescents.</p>
          </div>
        </div>
      </div>-->
    </div>
    <hr>
    <div class="text-center"><a class="btn btn-md btn-primary" href="qui-sommes-nous.php">Découvrir les engagements Bordas<i class="icon-angle-right"></i></a> </div>
  </div>
</div>
