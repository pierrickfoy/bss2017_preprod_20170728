
<div class="bss-hero bss-programme">
  <!--<div class="bss-slider"><img src="img/slide2.jpg"  alt="" class="img-responsive"/></div>-->
  <div class="container">
    <div class="bss-intro-area ">
      <h1 id="tagline" class="h1">Soutien scolaire en ligne</h1>
      <h2 id="subtitle" class="h2">Le seul site conforme aux programmes du CP à la Terminale.</h2>
      <!--<a href="/" class="btn hide-sm btn-contrast btn-large btn-semi-transparent js-show-how-it-works"> En savoir plus ?</a>-->
      
      <div class="bss-search-form">
        <div class="container">
          <div class="row ">
            <div class="col-md-10 col-md-offset-1">
              <form class="form-horizontal" action="tunnel-1.php">
                <div class="form-group">
                  <label for="choix-classe" class="sr-only">Classe</label>
                  <div class="col-sm-4">
                    <select  name="choix-classe" class="form-control" id="choix-classe" placeholder="Classe" >
                    <option value="Classe" selected="selected">Classe</option>
                     <option value="----------">----------</option>
                      <option value="CP">CP</option>
                      <option value="CE1">CE1</option>
                      <option value="CE2">CE2</option>
                      <option value="CM1">CM1</option>
                      <option value="CM2">CM2</option>
                      <option value="-----------">----------</option>
                      <option value="6e">6e</option>
                      <option value="5e">5e</option>
                      <option value="4e">4e</option>
                      <option value="3e">3e</option>
                      <option value="----------">----------</option>
                      <option value="2de">2de</option>
                      <option value="1re L">1re L</option>
                      <option value="1re ES">1re ES</option>
                      <option value="1re S">1re S</option>
                      <option value="Tle L">Tle L</option>
                      <option value="Tle ES">Tle ES</option>
                      <option value="Tle S">Tle S</option>
                      <option value="----------">----------</option>
                      <option value="1re STD2A">1re STD2A</option>
                      <option value="1re ST2S">1re ST2S</option>
                      <option value="1re STMG">1re STMG</option>
                      <option value="1re STI2D">1re STI2D</option>
                      <option value="1re STL">1re STL</option>
                      <option value="Tle STD2A">Tle STD2A</option>
                      <option value="Tle ST2S">Tle ST2S</option>
                      <option value="Tle STMG">Tle STMG</option>
                      <option value="Tle STI2D">Tle STI2D</option>
                      <option value="Tle STL">Tle STL</option>
                    </select>
                  </div>
                  <label for="choix-matiere" class="sr-only">Matière</label>
                  <div class="col-sm-4">
                    <select  name="choix-matiere" class="form-control" id="choix-matiere" placeholder="Matiere" >
                    <option value="Matière" selected="selected">Matière</option>
                     <option value="----------">----------</option>
                      <option value="Français">Français</option>
                      <option value="Maths">Maths</option>
                      <option value="Anglais">Anglais</option>
                      <option value="Hist/Géo">Hist/Géo</option>
                      <option value="SVT">SVT</option>
                      <option value="Phys/Chim">Phys/Chim</option>
                      <option value="SES">SES</option>
                      <option value="Sciences">Sciences</option>
                      <option value="Philo">Philo</option>
                    </select>
                  </div>
                  <div class="col-xs-12 col-sm-4 ">
                    <button type="submit" class="btn btn-search btn-fw">Je cherche une formule pour mon enfant</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
