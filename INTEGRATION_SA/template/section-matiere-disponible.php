<div class="bss-section bloc-section-orange bss-matiere">
  <div class="container">
    <h1 id="tagline" class="h1">Les matières disponibles</h1>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-demo">
            <div class="item-matiere " role="tab" id="">
              <div class="bloc-matiere-item">
                <div class="col-xs-11 col-sm-11 col-md-5 no-padding"> <a href="#" class="align-left"><img src="img/icone-matiere.jpg" class="img-responsive"></a>
                  <h2 class=""><a href="#">Soutien scolaire en ligne en [MATIERE] en [CLASSE] </a></h2>
                </div>
                <ul class="list-inline col-sm-4 col-md-5 no-padding hidden-xs hidden-sm">
                  <li><i class="icon-book"></i><span>Cours</span></li>
                  <li><i class="icon-docs"></i><span>Exercices</span></li>
                  <li><i class="icon-doc-text"></i><span>Corrigés</span></li>
                  <li><i class="icon-coverflow"></i><span>Animations</span></li>
                  <li><i class="icon-movie"></i><span>Vidéos</span></li>
                  <li><i class="icon-stopwatch"></i><span>Examen</span></li>
                </ul>
                <div class="col-xs-1 col-sm-1 col-md-2 no-padding text-center col-voir-demo"> <a  class="btn  btn-primary  hidden-md hidden-lg" role="button" data-toggle="collapse" data-parent="#accordion" href="#demo-1" aria-expanded="true" aria-controls="demo-1">></a> <a  class="btn   btn-primary hidden-xs hidden-sm btn-fw" role="button" data-toggle="collapse" data-parent="#accordion" href="#demo-1" aria-expanded="true" aria-controls="demo-1">Voir une démo<i class="icon-angle-right"></i></a> </div>
              </div>
            </div>
            <div id="demo-1" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="http://applets.editions-bordas.fr/demo/3133097368542"></iframe>
                </div>
              </div>
            </div>
            <div class="item-matiere " role="tab" id="">
              <div class="bloc-matiere-item">
                <div class="col-xs-11 col-sm-11 col-md-5 no-padding"> <a href="#" class="align-left"><img src="img/icone-matiere.jpg" class="img-responsive"></a>
                  <h2 class=""><a href="#">Soutien scolaire en ligne en [MATIERE] en [CLASSE] </a></h2>
                </div>
                <ul class="list-inline col-sm-4 col-md-5 no-padding hidden-xs hidden-sm">
                  <li><i class="icon-book"></i><span>Cours</span></li>
                  <li><i class="icon-docs"></i><span>Exercices</span></li>
                  <li><i class="icon-doc-text"></i><span>Corrigés</span></li>
                  <li><i class="icon-coverflow"></i><span>Animations</span></li>
                  <li><i class="icon-movie"></i><span>Vidéos</span></li>
                  <li><i class="icon-stopwatch"></i><span>Examen</span></li>
                </ul>
                <div class="col-xs-1 col-sm-1 col-md-2 no-padding text-center"> <a  class="btn  btn-primary  hidden-md hidden-lg" role="button" data-toggle="collapse" data-parent="#accordion" href="#demo-2" aria-expanded="true" aria-controls="demo-2">></a> <a  class="btn   btn-primary hidden-xs hidden-sm btn-fw" role="button" data-toggle="collapse" data-parent="#accordion" href="#demo-2" aria-expanded="true" aria-controls="demo-2">Voir une démo<i class="icon-angle-right"></i></a> </div>
              </div>
            </div>
            <div id="demo-2" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="http://applets.editions-bordas.fr/demo/3133097368542"></iframe>
                </div>
              </div>
            </div>
            <div class="item-matiere " role="tab" id="">
              <div class="bloc-matiere-item">
                <div class="col-xs-11 col-sm-11 col-md-5 no-padding"> <a href="#" class="align-left"><img src="img/icone-matiere.jpg" class="img-responsive"></a>
                  <h2 class=""><a href="#">Soutien scolaire en ligne en [MATIERE] en [CLASSE] </a></h2>
                </div>
                <ul class="list-inline col-sm-4 col-md-5 no-padding hidden-xs hidden-sm">
                  <li><i class="icon-book"></i><span>Cours</span></li>
                  <li><i class="icon-docs"></i><span>Exercices</span></li>
                  <li><i class="icon-doc-text"></i><span>Corrigés</span></li>
                  <li><i class="icon-coverflow"></i><span>Animations</span></li>
                  <li><i class="icon-movie"></i><span>Vidéos</span></li>
                  <li><i class="icon-stopwatch"></i><span>Examen</span></li>
                </ul>
                <div class="col-xs-1 col-sm-1 col-md-2 no-padding text-center"> <a  class="btn  btn-primary  hidden-md hidden-lg" role="button" data-toggle="collapse" data-parent="#accordion" href="#demo-3" aria-expanded="true" aria-controls="demo-3">></a> <a  class="btn   btn-primary hidden-xs hidden-sm btn-fw" role="button" data-toggle="collapse" data-parent="#accordion" href="#demo-3" aria-expanded="true" aria-controls="demo-3">Voir une démo<i class="icon-angle-right"></i></a> </div>
              </div>
            </div>
            <div id="demo-3" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="http://applets.editions-bordas.fr/demo/3133097368542"></iframe>
                </div>
              </div>
            </div>
            <div class="item-matiere " role="tab" id="">
              <div class="bloc-matiere-item">
                <div class="col-xs-11 col-sm-11 col-md-5 no-padding"> <a href="#" class="align-left"><img src="img/icone-matiere.jpg" class="img-responsive"></a>
                  <h2 class=""><a href="#">Soutien scolaire en ligne en [MATIERE] en [CLASSE] </a></h2>
                </div>
                <ul class="list-inline col-sm-4 col-md-5 no-padding hidden-xs hidden-sm">
                  <li><i class="icon-book"></i><span>Cours</span></li>
                  <li><i class="icon-docs"></i><span>Exercices</span></li>
                  <li><i class="icon-doc-text"></i><span>Corrigés</span></li>
                  <li><i class="icon-coverflow"></i><span>Animations</span></li>
                  <li><i class="icon-movie"></i><span>Vidéos</span></li>
                  <li><i class="icon-stopwatch"></i><span>Examen</span></li>
                </ul>
                <div class="col-xs-1 col-sm-1 col-md-2 no-padding text-center"> <a  class="btn  btn-primary  hidden-md hidden-lg" role="button" data-toggle="collapse" data-parent="#accordion" href="#demo-4" aria-expanded="true" aria-controls="demo-4">></a> <a  class="btn   btn-primary hidden-xs hidden-sm btn-fw" role="button" data-toggle="collapse" data-parent="#accordion" href="#demo-4" aria-expanded="true" aria-controls="demo-4">Voir une démo<i class="icon-angle-right"></i></a> </div>
              </div>
            </div>
            <div id="demo-4" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="http://applets.editions-bordas.fr/demo/3133097368542"></iframe>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
