<div class="bss-section bloc-section-bleu bss-newsletter">
  <div class="container">
    <div class="row">
      <div class=" col-sm-12 col-md-6">
        <div class="media">
          <div class="media-left"> <img src="img/icone-newsletter-bleu.png" > </div>
          <div class="media-body">
            <div class="h1  ">Vous souhaitez ...</div>
            <ul>
              <li>Recevoir notre documentation ?</li>
              <li>Bénéficier de nos offres spéciales ?</li>
              <li>Être tenu informé de nos actualités ?</li>
            </ul>
          </div>
        </div>
      </div>
      <div class=" col-sm-12 col-md-6">
        <form class="text-right" id="form-newsletter">
          <div class="input-group  ">
            <input type="text" class="form-control " placeholder="Mon adresse email">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button">Je m'inscris à la newsletter</button>
            </span> </div>
          <!-- /input-group -->
        </form>
      </div>
      
    </div>
  </div>
</div>
