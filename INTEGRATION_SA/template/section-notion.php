<div class="bss-section bloc-section-bleu bss-notion">
  <div class="container">
    <div class="row">
      <div class=" text-center">
        <h1 id="tagline" class="h1">Les fonctions [CLASSE]</h1>
      </div>
      <div class="col-sm-12 col-md-8 ">
        <h2>Contenu du chapitre : </h2>
        <p> Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. </p>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. </p>
        <h2>Objectifs pédagogiques :</h2>
        <p> In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>
        <p>Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. 
          
          Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. </p>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. </p>
        <p ><a href="#">Aenean massa.</a> Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.</p>
      </div>
      <div class="col-sm-12 col-md-4  hidden-xs">
        <div class="bloc-annonce"><div class="prepend"><b class="caret"></b> publicité <b class="caret"></b></div><a href="#"><img src="http://www.bordas-soutien-scolaire.com/images/bss-wengo-300.gif" class="img-responsive"></a></div>
        <div class="bss-share">
          <ul class="list-inline">
            <li><a href="#" class="share-facebook"><i class="icon-facebook"></i></a><span>Partager</span></li>
            <li><a href="#" class="share-twitter"><i class="icon-twitter"></i></a><span>Twitter</span></li>
            <li><a href="#" class="share-google"><i class="icon-gplus"></i></a><span>Partager</span></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
