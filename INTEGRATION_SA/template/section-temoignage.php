<div class="bss-section bloc-section-violet bss-temoin">
  <div class="container">
    <div class="row">
      <h1 id="tagline" >Derniers témoignages</h1>
      <div id="carousel-temoins" class="owl-carousel owl-theme">
        <div class="item">
          <div class="texte-temoin">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et <em class="auteur">Maran 07/2014</em></p>
          </div>
        </div>
        <div class="item">
          <div class="texte-temoin">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et<em class="auteur">Dradra51 09/2014</em></p>
          </div>
        </div>
        <div class="item">
          <div class="texte-temoin">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et <em class="auteur">Poppy 03/2015</em></p>
          </div>
        </div>
        <div class="item">
          <div class="texte-temoin">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et <em class="auteur">Laetitia et Vincent R. 02/2015</em></p>
          </div>
        </div>
      </div>
      <div class="avis text-center"><a href="#" class="btn  btn-info" data-toggle="modal" data-target="#avisModal"><i class='icon-chat'></i> Donnez votre Avis</a></div>
    </div>
  </div>
</div>
