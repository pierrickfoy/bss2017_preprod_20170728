
<div class="bss-section  bloc-section-gris-bleu bss-test-produit" id="test-demo">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="h1">Tester Bordas Soutien scolaire</h1>
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="http://applets.editions-bordas.fr/demo/3133097368542"></iframe>
        </div>
        <hr>
        <p class="text-center"><a class="btn btn-md  btn-primary ancre" href="#totop">Je m'abonne<i class="icon-angle-right"></i></a></p>
      </div>
    </div>
  </div>
</div>
