<div class="bss-section bloc-section-blanc bss-video">
  <div class="container">
    <div class="row">
      <div class="col-md-6 ">
        <p class="h1">Comment ça marche ?</p>
         <h2 id="subtitle" class="h2">Des cours et des exercices interactifs pour réviser, s’entraîner... et progresser !</h2>
      
      </div>
      <div class="col-md-6 text-center"> 
        <!-- 16:9 aspect ratio -->
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/9Va3KEari7I?rel=0"></iframe>
        </div>
       
      </div>
       <div class="col-md-12 text-center"> <hr>
          <p class=""><a class="btn btn-info btn-md" href="#">Découvrir le service Bordas Soutien scolaire <i class="icon-angle-right"></i></a></p> </div>
    </div>
  </div>
</div>
