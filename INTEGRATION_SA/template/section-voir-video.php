
<div class="bss-section bloc-section-gris-orange bss-video-produit" id="voir-video">
  <div class="container">
    <div class="row">
    <div class="col-md-12">
        <h1 class="h1">Voir la vidéo de démonstration</h1></div>
      <div class="col-md-8 col-md-offset-2 ">
      
        <!-- 16:9 aspect ratio -->
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/9Va3KEari7I?rel=0"></iframe>
        </div>
        <hr>
        <p class="text-center"><a class="btn btn-md  btn-primary ancre" href="#totop">Je m'abonne<i class="icon-angle-right"></i></a></p>
      </div>
    </div>
  </div>
</div>
