jQuery(document).ready(function () {

    jQuery("[rel='tooltip']").tooltip();

    // hide #back-top first
    jQuery("#back-top").hide();

    // fade in #back-top
    jQuery(function () {
        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 100) {
                jQuery('#back-top').fadeIn();
            } else {
                jQuery('#back-top').fadeOut();
            }
        });

        // scroll body to 0px on click
        jQuery('#back-top a').click(function () {
            jQuery('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });

});
jQuery(function () {
    jQuery('audio').audioPlayer();
});
jQuery('[data-spy="scroll"]').each(function () {
    var jQueryspy = jQuery(this).scrollspy('refresh')
});
! function (jQuery) {
    jQuery(function () {

        var jQueryroot = jQuery('html, body');

        jQuery('a.smoothscrol').click(function () {
            var href = jQuery.attr(this, 'href');
            jQueryroot.animate({
                scrollTop: jQuery(href).offset().top
            }, 300, function () {
                window.location.hash = href;
            });
            return false;
        });
    })
}(window.jQuery)

var menuLeft = document.getElementById('cbp-spmenu-s1'),
    showLeftPush = document.getElementById('showLeftPush'),
    body = document.body;



showLeftPush.onclick = function () {
    classie.toggle(this, 'active');
    classie.toggle(body, 'cbp-spmenu-push-toright');
    classie.toggle(menuLeft, 'cbp-spmenu-open');
    disableOther('showLeftPush');
};

function disableOther(button) {


    if (button !== 'showLeftPush') {
        classie.toggle(showLeftPush, 'disabled');
    }

}