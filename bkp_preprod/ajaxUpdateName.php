<?php 
session_start();
    define( 'ROOT', dirname( dirname( __FILE__ ) ));
	$_CONST['path']["server"] = ROOT;
	
	include_once(ROOT."/lib/config.inc.php");
	include_once(ROOT."/lib/database.class.php");
	
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	$where = "";
	
	if(isset($_POST) && $_POST != ""){
		$_SESSION['products']['enfants'][0]['duree_engagement'] = $_POST['modification_duree'];
		for($i=0 ;$i<$_POST['nb_enfants'];$i++){
			$fetch = (bool) strlen(trim($_POST['prenoms'][$i]));
			if($fetch === false ){
				$_POST['prenoms'][$i] = "ENFANT ".$i;
			}else{
			$_SESSION["products"]["enfants"][$i]["D_PRENOM"] = $_POST['prenoms'][$i];
			}
		}
		$return = true;
		echo json_encode($return);
	}
	
	else return false;