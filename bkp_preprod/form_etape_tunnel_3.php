<?php
include_once(ROOT . "/lib/bill.class.php");
$compteur_erreur_cmd;
if( !isset($_SESSION['products']['enfants'][0]['duree_engagement'])){
			echo "<script>document.location.href='".( ($_CONST['TYPE_ENVIRONNEMENT'] !='dev' && $_CONST['TYPE_ENVIRONNEMENT'] !='preprod' && $_CONST['TYPE_ENVIRONNEMENT'] !='prod' ) ? $_CONST["HTTPS"] : $_CONST["URL2"]).$_CONST['URL_ACCUEIL']."comment-s-abonner.html';</script>";
		exit;
}


//Récupération du prix en fonction du produit 
$strSql = "SELECT * FROM bor_produit p INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix) WHERE p.produit_ean = '".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["ean"])."'"; 
$aProduct = $oDb->queryRow($strSql);

// calcul prix, duree, sans code promo
$iDe = $_SESSION['products']['enfants'][0]['duree_engagement'];
$strSql = "SELECT * FROM bor_duree_engagement WHERE de_id = ".$iDe.""; 
$aDureeEngagement = $oDb->queryTab($strSql);
$strFormuleIdYonix = $_SESSION['cart']['formule']['formule_id_yonix']; 
$strSql = "SELECT produit_prix FROM bor_produit WHERE formule_id_yonix = '".$strFormuleIdYonix."' AND de_id_yonix = '".mysql_real_escape_string( $aDureeEngagement[0]['de_id_yonix'])."' LIMIT 1"; 
$fPrix = $oDb->queryItem($strSql) ;
$fPrixMois = round( $fPrix / $aDureeEngagement[0]['de_engagement'] , 2 ) ;

// ajout d'un code promo 
if(isset($_POST["inputpromo"]) && $_POST["inputpromo"]=="Valider" && !empty($_POST["codepromo"])){
    unset($_SESSION["CODEPROMO"]);
    unset($_SESSION["products"]["enfants"][0]['CODEPROMO']);
    unset($_SESSION["products"]["enfants"][0]['REDUCTION']);
    
    $strStep = "step_1_2"; 
    $bStep1 = true; 
    //Vérification que le coupon est actif 
    $sql = "SELECT coupon_reduction , f.de_id
            FROM bor_coupon c 
                INNER JOIN bor_coupon_formule f ON (c.coupon_id = f.coupon_id) 
																WHERE c.coupon_code ='".mysql_real_escape_string($_POST["codepromo"])."' 
                AND f.formule_id ='".$_SESSION['cart']['formule']['formule_id']."'
                AND c.coupon_debut <= NOW() 
                AND c.coupon_fin >= NOW()
            "; 
    $aCoupons = $oDb->queryTab($sql);
    if($oDb->rows > 0){
        $_SESSION["CODEPROMO"]["CODE"] = mysql_real_escape_string($_POST["codepromo"]); 
        foreach($aCoupons as $aCoupon){
            $_SESSION["CODEPROMO"]["REDUCTION"][$aCoupon["de_id"]] = $aCoupon["coupon_reduction"] ;
				if(isset($_SESSION["CODEPROMO"]["REDUCTION"][$aCoupon["de_id"]]) && $_SESSION["CODEPROMO"]["REDUCTION"][$aCoupon["de_id"]] > 0){
					$fPrix = floatval($fPrix);
					$duree_mois = intval($_SESSION["CODEPROMO"]["REDUCTION"][$aCoupon["de_id"]]);
					
					//$newPrice = number_format(round( ($fPrix - ( ($fPrix * $duree_mois) / 100)), 2), 2, ',', '' ) ; 
					$newPrice = round( ($fPrix - ( ($fPrix * $duree_mois) / 100)), 2); 

					//$strNewPriceMois = round( $strNewPrice / $aDureeEngagement[0]['de_engagement'] , 2 ) ;
					$strNewPriceMois = round( $newPrice / $aDureeEngagement[0]['de_engagement'] , 2 ) ;
					$strNewPrice = str_replace('.',',',$newPrice);
					$strNewPriceMois = str_replace('.',',',$strNewPriceMois);
					$strMsg = 'Votre code promotionnel à bien été pris en compte !';

					$_SESSION["products"]["enfants"][0]["CODEPROMO"] = $_SESSION["CODEPROMO"]["CODE"];
					$_SESSION["products"]["enfants"][0]["REDUCTION"] = $_SESSION["CODEPROMO"]["REDUCTION"][$aCoupon["de_id"]];
					
				}else
					$strNewPrice ="";
        }
    }else{
        $strStep = "step_1_2"; 
        $bStep1 = true; 
        $bStep2 = false; 
        $strError = "Votre code promotionnel est faux ou périmé."; 
    }

// si un code promo est déjà en session ( pour ne pas le perdre après validation formulaire cb par ex.)
}else if(isset($_SESSION["CODEPROMO"]["CODE"])){
				$sql = "SELECT coupon_reduction , f.de_id
            FROM bor_coupon c 
                INNER JOIN bor_coupon_formule f ON (c.coupon_id = f.coupon_id) 
																WHERE c.coupon_code ='".mysql_real_escape_string($_SESSION["CODEPROMO"]["CODE"])."' 
                AND f.formule_id ='".$_SESSION['cart']['formule']['formule_id']."'
                AND c.coupon_debut <= NOW() 
                AND c.coupon_fin >= NOW()
            "; 
    $aCoupons = $oDb->queryTab($sql);
				if($oDb->rows > 0){
        foreach($aCoupons as $aCoupon){
            $_SESSION["CODEPROMO"]["REDUCTION"][$aCoupon["de_id"]] = $aCoupon["coupon_reduction"] ;
			if(isset($_SESSION["CODEPROMO"]["REDUCTION"][$aCoupon["de_id"]]) && $_SESSION["CODEPROMO"]["REDUCTION"][$aCoupon["de_id"]] > 0){
							$fPrix = floatval($fPrix);
							$duree_mois = intval($_SESSION["CODEPROMO"]["REDUCTION"][$aCoupon["de_id"]]);
							$newPrice = round( ($fPrix - ( ($fPrix * $duree_mois) / 100)), 2); 
							$strNewPriceMois = round( $newPrice / $aDureeEngagement[0]['de_engagement'] , 2 ) ;
							$strNewPrice = str_replace('.',',',$newPrice);
							$strNewPriceMois = str_replace('.',',',$strNewPriceMois);
							if(!isset($_SESSION['compteur_erreur_cmd'])){
											$strMsg = 'Votre code promotionnel à bien été pris en compte !';
							}
							
							
			}else
							$strNewPrice = "";
        }
    }else{
        $strStep = "step_1_2"; 
        $bStep1 = true; 
        $bStep2 = false; 
        $strError = "Votre code promotionnel est faux ou périmé."; 
    }
}

$fPrix = str_replace('.',',',$fPrix);
$fPrixMois = str_replace('.',',',$fPrixMois);

// **** TRAITEMENT FORMULAIRE PAIEMENT ****

// Vérification que le produit est bien en base 
$strSql = "SELECT * FROM bor_produit p INNER JOIN bor_duree_engagement d ON(d.de_id_yonix = p.de_id_yonix) INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) WHERE p.produit_ean = '".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["ean"])."'"; 
$aProduit = $oDb->queryRow($strSql); 
$bProduit = true; 
if($aProduit["formule_classe"] == "tribu"){
	foreach( $_SESSION["products"]["enfants"] as $aEnfant){
		$strSql = "SELECT * FROM bor_produit p INNER JOIN bor_duree_engagement d ON(d.de_id_yonix = p.de_id_yonix) INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix) WHERE c.classe_id = '".mysql_real_escape_string($aEnfant["classe"])."' AND f.formule_classe = 'reussite' AND d.de_id = '".mysql_real_escape_string($aEnfant["duree_engagement"])."' "; 
		$aProduit = $oDb->queryRow($strSql); 
		if(!$oDb->rows){
			$bProduit = false ; 
		}
	}
}

if(!$bProduit ){
	$strMessage = "Produit non retrouvé sur l'étape 3 (paiement) du tunnel de commande.<br><br>" ; 
	$strMessage .= "Clients<br>" ;
	$strMessage .= print_r($_SESSION["user"], true) ;
	$strMessage .= "<br><br>"; 
	$strMessage .= "Formule<br>" ;
	$strMessage .= print_r($_SESSION['cart']["formule"], true) ;
	$strMessage .= "<br><br>"; 
	$strMessage .= "Liste des enfants<br>" ;
	$strMessage .= print_r($_SESSION["products"], true) ;
	$mail = new intyMailer(); 
	// OPTIONAL 
	$mail->set(array('charset'  , 'utf-8')); 
	$mail->set(array('priority' , 3)); 
	$mail->set(array('replyTo'  , "noreply@bordas.tm.fr")); 
	$mail->set(array('alert '    ,  true)); 
	$mail->set(array('html'     ,  true)); 
	// OPTIONAL 
	$mail->set(array('subject' , "[".$_CONST["SITE_NAME"]."] Commande : Produit non retrouvé")); 
	$mail->set(array('message' , $strMessage)); 
	$mail->set(array('sending' , 'FROM' , "Bordas Soutien Scolaire" ,"noreply@bordas.tm.fr")); 
	$aStrEmail = "pierrick.foy@ecomiz.com";
	$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
	$mail->send();
	$aStrEmail = "hgonzalez-quijano@sejer.fr";
	$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
	$mail->send();
}

// Traitement du formulaire 
$bVerif = true ;
if(isset($_POST) && count($_POST) > 0 && !isset($_POST["inputpromo"])){
				//var_dump($_SESSION);
	//Verification sur les différent champs 
	$bVerif = true ; 
	$bDonne = false;
	$strError = "";
	if( $_SESSION['products']['enfants'][0]['duree_engagement'] < 3  && (!isset($_POST['cb_name']) || empty($_POST['cb_name']))){
		$bVerif = false; 
		$strError .= "<p>"; 
		$strError .= "Veuillez renseigner le nom du détenteur de la carte.";
		$strError .="</p>";
	}
	if( (!isset($_POST['cp_type']) || empty($_POST['cp_type']))){
		$bVerif = false; 
		$strError .= "<p>"; 
		$strError .= "Veuillez sélectionner le type de votre carte."; 
		$strError .="</p>";
	}
	if( (!isset($_POST['cb_numcarte']) || empty($_POST['cb_numcarte']))){
		$bVerif = false; 
		$strError .= "<p>"; 
		$strError .= "Veuillez sélectionner votre n° de carte bancaire.";
		$strError .="</p>"; 
	}

	if( !preg_match("/^[0-9]{16}$/", $_POST['cb_numcarte'])){
		$bVerif = false; 
		$strError .= "<p>"; 
		$strError .= "Votre n° de carte n’est pas conforme, il doit contenir 16 chiffres."; 
		$strError .="</p>";
	}
	if(( (!isset($_POST['cb_annee']) || empty($_POST['cb_annee'])) || (!isset($_POST['cb_mois']) || empty($_POST['cb_mois'])) )){
		$bVerif = false; 
		$strError .= "<p>"; 
		$strError .= "Veuillez sélectionner la date d’expiration de votre CB."; 
		$strError .="</p>";
	}
	$strCarteDate = $_POST['cb_annee'].'-'.$_POST['cb_mois'];
	$strDate = date('y-m');
	if($strCarteDate < $strDate){
		$bVerif = false; 
		$strError .= "<p>"; 
		$strError .= "La date d'expiration de la carte est dépassée."; 
		$strError .="</p>";
	}
	if((!isset($_POST['cb_controle']) || empty($_POST['cb_controle']))){
		$bVerif = false; 
		$strError .= "<p>"; 
		$strError .= "Veuillez sélectionner votre n° de contrôle."; 
		$strError .="</p>";
	}
	if( !preg_match("/^[0-9]{3}$/", $_POST['cb_controle'])){
		$bVerif = false; 
		$strError .= "<p>"; 
		$strError .= "Votre n° de contrôle n’est pas conforme, il doit contenir 3 chiffres."; 
		$strError .="</p>";
	}

	if( (!isset($_POST['cb_cgv']) || empty($_POST['cb_cgv']))){
		$bVerif = false; 
		$strError .= "<p>"; 
		$strError .= "Il faut accepter les Conditions Générales de Vente pour valider votre commande.";
		$strError .="</p>";
		/*if( (!isset($_POST['cb_donnee']) || empty($_POST['cb_donnee'])) &&  $_SESSION['products']['enfants'][0]['duree_engagement'] < 3  ){
			$bDonne = true;
		}*/
	}
	$bVerifDonnees = true;
	if( (!isset($_POST['cb_donnee']) || empty($_POST['cb_donnee'])) &&  $_SESSION['products']['enfants'][0]['duree_engagement'] < 3  ){
		$bVerif = false; 
		$bVerifDonnees = false;
		$strError .= "<p>"; 
		$strError .= "Il faut accepter la conservation sécurisée des données bancaires pour valider votre commande.";
		$strError .="</p>";
		$bDonne = true;
	}

	$aCbType = array ( 1 => "CB" , 2 => "EU", 3 => "CB"); 
		//Aucune erreur traitement de la commande 

		// l'adresse de facturation est déjà en session, on met en session les données cb
		$_SESSION['reglement']['cb_name'] = $_POST['cb_name']  ;
		
		// Modif Pf Ecomiz 2/10/2015 : code promo : passer le montant remisé dans <totalcommande> de bill
		//$_SESSION['reglement']['TOTALCOMMANDE'] = $aProduct['produit_prix'];
		if(isset($newPrice) && !empty($newPrice) && $newPrice != "" && $newPrice != 0)
			$_SESSION['reglement']['TOTALCOMMANDE'] = $newPrice;
		else
			$_SESSION['reglement']['TOTALCOMMANDE'] = $aProduct['produit_prix'];
		$_SESSION['reglement']['TYPECB'] = $aCbType[$_POST['cp_type']];
		$_SESSION['reglement']['NUMCB'] = $_POST['cb_numcarte'];
		$_SESSION['reglement']['MOISCB'] = $_POST['cb_mois'];
		$_SESSION['reglement']['ANNEECB'] = $_POST['cb_annee'];
		$_SESSION['reglement']['NUMCONTROLECB'] = $_POST['cb_controle'];

		//Vérification de la présence d'une autre commande pour le même client dans un délai de 30 seconde pour éviter les doublons
	$strSql = "SELECT commande_num FROM bor_commande WHERE client_id = '".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."' AND commande_date > ADDDATE(NOW(), INTERVAL -30 SECOND) LIMIT 1 "; 
	$strCommandeNum = $oDb->queryItem($strSql);
	if($oDb->rows > 0 ){
		//Une commande existe pour le client donc il s'agit d'un doublon on récupère le num commande 
		$_SESSION['cart']['NUMCOMMANDE'] = $strCommandeNum;
		if($_SESSION['paiement']['statut'] == "KO"){
						$_SESSION['compteur_erreur_cmd'] ++;
								if($_SESSION['compteur_erreur_cmd'] < 3){
												$strError = '<p>Suite à un problème technique, votre commande n’a pas pu aboutir. Votre abonnement n’est donc pas enregistré.</p>';
												$strError .= '<p>Afin de finaliser votre commande, nous vous invitons à essayer de nouveau.</p>';
								}else{
												$strError = '<p>Suite à un problème technique, votre commande n’a pas pu aboutir. Votre abonnement n’est donc pas enregistré.</p>';
												$strError .= '<p>Afin de finaliser votre commande, nous vous invitons à contacter notre service client par mail <a href="mailto:soutien-scolaire@bordas.tm.fr"><span class="blue_txt">soutien-scolaire@bordas.tm.fr</span></a> ou par téléphone au <span class="blue_txt">01 72 36 40 91</span> (du lundi au vendredi de 9h à 13h et de 14h à 18h).</p>';
												$strError .= '<p>Nous vous remercions de votre confiance. </p>';
												echo "<script>jQuery(document).ready(function(){jQuery('#bloc_erreur_cmd').html(' ');});</script>";
											
								}
		}else{
				$_SESSION['paiement']['statut'] = "OK"; 
				if($_CONST['TYPE_ENVIRONNEMENT'] !='dev' && $_CONST['TYPE_ENVIRONNEMENT'] !='preprod'){
					echo "<script>document.location.href='". $_CONST["HTTPS"] . $_CONST['URL_ACCUEIL']."commande/bordas/etape-4-confirmation-formule-".$_SESSION['cart']['formule']['formule_classe'].".html';</script>";
				}else{
					echo "<script>document.location.href='". $_CONST["URL2"] . $_CONST['URL_ACCUEIL']."commande/bordas/etape-4-confirmation-formule-".$_SESSION['cart']['formule']['formule_classe'].".html';</script>";
				}
				exit;
		}
	}else if($bVerif){
						
				// on récupère le dernier numéro de commande
				$strCommandeLast = $oDb->queryRow("SELECT  commande_num  as commande_num FROM bor_commande ORDER BY commande_id DESC LIMIT 1");
				if($strCommandeLast['commande_num']!="" ) {
								if(strtolower($_CONST['TYPE_ENVIRONNEMENT']) == 'dev'){
				$strCommandeLast = $strCommandeLast['commande_num'];
				$strCommandeNum = "BSSTST" . sprintf("%04d",((int)str_replace("BSSTST", "", $strCommandeLast)) + 1 ); 
								}else if(strtolower($_CONST['TYPE_ENVIRONNEMENT']) == 'preprod'){
												$strCommandeLast = $strCommandeLast['commande_num'];
				$strCommandeNum = "BSSPRP" . sprintf("%04d",((int)str_replace("BSSPRP", "", $strCommandeLast)) + 1 ); 
								}else if(strtolower($_CONST['TYPE_ENVIRONNEMENT']) == 'prod'){
												$strCommandeLast = $strCommandeLast['commande_num'];
				$strCommandeNum = "BSS" . sprintf("%07d",((int)str_replace("BSS", "", $strCommandeLast)) + 1 ); 
								}
				}
				
				$_SESSION['cart']['NUMCOMMANDE'] = $strCommandeNum;
				
				foreach( $_SESSION["products"]["enfants"] as $i => $aEnfant){
					$fetch = (bool) strlen(trim($aEnfant["D_PRENOM"]));
					if($fetch === false){
						$_SESSION["products"]["enfants"][$i]["D_PRENOM"] = "ENFANT ".$i;
					}
				}

				// on effectue le traitement BILL
					
				$oBill = new fluxBill();
					//var_dump($strCommandeNum);exit;
				$strAction = ""; 
				$strPromo = "";
				$strReduction = "" ; 
				if(isset($_SESSION["products"]["enfants"][0]["REDUCTION"]) && !empty($_SESSION["products"]["enfants"][0]["REDUCTION"]))
								$strReduction =  $_SESSION["products"]["enfants"][0]["REDUCTION"];
				if(isset($_SESSION["products"]["enfants"][0]["CODEPROMO"]) && !empty($_SESSION["products"]["enfants"][0]["CODEPROMO"]))
								$strPromo = $strAction = $_SESSION["products"]["enfants"][0]["CODEPROMO"];
				else if(isset($_SESSION['partenaire']['bill_codeaction_commande']) && !empty($_SESSION['partenaire']['bill_codeaction_commande']))
								$strAction = $_SESSION['partenaire']['bill_codeaction_commande'];
			
				$oBill->Init(false, $strAction);
				$oBill->SetClient();
				$oBill->SetShipping();
				$oBill->SetCoord();
				$oBill->SetProducts(false,$strPromo); 
				$oBill->SetReglement();
				$oBill->Close();
				$aRetour = $oBill->SendBill();
				
				//var_dump($aRetour['code']);exit;
				
				// aucune erreur : insertions en BDD
				if(!$aRetour['erreur']){
								// Insertion dans la table commande
								$strSql = "INSERT INTO bor_commande (client_id,
																	produit_ean, 
																	commande_num, 
																	commande_total, 
																	commande_statut,
																	commande_date,
																	commande_promo, 
																	commande_reduction
																	) VALUES (
																	'".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."',
																	'".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["ean"])."',
																	'".mysql_real_escape_string($_SESSION['cart']['NUMCOMMANDE'])."',
																	'".mysql_real_escape_string($_SESSION['reglement']['TOTALCOMMANDE'])."',
																	'1', 
																	NOW(),
																	'".mysql_real_escape_string($strPromo)."',
																	'".mysql_real_escape_string($strReduction)."'
																	) "; 
								$oDb->query($strSql); 
								$iCommandeId = $oDb->last_insert_id; 
								$_SESSION['cart']['IDCOMMANDE'] = $iCommandeId ;

								//Insertion dans la table enfants
								foreach ( $_SESSION["products"]["enfants"] as $aEnfants){
									$strSql = "INSERT INTO bor_commande_enfant ( commande_id, 
																					classe_id,
																					matiere_id,
																					formule_id,
																					de_id,
																					enfant_prenom,
																					enfant_ean, 
																					enfant_ean_reussite 
																				) VALUES (
																					'".mysql_real_escape_string($iCommandeId)."',
																					'".mysql_real_escape_string($aEnfants['classe'])."',
																					'".mysql_real_escape_string($aEnfants['matiere'])."',
																					'".mysql_real_escape_string($_SESSION["cart"]["formule"]['formule_id'])."',
																					'".mysql_real_escape_string($aEnfants['duree_engagement'])."',
																					'".mysql_real_escape_string($aEnfants['D_PRENOM'])."',
																					'".mysql_real_escape_string($aEnfants['ean'])."',
																					'".mysql_real_escape_string($aEnfants['ean_reussite'])."'
																				)"; 
									$oDb->query($strSql); 														 
								}

								//Insertion dans la table facture
								if(isset($_SESSION['shipping']) && count($_SESSION['shipping'])>0){
									$strSql = "INSERT INTO bor_commande_facture ( commande_id, 
																						pays_id, 
																						facture_adr1, 
																						facture_adr3, 
																						facture_ville, 
																						facture_cplivraison
																					) VALUES (
																						'".mysql_real_escape_string($iCommandeId)."',
																						'".mysql_real_escape_string($_SESSION['shipping']['pays_id'])."',
																						'".mysql_real_escape_string($_SESSION['shipping']['ADR1'])."',
																						'".mysql_real_escape_string($_SESSION['shipping']['ADR3'])."',
																						'".mysql_real_escape_string($_SESSION['shipping']['VILLELIVRAISON'])."',
																						'".mysql_real_escape_string($_SESSION['shipping']['CPLIVRAISON'])."'
																					)";
									$oDb->query($strSql); 
								}

								$strSql = "UPDATE bor_client SET acces_zoe = 1 WHERE client_id ='".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."'"; 
								$oDb->query($strSql);
								//On effectue l'appel au ws abonnement pour les durees inférieures à 1 an (id duree engagement 1 et 2 )  
								if( $_SESSION["products"]["enfants"][0]['duree_engagement']  < 3){
												$oAbo = new aboEditis();
												$strAction = "";
												if(isset($_SESSION['partenaire']['bill_codeaction_commande']) && !empty($_SESSION['partenaire']['bill_codeaction_commande']))
																$strAction = $_SESSION['partenaire']['bill_codeaction_commande'];
												$oAbo->initCommande($strAction,$strPromo);
												$a = $oAbo->setCommande();
								}
								// var_dump($a); exit; 
								$_SESSION['paiement']['statut'] = "OK"; 
								echo "<script>document.location.href='".( ($_CONST['TYPE_ENVIRONNEMENT'] !='dev' && $_CONST['TYPE_ENVIRONNEMENT'] !='preprod' && $_CONST['TYPE_ENVIRONNEMENT'] !='prod' ) ? $_CONST["HTTPS"] : $_CONST["URL2"]) . $_CONST['URL_ACCUEIL']."commande/bordas/etape-4-confirmation-formule-".$_SESSION['cart']['formule']['formule_classe'].".html';</script>";
								exit; 
				}else{
								$bVerif = false ; 
								// var_dump($aRetour['code']);
								$_SESSION['paiement']['statut'] = "KO"; 
								$_SESSION['paiement']['code'] = $aRetour['code']; 

								// HZ 20150123 : En cas d'erreur bill insertion de la commande avec statut -1, 
								// cela permet d'accrémeter le num commande en cas de doublon pour ne pas rester bloquer sur le même num
								$strSql = "INSERT INTO bor_commande (client_id,
																				produit_ean, 
																				commande_num, 
																				commande_total, 
																				commande_statut,
																				commande_date, 
																				commande_activation
																				) VALUES (
																				'".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."',
																				'".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["ean"])."',
																				'".mysql_real_escape_string($_SESSION['cart']['NUMCOMMANDE'])."',
																				'".mysql_real_escape_string($_SESSION['reglement']['TOTALCOMMANDE'])."',
																				'-1', 
																				NOW(),
																				'".mysql_real_escape_string($_SESSION["code_activation"]["CODE_AVANTAGE"])."'
																				) "; 
								$oDb->query($strSql); 
								//var_dump($aRetour['code']);
								switch ($aRetour['code']){
									case '0': $strMessage = "Votre commande a échoué.<br/><br/>Un problème technique empêche temporairement l'enregistrement de votre commande.<br/>Nous vous invitons à renouveler votre commande ultérieurement.<br/><br/>Nous vous remercions de votre compréhension.";  break;
									case '3': $strMessage = "Votre commande a échoué.<br/><br/>Un problème technique empêche temporairement l'enregistrement de votre commande.<br/>Nous vous invitons à renouveler votre commande ultérieurement.<br/><br/>Nous vous remercions de votre compréhension.";  break;
									case '4': $strMessage = "Votre commande a échoué.<br/><br/>Un problème technique empêche temporairement l'enregistrement de votre commande.<br/>Nous vous invitons à renouveler votre commande ultérieurement.<br/><br/>Nous vous remercions de votre compréhension.";  break;
									case '5': $strMessage = "Votre commande a échoué.<br/><br/>Les informations liées à votre carte bancaire sont incorrectes.<br/>Nous vous invitons à renouveler votre commande en vérifiant ces informations ou en choisissant une autre carte bancaire.<br/><br/>En vous remerciant de votre confiance et de votre fidélité."; break;
									case '7': $strMessage = "Votre commande a échoué.<br/><br/>Un problème technique empêche temporairement l'enregistrement de votre commande.<br/>Nous vous invitons à renouveler votre commande ultérieurement.<br/><br/>Nous vous remercions de votre compréhension.";  break;
									case '8': $strMessage = "Votre commande a échoué.<br/><br/>Votre établissement bancaire a refusé le paiement.<br/> Nous vous invitons à vérifier les informations liées à votre carte bancaire.<br/><br/>En vous remerciant de votre confiance et de votre fidélité."; break;
								}
								// en cas d'echec de la commande
								// reste sur la page avec message d'erreur
								// droit à 3 essais, puis blocage de la commande
							$_SESSION['compteur_erreur_cmd'] ++;
								if($_SESSION['compteur_erreur_cmd'] < 3){
												//$strError = '<p>Suite à un problème technique, votre commande n’a pas pu aboutir. Votre abonnement n’est donc pas enregistré.</p>';
												//$strError .= '<p>Afin de finaliser votre commande, nous vous invitons à essayer de nouveau.</p>';
												$strError = $strMessage;
								}else{
												$strError = '<p>Suite à un problème technique, votre commande n’a pas pu aboutir. Votre abonnement n’est donc pas enregistré.</p>';
												$strError .= '<p>Afin de finaliser votre commande, nous vous invitons à contacter notre service client par mail <a href="mailto:soutien-scolaire@bordas.tm.fr"><span class="blue_txt">soutien-scolaire@bordas.tm.fr</span></a> ou par téléphone au <span class="blue_txt">01 72 36 40 91</span> (du lundi au vendredi de 9h à 13h et de 14h à 18h).</p>';
												$strError .= '<p>Nous vous remercions de votre confiance. </p>';
												echo "<script>jQuery(document).ready(function(){jQuery('#bloc_erreur_cmd').html(' ');});</script>";
												
								}
								
								
				}
 }
}
?>

<div class="bss-section bloc-section-gris bss-tunnel">
  <div class="container">
<!--    <form method="post" action="" class="form-horizontal" id="paiement" title="form-code-promo">-->
      <div class="row">
        <div class="col-md-8">
          <h1 class="h1">Paiement</h1>
												<?php
												if(isset($strError) && $strError != ""){
												?>
												<div class="alert alert-danger" role="alert">
												<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<strong>Erreur !</strong><br>
												<?php echo $strError; ?> </div>
												<?php
												}
													if(isset($strMsg) && $strMsg != ""){
												?>
												<div class="alert alert-success" role="alert">
												<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<strong>Merci !</strong><br>
												<?php echo $strMsg; ?> </div>
												<?php
												}
												?>
										<div id="bloc_erreur_cmd">
								<form id="form_promo" method="post" name="form_promo">
          <div class="bss-connexion bloc-bss-1">
            <div class="input-group  ">
             <input type="text" required class="form-control" name="codepromo" form="form_promo" id="codepromo" placeholder="Entrez votre code promotionnel" value="<?php if(isset($_SESSION["CODEPROMO"]["CODE"]) && !empty($_SESSION["CODEPROMO"]["CODE"])) echo $_SESSION["CODEPROMO"]["CODE"] ; ?>" >
														<span class="input-group-btn">
														<input type="submit" 
														 <?php
				if($_SESSION['cart']['formule']['formule_id'] == 1){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Ciblée - Code promo Valider');" <?php
				}else if($_SESSION['cart']['formule']['formule_id'] == 2){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Réussite - Code promo Valider');" <?php
				}else if($_SESSION['cart']['formule']['formule_id'] == 3){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Tribu - Code promo Valider');" <?php
				}
				?> 
				   class="btn btn-primary " id="inputpromo" name="inputpromo" value="Valider" >
              </span> </div>
          </div>
								</form>
<div style="display:none;">
          <hr>
          <h2>Choisissez votre mode de paiement :</h2>
          <div class="radio">
            <label>
              <input name="form-code-promo" type="radio" id="optionsRadios1" form="paiement" value="option1" checked>
              Réglez la totalité de votre abonnement en une seule fois</label>
          </div>
          <div class="radio">
            <label>
              <input name="form-code-promo" type="radio" id="optionsRadios2" form="paiement" value="option2">
              Optez pour un paiement échelonné (prélèvement mensuel)</label>
          </div>
</div>
          <hr>
          <h2>Récapitulatif de votre paiement :</h2>
          <ul class="paiement-recap">
            <li>Abonnement d'une durée de <strong><?php echo $aDureeEngagement[0]['de_libelle']; ?> à
																<?php
																				if(isset($strNewPrice) && !empty($strNewPrice)){
																								echo ' '.$strNewPrice.'	€';
																								if($aDureeEngagement[0]['de_engagement'] == 1)
																												echo ' le premier mois';
																								else if($aDureeEngagement[0]['de_engagement'] == 3)
																												echo ' les trois premiers mois';
																								echo ' (au lieu de <s>'.$fPrix.' € </s> )';
																							
																				}else
																								echo $fPrix.'	€';
																?>
																			</strong> <?php if($iDe == 3) echo 'sans'; else echo 'avec'; ?> reconduction tacite</li>
            <li>Abonnement valable jusqu'au : <strong  class="bleu">
																<?php
																				$today = date("d-m-Y");
																				$date = new DateTime($today.'+ '.$aDureeEngagement[0]['de_engagement'].' month');
																				//var_dump($date);
																				echo $date->format('d/m/Y');
																?>
																</strong></li>
<!--            <li><strong>Paiement mensualisé à <?php echo $fPrixMois; ?>€ par mois, </strong></li>-->
												<?php if(isset($strNewPrice) && $_SESSION['products']['enfants'][0]['duree_engagement'] != 3){?>
												<li>Prix du premier prélévement : <strong><?php echo $strNewPrice; ?> €</strong></li>
            <li>Prix récurrent : <strong><?php echo $fPrix; ?> €</strong></li>
												<?php }
												
												
												?>
          </ul>
          <hr>
          <h2>Formulaire de paiement :</h2>
										
          <div class="bss-creation bloc-bss-1">
														<form method="post" action="" class="form-horizontal" id="paiement" >
            <div class="form-group">
              <label for="nom-carte-client" class="col-sm-6 control-label">
														<?php
																if($_SESSION['products']['enfants'][0]['duree_engagement'] < 3){
																				echo '<span class="obligatoire">*</span> ';
																}
														?>
																		Nom du porteur de la carte</label>
              <div class="col-sm-6">
																<input type="text" class="form-control" id="inputnomdetenteur"  maxlength="20" placeholder="Nom du porteur de la carte" name="cb_name" <?php if(isset($_POST['cb_name'])) echo "value=\"".stripslashes($_POST['cb_name'])."\""; ?>>
														</div>
            </div>
            <div class="form-group ">
              <label for="type-carte" class="col-sm-6 control-label"><span class="obligatoire">*</span> Type de carte</label>
              <div class="col-sm-6">
                <label class="radio-inline">
																		<input type="radio" name="cp_type" value="1" <?php if(isset($_POST['cp_type']) && $_POST['cp_type'] =="1" ) echo "checked"; ?> >
                  <span class="icone-visa">Visa</span> </label>
                <label class="radio-inline">
																		<input type="radio"  name="cp_type" value="2" <?php if(isset($_POST['cp_type']) && $_POST['cp_type'] =="2" ) echo "checked"; ?> >
                  <span class="icone-mastercard">Mastercard</span> </label>
                <label class="radio-inline">
																		<input type="radio"  name="cp_type" value="3" <?php if(isset($_POST['cp_type']) && $_POST['cp_type'] =="3" ) echo "checked"; ?>>
                  <span class="icone-cb">CB</span> </label>
              </div>
            </div>
            <div class="form-group  has-feedback" >
              <label for="numero-carte" class="col-sm-6 control-label"><span class="obligatoire">*</span> Numéro de carte</label>
              <div class="col-sm-6">

                <input type="text" class="form-control" id="inputnumcarte" name="cb_numcarte"  <?php if(isset($_POST['cb_numcarte'])) echo "value='".$_POST['cb_numcarte']."'"; ?>>
																<i class="icon-shield form-control-feedback"></i> <span id="inputSuccess2Status" class="sr-only">(sécurisé)</span> </div>
            </div>
            <div class="form-group">
              <label for="date-expiration-carte" class="col-sm-6 control-label"><span class="obligatoire">*</span> Date d'expiration</label>
              <div class="col-sm-3">

																		<select class="form-control" name="cb_mois">
																				<option >Mois</option>
																				<option value="01" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="01" ) echo "selected"; ?>>01</option>
																				<option value="02" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="02" ) echo "selected"; ?>>02</option>
																				<option value="03" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="03" ) echo "selected"; ?>>03</option>
																				<option value="04" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="04" ) echo "selected"; ?>>04</option>
																				<option value="05" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="05" ) echo "selected"; ?>>05</option>
																				<option value="06" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="06" ) echo "selected"; ?>>06</option>
																				<option value="07" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="07" ) echo "selected"; ?>>07</option>
																				<option value="08" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="08" ) echo "selected"; ?>>08</option>
																				<option value="09" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="09" ) echo "selected"; ?>>09</option>
																				<option value="10" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="10" ) echo "selected"; ?>>10</option>
																				<option value="11" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="11" ) echo "selected"; ?>>11</option>
																				<option value="12" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="12" ) echo "selected"; ?>>12</option>
																		</select>
              </div>
              <div class="col-sm-3">
																		<select name="cb_annee" class="form-control">
																				<option>Année</option>
																						<?php 
																						for($i = 0; $i<10; $i++){
																						echo "<option value='".(date('y')+$i)."' ".((isset($_POST['cb_annee']) && $_POST['cb_annee'] ==(date('y')+$i))? "selected" : "").">".(date('y')+$i)."</date>";
																					}
																						?>
																				</select>
              </div>
            </div>
            <div class="form-group  has-feedback" >
              <label for="numero-controle" class="col-sm-6 control-label"><span class="obligatoire">*</span> Cryptogramme sécurisé</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" id="inputnumcontrole" name="cb_controle"  <?php if(isset($_POST['cb_controle'])) echo "value='".$_POST['cb_controle']."'"; ?>>
																<i class="icon-shield form-control-feedback"></i> <span id="numero-controle-statut" class="sr-only">(sécurisé)</span> </div>
              <div class="col-sm-3"> <a id="popover" class="btn btn-primary btn-icon" tabindex="0"  role="button"  data-trigger="focus" rel="popover" data-content="" title="Emplacement du cryptogramme sécurisé">?</a> </div>
            </div>
            <!-- Modal aideccv-->
            <div class="modal aideccv fade" id="aideccv" tabindex="-1" role="dialog" aria-labelledby="aideccvLabel">
              <div class="modal-dialog " role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Emplacement du cryptogramme visuel</h4>
                  </div>
                  <div class="modal-body"> <img src="/img/verso-carte.jpg"  class="img-responsive"/> </div>
                </div>
              </div>
            </div>
            <div class="checkbox">
              <label>
																<input type="checkbox" id="informationbordas" name="cb_cgv">
                <span class="obligatoire">*</span> J'ai lu et j'accepte les <a 
				<?php
				if($_SESSION['cart']['formule']['formule_id'] == 1){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Ciblée - Lien cgv');" <?php
				}else if($_SESSION['cart']['formule']['formule_id'] == 2){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Réussite - Lien cgv');" <?php
				}else if($_SESSION['cart']['formule']['formule_id'] == 3){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Tribu - Lien cgv');" <?php
				}
				?>  
				href="/conditions-generales-de-vente.html">conditions générales de vente</a> et les <a 
				<?php
				if($_SESSION['cart']['formule']['formule_id'] == 1){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Ciblée - Lien cgu');" <?php
				}else if($_SESSION['cart']['formule']['formule_id'] == 2){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Réussite - Lien cgu');" <?php
				}else if($_SESSION['cart']['formule']['formule_id'] == 3){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Tribu - Lien cgu');" <?php
				}
				?> 
				href="/conditions-generales-d-utilisation.html">conditions générales d'utilisation</a> </label>
            
												</div>
													<?php 
																if( $_SESSION['products']['enfants'][0]['duree_engagement'] < 3 ){
																					echo '
																				<div class="checkbox">
																				<label>
																				<input type="checkbox" id="informationparter"  name="cb_donnee">
																				<span class="obligatoire">*</span> J\'accepte la conservation sécurisée de mes données bancaires **</label>
																				</div>';
																}
												?>
												<hr>
												<p class="legende"><span class="obligatoire">* <strong>champ obligatoire</strong></span><br>
												<?php 
																if( $_SESSION['products']['enfants'][0]['duree_engagement'] < 3 ){
																			echo '<div class="well legende">** La conservation sécurisée de vos données bancaires est nécessaire pour vous abonner mensuellement. Cela vous permet d’éviter de les saisir à nouveau pour la poursuite de votre abonnement. Nous vous rappelons que vous êtes libre d’interrompre l’abonnement mensuel dans les conditions définies aux conditions générales de ventes.</div>
																				';
																}
												?>
												<div class="form-group">
              <div class="col-sm-6"> &nbsp;</div>
              <div class="col-sm-6">
                <button type="submit" 
				<?php
				if($_SESSION['cart']['formule']['formule_id'] == 1){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Ciblée - Valider votre paiement');" <?php
				}else if($_SESSION['cart']['formule']['formule_id'] == 2){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Réussite - Valider votre paiement');" <?php
				}else if($_SESSION['cart']['formule']['formule_id'] == 3){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Tribu - Valider votre paiement');" <?php
				}
				?> 
				
				class="btn btn-primary btn-fw">Valider votre paiement <i class="icon-ok "></i></button>
              </div>
            </div>
														</form>
          </div>
          </div>
        </div>
        <div class="col-md-4 bss-step-sidebar">
          <div class="bloc-bss-step-sidebar bloc-bss-gris">
            <div class="bloc-bss-produit-titre">Ma Formule</div>
												<ul>
            <li><?php echo $_SESSION['cart']['formule']['formule_libelle'];?></li>
												<li><?php 
																if($_SESSION['cart']['formule']['formule_id'] == 1)
																				echo "1 classe - 1 matière";
																else if($_SESSION['cart']['formule']['formule_id'] == 2)
																				echo "1 classe - 1 Multi-matières";
																else if($_SESSION['cart']['formule']['formule_id'] == 3)
																				echo count($_SESSION["products"]["enfants"])." classes - Multi-matières";
																?>
												
												</li>
          </ul>
            <div class="text-right"><a href="<?php echo $_CONST['URL_ACCUEIL']; ?>comment-s-abonner.html"
			<?php
				if($_SESSION['cart']['formule']['formule_id'] == 1){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Ciblée - Changer de formule');" <?php
				}else if($_SESSION['cart']['formule']['formule_id'] == 2){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Réussite - Changer de formule');" <?php
				}else if($_SESSION['cart']['formule']['formule_id'] == 3){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Tribu - Changer de formule');" <?php
				}
				?> 
			class="btn btn-sm btn-default " type="button">Changer de formule</a></div>
          </div>

												
												<div class="bloc-bss-step-sidebar bloc-bss-gris">
            <div class="bloc-bss-produit-titre">Prix de mon abonnement :</div>
            <div class="bloc-bss-produit-stitre">Pour une durée de <span class='duree_libelle'> <?php echo $aDureeEngagement[0]['de_libelle']; ?></span> :</div>
            <div class="bloc-bss-produit-prix">
																<div style='display:inline;' id='prix_choisi'>
																				<?php
																								if(isset($strNewPrice) && !empty($strNewPrice)){
																												//echo $strNewPriceMois;
																												$strNewPriceMois_ex = explode(',',$strNewPriceMois); echo $strNewPriceMois_ex[0].'<sup>,'.$strNewPriceMois_ex[1];
																								}else{
																												$fPrixMois_ex = explode(',',$fPrixMois); echo $fPrixMois_ex[0].'<sup>,'.$fPrixMois_ex[1];
																								}
																								
																				?>
																				</sup>
																</div>
																<sup>€/mois</sup>
												</div>
            <?php if($iDe != 1) {?>
																<?php
																if(isset($strNewPrice) && !empty($strNewPrice)){	?>
																				<div class="legende text-center" id="ligne_prix_annee">(soit <span id='prix_annee'><?php echo $strNewPrice; ?></span>€ pour <span class='duree_libelle'> <?php echo $aDureeEngagement[0]['de_libelle']; ?></span>)</div>
																				<div class="legende ancien-prix text-center"><strong><s><?php echo $fPrixMois; ?>€</s> par mois</strong> (soit <s><?php echo $fPrix; ?>€</s> pour <?php echo $aDureeEngagement[0]['de_libelle']; ?>)</div>
																<?php												
																}else{
																?>
																				<div class="legende text-center" id="ligne_prix_annee">(soit <span id='prix_annee'><?php echo $fPrix; ?></span>€ pour <span class='duree_libelle'> <?php echo $aDureeEngagement[0]['de_libelle']; ?></span>)</div>
																<?php												
																}
												}?>
          </div>
												
												
          <div class="bloc-bss-step-sidebar bloc-bss-gris">
            <div class="bloc-bss-produit-titre">Mon abonnement pour :</div>
												<?php 
												
																foreach($_SESSION["products"]["enfants"] as $key=>$enfant){
																				?>
																				<div class="form-group">
																						<label for="nom-enfant" class="control-panel">Nom de l'enfant</label>
																						<ul class="liste-enfant">
<!--																								<li><?php echo $enfant['D_PRENOM'][$key]; ?></li>-->
																								<li>
																												<?php
																												if($_SESSION['cart']['formule']['formule_id'] == 3)
																																echo $_SESSION["products"]["enfants"][$key]["D_PRENOM"];
																												else
																																echo $enfant['D_PRENOM'];
																												?>
																								</li>
																						</ul>
																				</div>
																				<label for="classe" class="control-panel">
																							Classe :
																				</label>
																				<ul class="liste-matiere">
																									<?php
																												$iNomClasse = $oDb->queryRow("SELECT classe_name FROM bor_classe				
																												WHERE classe_id = ".$_SESSION["products"]["enfants"][$key]["classe"]."");
																												echo '<li>'.$iNomClasse['classe_name'].'</li>';
																								?>
																				</ul>

																				<label for="matiere" class="control-panel">
																								<?php
																								if($_SESSION['cart']['formule']['formule_id'] == 1)
																												echo 'Matière :';
																								else
																												echo 'Matières :';
																								?>
																				</label>
																				<ul class="liste-matiere">
																				<?php
																				// ciblee : une seule matière
																				if($_SESSION['cart']['formule']['formule_id'] == 1){
																					$iNomMatiere = $oDb->queryRow("SELECT matiere_titre
																					FROM bor_matiere
																					WHERE matiere_id = ".$_SESSION["products"]["enfants"][0]["matiere"]."");
																					echo '<li>'.$iNomMatiere['matiere_titre'].'</li>';
																				// réussite : plusieurs matières associées à une classe
																				}else{
																					$aMatieres = $oDb->queryTab("
																					SELECT mc.matiere_id, m.matiere_titre as mc_titre
																					FROM bor_matiere_classe mc
																					INNER JOIN bor_matiere m ON (m.matiere_id = mc.matiere_id)
																					INNER JOIN bor_classe_page cp ON (mc.classe_id = cp.classe_page_id)
																					WHERE cp.classe_page_id =".mysql_real_escape_string((int)$_SESSION["products"]["enfants"][$key]["classe"])."
																					AND mc.mc_dispo = 1");
																					
																					foreach($aMatieres as $iMatiere){
																									echo '<li>'.$iMatiere['mc_titre'].'</li>';
																					}
																				}
																				?>
																				</ul>
																				<hr>
																				<?php
																}
																?>
            <div class="bloc-bss-produit-titre">Mes informations personnelles</div>
            <ul class="recap-info-perso">
              <li><strong>Civilité :</strong>
																		<?php $aCivilitesXml = array (
																				'1' => 'Monsieur',
																				'2' => 'Madame',
																				'3' => 'Mademoiselle',
																			);
																	echo $aCivilitesXml[$_SESSION['user']['CIVILITECLIENT']]; ?></li>
              <li><strong>Prénom :</strong> <?php echo $_SESSION['user']['PRENOMCLIENT']; ?></li>
              <li><strong>Nom :</strong> <?php echo $_SESSION['user']['NOMCLIENT']; ?></li>
              <li><strong>Identifiant :</strong> <?php echo $_SESSION['user']['LOGIN']; ?></li>
              <li><strong>Mot de passe :</strong> <?php echo $_SESSION['user']['MOTDEPASSE']; ?></li>
            </ul>
          </div>
          <div class="bloc-bss-step-sidebar bloc-bss-gris bloc-bss-bleu">
            <div class="bloc-bss-produit-titre text-center"><i class="icon-shield "></i> Paiement sécurisé</div>
            <div 
				<?php
				if($_SESSION['cart']['formule']['formule_id'] == 1){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Ciblée - En savoir plus sur le paiement securisé');" <?php
				}else if($_SESSION['cart']['formule']['formule_id'] == 2){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Réussite - En savoir plus sur le paiement securisé');" <?php
				}else if($_SESSION['cart']['formule']['formule_id'] == 3){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 3', 'F.Tribu - En savoir plus sur le paiement securisé');" <?php
				}
				?> 
			
			class="text-center"><a role="button" data-toggle="collapse" href="#collapseSecurite" aria-expanded="false" aria-controls="collapseSecurite" class="btn  btn-outline  btn-fw collapsed">En savoir plus</a></div>
            <div class="collapse" id="collapseSecurite">
              <div class="well"> Tout paiement effectué sur notre site fait l’objet d’un système de sécurisation par certificat SSL (Secure Sockets Layer) permettant le cryptage de l'information relative à vos coordonnées bancaires. </div>
            </div>
          </div>
        </div>
      </div>
<!--    </form>-->
  </div>
</div>
<?php

/* Implémentation plan de taggage - EFFILIATION */
	if(true || strtolower($_CONST['TYPE_ENVIRONNEMENT']) != 'dev'){
		// étapes 1 et 2
		
			if(isset($_SESSION["products"]["enfants"][0]["ean"])){
				// si on a un EAN
				echo '<script src="https://mastertag.effiliation.com/mt660015994.js?page=product&idp='.$_SESSION["products"]["enfants"][0]["ean"].'" async="async" ></script>';
			}else{
				// sinon on indique de quelle formule il s'agit
				echo '<script src="https://mastertag.effiliation.com/mt660015994.js?page=product&idp='.$_SESSION['cart']['formule']['formule_classe'].'" async="async" ></script>';
			}

	}
	/* --- */

?>