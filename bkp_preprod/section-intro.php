
<div class="bss-hero bss-programme">
  <div class="container">
    <div class="bss-intro-area ">
      <h1 id="tagline" class="h1">Soutien scolaire en ligne</h1>
      <h2 id="subtitle" class="h2">Le seul site conforme aux programmes du CP à la Terminale.</h2>
      <div class="bss-search-form">
        <div class="container">
          <div class="row ">
            <div class="col-md-10 col-md-offset-1">
              <form class="form-horizontal" onSubmit="return checkAnswer();" method="post" >
                <div class="form-group">
                  <label for="choix-classe" class="sr-only">Classe</label>
                  <div class="col-sm-4">
                    <select  name="classe[0]" class="form-control choix-classe" id="choix-classe" placeholder="Classe" >
                    <option value="Classe" selected="selected">Choisir une classe</option>
					<?php
                     $ListClasses  = $oDb->queryTab("SELECT c.*
								FROM bor_classe_page cp
								INNER JOIN bor_classe c ON (c.classe_id = cp.classe_id) 
								INNER JOIN bor_produit p ON (p.classe_id_yonix = c.classe_id_yonix) 
								INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) 
								INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id)
								INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id)
								GROUP BY c.classe_id
								ORDER BY n.niveau_position, cn.cn_position
								"); 
						
						if($ListClasses){ 
							foreach($ListClasses as $classe){
								echo '<option value="'.$classe['classe_id'].'" '.(( isset($_SESSION["products"]["enfants"][0]["classe"]) && $_SESSION["products"]["enfants"][0]["classe"] == $classe['classe_id']) ? 'selected' : '' ) .'>'.$classe['classe_name'].'</option>'; 
							}  
						}
					?>
                    </select>
                  </div>
                  <label for="choix-matiere" class="sr-only">Matière</label>
                  <div class="col-sm-4">
                    <select  name="matiere" class="form-control" id="choix-matiere" placeholder="Matiere" >
						<option value="Matière" selected="selected">Matière</option>
                    </select>
                  </div>
                  <div class="col-xs-12 col-sm-4 ">
                    <button type="submit" 
					onclick="ga('send', 'event', 'Home', 'Commande', 'Sélecteur - Je cherche une formule pour mon enfant');" 
					class="btn btn-search btn-fw">Je cherche une formule pour mon enfant</button>
                  </div>
                </div>	
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
// chargement des matières en fonction de la classe
jQuery(document).ready(function(){
	function update_matiere2() {
	   jQuery.ajax({type:"POST", data: jQuery('.choix-classe').serialize(), url: "/templates/ajaxsearch_edito.php" ,dataType: 'json',	success: function(json) {
					jQuery('#choix-matiere option').remove(); 
					jQuery('#choix-matiere').append('<option value="">Choisir une matière</option>');
					jQuery.each(json, function(index, value) { 
						jQuery('#choix-matiere').append('<option value="'+ json[index]['matiere_id'] +'">'+ json[index]['mc_titre'] +'</option>');
					});
				},
				error: function(){
					jQuery('#choix-matiere').html('');
				}
			});
			return false;
	}
	// au chargement, si classe renseignée
	update_matiere2();

	// au changement de classe: changement des matières
	jQuery('.choix-classe').change(function () {
			update_matiere2();
	});	
});

// lien vers les pages edito
function checkAnswer(){
    var choixClasse = document.getElementById('choix-classe').value;
    var choixMatiere = document.getElementById('choix-matiere').value;
		jQuery.ajax({type:"POST", data:{classe: choixClasse, matiere: choixMatiere}, url: "/templates/ajaxsearch_link.php" ,dataType: 'json',	success: function(json) {
			if(!json[0]['matiere_url'])
				location = json[0]['classe_url'];
			else if(json[0]['classe_url'] && json[0]['matiere_url'])
				location = json[0]['classe_url']+json[0]['matiere_url']+'/';
			else if(!json[0]['classe_url'] && json[0]['matiere_url'])
				location = json[0]['matiere_url'];
		}});
       
    return false;
}
</script>
