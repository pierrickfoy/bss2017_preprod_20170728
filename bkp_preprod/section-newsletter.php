<div class="bss-section bloc-section-bleu bss-newsletter">
<a class="anchor" id="bloc_newsletters" style="display: block;
    position: relative;
    top: -250px;
    visibility: hidden;"></a>
  <div class="container">
    <div class="row">
      <div class=" col-sm-12 col-md-6">
        <div class="media">
          <div class="media-left"> <img src="/img/icone-newsletter-bleu.png" > </div>
          <div class="media-body">
            <div class="h1  ">Vous souhaitez ...</div>
            <ul>
              <li>Recevoir notre documentation ?</li>
              <li>Bénéficier de nos offres spéciales ?</li>
              <li>Être tenu informé de nos actualités ?</li>
            </ul>
          </div>
        </div>
      </div>
      <div class=" col-sm-12 col-md-6">
          <?php
            if (!empty($_POST['newsletter_email'])) {
                try{
			
                    $soap_options = array ( 'login' => 'WSProspect', 'password' => 'ProspectManagementPER', 'exception' => true); 
                    $client = new SoapClient("http://ws.edupole.net/prospectManagement/WSProspect.wsdl", $soap_options); 
                    $params = array(
                                            'codeSite' => 'BRD_SOUTIEN_SCO',
                                            'email' => $_POST['newsletter_email'], 
                                            'origin' => 'SABONNER-L1_BSS'
                                            );

                    $retour_ws = $client->createProspect($params); 

                    if(isset($retour_ws)){
                            if($retour_ws->creationStatus)
                                    $strMessage = "Votre adresse email a bien été enregistrée."; 
                            else
                                    $strMessage = "Cet email a déjà été utilisé, vous ne pouvez l'utiliser qu'une seule fois.";
                     
                    }else
                        $strMessage = "Une erreur est survenue lors de votre enregistrement."; 
                }catch (Exception $e){
                        $strMessage = "Une erreur est survenue lors de votre enregistrement.";
                }
                echo $strMessage;
            } else {
                echo '<form class="text-right" id="form-newsletter" action="#bloc_newsletters" name="formnewsletter" method="POST">
                  <div class="input-group  ">
                    <input name="newsletter_email" type="text" class="form-control " placeholder="Mon adresse email">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button" onclick="ga(\'send\', \'event\', \'Prospects\', \'Information\', \'Je m inscris à la newsletter\');document.formnewsletter.submit();">Je m\'inscris à la newsletter</button>
                    </span> </div>
                  <!-- /input-group -->
                 </form>';
                }
          ?>
      </div>
      
    </div>
  </div>
</div>
