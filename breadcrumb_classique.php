<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
 <!-- BREADCRUMB -->
  <div class="bss-breadcrumb" >
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h6>
			<ol class="breadcrumb">
				<li id="a" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="<?php echo $_CONST['URL_ACCUEIL'];?>" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
			<?php 
			$iClasse = "";
			$iMatiere = "";
			if(isset($_GET['classe_url'])){
				$iClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe_page, bor_classe WHERE bor_classe.classe_id = bor_classe_page.classe_id AND classe_page_url ='/".$_GET['classe_url']."/'");
			}
			if(isset($_GET['matiere_url'])){
				$iMatiere = $oDb->queryItem("SELECT matiere_titre FROM bor_matiere_page, bor_matiere WHERE bor_matiere.matiere_id = bor_matiere_page.matiere_id AND matiere_page_url ='/".$_GET['matiere_url']."/'");
			}
			if($_GET['templates_id'] == 14){ // page classe
			?>
				<li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="#" itemprop="url"> <span itemprop="title"><?php echo ucfirst($iClasse); ?></span> </a></li>
			<?php
			}else if($_GET['templates_id'] == 16){ // page matiere
			?>
				<li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="#" itemprop="url"> <span itemprop="title"><?php echo ucfirst($iMatiere); ?></span> </a></li>
			<?php
			}else if($_GET['templates_id'] == 15){ // page classe matiere
			?>
				<li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="/<?php echo strtolower($_GET['classe_url']); ?>/" itemprop="url"> <span itemprop="title"><?php echo ucfirst($iClasse); ?></span> </a></li>
				<li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="#" itemprop="url"> <span itemprop="title"><?php echo ucfirst($iMatiere); ?></span> </a></li>
			<?php
			}else if($_GET['templates_id'] == 55){ // page chapitre
			
			$aInfos = $oDb->queryRow("SELECT * FROM bor_chapitres WHERE chapitre_alias ='/".$_GET['chapitre_url']."/'");
			//var_dump($aInfos['chapitre_h1']);
			?>
				<li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="/<?php echo strtolower($_GET['classe_url']); ?>/" itemprop="url"> <span itemprop="title"><?php echo ucfirst($iClasse); ?></span> </a></li>
				<li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="/<?php echo strtolower($_GET['classe_url'])."/".strtolower($_GET['matiere_url']); ?>/" itemprop="url"> <span itemprop="title"><?php echo ucfirst($iMatiere); ?></span> </a></li>
				<li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="#" itemprop="url"> <span itemprop="title"><?php echo ucfirst($aInfos['chapitre_h1']); ?></span> </a></li>
			<?php
			}else if($_GET['templates_id'] == 56){ // page notion
			$aInfos = $oDb->queryRow("SELECT * FROM bor_chapitres WHERE chapitre_alias ='/".$_GET['chapitre_url']."/'");
			?>
				<li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="/<?php echo strtolower($_GET['classe_url']); ?>/" itemprop="url"> <span itemprop="title"><?php echo ucfirst($iClasse); ?></span> </a></li>
				<li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="/<?php echo strtolower($_GET['classe_url'])."/".strtolower($_GET['matiere_url']); ?>/" itemprop="url"> <span itemprop="title"><?php echo ucfirst($iMatiere); ?></span> </a></li>
				<li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="/<?php echo strtolower($_GET['chapitre_url']) ;?>/" itemprop="url"> <span itemprop="title"><?php echo ucfirst($aInfos['chapitre_h1']); ?></span> </a></li>
				<li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="#" itemprop="url"> <span itemprop="title"><?php echo ucfirst($aNotion['notions_h1']); ?></span> </a></li>
			<?php
			}else if($_GET['templates_id'] == 54){ // fiche produit
			if($aProduit['formule_id'] == 1)
				$fiche = "formule CIBLÉE";
			else if($aProduit['formule_id'] == 2)
				$fiche = "formule RÉUSSITE";
			else if($aProduit['formule_id'] == 3)
				$fiche = "formule TRIBU";
			?>
				<li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="#" itemprop="url"> <span itemprop="title">Abonnement <?php echo $fiche; ?></span> </a></li>
			<?php
			}else{
			?>
				<li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="#" itemprop="url"> <span itemprop="title"><?php echo get_template_title(); ?></span> </a></li>
			<?php
			}
			?>
			</ol>
          </h6>
        </div>
      </div>
    </div>
  </div>