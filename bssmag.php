<?php
 //ini_set('error_reporting', E_ALL);
 // ini_set('display_errors', '1'); 
session_start();

define('ROOT', dirname(__FILE__));
$_CONST['path']["server"] = ROOT;

include_once(ROOT . "/lib/config.inc.php");
include_once(ROOT . "/lib/database.class.php");
include_once(ROOT . "/lib/JoForm.class.php");
include_once(ROOT . "/lib/functions.php");
include_once(ROOT . "/lib/intyMailer.php");
include_once(ROOT . "/lib/bill.class.php");
include_once(ROOT . "/lib/abo.class.php");
include_once(ROOT . "/lib/httpclient.php");
$isMobile = (bool) preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
$oDb      = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);

$oDb->query("SET NAMES utf8");
LoadConfig($_CONST); //load eco_config table IN $_CONST

$_SESSION["langs_id"] = 1;

/* ************************** */
global $aLang, $_CONST;


/* Traitement des partenaire */

if(isset($_GET["partenaire"]) && !empty($_GET["partenaire"])){
    
   
    $strUrlPartenaire = mysql_real_escape_string($_GET["partenaire"]) ; 
    //Récupération des infos du partenaire
    $strSql = "SELECT * FROM bor_bssmag WHERE activation_url = '".$strUrlPartenaire. "' LIMIT 1"; 
    $aPartenaire = $oDb->queryRow($strSql);
     
    if($oDb->rows > 0 ){
            $_SESSION['partenaire_activation'] = $aPartenaire ; 
    }else{
        echo "<script>document.location.href='". $_CONST["URL2"]."';</script>";
        exit; 
    }
}


if (!isset($_GET["template"]) && empty($_GET["template"])) 
    $_GET["template"] = "activation1" ;


if (isset($_GET["template"]) && !empty($_GET["template"])) {
    $strSql     = "SELECT templates_name , templates_id FROM eco_templates ";
    $aTemplates = $oDb->queryTab($strSql);
    foreach ($aTemplates as $aTemplate) {
        if ($_GET['template'] == strToUrl($aTemplate['templates_name'])) {
            $_GET['templates_id'] = $aTemplate['templates_id'];
            break;
        }
    }
}


/************************************/
// var_dump($_GET['id_rubrique']);
// var_dump($_GET['templates_id']);
?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8">
<link rel="icon" href="/favicon.ico" />
<link rel="icon" type="image/png" href="/favicon.png" />
<!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" /><![endif]-->
<link rel="shortcut icon" href="/favicon.ico" />
<!--SEO-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php
get_meta_title_desc();

$t = get_template_filename();




	include('scripts_cart.php');
	include('css_cart.php'); 


?>

<?php
    if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
?>
        <script>(function() {
        var _fbq = window._fbq || (window._fbq = []);
        if (!_fbq.loaded) {
          var fbds = document.createElement('script');
          fbds.async = true;
          fbds.src = '//connect.facebook.net/en_US/fbds.js';
          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(fbds, s);
          _fbq.loaded = true;
        }
        _fbq.push(['addPixelId', '796542543725095']);
      })();
      window._fbq = window._fbq || [];
      window._fbq.push(['track', 'PixelInitialized', {}]);
      </script>
      <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=796542543725095&amp;ev=PixelInitialized" /></noscript>
<?php
    }
?>
</head>

<body id="cart" class="reussite">
<?php 
  
	if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'   ){
		//Gestion code analytics partenaire
		$strCodeUA = $_CONST['TAG_ANALYTICS'] ;
		if(isset($_SESSION['partenaire']['tag_analytics']) && !empty($_SESSION['partenaire']['tag_analytics'])){
			$strCodeUA = $_SESSION['partenaire']['tag_analytics'] ; 
			?>
				<script>
					  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					  ga('create', '<?php echo $strCodeUA; ?>', 'auto', {'allowLinker' : true});
					  ga('require', 'linker');
                                          ga('require', 'displayfeatures');
					  ga('send', 'pageview');


					  function decorateElements(element) {
						  ga('linker:decorate', element);
					  }
			<?php
		}
		else{
			?>
				<script>
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					ga('create', '<?php echo $strCodeUA; ?>', 'bordas-soutien-scolaire.com');
                                        ga('require', 'displayfeatures');
					ga('send', 'pageview');
			<?php
		}

	if(  isset($_SESSION['user']['IDCLIENT']) && $_SESSION['user']['IDCLIENT'] > 0 ){
?>
			var NumWeb = '<?php echo $_SESSION['user']['IDCLIENTWEB']; ?>';
			ga('set', 'dimension1', NumWeb);
			var NumCli = '<?php echo $_SESSION['user']['NUMCLIENT']; ?>';
			ga('set', 'dimension2', NumCli);
<?php 
	}
?>
		</script>
		<!-- /GA -->
<?php 
	}
   

  include('header_bssmag.php');
?>
<!-- CONTAINER FULL WIDTH ============================================== -->
<!-- Contenue -->
<div class="container">
<section class="content">
<div class="modal fade" id="modal_cgv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content modal-content-newslettre">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>	
			<div class="modal-header">
				<h2><?php echo get_template_title(18); ?></h2>
			</div>
			<div class="modal-body row">
				<div class="col-md-6">
					<?php echo get_template_data(18); ?>
				</div>   
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_cgu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content modal-content-newslettre">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>	
			<div class="modal-header">
				<h2><?php echo get_template_title(20); ?></h2>
			</div>
			<div class="modal-body row">
				<div class="col-md-6">
					<?php echo get_template_data(20); ?>
				</div>   
			</div>
		</div>
	</div>
</div>
 <?php

get_pageCart(); 

 ?>
</section>
</div>
<!-- /.CONTAINER FULL WIDTH ============================================== --> 
<?php include('footer_activation.php'); ?>
</body>

</html>
<?php

	function get_pageCart() {
		global $_GET;
		global $oDb;
		global $_CONST;
		global $_SESSION;
		global $aLang;
		
		//Si templates_id vide alors par défaut = cart accueil
                $iTemplate = mysql_real_escape_string($_GET["templates_id"]) ;
		
		$strSQLSearch = "SELECT templates_title, templates_content, templates_filename
						 FROM eco_templates t
						 INNER JOIN eco_templates_lang tl ON (tl.templates_id=t.templates_id)
						 WHERE tl.templates_id='".mysql_escape_string($iTemplate)."' 
						 AND tl.langs_id='".mysql_escape_string($_SESSION["langs_id"])."'";
		$aPage = $oDb->queryRow($strSQLSearch);	
		
		// var_dump($aPage['templates_filename']);exit;
		if (empty($aPage['templates_filename'])) {
			echo $aPage['templates_content'];
		}	
		elseif (!empty($aPage['templates_filename']) && is_file('./templates/'.$aPage['templates_filename'])) {
			include ('./templates/'.$aPage['templates_filename']);
		} 
		// else
			// include ('./templates/form_abonnement2.php');
	}
	$oDb->close();
?>