<?php 
//phpinfo();exit;
	// ini_set('display_errors', '1');
	// error_reporting(E_ALL);

	header('Content-Type:text/html; charset=utf-8');
	define( 'ROOT', dirname( __FILE__ )."/..");
	echo "START CRON cron_add_product.php";
	
	include_once(ROOT."/lib/config.inc.php");
	// echo $_CONST['path']["server"]; exit; 
	$_CONST['path']["server"] =ROOT;
	$_CONST["DATABASE_TYPE"] ="mysql";
	include_once(ROOT."/lib/database.class.php");
	include_once(ROOT."/lib/functions.php");

	
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	$oDb->query("SET NAMES 'utf8'");
	LoadConfig($_CONST); //load eco_config table IN $_CONST
	
	// var_dump(ROOT); exit; 
	/**
	* @return string
	* @param string	nom du champ SQL
	* @param string	flux XML
	* @desc retourne le contenue du niveau 1 d'un champ XML
	*/
	function eco_getXMLfield($strField, $strXMLFlux) {
		if (eregi("<$strField>(.*)</$strField>",$strXMLFlux,$aFields)) {
			return $aFields[1];
		}
	}

	/**
	* @return string
	* @param string		$strChaine 	Date au format DD/MM/YYYY
	* @desc Formattage d'une date française au format de la base de donnée
	*/
	function dateStringToSql($strChaine)
	{
		$arr = split( "/", $strChaine);
		if (count($arr)>1) {
			$temp_contenu_date = "'".$arr[2]."-".$arr[1]."-".$arr[0]."'";
		} else {
			$temp_contenu_date = "'".$strChaine."'";
		}
		return($temp_contenu_date);
	}

	//Url yonix
	$strUrlYonixListe = $_CONST['URL_YONIX_LISTE'] ; 
	$strUrlYonixProduit = $_CONST['URL_YONIX_PRODUIT'] ; 
	$strTable = "bor_produit";
	$iNbPP = 20; //Nombre de produit par page


	//Création de la backup
	$strDate = date("YmdHis"); 
	$strSql = "CREATE TABLE {$strTable}_$strDate LIKE $strTable";
	$oDb->query($strSql);
	$strSql = "INSERT INTO {$strTable}_$strDate SELECT * FROM $strTable";
	$oDb->query($strSql);

	//On vide la table avant l'importation
	$oDb->query("TRUNCATE TABLE $strTable");
	
	//On réinitialise le champ mc_dispo de la table bor_matiere_classe 
	$oDb->query("UPDATE bor_matiere_classe SET mc_dispo = 0 ") ; 

	//On va récupérer la liste de tous les produits à importer
	$strContent = file_get_contents($strUrlYonixListe);
	//Récupération du contenu dans un objet xml
	$oProduits = new SimpleXMLElement($strContent);

	//Récupération du nombre de produit pour calculer le nombre de page à parcourir
	$iNbProduits = (int)$oProduits->nombre ;
	$iNbPages = ceil($iNbProduits / $iNbPP); 
	// $iNbPages = 1 ; 
	//On va parcourir toutes les pages de produits
	for($i = 1 ; $i <= $iNbPages ; $i++){
		$strUrlYonixPage = $strUrlYonixListe."?F_page=$i";
		$strContent = file_get_contents($strUrlYonixPage);
		//Récupération du contenu dans un objet xml
		$oProduits = new SimpleXMLElement($strContent);
		foreach($oProduits->reference as $oProduit){
			//Pour chaque produit nous allons lire le xml
			$strUrlYonixEan = $strUrlYonixProduit.$oProduit->ean13 ;
			// $strContent = file_get_contents($strUrlYonixEan);
			// $oDetailsProduit = new SimpleXMLElement($strContent);
			// var_dump($oDetailsProduit->titre); 
			add_product($oProduit->ean13);
		}
	}
	
	function add_product($strEan){
		global $oDb; 
		global $strUrlYonixProduit; 
		global $strTable; 
		$strUrlYonixEan = $strUrlYonixProduit.$strEan ;
		$strContent = file_get_contents($strUrlYonixEan);
		if(!empty($strContent)){
			$oDetailsProduit = new SimpleXMLElement($strContent);
			
			//Récupération des champs à importer en base
			$aChamps["matiere_id_yonix"] ="";
			$aChamps["classe_id_yonix"] ="";
			$aChamps["formule_id_yonix"] ="";
			$aChamps["de_id_yonix"] ="";
			if(isset($oDetailsProduit->index_m1->index_m1_item)){
				$aAttribut= $oDetailsProduit->index_m1->index_m1_item->attributes(); 
				$aChamps["matiere_id_yonix"] = (string)$aAttribut["id"] ; 
				$aVal["matiere_libelle"] = (string)$oDetailsProduit->index_m1->index_m1_item ; 
				// var_dump($oDetailsProduit->index_m1->index_m1_item); 
			}
			if(isset($oDetailsProduit->index_m2->index_m2_item)){
				$aAttribut= $oDetailsProduit->index_m2->index_m2_item->attributes(); 
				$aChamps["classe_id_yonix"] = (string)$aAttribut["id"] ; 
				$aVal["classe_libelle"] = (string)$oDetailsProduit->index_m2->index_m2_item ; 
				// var_dump($aChamps["classe_libelle"] );
			}
			if(isset($oDetailsProduit->index_m3->index_m3_item)){
				$aAttribut= $oDetailsProduit->index_m3->index_m3_item->attributes(); 
				$aChamps["formule_id_yonix"] = (string)$aAttribut["id"] ; 
			}
			if(isset($oDetailsProduit->index_m4->index_m4_item)){
				$aAttribut= $oDetailsProduit->index_m4->index_m4_item->attributes(); 
				$aChamps["de_id_yonix"] = (string)$aAttribut["id"] ; 
			}
			
			$aChamps["produit_ean"] = isset($oDetailsProduit->ean13) ? (string)$oDetailsProduit->ean13 : "";
			$aChamps["produit_titre"] = isset($oDetailsProduit->titre) ? (string)$oDetailsProduit->titre : "";
			$aChamps["produit_support"] = isset($oDetailsProduit->support) ? (string)$oDetailsProduit->support : "";
			$aChamps["produit_isbn"] = isset($oDetailsProduit->isbn) ? (string)$oDetailsProduit->isbn : "";
			$aChamps["produit_prix"] = isset($oDetailsProduit->prix_euro_ttc) ? (string)str_replace(",",".",$oDetailsProduit->prix_euro_ttc): "";
			$aChamps["produit_actif"] = 1 ;
			
			//modif Ecomiz 20 10 2015
			//$aChamps["produit_presentation"] = isset($oDetailsProduit->extrait_presse) ? (string)$oDetailsProduit->extrait_presse : "";
			$aChamps["produit_accroche_yonix"] = isset($oDetailsProduit->accroche) ? (string)$oDetailsProduit->accroche : "";
			$aChamps["produit_presentation_yonix"] = isset($oDetailsProduit->presentation_internet) ? (string)$oDetailsProduit->presentation_internet : "";
			$aChamps["produit_prix_ht"] = isset($oDetailsProduit->prix_euro_ht) ? (string)str_replace(",",".",$oDetailsProduit->prix_euro_ht): "";
			
			//modif Ecomiz 17 02 2016
			//ajout produit_date_creation et produit_date_modification
			$aChamps["produit_date_creation"] = date("Y-m-d H:i:s");
			$aChamps["produit_date_modification"] = "0000-00-00 00:00:00";

			//Insertion du produit 
			//Création dynamiquement des champs select et value
			$strSelect = ""; 
			$strValue = ""; 
			foreach($aChamps as $strChamp => $strVal){
				$strSelect .= $strChamp .' ,' ; 
				$strValue .= "'".mysql_real_escape_string($strVal)."' ,"; 
			}
			//Suppression des virgule à la fin
			$strSelect = substr($strSelect,0,-1);
			$strValue = substr($strValue,0,-1);
			//Insertion
			$strSql = "INSERT INTO $strTable ($strSelect) VALUES ($strValue)"; 
			$oDb->query($strSql);
			
			//Si formule cible on active la matière/classe correspondante
			// echo $aChamps["matiere_id_yonix"] .'  '. $aChamps["classe_id_yonix"]. '<br/>';
			if(!empty($aChamps["matiere_id_yonix"]) && !empty($aChamps["classe_id_yonix"])){
				// echo $aChamps["matiere_id_yonix"] . ' - ' . $aChamps["classe_id_yonix"]. '<br>'; 
				//Insertion de la classe si elle n'existe pas
				$strSql = "SELECT classe_id FROM bor_classe WHERE classe_id_yonix = '".mysql_real_escape_string($aChamps["classe_id_yonix"])."'"; 
				$iClasseId = $oDb->queryItem($strSql);
				if(!($oDb->rows)){//Insertion de la classe
					$strSql = "INSERT INTO bor_classe (classe_actif, classe_name, classe_id_yonix) VALUES ( 0, '".mysql_real_escape_string($aVal["classe_libelle"])."' ,  '".mysql_real_escape_string($aChamps["classe_id_yonix"])."')"; 
					// echo $strSql ."<br>"; 
					$oDb->query($strSql);
					$iClasseId = $oDb->last_insert_id; 
				}
				//Insertion de la matière si elle n'existe pas
				$strSql = "SELECT matiere_id FROM bor_matiere WHERE matiere_id_yonix = '".mysql_real_escape_string($aChamps["matiere_id_yonix"])."'"; 
				$iMatiereId = $oDb->queryItem($strSql);
				if(!($oDb->rows)){//Insertion de la matiere
					$strSql = "INSERT INTO bor_matiere (matiere_actif, matiere_titre, matiere_id_yonix) VALUES ( 0, '".mysql_real_escape_string($aVal["matiere_libelle"])."' ,  '".mysql_real_escape_string($aChamps["matiere_id_yonix"])."')"; 
					// echo $strSql."<br>"; 
					$oDb->query($strSql);
					$iMatiereId = $oDb->last_insert_id; 
				}
				
				//Vérification de la liaison classe/matiere
				$strSql = "SELECT mc.mc_id 
								FROM bor_matiere_classe mc 
								INNER JOIN bor_classe c ON (c.classe_id = mc.classe_id) 
								INNER JOIN bor_matiere m ON (m.matiere_id = mc.matiere_id)
							WHERE c.classe_id_yonix = '".mysql_real_escape_string($aChamps["classe_id_yonix"])."'
							AND  m.matiere_id_yonix = '".mysql_real_escape_string($aChamps["matiere_id_yonix"])."'"; 
				$iMcId = $oDb->queryItem($strSql); 
				if(!$oDb->rows) {//Insertion de la combinaison
					$strSql = "INSERT INTO bor_matiere_classe (matiere_id, classe_id, mc_actif, mc_dispo) VALUES ('".mysql_real_escape_string($iMatiereId)."', '".mysql_real_escape_string($iClasseId)."', 0, 1)"; 
					// echo $strSql."<br>"; 
					$oDb->query($strSql);
				}else
					$oDb->query("UPDATE bor_matiere_classe SET mc_dispo = 1 WHERE mc_id = $iMcId"  );	
				
				
			}
		}
	}
	
	echo "<br>END CRON cron_add_product.php";


?>