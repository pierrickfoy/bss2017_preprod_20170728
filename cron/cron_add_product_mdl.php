<?php 
	header('Content-Type:text/html; charset=utf-8');
	define( 'ROOT', dirname( __FILE__ )."/..");
	echo "START CRON cron_add_product_mdl.php<br>";
	
	include_once(ROOT."/lib/config.inc.php");
	// echo $_CONST['path']["server"]; exit; 
	$_CONST['path']["server"] =ROOT;
	$_CONST["DATABASE_TYPE"] ="mysql";
	include_once(ROOT."/lib/database.class.php");
	include_once(ROOT."/lib/functions.php");

	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	$oDb->query("SET NAMES 'utf8'");

	/* Importation des constantes */
	LoadConfig($_CONST); 
	
	/* Debug if mode dev */
	if( $_CONST['TYPE_ENVIRONNEMENT'] == "DEV" || $_CONST['TYPE_ENVIRONNEMENT'] == "dev"){
		ini_set('display_errors', '1');
		ini_set('error_reporting', E_ALL);
	}else{
		ini_set('display_errors', '0');
		ini_set('error_reporting', 0);
	}
	set_time_limit(3000);
	ini_set('default_socket_timeout',3600);
	ini_set('max_input_time',3600);
	
	//Récupération de la liste des collections
	$strSql = "SELECT collection_idyonix FROM bor_collection_mdl WHERE collection_actif = 1"; 
	$aCollections = $oDb->queryTab($strSql);
	
	if($oDb->rows > 0 ){	
		foreach($aCollections as $aCollection){
			//Récupération de l'url à intérroger à prtir de la confi du BO
			if(isset($_CONST['URL_YONIX_LISTE_MDL']) && !empty($_CONST['URL_YONIX_LISTE_MDL'])){
				// $strUrl = $_CONST['URL_YONIX_LISTE_MDL']."?F_collection=".$aCollection['collection_idyonix'];
				$strUrl = $_CONST['URL_YONIX_LISTE_MDL']."?F_index_m4=62940";
				$ch = curl_init($strUrl);
				curl_setopt($ch, CURLOPT_HEADER, FALSE);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				$strXML = curl_exec($ch);
				curl_close($ch);
				//Execution que si on a récupérer un XML 
				if(isset($strXML) && !empty($strXML)){
					$oXmlDoc = new DOMDocument();
					$oXmlDoc->loadXML($strXML);
					$oReferences = $oXmlDoc->getElementsByTagName('references')->item(0);
					
					if( isset($oReferences) ){
				
						//Récupération du nombre d'ouvrages au total et donc le nombre de page
						$iReferences = $oReferences->getElementsByTagName('nombre')->item(0)->nodeValue;
						$iPage = round($iReferences/20) ;
						if($iPage == 0)
							$iPage = 1 ; 
							// var_dump($iPage);
				
						// On va boucler sur le nombre de page pour importer tous les ouvrages
						for( $i = 1; $i <= $iPage; $i++){
							importer_page($i,$aCollection['collection_idyonix']);
						}				
					} // if( isset($oReferences) ){
				} // if(isset($strXML) && !empty($strXML)){		
			} // if(isset($_CONST['URL_YONIX_LISTE_MDL']) && !empty($_CONST['URL_YONIX_LISTE_MDL'])){
		}
	}
	
	//Fonction pour récupérer la liste de tous les ouvrages présent dans une page
	function importer_page($iPage, $iCollection){
		global $oDb;
		global $_CONST; 
		
		//Récupération de l'url à intérroger à prtir de la confi du BO et du numéro de la page
		if(isset($_CONST['URL_YONIX_LISTE_MDL']) && !empty($_CONST['URL_YONIX_LISTE_MDL'])){
			$strUrl = $_CONST['URL_YONIX_LISTE_MDL']."?F_collection=".$iCollection."&F_page=$iPage";
			$ch = curl_init($strUrl);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			$strXML = curl_exec($ch);
			curl_close($ch);
			
			//Execution que si on a récupérer un XML 
			if(isset($strXML) && !empty($strXML)){
				$oXmlDoc = new DOMDocument();
				$oXmlDoc->loadXML($strXML);
				$oReferences = $oXmlDoc->getElementsByTagName('references')->item(0);
				$oReference = $oReferences->getElementsByTagName('reference');
				if( isset($oReference) ){
					foreach($oReference as $oOuvrage) {
						$strEan = $oOuvrage->getElementsByTagName('ean13')->item(0)->nodeValue ;
						if(isset($strEan) && !empty($strEan))
							importer_ouvrage($strEan);
					} // foreach($oReference as $oOuvrage) {
				} // if( isset($oReferences) ){
			} // if(isset($strXML) && !empty($strXML)){		
		} // if(isset($_CONST['URL_YONIX_LISTE_MDL']) && !empty($_CONST['URL_YONIX_LISTE_MDL'])){
	}
	
	
	
	//Fonction pour importer les infos d'un ouvrage
	function importer_ouvrage($strEan){
		global $_CONST; 
		global $oDb;
		//Récupération de la liste des offres disponible 
		$strSql = "SELECT offre_id_yonix FROM bor_offre "; 
		$aOffres = $oDb->queryTab($strSql);
		foreach ($aOffres as $aOffre)
			$aListeOffres[] = $aOffre['offre_id_yonix']; 
		
		if(isset($_CONST['URL_YONIX_OUVRAGE']) && !empty($_CONST['URL_YONIX_OUVRAGE'])){
			$strUrl = $_CONST['URL_YONIX_OUVRAGE'].$strEan;
			
			$ch = curl_init($strUrl);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			$strXML = curl_exec($ch);
			curl_close($ch);
			//Execution que si on a récupérer un XML 
			if(isset($strXML) && !empty($strXML)){
				$oXmlDoc = new DOMDocument();
				$oXmlDoc->loadXML($strXML);
				$oReference = $oXmlDoc->getElementsByTagName('reference')->item(0);
				$strIsbn = isset($oReference->getElementsByTagName('isbn')->item(0)->nodeValue) ? $oReference->getElementsByTagName('isbn')->item(0)->nodeValue : '' ;
				$strTitre = isset($oReference->getElementsByTagName('titre')->item(0)->nodeValue) ? $oReference->getElementsByTagName('titre')->item(0)->nodeValue : '' ;
				$strDateParution = isset($oReference->getElementsByTagName('date_parution')->item(0)->nodeValue) ? $oReference->getElementsByTagName('date_parution')->item(0)->nodeValue : '' ;
				$strImage  = isset($oReference->getElementsByTagName('image_grand_format')->item(0)->nodeValue) ? $oReference->getElementsByTagName('image_grand_format')->item(0)->nodeValue : '' ;
				$strImageDateUpd  = isset($oReference->getElementsByTagName('image_grand_format')->item(0)->nodeValue) ? $oReference->getElementsByTagName('image_grand_format')->item(0)->getAttribute("date_maj") : '' ;
				$strCollectionId  = isset($oReference->getElementsByTagName('collection')->item(0)->nodeValue) ? $oReference->getElementsByTagName('collection')->item(0)->getAttribute("id") : '' ;
				$strCollection = isset($oReference->getElementsByTagName('collection')->item(0)->nodeValue) ? $oReference->getElementsByTagName('collection')->item(0)->nodeValue : '' ;
				
				// var_dump($strCollection); exit; 
				//Récupération de item4
				$oItems = $oReference->getElementsByTagName('index_m4')->item(0);
				$bInsert = false; 
				if(isset($oItems)){
					$oItem = $oItems->getElementsByTagName('index_m4_item');
					if( isset($oItem) ){
						$aOffre = array(); 
						$i=0; 
						foreach($oItem as $oIt) {
							//Vérification si l'ouvrage est associé à une offre 
							if( in_array($oIt->getAttribute("id"), $aListeOffres)){
								$aOffre['id'] = $oIt->getAttribute("id");
								$aOffre['Name'] = $oIt->nodeValue ;
								$bInsert = true; 
								break; 								
							}
							$i++;
							
						}
					}
				}
				
				if($bInsert){
					$aOuvrage = array 	(
											"strEan" => $strEan,
											"strIsbn" => $strIsbn ,
											"strOffre" => $aOffre['id'] ,
											"strTitre" => $strTitre ,
											"strDateParution" => $strDateParution ,
											"strImage" => $strImage ,
											"strImageDateUpd" => $strImageDateUpd ,
											"strCollectionId" => $strCollectionId ,
											"strCollection" => $strCollection 
										);
					ajouter_ouvrage($aOuvrage);
				}
				// var_dump($aOuvrage);exit;
			} // if(isset($strXML) && !empty($strXML)){		
		} // if(isset($_CONST['URL_YONIX_LISTE_MDL']) && !empty($_CONST['URL_YONIX_LISTE_MDL'])){	
	}
	
	//Fonction pour ajouter un ouvrage à la liste 
	function ajouter_ouvrage($aOuvrage){
		global $_CONST; 
		global $oDb; 
		// var_dump('test');exit;
		if(is_array($aOuvrage)){
		
			//Récupération des clés étrangères 
			//Série 
			// var_dump($aOuvrage['strSerie'] );exit;
			
			
			 
			//Vérification si l'ouvrage est déjà présent en base 
			$strSql = "SELECT ouvrage_id , ouvrage_date_image, ouvrage_upd FROM bor_ouvrage WHERE ouvrage_ean = '".$aOuvrage['strEan']."' LIMIT 1"; 
			$tabOuvrage = $oDb->queryRow($strSql);
			
			$bClasses = false; // Booléen pour mettre à jour la table ouvrage_classe que si nous avons mis à jour l'ouvrage
			if($oDb->rows > 0 ){
				$iOuvrageId = $tabOuvrage['ouvrage_id'];
				//L'ouvrage est déjà présent en base, on va juste effectué une mise à jour des données => On le met à jour que si le flag mise à jour est initialisé
				if(isset($_CONST['FORCER_UPD']) && $_CONST['FORCER_UPD'] == 1){		
					//Vérification si l'ouvra n'est pas bloqué
					if($tabOuvrage['ouvrage_upd'] == 1){
						$bClasses = true; 
						$strSql = "	UPDATE bor_ouvrage SET 
															ouvrage_titre = '".mysql_real_escape_string($aOuvrage['strTitre'])."',
															ouvrage_isbn = '".mysql_real_escape_string($aOuvrage['strIsbn'])."',
															ouvrage_offre_idyonix = '".mysql_real_escape_string($aOuvrage['strOffre'])."',
															ouvrage_date_parution = '".convertir_date($aOuvrage['strDateParution'])."',
															ouvrage_id_collection = '".mysql_real_escape_string($aOuvrage['strCollectionId'])."',
															ouvrage_nom_collection = '".mysql_real_escape_string($aOuvrage['strCollection'])."',
															ouvrage_date_upd = NOW()
										WHERE ouvrage_id = $iOuvrageId "; 
									
						$oDb->query($strSql); 
						//Vérification si on doit mettre à jour l'image 
						if($aOuvrage['strImageDateUpd'] > $tabOuvrage['ouvrage_date_image'] ){
							$strSql = "	UPDATE bor_ouvrage SET 
															ouvrage_image_grand_format = '".mysql_real_escape_string($aOuvrage['strImage'])."',
															ouvrage_date_image = '".$aOuvrage['strImageDateUpd']."'														
										WHERE ouvrage_id = $iOuvrageId "; 
							$oDb->query($strSql);
							//On met à jour le fichier image 
							$strImage = file_get_contents($_CONST['IMAGE_URL'].$aOuvrage['strEan'].'.gif'); 
							// var_dump($strImage); 
							if(!empty($strImage)){
								//On supprime l'image actuelle de la table eco_files 
								$strSql = "DELETE FROM eco_files WHERE files_table_id ='$iOuvrageId' AND files_table_source='bor_ouvrage' AND files_field_name='ouvrage_image'"; 
								$oDb->query($strSql); 
								$fp = fopen("../uploads/ouvrages/{$iOuvrageId}.gif", 'w');
								fwrite($fp, $strImage);
								fclose($fp);
								$strSql= "INSERT INTO eco_files (files_path,files_name,files_table_source,files_table_id_name,files_field_name,files_table_id,files_date)
											VALUES ('../uploads/ouvrages/{$iOuvrageId}.gif','".$aOuvrage['strImage']."','bor_ouvrage','ouvrage_id','ouvrage_image','$iOuvrageId',NOW())";
								$oDb->query($strSql); 
							}
						}
					}
				}				
			}else{
				//L'ouvrage n'est pas présent en base => Insertion
				$strSql = "INSERT INTO bor_ouvrage	(
														ouvrage_ean,
														ouvrage_titre,
														ouvrage_isbn,
														ouvrage_offre_idyonix,
														ouvrage_image_grand_format,
														ouvrage_date_image,
														ouvrage_date_parution,
														ouvrage_date_add,
														ouvrage_date_upd,
														ouvrage_actif, 
														ouvrage_upd,
														ouvrage_id_collection,
														ouvrage_nom_collection
													) 
												VALUES	(
															'".mysql_real_escape_string($aOuvrage['strEan'])."',
															'".mysql_real_escape_string($aOuvrage['strTitre'])."',
															'".mysql_real_escape_string($aOuvrage['strIsbn'])."',
															'".mysql_real_escape_string($aOuvrage['strOffre'])."',
															'".mysql_real_escape_string($aOuvrage['strImage'])."',
															'".mysql_real_escape_string($aOuvrage['strImageDateUpd'])."',
															'".convertir_date($aOuvrage['strDateParution'])."',
															NOW(),
															NOW(),
															1,
															1,
															'".mysql_real_escape_string($aOuvrage['strCollectionId'])."',
															'".mysql_real_escape_string($aOuvrage['strCollection'])."'
														)";
				$oDb->query($strSql); 
				$iOuvrageId = $oDb->last_insert_id; 
				$bClasses = true; 
				//On met à jour le fichier image 
				$strImage = file_get_contents($_CONST['IMAGE_URL'].$aOuvrage['strEan'].'.gif'); 
				// var_dump($strImage); 
				if(!empty($strImage)){
					$fp = fopen("../uploads/ouvrages/{$iOuvrageId}.gif", 'w');
					fwrite($fp, $strImage);
					fclose($fp);
					$strSql= "INSERT INTO eco_files (files_path,files_name,files_table_source,files_table_id_name,files_field_name,files_table_id,files_date)
								VALUES ('../uploads/ouvrages/{$iOuvrageId}.gif','".$aOuvrage['strImage']."','bor_ouvrage','ouvrage_id','ouvrage_image','$iOuvrageId',NOW())";
					$oDb->query($strSql); 
				}
			}
			
			
		} //if(is_array($aOuvrage)){
	}

	
	function convertir_date($strDate, $strFormat="FrtoEn"){
		if($strFormat == "FrtoEn"){
			$aTmpDate = explode("/",$strDate); 
			$strDate = $aTmpDate[2].'-'.$aTmpDate[1].'-'.$aTmpDate[0]; 
		}else{
			$aTmpDate = explode("-",$strDate); 
			$strDate = $aTmpDate[2].'/'.$aTmpDate[1].'/'.$aTmpDate[0];
		}
		return $strDate;
	}
	
	echo "END CRON cron_add_product_mdl.php";
	
	
		
	
?>