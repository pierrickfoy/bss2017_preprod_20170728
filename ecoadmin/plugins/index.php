<?php 
	ini_set('error_reporting', E_ALL); // DEV
	header('Content-Type: text/html; charset=iso-8859-1');
	session_start();
	define( 'ROOT', dirname( __FILE__ ).'/..');
	$_CONST['path']["server"] = ROOT;
	
	include_once(ROOT."/lib/login.class.php");
	include_once(ROOT."/lib/config.inc.php");
	include_once(ROOT."/lib/database.class.php");
	include_once(ROOT."/lib/JoForm.class.php");
	include_once(ROOT."/lib/JoTable.class.php");
	//include_once(ROOT."/lib/stats.class.php");
	include_once(ROOT."/lib/googlecharts.class.php");
	include_once(ROOT."/lib/functions.php");
	
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	
	/**** EXECUTION AJAX A PARTIR DU TEMPLATE FORM_AVIS.PHP ****/
	if(isset($_POST['save_avis']) && $_POST['save_avis'] =='click' && isset($_POST['idAvis']) && $_POST['idAvis'] > 0 ){
		$oDb->query("UPDATE ttp_avis SET avis_accueil = !avis_accueil WHERE avis_id = '".mysql_escape_string($_POST['idAvis'])."'");
		exit;		
	}
	
	LoadConfig($_CONST); //load eco_config table IN $_CONST
	
	/* LOGIN DATA */
	$oLogin = new EcoLogin($_CONST["session_name"], 'Talk To Play');
	if (isset($_GET["action"]) && $_GET["action"] ==  "logout")
		$oLogin->Logout();
	if (isset($_POST['login']) && isset($_POST['password']) && !empty($_POST['login']) && !empty($_POST['password']))
		if ($oLogin->LogMe($_POST['login'], $_POST['password']) == false)
			header('Location: login.html');
	
	//check if loged in
	if (!isset($_SESSION[$_CONST["session_name"]]["users_id"]) || empty($_SESSION[$_CONST["session_name"]]["users_id"]))
		header('Location: login.html');
	/* END LOGIN */	
	
	
	

?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Administration</title>
    <meta name="author" content="EcomiZ" />
    <meta name="description" content="Moviken Dashboard" />
    <meta name="application-name" content="Moviken Dashboard" />

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Le styles -->
    <!-- Use new way for google web fonts 
    http://www.smashingmagazine.com/2012/07/11/avoiding-faux-weights-styles-google-web-fonts -->
    <!-- Headings -->
    <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />  -->
    <!-- Text -->
    <!-- <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' /> --> 
    <!--[if lt IE 9]>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Droid+Sans:400" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Droid+Sans:700" rel="stylesheet" type="text/css" />
    <![endif]-->

    <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
    <link href="css/supr-theme/jquery.ui.supr.css" rel="stylesheet" type="text/css"/>
    <link href="css/icons.css" rel="stylesheet" type="text/css" />
    <!-- Plugin stylesheets -->
    <link href="plugins/qtip/jquery.qtip.css" rel="stylesheet" type="text/css" />
    <link href="plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
    <link href="plugins/jpages/jPages.css" rel="stylesheet" type="text/css" />
    <link href="plugins/prettify/prettify.css" type="text/css" rel="stylesheet" />
    <link href="plugins/inputlimiter/jquery.inputlimiter.css" type="text/css" rel="stylesheet" />
    <link href="plugins/ibutton/jquery.ibutton.css" type="text/css" rel="stylesheet" />
    <link href="plugins/uniform/uniform.default.css" type="text/css" rel="stylesheet" />
    <link href="plugins/color-picker/color-picker.css" type="text/css" rel="stylesheet" />
    <link href="plugins/select/select2.css" type="text/css" rel="stylesheet" />
    <link href="plugins/validate/validate.css" type="text/css" rel="stylesheet" />
    <link href="plugins/pnotify/jquery.pnotify.default.css" type="text/css" rel="stylesheet" />
    <link href="plugins/pretty-photo/prettyPhoto.css" type="text/css" rel="stylesheet" />
    <link href="plugins/smartWizzard/smart_wizard.css" type="text/css" rel="stylesheet" />
    <link href="plugins/dataTables/jquery.dataTables.css" type="text/css" rel="stylesheet" />
    <link href="plugins/elfinder/elfinder.css" type="text/css" rel="stylesheet" />
    <link href="plugins/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css" type="text/css" rel="stylesheet" />
    <link href="plugins/search/tipuesearch.css" type="text/css" rel="stylesheet" />

    <!-- Main stylesheets -->
    <link href="css/main.css" rel="stylesheet" type="text/css" /> 
    <!-- Right to left version 
    <link href="css/rtl.css" rel="stylesheet" type="text/css" /> -->

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="images/favicon.ico" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/apple-touch-icon-144-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/apple-touch-icon-114-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon-57-precomposed.png" />
    
    <script type="text/javascript">
        //adding load class to body and hide page
        //document.documentElement.className += 'loadstate';
    </script>
	
	 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>

    <!-- Load plugins -->
    <script type="text/javascript" src="plugins/qtip/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.grow.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.tooltip_0.4.4.js"></script>
    <script type="text/javascript" src="plugins/flot/jquery.flot.orderBars.js"></script>
	<script type="text/javascript" src="plugins/flot/jquery.flot.time.js"></script>
	
    <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="plugins/knob/jquery.knob.js"></script>
    <script type="text/javascript" src="plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="plugins/prettify/prettify.js"></script>

    <script type="text/javascript" src="plugins/watermark/jquery.watermark.min.js"></script>
    <script type="text/javascript" src="plugins/elastic/jquery.elastic.js"></script>
    <script type="text/javascript" src="plugins/inputlimiter/jquery.inputlimiter.1.3.min.js"></script>
    <script type="text/javascript" src="plugins/maskedinput/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/ibutton/jquery.ibutton.min.js"></script>
    <script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="plugins/stepper/ui.stepper.js"></script>
    <script type="text/javascript" src="plugins/color-picker/colorpicker.js"></script>
    <script type="text/javascript" src="plugins/timeentry/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="plugins/select/select2.min.js"></script>
    <script type="text/javascript" src="plugins/dualselect/jquery.dualListBox-1.3.min.js"></script>
    <script type="text/javascript" src="plugins/tiny_mce/jquery.tinymce.js"></script>
	
	 <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="plugins/search/tipuesearch_set.js"></script>
    <script type="text/javascript" src="plugins/search/tipuesearch_data.js"></script><!-- JSON for searched results -->
    <script type="text/javascript" src="plugins/search/tipuesearch.js"></script>

    <script type="text/javascript" src="plugins/animated-progress-bar/jquery.progressbar.js"></script>
    <script type="text/javascript" src="plugins/pnotify/jquery.pnotify.min.js"></script>
    <script type="text/javascript" src="plugins/lazy-load/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="plugins/jpages/jPages.min.js"></script>
    <script type="text/javascript" src="plugins/pretty-photo/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="plugins/smartWizzard/jquery.smartWizard-2.0.min.js"></script>

    <script type="text/javascript" src="plugins/ios-fix/ios-orientationchange-fix.js"></script>

    <script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/elfinder/elfinder.min.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.js"></script>
    <script type="text/javascript" src="plugins/plupload/plupload.html4.js"></script>
    <script type="text/javascript" src="plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>
    <script type="text/javascript" src="plugins/totop/jquery.ui.totop.min.js"></script> 
	
	<?php 
	if(isset($_GET['id']) && !empty($_GET['id'])){
	?>
		<script type="text/javascript" src="plugins/jquery-1.9.1.js"></script>
		<script type="text/javascript">
			var jQueryR = $.noConflict();
		</script>	
		<script type="text/javascript" src="plugins/ajaxupload.js"></script>
	<?php 
	}
	?>
		
</head>
      
    <body>
    <!-- loading animation
    <div id="qLoverlay"></div>
    <div id="qLbar"></div>  -->
    
    <div id="header">

        <div class="navbar">
            <div class="navbar-inner">
              <div class="container-fluid">
                <a class="brand" href="index.php"><img src="images/logo.png" alt="logo" style="width: 75px;"/></a>
                <div class="nav-no-collapse">
                    <ul class="nav">
                        <li class="active"><a href="/reporting_API/"><span class="icon16 icomoon-icon-screen-2"></span> Accueil</a></li>
                    </ul>
                  
                    <ul class="nav pull-right usernav">
                
                        <li >
                            <a href="#" class="dropdown-toggle avatar" >
                                <span class="txt"><?php echo $_SESSION[$_CONST["session_name"]]["users_nomprenom"]; ?></span>
                                <b class="caret"></b>
                            </a>
							
                        </li>
                        <li><a href="index.php?action=logout"><span class="icon16 icomoon-icon-exit"></span> Logout</a></li>
                    </ul>
                </div><!-- /.nav-collapse -->
              </div>
            </div><!-- /navbar-inner -->
          </div><!-- /navbar --> 

    </div><!-- End #header -->

    <div id="wrapper">

        <!--Responsive navigation button-->  
        <div class="resBtn">
            <a href="#"><span class="icon16 minia-icon-list-3"></span></a>
        </div>
        
        <!--Left Sidebar collapse button-->  
        <!-- <div class="collapseBtn leftbar">
             <a href="#" class="tipR" title="Hide Left Sidebar"><span class="icon12 minia-icon-layout"></span></a>
        </div>-->

        <!--Sidebar background-->
        <div id="sidebarbg"></div>
        <!--Sidebar content-->
        <div id="sidebar">

            <!-- <div class="shortcuts">
                <ul>
                    <li><a href="support.html" title="Support section" class="tip"><span class="icon24 icomoon-icon-support"></span></a></li>
                    <li><a href="#" title="Database backup" class="tip"><span class="icon24 icomoon-icon-database"></span></a></li>
                    <li><a href="charts.html" title="Sales statistics" class="tip"><span class="icon24 icomoon-icon-pie-2"></span></a></li>
                    <li><a href="#" title="Write post" class="tip"><span class="icon24 icomoon-icon-pencil"></span></a></li>
                </ul>
            </div>--> 
			<!-- End search -->             

            <div class="sidenav">

                <div class="sidebar-widget" style="margin: -1px 0 0 0;">
                    <h5 class="title" style="margin-bottom:0">Navigation</h5>
                </div><!-- End .sidenav-widget -->

                <div class="mainnav">
                    <ul>
						<?php
							$aMenu = $oDb->queryTab("SELECT * FROM eco_bo_menu WHERE menu_visible=1 ORDER BY menu_poids");
							foreach ($aMenu as $aMenuItem) {
								$iRightLevel = $oDb->queryItem('SELECT rights_level FROM eco_bo_rights WHERE users_id='.$_SESSION[$_CONST['session_name']]['users_id'].' AND menu_id='.$aMenuItem['menu_id']);
								if ($oDb->rows == 1 && $iRightLevel >0 )
									echo ' <li><a href="index.php?template='.$aMenuItem['menu_template'].'"><span class="icon16 '.$aMenuItem['menu_icon'].'"></span>'.$aMenuItem['menu_title'].'</a></li>';
							}
							
						?>
                    </ul>
                </div>
            </div><!-- End sidenav -->


        </div><!-- End #sidebar -->
		
		
		
		 <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->

                
                <!-- Build page from here: -->
				<?php

					
					if (empty($_GET["template"]) || !file_exists('.'.$_CONST["path"]["templates"].'form_'.$_GET["template"].'.php')){
						$_GET["template"] = "home";
						
			
						$iRightLevel_tpl = $oDb->queryItem('SELECT rights_level 
														FROM eco_bo_rights r
														INNER JOIN eco_bo_menu m ON m.menu_id=r.menu_id
														WHERE users_id='.$_SESSION[$_CONST['session_name']]['users_id'].' AND menu_template=\''.mysql_escape_string($_GET["template"]).'\'');
						if (!file_exists('.'.$_CONST["path"]["templates"].'form_'.$_GET["template"].'.php'))
							echo 'Aucune page trouv�e';
						else
							include ('.'.$_CONST["path"]["templates"].'form_'.$_GET["template"].'.php');
					}else{  
						$iRightLevel_tpl = $oDb->queryItem('SELECT rights_level 
														FROM eco_bo_rights r
														INNER JOIN eco_bo_menu m ON m.menu_id=r.menu_id
														WHERE users_id='.$_SESSION[$_CONST['session_name']]['users_id'].' AND menu_template=\''.mysql_escape_string($_GET["template"]).'\'');
						include ('.'.$_CONST["path"]["templates"].'form_'.$_GET["template"].'.php');
					}
				?>
		
			</div><!-- End contentwrapper -->
        </div><!-- End #content -->
    
    </div><!-- End #wrapper -->
    
    <!-- Le javascript
    ================================================== -->
   
   

    <!-- Init plugins -->  <!-- 
    <script type="text/javascript" src="js/statistic.js"></script> 
	-->
	<!-- Control graphs ( chart, pies and etc) -->
	<?php
	/*	if (isset($aChartsData)) {
			$oCharts = new Stats($aChartsData);
			$oCharts->PrintCharts();
		}*/
	?>
    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="plugins/jquery-ui-1.10.3.custom.min.js"></script>
    
    <script type="text/javascript" src="js/main.js"></script>


    </body>
</html>
