<?php 
//Script pour l'export des resiliations 
define( 'ROOT', dirname( __FILE__ )."/.." );
$_CONST['path']["server"] = ROOT;
include_once(ROOT."/lib/login.class.php");
include_once(ROOT."/lib/config.inc.php");
include_once(ROOT."/lib/database.class.php");

$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	$oDb->query("SET NAMES 'utf8' ");
	
$strDateNow = date('Y_m_d_H_m_s');
// Modif preprod 2017 06 22 : bug création fichier csv :
// $strNameFile = ROOT.'/ecoadmin/files/Export_resiliation_'.$strDateNow.'.csv' ;
$strNameFile = dirname( __FILE__ ).'/files/Export_resiliation_'.$strDateNow.'.csv' ;
$fp = fopen($strNameFile, 'w');

$aExplain = $oDb->queryRow("SELECT 'id resiliation', 'id commande', 'duree','formule' ,'q1' ,'q2' ,'q3' ,'q4' ,'q5' ,'q6', 'expression'");
$iCompteur = 0;
$aCsv = array();
foreach($aExplain as $iField => $aField){
	if(!is_numeric($iField))
		$aCsv[0][] =  $aField;
}
$iCompteur++;

$strSql = "SELECT r.resiliation_id as 'id resiliation', r.commande_id as 'id commande', r.duree, r.formule,
questions as 'q1',
questions as 'q2',
questions as 'q3',
questions as 'q4',
questions as 'q5',
questions as 'q6',
expression
FROM bor_resiliation r
"; 

$aRepondants = $oDb->queryTab($strSql);

foreach($aRepondants as $iRepondants => $aRepondantsInfos){
	foreach( $aCsv[0] as $aKey){
		if($aKey == "q1" || $aKey == "q2" || $aKey == "q3" || $aKey == "q4" || $aKey == "q5" || $aKey == "q6"){
			$questions = explode(',',$aRepondantsInfos[$aKey]);
			if(in_array($aKey, $questions)){
				$aCsv[$iCompteur][] = 'oui';
			}else{
				$aCsv[$iCompteur][] = '' ;
			}
		}
		else
			$aCsv[$iCompteur][] = utf8_decode($aRepondantsInfos[$aKey]) ;
	}
	$iCompteur++;
}

foreach ($aCsv as $fields) {
   fputcsv($fp, $fields, ';');
}

fclose($fp);

header('Content-Type: application/force-download');
header('Content-Disposition: attachment; filename='.basename($strNameFile));
readfile($strNameFile);