<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "eng_cadeau";
$strTable = 'eng_cadeau';
$fieldId = "cadeau_id";
$strTitle = "Cadeaux";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
			
			$strSQLList = "	SELECT 	c.*, ca.categorie_libelle,
									IF(cadeau_actif = 1 , 'Oui', 'Non') as visible, 
									IF(cadeau_type = 1 , 'Document', 
										IF(cadeau_type = 2 , 'Url', 'Fond')
									) as type
							FROM $strTable c 
								INNER JOIN eng_categorie ca ON (ca.categorie_id = c.categorie_id)
								";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "$fieldId");
			$oTable->addColumn("Actif", "visible");			
			$oTable->addColumn("Catégorie", "categorie_libelle");			
			$oTable->addColumn("Libellé", "cadeau_titre");			
			$oTable->addColumn("Type", "type");			
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
			?>
			<div class="title">
					<h4>
						<span>Data form</span>
					</h4>
				</div>
				<div class="content noPad clearfix">
			<?php
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
					
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
				
				
			
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			$Form->AddSelectArray('cadeau_actif', 'Actif : ', array(1=>"Oui", 0=>"Non"),true);	
			$Form->AddSelectArray('cadeau_type', 'Type : ', array(1=>"Document", 2=>"Url", 3=>"Fond d'écran"),true);	
			$Form->AddSelectSQL('categorie_id', 'Catégorie :','SELECT categorie_id as id,categorie_libelle as lib FROM eng_categorie WHERE categorie_actif = 1 ', 'lib', 'id', "", true);
			
			$Form->AddInput('text', 'cadeau_titre',  'Libellé : ', true);
			$Form->AddInput('text', 'cadeau_url',  'Url : ', false);
			
			$Form->AddAjaxFile("cadeau_illustration", "Illustration : ", "../uploads/cadeaux/illustrations/","eng_cadeau", "cadeau_illustration", $_GET["id"], "img", 1, $strFilesTable="eco_files");
			$Form->AddAjaxFile("cadeau_doc", "Document : ", "../uploads/cadeaux/doc/","eng_cadeau", "cadeau_doc", $_GET["id"], "doc", 1, $strFilesTable="eco_files");
			$Form->AddAjaxFile("cadeau_image_1", "Image (480 * 360) : ", "../uploads/cadeaux/800/","eng_cadeau", "cadeau_fond", $_GET["id"], "img", 1, $strFilesTable="eco_files");
			$Form->AddAjaxFile("cadeau_image_2", "Image (800 * 600) : ", "../uploads/cadeaux/1024/","eng_cadeau", "cadeau_fond", $_GET["id"], "img", 1, $strFilesTable="eco_files");
			$Form->AddAjaxFile("cadeau_image_3", "Image (1024 * 768) : ", "../uploads/cadeaux/1280/","eng_cadeau", "cadeau_fond", $_GET["id"], "img", 1, $strFilesTable="eco_files");
			
			$Form->CloseFormBox();
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			
			
			if ($Form->Validate($strButtonAction)) {
			
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
				
				
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					$oDb->updateTable($action, $strTable);
				}
				if ($action == $_CONST["FW_ACTION_INS"]) {
					$ID = mysql_insert_id();
					$oDb->query("UPDATE eco_files set files_table_id = $ID WHERE files_table_id ='-2' and files_table_source = 'eng_cadeau'");
				}else{
					$ID = mysql_real_escape_string($_GET["id"]);
				}
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			?></div><?php
		}
		?>
	</span>
</div>