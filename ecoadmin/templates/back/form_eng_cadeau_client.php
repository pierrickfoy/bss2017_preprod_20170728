<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "eng_cadeau_cleint";
$strTable = 'eng_client_cadeau';
$fieldId = "cadeau_client_id";
$strTitle = "Téléchargement des cadeaux";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			$strSQLList = "	SELECT c.categorie_libelle, cad.cadeau_titre, cad.cadeau_id, COUNT( cc.cadeau_client_id) as nb_telechargement, 
									IF(files_field_name ='cadeau_doc' , 'Document' , 
										IF(files_field_name ='cadeau_image_1' , 'Fond 800*600' , 
											IF(files_field_name ='cadeau_image_2' , 'Fond 1024*768' , 'Fond 1224*1024')
										)
									) as type_cadeau
												
								FROM $strTable cc 
									INNER JOIN eco_files f ON (f.files_id = cc.pj_id)
									INNER JOIN eng_cadeau cad ON (cad.cadeau_id = f.files_table_id) 
									INNER JOIN eng_categorie c ON (c.categorie_id = cad.categorie_id)
								WHERE type = 2
								GROUP BY (cc.pj_id)
								";
								
			$strSQLList .= "	UNION SELECT c.categorie_libelle, cad.cadeau_titre, cad.cadeau_id, COUNT( cc.cadeau_client_id) as nb_telechargement, 
									'URL' as type_cadeau												
								FROM $strTable cc 
									INNER JOIN eng_cadeau cad ON (cad.cadeau_id = cc.pj_id) 
									INNER JOIN eng_categorie c ON (c.categorie_id = cad.categorie_id)
								WHERE type = 1
								GROUP BY (cc.pj_id)
								";
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "cadeau_id");
			$oTable->addColumn("Catégorie", "categorie_libelle");
			$oTable->addColumn("Cadeau", "cadeau_titre");
			$oTable->addColumn("Détail", "type_cadeau");
			$oTable->addColumn("Téléchargements", "nb_telechargement");
			
			/*if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			*/
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		?>
	</span>
</div>