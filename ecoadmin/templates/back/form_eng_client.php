<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "eng_client";
$strTable = 'eng_client';
$fieldId = "client_id";
$strTitle = "Clients";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
			
			$strSQLList = "	SELECT 	c.*, count(co.ouvrage_id) as nb_ouvrage,
									IF(client_actif = 1 , 'Oui', 'Non') as visible			
							FROM $strTable c 
								LEFT JOIN eng_client_ouvrage co ON (co.client_id = c.client_id)
							GROUP BY c.client_id";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "$fieldId");
			$oTable->addColumn("Actif", "visible");			
			$oTable->addColumn("Groupe", "client_groupe");			
			$oTable->addColumn("Ouvrages", "nb_ouvrage");			
			$oTable->addColumn("Nom", "client_nom");			
			$oTable->addColumn("Prénom", "client_prenom");
			$oTable->addColumn("Origine", "client_origine");
			$oTable->addColumn("Id UTI", "client_id_uti");
			$oTable->addColumn("Id WEB", "client_id_web");
			$oTable->addColumn("Ajout", "client_date_add");
			$oTable->addColumn("Update", "client_date_upd");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
			?>
			<div class="title">
					<h4>
						<span>Data form</span>
					</h4>
				</div>
				<div class="content noPad clearfix">
			<?php
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
					
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
				
				
			
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			$Form->AddSelectArray('client_actif', 'Actif : ', array(1=>"Oui", 0=>"Non"),true);	
			$Form->AddSelectArray('client_groupe', 'Groupe : ', array("ELE"=>"ELE", "PAR"=>"PAR"),true, "ELE");	
			$Form->AddSelectArray('client_origine', 'Origine : ', array("podcast"=>"Podcast", "indices"=>"Indices"),true, "indices");	
			$Form->AddInput('text', 'client_nom',  'Nom : ', true);
			$Form->AddInput('text', 'client_prenom',  'Prénom : ', true);
			$Form->AddInput('text', 'client_id_uti',  'Id UTI : ', false);
			$Form->AddInput('text', 'client_id_web',  'Id WEB : ', false);
			
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"]){
				$strSql = "SELECT 	o.ouvrage_id as id,
									CONCAT(o.ouvrage_ean, ' - ',o.ouvrage_titre, ' - ' , co.client_ouvrage_date_add) as lib 
							FROM eng_ouvrage o 
							INNER JOIN eng_client_ouvrage co ON (co.ouvrage_id = o.ouvrage_id AND co.client_id = ".mysql_real_escape_string($_GET["id"])." )
							WHERE ouvrage_actif = 1";
				
				$Form->AddCheckSQL("ouvrages", "Ouvrages :", $strSql, true, "v", true); 			
			}
			
			$Form->CloseFormBox();
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			if ($_GET["id"] == $_CONST["FORM_ID_AJOUT"])
				$Form->AddHidden("client_date_add",  date("Y-m-d H:i:s"));
			else
				$Form->AddHidden("client_date_add",  $_POST['client_date_add']);
			$Form->AddHidden("client_date_upd",  date("Y-m-d H:i:s") );
			
			if ($Form->Validate($strButtonAction)) {
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
				
				$_REQUEST['client_date_upd'] =  date("Y-m-d H:i:s") ;
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					$oDb->updateTable($action, $strTable);
				}
				if ($action == $_CONST["FW_ACTION_DEL"] ){
					//Suppression dans la table association 
					$oDb->query("DELETE FROM eng_client_ouvrage WHERE client_id = ".mysql_escape_string($_GET["id"])); 
				}
				
				
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			?></div><?php
		}
		?>
	</span>
</div>