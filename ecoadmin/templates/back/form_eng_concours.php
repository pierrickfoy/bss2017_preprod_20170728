<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "eng_concours";
$strTable = 'eng_concours';
$fieldId = "concours_id";
$strTitle = "Résultats des concours";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
			$strSQLList = "	SELECT *, IF(concours_principale = 1, 'Oui', 'Non') as principale ,IF(concours_actif = 1, 'Oui', 'Non') as actif  FROM $strTable ";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "$fieldId");
			$oTable->addColumn("Année", "concours_annee");			
			$oTable->addColumn("Principale", "principale");
			$oTable->addColumn("Visible", "actif");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
			?>
			<div class="title">
					<h4>
						<span>Data form</span>
					</h4>
				</div>
				<div class="content noPad clearfix">
			<?php
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
					
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
			
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			$Form->AddSelectArray('concours_actif', 'Actif : ', array(1=>"Oui", 0=>"Non"),true);
			$Form->AddSelectArray('concours_principale', 'Principale : ', array(1=>"Oui", 0=>"Non"),true);
			$Form->AddInput('text', 'concours_annee',  'Année : ', true);
			
			$Form->AddArea('concours_texte',   'Html : ', 1,40, true,'tinymce');
			
			$Form->AddAjaxFile("concours_resultats", "Illustration : ", "../uploads/concours/","eng_concours", "concours_id", $_GET["id"], "img", 1, $strFilesTable="eco_files");
			$Form->AddAjaxFile("concours_pdf", "Pdf : ", "../uploads/concours/pdf/","eng_concours", "concours_pdf", $_GET["id"], "doc", 1, $strFilesTable="eco_files");
			
			$Form->CloseFormBox();
			$Form->AddHidden("$fieldId",  $_GET["id"]);
		
			if ($Form->Validate($strButtonAction)) {
			
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
		
		
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					$oDb->updateTable($action, $strTable);
				}
				if ($action == $_CONST["FW_ACTION_INS"]) {
					$ID = mysql_insert_id();
					$oDb->query("UPDATE eco_files set files_table_id = $ID WHERE files_table_id ='-2' and files_table_source = 'eng_concours'");
				}
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			?></div><?php
		}
		?>
	</span>
</div>