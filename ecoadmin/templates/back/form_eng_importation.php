<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "eng_importation";
$strTable = 'eng_importation';
$fieldId = "importation_id";
$strTitle = "Importation des Quizz";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
							
							<script type="text/javascript">
								function importation(){							
									$.ajax({
										  type: "post",
										  url: "cron_import_quizz.php"
									})
									.done(function( msg ) {
										alert( "Importation terminée"); 
										location.reload();
									});
								}
							</script>
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button>
			
					<button  style="margin-left:20px;" class="btn btn-success" onclick="importation();">
						<span class="icon16 icomoon-icon-box-add white"></span> Importer les Xml</button></center><br> ';
			
			
			$strSQLList = "	SELECT 	i.*,
									IF(importation_statut = 1 , 'En attente', 
										IF(importation_statut = 2 , 'A importer',
											IF(importation_statut = 3 , 'Import OK', 'Import KO')
										)
									) as statut
							FROM $strTable i 
								";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "$fieldId");
			$oTable->addColumn("Statut", "statut");			
			$oTable->addColumn("Libellé", "importation_libelle");			
			$oTable->addColumn("Date d'impotation", "importation_date");			
			$oTable->addColumn("Message d'erreur", "importation_erreur");			
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
			?>
			<div class="title">
					<h4>
						<span>Data form</span>
					</h4>
				</div>
				<div class="content noPad clearfix">
			<?php
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
					
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
				
				
			
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			$Form->AddSelectArray('importation_statut', 'Statut : ', array(1=>"En attente", 2=>"A importer", 3 =>"Importation Ok", 4 =>"Importation Ko"),true);				
			$Form->AddInput('text', 'importation_libelle',  'Libellé : ', true);
			
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"]){
				$Form->AddLabel('importation_date', 'Date :',  $_POST['importation_date']);
				$Form->AddLabel('importation_erreur', 'Erreur :',  $_POST['importation_erreur']);
			}
			
			$Form->AddAjaxFile("importation_xml", "Xml : ", "../uploads/xml_quizz/","eng_importation", "importation_id", $_GET["id"], "doc", 1, $strFilesTable="eco_files");
			
			
			$Form->CloseFormBox();
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			
			
			if ($Form->Validate($strButtonAction)) {
			
				
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
				
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					$oDb->updateTable($action, $strTable);
				}
				if ($action == $_CONST["FW_ACTION_INS"]) {
					$ID = mysql_insert_id();
					$oDb->query("UPDATE eco_files set files_table_id = $ID WHERE files_table_id ='-2' and files_table_source = 'eng_importation'");
				}else{
					$ID = mysql_real_escape_string($_GET["id"]);
				}
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			?></div><?php
		}
		?>
	</span>
</div>