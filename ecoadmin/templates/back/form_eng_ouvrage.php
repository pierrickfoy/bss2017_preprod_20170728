<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "eng_ouvrage";
$strTable = 'eng_ouvrage';
$fieldId = "ouvrage_id";
$strTitle = "Ouvrages";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
							<script type="text/javascript">
								function importation(){							
									$.ajax({
										  type: "post",
										  url: "cron_import_ouvrages.php"
									})
									.done(function( msg ) {
										alert( "Importation terminée"); 
										location.reload();
									});
								}
							</script>
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button>
							<button  style="margin-left:20px;" class="btn btn-success" onclick="importation();"><span class="icon16 icomoon-icon-box-add white"></span> Importer</button></center><br>';

			//Création d'une table temporraire contenant les langues de chaque ouvrage pour effectuer d'effectuer une quadruple jointure dans la requete principale
			$strSqlLangue = "SELECT ol.ouvrage_id, GROUP_CONCAT(l.langue_libelle) as langue_libelle FROM eng_ouvrage_langue ol LEFT JOIN eng_langue l ON (l.langue_id = ol.langue_id) GROUP BY ol.ouvrage_id"; 
			$strSql = "CREATE TEMPORARY TABLE IF NOT EXISTS tmp_ouvrage_langue AS ($strSqlLangue)";
			$oDb->query($strSql);
			
			$strSqlClasse= "SELECT oc.ouvrage_id, GROUP_CONCAT(c.classe_libelle) as classes FROM eng_ouvrage_classe oc LEFT JOIN eng_classe c ON (c.classe_id = oc.classe_id) GROUP BY oc.ouvrage_id"; 
			$strSql = "CREATE TEMPORARY TABLE IF NOT EXISTS tmp_ouvrage_classe AS ($strSqlClasse)";
			$oDb->query($strSql);
			
			
			
			$aTest = $oDb->queryTab("SELECT * FROM tmp_ouvrage_langue");
			
			$strSQLList = "	SELECT o.* , s.serie_libelle, GROUP_CONCAT(ol.langue_libelle) as langue_libelle, DATE_FORMAT(o.ouvrage_date_parution , '%d/%m/%Y') as date_parution, GROUP_CONCAT(oc.classes) as classes,
								IF(ouvrage_actif = 1 , 'Oui', 'Non') as visible,
								IF(ouvrage_extrait = 1 , 'Oui', 'Non') as extrait,
								IF(ouvrage_podcast = 1 , 'Oui', 'Non') as podcast,
								IF(ouvrage_upd = 1 , 'Oui', 'Non') as miseajour
							FROM $strTable o
								INNER JOIN eng_serie s ON (o.serie_id = s.serie_id)
								LEFT JOIN tmp_ouvrage_langue ol ON (ol.ouvrage_id = o.ouvrage_id  )
								LEFT JOIN tmp_ouvrage_classe oc ON (oc.ouvrage_id = o.ouvrage_id  )
							GROUP BY o.ouvrage_id
						";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "$fieldId");
			$oTable->addColumn("Ean", "ouvrage_ean");
			$oTable->addColumn("Collection", "ouvrage_collection_ordre");
			$oTable->addColumn("Série", "serie_libelle");
			$oTable->addColumn("Titre", "ouvrage_titre");
			$oTable->addColumn("Sous titre", "ouvrage_soustitre");
			$oTable->addColumn("Update", "miseajour");
			$oTable->addColumn("Classes", "classes");
			$oTable->addColumn("Langue", "langue_libelle");
			$oTable->addColumn("Date parution", "date_parution");
			$oTable->addColumn("Extrait", "extrait");
			$oTable->addColumn("Podcast", "podcast");
			$oTable->addColumn("Ajout", "ouvrage_date_add");
			$oTable->addColumn("Update", "ouvrage_date_upd");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
			?>
			<div class="title">
					<h4>
						<span>Data form</span>
					</h4>
				</div>
				<div class="content noPad clearfix">
			<?php
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
					
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
				
				//Récupératin des valeurs des checkbox
				$strSql = "SELECT classe_id FROM eng_ouvrage_classe WHERE ouvrage_id = '".mysql_real_escape_string($_GET["id"])."'"; 
				$aClasses = $oDb->queryTab($strSql); 
				foreach ($aClasses as $aClasse)
					$_POST['classes'][] = $aClasse['classe_id']; 
					
				//Récupératin des valeurs des checkbox
				$strSql = "SELECT langue_id FROM eng_ouvrage_langue WHERE ouvrage_id = '".mysql_real_escape_string($_GET["id"])."'"; 
				$aLangues = $oDb->queryTab($strSql); 
				foreach ($aLangues as $aLangue)
					$_POST['langues'][] = $aLangue['langue_id']; 
			
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			$Form->AddSelectArray('ouvrage_upd', 'Mise à jour : ', array(0=>"Non", 1=>"Oui"),true);	
			$Form->AddSelectArray('ouvrage_actif', 'Actif : ', array(1=>"Oui", 0=>"Non"),true );	
			$Form->AddSelectSQL('serie_id', 'Série :','SELECT serie_id as id,serie_libelle as lib FROM eng_serie WHERE serie_actif = 1 ', 'lib', 'id', "", true);
			// $Form->AddSelectSQL('langue_id', 'Langue :','SELECT langue_id as id,langue_libelle as lib FROM eng_langue WHERE langue_actif = 1 ', 'lib', 'id', "", true);		
$Form->AddCheckSQL("langues", "Langues :", 'SELECT langue_id as id,langue_libelle as lib FROM eng_langue WHERE langue_actif = 1 ', true, "h"); 						
			$Form->AddInput('text', 'ouvrage_ean',  'Ean : ', true);
			$Form->AddInput('text', 'ouvrage_titre',  'Titre : ', true);			
			$Form->AddInput('text', 'ouvrage_soustitre',  'Sous-titre : ', false);
			$Form->AddInput('text', 'ouvrage_collection_ordre',  'Collection ordre : ', false);
			$Form->AddInput('date', 'ouvrage_date_parution',  'Date parution (yyyy-mm-dd) : ', false);
			$Form->AddSelectArray('ouvrage_extrait', 'Extrait : ', array(1=>"Oui", 0=>"Non"),true );			
			$Form->AddSelectArray('ouvrage_podcast', 'Podcast : ', array(1=>"Oui", 0=>"Non"),true );
			$Form->AddCheckSQL("classes", "Classes :", 'SELECT classe_id as id,classe_libelle as lib FROM eng_classe WHERE classe_actif = 1 ', true, "h"); 			
			$Form->AddAjaxFile("ouvrage_image", "Image : ", "../uploads/ouvrages/","eng_ouvrage", $fieldId, $_GET["id"], "img", 1, $strFilesTable="eco_files");
			
			$Form->CloseFormBox();
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			if ($_GET["id"] == $_CONST["FORM_ID_AJOUT"])
				$Form->AddHidden("ouvrage_date_add",  date("Y-m-d H:i:s"));
			else
				$Form->AddHidden("ouvrage_date_add",  $_POST['ouvrage_date_add']);
			$Form->AddHidden("ouvrage_date_upd",  date("Y-m-d H:i:s") );
			
			if ($Form->Validate($strButtonAction)) {
			
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
			
				$_REQUEST['ouvrage_date_upd'] =  date("Y-m-d H:i:s") ;
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					$oDb->updateTable($action, $strTable);
				}
				if ($action == $_CONST["FW_ACTION_INS"]) {
					$ID = mysql_insert_id();
					$oDb->query("UPDATE eco_files set files_table_id = $ID WHERE files_table_id ='-2' and files_table_id_name = 'ouvrage_id'");
				}else{
					$ID = mysql_real_escape_string($_GET["id"]);
				}
				
				//Mise à jour des classes 
				$strSql = "DELETE  FROM eng_ouvrage_classe WHERE ouvrage_id = '$ID' ";
				$oDb->query($strSql	);
				foreach( $_REQUEST['classes'] as $iR => $iClasseId){									
					$strSql = "INSERT INTO eng_ouvrage_classe (ouvrage_id, classe_id) VALUES ('$ID', '$iClasseId')"; 	
					$oDb->query($strSql);
				}
				
				//Mise à jour des classes 
				$strSql = "DELETE  FROM eng_ouvrage_langue WHERE ouvrage_id = '$ID' ";
				$oDb->query($strSql	);
				foreach( $_REQUEST['langues'] as $iR => $iLangueId){									
					$strSql = "INSERT INTO eng_ouvrage_langue (ouvrage_id, langue_id) VALUES ('$ID', '$iLangueId')"; 	
					$oDb->query($strSql);
				}
				
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			?></div><?php
		}
		?>
	</span>
</div>