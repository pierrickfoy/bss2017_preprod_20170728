<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "eng_podcast";
$strTable = 'eng_podcast';
$fieldId = "podcast_id";
$strTitle = "Podcasts";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
			
			$strSQLList = "	SELECT 	p.*,o.ouvrage_titre, o.ouvrage_ean, o.ouvrage_soustitre,
									IF(podcast_actif = 1 , 'Oui', 'Non') as visible	, 
									COUNT(pc.podcast_client_id) as telechargement
							FROM $strTable p
								INNER JOIN eng_ouvrage o ON (o.ouvrage_id = p.ouvrage_id)
								LEFT JOIN eng_podcast_client pc ON (pc.podcast_id = p.podcast_id)
							GROUP BY p.podcast_id
								
						";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "$fieldId");
			$oTable->addColumn("Podcast", "podcast_titre");			
			$oTable->addColumn("Ean", "ouvrage_ean");
			$oTable->addColumn("Ouvrage", "ouvrage_titre");
			$oTable->addColumn("Sous titre", "ouvrage_soustitre");
			$oTable->addColumn("Actif", "visible");			
			$oTable->addColumn("Téléchargements", "telechargement");			
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
			?>
			<div class="title">
					<h4>
						<span>Data form</span>
					</h4>
				</div>
				<div class="content noPad clearfix">
			<?php
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
					
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
				
				
			
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			$Form->AddSelectArray('podcast_actif', 'Actif : ', array(1=>"Oui", 0=>"Non"),true );	
			$Form->AddSelectSQL('ouvrage_id', 'Ouvrage :','SELECT ouvrage_id as id,ouvrage_titre as lib FROM eng_ouvrage WHERE ouvrage_actif = 1 ', 'lib', 'id', "", true);
			$Form->AddInput('text', 'podcast_titre',  'Titre : ', true);
			
			// $Form->AddAjaxFile("podcast_mp4", "Fichier vidéo | .mp4 : ", "../uploads/podcasts/mp4/","eng_podcast", "podcast_video", $_GET["id"], "doc", 1, $strFilesTable="eco_files");
			// $Form->AddAjaxFile("podcast_mkv", "Fichier vidéo | .mkv : ", "../uploads/podcasts/mkv/","eng_podcast", "podcast_video", $_GET["id"], "doc", 1, $strFilesTable="eco_files");
			// $Form->AddAjaxFile("podcast_mov", "Fichier vidéo | .mov : ", "../uploads/podcasts/mov/","eng_podcast", "podcast_video", $_GET["id"], "doc", 1, $strFilesTable="eco_files");
			// $Form->AddAjaxFile("podcast_webm", "Fichier vidéo | .webm : ", "../uploads/podcasts/webm/","eng_podcast", "podcast_video", $_GET["id"], "doc", 1, $strFilesTable="eco_files");
			// $Form->AddAjaxFile("podcast_ogv", "Fichier vidéo | .ogv : ", "../uploads/podcasts/ogv/","eng_podcast", "podcast_video", $_GET["id"], "doc", 1, $strFilesTable="eco_files");
			// $Form->AddAjaxFile("podcast_mp3", " Fichier audio | .mp3 : ", "../uploads/podcasts/mp3/","eng_podcast", "podcast_audio", $_GET["id"], "doc", 1, $strFilesTable="eco_files");
			// $Form->AddAjaxFile("podcast_ogg", " Fichier audio | .ogg : ", "../uploads/podcasts/ogg/","eng_podcast", "podcast_audio", $_GET["id"], "doc", 1, $strFilesTable="eco_files");
			// $Form->AddAjaxFile("podcast_wav", " Fichier audio | .wav : ", "../uploads/podcasts/wav/","eng_podcast", "podcast_audio", $_GET["id"], "doc", 1, $strFilesTable="eco_files");
			
			/************************************* Récupération des podcast ********************************************/
			$aLibelleFichier = array ( "podcast_video" => "Fichier vidéo" , "podcast_audio" => "Fichier audio" );
			$aPathFichier = array ( "podcast_video" => "../_PODCASTS/videos" , "podcast_audio" => "../_PODCASTS/audios" );
			$aListeFichier = array( 	"1podcast_mp4" => "podcast_video" ,
											"3podcast_mkv" => "podcast_video" ,
											"4podcast_mov" => "podcast_video" ,
											"2podcast_webm" => "podcast_video" ,
											"0podcast_ogv" => "podcast_video" ,
											"podcast_mp3" => "podcast_audio" ,
											"podcast_ogg" => "podcast_audio" ,
											"podcast_wav" => "podcast_audio" 
										);
										
										
			foreach ( $aListeFichier as $strChamp => $strType ){
				$aTmpName = explode('_', $strChamp); 
				$strExt = $aTmpName[1] ;
				$strUpdate = "";
				$aListe = array(); 
				if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"]){
					$aPodcast = $oDb->queryRow("SELECT files_path , files_name FROM eco_files WHERE files_field_name = '$strChamp' AND files_table_id ='".mysql_escape_string($_GET["id"])."'");
					if($oDb->rows > 0 ) {
						$aTmp = explode('/', $aPodcast['files_name']); 
						$aTmp = array_reverse($aTmp); 
						$aHtml = "<a href='".$aPodcast['files_path']."' target='_blank'>".$aTmp[0]."</a>"; 
						$Form->AddLabel('',$aLibelleFichier[$strType]." | .$strExt :",$aHtml , false);
						$strUpdate = "Modifier ";
					}
				}			
				$strPath = $aPathFichier[$strType] ; 
				$aFiles = ScanDirectory($strPath, array ($strExt));	
				foreach($aFiles as $iK => $strFile)
					$aListe[$strPath.'/'.$strFile] = $strFile ; 
				$Form->AddSelectArray($strChamp, "$strUpdate ".$aLibelleFichier[$strType]." | .$strExt : ", $aListe,false );
				
			}
			
			
			$Form->CloseFormBox();
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			
			
			if ($Form->Validate($strButtonAction)) {
			
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
			
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					$oDb->updateTable($action, $strTable);
				}
				if ($action == $_CONST["FW_ACTION_INS"]) {
					$iPodcastId = mysql_insert_id();					
				}else
					$iPodcastId = mysql_escape_string($_GET["id"]); 
					
				
					
				// $oDb->query("UPDATE eco_files set files_table_id = $ID WHERE files_table_id ='-2' and files_table_source = 'eng_podcast'");
				
				/***************** Traitement des podcast ***********************/
				
				$aListeFichier = array( 	"1podcast_mp4" => "podcast_video" ,
											"3podcast_mkv" => "podcast_video" ,
											"4podcast_mov" => "podcast_video" ,
											"2podcast_webm" => "podcast_video" ,
											"0podcast_ogv" => "podcast_video" ,
											"podcast_mp3" => "podcast_audio" ,
											"podcast_ogg" => "podcast_audio" ,
											"podcast_wav" => "podcast_audio" 
										);
										
				foreach ( $aListeFichier as $strChamp => $strType ){
					if( isset($_REQUEST[$strChamp]) && !empty($_REQUEST[$strChamp]) && is_file($_REQUEST[$strChamp]) ){					
						$aTmpName = explode('_', $strChamp); 
						$strExt = $aTmpName[1] ;
						$strNewPath = "../uploads/podcasts/$strExt/" ;
						$strPathFile = $strNewPath.$iPodcastId.'.'.$strExt;
						
						$bFiles = false ; 
						if(rename($_REQUEST[$strChamp], $strPathFile)){
							$bFiles = true; 
						}else if(copy($_REQUEST[$strChamp], $strPathFile)){
							$bFiles = true; 
						}
						
						if($bFiles){
							//Mise à jour de la table eco_files
							$oDb->query("DELETE FROM eco_files WHERE files_field_name ='$strChamp' AND files_table_id ='$iPodcastId'"); 
							$oDb->query("INSERT INTO eco_files (files_path,files_name,files_table_source,files_table_id_name,files_field_name,files_table_id,files_date)
														VALUES ('$strPathFile',
																'".$_REQUEST[$strChamp]."',
																'eng_podcast',
																'$strType',
																'$strChamp',
																'$iPodcastId',
																NOW()
																)");
						}
					}				
				}
					
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			?></div><?php
		}
		?>
	</span>
</div>
<?php 


