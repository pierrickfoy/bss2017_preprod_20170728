<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "eng_quizz";
$strTable = 'eng_quizz';
$strTable2 = 'eng_quizz_question';
$strTable3 = 'eng_quizz_reponse';
$fieldId = "quizz_id";
$strTitle = "Quizz";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		
		/* PARTIE QUESTION */
		
		
				/* LISTE */
			if (empty($_GET["id"])) {
				echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
				
				
				$strSQLList = "	SELECT 	q.*,o.ouvrage_titre, o.ouvrage_ean, o.ouvrage_soustitre,
										IF(quizz_actif = 1 , 'Oui', 'Non') as visible			
								FROM $strTable q
									INNER JOIN eng_ouvrage o ON (o.ouvrage_id = q.ouvrage_id)
									
							";
				
				?>
				 <div class="title">
					<h4>
						<span>Data list</span>
					</h4>
				</div>
				<div class="content noPad clearfix">
									
				<?php
				
				$oTable = new JoTable("$strTable");
				$oTable->bOptMultiSearch = false;
				$oTable->strCSSdefaultClass = "gradeJM";
				$oTable->strJSPath = "../lib/media/js/";
				$oTable->strCSSPath = './css/';
				$oTable->PrintHeaders(false);
				$oTable->addColumn("Id", "$fieldId");
				$oTable->addColumn("Ean", "ouvrage_ean");
				$oTable->addColumn("Ouvrage", "ouvrage_titre");
				$oTable->addColumn("Sous titre", "ouvrage_soustitre");
				$oTable->addColumn("Quizz ajout", "quizz_date_add");
				$oTable->addColumn("Quizz update", "quizz_date_upd");
				$oTable->addColumn("Actif", "visible");			
				if ($iRightLevel_tpl > 1){
					$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
					$oTable->addAction("Questions", array("_classicon_" => "icomoon-icon-list-view-2", "id_quizz" => "$fieldId", "_all_" => "template={$tpl}_question"));
				}
				if ($iRightLevel_tpl > 2)
					$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
				$oTable->ShowTableFromSQL($strSQLList);
				?></div><?php
			}
			/* FORM */
			else {
				?>
				<div class="title">
						<h4>
							<span>Data form</span>
						</h4>
					</div>
					<div class="content noPad clearfix">
				<?php
				$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
				$action = $_CONST["FW_ACTION_INS"];
				$strButtonAction = "Enregistrer";
				$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
				
						
				if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
				{
					$Form->Set_EditData($strSQLForm);
					$action = $_CONST["FW_ACTION_UPD"];
					
					
				
				}
				if (!empty($_GET["del"])) {
					$action = $_CONST["FW_ACTION_DEL"];
					$strButtonAction = "Supprimer";
				}
				
				$Form->AddSelectArray('quizz_actif', 'Actif : ', array(1=>"Oui", 0=>"Non"),true );	
				$Form->AddSelectSQL('ouvrage_id', 'Ouvrage :','SELECT ouvrage_id as id,ouvrage_titre as lib FROM eng_ouvrage WHERE ouvrage_actif = 1 ', 'lib', 'id', "", true);
				if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"]){
					$Form->AddLabel('quizz_tmp_add', 'Ajout :',  $_POST['quizz_date_add']);
					$Form->AddLabel('quizz_tmp_upd', 'Modification :',  $_POST['quizz_date_upd']);
				}
				
				$Form->CloseFormBox();
				$Form->AddHidden("$fieldId",  $_GET["id"]);
				if ($_GET["id"] == $_CONST["FORM_ID_AJOUT"])
					$Form->AddHidden("quizz_date_add",  date("Y-m-d H:i:s"));
				else
					$Form->AddHidden("quizz_date_add",  $_POST['quizz_date_add']);
				$Form->AddHidden("quizz_date_upd",  date("Y-m-d H:i:s") );
				
				if ($Form->Validate($strButtonAction)) {
				
					$_POST = $oDb->secureData($_POST);
					$_REQUEST = $oDb->secureData($_REQUEST);
					$_GET = $oDb->secureData($_GET);
				
					$_REQUEST['quizz_date_upd'] =  date("Y-m-d H:i:s") ;
					if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
						|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
						$oDb->updateTable($action, $strTable);
					}
					
					echo "<script>document.location.href='index.php?template=$tpl';</script>";
				}
				else
					$Form->Display();
				?></div><?php
			}
	
		?>
	</span>
</div>