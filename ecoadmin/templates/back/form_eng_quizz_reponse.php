<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "eng_quizz_reponse";
$strTable = 'eng_quizz_reponse';
$fieldId = "reponse_id";
$strTitle = "Réponses";

if(isset($_GET['id_quizz_question']) && !empty($_GET['id_quizz_question'])){
			$iQuestionId = mysql_real_escape_string($_GET['id_quizz_question']);
			$strSql = "SELECT question_texte FROM  eng_quizz_question WHERE question_id = $iQuestionId"; 
			$strTitle = $oDb->queryItem($strSql);
		}
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active">Réponses : <?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		
		/* PARTIE QUESTION */
		if(isset($_GET['id_quizz_question']) && !empty($_GET['id_quizz_question'])){
			if (empty($_GET["id"])) {
				echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id_quizz_question='.$iQuestionId.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
				
				
				$strSQLList = "	SELECT 	*,
										IF(reponse_actif = 1 , 'Oui', 'Non') as visible	,		
										IF(reponse_correcte = 1 , 'Oui', 'Non') as correcte			
								FROM $strTable 
								WHERE question_id = $iQuestionId
									
							";
				
				?>
				 <div class="title">
					<h4>
						<span>Data list</span>
					</h4>
				</div>
				<div class="content noPad clearfix">
									
				<?php
				
				$oTable = new JoTable("$strTable");
				$oTable->bOptMultiSearch = false;
				$oTable->strCSSdefaultClass = "gradeJM";
				$oTable->strJSPath = "../lib/media/js/";
				$oTable->strCSSPath = './css/';
				$oTable->PrintHeaders(false);
				$oTable->addColumn("Id", "$fieldId");
				$oTable->addColumn("Actif", "visible");
				$oTable->addColumn("Position", "reponse_ordre");
				$oTable->addColumn("Question", "reponse_texte");
				$oTable->addColumn("Bonne réponse", "correcte");
				if ($iRightLevel_tpl > 1)
					$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "id_quizz_question=".$iQuestionId."&template=$tpl"));
				if ($iRightLevel_tpl > 2)
					$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "id_quizz_question=".$iQuestionId."&template=$tpl&del=1"));
				$oTable->ShowTableFromSQL($strSQLList);
				?></div><?php
			}
			/* FORM */
			else {
				?>
				<div class="title">
						<h4>
							<span>Data form</span>
						</h4>
					</div>
					<div class="content noPad clearfix">
				<?php
				$Form = new JoForm($strTable."_Form","index.php?id_quizz_question={$iQuestionId}&template={$tpl}&id=".$_GET["id"], "70% align=center", true, "$strTitle", "../js/", "../css/");
				$action = $_CONST["FW_ACTION_INS"];
				$strButtonAction = "Enregistrer";
				$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
				
						
				if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
				{
					$Form->Set_EditData($strSQLForm);
					$action = $_CONST["FW_ACTION_UPD"];
					
					
				
				}
				if (!empty($_GET["del"])) {
					$action = $_CONST["FW_ACTION_DEL"];
					$strButtonAction = "Supprimer";
				}
				
				$Form->AddSelectArray('reponse_actif', 'Actif : ', array(1=>"Oui", 0=>"Non"),true );	
				$Form->AddInput('text', 'reponse_ordre',  'Position : ', true);
				$Form->AddArea('reponse_texte',   'Question : ', 1,40, true);
				$Form->AddSelectArray('reponse_correcte', 'Bonne réponse : ', array(1=>"Oui", 0=>"Non"),true );	
				
				$Form->CloseFormBox();
				$Form->AddHidden("$fieldId",  $_GET["id"]);
				$Form->AddHidden("id_quizz_question",  $iQuestionId);
				$Form->AddHidden("question_id",  $iQuestionId);
				
				if ($Form->Validate($strButtonAction)) {
				
					$_POST = $oDb->secureData($_POST);
					$_REQUEST = $oDb->secureData($_REQUEST);
					$_GET = $oDb->secureData($_GET);
					$_REQUEST['question_id'] = $iQuestionId; 
					if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
						|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
						$oDb->updateTable($action, $strTable);
					}
					echo "<script>document.location.href='index.php?id_quizz_question={$iQuestionId}&template=$tpl';</script>";
				}
				else
					$Form->Display();
				?></div><?php
			}
		}else{
			echo " Merci de sélectionner un quizz"; 
		}
		?>
	</span>
</div>