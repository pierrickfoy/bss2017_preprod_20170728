<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "activation";
$strTable = 'bor_activation';
$fieldId = "activation_id";

if (!empty($_GET["id"]) && !empty($_GET["id_bloc"])){
	$strSql = "SELECT activation_name FROM $strTable WHERE activation_id = ".$_GET["id"]."";
	$strActivationName = $oDb->queryItem($strSql);
	$strTitle = "Code activation | ".$strActivationName." : Formule";
}
else if (!empty($_GET["id"])){
	$strSql = "SELECT activation_name FROM $strTable WHERE activation_id = ".$_GET["id"]."";
	$strActivationName = $oDb->queryItem($strSql);
	$strTitle = "Code activation | ".$strActivationName;
}
else
{
	$strTitle = "Code activation";
}
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
							<script type="text/javascript">
								function importation(){							
									$.ajax({
										  type: "post",
										  url: "../cron/cron_add_product_mdl.php"
									})
									.done(function( msg ) {
										alert( "Importation terminée"); 
										location.reload();
									});
								}
							</script>
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])&& empty($_GET["id_bloc"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button>
							<button  style="margin-left:20px;" class="btn btn-success" onclick="importation();"><span class="icon16 icomoon-icon-box-add white"></span> Importer</button></center><br>';

			
			
			
			
			
			$strSQLList = "	SELECT * ,DATE_FORMAT(activation_date , '%d/%m/%Y') as activation_date, IF(partenariat_actif = 1, 'Oui', 'Non') as partenariat_actif FROM $strTable  ";
			
			?>
			 <div class="title">
				<h4>
					<span>Liste Partenariats</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "$fieldId");
			$oTable->addColumn("Nom PARTENARIAT", "activation_name");
			$oTable->addColumn("URL technique", "activation_url");
			$oTable->addColumn("URL Plateforme personnalisée", "activation_plateforme");
			$oTable->addColumn("Code action", "bill_codeaction");
			$oTable->addColumn("Origine inscription", "per_origine");
			$oTable->addColumn("Actif", "partenariat_actif");
			
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else if(!empty($_GET["id"])&& empty($_GET["id_formule"])){
			?>
			<!--<div class="title">
					<h4>
						<span>Partenariat</span>
					</h4>
				</div>
				<div class="content noPad clearfix">-->
			<?php
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
					
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
				
				
					
				
			
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			$Form->OpenFormBox('Partenariat');
				$Form->AddInput('text', 'activation_name',  'Nom du partenariat : ', true);
				$Form->AddInput('text', 'activation_url',  'URL d\'accès technique (repertoire virtuel): ', true);
				$Form->AddInput('text', 'activation_url_marketing',  'URL d\'accès marketing (sous-domaine BSS): ', true);
				$Form->AddAjaxFile("partenaire_image_bordas", "Logo Bordas (496px x 84px) : ", "../uploads/partenaires_bordas/","bor_partenaire_activation", $fieldId, $_GET["id"], "img", 1, $strFilesTable="eco_files");
				$Form->AddAjaxFile("partenaire_image", "Logo Partenaire (496px x 84px) : ", "../uploads/partenaires/","bor_partenaire_activation", $fieldId, $_GET["id"], "img", 1, $strFilesTable="eco_files");
				//$Form->AddInput('text', 'activation_texte',  'Texte activation : ', false);
				$Form->AddArea('activation_texte',  'Texte activation : ', 4, 40, false, "tinymce");
				$Form->AddInput('text', 'bill_codeaction',  'Code action : ', false,  "", null, "", "^[A-Z]{6}$");
				$Form->AddInput('text', 'per_origine',  'Origine inscription : ', false);
				//$Form->AddRadio('partenariat_actif', 'Actif : ', array(1=>'Oui', 0=>'Non'), true);
				$aActif = array(1 => "Oui", 0 => "Non"); 
				$Form->AddSelectArray("partenariat_actif", "Actif : ",$aActif , false);
			$Form->CloseFormBox();
			
			$Form->OpenFormBox('Fonctionnalité(s) activée(s)');
				$Form->OpenFormBox('Où trouver le code d\'activation');
					//$Form->AddRadio('ou_trouver_code', 'Activer Où trouver mon code : ', array(1=>'Oui', 0=>'Non'), true);
					$Form->AddAjaxFile("image_ou_trouver_code", "Visuel Où trouver mon code (586px x 197px) : ", "../uploads/partenaires/","bor_partenaire_activation", $fieldId, $_GET["id"], "img", 1, $strFilesTable="eco_files");
					$aActif = array(1 => "Oui", 0 => "Non"); 
					$Form->AddSelectArray("ou_trouver_code", "Actif : ",$aActif , false);
				$Form->CloseFormBox();
				
				$Form->OpenFormBox('J\'accède à la plateforme personnalisée');
					$Form->AddInput('text', 'activation_plateforme',  'URL Plateforme : ', false);
				$Form->CloseFormBox();
				$resident_type = $oDb->queryItem('SELECT resident_type FROM bor_activation WHERE activation_id='.($_GET["id"]));

				$Form->OpenFormBox('Je prouve que je suis résident');
					$Form->AddRadio('resident_type', 'Choisir : ', array(2=>'Région', 1=>'Département', 0=>'Ville'), true , "h", $resident_type);
					$Form->AddInput('text', 'resident_nom',  'Nom  : ', false);
					$aActif = array(1 => "Oui", 0 => "Non"); 
					$Form->AddSelectArray("resident_actif", "Actif : ",$aActif , false);
				$Form->CloseFormBox();
			$Form->CloseFormBox();
			
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			if ($_GET["id"] == $_CONST["FORM_ID_AJOUT"])
                                $Form->AddHidden("partenaire_date",  date("Y-m-d"));
                        else
                                $Form->AddHidden("partenaire_date",  $_POST['partenaire_date']);
			
			if ($Form->Validate($strButtonAction)) {
			
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
			
				$_REQUEST['ouvrage_date_upd'] =  date("Y-m-d H:i:s") ;
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					$oDb->updateTable($action, $strTable);
				}
				if ($action == $_CONST["FW_ACTION_INS"]) {
					$ID = mysql_insert_id();
					$oDb->query("UPDATE eco_files set files_table_id = $ID WHERE files_table_id ='-2' and files_table_id_name = 'activation_id'");
				}else{
					$ID = mysql_real_escape_string($_GET["id"]);
				}
				
				
				
				
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			?><!--</div>-->
				
			<div class="row-fluid">
				<div class="span12">
					<div class="box gradient">
						<div class="title">
							<h4>
								<span>Formule(s) associée(s)</span>
							</h4>
						</div>
						<div class="content">
							<?php 
							if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"]){
								echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id_formule='.$_CONST["FORM_ID_AJOUT"].'&id='.mysql_real_escape_string($_GET['id']).'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
								$strSQLList = "	SELECT * , 
												IF(reconduction = 1, 'Oui', 'Non') as reconduction,
												CASE WHEN formule_type = 0 THEN 'Formule ciblée'
												WHEN formule_type = 1 THEN 'Formule réussite'
												ELSE 'Formule tribu' END AS formule_type,
												CASE WHEN engagement_type = 0 THEN '1 MOIS'
												WHEN engagement_type = 1 THEN '3 MOIS'
												ELSE '12 MOIS' END AS engagement_type,
												DATE_FORMAT(fa.date_debut,'%d/%m/%Y') as date_debut,
												DATE_FORMAT(fa.date_fin,'%d/%m/%Y') as date_fin
												FROM bor_formule_associee fa

												WHERE fa.activation_id = '".mysql_real_escape_string($_GET['id'])."' AND fa.tpl = '".$tpl."'";
								$oTable = new JoTable("$strTable");
								$oTable->bOptMultiSearch = false;
								$oTable->strCSSdefaultClass = "gradeJM";
								$oTable->strJSPath = "../lib/media/js/";
								$oTable->strCSSPath = './css/';
								$oTable->PrintHeaders(false);
								//$oTable->addColumn("Id", "formule_associee_id");
								$oTable->addColumn("EAN", "ean13");
								$oTable->addColumn("Formule", "formule_type");
								$oTable->addColumn("Engagement", "engagement_type");
								$oTable->addColumn("Tacite reconduction", "reconduction");
								$oTable->addColumn("Date de début d'abonnement", "date_debut");
								$oTable->addColumn("Date de fin d'abonnement", "date_fin");
								
								if ($iRightLevel_tpl > 1)
									$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id_formule" => "formule_associee_id", "_all_" => "template={$tpl}&id=".mysql_real_escape_string($_GET['id'])));
								if ($iRightLevel_tpl > 2)
									$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id_formule" => "formule_associee_id", "_all_" => "template=$tpl&del=1&id=".mysql_real_escape_string($_GET['id'])));
								$oTable->ShowTableFromSQL($strSQLList);
							}else{
								echo "<p>Il faut enregistrer le partenaire avant de pouvoir éditer les formules.</p>"; 
							}
							
							?>	
						</div>
				 </div>
				</div>
			</div>
				
				<?php
		}else if(!empty($_GET["id_formule"])){
			?>
			<div class="title">
					<h4>
						<span>Formule</span>
					</h4>
				</div>
				<div class="content noPad clearfix">
					<?php
					$strTable = 'bor_formule_associee';
					
					$Form = new JoForm($strTable."_Form", "index.php?id_formule=".mysql_real_escape_string($_GET["id_formule"])."&template=activation&id=".mysql_real_escape_string($_GET["id"]). ( $_GET['del'] == 1 ? "&del=1": ""), "70% align=center", true, "$strTitle", "../js/", "../css/");
					$action = $_CONST["FW_ACTION_INS"];
					$strButtonAction = "Enregistrer";
					$strSQLForm = "SELECT * FROM bor_formule_associee WHERE formule_associee_id='".mysql_real_escape_string($_GET["id_formule"])."' AND tpl = '".$tpl."'";
					
							
					if ($_GET["id_formule"] != $_CONST["FORM_ID_AJOUT"])
					{
						$Form->Set_EditData($strSQLForm);
						$action = $_CONST["FW_ACTION_UPD"];
					}
					if (!empty($_GET["del"])) {
						$action = $_CONST["FW_ACTION_DEL"];
						$strButtonAction = "Supprimer";
					}
					
					$Form->AddInput('text', 'ean13',  'EAN : ', false);
					$Form->AddRadio('formule_type', 'Formule : ', array(0=>'Formule ciblée', 1=>'Formule Réussite', 2=>'Formule Tribu de 1 à 5 enfants'), true , "h");
					$Form->AddRadio('engagement_type', 'Engagement : ', array(0=>'1 MOIS', 1=>'3 MOIS', 2=>'12 MOIS'), true , "h");
					$Form->AddRadio('reconduction', 'Tacite reconduction : ', array(1=>'Oui', 0=>'Non'), true);
					$Form->AddInput('text', 'date_debut',  'Date de début d’abonnement (JJ/MM/AAAA) : ', false);
					$Form->AddInput('text', 'date_fin',  'Date de fin d’abonnement (JJ/MM/AAAA) : ', false);
					
					$Form->CloseFormBox();
					$Form->AddHidden("activation_id",  $_GET["id"]);
					$Form->AddHidden("tpl",  $tpl);
					$Form->AddHidden("formule_associee_id",  $_GET["id_formule"]);
					if ($_GET["id"] == $_CONST["FORM_ID_AJOUT"])
						$Form->AddHidden("partenaire_date",  date("Y-m-d"));
					else
						$Form->AddHidden("partenaire_date",  $_POST['partenaire_date']);
					
					if ($Form->Validate($strButtonAction)) {
					
						$_POST = $oDb->secureData($_POST);
						$_REQUEST = $oDb->secureData($_REQUEST);
						$_GET = $oDb->secureData($_GET);
					
						if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
							|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
							$oDb->updateTable($action,"bor_formule_associee");
						}
								
						
						
						echo "<script>document.location.href='index.php?template={$tpl}&id=".mysql_real_escape_string($_GET["id"])."';</script>";
					}
					else
						$Form->Display();
					?>
					
				</div>	
			<?php
			
		}
		?>
	</span>
</div>