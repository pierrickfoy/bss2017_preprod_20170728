<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "article";
$tpl2 = "rubrique";
$strTable = 'bor_'.$tpl;
$strTable2 = 'bor_'.$tpl2;
$fieldId = $tpl.'_id';
$strTitle = "Article";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			// echo $strTable2;exit; 
			
			$strSQLList = "	SELECT t.*, IF(t.{$tpl}_actif = 1 , 'Oui', 'Non')as actif, 
							s.rubrique_titre
							FROM $strTable t
							INNER JOIN $strTable2 s ON (s.rubrique_id = t.rubrique_id) ";
			// var_dump($strSQLList);exit;
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("ID", "article_id");
			$oTable->addColumn("Titre rubrique", "rubrique_titre");
			$oTable->addColumn("Titre", "article_titre");
			$oTable->addColumn("Position", "article_position");
			$oTable->addColumn("actif", "actif");
			$oTable->addColumn("Date publication", "article_date_publication");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
		// $errorimg='';
		
			
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
			/* LOAD LANG FIELDS */
			// $aFieldsLang = $oDb->queryTab("SELECT * FROM $strTable2 t WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'");
			// foreach ($aFieldsLang as $aData) {
				// $iLangsID = $aData['langs_id'];
				// foreach ($aData as $FieldName => $FieldValue) {
					// $_POST[$FieldName.'-'.$iLangsID] = $FieldValue;
				// }
			// }
			/* END LOAD LANG FIELDS */
			
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			$Form->OpenFormBox('General');
			$Form->AddSelectArray('article_actif', 'Actif : ', array(1=>"Oui", 0=>"Non"),true);
			$Form->AddInput('text', 'article_position',  'Position : ', false);
			
			 
			$Form->AddSelectSQL('rubrique_id', 'Rubrique :',"SELECT rubrique_id, rubrique_titre FROM `bor_rubrique` ", 'rubrique_titre', 'rubrique_id', "", true);
			$Form->AddInput('text', 'article_titre',  'Titre* : ', true);
			$Form->AddArea('article_soustitre',  'Accroche : ',6, 8, false, "tinymce");
			$Form->AddInput('text','article_url',  'URL SEO* : ', true);
			$Form->AddInput('text','article_source',  'Source : ', false);
			$Form->AddArea('article_content',  'Contenu : ',6, 8, false,"tinymce");
			$Form->AddInput('text', 'article_metatitle',  'META title : ', false);
			$Form->AddArea('article_metadesc',  'META description : ',6, 8, false);
			$Form->AddInput('text',"article_date_publication",  "Date de publication :", true);
			if($_GET["id"]!='-2')
                            $Form->AddInput('text" disabled style="',"article_date_add",  "Date d'ajout");

			
			/*MABA*/
			if($_GET["id"]=='-2') {
			
				$sqlfiles_table_id = "SHOW TABLE STATUS LIKE  '".$strTable."'";
				$next =  $oDb -> queryForm($sqlfiles_table_id);
				$next = $next['Auto_increment'];

			}
			else { 
		        $next = $_GET["id"]; 
			}
			/*MABA*/ 
			$Form->AddAjaxFile('article_image', 'Image 1200x380px : ', "../uploads/articles/","bor".$tpl, $fieldId, $next, "img", 1, $strFilesTable="eco_files",100 , True);
			$Form->AddInput('text','article_lien_image',  'Lien image : ', false);
			$Form->AddRadio('article_target_lien_image', 'Nouvelle fenêtre : ', array( 1=>'Oui', 0=>'Non'), true , "h", "0");
			$Form->AddHidden("$fieldId",  $_GET["id"]);
                        $Form->AddHidden("article_date_add",  $_POST["article_date_add"]);
					$Form->CloseFormBox();
			// $aFieldsLang = $oDb->queryRow("SELECT * FROM eco_files where
										// files_table_source = 'borarticle' 
										// AND files_table_id_name='article_id' 
										// AND files_field_name ='article_image'
										// AND files_table_id='".$next."'");
				// if($aFieldsLang == true)	
				// {	
					if ($Form->Validate($strButtonAction)) {
						
						$_POST = $oDb->secureData($_POST);
						$_REQUEST = $oDb->secureData($_REQUEST);
						$_GET = $oDb->secureData($_GET);

						if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
							|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
							if($_GET["id"]=='-2')
								$_REQUEST['article_date_add'] = date('Y-m-d');
							$oDb->updateTable($action, $strTable);
						}
						
						
						/*DELETE KANG FIELDS */
						if ($action == $_CONST["FW_ACTION_DEL"] && $iRightLevel_tpl > 2) {
							// $oDb->Squery("DELETE FROM $strTable2 WHERE $fieldId='".($_REQUEST["$fieldId"])."'");
							$oDb->Squery("DELETE FROM $strTable WHERE $fieldId='".($_REQUEST["$fieldId"])."'");
						}
						echo "<script>document.location.href='index.php?template=$tpl';</script>";
					}
					else
						$Form->Display();
				// }
				// else
			// $errorimg="Veuillez uploader une image article";
			
		}
		?>
	</span>
</div>