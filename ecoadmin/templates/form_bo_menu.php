<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "bo_menu";
$strTable = 'eco_'.$tpl;
$strTable2 = $strTable.'_lang';
$fieldId = 'menu_id';
$strTitle = "Menus Back office";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
			
			$strSQLList = "	SELECT * 
							FROM $strTable t ORDER BY menu_poids
							";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "menu_id");
			$oTable->addColumn("Titre", "menu_title");
			$oTable->addColumn("Poids", "menu_poids");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
		
		
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
			/* LOAD LANG FIELDS 
			$aFieldsLang = $oDb->queryTab("SELECT * FROM $strTable2 t WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'");
			foreach ($aFieldsLang as $aData) {
				$iLangsID = $aData['langs_id'];
				foreach ($aData as $FieldName => $FieldValue) {
					$_POST[$FieldName.'-'.$iLangsID] = $FieldValue;
				}
			}
			 END LOAD LANG FIELDS */
			
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			$Form->OpenFormBox('General');
			$Form->AddRadio('menu_visible', 'Visible : ', array(1=>'Oui', 0=>'Non'), true);
			$Form->AddInput('text', 'menu_template',  'Template : ', false);
			$Form->AddInput('text', 'menu_title',  'Titre : ', false);
			$Form->AddInput('text', 'menu_poids',  'Ordre : ', false);
			$Form->AddInput('text', 'menu_icon',  'Icon : ', false);
			$Form->AddInput('text', 'menu_poids',  'Poids : ', false);
			// $Form->AddAjaxFile('menu_img', 'Image : ', "../uploads/bo_menu/","rukh_".$tpl, $fieldId, $_GET["id"], "img", 1, $strFilesTable="eco_files");
				
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			$Form->CloseFormBox();
			if ($Form->Validate($strButtonAction)) {
				// $_REQUEST[$tpl.'_created_date'] = $action == $_CONST["FW_ACTION_INS"] ? Date("Y-m-d H:i:s") : $_POST[$tpl.'_created_date'];
				// $_REQUEST[$tpl.'_updated_date'] =  Date("Y-m-d H:i:s");
				// $_REQUEST[$tpl.'_lang_created_date'] = $action == $_CONST["FW_ACTION_INS"] ? Date("Y-m-d H:i:s") : $_POST[$tpl.'_created_date'];
				// $_REQUEST[$tpl.'_lang_updated_date'] =  Date("Y-m-d H:i:s");
				
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					$oDb->updateTable($action, $strTable);
				}
				
				/* UPDATE LANG FIELDS */
				// foreach($_REQUEST as $Key => $Value) {
					// if (preg_match('/([a-zA-Z_]+)-(\d+)/', $Key, $aMatches)) { 
						// $fieldName = $aMatches[1];
						// $langs_id = $aMatches[2];
						// if ($action == $_CONST["FW_ACTION_UPD"]  && $iRightLevel_tpl > 1)
							// $oDb->Squery("UPDATE $strTable2 SET $fieldName='".mysql_escape_string($Value)."' WHERE langs_id='".mysql_escape_string($langs_id)."' AND $fieldId='".mysql_escape_string($_REQUEST["$fieldId"])."'");
						// elseif ($action == $_CONST["FW_ACTION_INS"])
							// $aInsertData[$langs_id][$fieldName] = $Value;
					// }
				// }
				/* INSERT LANG FIELDS */
				// if ($action == $_CONST["FW_ACTION_INS"]) {
					// $ID = mysql_insert_id();
					// foreach($aInsertData as $langs_id => $aData) {
						// $oDb->Squery("INSERT INTO $strTable2 VALUES('', '".$ID."', '".mysql_escape_string($langs_id)."', '".mysql_escape_string($aData['menu_title'])."', '".$_REQUEST[$tpl.'_lang_created_date']."','".$_REQUEST[$tpl.'_lang_updated_date']."')");
					// }
				// }
				
				/*DELETE KANG FIELDS */
				// if ($action == $_CONST["FW_ACTION_DEL"] && $iRightLevel_tpl > 2) {
					// $oDb->Squery("DELETE FROM $strTable2 WHERE $fieldId='".mysql_escape_string($_REQUEST["$fieldId"])."'");
					// $oDb->Squery("DELETE FROM $strTable WHERE $fieldId='".mysql_escape_string($_REQUEST["$fieldId"])."'");
				// }
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			
		}
		?>
	</span>
</div>