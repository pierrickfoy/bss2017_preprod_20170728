<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "classe_solo";
$strTable = 'bor_'.$tpl;
$fieldId = $tpl.'_id';
$strTitle = "Classes solo";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
			
			$strSQLList = "	SELECT *, IF({$tpl}_actif = 1 , 'Oui', 'Non') as actif 
							FROM $strTable";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "classe_solo_id");
			$oTable->addColumn("Id yonnix", "classe_solo_yonixid");
			$oTable->addColumn("Titre", "classe_solo_titre");
			$oTable->addColumn("MetatTitle", "classe_solo_metatitle");
			$oTable->addColumn("Actif", "actif");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
		
		
			
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
			
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			$Form->OpenFormBox('General');
			
			$Form->AddSelectArray('classe_solo_actif', 'Actif* : ', array(1=>"Oui", 0=>"Non"),true);
			//$Form->AddSelectSQL('classe_id', 'Classe : ', 'SELECT classe_id as id, classe_name as lib FROM bor_classe WHERE classe_actif = 1 ORDER BY classe_name ', 'lib', 'id', true, true);
			$Form->AddInput('text', 'classe_solo_yonixid',  'Yonix ID* : ', true);
			$Form->AddInput('text', 'classe_solo_titre',  'Titre* : ', true);
                        $Form->AddInput('text', 'classe_solo_metatitle',  'Metatitle* : ', true);
			$Form->AddInput('text', 'classe_solo_metadesc',  'Meta description : ', true);
                        //$Form->AddInput('text', 'classe_solo_url',  'url réécrite : ', true);
                        //$Form->AddInput('text', 'classe_solo_URL_CGS',  'URL CGS : ', true);
			$Form->AddArea('classe_solo_accroche',  'Accroche : ',6, 8, false, 'tinymce');
			$Form->AddArea('classe_solo_desc',  'Description : ',6, 8, false, 'tinymce');
			if($_GET["id"]=='-2') {			
				$sqlfiles_table_id = "SHOW TABLE STATUS LIKE  '".$strTable."'";
				$next =  $oDb -> queryForm($sqlfiles_table_id);
				$next = $next['Auto_increment'];
			}
			else { 
		        $next = $_GET["id"]; 
			}
			
			$Form->AddAjaxFile('classe_solo_image', 'Image : ', "../uploads/classes/","bor".$tpl, $fieldId, $next, "img", 1, "eco_files");
			
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			$Form->CloseFormBox();
			if ($Form->Validate($strButtonAction)) {
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					$oDb->updateTable($action, $strTable);
				}
				
				/*DELETE KANG FIELDS */
				if ($action == $_CONST["FW_ACTION_DEL"] && $iRightLevel_tpl > 2) {
					$oDb->Squery("DELETE FROM $strTable WHERE $fieldId='".($_REQUEST["$fieldId"])."'");
				}
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			
		}
		?>
	</span>
</div>