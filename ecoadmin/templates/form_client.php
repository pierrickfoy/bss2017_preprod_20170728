<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "client";
$strTable = 'bor_'.$tpl;
$fieldId = $tpl.'_id';
$strTitle = "Clients";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
			
			$strSQLList = "	SELECT *, IF(acces_zoe = 1 , 'Oui', 'Non') as acces_zoe , DATE_FORMAT(client_date_add , '%d/%m/%Y') as client_date_add
							FROM $strTable";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "client_id");
			$oTable->addColumn("Id Web", "client_id_web");
			$oTable->addColumn("N° Client", "client_num");
			$oTable->addColumn("Date inscription", "client_date_add");
			$oTable->addColumn("Nom", "client_nom");
			$oTable->addColumn("Prénom", "client_prenom");
			$oTable->addColumn("Email", "client_email");
			$oTable->addColumn("Accès ZOE", "acces_zoe");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			
			// $oTable->ShowTableFromSQL($strSQLList);
			$oTable->ShowTableFromSQL($strSQLList ,50,  "dynamicTableSortable", "0", "desc");
			?></div><?php
		}
		/* FORM */
		else {
		
		
			?>
			
			
				<div class="box hover">
					<div class="title">
						<h4> 
							<span>Informations</span>
						</h4>
					</div>
					<div class="content">
						<?php 
						$strSQLList = "	SELECT *, IF(acces_zoe = 1 , 'Oui', 'Non') as acces_zoe , DATE_FORMAT(client_date_add , '%d/%m/%Y') as client_date_add
									FROM $strTable WHERE client_id = '".mysql_real_escape_string($_GET["id"])."'";
						$oTable = new JoTable("$strTable");
						$oTable->bOptMultiSearch = false;
						$oTable->strCSSdefaultClass = "gradeJM";
						$oTable->strJSPath = "../lib/media/js/";
						$oTable->strCSSPath = './css/';
						$oTable->PrintHeaders(false);
						$oTable->addColumn("Id Web", "client_id_web");
						$oTable->addColumn("N° Client", "client_num");
						$oTable->addColumn("Date inscription", "client_date_add");
						$oTable->addColumn("Nom", "client_nom");
						$oTable->addColumn("Prénom", "client_prenom");
						$oTable->addColumn("Email", "client_email");
						$oTable->addColumn("Accès ZOE", "acces_zoe");
						
						$oTable->ShowTableFromSQL($strSQLList);
						
						?>	
					</div>
				</div>
				<div class="box hover">
					<div class="title">
						<h4> 
							<span>Adresse de facturation</span>
						</h4>
					</div>
					<div class="content">
						<?php 
						$strSQLList = "	SELECT f.*, p.pays_libelle
										FROM  bor_commande_facture f 
										INNER JOIN bor_commande  c ON (c.commande_id = f.commande_id) 
										LEFT JOIN bor_pays p ON (p.pays_id = f.pays_id)
									WHERE c.client_id = '".mysql_real_escape_string($_GET["id"])."'
									GROUP BY f.facture_id";
						$oTable = new JoTable("$strTable");
						$oTable->bOptMultiSearch = false;
						$oTable->strCSSdefaultClass = "gradeJM";
						$oTable->strJSPath = "../lib/media/js/";
						$oTable->strCSSPath = './css/';
						$oTable->PrintHeaders(false);
						$oTable->addColumn("Id", "facture_id");
						$oTable->addColumn("N° et rue", "facture_adr1");
						$oTable->addColumn("Complément", "facture_adr3");
						$oTable->addColumn("Code postal", "facture_cplivraison");
						$oTable->addColumn("Ville", "facture_ville");
						$oTable->addColumn("Pays", "pays_libelle");
						
						$oTable->ShowTableFromSQL($strSQLList);
						
						?>	
					</div>
				</div>
			
			<?php
		}
		?>
	</span>
</div>