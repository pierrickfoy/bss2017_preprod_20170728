<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "commande";
$strTable = 'bor_'.$tpl;
$fieldId = $tpl.'_id';
$strTitle = "Commandes";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';

			/*$strSQLList = " SELECT  c.*,cl.*,p.*, de.*, f.formule_libelle , DATE_FORMAT(commande_date, '%d/%m/%Y') as date, 
							IF(de_engagement < 12 , 'Oui', 'Non') as tacite,
							IF(commande_resiliation = 1 , 'Oui', 'Non') as resiliation, 
							IF(commande_total ='' ,  'Gratuit',commande_total) as commande_total, 
							IF(commande_statut = -1 , 'Erreur Bill', 'Ok') as statut
							FROM bor_commande c
							INNER JOIN bor_client cl ON (cl.client_id = c.client_id) 
							INNER JOIN bor_produit p ON (p.produit_ean = c.produit_ean) 
							INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix) 
							INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
							"; */
			?>
			 <div class="title">
				<h4>
					<span>Commandes</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
			<?php
			// *** Ecomiz 20 04 2016
			require '../lib/eyedatagrid/class.eyemysqladap.inc.php';
			require '../lib/eyedatagrid/class.eyedatagrid.inc.php';
			
			// Order by
			if($_GET['order'] == "")
				$_GET['order'] = "commande_id:DESC";
			
			// Load the database adapter
			$db = new EyeMySQLAdap($_CONST["db"]["host"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["name"]);
			// Load the datagrid class
			$x = new EyeDataGrid($db, '../lib/eyedatagrid/images/', 'template=commande');
			
			$strJoin = "LEFT JOIN bor_client cl ON (cl.client_id = c.client_id) 
						LEFT JOIN bor_produit p ON (p.produit_ean = c.produit_ean) 
						LEFT JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix) 
						LEFT JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)";
			$strWhere = "";
			$strGroupBy = "";

			$x->setQuery("	c.commande_id,
							DATE_FORMAT(commande_date, '%d/%m/%Y') as date,
							c.commande_num,
							cl.client_num,
							cl.client_nom,
							cl.client_prenom,
							p.produit_ean,
							de.de_libelle,
							f.formule_libelle,
							if(de_engagement < 12 , 'Oui', 'Non') as tacite,
							if(commande_resiliation = 1 , 'Oui', 'Non') as resiliation, 
							if(commande_total ='' ,  'Gratuit',commande_total) as commande_total, 
							if(commande_statut = -1 , 'Erreur Bill', 'Ok') as statut,
							CONCAT('<a href=\'index.php?template=commande&id=',c.commande_id,'\'><span class=\'icon16 icomoon-icon-pencil-2\'></span></a>') as lien"
							, "bor_commande".' c '.$strJoin, '', $strWhere
							);
							
			$x->setColumnType( 'commande_id', EyeDataGrid::TYPE_HTMLENTITIES, '');
			$x->setColumnHeader( 'commande_id', 'ID');
			$x->setColumnType( 'date', EyeDataGrid::TYPE_HTMLENTITIES, '');
			$x->setColumnHeader( 'date', 'Date');
			$x->setColumnType( 'commande_num', EyeDataGrid::TYPE_HTMLENTITIES, '');
			$x->setColumnHeader( 'commande_num', 'N°');
			$x->setColumnType( 'client_num', EyeDataGrid::TYPE_HTMLENTITIES, '');
			$x->setColumnHeader( 'client_num', 'N° Client');
			$x->setColumnType( 'client_nom', EyeDataGrid::TYPE_HTMLENTITIES, '');
			$x->setColumnHeader( 'client_nom', 'Nom');
			$x->setColumnType( 'client_prenom', EyeDataGrid::TYPE_HTMLENTITIES, '');
			$x->setColumnHeader( 'client_prenom', 'Prénom');
			$x->setColumnType( 'produit_ean', EyeDataGrid::TYPE_HTMLENTITIES, '');
			$x->setColumnHeader( 'produit_ean', 'Ean');
			$x->setColumnType( 'formule_libelle', EyeDataGrid::TYPE_BRUT, '');
			$x->setColumnHeader( 'formule_libelle', 'Formule');
			$x->setColumnType( 'de_libelle', EyeDataGrid::TYPE_HTMLENTITIES, '');
			$x->setColumnHeader( 'de_libelle', 'Engagement');
			$x->setColumnType( 'commande_total', EyeDataGrid::TYPE_HTMLENTITIES, '');
			$x->setColumnHeader( 'commande_total', 'Total');
			$x->setColumnType( 'tacite', EyeDataGrid::TYPE_HTMLENTITIES, '');
			$x->setColumnHeader( 'tacite', 'Tacite R');
			$x->setColumnType( 'resiliation', EyeDataGrid::TYPE_HTMLENTITIES, '');
			$x->setColumnHeader( 'resiliation', 'Résilié');
			$x->setColumnType( 'statut', EyeDataGrid::TYPE_HTMLENTITIES, '');
			$x->setColumnHeader( 'statut', 'Statut');
			$x->setColumnType('lien', EyeDataGrid::TYPE_BRUT, '');
			$x->setColumnHeader('lien', 'Action');
			
			if (EyeDataGrid::isAjaxUsed())
			{
				$x->printTable();
				exit;
			}
			// Print the table
			$x->printTable();
			
			// *** /Ecomiz 20 04 2016
			
			/*$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "commande_id");
			$oTable->addColumn("Date", "date");
			$oTable->addColumn("N°", "commande_num");
			$oTable->addColumn("N° Client", "client_num");
			$oTable->addColumn("Nom", "client_nom");
			$oTable->addColumn("Prénom", "client_prenom");
			$oTable->addColumn("Ean", "produit_ean");
			$oTable->addColumn("Formule", "formule_libelle");
			$oTable->addColumn("Engagement", "de_libelle");
			$oTable->addColumn("Total", "commande_total");
			$oTable->addColumn("Tacite R", "tacite");
			$oTable->addColumn("Résilié", "resiliation");
            $oTable->addColumn("Statut", "statut");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			$oTable->ShowTableFromSQL($strSQLList ,50,  "dynamicTableSortable", "0", "desc");*/
			
			?></div><?php
		}
		/* FORM */
		else {
		
		
			?>
			
			
				<div class="box hover">
					<div class="title">
						<h4> 
							<span>Parents</span>
						</h4>
					</div>
					<div class="content">
						<?php 
						$strSQLList = "	SELECT *,  DATE_FORMAT(cl.client_date_add , '%d/%m/%Y') as client_date_add 
											FROM $strTable c
											INNER JOIN bor_client cl ON (cl.client_id = c.client_id) 
											WHERE c.commande_id = '".mysql_real_escape_string($_GET["id"])."'";
						$oTable = new JoTable("$strTable");
						$oTable->bOptMultiSearch = false;
						$oTable->strCSSdefaultClass = "gradeJM";
						$oTable->strJSPath = "../lib/media/js/";
						$oTable->strCSSPath = './css/';
						$oTable->PrintHeaders(false);
						$oTable->addColumn("Id Web", "client_id_web");
						$oTable->addColumn("N° Client", "client_num");
						$oTable->addColumn("Date inscription", "client_date_add");
						$oTable->addColumn("Nom", "client_nom");
						$oTable->addColumn("Prénom", "client_prenom");
						$oTable->addColumn("Email", "client_email");

						$oTable->ShowTableFromSQL($strSQLList);
						
						?>	
					</div>
				</div>
				<div class="box hover">
					<div class="title">
						<h4> 
							<span>Formule achetée</span>
						</h4>
					</div>
					<div class="content">
						<?php 
						$strSQLList = "	SELECT *,  	IF(de_engagement < 12 , 'Oui', 'Non') as tacite ,
													IF(commande_resiliation = 1 , 'Oui', 'Non') as resiliation, 
													IF(commande_total ='' ,  'Gratuit',commande_total) as commande_total																						
											FROM $strTable c
											INNER JOIN bor_produit p ON (p.produit_ean = c.produit_ean) 
											INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix) 
											INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
											WHERE c.commande_id = '".mysql_real_escape_string($_GET["id"])."'";
											
						$oTable = new JoTable("$strTable");
						$oTable->bOptMultiSearch = false;
						$oTable->strCSSdefaultClass = "gradeJM";
						$oTable->strJSPath = "../lib/media/js/";
						$oTable->strCSSPath = './css/';
						$oTable->PrintHeaders(false);
						$oTable->addColumn("EAN", "produit_ean");
						$oTable->addColumn("Titre", "produit_titre");
						$oTable->addColumn("Formule", "formule_libelle");
						$oTable->addColumn("Engagement", "de_libelle");
						$oTable->addColumn("Total", "commande_total");
						$oTable->addColumn("Tacite R", "tacite");
						$oTable->addColumn("Résilié", "resiliation");
						$oTable->addColumn("IdAbonnement", "commande_abo_id");
						$oTable->addColumn("idReferenceCB", "commande_cb_ref");
						
						$oTable->ShowTableFromSQL($strSQLList);
						
						?>	
					</div>
				</div>
				
				<div class="box hover">
					<div class="title">
						<h4> 
							<span>Enfants</span>
						</h4>
					</div>
					<div class="content">
						<?php 
						$strSQLList = "	SELECT *
											FROM $strTable c
											INNER JOIN bor_commande_enfant ce ON (c.commande_id = ce.commande_id) 
											LEFT JOIN bor_classe cl ON (cl.classe_id = ce.classe_id)
											LEFT JOIN bor_matiere m ON (m.matiere_id = ce.matiere_id)
											WHERE c.commande_id = '".mysql_real_escape_string($_GET["id"])."'";
											
						$oTable = new JoTable("$strTable");
						$oTable->bOptMultiSearch = false;
						$oTable->strCSSdefaultClass = "gradeJM";
						$oTable->strJSPath = "../lib/media/js/";
						$oTable->strCSSPath = './css/';
						$oTable->PrintHeaders(false);
						$oTable->addColumn("Id", "enfant_id");
						$oTable->addColumn("Prénom", "enfant_prenom");
						$oTable->addColumn("Classe", "classe_name");
						$oTable->addColumn("Matière", "matiere_titre");
						
						
						$oTable->ShowTableFromSQL($strSQLList);
						
						?>	
					</div>
				</div>

			<?php
		}
		?>
	</span>
</div>