<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "coupon";
$strTable = 'bor_coupon';
$fieldId = "coupon_id";
$strTitle = "Code Promo";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
							<script type="text/javascript">
								function importation(){							
									$.ajax({
										  type: "post",
										  url: "../cron/cron_add_product_mdl.php"
									})
									.done(function( msg ) {
										alert( "Importation terminée"); 
										location.reload();
									});
								}
							</script>
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button>
							</center><br>';

			
			
			
			
			
			$strSQLList = "	SELECT  * ,  
                                                DATE_FORMAT(coupon_debut , '%d/%m/%Y') as debut, 
                                                DATE_FORMAT(coupon_fin , '%d/%m/%Y') as fin ,
                                                CONCAT('-', coupon_reduction, '%') as reduction, 
                                                IF(coupon_actif=1 , 'Oui', 'Non') as actif
                                        FROM $strTable  ";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "$fieldId");
			$oTable->addColumn("Opération", "coupon_op");
			$oTable->addColumn("Code avantage", "coupon_code");
			$oTable->addColumn("Début", "debut");
                        $oTable->addColumn("Fin", "fin");
                        $oTable->addColumn("Réduction", "coupon_reduction");
                        $oTable->addColumn("Mensualité", "coupon_mensualite");
                        $oTable->addColumn("Actif", "actif");
                        
			
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
			?>
			<div class="title">
					<h4>
						<span>Data form</span>
					</h4>
				</div>
				<div class="content noPad clearfix">
			<?php
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * , DATE_FORMAT(coupon_debut , '%d/%m/%Y') as coupon_debut, DATE_FORMAT(coupon_fin , '%d/%m/%Y') as coupon_fin FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
					
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
                                
                                //Récupératin des valeurs des checkbox
				$strSql = "SELECT * FROM bor_coupon_formule WHERE coupon_id = '".mysql_real_escape_string($_GET["id"])."'"; 
				$aFormules = $oDb->queryTab($strSql); 
				foreach ($aFormules as $aFormule)
                                    $_POST['coupon_formule'][] = $aFormule['formule_id'].'-'.$aFormule['de_id']; 
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			$Form->AddSelectArray('coupon_actif', 'Actif : ', array(1=>"Oui", 0=>"Non"),true);
			$Form->AddInput('text', 'coupon_op',  'Opération : ', true);
                        $Form->AddInput('text', 'coupon_code',  'Code avantage : ', true,  "", null, "", "^[0-9A-Z]{6}$");
                        $Form->AddInput('text', 'coupon_debut',  'Date début (JJ/MM/AAAA) : ', false, "", null, "", "^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((1[6-9]|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((1[6-9]|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((1[6-9]|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$");
                        $Form->AddInput('text', 'coupon_fin',  'Date fin (JJ/MM/AAAA) : ', false, "", null, "", "^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((1[6-9]|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((1[6-9]|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((1[6-9]|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$");
                        $Form->AddInput('text', 'coupon_reduction',  'Réduction en % : ', true, "", null, "", "^(0?[1-9]|[1-9][0-9])$");
                        $Form->AddInput('text', 'coupon_mensualite',  'Mensualité : ', false);
                        
                        
                        //$Form->AddCheckSQL("test", $label, $strSQL, $require=false, $aff="h", $bChecked = false)
                        $Form->AddCheckSQL("coupon_formule", "Formules :",  "SELECT CONCAT(f.formule_id, '-', de.de_id) as id, CONCAT (' ', f.formule_libelle, ' ' , REPLACE(de.de_libelle,'Sans engagement', 'Engagement 1 MOIS')) as lib FROM bor_formule f , bor_duree_engagement de ORDER BY f.formule_libelle, de.de_position"   , false, "v"); 		
                        
                        
                        $Form->AddHidden("$fieldId",  $_GET["id"]);
			
			
			
			if ($Form->Validate($strButtonAction)) {
                            			
                            $_POST = $oDb->secureData($_POST);
                            $_REQUEST = $oDb->secureData($_REQUEST);
                            $_GET = $oDb->secureData($_GET);
                            
                            //  Mise a jour du format des dates avant enregistrement
                            $_REQUEST['coupon_debut'] = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['coupon_debut'])));
                            $_POST['coupon_debut'] = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['coupon_debut'])));
                            $_GET['coupon_debut'] = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['coupon_debut'])));
                            $_REQUEST['coupon_fin'] = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['coupon_fin'])));
                            $_POST['coupon_fin'] = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['coupon_fin'])));
                            $_GET['coupon_fin'] = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['coupon_fin'])));
                                
                            //Enregistrement du coupon 
                            if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
                                    || ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
                                   $oDb->updateTable($action, $strTable);
                                   
                            }
                            if ($action == $_CONST["FW_ACTION_INS"]) {
                                    $ID = mysql_insert_id();
                            }else{
                                    $ID = mysql_real_escape_string($_GET["id"]);
                            }
                            
                            //Enregistrement des formules associées
                            $strSql = "DELETE  FROM bor_coupon_formule WHERE coupon_id = '$ID' ";
                            $oDb->query($strSql	);
                            foreach( $_REQUEST['coupon_formule'] as $iR => $strFormule){	
                                $aFormule = explode('-',$strFormule );
                                $strSql = "INSERT INTO bor_coupon_formule (coupon_id, formule_id, de_id) VALUES ('$ID', '".$aFormule[0]."', '".$aFormule[1]."')"; 	
                                $oDb->query($strSql);
                            }
				
				
				
				
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			?></div><?php
		}
		?>
	</span>
</div>