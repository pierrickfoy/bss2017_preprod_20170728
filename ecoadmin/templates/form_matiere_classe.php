<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "matiere_classe";
//$strTable = 'bor_'.$tpl;
$strTable = 'bor_'.$tpl.'_edito';
$fieldId = 'mc_id';
$strTitle = "Matières - Classes";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
			
			$strSQLList = "	SELECT mc.*, c.classe_page_titre, m.matiere_page_titre, IF(mc_actif = 1 , 'Oui', 'Non') as actif , IF(mc_dispo = 1 , 'Oui', 'Non') as mc_dispo 
							FROM 
								bor_matiere_classe_edito mc 
							INNER JOIN bor_classe_page c ON (c.classe_page_id = mc.classe_id)
							INNER JOIN bor_matiere_page m ON (m.matiere_id = mc.matiere_id) 
					";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "mc_id");
			$oTable->addColumn("Classe", "classe_page_titre");
			$oTable->addColumn("Matière", "matiere_page_titre");
			$oTable->addColumn("Titre", "mc_titre");
			$oTable->addColumn("Position", "mc_position");
			$oTable->addColumn("Disponible", "mc_dispo");// Ecomiz Lot-2
			$oTable->addColumn("Actif", "actif");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else if(!empty($_GET["id"])&& empty($_GET["slider_image_id"])){
		
		
			
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
			
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
				// Ajout 2017 05 19 :
				$_POST['mc_page_type'] = explode(',', $_POST['mc_page_type']);
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			$Form->OpenFormBox($strTitle);
			
			if($_GET["id"]=='-2') {			
				$sqlfiles_table_id = "SHOW TABLE STATUS LIKE  '".$strTable."'";
				$next =  $oDb -> queryForm($sqlfiles_table_id);
				$next = $next['Auto_increment'];
			}
			else { 
		        $next = $_GET["id"]; 
			}
			
			$Form->AddSelectArray('mc_actif', 'Actif* : ', array(1=>"Oui", 0=>"Non"),true);
			$Form->AddSelectSQL('matiere_id', 'Matière* : ', 'SELECT matiere_id as id, matiere_titre as lib FROM bor_matiere  ', 'lib', 'id', false, false);
			$Form->AddSelectSQL('classe_id', 'Classe* : ', 'SELECT classe_page_id as id, classe_page_titre as lib FROM bor_classe_page WHERE classe_page_actif = 1 ORDER BY classe_page_titre ', 'lib', 'id', false, false);
			$Form->AddInput('text', 'mc_titre',  'Titre H1* : ', true);
			$Form->AddInput('text', 'mc_metatitle',  'Meta Title* : ', false);
			$Form->AddInput('text', 'mc_url_canonical',  'Url canonical : ', false);
			$Form->AddArea('mc_metadesc',  'Meta Description* : ',6, 8, true);
			$Form->AddArea('mc_accroche',  'Accroche : ',6, 8, false, 'tinymce');
			$Form->AddArea('mc_content',  'Contenu : ',6, 8, false, 'tinymce');
			//$Form->AddAjaxFile('mc_image', 'Image : ', "../uploads/matieres-classes/","bor".$tpl, $fieldId, $next, "img", 10, "eco_files");
			//$Form->AddInput('text', 'mc_URL_CGS',  'URL CGS : ', false);
			$Form->AddInput('text', 'mc_position',  'Position : ', false);
			$Form->AddSelectArray('mc_dispo', 'Disponibilité* : ', array(1=>"Oui", 0=>"Non"),true);// Ecomiz Lot-2

			// ajout 2017 05 19 :
			$TabData = array(1=> 'Cours', 2=>'Exercices', 3=>'Corrigés', 4=>'Animations',5=>'Vidéos',6=>'Examen');
            $Form->AddCheckTable('mc_page_type', 'Type de ressources :', $TabData);
			// /fin ajout 2017 05 19 
			
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			$Form->CloseFormBox();

			// Ajout 20 07 2016
			$Form->OpenFormBox('URL Slider');
				$Form->AddInput('text', 'matiere_classe_slider_url',  'URL slider : ', false);
				$TabData = array(1=> 'Oui');
				$Form->AddCheckTable('matiere_classe_slider_url_blank', 'Ouvrir le lien dans un nouvel onglet :', $TabData);
			$Form->CloseFormBox();
			// Fin ajout 20 07 2016
			
			// Ajout 20 07 2016
			$Form->OpenFormBox('Lien "Tester une leçon"');
				$Form->AddInput('text', 'matiere_classe_tester_url',  'URL "Tester une leçon" : ', false);
				$TabData = array(1=> 'Oui');
				$Form->AddCheckTable('matiere_classe_tester_url_blank', 'Ouvrir le lien dans un nouvel onglet :', $TabData);
			$Form->CloseFormBox();
			// Fin ajout 20 07 2016

			if ($Form->Validate($strButtonAction)) {
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
				
				// Ajout 2017 05 19 :
				$_REQUEST['mc_page_type'] = implode(',', $_REQUEST['mc_page_type']);
				
				// Ajout 20 07 2016
				$_REQUEST['matiere_classe_slider_url_blank'] = implode(',', $_REQUEST['matiere_classe_slider_url_blank']);
				$_REQUEST['matiere_classe_tester_url_blank'] = implode(',', $_REQUEST['matiere_classe_tester_url_blank']);
				
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					
					if($action == $_CONST["FW_ACTION_INS"]){
						$strSql ="SELECT mc_id FROM bor_matiere_classe_edito WHERE classe_id = '".($_REQUEST["classe_id"])."' AND matiere_id = '".($_REQUEST["matiere_id"])."'";
						$oDb->queryItem($strSql); 
						if($oDb->rows > 0 ) {	
							echo "Cette combinaison est déjà présente dans la base."; 
						}else
							$oDb->updateTable($action, $strTable);
					}else
						$oDb->updateTable($action, $strTable);
				}
				
				/*DELETE KANG FIELDS */
				if ($action == $_CONST["FW_ACTION_DEL"] && $iRightLevel_tpl > 2) {
					$oDb->Squery("DELETE FROM $strTable WHERE $fieldId='".($_REQUEST["$fieldId"])."'");
				}
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
		?>
		<div class="row-fluid">
				<div class="span12">
					<div class="box gradient">
						<div class="title">
							<h4>
								<span>Slider</span>
							</h4>
						</div>
						<div class="content">
							<?php 
							if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"]){
								echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&slider_image_id='.$_CONST["FORM_ID_AJOUT"].'&id='.mysql_real_escape_string($_GET['id']).'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';

								// récupère les images à partir de l'id page et du tpl
								$strSQLList = "	SELECT *
												FROM bor_slider_page sp
												WHERE sp.page_id = '".mysql_real_escape_string($_GET['id'])."' AND sp.tpl = '".$tpl."'";
												
								$oTable = new JoTable("$strTable");
								$oTable->bOptMultiSearch = false;
								$oTable->strCSSdefaultClass = "gradeJM";
								$oTable->strJSPath = "../lib/media/js/";
								$oTable->strCSSPath = './css/';
								$oTable->PrintHeaders(false);
								//$oTable->addColumn("Id", "slider_image_id");
								$oTable->addColumn("Titre", "titre");
								$oTable->addColumn("Sous-titre", "sous_titre");
								//$oTable->addColumn("files_id", "files_id");
								$oTable->addColumn("Position", "position");

								if ($iRightLevel_tpl > 1)
									$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "slider_image_id" => "slider_image_id", "_all_" => "template={$tpl}&id=".mysql_real_escape_string($_GET['id'])));
								if ($iRightLevel_tpl > 2)
									$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "slider_image_id" => "slider_image_id", "_all_" => "template=$tpl&del=1&id=".mysql_real_escape_string($_GET['id'])));
								$oTable->ShowTableFromSQL($strSQLList);
							}else{
								echo "<p>Il faut enregistrer la page avant de pouvoir éditer le slider.</p>"; 
							}
							
							?>	
						</div>
				 </div>
				</div>
			</div>
		<?php
		}else if(!empty($_GET["slider_image_id"])){
		?>
		<div class="title">
					<h4>
						<span>Image du slider</span>
					</h4>
				</div>
				<div class="content noPad clearfix">
					<?php
					$strTable = 'bor_slider_page';
					
					$Form = new JoForm($strTable."_Form", "index.php?slider_image_id=".mysql_real_escape_string($_GET["slider_image_id"])."&template=".$tpl."&id=".mysql_real_escape_string($_GET["id"]). ( $_GET['del'] == 1 ? "&del=1": ""), "70% align=center", true, "$strTitle", "../js/", "../css/");
					$action = $_CONST["FW_ACTION_INS"];
					$strButtonAction = "Enregistrer";
					$strSQLForm = "SELECT * FROM bor_slider_page WHERE slider_image_id='".mysql_real_escape_string($_GET["slider_image_id"])."' AND tpl = '".$tpl."'";

					if ($_GET["slider_image_id"] != $_CONST["FORM_ID_AJOUT"])
					{
						$Form->Set_EditData($strSQLForm);
						$action = $_CONST["FW_ACTION_UPD"];
					}
					if (!empty($_GET["del"])) {
						$action = $_CONST["FW_ACTION_DEL"];
						$strButtonAction = "Supprimer";
					}

					if($_GET["slider_image_id"]=='-2') {
						$sqlfiles_table_id = "SHOW TABLE STATUS LIKE  '".$strTable."'";
						$next =  $oDb -> queryForm($sqlfiles_table_id);
						$next = $next['Auto_increment'];
					}
					else { 
						$next = $_GET["slider_image_id"]; 
					}

					$Form->AddInput('text', 'titre',  'Titre : ', false);
					$Form->AddInput('text', 'sous_titre',  'Sous-titre : ', false, 'placeholder="Si vide, affiche \'Conforme aux programmes – Rédigé par des enseignants\'"');
					$Form->AddInput('text', 'position',  'Position : ', false);
					$Form->AddAjaxFile('files_id', 'Image : ', "../uploads/classes/","bor_slider_page", "slider_image_id", $next, "img", 1, "eco_files");
					$ID = mysql_insert_id();
					$Form->CloseFormBox();
					$Form->AddHidden("page_id",  $_GET["id"]);
					$Form->AddHidden("tpl",  $tpl);
					$Form->AddHidden("slider_image_id",  $_GET["slider_image_id"]);
					if ($Form->Validate($strButtonAction)) {
						$_POST = $oDb->secureData($_POST);
						$_REQUEST = $oDb->secureData($_REQUEST);
						$_GET = $oDb->secureData($_GET);
						if(isset($_SESSION['iFileID']))
							$_REQUEST['files_id'] = $_SESSION['iFileID'];
						else{
							$files_id_recup  = $oDb->queryItem("SELECT files_id
							FROM   eco_files
							WHERE files_table_id = ".$_REQUEST["slider_image_id"]."
							AND files_table_source = 'bor_slider_page'
							");
							$_REQUEST['files_id'] = $files_id_recup;
						}

						if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
							|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
							$oDb->updateTable($action,"bor_slider_page");
						}

						unset($_SESSION['iFileID']);
						echo "<script>document.location.href='index.php?template={$tpl}&id=".mysql_real_escape_string($_GET["id"])."';</script>";
					}
					else
						$Form->Display();
					?>
				</div>
		<?php
			
		}
		?>
	</span>
</div>