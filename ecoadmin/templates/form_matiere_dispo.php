<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "matiere_dispo";
$strTable = 'bor_'.$tpl;
$fieldId = 'matiere_dispo_id';
$strTitle = "Disponibilité des matières (popin email classe > commande)";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
			
			$strSQLList = "	SELECT 
									md.matiere_dispo_id,
									c.classe_name, 
									m.matiere_titre, 
									d.datedispo_date 
							FROM 
								bor_matiere_dispo md
							INNER JOIN bor_classe c ON (c.classe_id = md.classe_id)
							INNER JOIN bor_matiere m ON (m.matiere_id = md.matiere_id) 
							INNER JOIN bor_datedispo d ON (d.datedispo_id = md.datedispo_id)
					";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "matiere_dispo_id");
			$oTable->addColumn("Classe", "classe_name");
			$oTable->addColumn("Matière", "matiere_titre");
			$oTable->addColumn("Date", "datedispo_date");
			
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
		
		
			
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
			
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			$Form->OpenFormBox('General');
			
			
			$Form->AddSelectSQL('classe_id', 'Classe : ', 'SELECT classe_id as id, classe_name as lib FROM bor_classe ORDER BY classe_name ', 'lib', 'id', true, true);
			$Form->AddSelectSQL('matiere_id', 'Matière : ', 'SELECT matiere_id as id, matiere_titre as lib FROM bor_matiere  ORDER BY matiere_position ', 'lib', 'id', true, true);
			$Form->AddSelectSQL('datedispo_id', 'Date : ', 'SELECT datedispo_id as id, datedispo_date as lib FROM bor_datedispo  ORDER BY datedispo_position ', 'lib', 'id', true, true);// Ecomiz Lot-2
			
			
			
			
			
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			$Form->CloseFormBox();
			if ($Form->Validate($strButtonAction)) {
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					
					if($action == $_CONST["FW_ACTION_INS"]){
						$strSql ="SELECT matiere_dispo_id FROM  bor_matiere_dispo WHERE classe_id = '".($_REQUEST["classe_id"])."' AND matiere_id = '".($_REQUEST["matiere_id"])."' AND datedispo_id = '".($_REQUEST["datedispo_id"])."'";
						$oDb->queryItem($strSql); 
						if($oDb->rows > 0 ) {	
							echo "Cette combinaison est déjà présente dans la base."; 
						}else
							$oDb->updateTable($action, $strTable);
					}else
						$oDb->updateTable($action, $strTable);
				}
				
				/*DELETE KANG FIELDS */
				if ($action == $_CONST["FW_ACTION_DEL"] && $iRightLevel_tpl > 2) {
					$oDb->Squery("DELETE FROM $strTable WHERE $fieldId='".($_REQUEST["$fieldId"])."'");
				}
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			
		}
		?>
	</span>
</div>