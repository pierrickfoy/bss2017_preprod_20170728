<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "notions";
$strTable = 'bor_'.$tpl;
$fieldId = $tpl.'_id';
$strTitle = "Notions";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
			
			$strSQLList = "	SELECT *, IF(notions_status = 1 , 'Oui', 'Non') as actif 
							FROM $strTable";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "notions_id");
			$oTable->addColumn("Id chapitre", "chapitre_id");
			$oTable->addColumn("Nom", "notions_h1");
			$oTable->addColumn("Actif", "actif");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
		
		
			
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
			
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			$Form->OpenFormBox('Pages CLASSE > MATIÈRE > CHAPITRE > NOTIONS');
			
			$Form->AddSelectArray('notions_status', 'Actif* : ', array(1=>"Oui", 0=>"Non"),true);
            //$Form->AddSelectSQL('classes_yonix_id', 'Classe : ', 'SELECT classe_id_yonix as id, classe_name as lib FROM bor_classe WHERE classe_actif = 1 ORDER BY classe_name ', 'lib', 'id', true, true);
			//$Form->AddSelectSQL('matiere_yonix_id', 'Matière : ', 'SELECT matiere_id_yonix as id, matiere_titre as lib FROM bor_matiere  ORDER BY matiere_position ', 'lib', 'id', true, true);
			$Form->AddSelectSQL('chapitre_id', 'Chapitre* : ', 'SELECT chapitre_id as id, chapitre_h1 as lib FROM bor_chapitres ', 'lib', 'id', true, true);
			
            $Form->AddInput('text', 'notions_h1',  'Titre H1* : ', true);
			$Form->AddInput('text', 'notions_metatitle',  'Meta Title* : ', true);
			$Form->AddInput('text', 'notions_url_canonical',  'Url canonical : ', false);
			$Form->AddArea('notions_metadesc',  'Meta Description* : ',6, 8, true);
			$Form->AddInput('text', 'notions_url_seo',  'Permalien (url réécrite SEO) : ', true);
			$Form->AddArea('notions_accroche',  'Accroche : ',6, 8, true, 'tinymce');
			$Form->AddArea('notions_content',  'Contenu : ',6, 8, true, 'tinymce');
            //$Form->AddInput('text', 'notions_url_cgs',  'URL CGS : ', false);
			$Form->AddInput('text', 'notions_rang',  'Position : ', true);
            
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			$Form->CloseFormBox();
                        
			if ($Form->Validate($strButtonAction)) {
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					$oDb->updateTable($action, $strTable);
				}
				
				/*DELETE KANG FIELDS */
				if ($action == $_CONST["FW_ACTION_DEL"] && $iRightLevel_tpl > 2) {
					$oDb->Squery("DELETE FROM $strTable WHERE $fieldId='".($_REQUEST["$fieldId"])."'");
				}
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			
                        }
		?>
	</span>
</div>