<?php

if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "ouvrage";
$strTable = 'bor_ouvrage';
$fieldId = "ouvrage_id";
$strTitle = "Ouvrages";
set_time_limit(3000);
ini_set('default_socket_timeout',3600);
ini_set('max_input_time',3600);
//echo '<pre>';
//var_dump(ini_get_all());
//var_dump(ini_get(default_socket_timeout));
//var_dump(ini_get(max_input_time));
//phpinfo();
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
							<script type="text/javascript">
								function importation(){
									$('#loading').html('<span class="alert" >L\'importation peut prendre quelques minutes. Merci de patienter.</span>');
									$.ajax({
										  type: "post",
										  url: "../cron/cron_add_product_mdl.php"
									})
									.done(function( msg ) {
										alert( "Importation terminée"); 
										location.reload();
									});
								}
							</script>
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button>
							<button  style="margin-left:20px;" class="btn btn-success" onclick="importation();"><span class="icon16 icomoon-icon-box-add white"></span> Importer</button><br/><br/><p id="loading"></p></center><br>';

			
			
			
			
			
			$strSQLList = "	SELECT o.* ,DATE_FORMAT(o.ouvrage_date_parution , '%d/%m/%Y') as date_parution,
								IF(ouvrage_actif = 1 , 'Oui', 'Non') as visible,
								IF(ouvrage_upd = 1 , 'Oui', 'Non') as miseajour
							FROM $strTable o
							GROUP BY o.ouvrage_id
						";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "$fieldId");
			$oTable->addColumn("Id collection", "ouvrage_id_collection");
			$oTable->addColumn("Collection", "ouvrage_nom_collection");
			$oTable->addColumn("Ean", "ouvrage_ean");
			$oTable->addColumn("ISBN", "ouvrage_isbn");
			$oTable->addColumn("Titre", "ouvrage_titre");
			$oTable->addColumn("Update", "miseajour");
			$oTable->addColumn("Date parution", "date_parution");
			$oTable->addColumn("Ajout", "ouvrage_date_add");
			$oTable->addColumn("Update", "ouvrage_date_upd");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
			?>
			<div class="title">
					<h4>
						<span>Data form</span>
					</h4>
				</div>
				<div class="content noPad clearfix">
			<?php
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
					
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
				
				
					
				
			
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			$Form->AddSelectArray('ouvrage_upd', 'Mise à jour : ', array(0=>"Non", 1=>"Oui"),true);	
			$Form->AddSelectArray('ouvrage_actif', 'Actif : ', array(1=>"Oui", 0=>"Non"),true );	
			// $Form->AddSelectSQL('langue_id', 'Langue :','SELECT langue_id as id,langue_libelle as lib FROM eng_langue WHERE langue_actif = 1 ', 'lib', 'id', "", true);		
			$Form->AddInput('text', 'ouvrage_ean',  'Ean : ', true);
			$Form->AddInput('text', 'ouvrage_titre',  'Titre : ', true);			
			$Form->AddInput('date', 'ouvrage_date_parution',  'Date parution (yyyy-mm-dd) : ', false);
			$Form->AddAjaxFile("ouvrage_image", "Image : ", "../uploads/ouvrages/","bor_ouvrage", $fieldId, $_GET["id"], "img", 1, $strFilesTable="eco_files");
			
			$Form->CloseFormBox();
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			if ($_GET["id"] == $_CONST["FORM_ID_AJOUT"])
				$Form->AddHidden("ouvrage_date_add",  date("Y-m-d H:i:s"));
			else
				$Form->AddHidden("ouvrage_date_add",  $_POST['ouvrage_date_add']);
			$Form->AddHidden("ouvrage_date_upd",  date("Y-m-d H:i:s") );
			
			if ($Form->Validate($strButtonAction)) {
			
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
			
				$_REQUEST['ouvrage_date_upd'] =  date("Y-m-d H:i:s") ;
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					$oDb->updateTable($action, $strTable);
				}
				if ($action == $_CONST["FW_ACTION_INS"]) {
					$ID = mysql_insert_id();
					$oDb->query("UPDATE eco_files set files_table_id = $ID WHERE files_table_id ='-2' and files_table_id_name = 'ouvrage_id'");
				}else{
					$ID = mysql_real_escape_string($_GET["id"]);
				}
				
				
				
				
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			?></div><?php
		}
		?>
	</span>
</div>