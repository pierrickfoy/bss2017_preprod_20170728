<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "partenaire";
$strTable = 'bor_partenaire';
$fieldId = "partenaire_id";
$strTitle = "Partenaires";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
							
		<?php
		
		/* LISTE */
		if (empty($_GET["id"]) && empty($_GET["id_bloc"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button>
							</center><br>';

			
			
			
			
			
			$strSQLList = "	SELECT p.* ,DATE_FORMAT(p.partenaire_date , '%d/%m/%Y') as partenaire_date	FROM $strTable p ";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "$fieldId");
			$oTable->addColumn("Date", "partenaire_date");
			$oTable->addColumn("Nom", "partenaire_name");
			$oTable->addColumn("Url", "partenaire_url");
			$oTable->addColumn("Tags Analytics", "tag_analytics");
			
			$oTable->addColumn("Commande : Code action", "bill_codeaction_commande");
			$oTable->addColumn("Commande : Origine inscription", "per_origine_commande");
			
			$oTable->addColumn("Découverte : Code action", "bill_codeaction_decouverte");
			$oTable->addColumn("Découverte : Origine inscription", "per_origine");
			
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else if(!empty($_GET["id"])){
			?>
			
			
				<div class="title">
					<h4>
						<span>Data form</span>
					</h4>
				</div>
				<div class="content noPad clearfix">
					<?php
					$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
					$action = $_CONST["FW_ACTION_INS"];
					$strButtonAction = "Enregistrer";
					$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
					
							
					if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
					{
						$Form->Set_EditData($strSQLForm);
						$action = $_CONST["FW_ACTION_UPD"];
					}
					if (!empty($_GET["del"])) {
						$action = $_CONST["FW_ACTION_DEL"];
						$strButtonAction = "Supprimer";
					}
					
					$Form->AddInput('text', 'partenaire_name',  'Nom : ', true);
					$Form->AddInput('text', 'partenaire_code',  'Code : ', true);
					$Form->AddInput('text', 'partenaire_url',  'URL : ', true);
					$Form->AddInput('text', 'tag_analytics',  'Tag Analytics : ', false);
					$Form->AddInput('text', 'url_header',  'URL Header : ', true);
					$Form->AddInput('text', 'url_footer',  'URL Footer : ', true);
					$Form->AddInput('text', 'url_cssjs',  'URL CSS/JS: ', false);
					
					$Form->AddInput('text', 'bill_codeaction_commande',  'Commande : Code action : ', false);
					$Form->AddInput('text', 'per_origine_commande',  'Commande : Origine inscription : ', false);
					
					$Form->AddInput('text', 'bill_codeaction_decouverte',  'Découverte : Code action : ', false);
					$Form->AddInput('text', 'per_origine',  'Découverte : Origine inscription : ', false);
					
					
					// $Form->AddInput('text', 'bill_origine',  'Origine BILL : ', false);
					
					
					
					
					
					$Form->CloseFormBox();
					$Form->AddHidden("$fieldId",  $_GET["id"]);
					if ($_GET["id"] == $_CONST["FORM_ID_AJOUT"])
						$Form->AddHidden("partenaire_date",  date("Y-m-d"));
					else
						$Form->AddHidden("partenaire_date",  $_POST['partenaire_date']);
					
					if ($Form->Validate($strButtonAction)) {
					
						$_POST = $oDb->secureData($_POST);
						$_REQUEST = $oDb->secureData($_REQUEST);
						$_GET = $oDb->secureData($_GET);
					
						if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
							|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
							$oDb->updateTable($action, $strTable);
						}
								
						
						
						echo "<script>document.location.href='index.php?template=$tpl';</script>";
					}
					else
						$Form->Display();
					?>
					<div class="box hover">
						<div class="title">
							<h4> 
								<span>Landing Page</span>
							</h4>
						</div>
						<div class="content">
							<?php 
							if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"]){
								echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id_bloc='.$_CONST["FORM_ID_AJOUT"].'&partenaire_id='.mysql_real_escape_string($_GET['id']).'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
								$strSQLList = "	SELECT * , 
													IF(bloc_visible = 1, 'Oui', 'Non') as bloc_visible
													FROM bor_partenaire_bloc pb
													INNER JOIN bor_partenaire_type_bloc ptb ON (ptb.type_id = pb.type_id) 
													WHERE pb.partenaire_id = '".mysql_real_escape_string($_GET['id'])."'";
								$oTable = new JoTable("$strTable");
								$oTable->bOptMultiSearch = false;
								$oTable->strCSSdefaultClass = "gradeJM";
								$oTable->strJSPath = "../lib/media/js/";
								$oTable->strCSSPath = './css/';
								$oTable->PrintHeaders(false);
								$oTable->addColumn("Id", "bloc_id");
								$oTable->addColumn("Type", "type_titre");
								$oTable->addColumn("Titre menu", "bloc_menu");
								$oTable->addColumn("Titre bloc (hors bloc html)", "bloc_titre");
								$oTable->addColumn("Visible", "bloc_visible");
								$oTable->addColumn("Position", "bloc_position");
								if ($iRightLevel_tpl > 1)
									$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id_bloc" => "bloc_id", "_all_" => "template={$tpl}&partenaire_id=".mysql_real_escape_string($_GET['id'])));
								if ($iRightLevel_tpl > 2)
									$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id_bloc" => "bloc_id", "_all_" => "template=$tpl&del=1&partenaire_id=".mysql_real_escape_string($_GET['id'])));
								$oTable->ShowTableFromSQL($strSQLList);
							}else{
								echo "<p>Il faut enregistrer le partenaire avant de pouvoir éditer les blos de sa landing page.</p>"; 
							}
							
							?>	
						</div>
					</div>
				</div>			
	<?php
		}else if(!empty($_GET["id_bloc"])){
		?>
				<div class="title">
					<h4>
						<span>Bloc Landing Page</span>
					</h4>
				</div>
				<div class="content noPad clearfix">
					<?php
					
					$Form = new JoForm($strTable."_Form", "index.php?id_bloc=".mysql_real_escape_string($_GET["id_bloc"])."&template=partenaire&partenaire_id=".mysql_real_escape_string($_GET["partenaire_id"]). ( $_GET['del'] == 1 ? "&del=1": ""), "70% align=center", true, "$strTitle", "../js/", "../css/");
					$action = $_CONST["FW_ACTION_INS"];
					$strButtonAction = "Enregistrer";
					$strSQLForm = "SELECT * FROM bor_partenaire_bloc WHERE bloc_id='".mysql_real_escape_string($_GET["id_bloc"])."'";
					
							
					if ($_GET["id_bloc"] != $_CONST["FORM_ID_AJOUT"])
					{
						$Form->Set_EditData($strSQLForm);
						$action = $_CONST["FW_ACTION_UPD"];
					}
					if (!empty($_GET["del"])) {
						$action = $_CONST["FW_ACTION_DEL"];
						$strButtonAction = "Supprimer";
					}
					
					
					$Form->AddSelectSQL('type_id', 'Type :',"SELECT type_id, type_titre FROM bor_partenaire_type_bloc ", 'type_titre', 'type_id', "", true);
					$Form->AddSelectArray('bloc_visible', 'Visible : ', array(1=>"Oui", 0=>"Non"),true);
					// $Form->AddSelectArray('btn_up', 'Bouton retour en haut : ', array(1=>"Oui", 0=>"Non"),true);
					
					$Form->AddInput('text', 'bloc_position',  'Position : ', false);
					$Form->AddInput('text', 'bloc_menu',  'Menu : ', false);
					$Form->AddInput('text', 'bloc_menu_tag',  'Tag analytics (menu) : ', false);
					$Form->AddArea('bloc_titre',  'Titre bloc (hors bloc html) : ',6, 8, false,"tinymce");
					$Form->AddArea('bloc_html',  'Html : ',6, 8, false,"tinymce");
			
					
					
					$Form->CloseFormBox();
					$Form->AddHidden("bloc_id",  $_GET["id_bloc"]);
					$Form->AddHidden("partenaire_id",  $_GET["partenaire_id"]);
					if ($_GET["id"] == $_CONST["FORM_ID_AJOUT"])
						$Form->AddHidden("partenaire_date",  date("Y-m-d"));
					else
						$Form->AddHidden("partenaire_date",  $_POST['partenaire_date']);
					
					if ($Form->Validate($strButtonAction)) {
					
						$_POST = $oDb->secureData($_POST);
						$_REQUEST = $oDb->secureData($_REQUEST);
						$_GET = $oDb->secureData($_GET);
					
						if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
							|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
							$oDb->updateTable($action,"bor_partenaire_bloc");
						}
								
						
						
						echo "<script>document.location.href='index.php?template={$tpl}&id=".mysql_real_escape_string($_GET["partenaire_id"])."';</script>";
					}
					else
						$Form->Display();
					?>
					
				</div>			
		<?php
		
		}
		?>
	</span>
</div>