<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "presse_document";
$strTable = 'bor_'.$tpl;
$fieldId = $tpl.'_id';
$strTitle = "Documents";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			// echo $strTable2;exit; 
			
			$strSQLList = "	SELECT *, IF({$tpl}_actif = 1 , 'Oui', 'Non')as actif 
							FROM $strTable 
							";
			// var_dump($strSQLList);exit;
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("ID", "presse_document_id");
			$oTable->addColumn("Titre", "presse_document_titre");
			$oTable->addColumn("Date", "presse_document_date");
			$oTable->addColumn("actif", "actif");
			$oTable->addColumn("Position", "presse_document_position");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
		
		
			
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
			/* LOAD LANG FIELDS */
			// $aFieldsLang = $oDb->queryTab("SELECT * FROM $strTable2 t WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'");
			// foreach ($aFieldsLang as $aData) {
				// $iLangsID = $aData['langs_id'];
				// foreach ($aData as $FieldName => $FieldValue) {
					// $_POST[$FieldName.'-'.$iLangsID] = $FieldValue;
				// }
			// }
			/* END LOAD LANG FIELDS */
			
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			$Form->OpenFormBox('General');

			$Form->AddInput('text', 'presse_document_titre',  'Titre : ', false);
			// $Form->AddRadio('presse_document_actif', 'Actif : ', array(1=>'Oui', 0=>'Non'), false , "h", 1);
			$Form->AddSelectArray('presse_document_actif', 'Actif* : ', array(1=>"Oui", 0=>"Non"),true);
			$Form->AddInput('text', 'presse_document_position',  'Position : ', false);
			if($_GET["id"]!='-2')
			$Form->AddHidden("presse_document_date",  $_POST["presse_document_date"]);
			// $Form->AddFile('presse_document_doc','Documents');
			
			if($_GET["id"]=='-2') {
			
				$sqlfiles_table_id = "SHOW TABLE STATUS LIKE  '".$strTable."'";
				$next =  $oDb -> queryForm($sqlfiles_table_id);
				$next = $next['Auto_increment'];

			}
			else { 
		        $next = $_GET["id"]; 
			}
			
			$Form->AddAjaxFile('presse_document_image', 'Documents : ', "../uploads/presse_documents/","bor".$tpl, $fieldId, $next, "doc", 1, $strFilesTable="eco_files");
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			$Form->CloseFormBox();
			if ($Form->Validate($strButtonAction)) {

				
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					if($_GET["id"]=='-2')
						$_REQUEST['presse_document_date'] = date('Y-m-d');
					$oDb->updateTable($action, $strTable);
				}
				
				
				/*DELETE KANG FIELDS */
				if ($action == $_CONST["FW_ACTION_DEL"] && $iRightLevel_tpl > 2) {
					// $oDb->Squery("DELETE FROM $strTable2 WHERE $fieldId='".($_REQUEST["$fieldId"])."'");
					$oDb->Squery("DELETE FROM $strTable WHERE $fieldId='".($_REQUEST["$fieldId"])."'");
				}
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			
		}
		?>
	</span>
</div>