<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "produit";
$strTable = 'bor_'.$tpl;
$fieldId = $tpl.'_id';
$strTitle = "Produits";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
			
			$strSQLList = "	SELECT *, IF({$tpl}_actif = 1 , 'Oui', 'Non') as actif 
							FROM $strTable";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "produit_id");
			$oTable->addColumn("EAN", "produit_ean");
			$oTable->addColumn("Titre", "produit_titre");
			$oTable->addColumn("Support", "produit_support");
			$oTable->addColumn("ISBN", "produit_isbn");
			$oTable->addColumn("Accroche", "produit_accroche_yonix");
			$oTable->addColumn("Prix", "produit_prix");
			$oTable->addColumn("Active", "produit_actif");
			$oTable->addColumn("Actif", "actif");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
		
		
			?>
			<div class="title">
					<h4>
						<span>Data form</span>
					</h4>
				</div>
				<div class="content noPad clearfix">
			<?php
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
			
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			
			$Form->AddSelectArray('produit_actif', 'Actif* : ', array(1=>"Oui", 0=>"Non"),true);
			$Form->AddInput('text', 'formule_id_yonix',  'Formule id YONIX : ', false);
			$Form->AddInput('text', 'classe_id_yonix',  'Classe id YONIX* : ', true);
			$Form->AddInput('text', 'matiere_id_yonix',  'Matiére id YONIX* : ', true);
			$Form->AddInput('text', 'de_id_yonix',  "Durée d'engagement YONIX* : ", true);
			$Form->AddInput('text', 'produit_ean',  'EAN* : ', true);
			$Form->AddInput('text', 'produit_titre',  'Titre* : ', true);
			$Form->AddInput('text', 'produit_support',  'Support* : ', true);
			$Form->AddInput('text', 'produit_isbn',  'ISBN* : ', true);
			$Form->AddInput('text', 'produit_prix',  'Prix TTC YONIX: ', true);
                        $Form->AddInput('text', 'produit_prix_ht',  'Prix HT YONIX: ', true);
			$Form->AddArea('produit_presentation_yonix',  'Présentation YONIX: ',6, 8, false, 'tinymce');
			$Form->AddArea('produit_accroche_yonix',  'Accroche YONIX: ',6, 8, false, 'tinymce');
			  
			 if($_GET["id"]=='-2') {			
				$sqlfiles_table_id = "SHOW TABLE STATUS LIKE  '".$strTable."'";
				$next =  $oDb -> queryForm($sqlfiles_table_id);
				$next = $next['Auto_increment'];
			}
			else { 
		        $next = $_GET["id"]; 
			}
			
			// $Form->AddAjaxFile('produit_image', 'Image : ', "../uploads/produits/","bor".$tpl, $fieldId, $next, "img", 1, "eco_files");
			
			
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			
			if ($Form->Validate($strButtonAction)) {
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					$oDb->updateTable($action, $strTable);
				}
				
				/*DELETE KANG FIELDS */
				if ($action == $_CONST["FW_ACTION_DEL"] && $iRightLevel_tpl > 2) {
					$oDb->Squery("DELETE FROM $strTable WHERE $fieldId='".($_REQUEST["$fieldId"])."'");
				}
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			?></div><?php
		}
		?>
	</span>
</div>