<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "resiliation";
$strTable = 'bor_resiliation';
$fieldId = "resiliation_id";


$strTitle = "Résiliation";

?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
							<script type="text/javascript">
								function importation(){							
									$.ajax({
										  type: "post",
										  url: "../ecoadmin/sc_export_resiliation.php.php"
									})
									.done(function( msg ) {
										alert( "Importation terminée"); 
										location.reload();
									});
								}
							</script>
		<?php
		
		/* LISTE */
		
			echo '
				<center>
					
					<a style="margin-left:20px;" class="btn btn-success" href="sc_export_resiliation.php" target="_blank"><img src="images/send.png"> Export résiliations</a>
					
				</center><br>';

			
			
			
			
			
			$strSQLList = "	SELECT * FROM $strTable  ";
			
			?>
			 <div class="title">
				<h4>
					<span>Liste des résiliations</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id résiliation", "$fieldId");
			$oTable->addColumn("Id commande", "commande_id");
			$oTable->addColumn("Durée", "duree");
			$oTable->addColumn("Formule", "formule");
			$oTable->addColumn("Questions", "questions");
			$oTable->addColumn("Expression", "expression");
			
			/*if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));*/
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php

		?>
	</span>
</div>