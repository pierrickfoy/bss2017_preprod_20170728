<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "rubrique";
$strTable = 'bor_'.$tpl;
$fieldId = $tpl.'_id';
$strTitle = "Rubrique";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
			
			$strSQLList = "	SELECT *, IF({$tpl}_actif = 1 , 'Oui', 'Non')as actif  
							FROM $strTable";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("ID", "rubrique_id");
			$oTable->addColumn("Titre", "rubrique_titre");
			$oTable->addColumn("Position", "rubrique_position");
			$oTable->addColumn("actif", "actif");
			$oTable->addColumn("Date ajout", "rubrique_date_add");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
		
		
			
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
			/* LOAD LANG FIELDS */
			// $aFieldsLang = $oDb->queryTab("SELECT * FROM $strTable2 t WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'");
			// foreach ($aFieldsLang as $aData) {
				// $iLangsID = $aData['langs_id'];
				// foreach ($aData as $FieldName => $FieldValue) {
					// $_POST[$FieldName.'-'.$iLangsID] = $FieldValue;
				// }
			// }
			/* END LOAD LANG FIELDS */
			
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			// $Form->AddSelectSQL('menu_group_id', 'Emplacement: ', 'SELECT menu_group_id, menu_group_name FROM eco_menu_group', 'menu_group_name', 'menu_group_id');
			// $Form->AddSelectSQL('menu_parent_id', 'Menu parent : ',"	SELECT t.menu_id, menu_title 
																		// FROM $strTable t 
																		// INNER JOIN $strTable2 t2 ON (t2.$fieldId = t.$fieldId)
																		// INNER JOIN eco_langs l ON (l.langs_id=t2.langs_id) 
																		// WHERE t2.langs_id=1", 'menu_title', 'menu_id');
			// $Form->AddSelectSQL('templates_id', 'Page : ', 'SELECT templates_id, templates_name FROM eco_templates', 'templates_name', 'templates_id');
			
			$Form->OpenFormBox('General');
			
			
			$Form->AddInput('text', 'rubrique_titre',  'Titre : ', true);
			$Form->AddInput('text', 'rubrique_position',  'Position : ', false);
			$Form->AddSelectArray('rubrique_actif', 'Actif* : ', array(1=>"Oui", 0=>"Non"),true);
			$Form->AddArea('rubrique_description',  'Description : ',6, 8, false, "tinymce");
			$Form->AddInput('text', 'rubrique_metatitle',  'META title : ', false);
			$Form->AddArea('rubrique_metadesc',  'META description : ',6, 8, false);
			if($_GET["id"]!='-2')
			$Form->AddHidden("rubrique_date_add",  $_POST["rubrique_date_add"]);
			
			/*MABA*/
			// if($_GET["id"]=='-2') {
			
				// $sqlfiles_table_id = "SHOW TABLE STATUS LIKE  '".$strTable."'";
				// $next =  $oDb -> queryForm($sqlfiles_table_id);
				// $next = $next['Auto_increment'];

			// }
			// else { 
		        // $next = $_GET["id"]; 
			// }
			/*MABA*/
			// $Form->AddAjaxFile('rubrique_img', 'Image : ', "../images/uploads/rubrique/","bor".$tpl, $fieldId, $next, "img", 1, $strFilesTable="eco_files");
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			$Form->CloseFormBox();
			if ($Form->Validate($strButtonAction)) {
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
				
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					if($_GET["id"]=='-2')
						$_REQUEST['rubrique_date_add'] = date('Y-m-d');
					$oDb->updateTable($action, $strTable);
				}
				
				
				/*DELETE KANG FIELDS */
				if ($action == $_CONST["FW_ACTION_DEL"] && $iRightLevel_tpl > 2) {
					// $oDb->Squery("DELETE FROM $strTable2 WHERE $fieldId='".($_REQUEST["$fieldId"])."'");
					$oDb->Squery("DELETE FROM $strTable WHERE $fieldId='".($_REQUEST["$fieldId"])."'");
				}
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			
		}
		?>
	</span>
</div>