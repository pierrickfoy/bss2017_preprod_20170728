<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "slider";
$strTable = 'bor_'.$tpl;
$fieldId = $tpl.'_id';
$strTitle = "Slider";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
			
			$strSQLList = "	SELECT *, IF({$tpl}_actif = 1 , 'Oui', 'Non')as actif , IF({$tpl}_target = 1 , '_blank', 'Interne')as target , 
										IF({$tpl}_popup = 1 , 'Oui', 'Non')as popup  FROM $strTable";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("ID", "slider_id");
			$oTable->addColumn("Titre", "slider_titre");
			$oTable->addColumn("Target", "target");
			$oTable->addColumn("Popup", "popup");
			$oTable->addColumn("Position", "slider_position");
			$oTable->addColumn("actif", "actif");
			$oTable->addColumn("Date ajout", "slider_date_add");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
		
		
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
			
			
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			$Form->OpenFormBox('General');
			$Form->AddSelectArray('slider_popup', 'Popup Youtube* : ', array(1=>"Oui", 0=>"Non"),true);
			
			$Form->AddSelectArray('slider_target', 'Target* : ', array(1=>"_blank", 0=>"interne"),true);
			
			$Form->AddInput('text', 'slider_titre',  'Titre : ', true);
			$Form->AddInput('text', 'slider_position',  'Position : ', false);
			$Form->AddSelectArray('slider_actif', 'Actif* : ', array(1=>"Oui", 0=>"Non"),true);
			$Form->AddInput('text', 'slider_lien',  'Lien : ', false);
			// $Form->AddInput('date', 'slider_date_add',  'Date ajout : ', true);
			if($_GET["id"]!='-2')
			$Form->AddHidden("slider_date_add",  $_POST["slider_date_add"]);
			/*MABA*/
			if($_GET["id"]=='-2') {
			
				$sqlfiles_table_id = "SHOW TABLE STATUS LIKE  '".$strTable."'";
				$next =  $oDb -> queryForm($sqlfiles_table_id);
				$next = $next['Auto_increment'];

			}
			else { 
		        $next = $_GET["id"]; 
			}
			/*MABA*/
			$Form->AddAjaxFile('slider_image', 'Image : ', "../uploads/slider/","bor".$tpl, $fieldId, $next, "img", 1, $strFilesTable="eco_files");
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			$Form->CloseFormBox();
			if ($Form->Validate($strButtonAction)) {
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					if($_GET["id"]=='-2')
						$_REQUEST['slider_date_add'] = date('Y-m-d');
					$oDb->updateTable($action, $strTable);
				}
				
	
				
				/*DELETE KANG FIELDS */
				if ($action == $_CONST["FW_ACTION_DEL"] && $iRightLevel_tpl > 2) {
					// $oDb->Squery("DELETE FROM $strTable2 WHERE $fieldId='".($_REQUEST["$fieldId"])."'");
					$oDb->Squery("DELETE FROM $strTable WHERE $fieldId='".($_REQUEST["$fieldId"])."'");
				}
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			
		}
		?>
	</span>
</div>