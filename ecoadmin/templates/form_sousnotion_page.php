<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "sousnotion_page";
$strTable = 'bor_'.$tpl;
$fieldId = $tpl.'_id';
$strTitle = "sous-Notions";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
			
			$strSQLList = "	SELECT *, IF(sousnotion_page_status = 1 , 'Oui', 'Non') as actif 
							FROM $strTable";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "sousnotion_page_id");
			$oTable->addColumn("Id Notions", "notions_id");
			$oTable->addColumn("Nom", "sousnotion_page_h1");
			$oTable->addColumn("Actif", "actif");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
		
		
			
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
			
			
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			$Form->OpenFormBox('Pages CLASSE > MATIÈRE > CHAPITRE > NOTION > SOUS-NOTIONS');
			
			$Form->AddSelectArray('sousnotion_page_status', 'Actif* : ', array(1=>"Oui", 0=>"Non"),true);
            //$Form->AddSelectSQL('classes_yonix_id', 'Classe : ', 'SELECT classe_id_yonix as id, classe_name as lib FROM bor_classe WHERE classe_actif = 1 ORDER BY classe_name ', 'lib', 'id', true, true);
			//$Form->AddSelectSQL('matiere_yonix_id', 'Matière : ', 'SELECT matiere_id_yonix as id, matiere_titre as lib FROM bor_matiere  ORDER BY matiere_position ', 'lib', 'id', true, true);
			$Form->AddSelectSQL('notions_id', 'Notion* : ', 'SELECT notions_id as id, notions_h1 as lib FROM bor_notions ', 'lib', 'id', true, true);
            $Form->AddInput('text', 'sousnotion_page_h1',  'Titre H1* : ', true);
			$Form->AddInput('text', 'sousnotion_page_metatitle',  'Meta Title* : ', true);
			$Form->AddArea('sousnotion_page_metadesc',  'Meta Description : ',6, 8, false);
			$Form->AddInput('text', 'sousnotion_page_url_seo',  'Permalien (url réécrite SEO) : ', false);
			$Form->AddArea('sousnotion_page_accroche',  'Accroche : ',6, 8, true, 'tinymce');
			$Form->AddArea('sousnotion_page_content',  'Contenu : ',6, 8, false, 'tinymce');
			$Form->AddInput('text', 'sousnotion_page_url',  'Position : ', false);
                        
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			$Form->CloseFormBox();
                        
			if ($Form->Validate($strButtonAction)) {
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					$oDb->updateTable($action, $strTable);
				}
				
				/*DELETE KANG FIELDS */
				if ($action == $_CONST["FW_ACTION_DEL"] && $iRightLevel_tpl > 2) {
					$oDb->Squery("DELETE FROM $strTable WHERE $fieldId='".($_REQUEST["$fieldId"])."'");
				}
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			
                        }
		?>
	</span>
</div>