<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "templates";
$strTable = 'eco_templates';
$strTable2 = 'eco_templates_lang';
$fieldId = "templates_id";
$strTitle = "Vos pages";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
			
			$strSQLList = "SELECT * FROM $strTable t";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Id", "templates_id");
			$oTable->addColumn("Nom", "templates_name");
			$oTable->addColumn("Visible", "templates_visible");
			$oTable->addColumn("Fichier", "templates_filename");
			
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
		
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
				
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			
			/* LOAD LANG FIELDS */ 
			$aFieldsLang = $oDb->queryTab("SELECT * FROM $strTable2 t WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'");
			foreach ($aFieldsLang as $aData) {
				$iLangsID = $aData['langs_id'];
				foreach ($aData as $FieldName => $FieldValue) {
					$_POST[$FieldName.'-'.$iLangsID] = $FieldValue;
				}
			}
			/* END LOAD LANG FIELDS */ 
			
			$Form->OpenFormBox('General');
			$Form->AddInput('text', 'templates_name',  'Nom de la page : ', true);
			$Form->AddInput('text', 'templates_filename',  'Fichier spécifique : ', false);
			$Form->AddRadio('templates_visible', 'Page visible ? : ', array(1=>'Oui', 0=>'Non'), true , "h", 1);
			$aRobots = array(1 => "No Index - Follow", 2 => "Index - No Follow", 3 => "No Index - No Follow"); 
			$Form->AddSelectArray("templates_meta_robot", "Robot : ",$aRobots , false);
			$Form->CloseFormBox();
			
			$aLangs = $oDb->queryTab('SELECT langs_id, langs_name FROM eco_langs');
			foreach ($aLangs as $aLang) {
				$Form->OpenFormBox($aLang['langs_name']);
				$Form->AddInput('text', 'templates_meta_title-'.$aLang['langs_id'],  'META title : ', true);
				$Form->AddArea('templates_meta_desc-'.$aLang['langs_id'],  'META description : ', 2, 40, true);
				$Form->AddInput('text', 'templates_title-'.$aLang['langs_id'],  'Titre : ', true);
				
				$Form->AddArea('templates_content-'.$aLang['langs_id'],  'Contenu : ', 40, 40, false, "tinymce");
				$Form->CloseFormBox();
			}
			
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			
			if ($Form->Validate($strButtonAction)) {
			
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
				
				$_REQUEST[$tpl.'_created_date'] = $action == $_CONST["FW_ACTION_INS"] ? Date("Y-m-d H:i:s") : $_POST[$tpl.'_created_date'];
				$_REQUEST[$tpl.'_updated_date'] =  Date("Y-m-d H:i:s");
				$_REQUEST[$tpl.'_lang_created_date'] = $action == $_CONST["FW_ACTION_INS"] ? Date("Y-m-d H:i:s") : $_POST[$tpl.'_created_date'];
				$_REQUEST[$tpl.'_lang_updated_date'] =  Date("Y-m-d H:i:s");
				
				if ((($action == $_CONST["FW_ACTION_UPD"] || $action == $_CONST["FW_ACTION_INS"]) && $iRightLevel_tpl > 1)
					|| ($action == $_CONST["FW_ACTION_DEL"]  && $iRightLevel_tpl > 2)) {
					$oDb->updateTable($action, $strTable);
				}
				
				/* UPDATE LANG FIELDS */
				foreach($_REQUEST as $Key => $Value) {
					if (preg_match('/([a-zA-Z_]+)-(\d+)/', $Key, $aMatches)) { 
						$fieldName = $aMatches[1];
						$langs_id = $aMatches[2];
						if ($action == $_CONST["FW_ACTION_UPD"] && $iRightLevel_tpl > 1)
							$oDb->Squery("UPDATE $strTable2 SET $fieldName='".($Value)."' WHERE langs_id='".mysql_escape_string($langs_id)."' AND $fieldId='".($_REQUEST["$fieldId"])."'");
						elseif ($action == $_CONST["FW_ACTION_INS"])
							$aInsertData[$langs_id][$fieldName] = $Value;
					}
				}
				/* INSERT LANG FIELDS */
				if ($action == $_CONST["FW_ACTION_INS"]) {
					$ID = mysql_insert_id();
					foreach($aInsertData as $langs_id => $aData) {
						$oDb->Squery("INSERT INTO $strTable2 VALUES('', '".$ID."', '".mysql_escape_string($langs_id)."', '".($aData['templates_meta_title'])."','".($aData['templates_meta_desc'])."','".($aData['templates_meta_keywords'])."','".($aData['templates_title'])."','".($aData['templates_content'])."','".$_REQUEST[$tpl.'_lang_created_date']."','".$_REQUEST[$tpl.'_lang_updated_date']."')");
					}
				}
				
				/*DELETE KANG FIELDS */
				if ($action == $_CONST["FW_ACTION_DEL"] && $iRightLevel_tpl > 2) {
					$oDb->Squery("DELETE FROM $strTable2 WHERE $fieldId='".($_REQUEST["$fieldId"])."'");
					$oDb->Squery("DELETE FROM $strTable WHERE $fieldId='".($_REQUEST["$fieldId"])."'");
				}
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
		}
		?>
	</span>
</div>