<?php
if (empty($iRightLevel_tpl)) {
	echo 'PERMISSION DENIED';
	return;
}

$oConnection = $oDb;
$tpl = "users";
$strTable = 'eco_bo_users';
$fieldId = "users_id";
$strTitle = "Utilisateurs";
?>


 <div class="heading">

	<h3><?php echo $strTitle; ?></h3>                    

	<div class="resBtnSearch">
		<a href="#"><span class="icon16 icomoon-icon-search-3"></span></a>
	</div>
	
	<ul class="breadcrumb">
		<li>You are here:</li>
		<li>
			<a href="index.php" class="tip" title="back to dashboard">
				<span class="icon16 icomoon-icon-screen-2"></span>
			</a> 
			<span class="divider">
				<span class="icon16 icomoon-icon-arrow-right-2"></span>
			</span>
		</li>
		<li class="active"><?php echo $strTitle; ?></li>
	</ul>

</div><!-- End .heading-->
                <div class="row-fluid">

                     <div class="span12">

                            <div class="box gradient">
		<?php
		
		/* LISTE */
		if (empty($_GET["id"])) {
			echo '<center><button class="btn btn-success" onclick="document.location=\'index.php?template='.$tpl.'&id='.$_CONST["FORM_ID_AJOUT"].'\'"><span class="icon16 icomoon-icon-box-add white"></span> Ajouter</button></center><br>';
			
			
			$strSQLList = "	SELECT t.*
							FROM $strTable t
							ORDER BY users_nomprenom";
			
			?>
			 <div class="title">
				<h4>
					<span>Data list</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
								
			<?php
			
			$oTable = new JoTable("$strTable");
			$oTable->bOptMultiSearch = false;
			$oTable->strCSSdefaultClass = "gradeJM";
			$oTable->strJSPath = "../lib/media/js/";
			$oTable->strCSSPath = './css/';
			$oTable->PrintHeaders(false);
			$oTable->addColumn("Nom", "users_nomprenom");
			$oTable->addColumn("Login", "users_login");
			if ($iRightLevel_tpl > 1)
				$oTable->addAction("Modifier", array("_classicon_" => "icomoon-icon-pencil-2", "id" => "$fieldId", "_all_" => "template=$tpl"));
			if ($iRightLevel_tpl > 2)
				$oTable->addAction("Supprimer", array("_classicon_" => "icomoon-icon-cancel-2", "id" => "$fieldId", "_all_" => "template=$tpl&del=1"));
			$oTable->ShowTableFromSQL($strSQLList);
			?></div><?php
		}
		/* FORM */
		else {
		
		
			
			$Form = new JoForm($strTable."_Form", "", "70% align=center", true, "$strTitle", "../js/", "../css/");
			$action = $_CONST["FW_ACTION_INS"];
			$strButtonAction = "Enregistrer";
			$strSQLForm = "SELECT * FROM $strTable WHERE $fieldId='".mysql_real_escape_string($_GET["id"])."'";
				
			if ($_GET["id"] != $_CONST["FORM_ID_AJOUT"])
			{
				$Form->Set_EditData($strSQLForm);
				$action = $_CONST["FW_ACTION_UPD"];
			}
			if (!empty($_GET["del"])) {
				$action = $_CONST["FW_ACTION_DEL"];
				$strButtonAction = "Supprimer";
			}
			$Form->OpenFormBox('General');
			$Form->AddInput("text", "users_email",  "Email : ", true);
			$Form->AddInput("text", "users_login",  "Login : ", true);
			$Form->AddInput("text", "users_pwd",  "Password: ", true);
			$Form->AddInput("text", "users_nomprenom",  "Nom et Prénom : ", true);
			$Form->AddHidden("$fieldId",  $_GET["id"]);
			
			
			//GESTION DES DROITS
			$Form->HtmlForm .= "</td>\n</tr><tr>\n<td  colspan=2><table width='100%'>\n";
			$Form->HtmlForm .= "<tr><td colspan=4><b>GESTION DROITS</b></td></tr>\n";
			$aMenu = $oDb->queryTab("SELECT * FROM eco_bo_menu");
			foreach ($aMenu as $aMenuItem) {
				$iRights_level = $oDb->queryItem('SELECT rights_level FROM eco_bo_rights WHERE users_id='.mysql_real_escape_string($_GET["id"]).' AND menu_id='.$aMenuItem['menu_id']);
				$iRights_level  = empty($iRights_level) ? 0 : $iRights_level ;
				
				$Form->HtmlForm .= '<tr>
									<td>'.$aMenuItem['menu_title'].'</td>
									<td><input type="radio" name="M-'.$aMenuItem['menu_id'].'[]" value="0" '.($iRights_level == 0 ? 'CHECKED' : '').'>FERMER</td>
									<td><input type="radio" name="M-'.$aMenuItem['menu_id'].'[]" value="1" '.($iRights_level == 1 ? 'CHECKED' : '').'>Voir</td>
									<td><input type="radio" name="M-'.$aMenuItem['menu_id'].'[]" value="2" '.($iRights_level == 2 ? 'CHECKED' : '').'>Modifier</td>
									<td><input type="radio" name="M-'.$aMenuItem['menu_id'].'[]" value="3" '.($iRights_level == 3 ? 'CHECKED' : '').'>Supprimer</td>
									</tr>';
			}
			
			$Form->HtmlForm .= "\n</table></td>\n</tr>";
			
			
			$Form->CloseFormBox();
			
			if ($Form->Validate($strButtonAction)) {
				$_POST = $oDb->secureData($_POST);
				$_REQUEST = $oDb->secureData($_REQUEST);
				$_GET = $oDb->secureData($_GET);
				
				$_REQUEST['users_created_date'] = $action == $_CONST["FW_ACTION_INS"] ? Date("Y-m-d H:i:s") : $_POST["users_created_date"];
				$_REQUEST['users_updated_date'] =  Date("Y-m-d H:i:s");
				
				
				
				$oDb->updateTable($action, $strTable);
				
				if ($action == $_CONST["FW_ACTION_INS"])
					$_GET["id"] = $oDb->last_insert_id;
				//GESTION DES DROITS
				foreach ($_POST as $iKey => $strVal) {
					$aTmp = explode('-', $iKey);
					if ($aTmp[0] == 'M') {
						$iRights_id = $oDb->queryItem('SELECT rights_id FROM eco_bo_rights WHERE users_id='.($_GET["id"]).' AND menu_id='.$aTmp[1]);
						if ($oDb->rows == 1)
							$oDb->Squery('UPDATE eco_bo_rights SET rights_level=\''.($strVal[0]).'\' WHERE rights_id='.$iRights_id);
						else
							$oDb->Squery('INSERT INTO eco_bo_rights VALUES("", "'.($_GET["id"]).'", "'.$aTmp[1].'", "'.($strVal[0]).'")');
					}
					
						
				}
				echo "<script>document.location.href='index.php?template=$tpl';</script>";
			}
			else
				$Form->Display();
			
		}
		?>
	</span>
</div>