

<!--PIED DE SITE-->
<footer class="bss-footer">
<div class="top-footer">
  <div class="container">
    <ul class="row list-inline reassurance text-center">
	<?php /*<li class="col-xs-12 col-sm-12 col-md-2"></li>*/ ?>
      <li class="col-xs-6 col-sm-4 col-md-3"><img src="/img/reassurance-1946.png" class="img-responsive">
        <p>Bordas : Éditeur scolaire depuis 1946</p>
      </li>
      <li class="col-xs-6 col-sm-4 col-md-3"><img src="/img/reassurance-enseignant.png" class="img-responsive">
        <p>Conforme aux programmes de l’Éducation nationale</p>
      </li>
      <li class="col-xs-6 col-sm-4 col-md-3"><img src="/img/reassurance-livres.png" class="img-responsive">
        <p>Contenus rédigés par des enseignants</p>
      </li>
      <?php /*<li class="col-xs-6 col-sm-4 col-md-2"><img src="/img/reassurance-famille.png" class="img-responsive">
        <p>Un compte Parent pour suivre ses résultats</p>
      </li>*/ ?>
      <li class="col-xs-6 col-sm-4 col-md-3"><img src="/img/reassurance-secure.png" class="img-responsive">
        <p>Paiement sécurisé</p>
        <p><img src="/img/icone-paiement.png" class="img-responsive"></p>
      </li>
      <?php /*<li class="col-xs-6 col-sm-4 col-md-2"><img src="/img/reassurance-service-client.png" class="img-responsive">
        <p>Un service Client à votre écoute</p>
      </li>*/ ?>
	  <?php /*<li class="col-xs-12 col-sm-12 col-md-2"></li>*/ ?>
    </ul>
  </div>
</div>  
<div class="mid-footer">
  <div class="container ">
    <div class="row">
    <div class="col-sm-12 h1 text-center">Besoin d'aide ?</div>
      <div class="col-sm-4 clearfix">
       
          <img src="/img/aide-phone.png" class="img-responsive align-left hidden-xs hidden-sm"> 
         
            <ul class="list-unstyled">
              <li><strong>Contactez-nous par téléphone :</strong> </li>
              <li class="phone-number"><?php echo $_CONST['RC_TEL']; ?></li>
              <li>Du lundi au vendredi, </li>
              <li><?php echo $_CONST['RC_HORAIRES']; ?></li>
            </ul>
         
       
      </div>
      <div class="col-sm-4 clearfix">
   
          <img src="/img/aide-mail.png" class="img-responsive align-left hidden-xs hidden-sm"> 
         
            <ul class="list-unstyled">
              <li><strong>Contactez-nous par email :</strong> </li>
              <li><a href="mailto:soutien-scolaire@bordas.tm.fr">soutien-scolaire@bordas.tm.fr</a></li>
            </ul>
       
    
      </div>
      <div class="col-sm-4 clearfix">
    
          <img src="/img/aide-faq.png" class="img-responsive align-left hidden-xs hidden-sm">  
        
            <ul class="list-unstyled">
              <li> <strong>Consultez notre FAQ :</strong> </li>
              <li><a href="/faq.html">Questions fréquentes</a></li>
            </ul>
        
   
      </div>
    </div>
  </div>
</div> 
<?php if((isset($_GET["templates_id"]) && ($_GET["templates_id"] != 50 && $_GET["templates_id"] != 51 && $_GET["templates_id"] != 52 && $_GET["templates_id"] != 53)) || !isset($_GET["templates_id"])){ ?>
<div class="bot-footer">   
  <div class="container ">
    <div class="row">
	<div class="col-sm-2 "></div>
      <div class="col-sm-4 ">
		<h4>Bordas soutien scolaire</h4>
        <ul>
          <li><a href="/mentions-legales.html">Mentions légales & Crédits</a></li>
          <li> <a href="/conditions-generales-d-utilisation.html">CGU</a></li>
          <li><a href="/conditions-generales-de-vente.html">CGV</a></li>
          <li><a href="/partenaires.html">Nos partenaires</a></li>
          <li><a href="/plan-du-site.html">Plan du site</a></li>
        </ul>
      </div>
	  <div class="col-sm-4 ">
		<h4>Les cours de soutien les plus demandés :</h4>
		<ul>
			<li><a href="/anglais/">Cours d’anglais en ligne</a></li>
			<li><a href="/maths/">Cours de maths en ligne</a></li>
			<li><a href="/francais/">Cours de français en ligne</a></li>
		</ul>
	  </div>
	  <div class="col-sm-2 "></div>
      <div class="col-sm-12 text-center">
        <ul class="list-inline">
          <li>Copyright <?php echo date("Y");?></li>
          <li>Bordas Soutien scolaire</li>
          <li><a href="http://www.editions-bordas.fr/" target="_blank" class="logo-bordas">Bordas</a></li>
        </ul>
      </div>
    </div>
  </div>
 </div>
 <?php } ?>
</footer>
