<section class="aide container">
   <h2>Besoin d’aide pour valider votre abonnement ?</h2>
   <div class="row">
      <div class="col-md-2">
        </div>
      <div class="col-md-4">
         <div class="left_aide"><img src="/images/tel_picto.png" alt=""></div>
         <div class="right_aide">
            <dl>
               <dt><?php echo $_CONST['RC_TEL']; ?></dt>
               <dd>Du lundi au vendredi, </dd>
               <dd><?php echo $_CONST['RC_HORAIRES']; ?></dd>
            </dl>
         </div>
      </div>
    
      <div class="col-md-4">
         <div class="left_aide"><img src="/images/contact_picto.png" alt=""></div>
         <div class="right_aide" id="aide_contact">
            <dl>
               <dt>Contactez-nous par email :</dt>
               <dd><a href="mailto:soutien-scolaire@bordas.tm.fr">soutien-scolaire@bordas.tm.fr</a></dd>
            </dl>
         </div>
      </div>
      
   </div>
</section>
<footer>
   <div class="block_reassurance">
      <div class="container">
         <ul class="row">
            <li class="col-md-3">
            </li>
            <li class="col-md-4">
               <div> <img src="/images/icon_reassurance1.png" alt=""> <span>Bordas : Éditeur
                  scolaire depuis 1946</span> 
               </div>
               <div><img src="/images/icon_reassurance2.png" alt=""> <span>Conforme aux programmes
                  de l’Éducation nationale</span> 
               </div>
            </li>
            <li class="col-md-4">
               <div> 	<img src="/images/icon_reassurance3.png" alt=""> <span>Contenus rédigés 
                  par des enseignants</span> 
               </div>
               
            </li>
            
            
         </ul>
      </div>
   </div>
   <div class="copyright">
      <div class="container">Bordas Soutien scolaire est un service des éditions 
         <a href="http://www.editions-bordas.fr/" target="_blank"><img src="/images/logo_copyright.png" alt=""></a>
      </div>
   </div>
</footer>