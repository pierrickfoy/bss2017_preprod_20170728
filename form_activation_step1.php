<?php
unset($_SESSION['cart']); 
unset($_SESSION['shipping']); 
unset($_SESSION['reglement']); 
unset($_SESSION['products']);
unset($_SESSION['paiement']);
unset($_SESSION['nb_enfant']);
unset($_SESSION['partenaire']);
unset($_SESSION["chx_cgu"]);
unset($_SESSION["code_activation"]);
if(isset($_POST["action"]) && $_POST["action"] == "step_1_1"){    
    
    if( !isset($_POST["code_activation"]) || empty($_POST["code_activation"]) || !preg_match("/^[0-9a-zA-Z-_]{1,21}$/", $_POST['code_activation']) ){
        $_SESSION['code_activation'] = $_POST["code_activation"] ;
        $bError = true; 
        $strStep = "step_1_1"; 
        $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
        $strError .= "Le code d’activation saisi n’est pas valide. Veuillez le vérifier et le ressaisir.";
        $strError .="</span></p>";
    }else{        
        $_SESSION['code_activation'] = $_POST["code_activation"] ;
        //Appel du web service pour vérifier le code d'activation testjm-RUS
        $strUrlWS = "https://www.e-interforum.com/intra/webservices/per/get_code_status.php?cm=BRD&code_avantage=".$_SESSION['code_activation'];
        try{
            $strXML = file_get_contents($strUrlWS); 
            if(!empty($strXML)){
                $doc = new DOMDocument();
                $doc->loadXML(($strXML));
                $strCodeErreur = $doc->getElementsByTagName('CODE_ERREUR')->item(0)->nodeValue;
                if($strCodeErreur == "0"){
                    $aCodeActivation["CODE_ERREUR"] = $doc->getElementsByTagName('CODE_ERREUR')->item(0)->nodeValue;
                    $aCodeActivation["CODE_AVANTAGE"] = $doc->getElementsByTagName('CODE_AVANTAGE')->item(0)->nodeValue; 
                    $aCodeActivation["MAX_ACTIVATION"] = $doc->getElementsByTagName('CODE_AVANTAGE')->item(0)->getAttribute("max_activation"); 
                    $aCodeActivation["NB_ACTIVATION"] = $doc->getElementsByTagName('CODE_AVANTAGE')->item(0)->getAttribute("nb_activation");
                    $aCodeActivation["CODE_ACTION"] = $doc->getElementsByTagName('CODE_ACTION')->item(0)->nodeValue; 
                    $aCodeActivation["ORIGINE"] = $doc->getElementsByTagName('ORIGINE')->item(0)->nodeValue; 
                    $aCodeActivation["CODE_SITE"] = $doc->getElementsByTagName('CODE_SITE')->item(0)->nodeValue; 
                    $aCodeActivation["DATE"] = $doc->getElementsByTagName('DATE')->item(0)->nodeValue; 
                    $oRessources = $doc->getElementsByTagName('RESSOURCE');
                    //Vérification que la ressource existe 
                    $aCodeActivation["PRODUIT"] = array();
                    foreach( $oRessources as $oRessource ){
                        $strEAN = $oRessource->getElementsByTagName('EAN13')->item(0)->nodeValue;
                        //$strEAN = 3133097365374;
                        $strSql = "SELECT p.*, f.formule_libelle, f.formule_classe FROM bor_produit p INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) WHERE produit_ean = '".mysql_real_escape_string($strEAN)."' LIMIT 1"; 
                        $aProduit = $oDb->queryRow($strSql);
                        if($oDb->rows > 0){
                            $aCodeActivation["PRODUIT"] = $aProduit ;
                            break;
                        }
                    }
                    if(count($aCodeActivation["PRODUIT"]) > 0 && $aCodeActivation["MAX_ACTIVATION"] > $aCodeActivation["NB_ACTIVATION"]){
                        $_SESSION['code_activation'] = $aCodeActivation;
                        //var_dump( $_SESSION['partenaire_activation']['code_activation']);var_dump($_SESSION['partenaire_activation']['code_activation']["PRODUIT"]['formule_classe']);exit;
                        echo "<script>document.location.href='".$_CONST["URL2"]."/code-activation/".$_SESSION['partenaire_activation']["activation_url"]."/activation-formule-tribu-etape-2.html';</script>";
                        exit;  
                    }else{
                        $bError = true; 
                        $strStep = "step_1_1"; 
                        $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
                        $strError .= "Le code d’activation saisi n’est pas valide. Veuillez le vérifier et le ressaisir.";
                        $strError .="</span></p>";
                    }
                }else{
                    $bError = true; 
                    $strStep = "step_1_1"; 
                    $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
                    $strError .= "Le code d’activation saisi n’est pas valide. Veuillez le vérifier et le ressaisir.";
                    $strError .="</span></p>";
                }
            }
        }catch(Exception $e){
            $bError = true; 
            $strStep = "step_1_1"; 
            $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
            $strError .= "Un problème technique empêche la vérification de votre code d'activation.";
            $strError .="</span></p>";
        }
    }
}
?>
<div class="container">
   <div class="row">
     
      <ul id="steps" class="col-md-12">
         <li class="detail_formule"><a class="active"><strong>1.</strong>Saisie du code d'activation<span class="arrow_right"></span></a></li>
         <li class="confirmation"><a  ><span class="arrow_left"></span><strong>2.</strong>Détail de ma formule <span class="arrow_right"></span></a></li>
         <li class="paiement"><a ><span class="arrow_left"></span><strong>3.</strong>Confirmation <span class="arrow_right"></span></a></li>
      </ul>
   </div>
    <div class="row">
    <div class="col-sm-12" style="text-align: center;">
        <h2 class="activation_description">Entrez votre code d'activation pour bénéficier de l'abonnement à la plateforme de soutien scolaire</h2>
    </div>
   </div>
   
   <div class="detail_formule">
      <div class="title_formule">
         <h2>
			Je saisis mon code d'activation
         </h2>
      </div>
      <div class="body_formule ">
         
		 <?php 
		 if($strStep =="step_1_1" && $bError){
			echo ' <div class="alert alert-danger">
				'.$strError.'
			 </div>';
		 }
			echo '	<form class="form-horizontal" role="form" method="post" id="form_activation" name="form_activation"> 
                                    <div class="form-group">
                                        <div class="col-sm-2 activation_col2" style="">
                                        </div>
                                        <div class="col-sm-3 " style="">
                                            <label style="" for="inputEmail3" class="control-label activation_label">Code d\'activation : </label>
                                        </div>
                                        <div class="col-sm-6" style="">
                                            <input name="code_activation" id="code_activation" maxlength="18" type="text"  class="form-control activation_code"  placeholder="" value="'.((isset($_SESSION["code_activation"]) && !empty($_SESSION["code_activation"])) ? $_SESSION["code_activation"] : "" ).'" style="">
                                        </div>
                                         
                                    </div>
						<div class="form-group">
						   <div class="col-sm-offset-3 col-sm-6" style="    margin-left: 0;">'; 
				if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
					?>
						<a href="#" onclick="ga('send', 'event','bouton', 'clic', 'Etape 1 - Bloc : Je saisis mon code d\'activation - VALIDER', 4);jQuery('#form_activation').submit();" class="btn btn-orange col-sm-offset-4"  style="margin-top:25px;float: right;">Valider >></a>
					<?php
				}else{
					?>
						<a href="#" onclick="jQuery('#form_activation').submit();" class="btn btn-orange col-sm-offset-4"  style="margin-top:25px;float: right;">Valider >></a>
					<?php
				}
				echo' </div>
						</div>
						<input type="hidden" name="action" value="step_1_1"/>
					</form>'; 
			
		 ?>
         
      </div>
      
      
   </div>
</div>
 
