<?php 

if(!isset($_SESSION['products'])){
    echo "<script>document.location.href='".$_CONST["URL2"]."/soutien-scolaire-en-ligne/';</script>";
    exit; 
}
// var_dump($_SESSION); 
// exit; 
//Vérification sur l'email 
$bAnnulation = false; 
    
    //var_dump($_SESSION["user"]); exit; 
    $strCommandeLast = $oDb->queryRow("SELECT max(commande_num) as commande_num FROM bor_commande");
    /*if($strCommandeLast['commande_num']!="" ) {
            $strCommandeLast = $strCommandeLast['commande_num'];
            if($_CONST['TYPE_ENVIRONNEMENT'] =='dev')
                $strCommandeNum = "BSSTST" . sprintf("%04d",((int)str_replace("BSSTST", "", $strCommandeLast)) + 1 ); 
            else
                $strCommandeNum = "BSS" . sprintf("%07d",((int)str_replace("BSS", "", $strCommandeLast)) + 1 );             
    }else {
            if($_CONST['TYPE_ENVIRONNEMENT'] =='dev')
                    $strCommandeLast = "BSSTST0099"; 
            else
                    $strCommandeLast = "BSS0000000";
            $strCommandeNum = "BSS" . sprintf("%07d",((int)str_replace("BSS", "", $strCommandeLast)) + 1 ); 
    }*/
	if($strCommandeLast['commande_num']!="" ) {
                    if(strtolower($_CONST['TYPE_ENVIRONNEMENT']) == 'dev'){
			$strCommandeLast = $strCommandeLast['commande_num'];
			$strCommandeNum = "BSSTST" . sprintf("%04d",((int)str_replace("BSSTST", "", $strCommandeLast)) + 1 ); 
                    }else if(strtolower($_CONST['TYPE_ENVIRONNEMENT']) == 'preprod'){
                        $strCommandeLast = $strCommandeLast['commande_num'];
			$strCommandeNum = "BSSPRP" . sprintf("%04d",((int)str_replace("BSSPRP", "", $strCommandeLast)) + 1 ); 
                    }else if(strtolower($_CONST['TYPE_ENVIRONNEMENT']) == 'prod'){
                        $strCommandeLast = $strCommandeLast['commande_num'];
			$strCommandeNum = "BSS" . sprintf("%07d",((int)str_replace("BSS", "", $strCommandeLast)) + 1 ); 
                    }
		}else {
                if($_CONST['TYPE_ENVIRONNEMENT'] =='dev')
                        $strCommandeLast = "BSSTST0099"; 
                else
                        $strCommandeLast = "BSS0000000";
                //$strCommandeNum = "BSS" . sprintf("%07d",((int)str_replace("BSS", "", $strCommandeLast)) + 1 ); 
        }

		
    $_SESSION['cart']['NUMCOMMANDE'] = $strCommandeNum;

    $oBill = new fluxBill();
    //Gestion code action partenaire 
    
    if(isset($_SESSION["partenaire_activation"]["bill_codeaction"]) && !empty($_SESSION["partenaire_activation"]["bill_codeaction"]))
        $strAction = $_SESSION["partenaire_activation"]["bill_codeaction"]; 
    else
        $strAction = $_SESSION["code_activation"]["CODE_ACTION"]; 
    //$strOrigine = $_SESSION["code_activation"]["ORIGINE"]; 
    $strOrigine = ""; 
    
    $oBill->Init(true, $strAction, $strOrigine);

    $oBill->SetClient();
    $oBill->SetShipping();
    $oBill->SetCoord();
    $oBill->SetProducts(true);
    $oBill->SetReglementGratuit();
    $oBill->Close();

    $aRetour = $oBill->SendBill(); 
		
    if(!$aRetour['erreur']){
            $oBill->sendFluxComemrcial($_SESSION['cart']['NUMCOMMANDE'], $_SESSION['user']['IDCLIENTWEB'], $_SESSION["products"]["enfants"][0]["ean"], $_SESSION['user']['EMAIL'],$_SESSION["code_activation"]["CODE_AVANTAGE"]);
            
            // Insertion dans la table commande
            $strSql = "INSERT INTO bor_commande (client_id,
                                        produit_ean, 
                                        commande_num, 
                                        commande_total, 
                                        commande_statut,
                                        commande_date, 
                                        commande_activation
                                        ) VALUES (
                                        '".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."',
                                        '".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["ean"])."',
                                        '".mysql_real_escape_string($_SESSION['cart']['NUMCOMMANDE'])."',
                                        '".mysql_real_escape_string($_SESSION['reglement']['TOTALCOMMANDE'])."',
                                        '1', 
                                        NOW(),
                                        '".mysql_real_escape_string($_SESSION["code_activation"]["CODE_AVANTAGE"])."'
                                        ) "; 
            $oDb->query($strSql); 
            $iCommandeId = $oDb->last_insert_id; 
            $_SESSION['cart']['IDCOMMANDE'] = $iCommandeId ; 
            //Insertion dans la table enfants
            foreach ( $_SESSION["products"]["enfants"] as $aEnfants){
                    $strSql = "INSERT INTO bor_commande_enfant ( commande_id, 
                                                    classe_id,
                                                    matiere_id,
                                                    formule_id,
                                                    de_id,
                                                    enfant_prenom,
                                                    enfant_ean, 
                                                    enfant_ean_reussite 
                                                   ) VALUES (
                                                    '".mysql_real_escape_string($iCommandeId)."',
                                                    '".mysql_real_escape_string($aEnfants['classe'])."',
                                                    '".mysql_real_escape_string($aEnfants['matiere'])."',
                                                    '".mysql_real_escape_string($_SESSION["cart"]["formule"]['formule_id'])."',
                                                    '".mysql_real_escape_string($aEnfants['duree_engagement'])."',
                                                    '".mysql_real_escape_string($aEnfants['D_PRENOM'])."',
                                                    '".mysql_real_escape_string($aEnfants['ean'])."',
                                                    '".mysql_real_escape_string($aEnfants['ean_reussite'])."'
                                                   )"; 
				$oDb->query($strSql); 														 
			}
			
			//Insertion dans la table facture
			if(isset($_SESSION['shipping']) && count($_SESSION['shipping'])>0){
				$strSql = "INSERT INTO bor_commande_facture ( commande_id, 
                                                                    pays_id, 
                                                                    facture_adr1, 
                                                                    facture_adr3, 
                                                                    facture_ville, 
                                                                    facture_cplivraison
                                                                   ) VALUES (
                                                                    '".mysql_real_escape_string($iCommandeId)."',
                                                                    '".mysql_real_escape_string($_SESSION['shipping']['pays_id'])."',
                                                                    '".mysql_real_escape_string($_SESSION['shipping']['ADR1'])."',
                                                                    '".mysql_real_escape_string($_SESSION['shipping']['ADR3'])."',
                                                                    '".mysql_real_escape_string($_SESSION['shipping']['VILLELIVRAISON'])."',
                                                                    '".mysql_real_escape_string($_SESSION['shipping']['CPLIVRAISON'])."'
                                                                   )";
				$oDb->query($strSql); 
			}
			
			$strSql = "UPDATE bor_client SET acces_zoe = 1 WHERE client_id ='".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."'"; 
			$oDb->query($strSql);
			//On effectu appel le ws abonnement pour les durees inférieur à 1 an (id duree engagement 1 et 2 )  
			
			// var_dump($a); exit; 
			$_SESSION['paiement']['statut'] = "OK"; 
			
			
		}else{
			$bVerif = false ; 
                        
			$_SESSION['paiement']['statut'] = "KO"; 
			$_SESSION['paiement']['code'] = $aRetour['code']; 
			
		}

?>
<div class="container">
 <div class="row">
      <ul id="steps" class="col-md-12">
		 <li class="detail_formule"><a class="active" ><strong>1.</strong>Saisie du code d'activation<span class="arrow_right"></span></a></li>
		 <li class="confirmation"><a  class="active" ><span class=" arrow_left "></span><strong>2.</strong>Détail de ma formule <span class="arrow_right"></span></a></li>
		 <li class="paiement"><a class="active" ><span class="arrow_left"></span><strong>3.</strong>Confirmation <span class="arrow_right"></span></a></li>
	</ul>
   </div>
    
 <div class="row">       
    <div class="col-sm-12" style="text-align: center;">         
        <h2 class="activation_formule">
            <?php
            $aDureeEngagement = $_SESSION['cart']['dureeEngagement'] ; 
                if($aDureeEngagement['de_engagement'] == 12)
                    $strDuree = "d'1 an";
                else 
                    $strDuree = "de ".$aDureeEngagement['de_engagement']." mois";
                
                echo date_format(date(), ' jS F Y');
                
                $oDb->query("SET lc_time_names = 'fr_FR'"); 
                $strDateDuJour = $oDb->queryItem("SELECT DATE_FORMAT(NOW(), '%d %M %Y')");
                $strDateFin = $oDb->queryItem("SELECT DATE_FORMAT('".$_SESSION['code_activation']["DATE"]."', '%d %M %Y')");
                //pour une durée $strDuree
                echo "Cet abonnement vous donne accès à l’ensemble des matières disponibles pour vos enfants.<br>
                        Il est valable à compter du $strDateDuJour et jusqu’au $strDateFin.";
            ?>
            
        </h2>
    </div>
   </div>
 <?php 
if( $bAnnulation){
?>
	<div class="confirmation">
	   <div class="title_formule">
		<h2>Vous avez déjà utilisé cette offre, vous ne pouvez plus y avoir droit.</h2>
		</div>
	</div>
		
<?php
}else{
?> 
	   
	<div class="confirmation">
	   <div class="title_formule">
			<?php 
				if($_SESSION['paiement']['statut'] == "OK"){
					echo '<h2>Votre abonnement a bien été enregistré.</h2>';
				}else{
					echo '<h2>Echec de la commande.</h2>';
				}
			?> 
	   </div>
	   <div class="body_formule">
			<?php
				$bAnalytics = false; 
				if($_SESSION['paiement']['statut'] == "OK"){
					$bAnalytics = true; 
					//Affichage de la liste des enfants 
					$strFormuleClasse = $_SESSION['cart']['formule']['formule_classe'];
					if($strFormuleClasse == "ciblee"){
						//Récupération du nom de la classe et la matière
						$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
						$strMatiere = $oDb->queryItem("SELECT matiere_titre FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['matiere'])."'");
						echo '<p class="detail_abonnement">Abonnement pour '.$_SESSION["products"]["enfants"][0]['D_PRENOM'].' en classe de '.$strClasse.' en '.$strMatiere.'.</p>'; 				
					}else if( $strFormuleClasse == "reussite"){
						//Récupération du nom de la classe et la matière
						$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
						echo '<p class="detail_abonnement">Abonnement pour '.$_SESSION["products"]["enfants"][0]['D_PRENOM'].' en classe de '.$strClasse.'.</p>'; 				
					}else if($strFormuleClasse == "tribu"){
						echo '<p class="detail_abonnement">'; 
						foreach($_SESSION["products"]["enfants"] as $aEnfant){
							$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($aEnfant['classe'])."'");
							$strMatiere = $oDb->queryItem("SELECT matiere_titre FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($aEnfant['matiere'])."'");
							echo 'Abonnement pour '.$aEnfant['D_PRENOM'].' en classe de '.$strClasse.'.<br/>'; 
						}
						echo '</p>';
					}
					
					//Récupération du prix en fonction du produit 
					$strSql = "SELECT * FROM bor_produit p INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix) WHERE p.produit_ean = '".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["ean"])."'"; 
					$aProduct = $oDb->queryRow($strSql);
					// var_dump($aProduct); 
					
					
					echo '<p>Votre abonnement porte le n°<strong>'.$_SESSION['cart']['NUMCOMMANDE'].'</strong>.</p>';
						
				?>
					
				  <p>Vous allez recevoir un <span class="blue_txt">email de confirmation de commande</span> avec vos codes d’accès, ceux de votre enfant et le lien 
					 pour vous connecter à la plateforme de soutien scolaire.
				  </p>
				  <p>Nous vous remercions de votre confiance.<br/>
					 Le service client reste à votre écoute pour toute question au<span class="blue_txt"> <?php echo $_CONST['RC_TEL']; ?></span> ou <a href="mailto:soutien-scolaire@bordas.tm.fr"><span class="blue_txt">par mail</span></a>. 
				  </p>
				<?php 
				}else{
					echo '<p>Suite à un problème technique, votre commande n’a pas pu aboutir. Votre abonnement n’est donc pas enregistré.</p>'; 
					echo '<p>Afin de finaliser votre commande, nous vous invitons à contacter notre service client par mail <a href="mailto:soutien-scolaire@bordas.tm.fr"><span class="blue_txt">soutien-scolaire@bordas.tm.fr</span></a> ou par téléphone au <span class="blue_txt">'.$_CONST['RC_TEL'].'</span> (du lundi au vendredi '.$_CONST['RC_HORAIRES'].').</p>'; 
					echo '<p>Nous vous remercions de votre confiance. </p>'; 
				}
				?>
	   </div>
	</div>
    
    
        <?php 
            if(!empty($_SESSION['partenaire_activation']["activation_plateforme"])){
        ?>
                <div class="confirmation">
                       <div class="title_formule">
                            <h2>J'accède à la plateforme de soutien scolaire</h2>
                       </div>
                        <div class="body_formule">
                            <a href="<?php echo $_SESSION['partenaire_activation']["activation_plateforme"] ; ?>" onclick="ga('send', 'event','bouton', 'clic', 'Etape 3 - Bloc : J\'accède à la plateforme de soutien scolaire - JE ME CONNECTE', 4);" class="btn btn-orange col-sm-offset-4"  style="margin-top:25px;">Je me connecte >></a>
                        </div>
                </div>
        <?php
            }
        ?>
<?php 
}
// var_dump( $bAnalytics); 
	// if($bAnalytics && (  $_CONST['TYPE_ENVIRONNEMENT'] !='dev' || true) ){
	if(false && $bAnalytics && (  $_CONST['TYPE_ENVIRONNEMENT'] !='dev' ) ){
	
		
	?>
		<!-- GA --> 
		<script>
		
			ga('require', 'ecommerce', 'ecommerce.js');
			
			ga('ecommerce:addTransaction', {
			  'id': '<?php echo $_SESSION['cart']['NUMCOMMANDE']; ?>',                     // Transaction ID. Required. 
			  'affiliation': '<?php echo $_SESSION["code_activation"]["CODE_ACTION"]; ?>',   // Affiliation or store name.
			  'revenue': '0',               // Grand Total.
			  'shipping': '0',                  // Shipping.
			  'tax': '10'                     // Tax.
			});
			
			ga('ecommerce:addItem', {
			  'id': '<?php echo $_SESSION['cart']['NUMCOMMANDE']; ?>',                     // Transaction ID. Required.
			  'name': 'Offre Découverte – Formule réussite Bordas Soutien scolaire',    // Product name. Required.
			  'sku': '<?php echo $aProduct['produit_ean']; ?>',                 // SKU/code.
			  'category': 'BOUQUET NUMERIQUE',         // Category or variation.
			  'price': '0',                 // Unit price.
			  'quantity': '1'                   // Quantity.
			});
						   
			ga('ecommerce:send');
			
			ga('ecommerce:clear');
		</script>
		<!-- /GA -->
		
		
		<!-- Facebook Conversion Code for BSS-achatFB -->
		<script>(function() {
		  var _fbq = window._fbq || (window._fbq = []);
		  if (!_fbq.loaded) {
			var fbds = document.createElement('script');
			fbds.async = true;
			fbds.src = '//connect.facebook.net/en_US/fbds.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(fbds, s);
			_fbq.loaded = true;
		  }
		})();
		window._fbq = window._fbq || [];
		window._fbq.push(['track', '6018688632045', {'value':'0','currency':'EUR'}]);
		</script>
		<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6018688632045&amp;cd[value]=0&amp;cd[currency]=EUR&amp;noscript=1" /></noscript>



	<?php 
   }
	unset($_SESSION['cart']); 
	unset($_SESSION['shipping']); 
	unset($_SESSION['reglement']); 
	unset($_SESSION['products']);
	unset($_SESSION['paiement']);
	unset($_SESSION['nb_enfant']);
	unset($_SESSION['partenaire']);
        unset($_SESSION["chx_cgu"]);
        unset($_SESSION["code_activation"]);
// var_dump($_SESSION['paiement']);
?>
 
