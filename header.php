	<?php
		//Gestion de la newsletter 
		ini_set("soap.wsdl_cache_enabled", 0); 
		header('Content-Type:text/html; charset=utf-8');
		$bNewsletter = false; 
		$bPost = false; 
		if(isset($_POST['action']) && $_POST['action'] =="newsletter"){
			$strMessage = ''; 
			$bPost = true;
			$bOpt1 = true; 
			$bOpt2 = false; 
			if(isset($_POST['email']) && !empty($_POST['email'])){
				$strEmail = $_POST['email'] ;
				if(is_valid_email($strEmail)){
					try{
						$soap_options = array ( 'login' => 'WSProspect', 'password' => 'ProspectManagementPER', 'exception' => true); 
						$client = new SoapClient("http://ws.edupole.net/prospectManagement/WSProspect.wsdl", $soap_options); 
						$params = array(
									'codeSite' => 'BRD_SOUTIEN_SCO',
									'email' => $strEmail, 
									'origin' => 'HEADER_BSS_NL',
									'optinNewsletter' => $bOpt1,
									'optinPartner' => $bOpt2
									);
									
						$retour_ws = $client->createProspect($params); 
						if(isset($retour_ws)){
							if($retour_ws->creationStatus){
								$strMessage = "Votre adresse email a bien été enregistrée."; 
								$bNewsletter = true; 
							}else
								$strMessage = "Attention !<br>Cet email a déjà été utilisé, vous ne pouvez l'utiliser qu'une seule fois.";
						// 
						}else
							$strMessage = "Une erreur est survenue lors de votre enregistrement."; 
					}catch (Exception $e){
						$strMessage = "Une erreur est survenue lors de votre enregistrement.";
					}
				}else
					$strMessage = "Veuillez renseigner une adresse email valide.";
			}else{
				$strMessage = "Veuillez renseigner votre email.";
			}
		}
	?>
<!--	<header class="container">-->
	<header class="bss-header ">

				<?php 
			if(isset($_POST['action']) && $_POST['action'] == 'logout'){
				unset($_SESSION['user']);
			}
			
			if(isset($_SESSION['user']['IDCLIENT']) && !empty($_SESSION['user']['IDCLIENT'])){
				if(isset($_SESSION["connexion"]) && !empty($_SESSION['connexion']))
					unset($_SESSION["connexion"]);
				$strLienMonCompte = $oDb->queryItem("SELECT templates_name FROM eco_templates WHERE templates_id = 30"); 
				$strLienMonAbo = 	$oDb->queryItem("SELECT templates_name FROM eco_templates WHERE templates_id = 31"); 
				//var_dump(StrToUrl($strLienMonCompte));
			/*	echo 	'	<div class="dropdown account_block_head" id="account_block_connected">
								<a  href="#" class="connect">
									<span class="account_iocn_left"></span>
									<span class="name_customer">'.$_SESSION['user']['PRENOMCLIENT'].' '.$_SESSION['user']['NOMCLIENT'].'</span>
									<span class="moncompte_button" id="moncompte_connecte">Mon compte</span>
									<span class="account_iocn_right"></span> 
								</a>
								<ul class="dropdown-menu myaccount" role="menu" aria-labelledby="dLabel" id="mon_compte">
									<li><a href="'. ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ). $_CONST['URL_ACCUEIL'] . strToUrl($strLienMonAbo) .'.html">Gérer mon abonnement</a></li>
									<li><a href="'. $_CONST['URL2'] . $_CONST['URL_ACCUEIL'] . strToUrl($strLienMonCompte) .'.html">Gérer mon compte</a></li>
									<li><a href="http://bordas-soutien-scolaire.eduplateforme.com" target="_blank">Accéder à la plateforme de soutien scolaire</a></li>
									<li class="deconnexion_btn"><a href="#" onclick="jQuery(\'#logout_form\').submit();">Se déconnecter </a></li>
								</ul>
							</div>
							<form method="post" id="logout_form" action="'. $_CONST['URL2'] . $_CONST['URL_ACCUEIL'] .'">
							<input type="hidden" name="action" value="logout"/>
							</form>
						';*/
				?>
<div id="totop"></div>
<!-- BLOC UTILISATEUR SUPLEMENTAIRE SI IDENTIFIE-->
<div class="container-fluid top-member">
<div class="container top-member">
  <div class="row">
    <div class="col-md-12 text-right">
      <ul class="list-inline">
        <li><a href="<?php echo ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ) . $_CONST['URL_ACCUEIL'] . strToUrl($strLienMonCompte); ?>.html">Mon profil</a></li>
        <li>|</li>
        <li><a href="<?php echo ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ). $_CONST['URL_ACCUEIL'] . strToUrl($strLienMonAbo); ?>.html">Mon abonnement</a></li>
      </ul>
    </div>
  </div>
</div>  
</div>
<!--HEADER FIXE SUR DESKTOP ET ECRAN LARGE - HEADER ABSOLUE SUR MOBILE ET PHABLETTE -->
<div class="container top-header fixed-top">
  <div class="row">
    <div class="col-xs-12 col-sm-4 text-center hidden-xs"> <a href="<?php echo $_CONST['URL2'];?>" class="bss-logo"> <img src="/img/logo.png" class="img-responsive"></a> </div>
    <div class="col-xs-12 col-sm-4 no-padding text-center hidden-xs"><a href="/" class="bss-slogan ">La réussite à portée de clic !</a></div>
    
    <div class="col-xs-12 col-sm-4 no-padding main-bloc-bss-connexion text-right">
      <div class="logo-mobile visible-xs-block"><a href="/" class="bss-logo"> <img src="/img/logo.png" class="img-responsive"></a></div>
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      </div>  
      <div class="bloc-bss-connexion">  <div class="account-name hidden-xs"><?php echo $_SESSION['user']['PRENOMCLIENT']; ?> <span><?php echo $_SESSION['user']['NOMCLIENT']; ?></span></div><a class="btn btn-user login collapsed" href="#"><i class="icon-user"></i><!--<i class="mini-icon icon-cancel"></i>--></a>
<!--										<a class="btn btn-connexion login  hidden-xs hidden-sm hidden-md collapsed" href="index" >Déconnexion</a>-->
										<a class="btn btn-connexion login  hidden-xs hidden-sm hidden-md collapsed" href="#" onclick="jQuery('#logout_form').submit();">Déconnexion</a>
												<form method="post" id="logout_form" action="<?php echo $_CONST['URL2'] . $_CONST['URL_ACCUEIL']; ?>">
												<input type="hidden" name="action" value="logout"/>
												</form>
						</div>
    </div>
  </div>
</div>
<?php
			}else{
				if(isset($_SESSION["connexion"]) && !empty($_SESSION['connexion'])){
					if($_SESSION["connexion"] == "erreur_connexion"){
						$bError = true;
						$strError = "Vos identifiant et mot de passe sont incorrects !";
					}else if($_SESSION["connexion"] == "erreur_champ"){
						$bError = true;
						$strError = "Veuillez renseigner un identifiant et un mot de passe.";
					}else if($_SESSION["connexion"] == "erreur_profil"){
						$bError = true;
						$strError = "Vous devez posséder un compte Parent pour utiliser ce site. Si vous n’en avez pas, inscrivez-vous directement sur le site Bordas soutien scolaire.";
					}
					unset($_SESSION["connexion"]); 
				}
				if(isset($_SESSION["connexion_identification"]) && !empty($_SESSION['connexion_identification'])){
					if($_SESSION["connexion_identification"] == "erreur_connexion"){
						$bError = true;
						$strError = "Vos identifiant et mot de passe sont incorrects !";
					}else if($_SESSION["connexion_identification"] == "erreur_champ"){
						$bError = true;
						$strError = "Veuillez renseigner un identifiant et un mot de passe.";
					}else if($_SESSION["connexion_identification"] == "erreur_profil"){
						$bError = true;
						$strError = "Vous devez posséder un compte Parent pour utiliser ce site. Si vous n’en avez pas, inscrivez-vous directement sur le site Bordas soutien scolaire.";
					}
					unset($_SESSION["connexion_identification"]); 
				}
			?>


		
<!--HEADER FIXE SUR DESKTOP ET ECRAN LARGE - HEADER ABSOLUE SUR MOBILE ET PHABLETTE -->
<div class="container top-header fixed-top">
  <div class="row">
    <div class="col-xs-12  col-sm-4 text-center hidden-xs"> <a href="/" class="bss-logo"> <img src="/img/logo.png" class="img-responsive"></a> </div>
    <div class="col-xs-12  col-sm-4 no-padding text-center hidden-xs"><a href="/" class="bss-slogan ">La réussite à portée de clic !</a></div>
    <div class="col-xs-12 col-sm-4 no-padding main-bloc-bss-connexion text-right">
      <div class="logo-mobile visible-xs-block"><a href="/" class="bss-logo"> <img src="/img/logo.png" class="img-responsive"></a></div>
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      </div>
						
		
						
						
						
      <div class="bloc-bss-connexion"> <a class="btn btn-user login collapsed" role="button" data-toggle="collapse" href="#collapseConnexion" aria-expanded="false" aria-controls="collapseConnexion"><i class="icon-user"></i><!--<i class="mini-icon icon-ok"></i>--></a> <a class="btn btn-connexion login  hidden-xs collapsed" role="button" data-toggle="collapse" href="#collapseConnexion" aria-expanded="false" aria-controls="collapseConnexion"><i class="icon-key"></i> Connexion</a> </div>
    </div>
  </div>
</div>



<!--COLLAPSE CONTENANT LES ELEMENTS DE CONNEXION-->
<div class="collapse bss-connexion <?php if(isset($bError) && $bError) echo 'in'; ?>" id="collapseConnexion">
  <div class="container">
    <div class="row">
      <div class="col-md-5 col-md-offset-1">
        <div class="bss-connexion">
		<?php
		//var_dump($_SESSION["connexion"]);
		if(isset($bError) && $bError){
										echo '<div class="erreur">
											<label style="color: red;">
												'.$strError.'
											</label>											
										</div>'; 
									}
		?>
          <form action="/proxy/proxy_connexion.php" method="post" title="Connexion" id="connexion-form">
<!--												<input type="hidden" name="source" value="<?php echo $strUrlPartenaire;?>" />-->
												<input type="hidden" name="action" value="submit">
												<input type="hidden" name="page" value="identification">
												<!--<input type="hidden" name="redirect_after" value="/soutien-scolaire-en-ligne/identification.html">-->
												<input type="hidden" name="redirect_after" value="<?php echo $_SERVER['REQUEST_URI'];?>">
            <p class="form-titre">Je gère mon compte</p>
            <p class="form-stitre">(abonnement(s), données personnelles)</p>
            <div class="row">
              <div class="form-group col-sm-6">
                <label for="connexion-identifiant">Identifiant</label>
                <input name="username" type="text" class="form-control" id="connexion-identifiant" placeholder="Identifiant" title="Identifiant">
              </div>
              <div class="form-group col-sm-6">
                <label for="connexion-mdp">Mot de passe</label>
                <input name="password" type="password" class="form-control" id="connexion-mdp" placeholder="Mot de passe" title="Mot de passe">
              </div>
            </div>
<!--            <p class="form-texte">
              <button type="submit" class="btn btn-primary btn-fw">Je me connecte à mon compte<i class="icon-angle-right"></i></button>
            </p>
            <p class="form-texte"><a href="mot-de-passe-oublie"  class="">Mot de passe oublié ?</a></p>-->
												<?php 
												if($_CONST['TYPE_ENVIRONNEMENT'] !='dev' ){
												?>
												<p class="form-texte"><button type="submit" onclick="ga('send', 'event', 'bouton', 'clic', 'Popin - Connexion : Je gère mon compte',  1);jQuery('#connexion-form').submit();" data-toggle="tab" class="btn btn-primary btn-fw" >Je me connecte à mon compte<i class="icon-angle-right"></i></button></p>
																<p class="form-texte"><a  data-toggle="modal" data-target="#motdepasseoublie" >Mot de passe oublié ?</a></p>
												<?php 
													}else{
												?>
																<p class="form-texte"><button type="submit" onclick="$('#connexion-form').submit();" data-toggle="tab" class="btn btn-primary btn-fw">Je me connecte à mon compte<i class="icon-angle-right"></i></button></p>
														<p class="form-texte"><a href="#"  data-toggle="modal" data-target="#motdepasseoublie" onclick="$('#modal_moncompte').modal('toggle');" >Mot de passe oublié ?</a></p>
												<?php } ?>
          </form>
        </div>
      </div>
      <div class="col-md-5 ">
        <div class="bss-plateforme">
          <p class="form-titre">J'accède à la plateforme <br>
            de Soutien scolaire</p>
          <p class="form-texte"><a href="http://bordas-soutien-scolaire.eduplateforme.com" target="_blank"  class="btn btn-primary btn-fw">Je me connecte à la plateforme<i class="icon-angle-right"></i></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
}
		?>
<nav class="navbar navbar-default" role="navigation" style="display:none;">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand visible-xs" href="#">Menu</a>
		</div>
	</div><!-- /.container-fluid -->
</nav>
<!-- MENU PRINCIPAL -->
<?php
if(	$_GET["templates_id"] != '50' &&
				$_GET["templates_id"] != '51' &&
				$_GET["templates_id"] != '52' &&
				$_GET["templates_id"] != '53'
								){
				include('menu.php');
								}
?>
</header>

<div class="modal fade" id="motdepasseoublie" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">				
	<div class="modal-dialog modal-lg">
		<div class="modal-content modal-content-newslettre">
<!--			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
			<!--Alert success-->
			<?php 
			$bPostForget = false;
			$bForget = false;
			if( isset($_SESSION['mdp_forget']) && !empty($_SESSION['mdp_forget'])){
				$bForget =true ;
				if($_SESSION['mdp_forget'] == "ok" ){
					$bPostForget = true;
					$strMessage = "Un e-mail vient de vous être envoyé. Veuillez suivre les instructions qui s'y trouvent.";
				}else if($_SESSION['mdp_forget'] == "erreur_update" ){
					$strMessageBox = "Cette adresse email n'est pas reconnue sur ce site. Si vous avez changé d'adresse email, merci de nous contacter." ; 
				}else if($_SESSION['mdp_forget'] == "erreur_champ" ){
					$strMessageBox = "Veuillez renseigner votre email." ;  
				}else if($_SESSION['mdp_forget'] == "erreur_technique" ){
					$strMessageBox = "Un problème technique empêche la récupération du mot de passe." ; 
				}else if($_SESSION['mdp_forget'] == "champ_incorrect" ){
					$strMessageBox = "Veuillez renseigner une adresse email valide." ; 
				}
				unset( $_SESSION['mdp_forget'] ) ;
				
			}
			
				if($bPostForget){
					echo '<div class="" id="news-warning">
							<div class="hero-unit text-center green">
								'.$strMessage.'
							</div>              
						</div>';
				}
			
			if(!$bPostForget){
			?>
			
			
			
			
				<form action="/proxy/proxy_mdpForget.php"  method="post" id="forgot-form">	
<!--					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Vous avez oublié votre mot de passe ?</h4>
						<?php
						if(isset($strMessageBox) && !empty($strMessageBox)){
							echo '<div class="" id="news-warning">
								<div class="hero-unit text-center red">
									'.$strMessageBox.'
								</div>              
							</div>';
						}
					?>
					
					
					</div>-->
								
					<div class="modal-body row">
									<!-- Modal Express Page-->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p><strong> Vous avez oublié votre mot de passe ?</p></strong>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
														<p>Pour retrouver l'identifiant et le mot de passe que vous avez créés lors de votre inscription sur le site des Editions Bordas ou de Bordas Soutien scolaire, merci de nous 
indiquer l'adresse e-mail saisie lors de cette inscription. Veuillez suivre les instructions qui figurent dans l'e-mail que vous allez recevoir d'ici quelques minutes.</p>
																<label>Entrez l’adresse e-mail associée à votre inscription :</label>
							<input type="email" id="txtEmail" class="form-control" name="email_forgot" value="" required>
										<div class="modal-footer">
						<button type="submit" class="btn btn-orange">Envoyer</button>
					</div>
										
										
										</div>
        </div>
      </div>
					</div>
				</form>	 
			<?php
			}
			?>
		</div>
	</div>
</div>
<?php 
	if($bForget){
?>
	<script>
		$('document').ready(function(){
			$('.mot_passe_oublie_link').click();
		});
	</script>
<?php
	}
?>