<header class="container">
<?php
 $strSql = "SELECT files_path FROM  eco_files WHERE files_field_name = 'partenaire_image' AND files_table_id= '".$_SESSION['partenaire_activation']["activation_id"]."'";
$strPathImage = $oDb->queryItem($strSql);
?>
    <div id="logo_bordas_activation" class="<?php if($strPathImage) echo "logo_left"; else echo "logo_center";?>">
		 <?php 
			$strSql = "SELECT files_path FROM  eco_files WHERE files_field_name = 'partenaire_image_bordas' AND files_table_id= '".$_SESSION['partenaire_activation']["activation_id"]."'";
			$strPathImage2 = $oDb->queryItem($strSql);
			if($strPathImage2)
				echo '<img id="logo_partenaire_bordas_activation"  class="img-responsive" src="'.str_replace('../','../../',$strPathImage2).'" alt="Bordas Soutien scolaire">';
			else
				echo '<img class="img-responsive" src="/images/logo.jpg" alt="Bordas Soutien scolaire">';
		?>
        <div class="slogan">L'entraînement  en ligne du CP à la Terminale</div>
    </div>
    <?php 
       
        if($strPathImage)
            echo '<img id="logo_partenaire_activation"  class="img-responsive" src="'.str_replace('../','../../',$strPathImage).'" alt="">'; 
    ?>
</header>

<div class="modal fade" id="motdepasseoublie" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">				
	<div class="modal-dialog modal-lg">
		<div class="modal-content modal-content-newslettre">
			<!--Alert success-->
			<?php 
			$bPostForget = false;
			$bForget = false;
			if( isset($_SESSION['mdp_forget']) && !empty($_SESSION['mdp_forget'])){
				$bForget =true ;
				if($_SESSION['mdp_forget'] == "ok" ){
					$bPostForget = true;
					$strMessage = "Un e-mail vient de vous être envoyé. Veuillez suivre les instructions qui s'y trouvent.";
				}else if($_SESSION['mdp_forget'] == "erreur_update" ){
					$strMessageBox = "Cette adresse email n'est pas reconnue sur ce site. Si vous avez changé d'adresse email, merci de nous contacter." ; 
				}else if($_SESSION['mdp_forget'] == "erreur_champ" ){
					$strMessageBox = "Veuillez renseigner votre email." ;  
				}else if($_SESSION['mdp_forget'] == "erreur_technique" ){
					$strMessageBox = "Un problème technique empêche la récupération du mot de passe." ; 
				}else if($_SESSION['mdp_forget'] == "champ_incorrect" ){
					$strMessageBox = "Veuillez renseigner une adresse email valide." ; 
				}
				unset( $_SESSION['mdp_forget'] ) ;
				
			}
			
				if($bPostForget){
					echo '<div class="" id="news-warning">
							<div class="hero-unit text-center green">
								'.$strMessage.'
							</div>              
						</div>';
				}
			
			if(!$bPostForget){
			?>
			
			
			
			
				<form action="/proxy/proxy_mdpForget.php"  method="post" id="forgot-form">	
<!--					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Vous avez oublié votre mot de passe ?</h4>
						<?php
						if(isset($strMessageBox) && !empty($strMessageBox)){
							echo '<div class="" id="news-warning">
								<div class="hero-unit text-center red">
									'.$strMessageBox.'
								</div>              
							</div>';
						}
					?>
					
					
					</div>-->
								
					<div class="modal-body row">
									<!-- Modal Express Page-->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Vous avez oublié votre mot de passe ?</h4>
      </div>
      <div class="modal-body">
        <div class="row">
			<div class="col-md-12">
				<p>Pour retrouver l'identifiant et le mot de passe que vous avez créés lors de votre inscription sur le site des Editions Bordas ou de Bordas Soutien scolaire, merci de nous 
				indiquer l'adresse e-mail saisie lors de cette inscription. Veuillez suivre les instructions qui figurent dans l'e-mail que vous allez recevoir d'ici quelques minutes.</p>
				<label>Entrez l’adresse e-mail associée à votre inscription :</label>
				<input type="email" id="txtEmail" class="form-control" name="email_forgot" value="" required />
				<div class="modal-footer">
					<input type="hidden" name="redirect_after" value="<?php echo $_SERVER['REQUEST_URI'];?>">
					<button type="submit" class="btn btn-orange">Envoyer</button>
				</div>
			</div>
        </div>
      </div>
					</div>
				</form>	 
			<?php
			}
			?>
		</div>
	</div>
</div>
<?php 
	if($bForget){
?>
	<script>
		$('document').ready(function(){
			$('.mot_passe_oublie_link').click();
		});
	</script>
<?php
	}
?>
