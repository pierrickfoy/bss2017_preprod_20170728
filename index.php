<?php
// Test Hedi
// error_reporting(E_ALL);
// ini_set('error_reporting', E_ALL);
 // ini_set('display_errors', '1');
	session_start();

	define( 'ROOT', dirname( __FILE__ ));
	$_CONST['path']["server"] = ROOT;
	
	include_once(ROOT."/lib/config.inc.php");
	include_once(ROOT."/lib/database.class.php");
	include_once(ROOT."/lib/JoForm.class.php");
	include_once(ROOT."/lib/functions.php");
	include_once(ROOT."/lib/intyMailer.php");
	include_once(ROOT."/lib/abo.class.php");
	include_once(ROOT."/lib/cb.class.php");
	$isMobile=(bool)preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	
	$oDb->query("SET NAMES utf8"); 
	LoadConfig($_CONST); //load eco_config table IN $_CONST
	
	$_SESSION["langs_id"] = 1 ;
	/* ************************** */
	global $aLang, $_CONST ; 
		
	
	
	/* Gestion des templates dynamiques */

		if(isset($_GET["templates_id"]) && $_GET["templates_id"] == 9){
			//Template rubrique => On doit detecter l'id de la rubrique à partir du titre de l'url 
			$strSql="SELECT rubrique_titre , rubrique_id FROM bor_rubrique WHERE rubrique_actif = 1"; 
			$aRubriques = $oDb->queryTab($strSql); 
			foreach ($aRubriques as $aRubrique){
				if($_GET['rubrique'] == strToUrl($aRubrique['rubrique_titre'])){
					$_GET['id_rubrique'] = $aRubrique['rubrique_id'] ; 
					break;
				}
			}
		}
		
		if(isset($_GET["template"]) && !empty($_GET["template"])){
			$strSql="SELECT templates_name , templates_id FROM eco_templates ";
			$aTemplates = $oDb->queryTab($strSql); 
			foreach ($aTemplates as $aTemplate){
				if($_GET['template'] == strToUrl($aTemplate['templates_name'])){
					$_GET['templates_id'] = $aTemplate['templates_id'] ; 
					break;
				}
			}
		}
		
// $strCodeActionBill = $oDb->queryItem("SELECT activation_id	FROM  bor_activation WHERE bill_codeaction ='BSSLDE'");
// $strDateFinBo = $oDb->queryItem("SELECT date_fin FROM  bor_formule_associee WHERE ean13 ='3133097365350' AND activation_id = '".$strCodeActionBill."' AND tpl = 'activation' LIMIT 1");
// var_dump($strDateFinBo);exit;
?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php get_meta_title_desc(); ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png" href="/favicon.png" />
<link rel="shortcut icon" href="/favicon.ico" />
<?php include('scripts.php');  ?>
<?php include('css.php'); ?>

<?php
   /* if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
?>
    <!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','//connect.facebook.net/en_US/fbevents.js');

	fbq('init', '796542543725095');
	fbq('track', 'PageView');
	fbq('track', 'AddToCart');

	</script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=796542543725095&ev=PageView&noscript=1" /></noscript>
	<!-- End Facebook Pixel Code -->
<?php
    }*/
/* Pagination des actualités */
//var_dump($_GET["templates_id"]);
if($_GET["templates_id"] == 9)
{
	if(isset($_GET["id_rubrique"]) && $_GET["id_rubrique"] == 0)
	{
		if(!isset($_GET["page"])){
			echo '<link rel="next" href="'.$_CONST["URL2"].'/actualite/page=2">';
		}
		else if($_GET["page"] == 2){
			echo '<link rel="prev" href="'.$_CONST["URL2"].'/actualite/">';
			echo '<link rel="next" href="'.$_CONST["URL2"].'/actualite/page='.($_GET["page"]+1).'">';
		}
		else
		{
			echo '<link rel="prev" href="'.$_CONST["URL2"].'/actualite/page='.($_GET["page"]-1).'">';
			echo '<link rel="next" href="'.$_CONST["URL2"].'/actualite/page='.($_GET["page"]+1).'">';
		}
	}else if(isset($_GET["id_rubrique"]))
	{
		$strSql = "SELECT rubrique_titre FROM bor_rubrique WHERE rubrique_id = '".$_GET["id_rubrique"]. "'"; 
		$aRubrique = $oDb->queryItem($strSql);
		
		$iTitre_rubrique = strToUrl($aRubrique);
		//var_dump($iTitre_rubrique);

		if(!isset($_GET["page"])){
			echo '<link rel="next" href="'.$_CONST["URL2"].'/actualite/'.$iTitre_rubrique.'/page=2">';
		}
		else if($_GET["page"] == 2){
			echo '<link rel="prev" href="'.$_CONST["URL2"].'/actualite/'.$iTitre_rubrique.'/">';
			echo '<link rel="next" href="'.$_CONST["URL2"].'/actualite/'.$iTitre_rubrique.'/page='.($_GET["page"]+1).'">';
		}
		else
		{
			echo '<link rel="prev" href="'.$_CONST["URL2"].'/actualite/'.$iTitre_rubrique.'/page='.($_GET["page"]-1).'">';
			echo '<link rel="next" href="'.$_CONST["URL2"].'/actualite/'.$iTitre_rubrique.'/page='.($_GET["page"]+1).'">';
		}
	}
}	
	
?>
<script type="text/javascript" src="//try.abtasty.com/3ce355e11137258fd6e6eb2b5905c879.js"></script>
</head>

<?php 

	/* Traitement des partenaire */
	if(isset($_POST["source"]) && !empty($_POST["source"])){
		$strUrlPartenaire = mysql_real_escape_string($_POST["source"]) ; 
		//Récupération des infos du partenaire
		$strSql = "SELECT * FROM bor_partenaire WHERE partenaire_url = '".$strUrlPartenaire. "' LIMIT 1"; 
		$aPartenaire = $oDb->queryRow($strSql);
		if($oDb->rows > 0 ){
			$_SESSION['partenaire'] = $aPartenaire ; 
		}
	}
if(isset($_POST['action']) && $_POST['action'] == 'logout'){
				unset($_SESSION['user']);
			}
	
// on nomme le body en fonction du template et login/logout
	$classe_body = '';
if(	$_GET["templates_id"] == '50' ||
				$_GET["templates_id"] == '51' ||
				$_GET["templates_id"] == '52' ||
				$_GET["templates_id"] == '53'){
				$classe_body .= ' tunnel';
				}
else if($_GET["templates_id"] == '31'){
				$classe_body .= ' gestion-abonnement';
}
if(isset($_SESSION['user'])){
				$classe_body .= ' logged-in';
}
$t = get_template_filename();
if($t == 'accueil'){
				$class= 'accueil';
				$classe_body .= ' accueil';
}
else
				$class= $t; ?>
<body id="<?php echo $class; ?>" class="<?php echo $classe_body; ?>" >

<!-- Tag public idées -->
<?php
//var_dump($_SESSION['cart']);
//var_dump($t);
if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
	// TAG HOME
	if($t == "accueil"){
?>
	<script type="text/javascript">
	var tip = tip || [];
	tip.push(['_setSegment', '4212', '5837', 'Home']);
	(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src=(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidees.com/p/tip/";
	var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window)
	</script>
<?php
	}
	// TAG CATEGORIE 1
	//else if ($t == "rubrique" || $t == "classes" || $t == "matieres" || $t == "lequipe" || $t == "abonnement"){
	else if ($t == "type_abo" || $t == "comment-s-abonner" || $t == "abonnement" || $t == "classe" || $t == "matiere"){
?>
	<script type="text/javascript"> 
	var tip = tip || []; 
	tip.push(['_setSegment', '4212', '5838', 
	/* To change >> start */ 
	'<?php echo $_GET['templates_id']; ?>'           // Id category - required 
	/* To change << end */ 
	]); 
	(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src=(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidees.com/p/tip/";
	var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window)
	</script> 
<?php
	}
	// TAG CATEGORIE 2
	else if ($t == "classe_matiere" || $t == "chapitre" || $t == "notion"){
?>
	<script type="text/javascript"> 
	var tip = tip || []; 
	tip.push(['_setSegment', '4212', '5839', 
	/* To change >> start */ 
	'<?php echo $_GET['templates_id']; ?>'           // Id category - required 
	/* To change << end */ 
	]); 
	(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src=(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidees.com/p/tip/";
	var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window)
	</script>
<?php
	}
	// TAG PRODUCT
	// Une partie du tag product se trouve ici : quand aucun ean n'est encore récupéré de la fiche produit
	// une autre sur section-fiche-produit.php : quand l'internaute clique sur je m'abonne, le tag est envoyé avec l'ean
	else if ($t == "fiche_produit"){
?>
	<script type="text/javascript"> 
	var tip = tip || []; 
	tip.push(['_setSegment', '4212', '5841', 'Product']); 
	(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src=(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidees.com/p/tip/";
	var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window)
	</script>
<?php
	}
	// TAG OTHER partout où il n'y a pas d'autre tag
	else{
		if($t != "etape-tunnel" && $t != "etape_tunnel_2" && $t != "etape_tunnel_3" && $t != "etape_tunnel_4")
?>
	<script type="text/javascript"> 
		var tip = tip || []; 
		tip.push(['_setSegment', '4212', '5844', 'Other']); 
		(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src=( document.location.protocol=="https:"?"https":"http")+"://tracking.publicidees.com/p/tip/"; 
		var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window)
	</script> 
<?php
	}
}
?>
<!-- /Tag public idées -->

<?php 
	if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
		$strCodeUA = $_CONST['TAG_ANALYTICS'] ;
		if(isset($_SESSION['partenaire']['tag_analytics']) && !empty($_SESSION['partenaire']['tag_analytics'])){
			$strCodeUA = $_SESSION['partenaire']['tag_analytics'] ; 
			?>
				<script>
					  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					  ga('create', '<?php echo $strCodeUA; ?>', 'auto', {'allowLinker' : true});
					  ga('require', 'linker');
                      ga('require', 'displayfeatures');
					  ga('send', 'pageview');


					  function decorateElements(element) {
						  ga('linker:decorate', element);
					  }
			<?php
		}
		else{
			?>
				<script>
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					ga('create', '<?php echo $strCodeUA; ?>', 'soutien.editions-bordas.fr');
                    ga('require', 'displayfeatures');
					ga('send', 'pageview');
			<?php
		}

			if(  isset($_SESSION['user']['IDCLIENT']) && $_SESSION['user']['IDCLIENT'] > 0 ){
?>
			var NumWeb = '<?php echo $_SESSION['user']['IDCLIENTWEB']; ?>';
			ga('set', 'dimension1', NumWeb);
			var NumCli = '<?php echo $_SESSION['user']['NUMCLIENT']; ?>';
			ga('set', 'dimension2', NumCli);
<?php 
			}
?>
		</script>
		<!-- /GA -->
<?php 
	}
	
	
	include('header.php'); ?>
<!-- CONTAINER FULL WIDTH ============================================== -->
		<!-- Contenue -->
<!--		<section id="home" class="content">-->
	<div class="main animsition">
<?php
//var_dump($_GET["templates_id"]);
if(	$_GET["templates_id"] == '50' ||
				$_GET["templates_id"] == '51' ||
				$_GET["templates_id"] == '52' ||
				$_GET["templates_id"] == '53'
								){
?>
				<!-- BREADCRUMB -->
  <div class="bss-breadcrumb" >
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h6>
            <ol class="breadcrumb">
              <li id="a" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="/index.php" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
              <li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child"> <a itemprop="url"> <span itemprop="title">Commande</span> </a></li>
            </ol>
          </h6>
        </div>
      </div>
    </div>
  </div>
  <!-- STEP COMMANDE -->
  <div class="bss-section bloc-section-blanc bss-step" >
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-3">
          <p class="text-center step-bloc <?php if(	$_GET["templates_id"] == '50') echo ' actif'; ?>"><span>1</span> Commande</p>
        </div>
        <div class="col-xs-12 col-sm-3">
          <p class="text-center step-bloc <?php if(	$_GET["templates_id"] == '51') echo ' actif'; ?>"><span>2</span> Informations personnelles</p>
        </div>
        <div class="col-xs-12 col-sm-3">
          <p class="text-center step-bloc <?php if(	$_GET["templates_id"] == '52') echo ' actif'; ?>"><span>3</span> Paiement</p>
        </div>
        <div class="col-xs-12 col-sm-3">
          <p class="text-center step-bloc <?php if(	$_GET["templates_id"] == '53') echo ' actif'; ?>"><span>4</span> Confirmation</p>
        </div>
      </div>
    </div>
  </div>
<?php
}
get_page();
	if(
		$t == "landing_page" ||
		$t == "type_abo" ||
		$t == "comment-s-abonner" ||
		$t == "rubrique" ||
		$t == "articles"
	){ include('templates/section-newsletter.php'); }
?>
		</div>
<!--		</section>-->
<!-- /.CONTAINER FULL WIDTH ============================================== --> 


<?php include('footer.php'); ?>
<?php
/* Implémentation plan de taggage - EFFILIATION */
if(true || strtolower($_CONST['TYPE_ENVIRONNEMENT']) != 'dev'){
	// page d'accueil
	if((isset($_GET["templates_id"]) && $_GET["templates_id"] == 1) || (!isset($_GET["templates_id"]) || empty($_GET["templates_id"])) )
		echo '<script src="http://mastertag.effiliation.com/mt660015994.js?page=home" async="async" ></script>';
	// pages génériques
	if(isset($_GET["templates_id"]) && ($_GET["templates_id"] == 47 || $_GET["templates_id"] == 48 || $_GET["templates_id"] == 18 || $_GET["templates_id"] == 19 || $_GET["templates_id"] == 20 || $_GET["templates_id"] == 23 || $_GET["templates_id"] == 11 || $_GET["templates_id"] == 9 || $_GET["templates_id"] == 4) )
		echo '<script src="http://mastertag.effiliation.com/mt660015994.js?page=generic" async="async"></script>';
	// pages catégories/rubriques
	/*if(isset($_GET["templates_id"]) && $_GET["templates_id"] == 9){
		// rubriques
		if($aRubrique['rubrique_id'] == 1)
			echo '<script src="http://mastertag.effiliation.com/mt660015994.js?page=category&idcat='.$aRubrique['rubrique_id'].'&wordingcat=CONSEIL" async="async" ></script>';
		if($aRubrique['rubrique_id'] == 2)
			echo '<script src="http://mastertag.effiliation.com/mt660015994.js?page=category&idcat='.$aRubrique['rubrique_id'].'&wordingcat=ACTUALITE" async="async" ></script>';
	}*/
	// pages catégories
	if(isset($_GET["templates_id"]) && ($_GET["templates_id"] == 12 || $_GET["templates_id"] == 16 || $_GET["templates_id"] == 15 || $_GET["templates_id"] == 14 || $_GET["templates_id"] == 13 || $_GET["templates_id"] == 5 || $_GET["templates_id"] == 49  || $_GET["templates_id"] == 55  || $_GET["templates_id"] == 56 )){
		//if(isset($_GET['classe_url']) && $_GET['classe_url'] != ""){
			echo '<script src="http://mastertag.effiliation.com/mt660015994.js?page=category&idcat='.$_GET['templates_id'].'&wordingcat=Formules" async="async" ></script>';
		/*}else if(isset($_GET['matiere_url']) && $_GET['matiere_url'] != ""){
			echo '<script src="http://mastertag.effiliation.com/mt660015994.js?page=category&idcat='.$_GET['templates_id'].'&wordingcat='.$_GET['matiere_url'].'" async="async" ></script>';
		}*/
	}
}
/* Fin Implémentation plan de taggage */
?>

<!-- Code Google de la balise de remarketing Digital Keys -->
<!--------------------------------------------------
Les balises de remarketing ne peuvent pas être associées aux informations personnelles ou placées sur des pages liées aux catégories à caractère sensible. Pour comprendre et savoir comment configurer la balise, rendez-vous sur la page http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 968194281;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/968194281/?value=0&guid=ON&script=0"/>
</div>
</noscript>


</body>
<script>
	jQuery(document).ready(function() {
  
			jQuery('#account_block_header a').click(function(){
				if(jQuery(this).parent().hasClass( "open" ))
					jQuery(this).parent().removeClass('open');
				else
					jQuery(this).parent().addClass('open');
			});
			jQuery('#account_block_connected a').click(function(){
				if(jQuery(this).parent().hasClass( "open" ))
					jQuery(this).parent().removeClass('open');
				else
					jQuery(this).parent().addClass('open');
			});
		<?php
		if($isMobile){
		?>
			jQuery('#menu-classe').click(function(){
				if(jQuery(this).parent().hasClass( "open" )){
					jQuery(this).parent().removeClass('open');
					jQuery(".menu-classe-sub").css("display", "none"); 
				}else{
					jQuery(this).parent().addClass('open');
					jQuery(".menu-classe-sub").css("display", "block"); 
				}
			});
			
			jQuery('#menu-matiere').click(function(){
				if(jQuery(this).parent().hasClass( "open" )) 
					jQuery(this).parent().removeClass('open');
					else
						jQuery(this).parent().addClass('open');
					
			});		
		<?php 
		}else{
		?>
			jQuery('#menu-classe').click(function(){
				document.location.href = jQuery('#menu-classe').attr('href'); 
			});
			jQuery('#menu-matiere').click(function(){
				document.location.href = jQuery('#menu-matiere').attr('href'); 
			});
			
		<?php 
		}
		?>
			jQuery('#menu-classe').hover(function(){
				
					jQuery(this).parent().addClass('open');
					jQuery(".menu-classe-sub").css("display", "block"); 
			
			});
			jQuery('#menu-classe').parent().mouseleave(function() {
				jQuery('#menu-classe').parent().removeClass('open');
			});
			
		
			
			jQuery('#menu-matiere').hover(function(){
				jQuery(this).parent().addClass('open');				
			});
			jQuery('#menu-matiere').parent().mouseleave(function() {
				jQuery('#menu-matiere').parent().removeClass('open');
			});
		
		
		jQuery('.navbar-brand').click(function(){
			jQuery('.navbar-toggle').click();
		});
	});
	
	
</script>
</html>
<?php $oDb->close(); ?>