<?php 

	session_start();
	// unset($_SESSION['prod']); 
	if( (isset($_GET['prod']) && $_GET['prod']==20140630 ) || (isset($_SESSION['prod']) && $_SESSION['prod']==20140630 )){
		if(isset($_GET['prod']) && $_GET['prod']==20140630)
			$_SESSION['prod'] = $_GET['prod'];
			
			define( 'ROOT', dirname( __FILE__ ));
			$_CONST['path']["server"] = ROOT;
			
			include_once(ROOT."/lib/config.inc.php");
			include_once(ROOT."/lib/database.class.php");
			include_once(ROOT."/lib/JoForm.class.php");
			include_once(ROOT."/lib/functions.php");
			include_once(ROOT."/lib/intyMailer.php");
			include_once(ROOT."/lib/abo.class.php");
			include_once(ROOT."/lib/cb.class.php");
			$isMobile=(bool)preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
			$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
			
			$oDb->query("SET NAMES utf8"); 
			LoadConfig($_CONST); //load eco_config table IN $_CONST
			
			$_SESSION["langs_id"] = 1 ;
				// echo $_SESSION["langs_id"];
		// var_dump($_SESSION["langs_id"]);
			/* ************************** */
			global $aLang, $_CONST ;
				
			
			
			/* Gestion des templates dynamiques */
			
			
					if(isset($_GET["templates_id"]) && $_GET["templates_id"] == 9){
						//Template rubrique => On doit detecter l'id de la rubrique à partir du titre de l'url 
						$strSql="SELECT rubrique_titre , rubrique_id FROM bor_rubrique WHERE rubrique_actif = 1"; 
						$aRubriques = $oDb->queryTab($strSql); 
						foreach ($aRubriques as $aRubrique){
							// var_dump($_GET['rubrique']);
							// var_dump(strToUrl($aRubrique['rubrique_titre']));
							if($_GET['rubrique'] == strToUrl($aRubrique['rubrique_titre'])){
								$_GET['id_rubrique'] = $aRubrique['rubrique_id'] ; 
								// var_dump($_GET['id_rubrique']);
								break;
							}
							// $_GET["templates_id"] = 1 ;
						}
					}
				
				
				if(isset($_GET["template"]) && !empty($_GET["template"])){
					$strSql="SELECT templates_name , templates_id FROM eco_templates ";
					$aTemplates = $oDb->queryTab($strSql); 
					foreach ($aTemplates as $aTemplate){
						if($_GET['template'] == strToUrl($aTemplate['templates_name'])){
							$_GET['templates_id'] = $aTemplate['templates_id'] ; 
							break;
						}
					}
				}
			/************************************/
			// var_dump($_GET['id_rubrique']); 
			// var_dump($_GET['templates_id']);
		?>
		<!DOCTYPE html>
		<html lang="fr">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<?php get_meta_title_desc(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" href="/favicon.ico" />
		<?php include('scripts.php');  ?>
		<?php include('css.php'); ?>

		<?php
		echo '<link href="/assets/bordas-soutien/css/style_menu.css" rel="stylesheet" type="text/css">';
		?>

		</head>


		<?php $t = get_template_filename(); if($t == 'accueil') $class= 'home'; else $class= $t; ?>
		<body id="<?php echo $class; ?>">
		<?php 
			if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
		?>
				<!-- GA -->
				<script>
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					ga('create', '<?php echo $_CONST['TAG_ANALYTICS']; ?>', 'bordas-soutien-scolaire.com');
					ga('send', 'pageview');
		<?php 
					if(  isset($_SESSION['user']['IDCLIENT']) && $_SESSION['user']['IDCLIENT'] > 0 ){
		?>
					var NumWeb = '<?php echo $_SESSION['user']['IDCLIENTWEB']; ?>';
					ga('set', 'dimension1', NumWeb);
					var NumCli = '<?php echo $_SESSION['user']['NUMCLIENT']; ?>';
					ga('set', 'dimension2', NumCli);
		<?php 
					}
		?>
				</script>
				<!-- /GA -->
		<?php 
			}
			
			
			include('header.php'); ?>
		<!-- CONTAINER FULL WIDTH ============================================== -->
				<!-- Contenue -->
				<section id="home" class="content">
					<?php 
					if($t != 'accueil')
						echo '<div class="container"> ';
						
					// var_dump($STRVAR);
					// $STRVAR = get_template_title(); 	
					 get_page(); 
					if($t != 'accueil')
						echo '</div>'; 
					?>
				</section>
		<!-- /.CONTAINER FULL WIDTH ============================================== --> 


		<?php include('footer.php'); ?>

		</body>
		<script>
			jQuery(document).ready(function() {
		  
					jQuery('#account_block_header a').click(function(){
						if(jQuery(this).parent().hasClass( "open" ))
							jQuery(this).parent().removeClass('open');
						else
							jQuery(this).parent().addClass('open');
					});
					jQuery('#account_block_connected a').click(function(){
						if(jQuery(this).parent().hasClass( "open" ))
							jQuery(this).parent().removeClass('open');
						else
							jQuery(this).parent().addClass('open');
					});
				<?php
				if($isMobile){
				?>
					jQuery('#menu-classe').click(function(){
						if(jQuery(this).parent().hasClass( "open" )){
							jQuery(this).parent().removeClass('open');
							jQuery(".menu-classe-sub").css("display", "none"); 
						}else{
							jQuery(this).parent().addClass('open');
							jQuery(".menu-classe-sub").css("display", "block"); 
						}
					});
					
					jQuery('#menu-matiere').click(function(){
						if(jQuery(this).parent().hasClass( "open" )) 
							jQuery(this).parent().removeClass('open');
							else
								jQuery(this).parent().addClass('open');
							
					});		
				<?php 
				}else{
				?>
					jQuery('#menu-classe').click(function(){
						document.location.href = jQuery('#menu-classe').attr('href'); 
					});
					jQuery('#menu-matiere').click(function(){
						document.location.href = jQuery('#menu-matiere').attr('href'); 
					});
					
				<?php 
				}
				?>
					jQuery('#menu-classe').hover(function(){
						
							jQuery(this).parent().addClass('open');
							jQuery(".menu-classe-sub").css("display", "block"); 
					
					});
					jQuery('#menu-classe').parent().mouseleave(function() {
						jQuery('#menu-classe').parent().removeClass('open');
					});
					
				
					
					jQuery('#menu-matiere').hover(function(){
						jQuery(this).parent().addClass('open');				
					});
					jQuery('#menu-matiere').parent().mouseleave(function() {
						jQuery('#menu-matiere').parent().removeClass('open');
					});
				
				
				jQuery('.navbar-brand').click(function(){
					jQuery('.navbar-toggle').click();
				});
			});
			
			
		</script>
		</html>
		<?php $oDb->close(); 
		exit;
	}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="canonical" href="http://www.bordas-soutien-scolaire.com">
<title>Bordas soutien scolaire : Pour la réussite de vos enfants, du CP à la Terminale</title>
<meta name="description" content="Un site d’entraînement en ligne pour accompagner votre enfant tout au long de l'année">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow">
<link rel="icon" href="/favicon.ico" />
	<script type="text/javascript" src="assets/bordas-soutien/js/bootstrap.js"></script>
	
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>

		<script type="text/javascript" src="assets/bordas-soutien/js/dropdown.js"></script>
		<script type="text/javascript" src="assets/bordas-soutien/js/collapse.js"></script>
		<script  src="/assets/bordas-soutien/js/jquery.flexslider.js"></script>
		<!-- Add fancyBox main JS and CSS files -->
			<script type="text/javascript" src="assets/bordas-soutien/js/jquery.fancybox.js?v=2.1.5"></script>
	
		
		<!-- Le styles -->
	<link href="/assets/bordas-soutien/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="/assets/bordas-soutien/css/style_menu.css" rel="stylesheet" type="text/css">
	<link href="/assets/bordas-soutien/css/bootstrap-theme.css" rel="stylesheet" type="text/css">
	<link href="/assets/bordas-soutien/css/global.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="/assets/bordas-soutien/css/jquery.fancybox.css?v=2.1.5" media="screen" />
   	<!--[if IE ]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->  

<!--[if gte IE 6]>
<style type="text/css">
.container-fluid {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
}
.row {
  margin-right: -15px;
  margin-left: -15px;
}

.container {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
}


.col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
  position: relative;
  min-height: 1px;
  padding-right: 15px;
  padding-left: 15px;
}
.col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9, .col-xs-10, .col-xs-11, .col-xs-12 {
  float: left;
}
.col-xs-12 {
  width: 100%;
}
.col-xs-11 {
  width: 91.66666667%;
}
.col-xs-10 {
  width: 83.33333333%;
}
.col-xs-9 {
  width: 75%;
}
.col-xs-8 {
  width: 66.66666667%;
}
.col-xs-7 {
  width: 58.33333333%;
}
.col-xs-6 {
  width: 50%;
}
.col-xs-5 {
  width: 41.66666667%;
}
.col-xs-4 {
  width: 33.33333333%;
}
.col-xs-3 {
  width: 25%;
}
.col-xs-2 {
  width: 16.66666667%;
}
.col-xs-1 {
  width: 8.33333333%;
}
.col-xs-pull-12 {
  right: 100%;
}
.col-xs-pull-11 {
  right: 91.66666667%;
}
.col-xs-pull-10 {
  right: 83.33333333%;
}
.col-xs-pull-9 {
  right: 75%;
}
.col-xs-pull-8 {
  right: 66.66666667%;
}
.col-xs-pull-7 {
  right: 58.33333333%;
}
.col-xs-pull-6 {
  right: 50%;
}
.col-xs-pull-5 {
  right: 41.66666667%;
}
.col-xs-pull-4 {
  right: 33.33333333%;
}
.col-xs-pull-3 {
  right: 25%;
}
.col-xs-pull-2 {
  right: 16.66666667%;
}
.col-xs-pull-1 {
  right: 8.33333333%;
}
.col-xs-pull-0 {
  right: 0;
}
.col-xs-push-12 {
  left: 100%;
}
.col-xs-push-11 {
  left: 91.66666667%;
}
.col-xs-push-10 {
  left: 83.33333333%;
}
.col-xs-push-9 {
  left: 75%;
}
.col-xs-push-8 {
  left: 66.66666667%;
}
.col-xs-push-7 {
  left: 58.33333333%;
}
.col-xs-push-6 {
  left: 50%;
}
.col-xs-push-5 {
  left: 41.66666667%;
}
.col-xs-push-4 {
  left: 33.33333333%;
}
.col-xs-push-3 {
  left: 25%;
}
.col-xs-push-2 {
  left: 16.66666667%;
}
.col-xs-push-1 {
  left: 8.33333333%;
}
.col-xs-push-0 {
  left: 0;
}
.col-xs-offset-12 {
  margin-left: 100%;
}
.col-xs-offset-11 {
  margin-left: 91.66666667%;
}
.col-xs-offset-10 {
  margin-left: 83.33333333%;
}
.col-xs-offset-9 {
  margin-left: 75%;
}
.col-xs-offset-8 {
  margin-left: 66.66666667%;
}
.col-xs-offset-7 {
  margin-left: 58.33333333%;
}
.col-xs-offset-6 {
  margin-left: 50%;
}
.col-xs-offset-5 {
  margin-left: 41.66666667%;
}
.col-xs-offset-4 {
  margin-left: 33.33333333%;
}
.col-xs-offset-3 {
  margin-left: 25%;
}
.col-xs-offset-2 {
  margin-left: 16.66666667%;
}
.col-xs-offset-1 {
  margin-left: 8.33333333%;
}
.col-xs-offset-0 {
  margin-left: 0;
}

 .container {
    width: 1170px;
  }
  
    .modal-lg {
    width: 900px;
  }
  
 .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
    float: left;
  }
  .col-md-12 {
    width: 100%;
  }
  .col-md-11 {
    width: 91.66666667%;
  }
  .col-md-10 {
    width: 83.33333333%;
  }
  .col-md-9 {
    width: 75%;
  }
  .col-md-8 {
    width: 66.66666667%;
  }
  .col-md-7 {
    width: 58.33333333%;
  }
  .col-md-6 {
    width: 50%;
  }
  .col-md-5 {
    width: 41.66666667%;
  }
  .col-md-4 {
    width: 33.33333333%;
  }
  .col-md-3 {
    width: 25%;
  }
  .col-md-2 {
    width: 16.66666667%;
  }
  

  
  
  .col-md-1 {
    width: 8.33333333%;
  }
  .col-md-pull-12 {
    right: 100%;
  }
  .col-md-pull-11 {
    right: 91.66666667%;
  }
  .col-md-pull-10 {
    right: 83.33333333%;
  }
  .col-md-pull-9 {
    right: 75%;
  }
  .col-md-pull-8 {
    right: 66.66666667%;
  }
  .col-md-pull-7 {
    right: 58.33333333%;
  }
  .col-md-pull-6 {
    right: 50%;
  }
  .col-md-pull-5 {
    right: 41.66666667%;
  }
  .col-md-pull-4 {
    right: 33.33333333%;
  }
  .col-md-pull-3 {
    right: 25%;
  }
  .col-md-pull-2 {
    right: 16.66666667%;
  }
  .col-md-pull-1 {
    right: 8.33333333%;
  }
  .col-md-pull-0 {
    right: 0;
  }
  .col-md-push-12 {
    left: 100%;
  }
  .col-md-push-11 {
    left: 91.66666667%;
  }
  .col-md-push-10 {
    left: 83.33333333%;
  }
  .col-md-push-9 {
    left: 75%;
  }
  .col-md-push-8 {
    left: 66.66666667%;
  }
  .col-md-push-7 {
    left: 58.33333333%;
  }
  .col-md-push-6 {
    left: 50%;
  }
  .col-md-push-5 {
    left: 41.66666667%;
  }
  .col-md-push-4 {
    left: 33.33333333%;
  }
  .col-md-push-3 {
    left: 25%;
  }
  .col-md-push-2 {
    left: 16.66666667%;
  }
  .col-md-push-1 {
    left: 8.33333333%;
  }
  .col-md-push-0 {
    left: 0;
  }
  .col-md-offset-12 {
    margin-left: 100%;
  }
  .col-md-offset-11 {
    margin-left: 91.66666667%;
  }
  .col-md-offset-10 {
    margin-left: 83.33333333%;
  }
  .col-md-offset-9 {
    margin-left: 75%;
  }
  .col-md-offset-8 {
    margin-left: 66.66666667%;
  }
  .col-md-offset-7 {
    margin-left: 58.33333333%;
  }
  .col-md-offset-6 {
    margin-left: 50%;
  }
  .col-md-offset-5 {
    margin-left: 41.66666667%;
  }
  .col-md-offset-4 {
    margin-left: 33.33333333%;
  }
  .col-md-offset-3 {
    margin-left: 25%;
  }
  .col-md-offset-2 {
    margin-left: 16.66666667%;
  }
  .col-md-offset-1 {
    margin-left: 8.33333333%;
  }
  .col-md-offset-0 {
    margin-left: 0;
  }


.carousel {
  position: relative;
}
.carousel-inner {
  position: relative;
  width: 100%;
  overflow: hidden;
}
.carousel-inner > .item {
  position: relative;
  display: none;
  -webkit-transition: .6s ease-in-out left;
          transition: .6s ease-in-out left;
}
.carousel-inner > .item > img,
.carousel-inner > .item > a > img {
  line-height: 1;
}
.carousel-inner > .active,
.carousel-inner > .next,
.carousel-inner > .prev {
  display: block;
}
.carousel-inner > .active {
  left: 0;
}
.carousel-inner > .next,
.carousel-inner > .prev {
  position: absolute;
  top: 0;
  width: 100%;
}
.carousel-inner > .next {
  left: 100%;
}
.carousel-inner > .prev {
  left: -100%;
}
.carousel-inner > .next.left,
.carousel-inner > .prev.right {
  left: 0;
}
.carousel-inner > .active.left {
  left: -100%;
}
.carousel-inner > .active.right {
  left: 100%;
}
.carousel-control {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  width: 15%;
  font-size: 20px;
  color: #fff;
  text-align: center;
  text-shadow: 0 1px 2px rgba(0, 0, 0, .6);
  filter: alpha(opacity=50);
  opacity: .5;
}
.carousel-control.left {
  background-image: -webkit-linear-gradient(left, color-stop(rgba(0, 0, 0, .5) 0%), color-stop(rgba(0, 0, 0, .0001) 100%));
  background-image:         linear-gradient(to right, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .0001) 100%);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#80000000', endColorstr='#00000000', GradientType=1);
  background-repeat: repeat-x;
}
.carousel-control.right {
  right: 0;
  left: auto;
  background-image: -webkit-linear-gradient(left, color-stop(rgba(0, 0, 0, .0001) 0%), color-stop(rgba(0, 0, 0, .5) 100%));
  background-image:         linear-gradient(to right, rgba(0, 0, 0, .0001) 0%, rgba(0, 0, 0, .5) 100%);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#80000000', GradientType=1);
  background-repeat: repeat-x;
}
.carousel-control:hover,
.carousel-control:focus {
  color: #fff;
  text-decoration: none;
  filter: alpha(opacity=90);
  outline: none;
  opacity: .9;
}
.carousel-control .icon-prev,
.carousel-control .icon-next,
.carousel-control .glyphicon-chevron-left,
.carousel-control .glyphicon-chevron-right {
  position: absolute;
  top: 50%;
  z-index: 5;
  display: inline-block;
}
.carousel-control .icon-prev,
.carousel-control .glyphicon-chevron-left {
  left: 50%;
}
.carousel-control .icon-next,
.carousel-control .glyphicon-chevron-right {
  right: 50%;
}
.carousel-control .icon-prev,
.carousel-control .icon-next {
  width: 20px;
  height: 20px;
  margin-top: -10px;
  margin-left: -10px;
  font-family: serif;
}
.carousel-control .icon-prev:before {
  content: '\2039';
}
.carousel-control .icon-next:before {
  content: '\203a';
}
.carousel-indicators {
  position: absolute;
  bottom: 10px;
  left: 50%;
  z-index: 15;
  width: 60%;
  padding-left: 0;
  margin-left: -30%;
  text-align: center;
  list-style: none;
}
.carousel-indicators li {
  display: inline-block;
  width: 10px;
  height: 10px;
  margin: 1px;
  text-indent: -999px;
  cursor: pointer;
  background-color: #000 \9;
  background-color: rgba(0, 0, 0, 0);
  border: 1px solid #fff;
  border-radius: 10px;
}
.carousel-indicators .active {
  width: 12px;
  height: 12px;
  margin: 0;
  background-color: #fff;
}
.carousel-caption {
  position: absolute;
  right: 15%;
  bottom: 20px;
  left: 15%;
  z-index: 10;
  padding-top: 20px;
  padding-bottom: 20px;
  color: #fff;
  text-align: center;
  text-shadow: 0 1px 2px rgba(0, 0, 0, .6);
}
.carousel-caption .btn {
  text-shadow: none;
}

  .navbar-nav {
    float: left;
    margin: 0;
  }
  .navbar-nav > li {
    float: left;
  }
  .navbar-nav > li > a {
    padding-top: 15px;
    padding-bottom: 15px;
  }
  .navbar-nav.navbar-right:last-child {
    margin-right: -15px;
  }
  
 .header_right {
float: right;
width: 600px;
 }
 
.header_left {
float: left;
}

div.col-md-8.prehome_offre
{
	float:none !important;
	clear:both;
	margin:0 auto;
	margin-bottom:40px
	}
    
.collapse
{
display:block !important
}

.navbar-default .navbar-toggle
{
display:none !important
}

</style>
<![endif]-->

	
    
	<style>a {
	color: initial;
	text-decoration: initial!important;
	}</style>

</head>

<body id="pre-home">
			
		<div class="top_center">
		<!-- AddThis Button BEGIN -->
			<div class="addthis_toolbox addthis_default_style">
				<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
				<a class="addthis_button_tweet"></a>
				<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
			</div>
			<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
			<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-533ab9615d262421"></script>
			<!-- AddThis Button END -->
		</div>
		<div class="header_center">
			<a href="/"><img class="img-responsive" src="/images/logo.jpg" alt=""></a>
		</div>
        
        <h1 class="slogan_prehome container">Pour la réussite de vos enfants, du CP à la Terminale</h1>
        
        <div class="spacer"></div>
        
		<div class="container prehome_blocks">
			<div class="row">
				
				
				<div class="col-md-8 prehome_offre">
				<div class="prehome_offre btn-blue">Maintenance</div>
				<div class="blue_sky prehome-desc">
				<p class="description-offre"> En raison d’une maintenance technique, notre site est actuellement indisponible.<br/>
Veuillez nous excuser pour la gêne occasionnée. Nous vous remercions de votre compréhension. </p>
				</div>
				</div>
				
			</div>
			
			
		</div>
		<div class="copyright" style=" position: absolute;  bottom: 0;  width: 100%;">
		<div class="container">Bordas Soutien scolaire est un service des éditions 
		<a href="http://www.editions-bordas.fr/" target="_blank"><img src="../../../images/logo_copyright.png" alt=""></a>
		<center class="seo_footer">
		Fondées en 1946, les éditions Bordas sont spécialistes de l’apprentissage et des manuels scolaires. Elles proposent aujourd’hui deux offres innovantes
		de soutien scolaire, en ligne ou à domicile, pour accompagner les élèves après l’école... et ainsi leur donner toutes les chances de réussite !
		</center>
		<div class="link-footer">
			<a href="http://www.editions-bordas.fr" class="btn-footer" target="_blank">Les éditions Bordas >></a>
			<a href="http://www.editions-bordas.fr/eleves" class="btn-footer"  target="_blank">Nos livres >></a>
		</div>		
		</div>
		</div>

</body>
</html>

