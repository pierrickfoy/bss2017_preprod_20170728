<?php
/**
 * Classe permettant de gérer l'affichage des formulaires
 * 
 * <p></p>
 * 
 * @name JoForm
 * @author MIZRAHI Johan <johan.mizrahi@ecomiz.com> 
 * @copyright EcomiZ
 * @version 1.0 - 2009-04-10
 * @version 2.0 - 2012-07-21
 */


class JoForm {
var $FormName = null;
var $FormHide = null;
var $HtmlForm = null;
var $Data = null;
var $fieldset = false;
var $required = array();
var $Error = array();
var $ErrorColor = '#FFBBBB';
var $FormRule = null;
var $iNbDateInput = 0;
var $iNbFileInput = 0;
var $iFormSize = 8;
var $readOnly = 0;
	function JoForm($name="JoForm", $action="", $width="100%", $fieldset=false, $title="", $PathJS = "./js/", $PathCSS = "./css/", $iRight="0", $FormSize='8')
	{
		$this->iFormSize = $FormSize;
		$this->fieldset = $fieldset;
		$this->FormName = $name;
		$this->HtmlForm .= "<form enctype=\"multipart/form-data\" name=\"$name\" method=\"post\" action=\"$action\" class=\"form-horizontal\">\n";
		switch($iRight) {
			case 1 : $this->readOnly = 1;
			break;
			case 2 : $this->readOnly = 0;
			break;		
		}
	}
	
	function CloseForm()
	{
		$this->HtmlForm .= "\n</form>\n";
	}
	
	function CheckError()
	{
		while (list($name, $value) = each ($this->Error))
		{
			//$this->HtmlForm = str_replace("<!--".$name."Error-->", $this->Error["$name"], $this->HtmlForm);
			$this->HtmlForm = str_replace("[BG".$name."Error]", " style=\"background:".$this->ErrorColor.";\"", $this->HtmlForm);
		}
	}
	
	function Display()
	{
		$this->CloseForm();
		$this->CheckError();
		echo $this->HtmlForm;
	}
	
	//open box to put form
	function OpenFormBox($strTitle) {
		$this->HtmlForm .= '<div class="row-fluid"><div class="span12">

								<div class="box">

									<div class="title">

										<h4> 
											<span>'.$strTitle.'</span>
										</h4>
										
									</div>
									<div class="content">';
	}
	
	//close box for form
	function CloseFormBox() {
		$this->HtmlForm .= '</div></div></div></div>';
	}
	
	function AddFormRule($func)
	{
		$this->FormRule = $func;
	}
	
	function Set_EditData($query)
	{
		global $oDb;
		$oDb->query($query);
		while (list($key, $val) = each($oDb->data))
			if (!isset($_POST["$key"]))
			$_POST["$key"] = $val[0];
	}
	
	function CheckRequire($name)
	{
		if (isset($_POST[$this->FormName]))
			foreach($this->required as $chps)
			{
				if ((!isset($_POST["$chps"]) || $_POST["$chps"] == "") && !isset($_FILES["$chps"]))
					$this->Error["$chps"] = "Champs obligatoire";
			}
	}
	
	function AddHidden($name, $value)
	{
		$value = isset($_POST["$name"]) ? $_POST["$name"] : $value;
		$this->FormHide .= "<input type='hidden' name='$name' value='$value'>\n";
	}
	
	function AddColorPicker($name, $label, $require=false)
	{
		//TODO
	}
	
	/* UPLOAD File in Ajax */
	function AddAjaxFile($strName, $strLabel, $strPath, $strSourceTable, $strSourceKey, $iSourceKey, $type="file", $iMaxFiles=0, $strFilesTable="eco_files", $iImgWidth=100) {
		global $oDb;
		global $_GET;
		
		$this->iNbFileInput++;
		$inbInput = $this->iNbFileInput;
		
		/* Delete action */
		if (!empty($_GET["del_files_id"])) {
			$strFileRealPath = $oDb->queryItem("SELECT files_path FROM $strFilesTable 
												WHERE 
													files_table_source='$strSourceTable' 
													AND files_table_id_name='$strSourceKey' 
													AND files_field_name='$strName' 
													AND files_table_id='$iSourceKey'
													AND files_id='".mysql_real_escape_string($_GET["del_files_id"])."'");
			$oDb->Squery("DELETE FROM $strFilesTable 
							WHERE 
							files_table_source='$strSourceTable' 
							AND files_table_id_name='$strSourceKey' 
							AND files_field_name='$strName' 
							AND files_table_id='$iSourceKey'
							AND files_id='".mysql_real_escape_string($_GET["del_files_id"])."'");
			@unlink($strFileRealPath);
		}	
		$strJS = "<script type= \"text/javascript\">/*<![CDATA[*/
			var button = jQueryR('#button$inbInput'), interval;
			new AjaxUpload(button,{
				action: '../lib/JoForm.upload.php', 
				name: '$strName',
				onSubmit : function(file, ext){
				";
		/*Vérification javascript du type de fichier si image*/
		if ($type == "img") {
			$strJS .= "if (ext && /^(jpg|png|jpeg|gif)$/.test(ext)){
							/* Setting data */
							this.setData({
								'path' : '$strPath',
								'savetable' : \"$strFilesTable\",
								'savefieldtable' : \"$strSourceTable\",
								'savefieldPK' : \"$strSourceKey\",
								'form_fieldName' : '$strName',
								'typeOfFile' : '$type', /* img OR file */
								'id' : '$iSourceKey',
								'imgWidth' : '$iImgWidth',
								'MaxFiles' : '$iMaxFiles',
								'returnURL' : '".$_SERVER['REQUEST_URI']."'
							});
							jQueryR('#JoFormUploadDiv$inbInput .UploadDivtext').text('Uploading ' + file);	
						} else {
							var strImg = \"<img src='images/delete.png'>\";
							jQueryR('#JoFormUploadDiv$inbInput .UploadDivtext').html(strImg + \" Seul les images sont authorisées\");
							return false;				
						}";
		} else { 
			$strJS .= "/* Setting data */
						this.setData({
								'path' : '$strPath',
								'savetable' : \"$strFilesTable\",
								'savefieldtable' : \"$strSourceTable\",
								'savefieldPK' : \"$strSourceKey\",
								'form_fieldName' : '$strName',
								'typeOfFile' : '$type', /* img OR file */
								'id' : '$iSourceKey',
								'imgWidth' : '$iImgWidth',
								'MaxFiles' : '$iMaxFiles',
								'returnURL' : '".$_SERVER['REQUEST_URI']."'
						});
						jQueryR('#JoFormUploadDiv$inbInput .UploadDivtext').text('Uploading ' + file);";				
		}
		$strJS .= "button.text('Uploading');
					this.disable();
					interval = window.setInterval(function(){
						var text = button.text();
						if (text.length < 13){
							button.text(text + '.');					
						} else {
							button.text('Uploading');				
						}
					}, 200);
				},
				onComplete: function(file, response){
					button.text('Upload');
							
					window.clearInterval(interval);
								
					// enable upload button
					this.enable();
					var reg=new RegExp(\"[:]+\", \"g\");
					var aResponse = response.split(reg);
					if (aResponse[0] == \"[OK]\") {
						jQueryR('#JoFormUploadDiv$inbInput .UploadDivtext').html(aResponse[2]);";
					
					if ($type == "img")	
						$strJS .= "jQueryR('<div></div>').appendTo('#JoFormUploadDiv$inbInput .UploadFile').html(aResponse[1]);";
					else
						$strJS .= "jQueryR('<div></div>').appendTo('#JoFormUploadDiv$inbInput .UploadFile').html(aResponse[1]);";
					
					$strJS .= "
					}
					else
						jQueryR('#JoFormUploadDiv$inbInput .UploadDivtext').html(aResponse[2]);	
								
				}
			});
		/*]]>*/</script>";
				
		$ErrName = $strName."Error";

		$this->HtmlForm .= '<div class="form-row row-fluid">
                                            <div class="span'.$this->iFormSize.'">
                                                <div class="row-fluid">';
                                                    
                                               
		$this->HtmlForm .= '<label class="form-label span4" for="normal">'.($strLabel).'</label>';
		
		$this->HtmlForm .= "<div id='JoFormUploadDiv$inbInput' class='JoFormUploadDiv'>";
		$this->HtmlForm .= "	<div class=\"wrapper\">
									<div id=\"button$inbInput\" class=\"Uploadbutton\">Upload</div>
									<div class=\"UploadDivtext\" id=\"text\"></div>
								</div>
								<div class=\"UploadFile\">";
		$oDb->Squery("	SELECT * FROM $strFilesTable 
			WHERE 
				files_table_source='$strSourceTable' 
				AND files_table_id_name='$strSourceKey' 
				AND files_field_name='$strName' 
				AND files_table_id='$iSourceKey'");
		$this->HtmlForm .= ($type != "img" ? "<ul>" : ""); 
		while ($oFiles = $oDb->GetObject()) {
			if ($type == "img") {
				$this->HtmlForm .= "<div><img src='".$oFiles->files_path."' width='$iImgWidth'><a href='".$_SERVER['REQUEST_URI']."&del_files_id=".$oFiles->files_id."'><img src='images/delete.png' alt='[suppr.]'></a></div>";
			}
			else {
				$this->HtmlForm .= "<li><a href='".$oFiles->files_path."'>".$oFiles->files_name."</a> <a href='".$_SERVER['REQUEST_URI']."&del_files_id=".$oFiles->files_id."'><img src='images/delete.png' alt='[suppr.]'></a></li>";
			}
		}
		$this->HtmlForm .= ($type != "img" ? "</ul>" : ""); 
		$this->HtmlForm .=	"	</div>
							</div>";
		
		$this->HtmlForm .= $strJS;
		$this->HtmlForm .= "<br><span id=\"$ErrName\" class='texte_error'><!--$ErrName--></span>";
				$this->HtmlForm .= "</div>";
			$this->HtmlForm .= "</div>";
		$this->HtmlForm .= "</div>";
		
		
		
	}
	
	function AddFile($name, $label, $type="file") {
		$ErrName = $name."Error";
		$path = $_POST["$name"];
		$this->HtmlForm .= "<tr>\n<td align='right' width=30%>";
		$this->HtmlForm .= ($label)."</td>\n<td align='left' width=50%>\n";
		if (empty($path))
			$this->HtmlForm .= "<input type='file' name='$name'>";
		else {
			if ($type == "img")
				$this->HtmlForm .= "<img src='$path' width='250px'>";
			else {
				$aPath = explode('/', $path);
				array_reverse($aPath);
				$this->HtmlForm .= "<a href='$path'>".$aPath[0]."</a>";
			}
			$this->HtmlForm .= "<a href='".$_SERVER['PHP_SELF']."&JoForm_Delfile=$name'>[DEL]</a>";
				
		}
		$this->HtmlForm .= "<br><span id=\"$ErrName\" class='texte_error'><!--$ErrName--></span></td></tr>\n";
	}
	
	function AddInput($type, $name, $label, $require=false, $atributes="", $function=null, $event="OnChange", $regex="")
	{
		$ErrName = $name."Error";
		$strClass = "text";
		$strId = "normalInput";
		$strClass = "span8";
		$val = isset($_POST["$name"]) ? $_POST["$name"] : "";
		if ($type == "date")
		{
			$type="text";
			$strClass = "hasDatepicker";
			$strId = "datepicker";
			
		}
		$this->HtmlForm .= '<div class="form-row row-fluid">
                                            <div class="span'.$this->iFormSize.'">
                                                <div class="row-fluid">';
                                                    
                                               
		$this->HtmlForm .= '<label class="form-label span4" for="normal">'.($label).'</label>';
		
		if($this->readOnly==0){
		$this->HtmlForm .= "\n<input type=\"$type\" class=\"$strClass $type\" id=\"$strId\" name=\"$name\" value=\"$val\" $atributes ";
		$this->HtmlForm .= "[BG".$ErrName."] ";
		if (isset($function))
		{
			$paramNames = array('fct', 'FormName', 'FieldName');			
			$paramValues = array("$function", $this->FormName, $name);
			$this->HtmlForm .= "$event=\"javascript:$function";
		}
		
		$this->HtmlForm .= " /><span id=\"$ErrName\" class='texte_error'><!--$ErrName--></span></td></tr>\n";
		}else{
			$this->HtmlForm .= "\n $val ";
			$this->HtmlForm .= "<span id=\"$ErrName\" class='texte_error'><!--$ErrName--></span></td></tr>\n";
		}
		if ($require == true)
		{
			array_push($this->required, $name);
			$this->CheckRequire($name);
			$this->HtmlForm .= '<div class="required_tag tooltip hover left"></div>';
		}
                if (!empty($regex))
		{
                    
                    $subject = $_POST["$name"];
                    $pattern = '/'.$regex.'/';
                    if(!preg_match($pattern, $subject, $matches)){
			$this->Error["$name"] = "Champs obligatoire";
			$this->HtmlForm .= '<div class="required_tag tooltip hover left"></div>';
                    }
		}
		$this->HtmlForm .= ' </div></div></div>';
	}
	
	function AddSelectArray($name, $label, $list, $requiredOption=false, $strDefault = null)
	{
		$ErrName = $name."Error";
		$val = isset($_POST["$name"]) ? $_POST["$name"] : "";
		if(isset($_POST["$name"]) && !empty($_POST["$name"]))
			$val = $_POST["$name"] ; 
		else if(!empty($strDefault))
			$val = $strDefault ; 
		else 
			$val = ""; 
		$this->HtmlForm .= '<div class="form-row row-fluid">
                                            <div class="span'.$this->iFormSize.'">
                                                <div class="row-fluid">';
		$this->HtmlForm .= '<label class="form-label span4" for="normal">'.($label).'</label>';
		
		if($this->readOnly == 0){
			$strShow = isset($list[$val]) ? $list[$val] :  "Choose";
			$this->HtmlForm .= '<div class="span8 controls">';
			$this->HtmlForm .= "<select name=\"$name\">";
			if($requiredOption == false){
				$this->HtmlForm .= "<option value=0> => Choisissez</option>\n";
			}
		while (list($key, $value) = each($list))
		{
			$this->HtmlForm .= "<option value=\"$key\"";
			$this->HtmlForm .= ($key == $val) ? "SELECTED" : "";
			$this->HtmlForm .= ">".($value)."</option>\n";
		}
		$this->HtmlForm .= "</select></div>\n<span id=\"$ErrName\" class='texte_error'><!--$ErrName--></span>";
		}else{
			while (list($key, $value) = each($list))
			{
				if($key == $val){
					$this->HtmlForm .= ($value)."\n<span id=\"$ErrName\" class='texte_error'><!--$ErrName--></span>";
				}			
			}		
		}
		$this->HtmlForm .= ' </div></div></div>';
	}
	
	function AddSelectSQL($name, $label, $query, $field_name, $field_value, $opt="", $requiredOption=false, $strDefault = null)
	{
		global $oDb;
		
		$ErrName = $name."Error";
		// $val = isset($_POST["$name"]) ? $_POST["$name"] : "";
		if(isset($_POST["$name"]) && !empty($_POST["$name"]))
			$val = $_POST["$name"] ; 
		else if(!empty($strDefault))
			$val = $strDefault ; 
		else 
			$val = ""; 
			
		$this->HtmlForm .= '<div class="form-row row-fluid">
                                            <div class="span'.$this->iFormSize.'">
                                                <div class="row-fluid">';
		$this->HtmlForm .= '<label class="form-label span4" for="normal">'.($label).'</label>';
		
		if($this->readOnly == 0){
			$this->HtmlForm .= '<div class="span8 controls">';
			$this->HtmlForm .= "<SELECT name=\"$name\" $opt>";
		$oDb->Squery($query);
			if($requiredOption == false){
		$this->HtmlForm .= "<option value=0> => Choisissez</option>\n";
			}
		while ($sel = $oDb->GetObject())
		{
			$this->HtmlForm .= "<option value=\"".$sel->$field_value."\"";
			$this->HtmlForm .= ($sel->$field_value == $val) ? "SELECTED" : "";
			$this->HtmlForm .= ">".($sel->$field_name)."</option>\n";
		}
		$this->HtmlForm .= "</select>\n<span id=\"$ErrName\" class='texte_error'><!--$ErrName--></span>";
		}else{
			$oDb->Squery($query);
			while ($sel = $oDb->GetObject())
			{
				if($sel->$field_value == $val) {
					$this->HtmlForm .= ($sel->$field_name)."\n<span id=\"$ErrName\" class='texte_error'><!--$ErrName--></span>";
				}
			}
		}
		
		$this->HtmlForm .= ' </div></div></div></div>';
	}
	/*Ajouté le 2010/02/22*/
	/*Lecture des fichiers à partir d'un path*/
	function AddSelectPath($name, $label, $path, $field_name, $field_value, $extension="0", $opt="", $requiredOption=false)
	{
		$ErrName = $name."Error";
		$val = isset($_POST["$name"]) ? $_POST["$name"] : "";
		
			$this->HtmlForm .= "<tr>\n<td align='right'>$label</td>\n";
			if($this->readOnly == 0){
				$this->HtmlForm .="<td align='left'>\n<SELECT name=\"$name\" $opt>";
				if($requiredOption == false){
					$this->HtmlForm .= "<option value=0> => Choisissez</option>\n";
				}
				//Lecture du répertoire
				$dir = opendir($path);
		
				while($file = readdir($dir)) {
					if($file!="." && $file!=".."){	
						$file_libelle ="";
						$file_extension="";
						$strTmpFile = explode(".",$file);
						$file_value = substr($strTmpFile[0],5);
						$file_extension= $strTmpFile[1];
						if($extension==0)
							$file_libelle = $file_value;
						else
							$file_libelle = $file_value.'.'.$file_extension;
			
						$this->HtmlForm .= "<option value=\"".$file_value."\"";
						$this->HtmlForm .= ($file_value == $val) ? "SELECTED" : "";
						$this->HtmlForm .= ">".($file_libelle)."</option>\n";
					}
				}
				closedir($dir); 
				$this->HtmlForm .= "</select>\n<span id=\"$ErrName\" class='texte_error'><!--$ErrName--></span></td>\n</tr>";
			}else{
				$this->HtmlForm .= "<td>$val\n<span id=\"$ErrName\" class='texte_error'><!--$ErrName--></span></td></tr>";
			}
	}
	
	function AddRadio($name, $label, $table, $require=false, $aff="h" , $strDefault = null)
	{
		$ErrName = $name."Error";
		// $val = isset($_POST["$name"]) ? $_POST["$name"] : "";
		if(isset($_POST["$name"]) && !empty($_POST["$name"]))
			$val = $_POST["$name"] ; 
		else if(!empty($strDefault))
			$val = $strDefault ; 
		else 
			$val = ""; 
		$separator = $aff == "h" ? "&nbsp;&nbsp;" : "<br>";
		
		$this->HtmlForm .= '<div class="form-row row-fluid">
                                            <div class="span'.$this->iFormSize.'">
                                                <div class="row-fluid">';
                                                    
                                               
		$this->HtmlForm .= '<label class="form-label span4" for="normal">'.($label).'</label>';
		if ($require == true)
		{
			array_push($this->required, $name);
			$this->CheckRequire($name);
			$this->HtmlForm .= '<div class="required_tag tooltip hover left"></div>';
		}
		
		while (list($key, $text) = each($table))
		{
			$this->HtmlForm .= "<input type=\"radio\" name=\"$name\" value=\"$key\"";
			$this->HtmlForm .= ($key == $val) ? " CHECKED" : "";
			$this->HtmlForm .= " [BG".$ErrName."] ";
			if($this->readOnly == 1)
				$this->HtmlForm .= " disabled ";				
			$this->HtmlForm .= ">$text $separator\n";
		}
		$this->HtmlForm .= (($separator=="<br>") ? "" : "<br>")."<span id=\"$ErrName\" class='texte_error'>
		<!--$ErrName--></span>";
		
		
		$this->HtmlForm .= ' </div></div></div>';
	}
	
	function AddCheck($name, $label, $table, $require=false, $aff="h")
	{
		$ErrName = $name."Error";
		$val = isset($_POST["$name"]) ? $_POST["$name"] : array();
		$separator = $aff == "h" ? "&nbsp;&nbsp;" : "<br>";
		$this->HtmlForm .= "<tr>\n<td align='right' width=20%>";
		if ($require == true)
		{
			array_push($this->required, $name);
			$this->CheckRequire($name);
			$this->HtmlForm .= "*";
		}
		$this->HtmlForm .= "$label</td>\n<td align='left' width=50%>\n";
		while (list($key, $text) = each($table))
		{
			$this->HtmlForm .= "<input type=\"checkbox\" name='".$name."[]' value=\"$key\"";
			$this->HtmlForm .= (array_search($key, $val) === false) ? "" : " CHECKED";
			if($this->readOnly == 1)
				$this->HtmlForm .= " disabled ";				
			$this->HtmlForm .= ">$text $separator\n";
		}
		$this->HtmlForm .= (($separator=="<br>") ? "" : "<br>")."<span id=\"$ErrName\">".
							(isset($this->Error["$name"]) ? $this->Error["$name"] : "")."<!--$ErrName--></span>";
		$this->HtmlForm .= "\n</td>\n</tr>";
	}
	
	function AddCheckSQL($name, $label, $strSQL, $require=false, $aff="h", $bChecked = false)
	{
	
		global $oDb;
		
		$ErrName = $name."Error";
		$val = isset($_POST["$name"]) ? $_POST["$name"] : array();
		$separator = $aff == "h" ? "&nbsp;&nbsp;" : "<br>";
		$this->HtmlForm .= " <div class='form-row row-fluid'> <div class='span8'><div class='row-fluid'><label class='form-label span4' for='normal'>";
		if ($require == true)
		{
			array_push($this->required, $name);
			$this->CheckRequire($name);
			$this->HtmlForm .= "*";
		}
		$this->HtmlForm .= "$label</label><div class='span8 controls'>";
		$aResult = $oDb->queryTab($strSQL);

		foreach ($aResult AS $aCheckLine)
		{
			$this->HtmlForm .= "<input style=\"margin-bottom: 5px !important;\" type=\"checkbox\" name='".$name."[]' value=\"".$aCheckLine["id"]."\"";
			$this->HtmlForm .= ( in_array( $aCheckLine["id"] , $val ) || $bChecked)  ? " CHECKED " : " ";
			if($this->readOnly == 1)
				$this->HtmlForm .= " disabled ";				
			$this->HtmlForm .= ">".$aCheckLine["lib"]." $separator";
		}
		$this->HtmlForm .= (($separator=="<br>") ? "" : "<br>")."<span id=\"$ErrName\">".
							(isset($this->Error["$name"]) ? $this->Error["$name"] : "")."<!--$ErrName--></span>";
		$this->HtmlForm .= "</div><span id='".$name."Error' class='texte_error'><!--ouvrage_podcastError--></span> </div></div></div>";
	}
	
	function AddCheckTable($name, $label, $table, $require=false, $maxline=5)
	{
		$ErrName = $name."Error";
		$val = isset($_POST["$name"]) ? $_POST["$name"] : array();
		$this->HtmlForm .= "<tr>\n<td colspan=2><b>$label</b>";
		if ($require == true)
		{
			array_push($this->required, $name);
			$this->CheckRequire($name);
			$this->HtmlForm .= "*";
		}
		$this->HtmlForm .= "</td>\n</tr><tr>\n<td  colspan=2><table width='100%'>\n";
		$i=0;
		while (list($key, $text) = each($table))
		{
			if ($i == 0)
				$this->HtmlForm .= "<tr>";
			if ($i != 0 && $i % $maxline == 0)
				$this->HtmlForm .= "</tr><tr>";
			$this->HtmlForm .= "<td><input type=\"checkbox\" name='".$name."[]' value=\"$key\"";
			$this->HtmlForm .= (array_search($key, $val) === false) ? "" : " CHECKED";
			if($this->readOnly == 1)
				$this->HtmlForm .= " disabled ";				
			$this->HtmlForm .= ">$text $separator\n</td>";
			$i++;
		}
		if ($i != 0)
			$this->HtmlForm .= "</tr>";
		$this->HtmlForm .= "</table>";
		$this->HtmlForm .= "<br><span id=\"$ErrName\">".(isset($this->Error["$name"]) ? $this->Error["$name"] : "")."<!--$ErrName--></span>";
		$this->HtmlForm .= "\n</td>\n</tr>";
	}
	
	function AddArea($name, $label, $rows, $cols, $require=false, $opt = "")
	{
		$ErrName = $name."Error";
		$val = isset($_POST["$name"]) ? $_POST["$name"] : "";
		
		$this->HtmlForm .= '<div class="form-row row-fluid">
                                            <div class="span'.$this->iFormSize.'">
                                                <div class="row-fluid">';
		$this->HtmlForm .= '<label class="form-label span4" for="normal">'.($label).'</label>';
		
		$this->HtmlForm .= '<div class="span8 controls">';
		$this->HtmlForm .= "<textarea name=\"$name\" cols=\"$cols\" rows=\"$rows\" class=\"uniform $opt\" ";
		if (isset($this->Error["$name"]))
			$this->HtmlForm .= "style='background:".$this->ErrorColor.";' ";
		if($this->readOnly == 1)
			$this->HtmlForm .= " disabled ";				
		$this->HtmlForm .= ">$val</textarea>";
		$this->HtmlForm .= "<br><span id=\"$ErrName\">".
							(isset($this->Error["$name"]) ? $this->Error["$name"] : "")."</span>";
		if ($require == true)
		{
			array_push($this->required, $name);
			$this->CheckRequire($name);
			$this->HtmlForm .= '<div class="required_tag tooltip hover left"></div>';
		}
		$this->HtmlForm .= ' </div></div></div></div>';
	}
	
	function Validate($value = "Valider")
	{
		global $_POST;
		global $_GET;
		global $_REQUEST;
		global $oDb;
		
		// $_POST = $oDb->secureData($_POST); 
		// $_GET = $oDb->secureData($_GET); 
		// $_REQUEST = $oDb->secureData($_REQUEST); 
			$this->HtmlForm .= $this->FormHide;	
		if (isset($_POST[$this->FormName]))
		{
			$CheckRule = true;
			$func = $this->FormRule;
			if (isset($func))
				$CheckRule = $func($this, $_POST);
			if (count($this->Error) == 0 && $CheckRule == true)
			{
				$this->Data = $_POST;
				return true;
			}
			else
			{
				if($this->readOnly == 0){
				$this->HtmlForm .= '<div class="clear form-actions">
                                           <button type="submit" class="btn btn-info" name="'.$this->FormName.'" value="'.$value.'">'.$value.'</button>
                                           <button type="button" onclick="window.history.back()" class="btn">Cancel</button>
                                        </div>';
				}
				return false;
			}
		}
		if($this->readOnly == 0){
			
			$this->HtmlForm .= '<div class="clear form-actions">
                                           <button type="submit" class="btn btn-info" name="'.$this->FormName.'" value="'.$value.'">'.$value.'</button>
                                           <button type="button" onclick="window.history.back()" class="btn">Cancel</button>
                                        </div>';
		}
		return false;
	}
	
	function AddScript($strScript){
		$this->HtmlForm .= $strScript;
	}
	
	function AddLabel($name,$label,$text, $bHidden = true)
	{
		//$ErrName = $name."Error";
		$DateStr="";
		$val = isset($_POST["$name"]) ? $_POST["$name"] : "";		
		$this->HtmlForm .= "<div class='form-row row-fluid'> <div class='span8'><div class='row-fluid'><label class='form-label span4' for='normal'>";
		$this->HtmlForm .= ($label)."</label><div class='span8 controls' style=' line-height: 35px;'>";
		$this->HtmlForm .= " $text ";
		$this->HtmlForm .= "</div> </div></div></div>";
		if($bHidden)
			$this->AddHidden($name, $text);
	}
}
?>