<?php
session_start();
if($_SERVER['SERVER_NAME']=='localhost')
	$_CONST['path']["server"] = $_SERVER['DOCUMENT_ROOT']."/lenigme/";
else
	$_CONST['path']["server"] = $_SERVER['DOCUMENT_ROOT']."/";
include_once($_CONST['path']["server"]."/lib/config.inc.php");
include_once($_CONST['path']["server"]."/lib/database.class.php");
	
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	
	if (!empty($_POST["path"]) && !empty($_POST["form_fieldName"]) ) {
		/*configuration*/
		$uploaddir = $_POST["path"];
		$field = $_POST["form_fieldName"];
		$iImgWidth = $_POST["imgWidth"] ? $_POST["imgWidth"] : 100;
		$uploadfile = $uploaddir."tmp_".basename($_FILES["$field"]['name']);
		
		/* Get extension file */
		$aFilesParts = explode('.', $_FILES["$field"]['name']);
		$aFilesParts = array_reverse($aFilesParts);
		$strExtension = $aFilesParts[0];
		
		/* check max number files MaxFiles */
		//TODO
		$oDb->Squery("	SELECT files_id 
						FROM ".$_POST["savetable"]." 
						WHERE
							files_table_source='".$_POST["savefieldtable"]."' 
							AND files_table_id_name='".$_POST["savefieldPK"]."'
							AND files_field_name='".$_POST["form_fieldName"]."' 
							AND files_table_id='".$_POST["id"]."'");
		
		if ($oDb->rows >= $_POST["MaxFiles"])
			echo "[error]: :<img src='images/error.png' alt='Erreur - '> Limite de fichiers atteinte.";
		else {
			/*lets go to action :)*/
			
			
			if (move_uploaded_file($_FILES["$field"]['tmp_name'], $uploadfile)) {
				if (!empty($_POST["savetable"]) && !empty($_POST["savefieldtable"]) && !empty($_POST["savefieldPK"]) && !empty($_POST["form_fieldName"])) {
					$oDb->Squery("INSERT INTO ".$_POST["savetable"]." VALUES ('', '".$uploadfile."', '".$_FILES["$field"]['name']."', '".$_POST["savefieldtable"]."', '".$_POST["savefieldPK"]."', '".$_POST["form_fieldName"]."', '".$_POST["id"]."', NOW())");

					$iFileID = $oDb->last_insert_id;
					$_SESSION['iFileID'] = $iFileID;
					
					$strNewFileName = $iFileID.'.'.$strExtension;
					
					if (!@copy($uploadfile, $uploaddir.$strNewFileName))
						echo "[error]: :<img src='images/error.png' alt='Erreur - '> Renomage impossible (copy).";
					else {
						$oDb->Squery("UPDATE ".$_POST["savetable"]." SET files_path='".($uploaddir.$strNewFileName)."' WHERE files_id='$iFileID'");
						//echo "UPDATE ".$_POST["savetable"]." SET files_name='$strNewFileName', files_path='".($uploaddir.$strNewFileName)."' WHERE files_id='$iFileID'";
						if (!@unlink($uploadfile))
							echo "[error]: :<img src='images/error.png' alt='Erreur - '> Renomage impossible (unlink).";
					}
				}
				if ($_POST["typeOfFile"] == "img")
					echo "[OK]:<img src='".($uploaddir.$strNewFileName)."' width='$iImgWidth'><a href='".$_POST['returnURL']."&del_files_id=$iFileID'><img src='images/delete.png' alt='[suppr.]'></a>:<img src='images/add.png' alt='OK - '> Fichier envoyé.";
				else
					echo "[OK]:<a href='".($uploaddir.$strNewFileName)."'>".$_FILES["$field"]['name']."</a>:<img src='images/add.png' alt='OK - '> Fichier envoyé.";
			} else {
			  echo "[error]: :<img src='images/error.png' alt='Erreur - '> Copie impossible<br>";
			}
		}
	}
	else 
		echo "[error]::<img src='images/error.png' alt='Erreur - '> Pas de path ou de nom transmit.";
	$oDb->Close();

?>