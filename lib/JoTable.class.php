<?php

/**
 * Classe permettant de g�rer l'affichage de tableau pagin�s
 * 
 * <p></p>
 * 
 * @name JoTable
 * @author MIZRAHI Johan <johan.mizrahi@businessdecision.com> 
 * @copyright Business and decision 2009
 * @version 1.0.0 (10-04-2009)
 */
 
 class JoTable {
 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. propri�t�s    */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * @var array
    * @desc collonne a affich�e
    */
    public $aCols;
    
    /**
    * @var integer
    * @desc nombre de colonne
    */
    public $iColsNumber = 0;
    
    /**
    * @var array
    * @desc actions a affich�e
    */
    public $aAct;
    
    /**
    * @var array
    * @desc filtres � afficher
    */
    public $aFilter;
    
    /**
    * @var integer
    * @desc nombre d'actions
    */
    public $iActNumber = 0;
    
    /**
    * @var integer
    * @desc nombre de filtre
    */
    public $iFilterNumber = 0;
    
    /**
    * @var integer
    * @desc nombre de colonne pour l'affichage filtre
    */
    public $iFilterColNumber = 4;
    
    /**
    * @var integer
    * @desc taille des icon d'action en pixel
    */
    public $iIconSize= 32;
    
    /**
    * @var string
    * @desc nom du tableau
    */
    public $strName;
    
    /**
    * @var string
    * @desc path vers le rep CSS
    */
    public $strCSSPath = "css/ecomiz/";
   
    /**
    * @var string
    * @desc path vers le rep CSS
    */
    public $strCSSfield;
    
    /**
    * @var string
    * @desc CSS class par defaut
    */
    public $strCSSdefaultClass = "gradeC";
    
    /**
    * @var array
    * @desc table de class conditionnelle
    */
    public $aCSSClass;
    
     /**
    * @var string
    * @desc path vers le rep JS
    */
    public $strJSPath = "lib/media/js/";

    /**
    * @var boolean
    * @desc option pour la recherche multiple (par colonne).
    */
	public $bOptMultiSearch = true;
	
	/**
    * @var boolean
    * @desc option pour la selection (surbrillance) des lignes.
    */
	public $bOptRowSelect = true;
	
	/**
    * @var boolean
    * @desc option de mise en cache. evite de relancer les requetes en cas de rechargement de page.
    */
	public $bOptCache = false;
	
	
	/**
    * @var boolean
    * @desc option de sauvegarde de la recherche.
    */
	public $bOptStateSave = true;

	/**
    * @var boolean
    * @desc option de la value de choose dans une combo
    */
	public $bOptChooseValue = true;
	
	/**
    * @var boolean
    * @desc option pour le lien des action, si true : REQUEST_URI si false PHP_SELF
    */
	public $bGetFullURLAction = false;
	
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name JoTable::__construct()
    * @return void 
    */
    public function __construct($strName) {
 		$this->strName = $strName;
    } 
    
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    /*  2.1 m�thodes priv�es   */
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/ 
    
	
    private function ShowTableHeader($iRows = 10 , $strClass = "dynamicTable", $iCol = 0, $strSort = "asc") {
	
    	echo "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" rel=\"$iRows\"   col=\"$iCol\"  sort=\"$strSort\"  class=\"responsive $strClass display table table-bordered\">";
    	echo "<thead><tr>";
		foreach ($this->aCols as $aheaders) {
			if (!empty($aheaders['size']))
				$strWidth = "width='".$aheaders['size']."'";
			else 
				$strWidth = "";
			echo "<th $strWidth style='text-align:".$aheaders['align']."'>".$aheaders['libelle']."</th>";
		}
		if ($this->iActNumber > 0)
		 echo "<th style='width: ".(($this->iIconSize + 5) * $this->iActNumber)."px !important;'></th>";
		echo "</tr></thead>";
    }
    
    private function ShowTableBody($aData) {
    	echo "<tbody>";
		foreach($aData as $aValue) {
			if (!empty($this->strCSSfield) && !empty($this->aCSSClass)) {
				$strClass = $this->aCSSClass[$aValue[$this->strCSSfield]];
			}
			else
				$strClass = $this->strCSSdefaultClass;
			$strBufferLine = "<tr class=\"$strClass\">";
			foreach ($this->aCols as $aheaders) {
				if (!empty($aheaders['size']))
					$strWidth = "width='".$aheaders['size']."'";
				else 
					$strWidth = "";
				$strBufferLine .= "<td $strWidth style='text-align:".$aheaders['align']."'>".$aValue[$aheaders['name']]."</td>";
			}
			
			echo $strBufferLine;
			if ($this->iActNumber > 0) {
				echo "<td>";
				foreach ($this->aAct as $aAct) {
						$strLink = "<a href='".($this->bGetFullURLAction===true ? $_SERVER["REQUEST_URI"]."&" : $_SERVER['PHP_SELF']."?");
	
					foreach ($aAct["param"] as $key => $value) {
						//	if (!ereg("\^_*+_$", $key) && !empty($aValue[$value]) && !empty($key))
						if (!preg_match("/^_$/", $key) && !empty($aValue[$value]) && !empty($key))
							$strLink .= "$key=".$aValue[$value]."&";
					}
					if (!empty($aAct["param"]["_all_"]))
						$strLink .= $aAct["param"]["_all_"];
					$strLink .= "' ";
					if (!empty($aAct["param"]["_JSfct_"])) {
						$strLink .= $aAct["param"]["_JSfct_"]["event"]."='".$aAct["param"]["_JSfct_"]["function"]."(";
						foreach ($aAct["param"]["_JSfct_"]["params"] AS $key => $strFieldName) {
							$strLink .= $aData[$strFieldName].",";
						}
						$strLink = substr($strLink, 0, -1).")'";
					}
					if (!empty($aAct["param"]["_option_"]))
						$strLink .= $aAct["param"]["_option_"];
					echo $strLink." title='".$aAct["libelle"]."'>";
					
					if (!empty($aAct["param"]["_classicon_"]))
					 echo "<span class=\"icon16 ".$aAct["param"]["_classicon_"]."\"></span>";
					else if (!empty($aAct["param"]["_image_"]))
						echo "<img border=0 src='".$aAct["param"]["_image_"]."' style='float: left;'>";
					else
						echo $aAct["libelle"];
					echo "</a>";
				}
				echo "</td>";
			}
			echo "</tr>";
		}
		echo "</tbody>";
    }
    
    private function ShowTableFooter() {
    	
		echo "</table>";
    }
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    /*  2.1 m�thodes publiques */
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * PrintHeaders - Affiche les headers n�cessaire au fonctionnement de la classe.
    * 
    * <p></p>
    * 
    * @name JoTable::PrintHeaders()
    * @return void 
    */
    public function PrintHeaders($strCol="0",$strTri="asc") {
    	global $_CONST;
    	
    	
		
    }
	
	/**
    * setCSS - D�fini le fichier CSS a utiliser.
    * 
    * <p></p>
    * 
    * @name JoTable::setCSS()
    * @param string - nom du champs qui doit �tre pr�sente dans la requete ou tableau
    * @param array - tableau de class conditionnelle (en fonction de la valeur du champs)
    * @return void 
    */
    public function setCSS($strCSSfield = "", $aClass = "") {
    	$this->strCSSfield = $strCSSfield;
    	$this->aCSSClass = $aClass;
    }
    
    /**
    * addColumn - Ajoute des colonnes au tableau.
    * 
    * <p></p>
    * 
    * @name JoTable::addColumn()
    * @param string - nom affich� de la colonne
    * @param string - nom du champ dans la requete
    * @param integer - largeur de la colonne
    * @param string - alignement des donn�es dans la colonne.
    * @return void 
    */
    public function addColumn($strColLib, $strColName, $iSize=0, $strAlign="center") {
    	
    	$this->aCols[$this->iColsNumber]['libelle'] = $strColLib;
    	$this->aCols[$this->iColsNumber]['name'] = $strColName;
    	$this->aCols[$this->iColsNumber]['size'] = $iSize;
    	$this->aCols[$this->iColsNumber]['align'] = $strAlign;
    	$this->iColsNumber++;
    }
    
    /**
    * addAction - Ajoute des actions sur chaque lignes
    * 
    * <p></p>
    * 
    * @name JoTable::addAction()
    * @param string - libelle
    * @param array - parametre
    * @param string - events javascript
    * @return void 
    */
    public function addAction($strActLib, $aActParam, $strActJS = "") {
    	
    	$this->aAct[$this->iActNumber]['libelle'] = $strActLib;
    	$this->aAct[$this->iActNumber]['param'] = $aActParam;
    	$this->aAct[$this->iActNumber]['js'] = $strActJS;
    	$this->iActNumber++;
    }
  	
    /**
    * ShowTable - Affiche le tableau automatiqment.
    * 
    * <p>affiche toute les donn�es d'une table SQL. le nom du tableau doit etre le nom de la table.</p>
    * 
    * @name JoTable::ShowTable()
    * @return void 
    */
    public function ShowTable() {
    	global $oDb;
    	
    	$strSQL = "SHOW FULL FIELDS FROM ".$this->strName;
    	$aResults = $oDb->queryTab($strSQL);
    	foreach ($aResults as $aValue) {
    		$this->addColumn($aValue["Field"], $aValue["Field"]);
    	}
    	$strSQL = "SELECT * FROM ".$this->strName;
    	$this->ShowTableFromSQL($strSQL);
    }
    
    /**
    * ShowTableFromArray - Affiche le tableau a partir d'un array.
    * 
    * <p></p>
    * 
    * @name JoTable::ShowTableFromArray()
    * @param string - tableau de donn�e
    * @return void 
    */
    public function ShowTableFromArray($aData, $iRows=10 , $strClass = "dynamicTable", $iCol = 0, $strSort = "asc") {
    	$this->ShowTableFilter();
    	$this->ShowTableHeader($iRows, $strClass, $iCol, $strSort);
    	if ($this->bOptCache === true) {
	    	if (empty($_SESSION['JoTable_Data_'.$this->strName]))
				$_SESSION['JoTable_Data_'.$this->strName] = $aData;
			$aData = $_SESSION['JoTable_Data_'.$this->strName];
    	}
    	$this->ShowTableBody($aData);
    	$this->ShowTableFooter();
		echo '
				<script type="text/javascript" src="./scripts/DataTables/jquery.dataTables.js"></script>
				<script type="text/javascript" src="./scripts/adminica/adminica_datatables.js"></script>';
    }
    
    /**
    * ShowTableFromSQL - Affiche le tableau a partir d'une requete sql.
    * 
    * <p></p>
    * 
    * @name JoTable::ShowTable()
    * @param string - requete SQL
    * @return void 
    */
    public function ShowTableFromSQL($strSQL,$iRows=50, $strClass = "dynamicTable", $iCol = 0, $strSort = "asc") {
    	global $oDb;	
    
    	$this->ShowTableHeader($iRows, $strClass, $iCol, $strSort);
    	$aData = array();
    	if ($this->bOptCache === true) {
    		if (empty($_SESSION['JoTable_Data_'.$this->strName]))
				$_SESSION['JoTable_Data_'.$this->strName] = $oDb->queryTab($strSQL);
			$aData = $_SESSION['JoTable_Data_'.$this->strName];
    	}
    	else
    		$aData  = $oDb->queryTab($strSQL);
		$this->ShowTableBody($aData);
		$this->ShowTableFooter();	
		
    }
    
    /**
    * Destructeur
    * 
    * <p>Destruction de l'instance de classe</p>
    * 
    * @name JoTable::__destruct()
    * @return void
    */
    public function __destruct() {
    }
 }
 ?> 
