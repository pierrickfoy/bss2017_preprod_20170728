<?php

/**
 * Classe permettant de g�rer l'affichage g�nrale de la trame du site (css, module etc...)
 * 
 * <p>d�tail de la classe</p>
 * 
 * @name Jo_interface
 * @author MIZRAHI Johan <johan@cyber-cafete.com> 
 * @link http://www.ecomiz.fr
 * @copyright Johan MIZRAHI 2009
 * @version 1.0.0 (23-03-2009)
 */
 
 class Jo_interface {
 
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  1. propri�t�s    */
    /*~*~*~*~*~*~*~*~*~*~*/
    
  
    /**
     * @var string
     * @desc nom de la boutique
     */
    public $strBoutiqueName;
    
     /**
     * @var int
     * @desc ID de la boutique
     */
    public $iBoutiqueId;
    /**
     * @var string
     * @desc type de menu pour le catalogue. h = horizotal, v = verticale (ds module)
     */
    public $strMenuType = 'none';
    
     /**
     * @var string
     * @desc skin
     */
    public $strSkinName;
    
     /**
     * @var string
     * @desc nom du template
     */
    public $strTemplateName;
    
     /**
     * @var string
     * @desc nom du template
     */
    public $strTemplateTitle;
    
     /**
     * @var string
     * @desc path vers le template
     */
    public $strTemplatePath;
    
     /**
     * @var integer
     * @desc id du template
     */
    public $iTemplateId;
    
     /**
     * @var string
     * @desc path vers le repertoire ou ce trouve les fichiers de langues.
     */
    public $strLangPath = "./lang/";
    
     /**
     * @var string
     * @desc nom de la langues par defaut.
     */
    public $strDefaultLanguage = "fr";
    
     /**
     * @var integer
     * @desc identifiant de la langue de l'utilisateur
     */
    public $iLangId;
    
     /**
     * @var array
     * @desc tableau des codes erreurs
     */
    public $aErrorsCodes = array();
    
    
    /*~*~*~*~*~*~*~*~*~*~*/
    /*  2. m�thodes      */
    /*~*~*~*~*~*~*~*~*~*~*/
    
    /**
    * Constructeur
    * 
    * <p>cr�ation de l'instance de la classe</p>
    * 
    * @name Jo_interface::__construct()
    * @param integer - id de la langue choisi
    * @return void 
    * @todo recuperer les variable de facon dynamique en fonction du domaine
    */
    public function __construct() {
    	global $oDb;
    	global $_CONST;
    	
    	$this->strBoutiqueName = "EcomiZ";
    	$this->strMenuType = 'h';  
		$this->iBoutiqueId = 1;
    } 
    
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    /*  2.1 m�thodes priv�es   */
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/ 
    /**
    * GetModule  - Retourne les modules de l'interface de navigation.
    * 
    * <p>Retourne une chaine au format HTML contenant le module demand�.</p>
    * 
    * @name Jo_interface::GetModule()
    * @param string - nom du module
    * @param string - titre du module
    * @return void
    */
    private function GetModule($strModName, $strModTitle) {
    	global $_CONST;
    	global $oDb;
    	global $aLang;
    	
    	echo "<div class=\"widget\">
    			<div class=\"ui-widget-header ui-corner-all\">
					<div class=\"ui-state-default ui-corner-tr ui-corner-tl\">
						<div id=\"mod_$strModName\"><strong>$strModTitle</strong></div>
					</div>
					<div class=\"ui-widget-content\">";
    	$filename = $_CONST["path"]["modules"]."mod_".$strModName.".php";
    	if (file_exists($filename))
    		include($_CONST["path"]["modules"]."mod_".$strModName.".php");
    	echo "</div>\n</div>\n</div>";
    }
    
	/**
    * languages - retourne un tableau des langues.
    * 
    * <p></p>
    * 
    * @name Jo_interface::languages()
    * @return array
    */
	private function languages()
	{
		// pack abbreviation/language array
		// important note: you must have the default language as the last item in each major language, after all the
		// en-ca type entries, so en would be last in that case
		$a_languages = array(
		'af' => 'Afrikaans',
		'sq' => 'Albanian',
		'ar-dz' => 'Arabic (Algeria)',
		'ar-bh' => 'Arabic (Bahrain)',
		'ar-eg' => 'Arabic (Egypt)',
		'ar-iq' => 'Arabic (Iraq)',
		'ar-jo' => 'Arabic (Jordan)',
		'ar-kw' => 'Arabic (Kuwait)',
		'ar-lb' => 'Arabic (Lebanon)',
		'ar-ly' => 'Arabic (libya)',
		'ar-ma' => 'Arabic (Morocco)',
		'ar-om' => 'Arabic (Oman)',
		'ar-qa' => 'Arabic (Qatar)',
		'ar-sa' => 'Arabic (Saudi Arabia)',
		'ar-sy' => 'Arabic (Syria)',
		'ar-tn' => 'Arabic (Tunisia)',
		'ar-ae' => 'Arabic (U.A.E.)',
		'ar-ye' => 'Arabic (Yemen)',
		'ar' => 'Arabic',
		'hy' => 'Armenian',
		'as' => 'Assamese',
		'az' => 'Azeri',
		'eu' => 'Basque',
		'be' => 'Belarusian',
		'bn' => 'Bengali',
		'bg' => 'Bulgarian',
		'ca' => 'Catalan',
		'zh-cn' => 'Chinese (China)',
		'zh-hk' => 'Chinese (Hong Kong SAR)',
		'zh-mo' => 'Chinese (Macau SAR)',
		'zh-sg' => 'Chinese (Singapore)',
		'zh-tw' => 'Chinese (Taiwan)',
		'zh' => 'Chinese',
		'hr' => 'Croatian',
		'cs' => 'Czech',
		'da' => 'Danish',
		'div' => 'Divehi',
		'nl-be' => 'Dutch (Belgium)',
		'nl' => 'Dutch (Netherlands)',
		'en-au' => 'English (Australia)',
		'en-bz' => 'English (Belize)',
		'en-ca' => 'English (Canada)',
		'en-ie' => 'English (Ireland)',
		'en-jm' => 'English (Jamaica)',
		'en-nz' => 'English (New Zealand)',
		'en-ph' => 'English (Philippines)',
		'en-za' => 'English (South Africa)',
		'en-tt' => 'English (Trinidad)',
		'en-gb' => 'English (United Kingdom)',
		'en-us' => 'English (United States)',
		'en-zw' => 'English (Zimbabwe)',
		'en' => 'English',
		'us' => 'English (United States)',
		'et' => 'Estonian',
		'fo' => 'Faeroese',
		'fa' => 'Farsi',
		'fi' => 'Finnish',
		'fr-be' => 'French (Belgium)',
		'fr-ca' => 'French (Canada)',
		'fr-lu' => 'French (Luxembourg)',
		'fr-mc' => 'French (Monaco)',
		'fr-ch' => 'French (Switzerland)',
		'fr' => 'French (France)',
		'mk' => 'FYRO Macedonian',
		'gd' => 'Gaelic',
		'ka' => 'Georgian',
		'de-at' => 'German (Austria)',
		'de-li' => 'German (Liechtenstein)',
		'de-lu' => 'German (Luxembourg)',
		'de-ch' => 'German (Switzerland)',
		'de' => 'German (Germany)',
		'el' => 'Greek',
		'gu' => 'Gujarati',
		'he' => 'Hebrew',
		'hi' => 'Hindi',
		'hu' => 'Hungarian',
		'is' => 'Icelandic',
		'id' => 'Indonesian',
		'it-ch' => 'Italian (Switzerland)',
		'it' => 'Italian (Italy)',
		'ja' => 'Japanese',
		'kn' => 'Kannada',
		'kk' => 'Kazakh',
		'kok' => 'Konkani',
		'ko' => 'Korean',
		'kz' => 'Kyrgyz',
		'lv' => 'Latvian',
		'lt' => 'Lithuanian',
		'ms' => 'Malay',
		'ml' => 'Malayalam',
		'mt' => 'Maltese',
		'mr' => 'Marathi',
		'mn' => 'Mongolian (Cyrillic)',
		'ne' => 'Nepali (India)',
		'nb-no' => 'Norwegian (Bokmal)',
		'nn-no' => 'Norwegian (Nynorsk)',
		'no' => 'Norwegian (Bokmal)',
		'or' => 'Oriya',
		'pl' => 'Polish',
		'pt-br' => 'Portuguese (Brazil)',
		'pt' => 'Portuguese (Portugal)',
		'pa' => 'Punjabi',
		'rm' => 'Rhaeto-Romanic',
		'ro-md' => 'Romanian (Moldova)',
		'ro' => 'Romanian',
		'ru-md' => 'Russian (Moldova)',
		'ru' => 'Russian',
		'sa' => 'Sanskrit',
		'sr' => 'Serbian',
		'sk' => 'Slovak',
		'ls' => 'Slovenian',
		'sb' => 'Sorbian',
		'es-ar' => 'Spanish (Argentina)',
		'es-bo' => 'Spanish (Bolivia)',
		'es-cl' => 'Spanish (Chile)',
		'es-co' => 'Spanish (Colombia)',
		'es-cr' => 'Spanish (Costa Rica)',
		'es-do' => 'Spanish (Dominican Republic)',
		'es-ec' => 'Spanish (Ecuador)',
		'es-sv' => 'Spanish (El Salvador)',
		'es-gt' => 'Spanish (Guatemala)',
		'es-hn' => 'Spanish (Honduras)',
		'es-mx' => 'Spanish (Mexico)',
		'es-ni' => 'Spanish (Nicaragua)',
		'es-pa' => 'Spanish (Panama)',
		'es-py' => 'Spanish (Paraguay)',
		'es-pe' => 'Spanish (Peru)',
		'es-pr' => 'Spanish (Puerto Rico)',
		'es-us' => 'Spanish (United States)',
		'es-uy' => 'Spanish (Uruguay)',
		'es-ve' => 'Spanish (Venezuela)',
		'es' => 'Spanish (Traditional Sort)',
		'sx' => 'Sutu',
		'sw' => 'Swahili',
		'sv-fi' => 'Swedish (Finland)',
		'sv' => 'Swedish',
		'syr' => 'Syriac',
		'ta' => 'Tamil',
		'tt' => 'Tatar',
		'te' => 'Telugu',
		'th' => 'Thai',
		'ts' => 'Tsonga',
		'tn' => 'Tswana',
		'tr' => 'Turkish',
		'uk' => 'Ukrainian',
		'ur' => 'Urdu',
		'uz' => 'Uzbek',
		'vi' => 'Vietnamese',
		'xh' => 'Xhosa',
		'yi' => 'Yiddish',
		'zu' => 'Zulu' );
	
		return $a_languages;
	}
	
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/
    /*  2.1 m�thodes publiques */
    /*~*~*~*~*~*~*~*~*~*~*~*~*~*/

	/**
    * SetError - gestion des erreurs de l'interface.
    * 
    * <p>permet d'afficher et/ou de loguer tous type d'erreur de l'interface.</p>
    * 
    * @name Jo_interface::SetError()
    * @param integer - Code erreur ex: 403
    * @param boolean - true or false : show error message
    * @param boolean - true or false : log error message
    * @return void 
    * @todo ajouter vrai affichage + log
    */
	public function SetError($iCodeError, $bShowit, $bLogit = false) {

		if ($bShowit === true) {
			echo "<div class=\"ui-widget\">
					<div style=\"padding: 0pt 0.7em;\" class=\"ui-state-error ui-corner-all\"> 
						<p><span style=\"float: left; margin-right: 0.3em;\" class=\"ui-icon ui-icon-alert\"></span> 
						<strong>Alert:</strong> ERROR - Code $iCodeError : ".$this->aErrorsCodes[$iCodeError]."</p>
					</div>
				</div>";
		}
	}
	
 /**
    * ShowInfo - affiche les information de l'interface.
    * 
    * <p>Permet d'afficher les messages de l'interface avec Jquery UI theme.</p>
    * 
    * @name Jo_interface::ShowInfo()
    * @param string - strong message
    * @param string - message
    * @return void 
    */
	public function ShowInfo($strStrongMsg, $strMessage) {
		echo "<div class=\"ui-widget\">
				<div style=\"padding: 0pt 0.7em; margin-top: 20px;\" class=\"ui-state-highlight ui-corner-all\"> 
					<p><span style=\"float: left; margin-right: 0.3em;\" class=\"ui-icon ui-icon-info\"></span>
					<strong>$strStrongMsg</strong>  $strMessage</p>
				</div>
			</div>";
	}
   
      
	/**
    * GetLanguages - retourne la langue de l'utilisateur ou inclus le fichier de lang.
    * 
    * <p>choice of redirection header or just getting language data. 
    * call this you only need to use the $feature parameter.</p>
    * 
    * @name Jo_interface::GetLanguages()
    * @param string - 'data' (retourne les langages users) ou 'header' (inclu le fichier langue)
    * @return string 
    */
	public function GetLanguages($feature, $strForceLanguage = "")
	{
		global $_SESSION;
		
		// get the languages
		$a_languages = $this->languages();
		$index = '';
		$complete = '';
		$found = false;// set to default value
		//prepare user language array
		$user_languages = array();
	
		//check if user want to force language
		if (empty($strForceLanguage))
		{
			//check to see if language is set
			if ( isset( $_SERVER["HTTP_ACCEPT_LANGUAGE"] ) )
			{
				$languages = strtolower( $_SERVER["HTTP_ACCEPT_LANGUAGE"] );
				// $languages = ' fr-ch;q=0.3, da, en-us;q=0.8, en;q=0.5, fr;q=0.3';
				// need to remove spaces from strings to avoid error
				$languages = str_replace( ' ', '', $languages );
				$languages = explode( ",", $languages );
				//$languages = explode( ",", $test);// this is for testing purposes only
		
				foreach ( $languages as $language_list )
				{
					// pull out the language, place languages into array of full and primary
					// string structure:
					$temp_array = array();
					// slice out the part before ; on first step, the part before - on second, place into array
					$temp_array[0] = substr( $language_list, 0, strcspn( $language_list, ';' ) );//full language
					$temp_array[1] = substr( $language_list, 0, 2 );// cut out primary language
					//place this array into main $user_languages language array
					$user_languages[] = $temp_array;
				}
		
				//start going through each one
				for ( $i = 0; $i < count( $user_languages ); $i++ )
				{
					foreach ( $a_languages as $index => $complete )
					{
						if ( $index == $user_languages[$i][0] )
						{
							// complete language, like english (canada)
							$user_languages[$i][2] = $complete;
							// extract working language, like english
							$user_languages[$i][3] = substr( $complete, 0, strcspn( $complete, ' (' ) );
						}
					}
				}
			}
			else// if no languages found
			{
				$user_languages[0] = array( '','','','' ); //return blank array.
			}
			 //print_r($user_languages);
			// return parameters
			if ( $feature == 'data' )
			{
				return $user_languages;
			}
		
			// this is just a sample, replace target language and file names with your own.
			elseif ( $feature == 'header' )
			{
				$_SESSION['langage'] = $user_languages[0][1] != "en" ? "fr" : "en";
				switch ( $user_languages[0][1] )// get default primary language, the first one in array that is
				{
					case 'en':
						$location = 'en.lang.php';
						$found = true;
						break;
					case 'fr':
						$location = 'fr.lang.php';
						$found = true;
						break;
					default:
						$location = 'fr.lang.php';
						break;
				}
	
				if (file_exists($this->strLangPath.$location))
					return($this->strLangPath.$location);
				else
					return($this->strLangPath.$this->strDefaultLanguage.".lang.php");
			}
		}
		else {
			if ( $feature == 'data' )
				return $strForceLanguage;
			elseif ( $feature == 'header' )
				$location = $strForceLanguage.".lang.php";
			if (file_exists($this->strLangPath.$location))
				return($this->strLangPath.$location);
			else
				return($this->strLangPath.$this->strDefaultLanguage.".lang.php");
		}
	}
    
    /**
    * GetHtmlMeta - Retourne les headers HTML.
    * 
    * <p>Retourne les metatags (header) au format html.
    * Le titre et de la description de la page en fonction du template � afficher.
    * Le copiright et autre informations en fonctions de la configuration de la boutique.</p>
    * 
    * @name Jo_interface::GetHtmlMeta()
    * @param integer - id du produit si on est sur un template produit
    * @return string 
    */
    public function GetHtmlMeta($iProductId=0) {
    	global $oDb;
    	global $_CONST;
    	
    	$strHtmlHeaders = "";
    	$strSql = "SELECT template_title, template_keywords, template_abstract, template_desc 
    				FROM ".$_CONST["tables"]["template"]." 
    				WHERE template_id='".mysql_real_escape_string($this->iTemplateId)."'";
    	$oDb->query($strSql);
    	if ($oDb->rows == 1) {
    		if (!empty($iProductId)) {
    			$oDb->Squery("SELECT products_libelle, products_keywords, products_text 
    						FROM ".$_CONST["tables"]["products"]."
    						WHERE products_id='".mysql_real_escape_string($iProductId)."'");
    			$oProduct = $oDb->GetObject();
    			$oDb->data["template_keywords"][0] = $oProduct->products_keywords;
    			$oDb->data["template_desc"][0] = $oProduct->products_text;
    			$oDb->data["template_title"][0] = "Fiche produit : ".$oProduct->products_libelle;
    		}
    		
    		$strHtmlHeaders = "
    			<meta http-equiv=\"Content-Type\" content=\"text/html;charset=iso-8859-1\" />
		    	<meta name=\"publisher\" content=\"EcomiZ\" />\n
		    	<meta name=\"author\" content=\"EcomiZ\" />\n
				<meta name=\"copyright\" content=\"EcomiZ\" />\n
				<meta name=\"generator\" content=\"ZendStudio\" />\n
				<meta name=\"revisit-after\" content=\"1 Days\" />\n
				<meta name=\"keywords\" content=\"".$oDb->data["template_keywords"][0]."\" />\n
				<meta name=\"description\" content=\"".$oDb->data["template_desc"][0]."\" />\n
				<meta name=\"abstract\" content=\"".$oDb->data["template_abstract"][0]."\" />\n
				<title>".$oDb->data["template_title"][0]." | ".$this->strBoutiqueName."</title>\n";
    	}
    	echo $strHtmlHeaders;
    }
    
    /**
    * GetSkin  - Retourne l'inclusion des CSS.
    * 
    * <p>Retourne une chaine au format HTML contenant les CSS relatifs au skin demand�.</p>
    * 
    * @name Jo_interface::GetSkin()
    * @return string 
    */
    public function GetSkin() {
    	global $_CONST;
    	global $oDb;
    	
    	$oDb->query("	SELECT boutique_skin 
    					FROM ".$_CONST["tables"]["boutique"]."
    					WHERE boutique_id='".$this->iBoutiqueId."'");
    	$strHtmlCss = "<link rel=\"stylesheet\" type=\"text/css\" href=\"".$_CONST["path"]["css"]."skin_".$oDb->data["boutique_skin"][0].".css\">\n";
    	$this->strSkinName = "skin_".$oDb->data["boutique_skin"][0];
    	echo $strHtmlCss;
    }
    
    /**
    * SetTemplate  - Retourne le temlate et son path � affciher.
    * 
    * <p>Retourne un tableau contenant le nom du template ainsi que son path.
    * array("tpl" => [name], "tplPath" => [Path])</p>
    * 
    * @name Jo_interface::SetTemplate()
    * @param string - nom du template
    * @return void
    */
    public function SetTemplate($strTpl) {
    	global $_CONST;
    	global $oDb;
    	
    	if (empty($strTpl))
    		$strTpl = "accueil";
    	$strTpl = mysql_real_escape_string($strTpl);
    	$oDb->query("	SELECT template_id, template_title_fr 
    					FROM ".$_CONST["tables"]["template"]."
    					WHERE template_libelle='".$strTpl."'");
    	
		$strTemplate = $_CONST['path']['templates']."form_".$strTpl.".php";
		$iTplId =  $oDb->data["template_id"][0];
		$strTemplateTitle =  $oDb->data["template_title_fr"][0];
		
		if (!file_exists($strTemplate)) {
			$strTemplate = $_CONST['path']['templates']."form_accueil.php";
			$strTpl = "accueil";
			$iTplId = 1;
			$strTemplateTitle = "Accueil";
		}
		$this->iTemplateId = $iTplId;
		$this->strTemplateName = $strTpl;
		$this->strTemplatePath = $strTemplate;
		$this->strTemplateTitle = $strTemplateTitle;
    }
    
    /**
    * GetColumn  - Retourne les collonnes contenant les modules de l'interface de navigation.
    * 
    * <p>Retourne une chaine au format HTML contenant la colonne gauche ou droite 
    * en fonction du param�tre contenant les module de l'interface de navigation du site.
    * ceci en fonction de la configuration de la boutique.</p>
    * 
    * @name Jo_interface::GetColumn()
    * @param string - prend pour valeur 'left' ou 'right'
    * @return void
    */
    public function GetColumn($strSide) {
    	global $oDb;
    	global $_CONST;
    	
    	$strSqlCol = "SELECT a.module_title, a.module_libelle 
    				FROM ".$_CONST["tables"]["module"]." a
    				WHERE a.module_side='".mysql_real_escape_string($strSide)."'
    				ORDER BY a.module_order";
    	$aTab = $oDb->queryTab($strSqlCol);
    	
    	if ($oDb->rows > 0) {
    		foreach ($aTab as $aMod)
    			$this->GetModule($aMod["module_libelle"], $aMod["module_title"]);
    	}
    }
    
    /**
    * GetTop - Affiche la partie haute du site.
    * 
    * <p>.</p>
    * 
    * @name Jo_interface::GetTop()
    * @return void
    */
	public function GetTop()
	{
		global $_CONST;
		
		$filename = $_CONST["path"]["logo"].$this->iBoutiqueId.".html";
		if (file_exists($filename)) {
			$handle = fopen($filename, "r");
			$contents = fread($handle, filesize($filename));
			fclose($handle);
			echo $contents;
		}
		else 
			echo "<span class='logotitle'>".$this->strBoutiqueName."</span>";
	}
    
    /**
    * GetFooter  - Retourne le bas de la page
    * 
    * <p></p>
    * 
    * @name Jo_interface::GetFooter()
    * @return void
    */
    public function GetFooter() {
    	
    	echo "<div class='footerdiv'><a href='page-mentions_legales.html'>Mentions l�gales</a> | <a href='page-contactez_nous.html'>Contactez-nous</a> </div>";
    	
    }
    
 /**
    * verif_page_access
    * 
    * <p>R�cup�ration du droit de l'utilisateur sur l'icon</p>
    * 
    * @name Jo_interface::verif_page_access()
    * @param String correspondant au nom du template
    * @return integer
    */ 
    public function verif_page_access($strIconsTemplate){
		global $_SESSION;	
		global $oDb;
		global $_CONST;
		
		$strSQLaccess = "SELECT a.users_rw FROM eco_assoc_users_nav_icons a INNER JOIN eco_nav_icons i ON (a.nav_icons_id = i.nav_icons_id)WHERE i.nav_icons_template = '".$strIconsTemplate."' AND a.users_id ='".$_SESSION[$_CONST["session_name"]]["users_id"]."'";
		$resRw = $oDb->queryItem($strSQLaccess);
		return $resRw;
    }
    
    /**
    * Destructeur
    * 
    * <p>Destruction de l'instance de classe</p>
    * 
    * @name Jo_interface::__destruct()
    * @return void
    */
    public function __destruct() {
    
    }
 }
 ?> 