<?php 
	class cb {
		public $strUrlWsdl = "https://www.interforum.fr/it-abonnement/coordBancairesWebService?wsdl";
		public $strLoginSite = "Bordas"; 
		public $strPasswordSite = "158Abo94cb78"; 
		public $strOrigine = "BO_BSS"; 
		public $strMarque = "BRD"; 
		public $strCodeSite ="BRD_SOUTIEN_SCO"; 
		public $strCodeAction ="TSTABO"; 
		public $codeTransaction ="CDI"; 
		public $params = array();  
		public $strProfil = 'PAR';  
		
		public $aCodeToCiv = array( "M." => 1 , "Mme" => 2, "Mlle" => 3);
		public $aCivToCode = array( 1 => "M." , 2=> "Mme" , 3 => "Mlle" );
		
		public $strFluxXml = "" ; 
		
		
		public function getErreur($strCode){
			switch ($strCode){
				case "01": $strErreur = "Certaines données sont introuvables, Votre abonnement n'a pas été activé." ; break; 
				case "02": $strErreur = "Vos coordonnées bancaires sont invalide. Votre abonnement n'a pas été activé." ; break; 
				case "03": $strErreur = "Une erreur est survenue lors de l'enregistrement de votre abonnement." ; break; 
				case "04": $strErreur = "Une erreur est survenue lors de l'enregistrement de votre abonnement." ; break; 
				case "05": $strErreur = "Une erreur est survenue lors de l'enregistrement de votre abonnement." ; break; 
				case "06": $strErreur = "Votre carte bancaire n'est pas valide. Votre abonnement n'a pas été activé." ; break; 
				case "07": $strErreur = "Une erreur est survenue lors de l'enregistrement de votre abonnement." ; break; 
				case "08": $strErreur = "Une erreur est survenue lors de l'enregistrement de votre abonnement." ; break; 
				case "09": $strErreur = "Une erreur est survenue lors de l'enregistrement de votre abonnement." ; break; 
				case "10": $strErreur = "Votre abonnement n'a pas été retrouvé." ; break; 
				default: $strErreur = "Une erreur est survenue lors de l'enregistrement de votre abonnement." ; break; 
			}
			
			return $strErreur ; 
		}
		
		//Fonction retourne true si la date de validation de la carte expire dans les 30 jours 
		public function verifExpirationCarte($numClient,$idCarte){	
			$aExpiration = $this->getInfoCarte($numClient,$idCarte) ; 
			// var_dump($aExpiration);
			if($aExpiration['erreur'])
				return false; 
			else{
				$strDateExpire = $aExpiration['dateExpCB'] ; 
				$strMoisExpire = substr($strDateExpire, 0, 2);
				$strAnneeExpire = substr( date('Y'),0,-2).substr($strDateExpire, 2, 2);
				
				// $strDifference = strtotime($strAnneeExpire."-".$strMoisExpire."-01")-strtotime("2016-08-20"); 
				
				$strNow = time();
				// $strNow = strtotime("2016-08-01");
				$strDifference = strtotime($strAnneeExpire."-".$strMoisExpire."-01")-$strNow; 
				$iInterval = intval($strDifference/86400); 
				// var_dump($iInterval); 
				if($iInterval <= 30)
					return true; 
			}
		}
		
		
		
		public function getInfoCarte($numClient,$idCarte){	
// var_dump(		$numClient);
// var_dump(		$idCarte);
// $idCarte = 241 ;
			if( !empty($numClient) && !empty($idCarte)){
				$strFlux = 	'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cb="http://cb.it.cxf.generated/">';
				$strFlux .=		'<soapenv:Header></soapenv:Header>';
				$strFlux .=		'<soapenv:Body>';
				$strFlux .=			'<cb:controleValiditeCoordBancaires>';
				$strFlux .=				'<password>'.$this->strPasswordSite .'</password>';
				$strFlux .=				'<client>';
				// $strFlux .=					'<numClientLineavad>'.$numClient.'</numClientLineavad>';
				$strFlux .=					'<idClientSiteWeb>'.$numClient.'</idClientSiteWeb>';
				$strFlux .=					'<codeMarque>'.$this->strMarque .'</codeMarque>';
				$strFlux .=					'<codeSite>'.$this->strCodeSite .'</codeSite>';
				$strFlux .=				'</client>';
				$strFlux .=				'<idReferenceCb>'.$idCarte.'</idReferenceCb>';
				$strFlux .=			'</cb:controleValiditeCoordBancaires>';
				$strFlux .=		'</soapenv:Body>';
				$strFlux .=	'</soapenv:Envelope>';
				
				$aRetour = $this->executeWs($strFlux, "controleValiditeCoordBancaires");
				
				if($aRetour['ws']){
					//On vérifie si la réponse ok ou ko
					$oReponse = $aRetour['oReponse'] ; 
					
					if($oReponse->infosCb->coordBancaire->codeReponse == "OK"){
						$aReponse['erreur'] = false; 
						$aReponse['idReferenceCb'] = (string)$oReponse->infosCb->coordBancaire->idReferenceCb ;
						$aReponse['dateExpCB'] = (string)$oReponse->infosCb->coordBancaire->dateExpCB ;
						// var_dump($aReponse); 
					}else{
					// var_dump( $oReponse->infosCb->coordBancaire->erreurs); 
						$oErreur = $oReponse->infosCb->coordBancaire->erreurs; 
						$aReponse['erreur'] = true; 
						$aReponse['code'] = $oErreur->erreur->code ; 
						$aReponse['message'] = $oErreur->erreur->message ; 
					}
				}else{
					//Erreur technique le flux a été enregistré en base
					$aReponse['erreur'] = true; 
					$aReponse['code'] = 99 ; 
					$aReponse['message'] = "Erreur technique" ; 
				}
			}else{
				//Erreur retourne 
				$aReponse['erreur'] = true; 
				$aReponse['code'] = 99 ; 
				$aReponse['message'] = "Paramètres manquants" ; 				
			}		
			
			return $aReponse;
		}
		
		public function getCoordBancaire($numClient,$idCarte){
			$strFlux = 	'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cb="http://cb.it.cxf.generated/">';
			$strFlux .=		'<soapenv:Header></soapenv:Header>';
			$strFlux .=		'<soapenv:Body>';
			$strFlux .=			'<cb:recupererCoordBancaires>';
			$strFlux .=				'<password>'.$this->strPasswordSite .'</password>';
			$strFlux .=				'<client>';
			// $strFlux .=					'<numClientLineavad>'.$numClient.'</numClientLineavad>';
			$strFlux .=					'<idClientSiteWeb>'.$numClient.'</idClientSiteWeb>';
			$strFlux .=					'<codeMarque>'.$this->strMarque .'</codeMarque>';
			$strFlux .=					'<codeSite>'.$this->strCodeSite .'</codeSite>';
			$strFlux .=				'</client>';
			$strFlux .=				'<idReferenceCb>'.$idCarte.'</idReferenceCb>';
			$strFlux .=			'</cb:recupererCoordBancaires>';
			$strFlux .=		'</soapenv:Body>';
			$strFlux .=	'</soapenv:Envelope>';
			$aRetour = $this->executeWs($strFlux, "recupererCoordBancaires");
			
			if($aRetour['ws']){
				//On vérifie si la réponse ok ou ko
				$oReponse = $aRetour['oReponse'] ; 
				
				if($oReponse->infosCb->coordBancaire->codeReponse == "OK"){
					$aReponse['erreur'] = false; 
					$aReponse['idReferenceCb'] = (string)$oReponse->infosCb->coordBancaire->idReferenceCb ;
					$aReponse['libelleCb'] = (string)$oReponse->infosCb->coordBancaire->libelleCb ;
					$aReponse['typeCB'] = (string)$oReponse->infosCb->coordBancaire->typeCB ;
					$aReponse['numeroCB'] = (string)$oReponse->infosCb->coordBancaire->numeroCB ;
					$aReponse['porteurCB'] = (string)$oReponse->infosCb->coordBancaire->porteurCB ;
					$aReponse['dateExpCB'] = (string)$oReponse->infosCb->coordBancaire->dateExpCB ;
					// var_dump($aReponse); 
				}else{
				// var_dump( $oReponse->infosCb->coordBancaire->erreurs); 
					$oErreur = $oReponse->infosCb->coordBancaire->erreurs; 
					$aReponse['erreur'] = true; 
					$aReponse['code'] = $oErreur->erreur->code ; 
					$aReponse['message'] = $oErreur->erreur->message ; 
				}
			}else{
				//Erreur technique le flux a été enregistré en base
				$aReponse['erreur'] = true; 
				$aReponse['code'] = 99 ; 
				$aReponse['message'] = "Erreur technique" ; 
			}
			
			// var_dump($aRetour);
			// var_dump($aReponse);
			return $aReponse ;  
			
	
		}
		

		
		public function deleteCb($numClient, $idRefCb){
			$strFlux = 	'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cb="http://cb.it.cxf.generated/">';
			$strFlux .=		'<soapenv:Header></soapenv:Header>';
			$strFlux .=		'<soapenv:Body>';
			$strFlux .=			'<cb:deleteCoordBancaires>';
			$strFlux .=				'<password>'.$this->strPasswordSite .'</password>';
			$strFlux .=				'<client>';
			// $strFlux .=					'<numClientLineavad>'.$numClient.'</numClientLineavad>';
			$strFlux .=					'<idClientSiteWeb>'.$numClient.'</idClientSiteWeb>';
			$strFlux .=					'<codeMarque>'.$this->strMarque .'</codeMarque>';
			$strFlux .=					'<codeSite>'.$this->strCodeSite .'</codeSite>';
			$strFlux .=				'</client>';
			$strFlux .=				'<idReferenceCb>'.$idRefCb.'</idReferenceCb>';
			$strFlux .=			'</cb:deleteCoordBancaires>';
			$strFlux .=		'</soapenv:Body>';
			$strFlux .=	'</soapenv:Envelope>';
			$aRetour = $this->executeWs($strFlux, "deleteCoordBancaires");
			if($aRetour['ws']){
				//On vérifie si la réponse ok ou ko
				$oReponse = $aRetour['oReponse'] ; 
				if(isset($oReponse->infoCbOut->codeReponse) &&  $oReponse->infoCbOut->codeReponse == "OK"){
					return true; 
				}else{
					return false; 
				}
			}else{
				return false; 
			}
		}
		
		
		public function updateCb($numClient, $idRefCb, $strTypeCb, $strNumeroCB, $strPorteurCB,$strDateExpCB){
			$strFlux = 	'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cb="http://cb.it.cxf.generated/">';
			$strFlux .=		'<soapenv:Header></soapenv:Header>';
			$strFlux .=		'<soapenv:Body>';
			$strFlux .=			'<cb:modifCoordBancaires>';
			$strFlux .=				'<password>'.$this->strPasswordSite .'</password>';
			$strFlux .=				'<client>';
			// $strFlux .=					'<numClientLineavad>'.$numClient.'</numClientLineavad>';
			$strFlux .=					'<idClientSiteWeb>'.$numClient.'</idClientSiteWeb>';
			$strFlux .=					'<codeMarque>'.$this->strMarque .'</codeMarque>';
			$strFlux .=					'<codeSite>'.$this->strCodeSite .'</codeSite>';
			$strFlux .=				'</client>';
			$strFlux .=				'<infosCb>';
			$strFlux .=					'<idReferenceCb>'.$idRefCb.'</idReferenceCb>';
			$strFlux .=					'<typeCB>'.$strTypeCb.'</typeCB>';
			$strFlux .=					'<numeroCB>'.$strNumeroCB.'</numeroCB>';
			$strFlux .=					'<porteurCB>'.$strPorteurCB.'</porteurCB>';
			$strFlux .=					'<dateExpCB>'.$strDateExpCB.'</dateExpCB>';
			$strFlux .=				'</infosCb>';
			$strFlux .=			'</cb:modifCoordBancaires>';
			$strFlux .=		'</soapenv:Body>';
			$strFlux .=	'</soapenv:Envelope>';
			$aRetour = $this->executeWs($strFlux, "modifCoordBancaires");
			if($aRetour['ws']){
				//On vérifie si la réponse ok ou ko
				$oReponse = $aRetour['oReponse'] ; 
				if(isset($oReponse->infoCbOut->codeReponse) &&  $oReponse->infoCbOut->codeReponse == "OK"){
					$aReponse['erreur'] = false; 
					$aReponse['idReferenceCb'] = (string)$oReponse->infoCbOut->idReferenceCb ;
				}else{
				// var_dump( $oReponse->infosCb->coordBancaire->erreurs); 
					$oErreur = $oReponse->infoCbOut->erreurs; 
					$aReponse['erreur'] = true; 
					$aReponse['code'] = (string)$oErreur->erreur->code ; 
					$aReponse['message'] = (string)$oErreur->erreur->message ; 
				}
			}else{
				//Erreur technique le flux a été enregistré en base
				$aReponse['erreur'] = true; 
				$aReponse['code'] = 99 ; 
				$aReponse['message'] = "Erreur technique" ; 
			}
			return $aReponse;
		}
		
		public function executeWs($strFlux, $strAction){
			try{
				ini_set("soap.wsdl_cache_enabled",1);
				$client = new SoapClient($this->strUrlWsdl, array('trace'=>1));		
				$client->__setCookie('session_id', session_id());
				$retour_ws = $client->__doRequest($strFlux, $this->strUrlWsdl, "", 1 );
				
				// var_dump($retour_ws); 
				if($bOut = preg_match("#<out>(.*)</out>#", $retour_ws, $aReponse)){
					$oReponse = new SimpleXMLElement($aReponse[0]);
					$aRetour['ws'] = true ; 
					$aRetour['oReponse'] = $oReponse ;
					$this->saveFlux("cb.class.php",$strAction, $strFlux ,$retour_ws,"OK") ; 
				}else{
					$this->saveFlux("cb.class.php",$strAction, $strFlux ,$retour_ws,"KO") ; 
					//Initialisation du tableau réponse
					$aRetour['ws'] = false ; 
					$aRetour['message'] = "REPONSE INCORRECTE";
				}
				return $aRetour; 
			}catch (Exception $e){
				$this->saveFlux("cb.class.php",$strAction, $strFlux ,$retour_ws,"KO") ; 
				//Initialisation du tableau réponse
				$aRetour['ws'] = false ; 
				$aRetour['message'] = "ERREUR TECHNIQUE";
				return $aRetour; 
			}
		}
		
		
		public function saveFlux($flux_name, $flux_action,   $flux_send, $flux_response,$flux_statut){
			global $oDb; 
			$strSql = "INSERT INTO bor_logs_flux (flux_date, flux_name, flux_action, flux_send, flux_response, flux_statut)
								VALUES ( NOW(), '".mysql_real_escape_string($flux_name)."','".mysql_real_escape_string($flux_action)."','".mysql_real_escape_string($flux_send)."','".mysql_real_escape_string($flux_response)."','".mysql_real_escape_string($flux_statut)."' ) ";
			
			$oDb->query($strSql);
		}
		
		
		
		public function verifInterneCb($strCbName, $strCbType, $strCbNum, $strCbAnnee, $strCbMois, $strCbControle){
				$bVerif = true ; 
				$strError = ""; 
			if(!isset($strCbName) || empty($strCbName)){
				$bVerif = false; 
				$strError = "Le nom du détenteur de la carte est obligatoire."; 
			}
			if($bVerif  && (!isset($strCbType) || empty($strCbType))){
				$bVerif = false; 
				$strError = "Veuillez cocher le type de votre carte bancaire."; 
			}
			if($bVerif  && (!isset($strCbNum) || empty($strCbNum))){
				$bVerif = false; 
				$strError = "Veuillez saisir votre numéro de carte bancaire."; 
			}
			if($bVerif  &&  !preg_match("/^[0-9]{13,16}$/", $strCbNum)){
				$bVerif = false; 
				$strError = "Le numéro de la carte bancaire n'est pas valide."; 
			}
			$strCarteDate = $strCbAnnee.'-'.$strCbMois;
			$strDate = date('y-m');
			if($bVerif  && $strCarteDate < $strDate){
				$bVerif = false; 
				$strError = "La date d'expiration de la carte est dépassée."; 
			}
			if($bVerif  && (!isset($strCbControle) || empty($strCbControle))){
				$bVerif = false; 
				$strError = "Veuillez saisir le numéro de contrôle."; 
			}
			if($bVerif  &&  !preg_match("/^[0-9]{3}$/", $strCbControle)){
				$bVerif = false; 
				$strError = "Le numéro de contrôle n'est pas valide."; 
			}
			
			return array("bVerif" => $bVerif , "strMessage" => $strError);
			
			
			
		}
		
		
		
	}
?>