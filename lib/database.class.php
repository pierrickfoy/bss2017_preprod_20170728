<?php
/******************************************************************************
* Description : ce fichier fait un include du fichier class
*				de la base de donn�e utilis�e o� sont d�finies les functions
*				sp�cifiques � cette base.
*				Le fichier inclut offre un certain nombre de fonctionnalit�s
*               comme la cr�ation d'une connexion, l'ex�cution d'une requ�te
*               devant retourner un champ, une ligne ou un ensemble de ligne ou
*               encore l'affichage du temps d'ex�cution d'un requ�te, la r�cup�ration
*               du dernier enregistrement ins�r� au cours de la session...
*
* Version : 2.0
*
* Date : 22/01/2001, 15/05/2003
*
* Author : V2.0 L. Franchomme, R. Carles
*******************************************************************************/


include_once($_CONST['path']["server"]."/lib/database/".$_CONST["DATABASE_TYPE"].".class.php");

class database extends database_spec {
	
	var $actionInsert="INS";
	var $actionUpdate="UPD";
	var $actionDelete="DEL";
	var $idInsert="-2";
	
	/**
	* @return database
	* @param string 	$database_name 	le nom de la base de donn�es
	* @param string 	$username 		le nom de l'utilisateur servant � la connexion
	* @param string 	$password 		le mot de passe de connexion
	* @param string 	$host 			l'adresse IP du serveur (optionnel) : 127.0.0.1 par d�faut
	* @desc Appel du constructeur de la classe parente
	* 		contient tous les �l�ments sp�cifiques � la base de donn�es de destination pour constituer une couche d'abstraction
	*/
	function database($database_name, $username, $password, $host="",$port="") {
		
		global $_CONST;
		$this->actionInsert=($_CONST["FW_ACTION_INS"]?$_CONST["FW_ACTION_INS"]:"INS");
		$this->actionUpdate=($_CONST["FW_ACTION_UPD"]?$_CONST["FW_ACTION_UPD"]:"UPD");
		$this->actionDelete=($_CONST["FW_ACTION_DEL"]?$_CONST["FW_ACTION_DEL"]:"DEL");
		$this->idInsert=($_CONST["FORM_ID_AJOUT"]?$_CONST["FORM_ID_AJOUT"]:"-2");
		parent::database_spec($database_name, $username, $password, $host, $port);
	}
	
	/***********************************************************************/
	/*********************** METHODES PUBLIQUES ****************************/
	/***********************************************************************/
	
	
	/**
	* @return mixed
	* @param string	$query 	Requ�te SQL
	* @desc Cr�ation d'un tableau de valeur � partir d'une requ�te pour un formulaire : tableau de la forme $values[CHAMP]=VALEUR;
	*/
	function queryForm($query) {
		$result = $this->queryRow($query);
		while ($ligne = each($result)) {
			if (!is_numeric($ligne["key"])) {
				$values[$ligne["key"]]=$ligne["value"];
			}
		}
		return $values;
	}
	
	/**
	* @return mixed
	* @param string	$query 	Requ�te SQL
	* @desc Execute une requ�te sans �craser les tableaux de r�sultat en cours ($this->data)
	*/
	function queryThru($query)
	{
		// sauvegarde du contexte actuel
		$data_bkp 		= $this->data;
		$rows_bkp 		= $this->rows;
		$this->query($query);
		$data1 			= $this->data;
		$this->data   	= $data_bkp;
		$this->rows   	= $rows_bkp;
		return($data1);
	}
	
	/**
	* @return string
	* @param string $query chaine SQL
	* @desc Retourne le r�sultat d'une requ�te en XML
	*/
	function queryXML($query)
	{
		// execution de la requete
		$result=$this->queryTab($query);
		$xml="<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>\r\n";
		$xml.="<".$this->database_name." rows=\"".$this->rows."\" fields=\"".$this->fields."\">\r\n";
		for ($num_ligne = 0; $num_ligne < $this->rows ; $num_ligne++) {
			$xml.="\t<RECORD line=\"".($num_ligne+1)."\">\r\n";
			for ($field = 0; $field < $this->fields ; $field++) {
				$string=false;
				if ($this->type[$field]=="string" || $this->type[$field]=="blob") {
					$string=true;
				}
				$name_field=strtoupper($this->name[$field]);
				$xml.="\t\t<".$name_field." type=\"".$this->type[$field]."\" width=\"".$this->len[$field]."\">\r\n";
				$xml.="".htmlspecialchars($result[$num_ligne][$this->name[$field]]);
				$xml.="\t\t</".$name_field.">\r\n";
			}
			$xml.="\t</RECORD>\r\n";
		}
		$xml.="</".$this->database_name.">\r\n";
		// renvoie du r�sultat
		return($xml);
	}
	
	/**
	* @return void
	* @desc Affiche le r�sultat d'une requ�te
	*/
	function afficheResQuery()
	{
		// recuperation du r�sultat
		echo $this->getHeader("R&eacute;sultat d'une requ&egrave;te", "#9999CC");
		
		if (is_array($this->data))
		{
			reset($this->data);
			for ($num_ligne = 0; $num_ligne < $this->rows ; $num_ligne++)
			{
				if (is_array($this->data[$num_ligne]))
				{
					echo "<TR><TD BGCOLOR=\"#FFFFFF\" ALIGN=\"center\" COLSPAN=\"2\"><FONT FACE=\"Verdana,Helvetica,Arial\" COLOR=\"#893737\" SIZE=\"2\"><B>ligne ".intval($num_ligne + 1)."</B></FONT></TD></TR>";
					while (list($key, $val) = each($this->data[$num_ligne]))
					{
						echo "<TR>
                                <TD BGCOLOR=\"#CCCCFF\" ALIGN=\"left\"><FONT FACE=\"Verdana,Helvetica,Arial\" COLOR=\"#FFFD3E\" SIZE=\"2\"><B>$key</B></FONT></TD>
                                <TD BGCOLOR=\"#CCCCCC\" ALIGN=\"left\"><FONT FACE=\"Verdana,Helvetica,Arial\" COLOR=\"#000000\" SIZE=\"2\"><B>$val&nbsp;</B></FONT></TD>
                              </TR>";
					}
				}
				else
				{
					while (list($key, $val) = each($this->data))
					{
						echo "<TR>
                                <TD BGCOLOR=\"#CCCCFF\" ALIGN=\"left\"><FONT FACE=\"Verdana,Helvetica,Arial\" COLOR=\"#FFFD3E\" SIZE=\"2\"><B>$key</B></FONT></TD>
                                <TD BGCOLOR=\"#CCCCCC\" ALIGN=\"left\"><FONT FACE=\"Verdana,Helvetica,Arial\" COLOR=\"#000000\" SIZE=\"2\"><B>$val&nbsp;</B></FONT></TD>
                              </TR>";
					}
				}
			}
		}
		else if ($this->rows > 0)
		{
			echo "<TR>
                    <TD BGCOLOR=\"#CCCCFF\" ALIGN=\"left\"><FONT FACE=\"Verdana,Helvetica,Arial\" COLOR=\"#FFFD3E\" SIZE=\"2\"><B>valeur</B></FONT></TD>
                    <TD BGCOLOR=\"#CCCCCC\" ALIGN=\"left\"><FONT FACE=\"Verdana,Helvetica,Arial\" COLOR=\"#000000\" SIZE=\"2\"><B>".$this->data."&nbsp;</B></FONT></TD>
                  </TR>";
		}
		else
		{
			echo "<TR><TD BGCOLOR=\"#CCCCCC\" ALIGN=\"left\" COLSPAN=\"2\"><FONT FACE=\"Verdana,Helvetica,Arial\" COLOR=\"#000000\" SIZE=\"2\"><B>aucun r&eacute;sultat</B></FONT></TD></TR>";
		}
		
		echo $this->getFooter();
	}
	
	
	/***********************************************************************/
	/************************ METHODES PRIVEES *****************************/
	/***********************************************************************/
	
	/**
	* @return mixed
	* @param integer	$row 	Num�ro de la ligne
	* @desc Retourne les valeurs d'une ligne
	*/
	function Fetch($row)
	{
		$cnt = 0;
		reset($this->data);
		while ( list( $key, $val ) = each( $this->data ) )
		{
			//			$return[$key] = $val[$row];
			$return[strtolower($key)] = $val[$row];
			$return[$cnt++] = $val[$row];
		}
		return $return;
	}
	
	
	/**
	* @return string
	* @param string $titre le titre du tableau
	* @param string $bgcolor la couleur de fond du titre (optionnel) : #B24609 par d�faut
	* @desc permet d'afficher le haut du tableau servant � afficher les informations (erreur, r�sultat d'une requ�te...).
	*/
	function getHeader($titre, $bgcolor = "#B24609")
	{
		return "<BR><TABLE BORDER=\"1\" CELLSPACING=\"1\" CELLPADDING=\"4\">
                    <TR BGCOLOR=\"$bgcolor\">
                        <TD ALIGN=\"center\" COLSPAN=\"2\">
                           <FONT FACE=\"Verdana,Helvetica,Arial\" COLOR=\"#FFFFFF\" SIZE=\"1\"><B>$titre</B></FONT>
                        </TD>
                    </TR>";
	}
	
	/**
	* @return string
	* @desc permet d'afficher le bas du tableau servant � afficher les informations (erreur, r�sultat d'une requ�te...).
	*/
	function getFooter()
	{
		return "</TABLE><BR>";
	}
	
	/**
	* @return void
	* @param string $msg le message d'erreur
	* @desc permet d'afficher un message d'erreur de la base
	*/
	function error($msg)
	{
		global $_CONST, $_SERVER, $_REQUEST, $_SESSION;

		$message=$this->getError();

		//et lui communiquer l'URL suivante :<BR>".$_SERVER["REQUEST_URI"];
		$strSubject = "[".$_CONST['TYPE_ENVIRONNEMENT']."][ECOMIZ] ERREUR SQL";
		$strMailMsg = "<b>Erreur : </b><br>".$msg."<br><br>";
		$strMailMsg .= "<b>Message : </b><br>".$message["message"]."<br><br>";
		$strMailMsg .= "<b>Code : </b><br>".$message["code"]."<br><br>";
		$strMailMsg .= "<b>URL : </b><br>".$_SERVER["REQUEST_URI"]."<br><br>";
		$strMailMsg .= "<b>Trace : </b><br><pre>".print_r(debug_backtrace(), true)."</pre><br><br>";
		$strMailMsg .= "<b>Session : </b><br><pre>".print_r($_SESSION, true)."</pre><br><br>";
		//echo $strMailMsg;
		echo 'E.';
		/*$message="";
		$msg="Le gestionnaire du site a �t� inform� de l'erreur qui s'est produite.<br>Le correctif sera mis en place rapidement.<br><br>Merci de votre compr�hension.";
		echo $msg;*/
	}
	
	function message($message1,$message2="") {
		return $message1.' - '.addslashes($message2);
	}
	/***********************************************************************/
	/****************************** TIME **********************************/
	/***********************************************************************/
	
	/**
	* @return void
	* @param boolean $etat true/false
	* @desc permet de fixer � true/false l'affichage du temps d'ex�cution des requ�tes SQL.
	*/
	function setDebugTime($etat)
	{
		$this->debug_time = $etat;
	}
	
	/**
	* @return void
	* @desc Enter description here...
	*/
	function initTimeQuery()
	{
		$mtime = microtime();
		$mtime = explode(" ", $mtime);
		$this->time_deb = $mtime[1] + $mtime[0];
	}
	
	/**
	* @return void
	* @param string $query chaine SQL
	* @desc Enter description here...
	*/
	function displayTimeQuery($query)
	{
		$mtime = microtime();
		$mtime = explode(" ", $mtime);
		$this->time_fin = $mtime[1] + $mtime[0];
		
		$duree = $this->time_fin - $this->time_deb;
		
		/*echo $this->getHeader("Information sur une requ&egrave;te", "#9999CC");
		echo "<TR>
                <TD BGCOLOR=\"#CCCCFF\" ALIGN=\"left\"><FONT FACE=\"Verdana,Helvetica,Arial\" COLOR=\"#FFFD3E\" SIZE=\"2\"><B>Requ&egrave;te</B></FONT></TD>
                <TD BGCOLOR=\"#CCCCCC\" ALIGN=\"left\"><FONT FACE=\"Verdana,Helvetica,Arial\" COLOR=\"#000000\" SIZE=\"2\"><B>$query</B></FONT></TD>
              </TR>
              <TR>
                <TD BGCOLOR=\"#CCCCFF\" ALIGN=\"left\"><FONT FACE=\"Verdana,Helvetica,Arial\" COLOR=\"#FFFD3E\" SIZE=\"2\"><B>Temps d'ex&eacute;cution</B></FONT></TD>
                <TD BGCOLOR=\"#CCCCCC\" ALIGN=\"left\"><FONT FACE=\"Verdana,Helvetica,Arial\" COLOR=\"#000000\" SIZE=\"2\"><B>$duree sec</B></FONT></TD>
              </TR>";
		echo $this->getFooter();*/
		return( "G�n�rer en $duree sec");
	}
	
	/***********************************************************************/
	/**************************** PROCESSUS ********************************/
	/***********************************************************************/
	
	/**
	* @return void
	* @param string	$table 	Nom de la table
	* @desc Insertion d'un enregistrement dans une table
	*/
	function insertQuery($table) {
		$this->updateTable($this->actionInsert, $table);
	}
	
	/**
	* @return void
	* @param string	$table 	Nom de la table
	* @desc Mise � jour d'un enregistrement d'une table
	*/
	function updateQuery($table) {
		$this->updateTable($this->actionUpdate, $table);
	}
	
	/**
	* @return void
	* @param string	$table 	Nom de la table
	* @desc Suppresion d'un enregistrement d'une table
	*/
	function deleteQuery($table) {
		$this->updateTable($this->actionDelete, $table);
	}
	
	/**
	* @return void
	* @param string 	$table 	Nom de la table associative
	* @param string	$assoc 	Champ "pivot" (cl� de la table de r�f�rence associ�e)
	* @desc Mise � jour d'une associative
	*/
	function assocQuery($table,$assoc) {
		$this->updateTable($this->actionInsert, $table, $assoc);
	}
	
	function setBindValue(&$Bind,$Level,$Value,$Type="") {
		global $FormatBind;
		$bindIndex=":A_".$Level."_A";
		if ($Type!="" && !$this->allow_bind) {
			$Bind[$bindIndex]=$this->formatField($Value,$Type);
		} else {
			$Bind[$bindIndex]=$Value;
		}
		if ($this->allow_bind) {
			$Bind[$bindIndex]=str_replace("\'","'",$Bind[$bindIndex]);
			if ($this->isDate($Type)) $FormatBind[]=$bindIndex;
		}
		return $bindIndex;
	}
	
	function putBind($query,$param) {
		//		debug(array($query,$param));
		$strSQL=strtr($query,$param);
		//		debug($strSQL);
		return $strSQL;
	}
	
	/**
	* @return void
	* @param string	$form_action 	Action : INS, UPD ou DEL
	* @param string	$table 			Nom de la table � mettre � jour
	* @param string	$assoc 			Nom du champ pivot pour la mise � jour d'une associative : "" par d�faut
	* @desc Insert, Update ou Delete des donn�es dans une table ou une associative.
	* Si une s�quence (ORACLE ou PostgreSQL) de type SEQ_NOM est associ�e � une cl� primaire du type NOM_ID => la s�quence est lanc�e et ins�r�e dans les donn�es
	* Si un auto_increment (MySQL), serial (PostgreSQL) ou counter (odbc) est en place, la valeur est r�affect�e au champ une fois l'insertion faite
	*/
	function updateTable($form_action, $table, $assoc="") {
		global $FORM,$_REQUEST, $aBind;
		
		// si le POST ou le GET n'a pas �t� r�cup�r�
		if (!$FORM) $FORM=$_REQUEST;
		//		$aBind=array();
		
		$result=$this->describeTable($table);
		if ($result != FALSE) {
			$j=-1;
			foreach ($result as $value) {
				$j++;
				$field_set[$j]["field"] = $value["field"];
				$field_set[$j]["key"] = $value["key"];
				$field_set[$j]["increment"] = $value["increment"];
				$field_set[$j]["sequence"] = $value["sequence"];
				$field_set[$j]["type"]= $value["type"];
				// pour les cas o� on a une s�quence, elle est initialis�e
				if ( $value["key"] && $value["sequence"] && $FORM[$value["field"]]==$this->idInsert && $form_action==$this->actionInsert) {
					$FORM[$value["field"]]=$this->getNextId($table);
				}
				if ($assoc != "" && $value["field"]==$assoc) {
					if (!is_array($FORM[$assoc])) {
						$FORM[$assoc] = array(1=>$FORM[$assoc]);
					}
					foreach ($FORM[$assoc] as $field_assoc) {
						if ($field_assoc != "") {
							$field_set[$j]["value"][]=$this->formatField($field_assoc,$value["type"]);
							$field_set[$j]["bind"]=$this->setBindValue($aBind,$j,$field_assoc,$value["type"]);
						}
					}
				} else {
					$field_set[$j]["value"][0]=$this->formatField($FORM[$value["field"]],$value["type"]);
					$field_set[$j]["bind"]=$this->setBindValue($aBind,$j,$FORM[$value["field"]],$value["type"]);
				}
			}
			if ($assoc != "" && $form_action==$this->actionUpdate) {
				$strSQL=$this->updateRecord($this->actionDelete,$table,$field_set,$assoc);
				$action=$this->actionInsert;
			} else {
				$action=$form_action;
			}
			$strSQL=$this->updateRecord($action,$table,$field_set,$assoc);
		}
	}
	
	/**
	* @return void
	* @param string	$action 		Action : INS, UPD ou DEL
	* @param string	$table 			Nom de la table � mettre � jour
	* @param string	$field_set 		Tableau de d�finition des champs (nom, format, valeur etc...)
	* @param string	$assoc 			Nom du champ pivot pour la mise � jour d'une associative
	* @desc Execute les chaines SQL pass�e en param�tre
	*/
	function updateRecord($action,$table,$field_set,$assoc) {
		global $FORM, $aBind;
		$strSQL1 = $strSQL2 = $strSQL2Bind = $strSQLBind = $strSQL = '';
		switch ($action) {
			case $this->actionInsert: {
				$strSQL = "INSERT INTO " . $table . " (";
				$strSQLBind=$strSQL;
				for ($j = 0; $j < sizeof($field_set); $j++) {
					if ( !$field_set[$j]["increment"] ) {
						$strSQL1 .= ($strSQL1==""?"":", ").$field_set[$j]["field"] ;
						if ($assoc != "" && $field_set[$j]["field"]==$assoc) {
							$strSQL2 .= ($strSQL2==""?"":", ").$field_set[$j]["bind"];
						} else {
							$strSQL2 .= ($strSQL2==""?"":", ").$field_set[$j]["value"][0];
						}
						$strSQL2Bind .= ($strSQL2Bind==""?"":", ").$field_set[$j]["bind"];
					} else {
						$auto_increment=$field_set[$j]["field"];
					}
				}
				$strSQLBind.=$strSQL1.") VALUES (".$strSQL2Bind.")";
				$strSQL.=$strSQL1.") VALUES (".$strSQL2.")";
			
				break;
			}
			case $this->actionUpdate: {
				$strSQL = "UPDATE " . $table . " SET ";
				$strSQLBind=$strSQL;
				$strSQL1Bind =''; 
				for ($j = 0; $j < sizeof($field_set); $j++) {
					if ($field_set[$j]["key"]) {
						$strSQL2 .= ($strSQL2==""?" WHERE ":" AND ").$field_set[$j]["field"] . "=" . $field_set[$j]["value"][0];
						$strSQL2Bind .= ($strSQL2Bind==""?" WHERE ":" AND ").$field_set[$j]["field"] . "=".$field_set[$j]["bind"];
					} else {
						$strSQL1 .= ($strSQL1==""?"":", ").$field_set[$j]["field"] . "=" . $field_set[$j]["value"][0];
						$strSQL1Bind .= ($strSQL1Bind==""?"":", ").$field_set[$j]["field"] . "=".$field_set[$j]["bind"];
					}
				}
				$strSQL.=$strSQL1.$strSQL2;
				$strSQLBind.=$strSQL1Bind.$strSQL2Bind;
			
				break;
			}
			case $this->actionDelete: {
				$strSQL = "DELETE FROM " . $table;
				$strSQLBind=$strSQL;
				for ($j = 0; $j < sizeof($field_set); $j++) {
					if ($field_set[$j]["key"] && $field_set[$j]["field"]!=$assoc) {
						$strSQL2 .= ($strSQL2==""?" WHERE ":" AND ").$field_set[$j]["field"] . "=" . $field_set[$j]["value"][0];
						$strSQL2Bind .= ($strSQL2Bind==""?" WHERE ":" AND ").$field_set[$j]["field"] . "=".$field_set[$j]["bind"];
					}
				}
				$strSQL .= $strSQL2;
				$strSQLBind.=$strSQL2Bind;
				break;
			}
		}
		if ($strSQL != "") {
			if ($assoc == "" ){
				//				$this->query($strSQL);
				$this->queryParam($strSQLBind,$aBind);
				if (isset($auto_increment) && ($FORM[$auto_increment]==$this->idInsert || !$FORM[$auto_increment])) {
					$this->getLastOid($strSQL);
					if (is_array($this->last_insert_id)) {
						$increment=$this->last_insert_id[$auto_increment];
					} else {
						$increment=$this->last_insert_id;
					}
					$FORM[$auto_increment]=$increment;
				}
			} else {
				for ($j = 0; $j < sizeof($field_set); $j++) {
					if ($field_set[$j]["field"]==$assoc) {
						if (!is_array($field_set[$j]["value"])) {
							$field_set[$j]["value"] = array(1=>$field_set[$j]["value"]);
						}
						if ($action == $this->actionDelete) {
							$this->queryParam($strSQLBind,$aBind);
						} else {
							foreach ($field_set[$j]["value"] as $value_assoc) {
								$aBind2=$aBind;
								$this->setBindValue($aBind2,$j,$value_assoc);
								if ($value_assoc != "") {
									$this->queryParam($strSQLBind,$aBind2);
								}
							}
						}
					}
				}
			}
		}
	}
	
	/**
	* @return string
	* @param string $field le nom du champ
	* @desc Retourne la valeur d'un champ
	*/
	function getValue($field) {
		$value = str_replace("\\","",$this->data[$field]);
		return $value;
	}
	
	/**
	* @return boolean
	* @param string $type Type de champ
	* @desc V�rifie si le type de champ d�fini un type num�rique
	*/
	function isNum($type) {
		$type=" ".strToLower($type);
		$return=false;
		if (strpos($type,"float")>0 ||
		strpos($type,"serial")>0 ||
		strpos($type,"counter")>0 ||
		strpos($type,"int")>0 ||
		strpos($type,"num")>0 ||
		strpos($type,"double")>0 ||
		strpos($type,"decimal")>0 ||
		strpos($type,"real")>0 ||
		strpos($type,"timestamp")>0) {
			$return=true;
		}
		return $return;
	}
	
	/**
	* @return boolean
	* @param string $type Type de champ
	* @desc V�rifie si le type de champ d�fini un type date
	*/
	function isDate($type) {
		$type=" ".strToLower($type);
		$return=false;
		if (strpos($type,"date")>0 ||
		strpos($type,"interval")>0) {
			$return=true;
		}
		return $return;
	}
	
	/**
	* @return boolean
	* @param string $type Type de champ
	* @desc V�rifie si le type de champ d�fini est un auto incr�ment
	*/
	function isIdentity($type) {
		$type=" ".strToLower($type);
		$return=false;
		if (strpos($type,"counter")>0 ||
		strpos($type,"serial")>0 ||
		strpos($type,"identity")>0) {
			$return=true;
		}
		return $return;
	}
	
	/**
	* @return string
	* @param string	$value 		Valeur
	* @param string	$type 		Type de la valeur
	* @desc Formattage de la valeur d'un champ
	*/
	function formatField($value,$type) {
		if ($this->isNum($type)) {
			if (empty($value)) {
				if ($value==="0") {
					return "0";
				} else {
					return "null";
				}
			} else {
				return str_replace(",",".",$value);
			}
		} elseif ($this->isDate($type)) {
			if ($value == ":DATE_COURANTE") {
				return str_replace(":DATE_COURANTE",$this->getNow(),$value);
			} else {
				if (empty($value)) {
					return "null";
				} else {
					return $this->dateStringToSql($value);
				}
			}
		} else {
			return "'".$this->stringToSql($value)."'";
		}
	}
}

/**
* @return string
* @param mixed $tab unknown
* @desc Retourne le minimum d'une liste de valeurs
*/
function aMin($tab) {
	foreach ($tab as $value) {
		if ($value) {
			if ($tabMin) {
				$tabMin=min($tabMin,$value);
			} else {
				$tabMin=$value;
			}
		}
	}
	return $tabMin;
}

function strLike($value) {
	global $fieldLike;
	return "upper(".$fieldLike.") like upper('%".trim($value)."%')";
}

function frenchDate($field, $label) {
	return "IF(".$field."='2222-02-02','-------------',date_format(".$field.", '%d/%m/%Y')) ".$label;
}
?>
