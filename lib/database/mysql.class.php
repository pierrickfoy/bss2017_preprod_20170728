<?php
/******************************************************************************
* Description : cette classe permet d'avoir un acc�s facilit� � une base de
*               donn�es MySQL. Elle offre un certain nombre de fonctionnalit�s
*               comme la cr�ation d'une connexion, l'ex�cution d'une requ�te
*               devant retourner un champ, une ligne ou un ensemble de ligne ou
*               encore l'affichage du temps d'ex�cution d'un requ�te, la r�cup�ration
*               du dernier enregistrement ins�r� au cours de la session...
*
* Version : 2.0
*
* Date : 22/01/2001, 15/08/2002
*
* Author : V2.0 R. CARLES
*******************************************************************************/

class database_spec
{
	/////////////////////////// VARIABLES PRIVEES /////////////////////////
	var $id;               		// l'index de la connexion � utiliser pour effectuer les requ�tes
	var $host;             		// l'adresse IP du serveur de base de donn�es
	var $port;             		// le port � utiliser pour se connecter � la base
	var $database_name;    		// le nom de la base de donn�e
	var $user;             		// le nom de l'utilisateur
	var $passwd;           		// le mot de passe de l'utilisateur
	var $info;					// les infos de version de la base en cours
	var $persistency;			// connexion persistante ou non
	
	var $debug_time = false;    // indique si ont doit afficher le temps d'ex�cution des requ�tes SQL
	var $time_deb;              // variable contenant le temps avant ex�cution de la requ�te
	var $time_end;              // variable contenant le temps apr�s ex�cution de la requ�te
	
	/////////////////////////// VARIABLES PUBLICS /////////////////////////
	var $result;           		// index de r�sultat de la derni�re requ�te ex�cut�e
	var $rows;             		// nombre de lignes renvoy�es par la derni�re requ�te
	var $fields;           		// nombre de colonnes renvoy�s par la derni�re requ�te
	var $data;             		// tableau ou texte contenant le r�sultat de la derni�re requ�te
	var $affected_rows;			// nombre de lignes affect�es par la derni�re requ�te
	
	var $last_insert_id;   		// nombre contenant l'id (champ de type AUTO_INCREMENTED) du dernier enregistrement ins�r�
	var $database_title			="MySQL";
	var $allow_bind=false;
	
	
	public function secureData ($aArray){
		if(!get_magic_quotes_gpc()){
			if(count($aArray)>0){
				foreach($aArray as $iKey => $strValue){
					if(is_array($strValue)){
						$this->secureData($strValue);
					}else{
						$aArray[$iKey] = mysql_escape_string( $strValue ) ; 
					}
				}	
			}
		}
		return $aArray  ; 
		
	}
	/////////////////////////// FONCTIONS MEMBRES /////////////////////////
	/**
	* @return database
	* @param string 	$database_name 	le nom de la base de donn�es
	* @param string 	$username 		le nom de l'utilisateur servant � la connexion
	* @param string 	$password 		le mot de passe de connexion
	* @param string 	$host 			l'adresse IP du serveur (optionnel) : 127.0.0.1 par d�faut
	* @param string 	$port 			le port de connexion (optionnel) : 3306 par d�faut
	* @desc Constructeur. Permet de se connecter � la base de donn�e
	*/
	function database_spec($database_name, $username, $password, $host = "", $port = "", $persistency = false)
	{
		$this->database_name = $database_name;
		$this->user = $username;
		$this->passwd = $password;
		$this->host = ($host?$host:"127.0.0.1");
		$this->port = ($port?$port:"3306");
		$this->persistency = $persistency;
		$this->info = $this->getInfo();
		
		// connexion � la base de donn�es
		if($this->persistency) {
			$this->id = @mysql_pconnect("$host:$port", "$username", "$password") or $this->error($this->message("Impossible de connecter au serveur ".$this->database_title));
		} else {
			$this->id = @mysql_connect("$host:$port", "$username", "$password") or $this->error($this->message("Impossible de connecter au serveur ".$this->database_title));
		}
		@mysql_select_db($database_name) or $this->error($this->message("Impossible de s&eacute;lectionner la base ".$this->database_title));
	}
	
	/**
	* @return void
	* @param string $query chaine SQL
	* @desc Ex�cute une requ�te
	*/
	function queryInit($query)
	{
		// execution de la requ�te
		$this->data = array();
		$this->type = array();
		$this->name = array();
		$this->len = array();
		
		if ($this->result = @mysql_query($query))
		{
			//nb de lignes affect�es
			$this->affected_rows=@mysql_affected_rows();
			
			// sauvegarde du nombre de champs renvoy�s par la requ�te
			$this->fields = @mysql_num_fields($this->result);
			
			// sauvegarde du nombre de lignes renvoy�s par la requ�te
			$this->rows = @mysql_num_rows($this->result);
			
			//last id
			$this->last_insert_id = @mysql_insert_id();
			
			for ($i=0; $i < $this->fields; $i++) {
				$this->type[]  = @mysql_field_type($this->result, $i);
				$this->name[]  = @mysql_field_name($this->result, $i);
				$this->len[]  = @mysql_field_len($this->result, $i);
			}
			return true;
		}
		else
		{
			$this->error($this->message("Impossible d'ex&eacute;cuter la requ&egrave;te :",$query));
			
			return false;
		}
	}
	
	/**
	* ajout� par johan MIZRAHI
	* le 21/02/2007
	*/
	function GetObject($result = null)
	{
		if (!isset($result))
			$result = $this->result;
		$object = @mysql_fetch_object($result);
		return $object;
	}
	
	/**
	* ajout� par johan MIZRAHI
	* le04/07/2007
	*/
	function Move($table_src, $table_dst, $name_id_src, $id_val)
	{
		$query = "INSERT INTO $table_dst SELECT * FROM $table_src WHERE $name_id_src='$id_val'";
		$resQ = @mysql_query($query);
		if ($resQ == false)
			$this->error($this->message("Move() : mysql_query() : Erreur lors de la  requete.<br>", $query));
		$query = "DELETE FROM $table_src WHERE $name_id_src='$id_val'";
		$resD = @mysql_query($query);
		if ($resD == false)
			$this->error($this->message("Move() : mysql_query() : Erreur lors de la  requete.<br>", $query));
		return($resQ && $resD ?  true : false);
	}
	
	/**
	* @return boolean
	* @param string $query chaine SQL
	* @desc permet d'ex�cuter une requ�te. Aucun r�sultat n'est
	renvoy� par cette fonction. Elle doit �tre utilis� pour effectuer
	des insertions, des updates... Elle est de m�me utilis�e par les
	autres fonction de la classe comme queryItem() et queryTab().
	*/
	function query($query)
	{
		
		// si on veut afficher le temps d'execution de la requete : initialisation
		if ($this->debug_time) $this->initTimeQuery();
		// execution de la requ�te
		$temp = array();
		if ($this->queryInit($query))
		{
			for ($num_ligne = 0; $num_ligne < $this->rows ; $num_ligne++)
			{
				$temp = @mysql_fetch_array($this->result,MYSQL_ASSOC) or $this->error($this->message("Erreur lors de la r&eacute;cup&eacute;ration des informations de la requ&egrave;te :",$query));
				while (list ($key, $val) = each($temp))
				{
					$data_temp[$key][$num_ligne] = $val;
				}
				$this->data=$data_temp;
			}
			return true;
		} else 
			return false;
	}
	
	/**
	* @return mixed
	* @param string $query chaine SQL
	* @desc permet d'ex�cuter une requ�te devant renvoyer une valeur
	*/
	
	function Squery($q)
	{
		$query = mysql_query($q) or $this->error($this->message("Squery() : mysql_query() : Erreur lors de la  requete.<br>".mysql_error(), $q));
		if (!$query) 
			return false;
		else {
			$this->result = $query;
			$this->rows = @mysql_num_rows($this->result);
			$this->affected_rows = @mysql_affected_rows();
			$this->last_insert_id = @mysql_insert_id();
			return $this->result;
		}
	}
	
	
	function queryItem($query)
	{
		// suppression de l'ancien r�sultat
		unset($this->data);
		
		// execution de la requete
		if ($this->queryInit($query) and $this->rows > 0)
		{
			// recuperation du r�sultat
			$tab_result = @mysql_fetch_array($this->result) or $this->error($this->message("Erreur lors de la r&eacute;cup&eacute;ration des informations de la requ&egrave;te :",$query));
			$this->data = $tab_result[0];
			
			// liberation de la memoire
			@mysql_free_result($this->result) or $this->error($this->message("Erreur lors de la lib&eacute;ration de la m&eacute;moire occup&eacute;e par le r&eacute;sultat de la requ&egrave;te :",$query));
		}
		
		// renvoie du r�sultat
		return($this->data);
	}
	
	/**
	* @return mixed
	* @param string $query chaine SQL
	* @desc permet d'ex�cuter une requ�te devant renvoyer une seule ligne de r�sultat.
	*/
	function queryRow($query)
	{
		// suppression de l'ancien r�sultat
		unset($this->data);
		
		// execution de la requete
		if ($this->queryInit($query) and $this->rows > 0)
		{
			// recuperation du r�sultat
			$this->data = @mysql_fetch_array($this->result) or $this->error($this->message("Erreur lors de la r&eacute;cup&eacute;ration des informations de la requ&egrave;te :",$query));
			
			// liberation de la memoire
			@mysql_free_result($this->result) or $this->error($this->message("Erreur lors de la lib&eacute;ration de la m&eacute;moire occup&eacute;e par le r&eacute;sultat de la requ&egrave;te :",$query));
		}
		
		// renvoie du r�sultat
		return($this->data);
	}
	
	/**
	* @return mixed
	* @param string $query chaine SQL
	* @desc permet d'ex�cuter une requ�te devant renvoyer plusieurs lignes de r�sultat.
	*/
	function queryTab($query, $bAssoc = false) {
		
		// suppression de l'ancien r�sultat
		unset($this->data);
		
		// execution de la requete
		if ($this->queryInit($query) and $this->rows > 0) {
			
			
			$sResult_type = $bAssoc ? MYSQL_ASSOC : MYSQL_BOTH;
			// recuperation du r�sultat
			for ($num_ligne = 0; $num_ligne < $this->rows ; $num_ligne++) {
				$this->data[$num_ligne] = @mysql_fetch_array($this->result, $sResult_type) 
						or $this->error($this->message("Erreur lors de la r&eacute;cup&eacute;ration des informations de la requ&egrave;te :",$query));
			}
			
			// liberation de la memoire
			@mysql_free_result($this->result) or $this->error($this->message("Erreur lors de la lib&eacute;ration de la m&eacute;moire occup&eacute;e par le r&eacute;sultat de la requ&egrave;te :",$query));
		}
		
		// renvoie du r�sultat
		return($this->data);
	}
	
	function queryParam($query,$param)
	{
		$this->query($this->putBind($query,$param));
	}
	
	/**
	* @return void
	* @desc permet de fermer la connexion avec la base
	*/
	function close()
	{
		@mysql_close($this->id) or $this->error($this->message("Impossible de fermer la connexion !"));
	}
	
	/**
	* @return void
	* @param string $query la requ�te
	* @desc permet de recuperer l'id du dernier objet ins�r� dans la base, si la requete est de type INSERT
	*/
	function getLastOid($query)
	{
		// on met la requete en minuscule sur une ligne
		$query = preg_replace("/[\s\n\r]+/", " ", trim($query));
		
		// on recupere le nom de la table dans le cas d'un INSERT
		if (preg_match("/^INSERT\s+INTO\s+([^\s]+)\s+.*/i", $query, $matches))
		{
			// on recupere le dernier enregistrement
			$this->last_insert_id = mysql_insert_id();
		}
	}
	
	/**
	* @return string
	* @param string $table Nom de la table
	* @desc permet de recuperer l'id du de la prochaine s�quence associ�e � la table
	*/
	function getNextId($table)
	{
		return true;
	}
	
	/**
	* @return mixed
	* @desc Retourne un tableau avec N� et message dela derni�re erreur
	*/
	function getError() {
		return array("code"=>@mysql_errno(),"message"=>@mysql_error());
	}
	
	/**
	* @return mixed
	* @desc Retrouve les informations c�t� client et c�t� serveur de la base de donn�es
	*/
	function getInfo() {
		$return["type"]=$this->database_title;
		//		$return["client_version"]=mysql_get_client_info();
		//		$return["server_version"]=mysql_get_server_info();
		$return["host"]=$_SERVER["SERVER_NAME"];
		$return["instance"]=$this->database_name;
		return $return;
	}
	
	/***********************************************************************/
	/**************************** PROCESSUS ********************************/
	/***********************************************************************/
	
	/**
	* @return mixed
	* @param string	$table 	Nom de la table
	* @desc Retourne un tableau avec "field","type","null","default","key","extra","increment","sequence"
	*/
	function describeTable($table) {
		$query = 'SHOW FIELDS FROM '.$table;		$i = 0;
		$result=$this->queryTab($query);
		foreach ($result as $ligne) {
			$return[$i]["field"]=$ligne["Field"];
			$return[$i]["type"]=$ligne["Type"];
			$return[$i]["null"]=($ligne["Null"]=="YES");
			$return[$i]["default"]=$ligne["Default"];
			$return[$i]["key"]=($ligne["Key"]=="PRI");
			$return[$i]["extra"]=$ligne["Extra"];
			$return[$i]["increment"]=($ligne["Extra"]=="auto_increment");
			$return[$i]["sequence"]=true;
			$i++;
		}
		return $return;
	}
	
	/**
	* @return string
	* @desc Retourne la fonction d'affichage de la date courante
	*/
	function getNow() {
		return "now()";
	}
	
	/**
	* @return string
	* @param string		$strChaine 	Date au format DD/MM/YYYY
	* @desc Formattage d'une date fran�aise au format de la base de donn�e
	*/
	function dateStringToSql($strChaine)
	{
		$arr = explode( "/", $strChaine);
		if (count($arr)>1) {
			$temp_contenu_date = "'".$arr[2]."-".$arr[1]."-".$arr[0]."'";
		} else {
			$temp_contenu_date = "'".$strChaine."'";
		}
		return($temp_contenu_date);
	}
	
	/**
	* @return string
	* @param string	$dateField 	Nom du champs date
	* @desc Formattage d'une date de la base de donn�e au format fran�ais
	*/
	function dateSqlToString($dateField)
	{
		$temp_date = "DATE_FORMAT(".$dateField.",'%d/%m/%Y')";
		return($temp_date);
	}
	
	/**
	* @return string
	* @param string	$value 	Cha�ne de caract�re
	* @desc Nettoyage des cotes pour les requ�tes SQL
	*/
	function stringToSql($value) {
		return $value;
	}
	
	/**
	* @return string
	* @param string 	$query 	Cha�ne SQL
	* @param integer 	$min 	Valeur Min
	* @param integer	$length	Nombre de lignes
	* @desc Transforme une requ�te pour qu'elle ne retourne que les lignes comprises entre la valeur $min et la valeur $max
	*/
	function getLimitedSQL($query,$min,$length) {
		$return = $query." LIMIT ".($min > 0 ? $min - 1 : $min).",".$length;
		return $return;
	}
	
	/**
	* @return string
	* @param string	$query 			Chaine SQL originale
	* @param string	$countFields 	Liste des champs sur lesquels portent le comptage (clause group by)
	* @desc Transforme une requ�te pour qu'elle retourne le comptage des lignes.
	*/
	function getCountSQL($query,$countFields="*") {
		$deb=strpos(strToLower($query), "from");
		$fin=aMin(array(strpos(strToLower($query), "order by"),strpos(strToLower($query), "group by"),strpos(strToLower($query), "having")));
		if (!$fin) {
			$fin=strlen($query);
		}
		$sql="select count(distinct ".$countFields.") ".substr($query, $deb, ($fin-$deb));
		return $sql;
	}

	/**
	* @return void
	* @param string	$table	Nom de la table impact�e
	* @param string	$key	Champ identifiant
	* @param string	$oldValue	Valeur du champ identifiant de l'enregistrement � dupliquer
	* @param string	$newValue Valeur de remplacement pour le champ identifiant
	* @desc Duplique un enregistrement d'une table sur une cl�.
	*/
	function duplicateRecord($table,$key,$oldValue,$newValue) {
		$tableTmp="ssd_tmp";
		$field_set=$this->describeTable($table);
		$strSQL = "INSERT INTO " . $table . " SELECT ";
		$j=-1;
		foreach ($field_set as $field) {
			$j++;
			if ( $field["increment"] ) {
				$auto_increment=$field["field"];
			}
			if ($field["field"]!=$key) {
				$strSQL .= $field["field"].", ";
			} else {
				$key_type=$field["type"];
				$strSQL .= $this->formatField($newValue,$field["type"]).", " ;
			}
		}
		$strSQL .= "FROM ".$tableTmp;//." WHERE ".$key."=".$this->formatField($oldValue,$key_type);
		$strSQL=str_replace(", FROM"," FROM",$strSQL);

		$this->query("DROP TABLE ".$tableTmp);
		$this->query("CREATE TABLE ".$tableTmp." SELECT * FROM ".$table." WHERE ".$key."=".$this->formatField($oldValue,$key_type));
		$this->query($strSQL);
	}
} // fin de la declaration de la classe

?>
