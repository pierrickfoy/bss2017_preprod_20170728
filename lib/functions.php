<?php

	function ScanDirectory($Directory, $aExtensions = array()){
	
		$MyDirectory = opendir($Directory) or die('Erreur');
		$aFiles = array(); 
		while($Entry = @readdir($MyDirectory)) {
			if(!is_dir($Directory.'/'.$Entry) && $Entry != '.' && $Entry != '..') {
				if(count($aExtensions)>0 ){
					$aTmp = explode(".",$Entry); 
					$aTmp = array_reverse($aTmp); 
					if(in_array($aTmp[0] ,$aExtensions))
						$aFiles[] =  $Entry;
				}else
					$aFiles[] =  $Entry;
			}
		}
		closedir($MyDirectory);
		return $aFiles ; 
	}

	/**
    * is_valid_email
    * 
    * <p>Vérification d'une adresse email, retourne true or false</p>
    * 
    * @name is_valid_email()
    * @param string $strEmail - Chaine à vérifier
    * @return bool
    */
	function is_valid_email ($strEmail){
		$Syntaxe='#^[\w.-]+@[\w.-_]+\.[a-zA-Z]{2,6}$#';  
		if(preg_match($Syntaxe,$strEmail))  
			return true;  
		else  
			return false;  
	}
	
	/**
    * LoadConfig
    * 
    * <p>Permet de créer le tableau des contantes à partir de la table configuration de la base de données</p>
    * 
    * @name LoadConfig()
    * @param &$aCONST - Adresse du tableau global contenant les constante
    * @return void
    */
    function LoadConfig(&$aCONST) {
		global $oDb;	
		$aConfig = $oDb->queryTab('SELECT config_code, config_value FROM eco_config');
		foreach ($aConfig as $aConfigLine) {
			$aCONST[$aConfigLine['config_code']] = $aConfigLine['config_value'];
		}
	}
	
	/**
    * StrToURL
    * 
    * <p>transforme une string en format url</p>
    * 
    * @name StrToURL()
    * @param string $strWord - Chaine à transformé
    * @return str
    */
     function StrToURL($strWord) {
    	//les balises
    	$strWord = strip_tags(html_entity_decode($strWord));
		//les accents 
		$strWord=trim(($strWord)); 
		$aReplace = array(	'À' => 'a' ,'Á' => 'a' ,'Â' => 'a' ,'Ã' => 'a' ,'Ä' => 'a' ,'Å' => 'a' ,'à' => 'a' ,'á' => 'a' ,'â' => 'a' ,'ã' => 'a' ,'ä' => 'a' ,'å' => 'a' ,
				'Ò' => 'o' ,'Ó' => 'o' ,'Ô' => 'o' ,'Õ' => 'o' ,'Ö' => 'o' ,'Ø' => 'o' ,'ò' => 'o' ,'ó' => 'o' ,'ô' => 'o' ,'õ' => 'o' ,'ö' => 'o' ,'ø' => 'o' ,
				'È' => 'e' ,'É' => 'e' ,'Ê' => 'e' ,'Ë' => 'e' ,'è' => 'e' ,'é' => 'e' ,'ê' => 'e' ,'ë' => 'e' ,'Ç' => 'c' ,'ç' => 'c' ,'Ì' => 'i' ,'Í' => 'i' ,
				'Î' => 'i' ,'Ï' => 'i' ,'ì' => 'i' ,'í' => 'i' ,'î' => 'i' ,'ï' => 'i' ,'Ù' => 'u' ,'Ú' => 'u' ,'Û' => 'u' ,'Ü' => 'u' ,'ù' => 'u' ,'ú' => 'u' ,
				'û' => 'u' ,'ü' => 'u' ,'ÿ' => 'y' ,'Ñ' => 'n' ,'ñ' => 'n', '!' =>'', '?'=>'' );
		
		$strWord =  strtr_unicode($strWord, $aReplace ) ; 
		//les caracètres spéciaux (aures que lettres et chiffres)
		$strWord = preg_replace('/([^a-z0-9]+)/i', '-', $strWord);
		return strtolower($strWord);	
    }
	
    /**
    * strtr_unicode
    * 
    */
	function strtr_unicode($str, $a = null, $b = null) {
		$translate = $a;
		if (!is_array($a) && !is_array($b)) {
			$a = (array) $a;
			$b = (array) $b;
			$translate = array_combine(
				array_values($a),
				array_values($b)
			);
		}
		// again weird, but accepts an array in this case
		return strtr($str, $translate);
	}
	
	/**
    * get_meta_title_desc
    * 
    * <p>Ecrit les balise méta de la page en fonction du template</p>
    * 
    * @name get_meta_title_desc()
    * @return void
    */	
	function get_meta_title_desc() {
		global $oDb;
		global $_GET;
		global $_CONST;
		global $_SESSION;
		
		$aRobots = array(1 => "noindex,follow", 2 => "index,nofollow", 3 => "noindex,nofollow"); 
		$strCanonical = ''; 
		$strLinkPrev='';
		$strLinkNext='';
		$strRobots='';
		//Si templates_id vide alors par défaut = acueil
		if(!isset($_GET["templates_id"]) || empty($_GET["templates_id"])){
			$iTemplate = 1 ; 
		}else	
			$iTemplate = mysql_real_escape_string($_GET["templates_id"]) ;
		
		//var_dump($iTemplate);
		if($iTemplate == 14){
			//Page classes -> Classe 
			$aInfos = $oDb->queryRow("SELECT * FROM bor_classe_page WHERE classe_page_url ='/".$_GET['classe_url']."/'");
			$aInfosPage["templates_meta_title"] = !empty($aInfos['classe_page_metatitle']) ? $aInfos['classe_page_metatitle'] : $aInfos['classe_page_titre'] ;
			$aInfosPage["templates_meta_desc"] =  !empty($aInfos['classe_page_metadesc']) ? strip_tags($aInfos['classe_page_metadesc']) : strip_tags ($aInfos['classe_page_titre']) ;
			if(isset($aInfos["classe_url_canonical"]) && $aInfos["classe_url_canonical"] != "")
				$strCanonical = $aInfos['classe_url_canonical'];
		}
		else if($iTemplate == 16){
			//Page matiere
			$aInfos = $oDb->queryRow("SELECT * FROM bor_matiere_page WHERE matiere_page_url ='/".$_GET['matiere_url']."/'");
			$aInfosPage["templates_meta_title"] = !empty($aInfos['matiere_page_metatitle']) ? $aInfos['matiere_page_metatitle'] : $aInfos['matiere_page_titre'] ;
			$aInfosPage["templates_meta_desc"] =  !empty($aInfos['matiere_page_metadesc']) ? strip_tags($aInfos['matiere_page_metadesc']) : strip_tags ($aInfos['matiere_page_titre']) ;
			if(isset($aInfos["matiere_url_canonical"]) && $aInfos["matiere_url_canonical"] != "")
				$strCanonical = $aInfos['matiere_url_canonical'];
		}
		else if($iTemplate == 15){
			//Page matiere - classe
			$iClasse = $oDb->queryItem("SELECT classe_page_id FROM bor_classe_page WHERE classe_page_url ='/".$_GET['classe_url']."/'");
			$iMatiere = $oDb->queryItem("SELECT matiere_id FROM bor_matiere_page WHERE matiere_page_url ='/".$_GET['matiere_url']."/'");
			$aInfos = $oDb->queryRow("SELECT * FROM bor_matiere_classe_edito WHERE matiere_id =".$iMatiere." AND classe_id =".$iClasse);
			$aInfosPage["templates_meta_title"] = !empty($aInfos['mc_metatitle']) ? $aInfos['mc_metatitle'] : $aInfos['mc_titre'] ;
			$aInfosPage["templates_meta_desc"] =  !empty($aInfos['mc_metadesc']) ? strip_tags($aInfos['mc_metadesc']) : strip_tags ($aInfos['mc_titre']) ;
			if(isset($aInfos["mc_url_canonical"]) && $aInfos["mc_url_canonical"] != "")
				$strCanonical = $aInfos['mc_url_canonical'];
		}
		else if($iTemplate == 55){
			//Page chapitre
			$aInfos = $oDb->queryRow("SELECT * FROM bor_chapitres WHERE chapitre_alias ='/".$_GET['chapitre_url']."/'");
			$aInfosPage["templates_meta_title"] = !empty($aInfos['chapitre_metatitle']) ? $aInfos['chapitre_metatitle'] : $aInfos['chapitre_h1'] ;
			$aInfosPage["templates_meta_desc"] =  !empty($aInfos['chapitre_metadesc']) ? strip_tags($aInfos['chapitre_metadesc']) : strip_tags ($aInfos['chapitre_h1']) ;
			if(isset($aInfos["chapitre_url_canonical"]) && $aInfos["chapitre_url_canonical"] != "")
				$strCanonical = $aInfos['chapitre_url_canonical'];
		}
		else if($iTemplate == 54){
			
			if($_GET['produit'] == "ciblee")
				$fiche = "formule CIBLÉE";
			else if($_GET['produit'] == "reussite")
				$fiche = "formule RÉUSSITE";
			else if($_GET['produit'] == "tribu")
				$fiche = "formule TRIBU";
			$aInfosPage["templates_meta_title"] = "Abonnement ".$fiche;
		}else if($iTemplate == 56){
		
			//Page notions
			$aInfos = $oDb->queryRow("SELECT * FROM bor_notions WHERE notions_url_seo ='/".$_GET['notion_url']."/'");
			$aInfosPage["templates_meta_title"] = !empty($aInfos['notions_metatitle']) ? $aInfos['notions_metatitle'] : $aInfos['notions_h1'] ;
			$aInfosPage["templates_meta_desc"] =  !empty($aInfos['notions_metadesc']) ? strip_tags($aInfos['notions_metadesc']) : strip_tags ($aInfos['notions_h1']) ;
			if(isset($aInfos["notions_url_canonical"]) && $aInfos["notions_url_canonical"] != "")
				$strCanonical = $aInfos['notions_url_canonical'];
		}else if ($iTemplate == 9){
			$pagination = "";
			if(isset($_GET['page']))
				$pagination = $_GET['page'];
			if($_GET['id_rubrique'] != 0){
				$strSQLList = "	SELECT rubrique_titre, rubrique_description
								FROM bor_rubrique
								WHERE rubrique_id = ".$_GET['id_rubrique']."";
				$strresult=$oDb->queryRow($strSQLList);
				if($pagination != "")
					$aInfosPage["templates_meta_title"] = $strresult['rubrique_titre'].' - page '.$pagination;
				else
					$aInfosPage["templates_meta_title"] = $strresult['rubrique_titre'];
			}else{
				if($pagination != "")
					$aInfosPage["templates_meta_title"] = 'Actualités Bordas Soutien Scolaire - page '.$pagination;
				else
					$aInfosPage["templates_meta_title"] = "Actualités Bordas Soutien Scolaire";
			}

		}else if ($iTemplate == 4){
			$aInfos = $oDb->queryRow("SELECT r.rubrique_titre, r.rubrique_id, a.article_id, a.article_titre, a.article_soustitre, a.article_metatitle, a.article_metadesc FROM bor_rubrique r INNER JOIN bor_article a ON (a.rubrique_id = r.rubrique_id) WHERE a.article_id = '".mysql_real_escape_string($_GET["article_id"])."'"); 
			$aInfosPage["templates_meta_title"] = !empty($aInfos['article_metatitle']) ? $aInfos['article_metatitle'] : $aInfos['article_titre'] ;
			$aInfosPage["templates_meta_desc"] =  !empty($aInfos['article_metadesc']) ? strip_tags($aInfos['article_metadesc']) : strip_tags ($aInfos['article_soustitre']) ;
			/*$strCanonical = $_CONST['URL_ACCUEIL'].'rubrique-'.strToUrl($aInfos['rubrique_titre']).'-'.$aInfos['article_id'].'-'.strToUrl($aInfos['article_titre']).'.html' ;*/ 
			$strCanonical = $_SERVER['REQUEST_URI'];
			$strImageFb = $_SERVER['SERVER_NAME'].'/img/logo_soutien.editions-bordas.png';
			$metaFb  = '<meta content="'.$_CONST['URL2'].$strCanonical.'" property="og:url">';
			$metaFb .= '<meta content="'.$strImageFb.'" property="og:image"> <!-- image en 200x200px -->';
			$metaFb .= '<meta content="'.$aInfos['article_metatitle'].'" property="og:title">';
			$metaFb .= '<meta content="'.$aInfos['article_metadesc'].'" property="og:description">';
		}else{
			//Récupération des méta pour le template
			$strSQLSearch = "SELECT tl.templates_meta_title, tl.templates_meta_desc, tl.templates_meta_keywords, t.templates_meta_robot, t.templates_name
							 FROM eco_templates_lang tl
							 INNER JOIN eco_templates t ON (t.templates_id = tl.templates_id)
							 WHERE tl.templates_id='".mysql_escape_string($iTemplate)."' 
							 AND tl.langs_id='".mysql_escape_string($_SESSION["langs_id"])."'";
			$aInfosPage = $oDb->queryRow($strSQLSearch);
			if($oDb->rows>0){
				$aInfosPage["templates_meta_title"] = $aInfosPage["templates_meta_title"];
				$aInfosPage["templates_meta_desc"] = $aInfosPage["templates_meta_desc"];
				if($aInfosPage['templates_meta_robot'] > 0 )
					$strRobots =  "\n<meta name=\"robots\" content=\"".$aRobots[$aInfosPage['templates_meta_robot']]."\">\n";
				else
					$strRobots =  "\n<meta name=\"robots\" content=\"index\">\n";
			}		
			/*if ($iTemplate == 1)
				$strCanonical = $_CONST['URL_ACCUEIL'] ;
			else
				$strCanonical = $_CONST['URL_ACCUEIL'].strToUrl($aInfosPage['templates_name']).'.html' ;*/
		}	
		
		if(empty($aInfosPage["templates_meta_title"])){	
			$aInfosPage["templates_meta_title"] = $_CONST["default_title"];
			$aInfosPage["templates_meta_desc"] = $_CONST["default_desc"];
			$strRobots =  "\n<meta name=\"robots\" content=\"index\">\n";
		}

if ($iTemplate == 1)
	$strCanonique = $_CONST['URL_ACCUEIL'];
/*else if($_GET["templates_id"] == 9)
{
	if(isset($_GET["id_rubrique"]) && $_GET["id_rubrique"] == 0)
	{
		$strCanonique = '/actualite/';
	}else
	{
		$strSql = "SELECT rubrique_titre FROM bor_rubrique WHERE rubrique_id = '".$_GET["id_rubrique"]. "'"; 
		$aRubrique = $oDb->queryItem($strSql);
		$iTitre_rubrique = strToUrl($aRubrique);
		$strCanonique = '/actualite/'.$iTitre_rubrique.'/';
	}
}	*/
else
	$strCanonique = $_SERVER['REQUEST_URI'];


if($strCanonical == ""){
	echo '<link rel="canonical" href="'.$_CONST['URL2'].$strCanonique.'">';
}else{
	if(substr( $strCanonical, 0, 7 ) === "http://"){
		echo '<link rel="canonical" href="'.$strCanonical.'">';
	}
	else{
		echo '<link rel="canonical" href="'.$_CONST['URL2'].$strCanonical.'">';
	}
}
?>
<?php
if ($iTemplate == 1){ // accueil
?>
<title><?php echo strip_tags($aInfosPage["templates_meta_title"]) .' | '. $_CONST["SITE_NAME"]; ?></title>
<?php
}else{ // reste du site
?>
<title><?php echo strip_tags($aInfosPage["templates_meta_title"]); ?></title>
<?php
}
?>
<meta name="description" content="<?php echo strip_tags($aInfosPage["templates_meta_desc"]); ?>" />
<meta name="publisher" content="<?php echo $_CONST["META_PUBLISHER"]; ?>" />
<?php echo $strRobots; ?>
<meta name="Author" LANG="fr" content="<?php echo $_CONST["META_AUTHOR"]; ?>">
<meta name="language" content="FR-fr" />
<!--<meta name="google-site-verification" content="<?php echo $_CONST["G_VERIF"]; ?>" />-->
<?php
if(isset($metaFb) && !empty($metaFb)){
				echo $metaFb;
}
?>
<?php 
}
	
	/**
    * get_page
    * 
    * <p>Permet d'afficher le contenu d'une page</p>
    * 
    * @name get_page
    * @return void
    */	
	function get_page() {
		global $_GET;
		global $oDb;
		global $_CONST;
		global $_SESSION;
		global $aLang;
		
		//Si templates_id vide alors par défaut = acueil
		if(!isset($_GET["templates_id"]) || empty($_GET["templates_id"])){
			$iTemplate = 1 ; 
		}else	
			$iTemplate = mysql_real_escape_string($_GET["templates_id"]) ;
		
		$strSQLSearch = "SELECT templates_title, templates_content, templates_filename
						 FROM eco_templates t
						 INNER JOIN eco_templates_lang tl ON (tl.templates_id=t.templates_id)
						 WHERE tl.templates_id='".mysql_escape_string($iTemplate)."' 
						 AND tl.langs_id='".mysql_escape_string($_SESSION["langs_id"])."'";
		$aPage = $oDb->queryRow($strSQLSearch);	
		
		if (empty($aPage['templates_filename'])) {
			echo $aPage['templates_content'];
		}	
		elseif (!empty($aPage['templates_filename']) && is_file('./templates/'.$aPage['templates_filename'])) {
			include ('./templates/'.$aPage['templates_filename']);
		} else
			include ('./templates/form_accueil.php');
	}
	
	/**
    * get_menu
    * 
    * <p>Retourne le menu</p>
    * 
    * @name get_menu
    * @param integer - GROUP ID
    * @return void
    */		
	function get_menu($iGroupmenu, $bResponsive=false){
		global $oDb;
		if(empty($_GET["templates_id"]))
			$iCurrentMenu = 1 ; 
		else
			$iCurrentMenu = $_GET["templates_id"];
		$oDb->query("SET NAMES utf8"); 
		$aMenuItems = $oDb->queryTab("	SELECT * FROM eco_menu m 
										INNER JOIN eco_menu_lang ml ON(ml.menu_id=m.menu_id)
										LEFT JOIN eco_templates_lang tl ON (tl.templates_id=m.templates_id AND tl.langs_id='".mysql_escape_string($_SESSION["langs_id"])."' )
										WHERE menu_group_id='".mysql_escape_string($iGroupmenu)."' 
										AND ml.langs_id='".mysql_escape_string($_SESSION["langs_id"])."'
										AND menu_parent_id='0'
										GROUP BY m.menu_id
										ORDER BY m.menu_ordre
										");
		$aMenus = array(); 
		foreach ($aMenuItems AS $aMenu) {  
			$strLink = !empty($aMenu['menu_link']) ? $aMenu['menu_link'] : '/'.StrToURL(($aMenu['templates_title'])).'.html';			 
			$bActive =($aMenu['templates_id'] == $iCurrentMenu) ? true : false; 
			$aMenus[] = array 	(
									"menu_id" => $aMenu['menu_id'],
									"menu_title" => $aMenu['menu_title'],
									"menu_class" => $aMenu['menu_class'],
									"menu_special" => $aMenu['menu_special'],
									"menu_link" => $strLink,
									"menu_active" => $bActive
								);
		}
		return $aMenus; 
	}
	
	/**
    * get_template_title
    * 
    * <p>retourne le titre du template actuel</p>
    * 
    * @name get_template_title
    * @param string - nom du template
    * @return str
    */	
	function get_template_title($iTemplate = "", $bOnlyTitle = true) {
		global $oDb;
		global $_GET;
		
		if (empty($iTemplate))
			$iTemplate = (empty($_GET["templates_id"]) ? 1 : $_GET["templates_id"]);
		if($bOnlyTitle){
			$strSQLSearch = "SELECT tl.templates_title 
							 FROM eco_templates t
							 INNER JOIN eco_templates_lang tl ON (tl.templates_id=t.templates_id)
							 WHERE tl.templates_id='".mysql_escape_string($iTemplate)."' 
							 AND tl.langs_id='".mysql_escape_string($_SESSION["langs_id"])."'";
			$strTitle = $oDb->queryItem($strSQLSearch);
		}else{
			$strSQLSearch = "SELECT t.templates_name as  templates_title
							 FROM eco_templates t
							 INNER JOIN eco_templates_lang tl ON (tl.templates_id=t.templates_id)
							 WHERE tl.templates_id='".mysql_escape_string($iTemplate)."' 
							 AND tl.langs_id='".mysql_escape_string($_SESSION["langs_id"])."'";
			$strTitle = $oDb->queryItem($strSQLSearch);
		}
		return($strTitle);
	}

	
	/**
    * get_template_data
    * 
    * <p>retourne le champ cms_html de la table eco_cms du template</p>
    * 
    * @name get_template_data
    * @param integer - ID du template
    * @return str
    */
	function get_template_data($iTemplate="") {
		global $oDb;
		
		if (empty($iTemplate))
			$iTemplate = (empty($_GET["templates_id"]) ? 1 : $_GET["templates_id"]);
			
		$strSQLSearch = "SELECT templates_content
						 FROM eco_templates t
						 INNER JOIN eco_templates_lang tl ON (tl.templates_id=t.templates_id)
						 WHERE tl.templates_id='".mysql_escape_string($iTemplate)."' 
						 AND tl.langs_id='".mysql_escape_string($_SESSION["langs_id"])."'";
		$strBuffer = $oDb->queryItem($strSQLSearch);	
		
		return($strBuffer);
	}
        
        /**
    * get_datablocks
    * 
    * <p>retourne le champ datablocks_content de la table eco_datablocks selon le nom demandé</p>
    * 
    * @name get_datablocks
    * @param string - nom du block
    * @return str
    */
	function get_datablocks($strName) {
		global $oDb;
		$strSQLSearch = "SELECT datablocks_content
						 FROM eco_datablocks 
						 WHERE datablocks_name='".mysql_escape_string($strName)."'";
		$strBuffer = $oDb->queryItem($strSQLSearch);
		return($strBuffer);
	}
	
	
	/**
	* get_template_filename
	* Retourne le nom du fichier php pas d'extension ni suffix (form_)
	*/
	function get_template_filename($iTemplate = "") {
		global $oDb;
		global $_GET;
		
		if (empty($iTemplate))
			$iTemplate = (empty($_GET["templates_id"]) ? 1 : $_GET["templates_id"]);
		
		$strSQLSearch = "SELECT templates_filename
						 FROM eco_templates t
						 INNER JOIN eco_templates_lang tl ON (tl.templates_id=t.templates_id)
						 WHERE tl.templates_id='".mysql_escape_string($iTemplate)."' 
						 AND tl.langs_id='".mysql_escape_string($_SESSION["langs_id"])."'";
		$strTitle = $oDb->queryItem($strSQLSearch);
		$strTitle = explode('.',$strTitle);
		$strTitle = mb_substr($strTitle[0],5);
		return($strTitle);
	}
	
	function add_avis(){
	
		global $oDb;
		global $_GET;
		extract($_POST);
		$strSQLSearch ="INSERT INTO `bor_avis` (`avis_id`,`avis_actif`, `avis_nom`, `avis_prenom`, `avis_description`, `avis_date_add`) 
		VALUES (NULL, '0', '".mysql_real_escape_string($nom)."', '".mysql_real_escape_string($prenom)."', '".mysql_real_escape_string($description)."', now());";
	
	if( mysql_query($strSQLSearch)) return true;
	 
	return false;
		
	}	
	
	
?>