<?php
######################################################################################## 
########################################################################################
###
###  Mail Class compatible with everymail client, every support.
###  This class allows you to send files (in progress)	 
###  	 
###	 @name mail 
###	 @author Edoaurd Kombo <edouard.kombo@live.fr>
###
######################################################################################## 
########################################################################################

class intyMailer {

	protected $_to					= '';
	protected $_reply_to			= '';
	protected $_recipient_syntax	= '';
	protected $_sender_syntax		= '';
	protected $_from				= '';
	protected $_subject				= 'none';
	protected $_message				= 'none';
	protected $_is_html				= false;
	protected $_confirmation		= false; 	//acknowledgement // accus� de r�ception
	protected $_headers;
	protected $_attachement;
	protected $_cc					= false;
	protected $_bcc					= false;
	protected $_priority			= false;
	protected $_charset				= 'utf-8';
	protected $_organization		= 'http://www.mywebsite.com';
	protected $_boundary;
	protected $_files				= false;
	protected $_result				= false;
	
	function __construct(){
	
		$this->_boundary 			= md5(uniqid(microtime(), TRUE));
	}
	
	function __destruct(){
	}
	
	
	///////////////////////////////////////////
	///////////////////////////////////////////
	// BACKGROUND METHODS
	///////////////////////////////////////////
	///////////////////////////////////////////	
	
		//We check email syntax
		private function isEmail($email)
		{
			
			return $result = (filter_var($email, FILTER_VALIDATE_EMAIL)) ? true : false ;
		}

		private function joinFiles(){
		
			$msg = '';
			
			if(is_array($this->_files))
			{
				foreach($this->_files as $key => $var)
				{
					$msg = $this->includeFiles($var);	
				}	
			
			} else {
			
				$msg = $this->includeFiles($this->_files);	
			}
			
			return $msg;
		}
		
		private function includeFiles($file_name){
		
			$msg = '';
			
			if(file_exists($file_name))
			{
				$file_type = filetype($file_name);
				$file_size = filesize($file_name);

				$handle = fopen($file_name, 'r') or die('File '.$file_name.'can t be open');
				$content = fread($handle, $file_size);
				$content = chunk_split(base64_encode($content));
				$f = fclose($handle);

				$msg .= '--'.$this->_boundary."\n";
				$msg .= 'Content-type:'.$file_type.';name='.$file_name."\n";
				$msg .= 'Content-transfer-encoding:base64'."\n";
				$msg .= $content."\n";
			}

			return $msg;
		}
	
	
	///////////////////////////////////////////
	///////////////////////////////////////////
	// SETTERS
	///////////////////////////////////////////
	///////////////////////////////////////////		
		
		
		private function setHeaders(){ //Mail headers
		
			$headers  = '';
			$headers .= 'From: '.$this->_sender_syntax.'' . "\n";	
			$headers .= 'Reply-To: '.$this->_reply_to.'' . "\n";	
			
			$headers .= 'MIME-Version: 1.0' . "\n";		
			
			//If we send a file
			if($this->_files)
			{
					
				$headers .= 'Content-Type: multipart/mixed;boundary='.$this->_boundary."\n";		
				$headers .= "\n";		
			
			} else {
				
				//Html or non Html mail
				if($this->_is_html){

					$headers .= 'Content-type: text/html; charset="'.$this->_charset.'"' . "\n";
				
				} else {
				
					$headers .= 'Content-type: text/plain; charset="'.$this->_charset.'"' . "\n";
				}		
			}
			$headers .= "\tformat=flowed;\n"; 
			$headers .= "\tcharset=\"".$this->_charset."\";\n"; 
			$headers .= "\treply-type=original\n"; 		
			$headers .= 'Content-Transfer-Encoding: 8bit'. "\n"; 
			
			//Carbone copy
			if(!empty($this->_cc)){
			
				$headers .= 'Cc: '.$this->_cc.'' . "\n";
			}
			
			//BCC
			if(!empty($this->_bcc)){
			
				$headers .= 'Bcc: '.$this->_bcc.'' . "\n";
			}		
			
			$headers .= 'Organization: '.$this->_organization.'' . "\n";
			$headers .= 'X-Mailer: PHP/'.phpversion().'' . "\n";
			
			//Mail confirmation
			if($this->_confirmation){
				
					$headers .= 'X-Confirm-Reading-To: '.$this->_from.'' . "\n";
			}
			
			//Priority of the mail
			if($this->_priority){
				
					$headers .= 'X-Priority: '.$this->_priority.'' . "\n";
			}
			
			return $this->_headers = $headers;
		}		
		
		function setFile($file){ //Add attachment
			
			try{
			
				if(empty($file)){

					$error = 'Attachment can not be empty.';
					throw new Exception($error);			
				
				} else {
				
					return $this->_files = $file;
				}		
			
			} catch(Exception $e) {
			
				echo 'Exception caught by method "'.__FUNCTION__.'": '.$e->getMessage()."\n";				
			}
		}	
		
		
		function setFormat($state){ //Set mail format (html or not)
		
			return $this->_is_html  = $state;
		}	
		
		
		//Priority of the mail (level between 1 and 3)
		private function setPriority($value){
		
			try{
			
				if(is_numeric($value)){
				
					if(($value <= 3) && ($value > 0)){
					
						return $this->_priority = $value;
					
					} else {
					
						$error = 'Priority degree must be between 1 and 3.';
						throw new Exception($error);				
					}
					
				} else {
			
					$error = 'Priority degree must be a number.';
					throw new Exception($error);			
				}		
			
			} catch(Exception $e) {
			
				echo 'Exception caught by method "'.__FUNCTION__.'": '.$e->getMessage()."\n";			
			}
		}	
		
		private function setCharset($charset){
		
			return $this->_charset = $charset;
		}	
		
		
		private function setReceptionAlert($state){ //Acknowledgment
		
			return $this->_confirmation = $state;
		}	
		
		private function setReplyTo($mail){
		
			try {
			
				if($this->isEmail($mail)){	
					
					$this->_reply_to = $mail;
				
				} else {
				
					$error = 'Email to reply to "'.$mail.'" is not a valid email address.';
					throw new Exception($error);				
				}
			
			} catch(Exception $e) {
			
				echo 'Exception caught by method "'.__FUNCTION__.'": '.$e->getMessage()."\n";		
			}
		}	
		
		private function setSending($nature , $user , $to){
		
			try {
			
				if($this->isEmail($to)){	
					
					if($nature == 'FROM'){ //Sender
					
						$this->_from			= strip_tags($to);
						
						$testUser				= (empty($user)) ? $this->_from : utf8_decode(strip_tags($user)) ;
						
						$this->_sender_syntax   = $testUser.' <'.$this->_from.'>';
					
					} else { //Receiver
					
						$this->_to				=  strip_tags($to);
						
						$testUser				= (empty($user)) ? $to : $user ;
						
						$this->_recipient_syntax= (empty($this->_recipient_syntax)) ? $testUser.' <'.$to.'>' : $testUser.' <'.$to.'>, ' ;
					
					}
				
				} else {
				
					$error = 'Email "'.$to.'" is not a valid email address.';
					throw new Exception($error);			
				}
			
			} catch(Exception $e) {
			
				echo 'Exception caught by method "'.__FUNCTION__.'": '.$e->getMessage()."\n";
			}
		}
		
		
		private function setCopy($nature , $emails){
		
			try{
			
				$mails = (!empty($emails)) ? strip_tags($emails) : false ;
				
				if(!$mails){
				
					$error = $nature.' can not be empty.';
					throw new Exception($error);
				
				} else {

					if($nature == 'BCC'){
					
						$this->_bcc = $emails;
					
					} else {
					
						$this->_cc = $emails;
					}
				}
			
			} catch(Exception $e) {
			
				echo 'Exception caught by method "'.__FUNCTION__.'": '.$e->getMessage()."\n";
			}	
		}

		private function setSubject($subject){
		
			return $this->_subject = (empty($subject)) ? $this->_subject : '=?UTF-8?Q?'.strip_tags($subject).'?=' ; //Encoding for mail clients
		}
		
		private function setMessage($message){
		
			$thisMessage = (empty($message)) ? $this->_message : $message ;
			$thisMessage = (!$this->_is_html) ? wordwrap($thisMessage , 70) : $thisMessage ; //Cut sentences every 70 caracters if non html text
			
			return $this->_message = $thisMessage;
		}
	
	
	///////////////////////////////////////////
	///////////////////////////////////////////
	// PUBLIC METHODS
	///////////////////////////////////////////
	///////////////////////////////////////////	
	
		function set($args){
			$result = null;
			switch($args[0]){
				
				case 'copy': $result = $this->setCopy($args[1] , $args[2]);			    
				break;
				case 'sending': $result = $this->setSending($args[1] , $args[2] , $args[3]);	
				break;
				case 'subject': $result = $this->setSubject($args[1]);			        
				break;			
				case 'message': $result = $this->setMessage($args[1]);		
				break;
				case 'charset': $result = $this->setCharset($args[1]);		
				break;
				case 'priority': $result = $this->setPriority($args[1]);		
				break;
				case 'replyTo': $result = $this->setReplyTo($args[1]);		
				break;
				case 'alert': $result = $this->setReceptionAlert($args[1]);		
				break;
				case 'html': $result = $this->setFormat($args[1]);		
				break;
				case 'attachment': $result = $this->setFile($args[1]);		
				break;			
			}
			return $result;
		}	
	
		function send(){
			
			try{
			
				//if(!mail($this->_to , $this->_subject , $this->_message.$this->joinFiles() , $this->setHeaders())){
				if(!mail($this->_to , $this->_subject , $this->_message , $this->setHeaders())){
				
					$error = 'Unable to send the mail.';
					throw new Exception($error);
				}			
			
			} catch(Exception $e) {
			
				echo 'Exception caught by method "'.__FUNCTION__.'": '.$e->getMessage()."\n";
			}
		}
}