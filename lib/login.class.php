<?php
 class EcoLogin {
 	var $strUserTable = "eco_bo_users";
 	var $strLoginField = "users_login";
 	var $strPwdField = "users_pwd";
 	var $strTitle = "";
 	var $strName = "";
 	var $strSubmitValue = "";
 	var $iErrorID = 0;
 	
 	function __construct($strName, $strTitle="BEV BackOffice", $strSubmitValue = "Connexion") {
 		$this->strName = $strName;
 		$this->strTitle = $strTitle;
 		$this->strSubmitValue = $strSubmitValue;
 	}
 	
 	/**
 	 * GetErrorMessage - retourne le message d'erreur correspondant au num�ro d'erreur.
 	 * 
 	 * @name GetErrorMessage
 	 * @param integer $iErrorID
 	 * @return string
 	 */
 	 private function GetErrorMessage($iErrorID) {
 	 	$aErrorsCase = array (
 	 		1 => "Your login or password is empty.",
 	 		2 => "SQL statement error: ".mysql_error(),
 	 		3 => "This login is unknown.",
 	 		4 => "Your login was found but your password is wrong.",
 	 		5 => "More than one user found - we can't logged in for security reasons."
 	 	);
 	 	
 	 	return($aErrorsCase[$iErrorID]);
 	 }
 	 
 	/**
 	 * GetForm - retiourne dans une string le code html du formulaire de login
 	 * @name GetForm
 	 * @param $strTitle
 	 * @param $strSubmitValue
 	 * @return string
 	 */
 	public function GetForm() {
	
		$strForm = '<div class="isolate">
				
				<div id="login_box" class="center" style="display:none;">
					<div class="main_container clearfix">
					'.(!empty($this->iErrorID) ? '<div class="alert dismissible alert_red"><img height="24" width="24" src="images/icons/small/white/alert_2.png">'.$this->GetErrorMessage($this->iErrorID).'</div>' : '').'
						<div class="box">
							<div class="block">
								<div class="section">
									<div class="alert dismissible alert_light">
										<img width="24" height="24" src="images/icons/small/grey/locked.png">
										<strong>'.$this->strTitle.'</strong>
									</div>
								</div>	
								<form action="index.php" class="validate_form" name="LoginForm" method="post">
								<fieldset class="label_side">
									<label for="username_field">Login<span></span></label>
									<div>
										<input type="text" id="username_field" class="required" name="LoginForm_login">
									</div>
								</fieldset>						
								<fieldset class="label_side">
									<label for="password_field">Password</label>
									<div>
										<input type="password" id="password_field" class="required"  name="LoginForm_pwd">
									</div>
								</fieldset>
								<fieldset class="no_label">																											
									<div style="">
										
										<button type="submit" name="LoginForm_submit" style="margin-left: 150px;">Connexon</button>
									</div>
								</fieldset>
								</form>	
							</div>
						</div>
						<a href="#" id="login_logo"><span>EcomiZ</span></a>
						<button data-dialog="dialog_register" class="on_dark dark dialog_button" style="float:right; margin:-30px 0 0 0;"
						onclick="window.location.href=\'mailto:support@ecomiz.com\'">
							<img src="images/icons/small/white/user.png">
							<span>Acc�s perdu ?</span>
						</button>
					</div>
					<div class="main_container clearfix" style="display:none;">
						<div class="box">
							<div class="block">
								<div class="section">
									<div class="alert dismissible alert_light">
										<img width="24" height="24" src="images/icons/small/grey/locked.png">
										<strong>'.$this->strTitle.'</strong>
									</div>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>';
		
 		/*$strForm ="
 		<div id='LoginBox_main'>
	 		<div id='LoginBox_logo'></div>
	 		<div id='LoginBox_title'>".$this->strTitle."</div>
	 		<div id='LoginBox_error'>".(!empty($this->iErrorID) ? $this->GetErrorMessage($this->iErrorID) : "")."</div>
	 		<div id='LoginBox_form'>
	 		 <form name='LoginForm' method='post' action='index.php'>
		 		<table id='LoginBox_formTable'>
			 		<tr class='LoginBox_formLine'>
			 			<!-- <td id='LoginBox_formColLeft'>Login :</td> -->
			 			<td class='LoginBox_formColRight'><input type='text' name='LoginForm_login' value='Votre login' onclick=\"this.value=''\" /></td>
			 		</tr>
			 		<tr class='LoginBox_formLine'>
			 			<!-- <td id='LoginBox_formColLeft'>Password :</td> -->
			 			<td class='LoginBox_formColRight'><input type='password' name='LoginForm_pwd' value='motpasse' onclick=\"this.value=''\" /></td>
			 		</tr>
			 		<tr class='LoginBox_formLine'>
			 		
			 			<td class='LoginBox_formButton' colspan='2'><input type=\"submit\" name=\"LoginForm_submit\" value=\"Connexion\" class=\"connexion-v\" style=\"margin:0 auto;\" /> </td>
			 		</tr>
			 	</table>
			 </form>
	 		</div>
 		</div>";*/
 		return($strForm);
 	}
 	
 	/**
 	 * ShowForm - affiche le formulaire de login.
 	 * @name ShowForm
 	 * @param string $strTitle
 	 * @param string $strSubmitValue
 	 * @return void
 	 */
 	public function ShowForm() {
 		echo $this->GetForm();
 	}
 	
 	/**
 	 * LogMe - Log un utilisateur en sesssion
 	 * <p>R�cup�re les informations utilisateurs en fonction des informations de login pass�s.<br>
 	 * En cas d'echec une erreur est renvoy�e.</p>
 	 * 
 	 * @name LogMe
 	 * @param string $strLogin
 	 * @param string $strPwd
 	 * @return bollean
 	 */
 	public function LogMe($strLogin, $strPwd) {
 		if (empty($strLogin) || empty($strPwd)) {
 			$this->iErrorID = 1;
 			return false;
 		}
 		$strLoginSafe = mysql_real_escape_string($strLogin);
 		$strPwdSafe = mysql_real_escape_string($strPwd);

		
 		$result = @mysql_query("SELECT * FROM ".$this->strUserTable." WHERE ".$this->strLoginField."='$strLoginSafe' AND ".$this->strPwdField."='$strPwdSafe'");
 		if ($result) {
 			$iNumRows = @mysql_num_rows($result);
			
 			if ($iNumRows == 1) {
	 			$aUser = @mysql_fetch_assoc($result);
	 			$_SESSION[$this->strName] = $aUser;
	 			$_SESSION['login_'.$this->strName] = true;
	 			$this->iErrorID = 0;
	 			return true;
 			}
 			elseif ($iNumRows > 1) {
 				$this->iErrorID = 5;
 				return false;
 			}
 			elseif ($iNumRows == 0) {
 				$result = mysql_query("SELECT * FROM ".$this->strUserTable." WHERE ".$this->strLoginField."='$strLoginSafe'");
 				$iNumRowsCheck = mysql_num_rows($result);
 				if ($iNumRowsCheck == 1)
 					$this->iErrorID = 4;
 				elseif ($iNumRowsCheck > 1)
 					$this->iErrorID = 5;
 				elseif ($iNumRowsCheck == 0)
 					$this->iErrorID = 3;
 				return false;
 			}
 		}
 		else {
 			$this->iErrorID = 2;
 			return false;
 		}
 	}
 	
 	/**
 	 * ShowAll - affichage g�n�ral de la page de login
 	 * @return boolean
 	 */
 	public function ShowAll() {
 		if (!empty($_SESSION[$this->strName]))
 			return true;
 		if (isset($_POST["LoginForm_login"]) && isset($_POST["LoginForm_pwd"])) {
 			if ($this->LogMe($_POST["LoginForm_login"], $_POST["LoginForm_pwd"]))
 				return true;
 			else
 				$this->ShowForm();	
 		}
 		else
 			$this->ShowForm();
 		return false;
 	}
 	
 	public function Logout() {
 		session_unset(); // On efface toutes les variables de la session
		session_destroy(); // Puis on d�truit la session
 	}
 }