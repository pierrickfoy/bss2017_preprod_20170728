<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<div class="navbar bssmenu navbar-default ">
  <div class="container">
    <div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="/">Accueil</a></li>
		<li class="dropdown bssmenu-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Classe<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li> 
              
              <div class="bssmenu-content">
                <div class="row">
					<?php
					$strSql = "SELECT niveau_id, niveau_titre, niveau_landing_page FROM bor_niveau WHERE niveau_actif = 1 ORDER BY niveau_position"; 
					$aNiveaux = $oDb->queryTab($strSql);
					foreach($aNiveaux as $aNiveau){
						?>
						<div class="col-sm-4 col-md-2  clearfix">
							<p class="categorie-classe">
								<?php
									if(isset($aNiveau['niveau_landing_page']) && $aNiveau['niveau_landing_page'] != ""){
										echo "<a href='".$aNiveau['niveau_landing_page']."'>".$aNiveau['niveau_titre']."</a>";
									}else{
										echo $aNiveau['niveau_titre'];
									} 
								?>
							</p>
							
							<?php
								$strSql = "SELECT *
								FROM bor_classe_page cp
								INNER JOIN bor_classe c ON (cp.classe_id = c.classe_id)
								INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id)
								WHERE cp.classe_page_actif = 1
								AND cn.niveau_id = ".$aNiveau['niveau_id']."
								GROUP BY classe_name
								ORDER BY cn_position ";
								$aClasses = $oDb->queryTab($strSql);
							?>
							
							<ul class="liste-classe list-unstyled">
								<?php
								foreach($aClasses as $iR => $aClasse){
								?>
									<li class="item-classe-col">
										<a href="<?php echo $aClasse['classe_page_url'];?>" class="item-menu">
											<?php echo $aClasse['classe_name']; ?>
										</a>
									</li>
								<?php 
								}
								?>
							</ul>
						</div>
						<?php
					}
					?>
                </div>
                <div class="row"> </div>
              </div>
            </li>
          </ul>
        </li>
		
		<li class="dropdown bssmenu-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Matière<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li> 
              <div class="bssmenu-content">
                <div class="row">
				<?php 
					$strSql = "SELECT *
					FROM bor_matiere_page pm
					INNER JOIN bor_matiere m ON (m.matiere_id = pm.matiere_id)
					WHERE m.matiere_actif = 1
					ORDER BY m.matiere_titre ";
					$aMatieres = $oDb->queryTab($strSql);
					//var_dump($aMatieres);
					
					foreach($aMatieres as $aMatiere){
						$i =0;
						?>
						<div class="col-sm-15 clearfix">
						<ul class="liste-classe list-unstyled">
								<li class="item-matiere-1col"><a href="<?php echo $aMatiere['matiere_page_url'];?>" class="item-menu"><?php echo $aMatiere['matiere_titre'];?></a></li>
						</ul>
					  </div>
						<?php
					}
				  ?>
                </div>
              </div>
            </li>
          </ul>
        </li>

        <li> <a href="<?php echo $_CONST['URL2'] . $_CONST['URL_ACCUEIL'] . 'qui-sommes-nous.html' ?>">Qui sommes-nous ?</a> </li>
        <li> <a href="<?php echo $_CONST['URL2'] . $_CONST['URL_ACCUEIL'] . 'comment-ca-marche.html' ?>">Comment ça marche ?</a> </li>
        <li> <a href="<?php echo $_CONST['URL2'] . $_CONST['URL_ACCUEIL'] . 'comment-s-abonner.html' ?>">Comment s'abonner ?</a> </li>
		<li> <a href="<?php echo $_CONST['URL2'] . $_CONST['URL_ACCUEIL'] . 'actualite/' ?>">Actualités</a> </li>
      </ul>
    </div>
  </div>
</div>