<?php
	if(session_id() == "")
		session_start();
	define( 'ROOT', dirname( __FILE__ ).'/..');
	$_CONST['path']["server"] = ROOT;
	
	include_once(ROOT."/lib/config.inc.php");
	include_once(ROOT."/lib/database.class.php");
	include_once(ROOT."/lib/JoForm.class.php");
	include_once(ROOT."/lib/functions.php");
	include_once(ROOT."/lib/intyMailer.php");
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	$oDb->query("SET NAMES 'utf8'"); 
	LoadConfig($_CONST); //load eco_config table IN $_CONST
	
	$_SESSION["langs_id"] = 1;
	$_SESSION["langs_code"] = 'fr';
	
	$strLoginSite = "Bordas"; 
	$strPasswordSite = "getInfosMethodBD01"; 
	$strMarque = "BRD"; 
	$strCodeSite ="BRD_SOUTIEN_SCO"; 

	unset($_SESSION['user']);
	
	// $strUrlWs = $_CONST['WSDL_PER']  ; 
	$strUrlWs = "http://ws.edupole.net/accountManagement/ws_accountManagement.wsdl"  ; 
	
	$aCivilite  = array( "M." => 1 , "Mme" => 2, "Mlle" => 3); 


	if(isset($_POST["username"]) && !empty($_POST["username"]) && isset($_POST["password"]) && !empty($_POST["password"])){
		try{
			ini_set("soap.wsdl_cache_enabled",1);	
			$client = new SoapClient($strUrlWs);
			
			$client->__setCookie('session_id', session_id());
			$params = array (
				'login' => $strLoginSite ,
				'password' => $strPasswordSite, 
				'marque' => $strMarque,
                'site' => $strCodeSite,
				'userLogin' => $_POST["username"],
				'userPassword' => $_POST["password"]
			);
				
			// $retour_ws =  $client ->__getFunctions();
			// var_dump($retour_ws); 
			$retour_ws = $client -> __soapCall('authentificateAccount', array($params));
			
			if(isset($retour_ws->user->id) && !empty($retour_ws->user->id)){
			// var_dump($retour_ws->user->id);
			// exit;
							
				if($retour_ws->user->profil =="PAR"){
					$strSql = "SELECT client_id , client_actif FROM bor_client WHERE client_id_web = '".$retour_ws->user->id."' AND client_actif = 1"; 
					$aClient = $oDb->queryRow($strSql);
					
					if($oDb->rows>0){
					
							$_SESSION['user']['IDCLIENT'] =$aClient['client_id']; 
							$_SESSION['user']['IDCLIENTWEB'] =$retour_ws->user->id; 
							$_SESSION['user']['NUMCLIENT'] = $retour_ws->user->customerNumber; 
							$_SESSION['user']['CIVILITECLIENT'] = $aCivilite[$retour_ws->user->civility]; 
							$_SESSION['user']['NOMCLIENT'] = $retour_ws->user->lastname;
							$_SESSION['user']['PRENOMCLIENT'] = $retour_ws->user->firstname;
							$_SESSION['user']['LOGIN'] = $retour_ws->user->login;  
							$_SESSION['user']['MOTDEPASSE'] = $_POST["password"];
							$_SESSION['user']['EMAIL'] = $retour_ws->user->email; 
							
							$strSql = "UPDATE bor_client SET 
															client_id_web = '".mysql_real_escape_string($_SESSION['user']['IDCLIENTWEB'])."', 
															client_nom = '".mysql_real_escape_string($_SESSION['user']['NOMCLIENT'])."', 
															client_prenom = '".mysql_real_escape_string($_SESSION['user']['PRENOMCLIENT'])."', 
															client_num = '".mysql_real_escape_string($_SESSION['user']['NUMCLIENT'])."', 
															client_email = '".mysql_real_escape_string($_SESSION['user']['EMAIL'])."'
											WHERE client_id = ".mysql_real_escape_string($aClient['client_id']) ; 
							// var_dump($strSql); exit;
							$oDb->query($strSql); 
							if(isset($_POST['redirect_after']) && !empty($_POST['redirect_after']))
								$strUrl = $_POST['redirect_after'] ; 
							else{
												//$strUrl = ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ). $_CONST['URL_ACCUEIL'] . "commande/bordas/etape-2-informations-personnelles-formule-".$_SESSION['cart']['formule']['formule_classe'].".html" ; 
												$strUrl = ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ). $_CONST['URL_ACCUEIL']; 
							}
							$_SESSION["connexion"] = "ok"; 
							header('Location: ' . $strUrl);
					}else{
						//Création du compte locale
						$_SESSION['user']['NUMCLIENT'] = $retour_ws->user->customerNumber;
						$_SESSION['user']['IDCLIENTWEB'] =$retour_ws->user->id; 						
						$_SESSION['user']['CIVILITECLIENT'] = $aCivilite[$retour_ws->user->civility]; 
						$_SESSION['user']['NOMCLIENT'] = $retour_ws->user->lastname;
						$_SESSION['user']['PRENOMCLIENT'] = $retour_ws->user->firstname;
						$_SESSION['user']['LOGIN'] = $retour_ws->user->login;  
						$_SESSION['user']['MOTDEPASSE'] = $_POST["password"];
						$_SESSION['user']['EMAIL'] = $retour_ws->user->email; 
						$strSql = "INSERT INTO bor_client (client_id_web,client_num, client_nom, client_prenom,  client_email, client_date_add,  acces_zoe, client_actif)
											VALUES 	(
														'".$_SESSION['user']['IDCLIENTWEB']."', 
														'".$_SESSION['user']['NUMCLIENT']."', 
														'".$_SESSION['user']['NOMCLIENT']."', 
														'".$_SESSION['user']['PRENOMCLIENT']."', 
														'".$_SESSION['user']['EMAIL']."', 
														NOW(), 
														0, 
														1
													)";
						// var_dump($strSql); 
						$oDb->query($strSql); 
						// header('Location: ' . $_CONST['URL'].$strUrl);
						$_SESSION['user']['IDCLIENT'] = $oDb->last_insert_id; 
						if(isset($_POST['redirect_after']) && !empty($_POST['redirect_after']))
							$strUrl = $_POST['redirect_after'] ; 
						else{
										//$strUrl = ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ). $_CONST['URL_ACCUEIL'] . "commande/bordas/etape-2-informations-personnelles-formule-".$_SESSION['cart']['formule']['formule_classe'].".html" ;						// $strUrl = "/soutien-scolaire-en-ligne/abonnement/formule-".$_SESSION['cart']['formule']['formule_classe'].".html" ; 
										$strUrl = ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ). $_CONST['URL_ACCUEIL']; 
						}
						$_SESSION["connexion"] = "ok"; 
						header('Location: ' . $strUrl);
					}
				}else{
					if(isset($_POST['redirect_after']) && !empty($_POST['redirect_after']))
						$strUrl = $_POST['redirect_after'] ; 
					else{
									//$strUrl = ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ). $_CONST['URL_ACCUEIL'] . "commande/bordas/etape-2-informations-personnelles-formule-".$_SESSION['cart']['formule']['formule_classe'].".html" ;					
									$strUrl = ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ). $_CONST['URL_ACCUEIL']; 
					}
											if(isset($_POST['page']) && $_POST['page'] == 'identification')
						$_SESSION["connexion_identification"] = "erreur_profil"; 
					else
						$_SESSION["connexion"] = "erreur_profil"; 
					header('Location: ' . $strUrl);
				}
			}else{
				if(isset($_POST['redirect_after']) && !empty($_POST['redirect_after']))
					$strUrl = $_POST['redirect_after'] ; 
				else{
								//$strUrl = ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ). $_CONST['URL_ACCUEIL'] . "commande/bordas/etape-2-informations-personnelles-formule-".$_SESSION['cart']['formule']['formule_classe'].".html" ;				// $_SESSION["connexion"] = "erreur_connexion"; 
								$strUrl = ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ). $_CONST['URL_ACCUEIL']; 
				}
									if(isset($_POST['page']) && $_POST['page'] == 'identification')
					$_SESSION["connexion_identification"] = "erreur_connexion"; 
				else
					$_SESSION["connexion"] = "erreur_connexion"; 
				header('Location: ' . $strUrl);
			}
		}catch (Exception $e){
			if(isset($_POST['redirect_after']) && !empty($_POST['redirect_after']))
				$strUrl = $_POST['redirect_after'] ; 
			else{
							//$strUrl = ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ). $_CONST['URL_ACCUEIL'] . "commande/bordas/etape-2-informations-personnelles-formule-".$_SESSION['cart']['formule']['formule_classe'].".html" ;			// $_SESSION["connexion"] = "erreur_connexion"; 
							$strUrl = ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ). $_CONST['URL_ACCUEIL']; 
			}
							if(isset($_POST['page']) && $_POST['page'] == 'identification')
				$_SESSION["connexion_identification"] = "erreur_connexion"; 
			else
				$_SESSION["connexion"] = "erreur_connexion";
			header('Location: ' . $strUrl);
		}
	}else{
		if(isset($_POST['redirect_after']) && !empty($_POST['redirect_after']))
			$strUrl = $_POST['redirect_after'] ; 
		else{
						//$strUrl = ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ). $_CONST['URL_ACCUEIL'] . "commande/bordas/etape-2-informations-personnelles-formule-".$_SESSION['cart']['formule']['formule_classe'].".html" ;		// $_SESSION["connexion"] = "erreur_champ"; 
						$strUrl = ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ). $_CONST['URL_ACCUEIL']; 
		}
					if(isset($_POST['page']) && $_POST['page'] == 'identification')
				$_SESSION["connexion_identification"] = "erreur_champ"; 
			else
				$_SESSION["connexion"] = "erreur_champ";
		header('Location: ' .$strUrl);
	}