<script type="text/javascript">
		jQuery.noConflict();	
</script>	
<?php	
	if(session_id() == "")
		session_start();
	define( 'ROOTWS', dirname( __FILE__ )."/..");
	$_CONST['path']["server"] = ROOTWS;

	include_once(ROOTWS."/lib/config.inc.php");
	include_once(ROOTWS."/lib/database.class.php");
	include_once(ROOTWS."/lib/JoForm.class.php");
	include_once(ROOTWS."/lib/functions.php");
	include_once(ROOTWS."/lib/intyMailer.php");
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	$oDb->query("SET NAMES 'utf8'"); 
	LoadConfig($_CONST); //load eco_config table IN $_CONST
		
	
	$_SESSION["langs_id"] = 1;
	$_SESSION["langs_code"] = 'fr';
	
	/* Paramètres d'appel au web service */
	$strPasswordSite = "createAccBD01"; 
	$strLoginSite = "Bordas"; 
	$strMarque = "BRD"; 
	$strCodeSite ="BRD_SOUTIEN_SCO"; 
	$strProfil ="PAR";  
	
	// LOT 3 FINAL
	$strOrigine = "";
	if(isset($_SESSION['partenaire']['per_origine_commande']) && !empty($_SESSION['partenaire']['per_origine_commande']))
		$strOrigine = $_SESSION['partenaire']['per_origine_commande'];
	
	//LOT6
	if(isset($_SESSION["products"]["enfants"][0]["CODEPROMO"]) && !empty($_SESSION["products"]["enfants"][0]["CODEPROMO"]))
		$strOrigine = "BSS-CODE-PROMO_".$_SESSION["products"]["enfants"][0]["CODEPROMO"];
	
	
	/* Modif Pf Ecomiz*/
	$strProvenance = ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ). $_CONST['URL_ACCUEIL'] ."commande/bordas/etape-2-informations-personnelles-formule-".$_SESSION['cart']['formule']['formule_classe'].".html" ;
	$strUrlProxy = ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] )."/proxy/proxy_createAccount_abo.php"; 
	$strUrlWs = "http://ws.edupole.net/accountManagement/ws_accountManagement.wsdl" ; 
	$aCivilite  = array( "M." => 1 , "Mme" => 2, "Mlle" => 3); 
	
	try
	{
		ini_set("soap.wsdl_cache_enabled", 0);
		$client = new SoapClient($strUrlWs);
		$client->__setCookie('session_id', session_id()); 
		
		$params = array (
			'post' => (isset($_POST) && !empty($_POST) ? $_POST : ""),
			'login' => $strLoginSite,
			'password' 	=> $strPasswordSite,
			'marque'	=> $strMarque,
			'site' => $strCodeSite,
			'profil'	=> $strProfil,
			'provenance' => $strProvenance,
			'origine' => $strOrigine,
			'origine' => 'bordas',
			'urlProxy' => $strUrlProxy
		);
		$retour_ws =  $client -> __soapCall('createAccount', array($params));
		if(isset($retour_ws->idUser) && $retour_ws->idUser > 0 ){
			//Après avoir créer l'utilisateur, il faut appeler la fonction getInfosAccountById pour récupérer le num client
			ini_set("soap.wsdl_cache_enabled",1);	
			$client2 = new SoapClient($strUrlWs);					
			$client2->__setCookie('session_id', session_id());
			$params = array (
				'login' => $strLoginSite ,
				'password' => 'getInfosMethodBD01', 
				'marque' => $strMarque, 
				'idUser' => $retour_ws->idUser
			);
			try{
				$retour2_ws = $client2-> __soapCall('getInfosAccountById', array($params));
			}
			catch(SoapFault $e){
				echo $e;
			}
			if(true === isset($retour2_ws)){
			$_SESSION['user']['NUMCLIENT'] = $retour2_ws->user->customerNumber; 
			//Création réuussie
			$_SESSION['user']['IDCLIENTWEB'] =$retour_ws->idUser; 
			$_SESSION['user']['CIVILITECLIENT'] = $aCivilite[$_POST['civility']]; 
			$_SESSION['user']['NOMCLIENT'] = $_POST['lastname'];
			$_SESSION['user']['PRENOMCLIENT'] = $_POST['firstname'];
			$_SESSION['user']['LOGIN'] = $_POST['login'];  
			$_SESSION['user']['MOTDEPASSE'] = $_POST['password'];
			$_SESSION['user']['EMAIL'] = $_POST['email']; 
			$_SESSION['user']['creation'] = true ; 
			//Création du compte locale 
			$strSql = "INSERT INTO bor_client (client_id_web,client_num, client_nom, client_prenom,  client_email, client_date_add,  acces_zoe, client_actif)
						VALUES 	(
								'".mysql_real_escape_string($_SESSION['user']['IDCLIENTWEB'])."', 
								'".mysql_real_escape_string($_SESSION['user']['NUMCLIENT'])."', 
								'".mysql_real_escape_string($_SESSION['user']['NOMCLIENT'])."', 
								'".mysql_real_escape_string($_SESSION['user']['PRENOMCLIENT'])."', 
								'".mysql_real_escape_string($_SESSION['user']['EMAIL'])."', 
								NOW(), 
								0, 
								1
								)";
			$oDb->query($strSql);
			$_SESSION['user']['IDCLIENT'] = $oDb->last_insert_id;
			//retour du ws
			//echo (($retour_ws->display));
				
			echo '<meta http-equiv="refresh" content="0; url='.$strProvenance.'" >';
			echo '<p>Votre inscription est prise en compte.</p>';
			echo '<a href="'.$strProvenance.'" target="_top" class="btn btn-fw  btn-primary"> Continuer <i class="icon-angle-circled-right"></i></a>';
			}else{
				echo 'erreur';
				echo (($retour_ws->display));
			}
		}else{
			echo (($retour_ws->display));
		}
	}
	catch (SoapFault $fault) {
		header('Location: ' . $strProvenance);
	}
	
		
	?>