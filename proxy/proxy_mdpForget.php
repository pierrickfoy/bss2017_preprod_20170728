<?php

	// error_reporting(E_ALL);
	// ini_set('error_reporting', E_ALL);
	// ini_set('display_errors', '1');
	if(session_id() == "")
		session_start();
	define( 'ROOT', dirname( __FILE__ ).'/..');
	$_CONST['path']["server"] = ROOT;
	
	include_once(ROOT."/lib/config.inc.php");
	include_once(ROOT."/lib/database.class.php");
	include_once(ROOT."/lib/JoForm.class.php");
	include_once(ROOT."/lib/functions.php");
	include_once(ROOT."/lib/intyMailer.php");
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	$oDb->query("SET NAMES 'utf8'"); 
	LoadConfig($_CONST); //load eco_config table IN $_CONST
	
	$_SESSION["langs_id"] = 1;
	$_SESSION["langs_code"] = 'fr';
	
	//Vérification si champ vide ou incorrecte
	if(isset($_POST['redirect_after']) && !empty($_POST['redirect_after']))
		$strUrl = $_POST['redirect_after'];
	else
		$strUrl = $_CONST['URL_ACCUEIL'];
	/*if(!isset($_POST["email_forgot"]) || empty($_POST["email_forgot"])){	
		$_SESSION["mdp_forget"] = "erreur_champ"; 
		header('Location: ' .$strUrl);
		exit; 
	}
	if( !is_valid_email($_POST["email_forgot"])){
		$_SESSION["mdp_forget"] = "champ_incorrect"; 
		header('Location: ' .$strUrl);
		exit; 
	}*/
					
	
	$strLoginSite = "Bordas"; 
	
	$strMarque = "BRD"; 
	$strCodeSite ="BRD_SOUTIEN_SCO"; 
	
	unset($_SESSION['user']);

	$strUrlWs = "http://ws.edupole.net/accountManagement/ws_accountManagement.wsdl"; 
	$aCivilite  = array( "M." => 1 , "Mme" => 2, "Mlle" => 3); 
	
	if(isset($_POST["email_forgot"]) && !empty($_POST["email_forgot"])){
		try{
			ini_set("soap.wsdl_cache_enabled",1);	
			$client = new SoapClient($strUrlWs);
			$strPasswordSite = "changePasswordMethodBD01"; 
			$client->__setCookie('session_id', session_id());
			$params = array (
				'login' => $strLoginSite ,
				'password' => $strPasswordSite, 
				'marque' => $strMarque, 
				'site' => $strCodeSite,
				'email' => $_POST["email_forgot"]
			);
			
			// $retour_ws =  $client ->__getFunctions();
			// var_dump($retour_ws); 
			$retour_ws = $client -> __soapCall('prepareChangePassword', array($params));
			
			if(isset($retour_ws->listAccount) && count($retour_ws->listAccount)>0){
				//Création du mail 
				$strMessage = "Bonjour,<br /><br />"; 
                                $a = $retour_ws->listAccount ; 
				
                                if(isset($a->groupLoginTicket->login) && !empty($a->groupLoginTicket->login)){
                                    $strMessage .= "Un compte est rattaché à cette adresse e-mail :<br /><br />";
                                    $strMessage .= "Pour redéfinir le mot de passe associé au compte ".$a->groupLoginTicket->login.", je clique <a href='".$_CONST['URL2'].$_CONST['URL_ACCUEIL'].'mot-de-passe-oublie-'.strToUrl($a->groupLoginTicket->ticket).".html'>ici</a><br/><br/>";
                                }else{
                                    $strMessage .= "Plusieurs comptes sont rattachés à cette adresse e-mail :<br /><br />";
                                    foreach($a->groupLoginTicket as $aAccount){
					$strMessage .= "Pour redéfinir le mot de passe associé au compte ".$aAccount->login.", je clique <a href='".$_CONST['URL2'].$_CONST['URL_ACCUEIL'].'mot-de-passe-oublie-'.strToUrl($aAccount->ticket).".html'>ici</a><br/><br/>";
                                    }
                                }
					
				
                               
				
				$strMessage .= "ATTENTION : les liens sont à usage unique et leur durée de validité est de 24 heures. <br /><br />";
				$strMessage .= "Cordialement,<br /><br />";
				$strMessage .= "Bordas Soutien scolaire";
                               
				// $_POST["email_forgot"] = "hz.hedizouari@gmail.com"; 
				$mail = new intyMailer(); 
				// OPTIONAL 
				$mail->set(array('charset'  , 'utf-8')); 
				$mail->set(array('priority' , 3)); 
				$mail->set(array('replyTo'  , "noreply@bordas.tm.fr")); 
				$mail->set(array('alert '    ,  true)); 
				$mail->set(array('html'     ,  true)); 
				// OPTIONAL 
				$mail->set(array('subject' , "Rappel de votre accès sur le site Bordas Soutien Scolaire")); 
				$mail->set(array('message' , $strMessage)); 
				$mail->set(array('sending' , 'FROM' , "Bordas Soutien Scolaire" ,"noreply@bordas.tm.fr")); 
				$mail->set(array('sending' , 'TO'   , "" ,$_POST["email_forgot"])); 
				$mail->send();
			
			
				
				if(isset($_POST['redirect_after']) && !empty($_POST['redirect_after']))
					$strUrl = $_POST['redirect_after'];
				else
					$strUrl = $_CONST['URL_ACCUEIL'];
				// $_SESSION["mdp_forget"] = "ok"; 
				if(isset($_POST['page']) && $_POST['page'] == 'motdepasse')
					$_SESSION["mdp_forget_motdepasse"] = "ok"; 
				else
					$_SESSION["mdp_forget"] = "ok"; 
				
				$_SESSION['goto'] = "step_3_1";
				
				header('Location: ' .$strUrl);
			}else{
				if(isset($_POST['redirect_after']) && !empty($_POST['redirect_after']))
					$strUrl = $_POST['redirect_after'];
				else
					$strUrl = $_CONST['URL_ACCUEIL'];
				// $_SESSION["mdp_forget"] = "erreur_update"; 
				if(isset($_POST['page']) && $_POST['page'] == 'motdepasse')
					$_SESSION["mdp_forget_motdepasse"] = "erreur_update"; 
				else
					$_SESSION["mdp_forget"] = "erreur_update"; 
				header('Location: ' .$strUrl);
			}			
		}catch (Exception $e){
			if(isset($_POST['redirect_after']) && !empty($_POST['redirect_after']))
				$strUrl = $_POST['redirect_after'];
			else
				$strUrl = $_CONST['URL_ACCUEIL'];
			// $_SESSION["mdp_forget"] = "erreur_technique"; 
			if(isset($_POST['page']) && $_POST['page'] == 'motdepasse')
				$_SESSION["mdp_forget_motdepasse"] = "erreur_technique"; 
			else
				$_SESSION["mdp_forget"] = "erreur_technique"; 
			header('Location: ' .$strUrl);
		}
	}else if(isset($_POST["action"]) && $_POST["action"] = "generate_password"){
		$strTicket = $_POST['ticket']; 
		if(!isset($_POST["login"]) || empty($_POST["login"]) || !isset($_POST["password"]) || empty($_POST["password"]) || !isset($_POST["confirm_pasword"]) || empty($_POST["confirm_pasword"])){
			$strUrl = $_CONST['URL_ACCUEIL'].'mot-de-passe-oublie-'.strToUrl($strTicket).".html";
			$_SESSION["confirmation"] = "erreur_champ"; 
			header('Location: ' .$strUrl);			
		}else{
			if($_POST["password"] == $_POST["confirm_pasword"]){
				try{
					ini_set("soap.wsdl_cache_enabled",1);	
					$client = new SoapClient($strUrlWs);
					$strPasswordSite = "changePasswordMethodBD01"; 
					$client->__setCookie('session_id', session_id());
					$params = array (
						'login' => $strLoginSite ,
						'password' => $strPasswordSite, 
						'marque' => $strMarque, 
						'site' => "PORTAIL",
						'ticket' => $_POST["ticket"],
						'userLogin' => $_POST["login"],
						'userPassword' => $_POST["password"]
					);
					$retour_ws = $client -> __soapCall('changePassword', array($params));
					if(isset($retour_ws->isUpdated)){
						if( $retour_ws->isUpdated )
							$_SESSION["confirmation"] = "ok";
						else
							$_SESSION["confirmation"] = "erreur_update";
							
							// var_dump('test');
						$strUrl = $_CONST['URL_ACCUEIL'].'mot-de-passe-oublie-'.strToUrl($strTicket).".html";
						header('Location: ' .$strUrl);
					}else{
						$strUrl = $_CONST['URL_ACCUEIL'].'mot-de-passe-oublie-'.strToUrl($strTicket).".html";
						$_SESSION["confirmation"] = "erreur_technique"; 
						header('Location: ' .$strUrl);
					}
				}catch (Exception $e){
					$strUrl = $_CONST['URL_ACCUEIL'].'mot-de-passe-oublie-'.strToUrl($strTicket).".html";
					$_SESSION["confirmation"] = "erreur_technique"; 
					header('Location: ' .$strUrl);
				}
			}else{
				$strUrl = $_CONST['URL_ACCUEIL'].'mot-de-passe-oublie-'.strToUrl($strTicket).".html";
				$_SESSION["confirmation"] = "erreur_password"; 
				header('Location: ' .$strUrl);
			}
		}
	}
	else{
		$strUrl = "/soutien-scolaire-en-ligne/" ;
		header('Location: ' .$strUrl);
	}
	