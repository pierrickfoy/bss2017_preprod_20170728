<?php	
	if(session_id() == "")
		session_start();
	// ini_set('error_reporting', E_ALL);
	// ini_set('display_errors', '1');
	define( 'ROOTWS', dirname( __FILE__ ).'/..');
	$_CONST['path']["server"] = ROOTWS;
		
	include_once(ROOTWS."/lib/config.inc.php");
	include_once(ROOTWS."/lib/database.class.php");
	include_once(ROOTWS."/lib/JoForm.class.php");
	include_once(ROOTWS."/lib/functions.php");
	include_once(ROOTWS."/lib/intyMailer.php");
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	$oDb->query("SET NAMES 'utf8'"); 
	LoadConfig($_CONST); //load eco_config table IN $_CONST
	
	$_SESSION["langs_id"] = 1;
	$_SESSION["langs_code"] = 'fr';
		
	$id_uti = "";
	$strPasswordSite = "editAccBD01"; 


	/* Paramètres d'appel au web service */
	$strLoginSite = "Bordas"; 
	$strMarque = "BRD"; 
	$strCodeSite ="BRD_SOUTIEN_SCO"; 
	$strProfil ="PAR";  
	
	$strLien = $oDb->queryItem("SELECT templates_name FROM  eco_templates WHERE templates_id = 30");
	if($_CONST['TYPE_ENVIRONNEMENT'] !='dev' && $_CONST['TYPE_ENVIRONNEMENT'] !='preprod'){
		$strProvenance = $_CONST['HTTPS'].$_CONST['URL_ACCUEIL'].strToUrl($strLien).'.html' ;
		$strUrlProxy = $_CONST['HTTPS']."/proxy/proxy_updateAccount_bordas.php";
	}else{
		$strProvenance = $_CONST['URL2'].$_CONST['URL_ACCUEIL'].strToUrl($strLien).'.html' ;
		$strUrlProxy = $_CONST['URL2']."/proxy/proxy_updateAccount_bordas.php";
	}
	$strUrlWs = "http://ws.edupole.net/accountManagement/ws_accountManagement.wsdl" ; 
	
	$aCivilite  = array( "M." => 1 , "Mme" => 2, "Mlle" => 3); 
	
 

	try
	{
		ini_set("soap.wsdl_cache_enabled", 0);
		$client = new SoapClient($strUrlWs, array('trace'=>0));
		$client->__setCookie('session_id', session_id()); 
		
		$params = array (
			'post' => (isset($_POST) && !empty($_POST) ? $_POST : ""),
			'login' => $strLoginSite,
			'password' 	=> $strPasswordSite,
			'marque'	=> $strMarque,
			'site' => $strCodeSite,
			'profil'	=> $strProfil,
			'provenance' => $strProvenance,
			'idUser' => $_SESSION['user']['IDCLIENTWEB'],
			'urlProxy' => $strUrlProxy
			
		);

		$retour_ws = $client -> __soapCall('editAccount', array($params));
		
		if(isset($retour_ws->idUser) && $retour_ws->idUser > 0 ){
			$_SESSION['user']['NUMCLIENT'] =$retour_ws->idUser; 
			$_SESSION['user']['CIVILITECLIENT'] = $aCivilite[$_POST['civility']]; 
			$_SESSION['user']['NOMCLIENT'] = $_POST['lastname'];
			$_SESSION['user']['PRENOMCLIENT'] = $_POST['firstname'];
			$_SESSION['user']['LOGIN'] = $_POST['login'];  
			$_SESSION['user']['MOTDEPASSE'] = $_POST['password'];
			$_SESSION['user']['EMAIL'] = $_POST['email']; 
			//Création du compte locale 
			$strSql = "UPDATE bor_client SET
									client_num = '".mysql_real_escape_string($_SESSION['user']['NUMCLIENT'])."',
									client_nom = '".mysql_real_escape_string($_SESSION['user']['NOMCLIENT'])."',
									client_prenom = '".mysql_real_escape_string($_SESSION['user']['PRENOMCLIENT'])."',
									client_email = '".mysql_real_escape_string($_SESSION['user']['EMAIL'])."'
							WHERE 
									client_id = '".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."'";
			$oDb->query($strSql);
			$_SESSION['user']['IDCLIENT'] = $oDb->last_insert_id; 
		}else{
			 echo (($retour_ws->display));  	  
		}
	}
	catch (SoapFault $fault) {
		header('Location: ' .$strProvenance);
	}
	?>