<?php
 define( 'ROOT', dirname( __FILE__ ));
	$_CONST['path']["server"] = ROOT;
	
	include_once(ROOT."/lib/config.inc.php");
	include_once(ROOT."/lib/database.class.php");
	include_once(ROOT."/lib/JoForm.class.php");
	include_once(ROOT."/lib/functions.php");
	include_once(ROOT."/lib/intyMailer.php");
	include_once(ROOT."/lib/abo.class.php");
	include_once(ROOT."/lib/cb.class.php");
	$isMobile=(bool)preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	
	$oDb->query("SET NAMES utf8"); 
	LoadConfig($_CONST); //load eco_config table IN $_CONST
	
	$_SESSION["langs_id"] = 1 ;
	/* ************************** */
	global $aLang, $_CONST ; 
 
 
$strSitemap = 'sitemap.xml';

if (date("Ymd",filemtime($strSitemap)) == date("Ymd")) {
	return;
}

/* Mail Log to EcomiZ support */
$strHeader = "MIME-Version: 1.0\r\n";
$strHeader .= "Content-type: text/html; charset=iso-8859-1\r\n";
$strHeader .= "To: ECOMIZ SUPPORT <pierrick.foy@ecomiz.com>\r\n";
$strHeader .= "From: ECOMIZ SERVER <pierrick.foy@ecomiz.com\r\n";


@chmod($strSitemap, 0777);
if (!($fp=@fopen($strSitemap,"w"))) {
	mail($_CONST["mail"]["support"],  "[BSS][SITEMAP GEN]", Date("D-m-Y H:i:s")."<br>ERROR oppening $strSitemap", $strHeader);
	return;
}
$aNoReference = array (	); 

$strSitemap="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
$strSitemap.="<urlset xmlns=\"http://www.google.com/schemas/sitemap/0.84\">\n";

					$strSql = "SELECT niveau_id, niveau_titre FROM bor_niveau WHERE niveau_actif = 1 ORDER BY niveau_position"; 
					$aNiveaux = $oDb->queryTab($strSql);
					foreach($aNiveaux as $aNiveau){

								$strSql = "SELECT *, cp.classe_page_id as ccc
								FROM bor_classe_page cp
								INNER JOIN bor_classe c ON (cp.classe_id = c.classe_id)
								INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id)
								WHERE cp.classe_page_actif = 1
								AND cn.niveau_id = ".$aNiveau['niveau_id']."
								GROUP BY classe_name
								ORDER BY cn_position ";
								$aClasses = $oDb->queryTab($strSql);

								foreach($aClasses as $iR => $aClasse){

										$strUrl = $_CONST['URL2'].$aClasse['classe_page_url'];
										$strSitemap.="\t<url>\n";
										$strSitemap.="\t\t<loc>$strUrl</loc>\n";
										$strSitemap.="\t\t<priority>1</priority>\n";
										$strSitemap.="\t\t<changefreq>monthly</changefreq>\n";
										$strSitemap.="\t</url>\n";

										//var_dump($aClasse['ccc']);
										$aMatiere = $oDb->queryTab("SELECT *
										FROM bor_matiere_page cmp, bor_matiere_classe_edito mc, bor_matiere m
										WHERE mc.matiere_id = cmp.matiere_id
										AND m.matiere_id = cmp.matiere_id
										AND mc.classe_id = ".$aClasse['ccc']."
										AND cmp.matiere_page_actif = 1");
										foreach($aMatiere as $matiere){
											$lien_classe_matiere = $aClasse['classe_page_url'].strToUrl($matiere['matiere_titre'])."/";

														$strUrl = $_CONST['URL2'].$lien_classe_matiere;
														$strSitemap.="\t<url>\n";
														$strSitemap.="\t\t<loc>$strUrl</loc>\n";
														$strSitemap.="\t\t<priority>1</priority>\n";
														$strSitemap.="\t\t<changefreq>daily</changefreq>\n";
														$strSitemap.="\t</url>\n";

														$strSql = "SELECT *
														FROM bor_chapitres c
														WHERE c.chapitre_classe_id = ".$aClasse['classe_id']."
														AND c.chapitre_matiere_id = ".$matiere['matiere_id']."
														AND c.chapitre_status = 1"; 
														$aChapitre = $oDb->queryTab($strSql);
														
														foreach($aChapitre as $chapitre){

																$strUrl = $_CONST['URL2'].strtolower($chapitre['chapitre_alias']);
																$strSitemap.="\t<url>\n";
																$strSitemap.="\t\t<loc>$strUrl</loc>\n";
																$strSitemap.="\t\t<priority>1</priority>\n";
																$strSitemap.="\t\t<changefreq>daily</changefreq>\n";
																$strSitemap.="\t</url>\n";
																
																$strSql = "SELECT *
																FROM bor_notions n
																WHERE n.chapitre_id = ".$chapitre['chapitre_id']."
																AND notions_status = 1"
																;
																$aNotion = $oDb->queryTab($strSql);
																
																foreach($aNotion as $notion){
																	
																	$strUrl = $_CONST['URL2'].$notion['notions_url_seo'];
																	$strSitemap.="\t<url>\n";
																	$strSitemap.="\t\t<loc>$strUrl</loc>\n";
																	$strSitemap.="\t\t<priority>1</priority>\n";
																	$strSitemap.="\t\t<changefreq>daily</changefreq>\n";
																	$strSitemap.="\t</url>\n";

																}
	
														}
													
											}
										
								}
								
					}
				
				
					$strSql = "SELECT *
					FROM bor_matiere_page cp
					INNER JOIN bor_matiere c ON (cp.matiere_id = c.matiere_id)
					WHERE matiere_page_actif = 1
					"; 
					$listeMatiere = $oDb->queryTab($strSql);
					foreach($listeMatiere as $mat)
					{

							$strUrl = $_CONST['URL2'].$mat['matiere_page_url'];
							$strSitemap.="\t<url>\n";
							$strSitemap.="\t\t<loc>$strUrl</loc>\n";
							$strSitemap.="\t\t<priority>1</priority>\n";
							$strSitemap.="\t\t<changefreq>monthly</changefreq>\n";
							$strSitemap.="\t</url>\n";

					}
					
					$strSql = "SELECT *
					FROM bor_article cp
					INNER JOIN bor_rubrique c ON (cp.rubrique_id = c.rubrique_id)
					WHERE article_actif = 1
					"; 
					$listeactu = $oDb->queryTab($strSql);
					
					foreach($listeactu as $actu)
					{
						if(isset($actu["article_url"]) && $actu["article_url"] != "")
						{
							$strUrl = $_CONST['URL2'].'/actualite/'.strToUrl($actu['rubrique_titre']).'/'.$actu['article_id'].'-'.$actu['article_url'];
							$strSitemap.="\t<url>\n";
							$strSitemap.="\t\t<loc>$strUrl</loc>\n";
							$strSitemap.="\t\t<priority>0.5</priority>\n";
							$strSitemap.="\t\t<changefreq>monthly</changefreq>\n";
							$strSitemap.="\t</url>\n";
						}
							
					}
					
					
					// pages statiques 
					$aPagesStatiques = array("/qui-sommes-nous.html",
					"/comment-ca-marche.html",
					"/comment-s-abonner.html",
					"/actualite/",
					"/produit/abonnement-formule-ciblee.html",
					"/produit/abonnement-formule-reussite.html",
					"/produit/abonnement-formule-tribu.html",
					"/faq.html",
					"/mentions-legales.html",
					"/conditions-generales-d-utilisation.html",
					"/conditions-generales-de-vente.html",
					"/plan-du-site.html",
					);
					foreach($aPagesStatiques as $statique){
						$strUrl = $_CONST['URL2'].$statique;
						$strSitemap.="\t<url>\n";
						$strSitemap.="\t\t<loc>$strUrl</loc>\n";
						$strSitemap.="\t\t<priority>1</priority>\n";
						$strSitemap.="\t\t<changefreq>monthly</changefreq>\n";
						$strSitemap.="\t</url>\n";
					}
					
					
					

$strSitemap.="</urlset>\n";
fwrite($fp,$strSitemap);
fclose($fp);
?>