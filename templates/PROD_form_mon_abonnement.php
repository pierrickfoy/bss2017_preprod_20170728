<?php
$_SESSION['strUrlToRedirect'] = (isset($_SERVER['HTTPS'])) ? 'https://' . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" : 'http://' . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
// redirection si internaute non identifié
if(!isset($_SESSION['user']['IDCLIENT']) || empty($_SESSION['user']['IDCLIENT'])){
	$strLien = $oDb->queryItem("SELECT templates_name FROM eco_templates WHERE templates_id=34"); 
	$strProvenance = $_CONST['URL2'].$_CONST['URL_ACCUEIL'].strToUrl($strLien).'.html' ;
	echo "<script>document.location.href='".$strProvenance."';</script>";
	exit;
} else {
$_SESSION['strUrlToRedirect'] = "";
	
/************************************************************************************************/
/*							RECUPERATION DES ABONNEMENTS DE L'UTILISATEUR						*/
/************************************************************************************************/

$bAbonnement = false; 
$login =$_SESSION['user']['LOGIN']; 
$idWeb = $_SESSION['user']['IDCLIENTWEB']; 
$created = gmdate('Y-m-d\TH:i:s\Z');
$CleSecret = "ry&uoNE8F6KUgnT"  ;
$strUrl = "https://www.e-interforum.com/intra/wsse/get_abonnement_client.php?cm=ZOE";

//Création de l'entete de securite pour appeler le WS 
$password =md5($idWeb.$CleSecret); 
$nonce = base64_encode(pack('H*', sha1(md5(microtime() . mt_rand() . uniqid(mt_rand(), true))))); 
$digest = base64_encode(sha1(base64_decode($nonce) . $created . $password, true)); 
$wsse_header = sprintf( 'X-WSSE: UsernameToken Username="%s", PasswordDigest="%s", Nonce="%s", Created="%s"',  $login, $digest, $nonce, $created );

//Appel du WS via curl pour initialiser l'entete
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $strUrl);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_COOKIESESSION, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false );
curl_setopt($curl, CURLOPT_HTTPGET, true);
curl_setopt($curl, CURLOPT_HTTPHEADER, array($wsse_header));

if (($return = curl_exec($curl)) !== FALSE) {	
	$oReponses = new SimpleXMLElement($return);
	/*echo '<pre>';
	var_dump($oReponses);//exit;
	echo '</pre>';*/
	if(isset($oReponses->user->children)){
		$oChildrens = $oReponses->user->children ;
		foreach($oChildrens->child as $oChildren){
			$oRessources = $oChildren->resources ;
			foreach($oRessources->{resource} as $oRessource){
				$aListeAboEnfant[utf8_decode((string)$oChildren->firstname)][(string)$oRessource->ean13][] = array( "titre"=> (string)$oRessource->title, "ean"=> (string)$oRessource->ean13, "date" =>(string)$oRessource->{'expiration-date'}, "classe" => (string)$oChildren->grade, "action-code" => $oRessource->{'action-code'}) ;
			}
		}
	}else{
		//Erreur flux 
		$bAbonnement = true;		
	}
}

$aCbType = array( 1 => "CB", 2 => "EU", 3 => "CB"); 
$aTypeCb = array( "CB" => 1, "EU" => 2 ); 
$bValidate = false; 
$strCommandeBloc = '' ;

/************************************************************************************************/
/*							TRAITEMENT DE L'ACTION DU POST										*/
/************************************************************************************************/

if(isset($_POST) && count($_POST)){
	
	//Récupération de l'action pour savoir dans quelle situation on se trouve 
	if(isset($_POST['action']) && !empty($_POST['action']) && isset($_POST['idcommande']) && !empty($_POST['idcommande'])){
		$strAction = $_POST['action'] ; // 3 type d'action : updatecb , deletecb, deleteabo
		$iCommandeId = $_POST['idcommande'] ;
		//Vérification que la commande est bien lié à l'utilisateur en ligne 
		$strSql = "SELECT * FROM bor_commande c WHERE c.commande_id  = '".mysql_real_escape_string($iCommandeId)."' AND c.client_id = '".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."' AND commande_statut = 1"; 
		$aCommande = $oDb->queryRow($strSql); 
		if($oDb->rows > 0 ){	
			$oCb = new cb();
			
// ****** UPDATE CB ******
			
			if($strAction == "updatecb"){
				//Modification de la carte, on doit vérifié les infos
				$aVerifCarte = $oCb->verifInterneCb($_POST["cb_name_$iCommandeId"],$aCbType[$_POST["cp_type_$iCommandeId"]],$_POST["cb_numcarte_$iCommandeId"],$_POST["cb_annee_$iCommandeId"],$_POST["cb_mois_$iCommandeId"],$_POST["cb_controle_$iCommandeId"]);
				if($aVerifCarte["bVerif"]){
					$aReponse = $oCb->updateCb($_SESSION['user']['IDCLIENTWEB'], $aCommande['commande_cb_ref'],$aCbType[$_POST["cp_type_$iCommandeId"]], $_POST["cb_numcarte_$iCommandeId"], $_POST["cb_name_$iCommandeId"],$_POST["cb_mois_$iCommandeId"].$_POST["cb_annee_$iCommandeId"]);
					if($aReponse['erreur']){
						//Erreur 
						$strErreurCarte = $aVerifCarte["message"] ; 
						$strCommandeBloc = $iCommandeId ; 
						$bErreur = true; 
					}else{
						$bErreur = false; 
						$bValidate = true; 
						$strCommandeBloc = $iCommandeId ; 
						$strMessageValidate = "Votre CB a bien été mise à jour.";
					}
				}else{
					//La carte est invalide
					$strErreurCarte = $aVerifCarte["strMessage"] ; 
					$strCommandeBloc = $iCommandeId ; 
					$bErreur = true; 
				}
				
// ****** DELETE CB ******
				
			}else if($strAction == "deletecb"){
				//On va comparer que les infos de la cb saisi corresponde au info de la cb enregistré 
				$aVerifCarte = $oCb->verifInterneCb($_POST["cb_name_$iCommandeId"],$aCbType[$_POST["cp_type_$iCommandeId"]],$_POST["cb_numcarte_$iCommandeId"],$_POST["cb_annee_$iCommandeId"],$_POST["cb_mois_$iCommandeId"],$_POST["cb_controle_$iCommandeId"]);
				if($aVerifCarte["bVerif"]){
					$aReponse = $oCb->getCoordBancaire($_SESSION['user']['IDCLIENTWEB'], $aCommande['commande_cb_ref']);
					if($aReponse['erreur']){
						//Erreur 
						$strErreurCarte = $aVerifCarte["message"] ; 
						$strCommandeBloc = $iCommandeId ; 
						$bErreur = true;
					}else{
						//On verifie que les infos de la CB correspond aux infos saisie par l'utilisateur
						$strCarteDebut = substr($aReponse['numeroCB'], 0, 5);
						$strCarteFin = substr($aReponse['numeroCB'], -1, 1);
						$strPostCarteDebut = substr($_POST["cb_numcarte_$iCommandeId"], 0, 5);
						$strPostCarteFin = substr($_POST["cb_numcarte_$iCommandeId"], -1, 1);
						if($strCarteDebut == $strPostCarteDebut && $strCarteFin == $strPostCarteFin && $aReponse['typeCB'] == $aCbType[$_POST["cp_type_$iCommandeId"]] && $aReponse['dateExpCB'] == $_POST["cb_mois_$iCommandeId"].$_POST["cb_annee_$iCommandeId"] ){
							if($oCb->deleteCb($_SESSION['user']['IDCLIENTWEB'], $aCommande['commande_cb_ref'])){
								//Carte supprimé
								$bErreur = false; 
								$bValidate = true; 
								$strCommandeBloc = $iCommandeId ; 
								$strMessageValidate = "Votre CB a bien été supprimée.";
								$strSql = "UPDATE  bor_commande SET commande_cb_ref = '' WHERE 	commande_id ='". $iCommandeId."'";
								$oDb->query($strSql);
							}else{
								//Erreur 
								$strErreurCarte = "Votre CB n'a pas pu être supprimée. Veuillez vérifier les coordonnées bancaires indiquées";
								$strCommandeBloc = $iCommandeId ; 
								$bErreur = true;
							}
						}else{
							//Erreur 
							$strErreurCarte = "Votre CB n'a pas pu être supprimée. Veuillez vérifier les coordonnées bancaires indiquées";
							$strCommandeBloc = $iCommandeId ; 
							$bErreur = true;
						}
					}
				}else{
					//La carte est invalide
					$strErreurCarte = $aVerifCarte["strMessage"] ; 
					$strCommandeBloc = $iCommandeId ; 
					$bErreur = true;
				}
				
// ****** DELETE ABONNEMENT ******
				
			}else if($strAction == "deleteabo"){
				$strResiliationBloc = $iCommandeId ;
				$oAbo = new aboEditis();
				$aAbo = $oAbo->resilierAbo($_SESSION['user']['IDCLIENTWEB'], $aCommande['commande_abo_id'],  $_POST["motif_$iCommandeId"] );
				if($aAbo['ws'] && isset($aAbo['oReponse']->abonnements->abonnement->codeReponse) && $aAbo['oReponse']->abonnements->abonnement->codeReponse == "OK"){
					$bResiliation = true; 
					$strSql = "UPDATE  bor_commande SET commande_cb_ref = '' WHERE 	commande_id ='". $iCommandeId."'";
					$oDb->query($strSql); 
					$_SESSION['bValidate'] = true;
					$_SESSION['strMessageValidate'] = "Votre abonnement a bien été résilié.";
				}else{
					$_SESSION['bErreur'] = true;
					$_SESSION['strErreurCarte'] = "Votre abonnement n’a pu être résilié. Veuillez nous contacter au ".$_CONST['RC_TEL'];
					$bResiliation = false; 
				}
				// date fin validité
				$cAbo = $oAbo->getInfoAbo($_SESSION['user']['IDCLIENTWEB'], $_SESSION['user']['NUMCLIENT']);
				if(isset($cAbo['ws']) && $cAbo['ws']){
					$cListeAboTmp = $cAbo["oReponse"] ; 
					foreach( $cListeAboTmp->abonnements as $cAbonnement){
						foreach( $cAbonnement->abonnement as $cInfosAbo){
							$id_abonnement = $cInfosAbo->idAbonnement;
						}
					}
				}
				$cAbo22 = $oAbo->getValiditeAbo($_SESSION['user']['IDCLIENTWEB'], $id_abonnement);
				$date_validite = $cAbo22['oReponse']->abonnements->abonnement->dateFinValidite;
				$date_fin_validite = substr($date_validite, 0, 2).'/'.substr($date_validite, 2, 2).'/'.substr($date_validite, 4, 4);
				
				$strMail = "Bonjour ".$_SESSION['user']['PRENOMCLIENT']." ".$_SESSION['user']['NOMCLIENT'].",<br><br>"; 
				$strMail .= "Vous venez de faire une demande de résiliation d’abonnement depuis le site ".$_CONST['URL']."<br><br>"; 
				$strMail .= "Date de demande de résiliation : ".date('d/m/Y').".<br><br><br>";
				$strMail .= "Nous vous confirmons la prise en compte de cette résiliation. Elle sera effective à la fin de votre période d’engagement en cours, soit le ".$date_fin_validite.".<br><br><br>";
				$strMail .= "Cordialement,<br><br>";
				$strMail .= "L'équipe Bordas Soutien scolaire";
					
				$strFrom = "noreply@bordas.tm.fr";
				$strFromTitre ="Bordas Soutien scolaire";
				$strTo = $_SESSION['user']['EMAIL'];
				$strSujet = 'Confirmation de résiliation d’abonnement'; 
				$strToTitre = '';
			
				$mail = new intyMailer(); 
				// OPTIONAL 
				$mail->set(array('charset'  , 'utf-8')); 
				$mail->set(array('priority' , 3)); 
				$mail->set(array('replyTo'  , $strFrom)); 
				$mail->set(array('alert '    ,  true)); 
				$mail->set(array('html'     ,  true)); 
				// OPTIONAL 
				$mail->set(array('subject' , $strSujet)); 
				$mail->set(array('message' , $strMail)); 
				$mail->set(array('sending' , 'FROM' , $strFromTitre ,$strFrom)); 
				$mail->set(array('sending' , 'TO'   , $strToTitre ,$strTo)); 
				$mail->send();
			}
		}
	}
}

/************************************************************************************************/
/*			RÉCUPÉRATION DE LA LISTE DES ABONNEMENTS AVEC TACITE RECONDUCTION					*/
/************************************************************************************************/

if( count($aListeAboEnfant) > 0 ){
// echo '<pre>';
// var_dump($aListeAboEnfant);
// echo '</pre>';
	//si une ou plusieurs ressources récupérées dans le flux
	foreach( $aListeAboEnfant as $strEnfant => $aAboEnfants){
// echo '<pre>';
// var_dump($strEnfant);
// echo '</pre>';
		//traitement du flux par enfant puis par ressource 
		foreach( $aAboEnfants as $strEan => $aRessource){
// echo '<pre>';
// var_dump($strEan);
// echo '</pre>';	
			foreach($aRessource as $aInfosRessource){
				
// echo '<pre>';
// var_dump($aInfosRessource);
// echo '</pre>';	
				
				$strSql = "SELECT * FROM bor_commande c WHERE c.produit_ean = '".mysql_real_escape_string($strEan)."' AND c.client_id='".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."' AND commande_statut = 1";
				$aCmd = $oDb->queryRow($strSql);
// echo '<pre>';
// var_dump($aCmd);
// echo '</pre>';	
				if($oDb->rows > 0 ){
					//La commande existe, il s'agit donc d'une commande réussite ou ciblee
					$strSql = "	SELECT * FROM bor_produit p 
								INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix) 
								INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) 
								INNER JOIN eco_files ef ON (ef.files_table_id = f.formule_id AND ef.files_field_name = 'formule_visuel') 
								LEFT JOIN bor_matiere m ON (m.matiere_id_yonix = p.matiere_id_yonix) 
								LEFT JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix) 
								WHERE p.produit_ean = '".mysql_real_escape_string($strEan)."'";
					$aProduit = $oDb->queryRow($strSql); 
						
					$aCommandes[$aCmd["commande_id"]]['enfants'][] = array( "name" => $strEnfant ,"ean"=>$aInfosRessource['ean'], "classe" => $aInfosRessource['classe'], "classe_id"=>$aProduit['classe_id'],"matiere_id"=>$aProduit['matiere_id'],"matiere_titre"=>$aProduit['matiere_titre'] );
					$aCommandes[$aCmd["commande_id"]]['ean'] = $aCmd["produit_ean"] ;                                                 
					$aCommandes[$aCmd["commande_id"]]['date'] = $aInfosRessource['date'] ; 
					$aCommandes[$aCmd["commande_id"]]['formule_classe'] = $aProduit['formule_classe'] ; 
					$aCommandes[$aCmd["commande_id"]]['formule_libelle'] =  $aProduit['formule_libelle'] ; 
					$aCommandes[$aCmd["commande_id"]]['files_path'] =  $aProduit['files_path'] ; 
					$aCommandes[$aCmd["commande_id"]]['produit_prix'] =  $aProduit['produit_prix'] ; 
					$aCommandes[$aCmd["commande_id"]]['de_engagement'] =  $aProduit['de_engagement'] ;
					$aCommandes[$aCmd["commande_id"]]['commande_activation'] = $aCmd["commande_activation"] ; 
					$aCommandes[$aCmd["commande_id"]]['code-action'] = (string)$aInfosRessource['action-code'];
					//Vérification si prix enregistré en base pour la commande : si oui paiement sinon offre découverte
					// if($aCmd['commande_total'] > 0 )
						// $aCommandes[$aCmd["commande_id"]]['type_offre'] = "classique" ;
					// else
						// $aCommandes[$aCmd["commande_id"]]['type_offre'] = "decouverte" ;
					
					if($aCmd['commande_total'] > 0){
						$aCommandes[$aCmd["commande_id"]]['type_offre'] = "classique" ;
					}else{
						if($aCommandes[$aCmd["commande_id"]]['code-action'] == "BSSMDL"){
							$aCommandes[$aCmd["commande_id"]]['type_offre'] = "decouverte" ;
						}else{
							$aCommandes[$aCmd["commande_id"]]['type_offre'] = "classique" ;
						}
					}
					
					$aCommandes[$aCmd["commande_id"]]['commande_id'] = $aCmd["commande_id"];
					//LOT6
					$aCommandes[$aCmd["commande_id"]]['commande_reduction'] = $aCmd["commande_reduction"] ;
				}else{
					
// echo '<pre>';
// var_dump($aRessource);
// echo '</pre>';					

					//Il s'agit d'une commande tribu
					//On va chercher dans la table enfant l'EAN TRIBU CORRESPONDANT à LA COMMANDE
					// VERIFICATION NOM ENFANT
                    $strSql = "SELECT * FROM bor_commande_enfant ce INNER JOIN bor_commande c ON (c.commande_id = ce.commande_id) WHERE ce.enfant_ean_reussite = '".mysql_real_escape_string($strEan)."' AND c.client_id='".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."' AND commande_statut = 1
					AND enfant_prenom = '".$strEnfant."'";
					$aCmd = $oDb->queryRow($strSql);
					
// echo '<pre>';
// var_dump($aInfosRessource);
// var_dump($aAboEnfants);
// echo '</pre>';					
					if($aCmd['formule_id'] == 3){
						$strSql = "	SELECT * FROM bor_produit p
									INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix)
									INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
									INNER JOIN eco_files ef ON (ef.files_table_id = f.formule_id AND ef.files_field_name = 'formule_visuel') 
									LEFT JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix)
									WHERE p.produit_ean = '".mysql_real_escape_string($aCmd['produit_ean'])."'";
						$aProduit = $oDb->queryRow($strSql);
						
						$aCommandes[$aCmd["commande_id"]]['enfants'][] = array( "name" => $strEnfant ,"ean"=>$aInfosRessource['ean'], "classe" => $aInfosRessource['classe']);
						$aCommandes[$aCmd["commande_id"]]['code-action'] = (string)$aInfosRessource['action-code'];
						$aCommandes[$aCmd["commande_id"]]['ean'] = $aCmd["produit_ean"] ; 
						$aCommandes[$aCmd["commande_id"]]['date'] = $aInfosRessource['date'] ; 
						$aCommandes[$aCmd["commande_id"]]['formule'] = "tribu" ; 
						$aCommandes[$aCmd["commande_id"]]['formule_classe'] = "tribu"; 
						$aCommandes[$aCmd["commande_id"]]['formule_libelle'] =  "Formule Tribu" ;
						$aCommandes[$aCmd["commande_id"]]['files_path'] =  $aProduit['files_path'] ; 
						$aCommandes[$aCmd["commande_id"]]['produit_prix'] =  $aProduit['produit_prix'] ; 
						$aCommandes[$aCmd["commande_id"]]['de_engagement'] =  $aProduit['de_engagement'] ; 
						$aCommandes[$aCmd["commande_id"]]['commande_id'] = $aCmd["commande_id"];
						$aCommandes[$aCmd["commande_id"]]['commande_activation'] = $aCmd["commande_activation"] ; 
						$aCommandes[$aCmd["commande_id"]]['commande_reduction'] = $aCmd["commande_reduction"] ;
						$aCommandes[$aCmd["commande_id"]]['type_offre'] = "classique" ;
					}
				}
			}
		}
	}
}

// echo '<pre>';
// var_dump($aCommandes);
// echo '</pre>';
// exit;
// echo '<pre>';
// var_dump($_SESSION['user']['IDCLIENTWEB']);
// var_dump($_SESSION['user']['NUMCLIENT']);
// echo '</pre>';
$oAbo = new aboEditis();
$aAbo = $oAbo->getInfoAbo($_SESSION['user']['IDCLIENTWEB'], $_SESSION['user']['NUMCLIENT']);

if(isset($aAbo['ws']) && $aAbo['ws']){
	$aListeAboTmp = $aAbo["oReponse"] ; 
	
// echo '<hr/><hr/><hr/><hr/><pre>DEBUG abo';
// var_dump($aListeAboTmp);
// echo '</pre><hr/><hr/><hr/><hr/>';	
	foreach( $aListeAboTmp->abonnements as $aAbonnement){
		foreach( $aAbonnement->abonnement as $aInfosAbo){
			

			$aCmdInfos = "";
			if((string)$aInfosAbo->codeReponse == "OK"){
				$strSql = "SELECT * FROM bor_commande c INNER JOIN bor_client cl ON (cl.client_id = c.client_id) WHERE commande_num = '".mysql_real_escape_string((string)$aInfosAbo->refCommandeSite)."'"; 
				$aCmdInfos = $oDb->queryRow($strSql);
				
// echo '<pre>DEBUG : ';
// var_dump($aInfosAbo->prenomEnfant);
// var_dump($aInfosAbo->idAbonnement);
// var_dump($aCmdInfos["commande_id"]);
// echo '</pre>';				
				
// echo '<pre>';
// var_dump($aCmdInfos);
// echo '</pre>';

				//if(isset($aCommandes[$aCmdInfos["commande_id"]])){
					$aCommandes[$aCmdInfos["commande_id"]]['code-action'] = (string)$aInfosRessource['action-code'];
					$aCommandes[$aCmdInfos["commande_id"]]['commande_abo_id'] = (string)$aInfosAbo->idAbonnement ;
					$aCommandes[$aCmdInfos["commande_id"]]['commande_id'] = $aCmdInfos["commande_id"];
					$aCommandes[$aCmdInfos["commande_id"]]['carte'] = (string)$aInfosAbo->idReferenceCb ;
					$aCommandes[$aCmdInfos["commande_id"]]['resilie'] = (string)$aInfosAbo->resilie ;
					$aCommandes[$aCmdInfos["commande_id"]]['commande_cb_ref'] = (string)$aInfosAbo->idReferenceCb ;
					$aCommandes[$aCmdInfos["commande_id"]]['de_engagement'] = (string)$aInfosAbo->dureeAbonnement ;
					if((string)$aInfosAbo->codePrix == "001" )
						$aCommandes[$aCmdInfos["commande_id"]]['produit_prix'] = (string)$aInfosAbo->prixTTC / 100 ;
					$aCommandes[$aCmdInfos["commande_id"]]['de_unite'] = (string)$aInfosAbo->dureeAbonnementUnite  ;
					$aCommandes[$aCmdInfos["commande_id"]]['client_num'] = $aCmdInfos["client_num"] ;
					$aCommandes[$aCmdInfos["commande_id"]]['abonnement'] = array ( 	"idAbonnement" => (string)$aInfosAbo->idAbonnement,
																			"refCommandeSite" => (string)$aInfosAbo->refCommandeSite ,
																			"origine" => (string)$aInfosAbo->origine ,
																			"login" => (string)$aInfosAbo->login ,
																			"email" => (string)$aInfosAbo->email ,
																			"prixTTC" => (string)$aInfosAbo->prixTTC ,
																			"codePrix" => (string)$aInfosAbo->codePrix ,
																			"dureeAbonnement" => (string)$aInfosAbo->dureeAbonnement ,
																			"dureeAbonnementUnite" => (string)$aInfosAbo->dureeAbonnementUnite ,
																			"taciteReconduction" => (string)$aInfosAbo->taciteReconduction ,
																			"dateFinValidite" => (string)$aInfosAbo->dateFinValidite );

					// ****** Mise à jour des infos cb et abonnement
					
					if(empty($aCmdInfos["commande_abo_id"]) && !empty($aInfosAbo->idAbonnement))
						$oDb->query("UPDATE bor_commande SET commande_abo_id = '".mysql_real_escape_string((string)$aInfosAbo->idAbonnement)."' WHERE commande_id = '".$aCmdInfos["commande_id"]."'");
					if(empty($aCmdInfos["commande_cb_ref"]) && !empty($aInfosAbo->idReferenceCb))
						$oDb->query("UPDATE bor_commande SET commande_cb_ref = '".mysql_real_escape_string((string)$aInfosAbo->idReferenceCb)."' WHERE commande_id = '".$aCmdInfos["commande_id"]."'");
					if($aCommandes[$aCmdInfos["commande_id"]]['resilie'] == "O")
						$oDb->query("UPDATE bor_commande SET commande_resiliation = 1 WHERE commande_id = '".$aCmdInfos["commande_id"]."'");
				//}
				
// echo '<pre>aCmdInfos ';
// var_dump($aCommandes);
// echo '</pre>';
			}
		}
	}
}

/************************************************************************************************/
/*										DÉBUT DE LA PAGE	:									*/
/************************************************************************************************/

include('./breadcrumb_classique.php');?>
<div class="bss-section bloc-section-bleu bss-gestion-abonnement">
  <div class="container">
    <div class="row">
		<div class="col-md-12">
			<h1 class="h1">Gérer mon abonnement</h1>
				<?php
				$strDateNow = time();
				//Récupération de la liste des abonnements du plus récent au plus ancien 
				$strSql = "	SELECT * FROM bor_commande c 
							INNER JOIN bor_client cl ON (cl.client_id = c.client_id) 
							INNER JOIN bor_produit p ON (c.produit_ean = p.produit_ean)
							INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
							INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix)
							WHERE c.client_id = '".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."'
							AND c.commande_statut = 1
							ORDER BY c.commande_id DESC"; 
				
				$commandeOk = 0;
				
/************************************************************************************************/
/*							SI UNE OU DES COMMANDES SONT RETROUVEES								*/
/************************************************************************************************/

				if($aCommandes){
					foreach($aCommandes as $key=>$aCommande){
						if(isset($aCommande['commande_id']) && !empty($aCommande['commande_id'])){
							$commandeOk = 1;
						}
					}
					if($commandeOk == 1){
					?>
						<h2 class="">Abonnement(s) en cours :</h2>

					<?php
					
/************************************************************************************************/
/*				GESTION DES MESSAGES DE VALIDATION ET MESSAGES D'ERREUR :						*/
/************************************************************************************************/					
					
						if($bErreur){
							echo  '
							<div class="alert alert-danger" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong>Erreur !</strong><br>
							'.$strErreurCarte.' </div>';
						}
						if($bValidate){
							echo  '
							<div class="alert alert-success" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong>Merci !</strong><br>
							'.$strMessageValidate.' </div>';				
						}
						if(isset($_SESSION['bErreur'])){
							echo  '
							<div class="alert alert-danger" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong>Erreur !</strong><br>
							'.$_SESSION['strErreurCarte'].' </div>';
							unset($_SESSION['bErreur']); 
							unset($_SESSION['strErreurCarte']); 
						}
						if(isset($_SESSION['bValidate'])){
							echo  '
							<div class="alert alert-success" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<strong>Merci !</strong><br>
							'.$_SESSION['strMessageValidate'].' </div>';	
							unset($_SESSION['bValidate']); 
							unset($_SESSION['strMessageValidate']); 							
						}
					}

/************************************************************************************************/
/*				PARCOURS DE CHAQUE ABONNEMENT POUR RÉCUPÉRER LES INFOS :						*/
/************************************************************************************************/
				
					$nombre_abonnements_actifs = 0;
					foreach($aCommandes as $key=>$aCommande){
						
					// echo '<pre>ABO :';
					// var_dump($aCommande['commande_id']);
					// echo '</pre>';
					
						$strDateAbo ="";
						//Récupération de la liste des enfants rattachés à cette commande 
						$strSql = "SELECT * FROM bor_commande_enfant ce INNER JOIN bor_classe c ON (c.classe_id = ce.classe_id) LEFT JOIN bor_matiere m ON (m.matiere_id = ce.matiere_id) WHERE ce.commande_id = '".mysql_real_escape_string($aCommande['commande_id'])."' ORDER BY ce.enfant_id DESC";
						$aEnfants = $oDb->queryTab($strSql);
							//On va récupérer les infos de l'abonnement  
							//if(isset($aCommande['commande_id']) && !empty($aCommande['commande_id'])){
							if(isset($aCommande) && !empty($aCommande)){
								$bAboEnCours = true;
								$strDate = $aCommande['date'];
								$strDateAbo = strtotime($strDate);
								
								// Modif 08 03 2016 : bug sur date format string
								//$strDate = $aCommande['date'];
								//$strDateAbo = strtotime($strDate);
								if(isset($aCommande["abonnement"]['dateFinValidite']) && !empty($aCommande["abonnement"]['dateFinValidite'])){
									$date = DateTime::createFromFormat('dmY', $aCommande["abonnement"]['dateFinValidite']);
									$strDateAbo = strtotime($date->format('Y-m-d'));
									$datetime = $date->format('Y-m-d');
								}else{
									$strDate = $aCommande['date'];
									$strDateAbo = strtotime($strDate);
									$datetime = $strDate;
								}
								
								$strDiff  = $strDateAbo - $strDateNow;
								$iDiffJour = round($strDiff/86400)  ;
								if($iDiffJour < 0 )
									$bAboEnCours = false;
								$oDate = new DateTime( $datetime );
								$strDateAbo =  $oDate->format("d/m/Y");
//var_dump($aCommande['date']);
								if($bAboEnCours){
									$nombre_abonnements_actifs++;
									//On va tester la validité de la carte bancaire via la classe cb
									$oCb = new cb();
									//Si pas de variable postée, on va récupérer les infos de la carte bancaire pour les afficher dans le formulaire 
									if(!isset($_POST['action']) || empty($_POST['action'])){
										$aReponse = $oCb->getCoordBancaire($_SESSION['user']['IDCLIENTWEB'], $aCommande['commande_cb_ref']);
										if(!$aReponse['erreur']){
											$_POST["cp_type_".$aCommande['commande_id']] = $aTypeCb[$aReponse['typeCB']];
											$_POST["cb_numcarte_".$aCommande['commande_id']] = $aReponse['numeroCB'];
											$_POST["cb_name_".$aCommande['commande_id']] = $aReponse['porteurCB'];
											$_POST["cb_mois_".$aCommande['commande_id']] = substr($aReponse['dateExpCB'], 0, 2) ; 
											$_POST["cb_annee_".$aCommande['commande_id']] = substr($aReponse['dateExpCB'],-2,2) ; 	
										}		
									}

									if($bAboEnCours && $oCb->verifExpirationCarte($aCommande['client_num'],$aCommande['commande_cb_ref'])){//si true la carte expire dans 30 jours
										echo '
												<div class="warn">
													<h2>Attention !</h2>
													<p>La date de validité de votre CB arrive bientôt à expiration.
													Vous devez mettre à jour votre CB afin de continuer à utiliser notre service de Soutien scolaire.
													</p>
													<div class="center"><a href="#" onclick="afficherBloc(\''.$aCommande['commande_id'].'\');" class="btn btn-primary btn-fw ">Modifier ma CB »</a></div>
												</div>
												';
									}
									$oInfosCarte = $oCb->getInfoCarte($_SESSION['user']['IDCLIENTWEB'],$aCommande['commande_cb_ref']) ;
									
/************************************************************************************************/
/*						DÉBUT DE CHAQUE BLOC D'ABONNEMENT EN COURS :							*/
/************************************************************************************************/	

									?>
									<div id="abo-en-cours-<?php echo $key; ?>" class="abo-en-cours clearfix">
										<div style="min-height:200px;" class="col-sm-2 col-md-2" >
											
											<?php
											if($aCommande["type_offre"] == "classique"){
												?>
												<img src="<?php echo $aCommande['files_path']; ?>" class="img-responsive">
												<?php
											}else{
												?>
												<img src="/uploads/formules/454.png" class="img-responsive">
												<?php
											}
											?>
										</div>
										<div class="col-sm-10 col-md-7">
											<?php
 // echo '<pre>';
 // var_dump($aEnfants);
 // echo '</pre>';
											if($aCommande["type_offre"] == "classique"){
												echo '<h3 class="h3">'.$aCommande['formule_libelle'].' <span>valable jusqu\'au '.$strDateAbo.'</span></h3>';
												if(count($aEnfants) > 1)
													echo '<p class="nombre-enfant"><strong>'.count($aEnfants).' enfants</strong></p>';
											}
											else{
												?>
												<h3 class="h3">Formule réussite <span>- Offre Découverte</span></h3>
												<?php
											}
										
											//Affichage du detail de l'abonnement
											echo '<ul class="abonnement-recap">'; 
											foreach($aEnfants as $aEnfant){
												$strSql = "SELECT classe_name FROM bor_classe c WHERE c.classe_id = '".$aEnfant['classe_id']."'";
												$iClasseEnfant = $oDb->queryItem($strSql);
												if($aEnfant['enfant_prenom'] == "")
													$aEnfant['enfant_prenom'] = "enfant";
												echo '<li class="enfant-recap"> '.$aEnfant['enfant_prenom'].' révise en '.(!empty($aEnfant['matiere_id'])  ? $aEnfant['matiere_titre'].' - ' : '') .(!empty($iClasseEnfant)  ? $iClasseEnfant : $aEnfant['classe']).'</li>';
											}
											echo '</ul>';
											?>
									<div class="panel-group" id="gestion-abonnement-<?php echo $key;?>" role="tablist" aria-multiselectable="true">
											<div class="menu-panel ">
											
												<a role="button" data-toggle="collapse" data-parent="#gestion-abonnement-<?php echo $key;?>" href="#details-abo-<?php echo $key;?>" aria-expanded="true" aria-controls="details-abo" class="btn btn-default collapsed"> <i class="icon-search"></i> Voir les détails </a>
												
												<?php if($aCommande["type_offre"] == "classique"){ ?>
												
													<a role="button" data-toggle="collapse" data-parent="#gestion-abonnement-<?php echo $key;?>" href="#modifier-abo-<?php echo $key;?>" aria-expanded="false" aria-controls="modifier-abo" class="btn btn-default collapsed"> <i class="icon-pencil"></i> Modifier </a>
														
												<?php } ?>
												
											</div>
											<div class="panel ">
												<div id="details-abo-<?php echo $key;?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
													<div class="bloc-bss-1">
														<p class="h4">Offre d'abonnement :</p>
														
														<?php
														// echo '<pre>';
														// var_dump($aCommande['abonnement']);
														// echo '</pre>';
														?>
														<ul>
															<li>
																<strong>
																	<?php
																	if($aCommande["type_offre"] == "classique")
																		echo $aCommande['formule_libelle'];
																	else
																		echo 'Formule Réussite';
																	?>
																</strong>
															</li>
															<li>
																Date de fin : <strong><?php echo $strDateAbo;?></strong>
																<?php if(isset($aCommande['abonnement']['taciteReconduction']) && $aCommande['abonnement']['taciteReconduction'] == "OUI") echo 'avec tacite reconduction';?>
															</li>
															<li>
																Numéro de commande : <strong><?php echo $aCommande['abonnement']['refCommandeSite'];?></strong>
															</li>
															<li>
																Prix TTC :
																<?php
																if($aCommande["type_offre"] == "classique"){
																	if(!empty($aCommande["commande_activation"])){
																					echo '<strong>Gratuit</strong>';
																	}else{
																		if(isset($aCommande['commande_reduction']) && $aCommande['commande_reduction'] > 0){
																		$newPrice = number_format(round( ($aCommande['produit_prix'] - ( $aCommande['produit_prix'] * $aCommande['commande_reduction'] / 100)), 2), 2, ',', '' ) ; 
																		echo '<strong>'.str_replace('.',',',$newPrice) ."€</strong> au lieu de ".$aCommande['produit_prix'].'€ '.( $aCommande['de_engagement'] > 1 ? 'pour les 3 premiers mois' : 'pour le premier mois' );
																		}else{
																			if($aCommande['de_engagement'] == 12){
																				echo '<strong>'.str_replace('.',',',$aCommande['produit_prix']).'€</strong> durant 12 mois.';
																			}else{
																				echo '<strong>'.str_replace('.',',',$aCommande['produit_prix']).'€</strong> tous les '.( $aCommande['de_engagement'] > 1 ? $aCommande['de_engagement'] : '' ).' mois.';
																			}
																		}
																	}
																}else{
																	echo '<strong>Gratuit</strong>';
																}
																?>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<?php
											if($aCommande['formule_classe'] == "ciblee"){
												$ListClasses  = $oDb->queryTab("SELECT c.* FROM bor_classe c 
																				INNER JOIN bor_matiere_classe mc ON (mc.classe_id = c.classe_id) 
																				INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id) 
																				INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 														
																				WHERE mc.mc_dispo = 1
																				GROUP BY c.classe_id
																				ORDER BY n.niveau_position, cn.cn_position");
											}else if($aCommande['formule_classe'] == "reussite"){
												$ListClasses  = $oDb->queryTab("SELECT c.* FROM bor_classe c 
																				INNER JOIN bor_produit p ON (p.classe_id_yonix = c.classe_id_yonix) 
																				INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) 
																				INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id) 
																				INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 		
																				WHERE f.formule_classe = 'reussite' 
																				GROUP BY c.classe_id
																				ORDER BY n.niveau_position, cn.cn_position"); 
											}else if($aCommande['formule_classe'] == "tribu"){
												$ListClasses  = $oDb->queryTab("SELECT c.* FROM bor_classe c 
																				INNER JOIN bor_produit p ON (p.classe_id_yonix = c.classe_id_yonix) 
																				INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) 
																				INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id) 
																				INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 		
																				WHERE f.formule_classe = 'reussite' 
																				GROUP BY c.classe_id
																				ORDER BY n.niveau_position, cn.cn_position"); 
											}
											
/************************************************************************************************/
/*								BLOC MODIFICATION DE L'ABONNEMENT								*/
/************************************************************************************************/

											?>
											<div class="panel ">
												<div id="modifier-abo-<?php echo $key;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
													<div class="bloc-bss-1">
														
														<?php
														// ****** SI L'ABONNEMENT N'EST PAS DÉJÀ RÉSILIÉ
														//if($aCommande['resilie'] =="N"){
														?>
														
															<p class="h4">Modifier l'abonnement</p>
															<div class="form-horizontal">

															<!-- ****** SELECT : CHOIX DE L'ENFANT -->
															
																<div class="form-group">
																	<label for="modif-enfant" class="col-sm-6 control-label">Je modifie l'abonnement de </label>
																	<div class="col-sm-6">
																	<?php
																	if($aCommande['formule_classe'] == "tribu"){
																	?>
																		<select class="enfant_tribu form-control">
																			<?php
																			foreach($aEnfants as $key_enfant=>$aEnfant){
																			?>
																			<option value="<?php echo $key.'-'.$key_enfant;?>">
																				<?php
																				echo $aEnfant['enfant_prenom'];
																				?>
																			</option>
																			<?php
																			}
																			?>
																		</select>
																	<?php
																	}else{
																	foreach($aEnfants as $key_enfant=>$aEnfant){
																		?>
																		<select style="display:none;	" class="enfant_tribu form-control">
																			<?php
																			foreach($aEnfants as $key_enfant=>$aEnfant){
																			?>
																			<option value="<?php echo $key.'-'.$key_enfant;?>">
																				<?php
																				echo $aEnfant['enfant_prenom'];
																				?>
																			</option>
																			<?php
																			}
																			?>
																		</select>
																	<?php
																			echo '<label class=" control-label">'.$aEnfant['enfant_prenom'].'</label>';
																		}
																	}	
																	?>
																	</div>
																</div>
															</div>		
														
														<?php
														//}
														// ****** FIN : SI L'ABONNEMENT N'EST PAS DÉJÀ RÉSILIÉ
														?>
																
														
															<?php
															
															// ****** BLOC DE MODIFICATION POUR CHAQUE ENFANT :
															// ****** les blocs sont masqués et affichés au select de l'enfant
															
															foreach($aEnfants as $key_enfant=>$aEnfant){
																$enf_classe_id_yonix  = $oDb->queryItem("	SELECT p.classe_id_yonix
																											FROM bor_produit p
																											WHERE p.produit_ean = '".$aEnfant['enfant_ean_reussite']."'");
																$enfantClasse2  = $oDb->queryItem("	SELECT c.classe_id
																									FROM bor_classe c
																									WHERE c.classe_id_yonix = ".$enf_classe_id_yonix);
																?>
															<form class="form-horizontal" action="/templates/update_abonnement.php" method="post" id="modif-abonnement-<?php echo $key.'-'.$key_enfant;?>">
																<div id="bloc-enfant-<?php echo $key.'-'.$key_enfant; ?>" class="bloc-enfant" style="display:none;">
																
																	<div class="form-group">
																		<label for="modif-classe" class="col-sm-6 control-label">Classe</label>
																		<div class="col-sm-6">
																			<?php
																			if($aCommande['formule_classe'] == "ciblee"){
																				?>
																				<select name="ciblee-classe-<?php echo $key.'-'.$key_enfant;?>" class="form-control classe" id="ciblee-classe-<?php echo $key.'-'.$key_enfant;?>" form="modif-abonnement-<?php echo $key.'-'.$key_enfant; ?>" title="tribu-classe">
																				<?php
																			}else{
																				?>
																				<select name="classe-<?php echo $key.'-'.$key_enfant;?>" class="form-control classe" id="classe-<?php echo $key.'-'.$key_enfant;?>" form="modif-abonnement-<?php echo $key.'-'.$key_enfant; ?>" title="tribu-classe">
																				<?php
																			}
																			?>
																			<option value="0">Choisir une classe</option>'; 
																			<?php	
																			if($ListClasses){ 
																				foreach($ListClasses as $classe){
																					echo '<option value="'.$classe['classe_id'].'" ';
																					if ( isset($enfantClasse2) && $enfantClasse2 == $classe['classe_id']) echo ' style="display:none;" ';
																					echo	' >';
																					echo $classe['classe_name'];
																					echo '</option>'; 
																				}  
																			}
																			?>
																				</select>
																		</div>
																	</div>
																	<?php
																	if($aCommande['formule_classe'] == "reussite" || $aCommande['formule_classe'] == "tribu"){
																		?>
																		<div class="form-group">
																			<label for="modif-matiere" class="col-sm-6 control-label">Matière(s)</label>
																			<div class="col-sm-6">
																				<p class="form-control-static">Multi-matières</p>
																			</div>
																		</div>
																		<?php
																	}else{
																		?>
																		<div class="form-group">
																			<label for="matiere[0]" class="col-sm-6 control-label">Matière</label>
																			<div class="col-sm-6">
																				<select name="matiere-ciblee-classe-<?php echo $key.'-'.$key_enfant;?>" class="form-control" id="matiere-ciblee-classe-<?php echo $key.'-'.$key_enfant;?>" form="modif-abonnement-<?php echo $key.'-'.$key_enfant; ?>" title="tribu-matiere">
																				<?php if(isset($aEnfant['matiere_id'])){
																								echo '<option value='.$aEnfant['matiere_id'].'>'.$aEnfant['matiere_titre'].'</option>';
																				}
																				?>
																				</select>
																			</div>
																		</div>
																		<?php
																	}
																	?>
																	<div class="form-group">
																		<div class="col-sm-6 hidden-xs"></div>
																		<div class="col-sm-6">
																			<?php
																			echo '<input type="hidden" name="nb_enfant" form="modif-abonnement-'.$key.'-'.$key_enfant.'" value="'.$key_enfant.'">';
																			echo '<input type="hidden" name="action" form="modif-abonnement-'.$key.'-'.$key_enfant.'" value="updateabo">';
																			echo '<input type="hidden" name="idcommande" form="modif-abonnement-'.$key.'-'.$key_enfant.'" value="'.$aCommande['commande_id'].'" >';
																			?>
																			<button type="submit" class="btn btn-primary btn-fw ">Je valide <i class="icon-ok"></i></button>
																		</div>
																	</div>
																
																</div>
															</form>
																<?php
															}

/************************************************************************************************/
/*								MODIFICATION CB / RÉSILIATION									*/
/************************************************************************************************/

														// si tacite reconduction et offre classique :
														if($aCommande['de_engagement'] < 12 && $aCommande["type_offre"] == "classique"){
															echo '<ul>';
															if( !$oInfosCarte['erreur'] && $aCommande['resilie'] =="N" ){
																echo '<li><a data-toggle="modal" data-target="#div_cb_'.$aCommande['commande_id'].'" id="updatecb_'.$aCommande['commande_id'].'" href="#abo-en-cours-'.$aCommande['commande_id'].'"  class="blue_link">Modifier mes coordonnées bancaires</a></li>';
															}
															if( $aCommande['resilie'] == "O" ){
																echo '<li>Votre abonnement se termine à la fin de votre période d’engagement le '.$strDateAbo.'</li>';
															}else{
																echo '<li><a data-toggle="modal" data-target="#div_resilier_'.$aCommande['commande_id'].'" id="resilier_'.$aCommande['commande_id'].'" href="#abo-en-cours-'.$aCommande['commande_id'].'" class="blue_link">Résilier mon abonnement</a></li>';
															}
															echo '</ul>';
														}
													?>
													</div>
													<?php

/************************************************************************************************/
/*									BLOC MODIFICATION CB POPUP									*/
/************************************************************************************************/

									?>
									<div class="modal fade modif_cb" id="div_cb_<?php echo $aCommande['commande_id']; ?>" >
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<?php
													if( isset($strAction) && $strAction =="deletecb")
														echo '<h4 class="modal-title" id="modif_titre_'.$aCommande['commande_id'].'"> Supprimer ma CB</h4>';
													else
														echo '<h4 class="modal-title" id="modif_titre_'.$aCommande['commande_id'].'"> Modifier ma CB</h4>';
												?>
												</div>
												<div class="modal-body">
													<?php
													if( isset($strAction) && $strAction =="deletecb")
														echo '		<p id="message_cb_'.$aCommande['commande_id'].'">En supprimant ma CB, je mets fin à mon abonnement Bordas Soutien Scolaire à la fin de ma période d\'engagement.</p>';
													else
														echo '		<p id="message_cb_'.$aCommande['commande_id'].'">Je renseigne les coordonnées de ma nouvelle CB.</p>';
													echo '		<form class="form-horizontal" role="form" method="post" id="form_cb_'.$aCommande['commande_id'].'">
																	<div class="form-group nom_detenteur_cb">
																		<label for="inputnomdetenteur" class="col-sm-6 control-label">Nom du porteur de la carte </label>
																		<div class="col-sm-6">
																			<input type="text" class="form-control" id="inputnomdetenteur"   maxlength="20"  name="cb_name_'.$aCommande['commande_id'].'" '. ( (isset($_POST['cb_name_'.$aCommande['commande_id']]) ) ? 'value="'.$_POST['cb_name_'.$aCommande['commande_id']].'"' : '' ) .'>
																		</div>
																	</div>
																	<div class="form_cb">
																		<div class="form-group">
																			<label for="inputytpecarte" class="col-sm-6 control-label">Type de carte</label>
																			<div class="col-sm-6">
																				<label class="radio-inline" >
																					<input type="radio" name="cp_type_'.$aCommande['commande_id'].'" value="1" '.( (isset($_POST['cp_type_'.$aCommande['commande_id']]) && $_POST['cp_type_'.$aCommande['commande_id']] == 1) ? "checked" : "").'> 
																					<span class="icone-visa">Visa</span> </label>
																				</label>
																				<label class="radio-inline">
																					<input type="radio"  name="cp_type_'.$aCommande['commande_id'].'" value="2" '.( (isset($_POST['cp_type_'.$aCommande['commande_id']]) && $_POST['cp_type_'.$aCommande['commande_id']] == 2) ? "checked" : "").'> 
																					<span class="icone-mastercard">Mastercard</span> </label>
																				</label>
																				<label class="radio-inline">
																					 <input type="radio"  name="cp_type_'.$aCommande['commande_id'].'" value="3" '.( (isset($_POST['cp_type_'.$aCommande['commande_id']]) && $_POST['cp_type_'.$aCommande['commande_id']] == 3) ? "checked" : "").'> 
																					 <span class="icone-cb">CB</span> </label>
																				</label>
																			</div>
																		</div>
																		<div class="form-group has-feedback">
																			<label for="inputnumcarte" class="col-sm-6 control-label">Numéro de carte </label>
																			<div class="col-sm-6">
																				<input type="text" class="form-control" id="inputnumcarte" name="cb_numcarte_'.$aCommande['commande_id'].'"  '. ( (isset($_POST['cb_numcarte_'.$aCommande['commande_id']]) ) ? 'value="'.$_POST['cb_numcarte_'.$aCommande['commande_id']].'"' : '' ) .'>
																				<i class="icon-shield form-control-feedback"></i>
																			</div>
																		</div>
																		<div class="form-group">
																		<label for="inputnumcarte" class="col-sm-6 control-label">Expiration </label>
																		<div class="col-sm-3">
																			 
																				<select class="form-control"  name="cb_mois_'.$aCommande['commande_id'].'">';
																			?>
																					<option value="01" <?php if(isset($_POST['cb_mois_'.$aCommande['commande_id']]) && $_POST['cb_mois_'.$aCommande['commande_id']] =="01" ) echo "selected"; ?>>01</option>
																					<option value="02" <?php if(isset($_POST['cb_mois_'.$aCommande['commande_id']]) && $_POST['cb_mois_'.$aCommande['commande_id']] =="02" ) echo "selected"; ?>>02</option>
																					<option value="03" <?php if(isset($_POST['cb_mois_'.$aCommande['commande_id']]) && $_POST['cb_mois_'.$aCommande['commande_id']] =="03" ) echo "selected"; ?>>03</option>
																					<option value="04" <?php if(isset($_POST['cb_mois_'.$aCommande['commande_id']]) && $_POST['cb_mois_'.$aCommande['commande_id']] =="04" ) echo "selected"; ?>>04</option>
																					<option value="05" <?php if(isset($_POST['cb_mois_'.$aCommande['commande_id']]) && $_POST['cb_mois_'.$aCommande['commande_id']] =="05" ) echo "selected"; ?>>05</option>
																					<option value="06" <?php if(isset($_POST['cb_mois_'.$aCommande['commande_id']]) && $_POST['cb_mois_'.$aCommande['commande_id']] =="06" ) echo "selected"; ?>>06</option>
																					<option value="07" <?php if(isset($_POST['cb_mois_'.$aCommande['commande_id']]) && $_POST['cb_mois_'.$aCommande['commande_id']] =="07" ) echo "selected"; ?>>07</option>
																					<option value="08" <?php if(isset($_POST['cb_mois_'.$aCommande['commande_id']]) && $_POST['cb_mois_'.$aCommande['commande_id']] =="08" ) echo "selected"; ?>>08</option>
																					<option value="09" <?php if(isset($_POST['cb_mois_'.$aCommande['commande_id']]) && $_POST['cb_mois_'.$aCommande['commande_id']] =="09" ) echo "selected"; ?>>09</option>
																					<option value="10" <?php if(isset($_POST['cb_mois_'.$aCommande['commande_id']]) && $_POST['cb_mois_'.$aCommande['commande_id']] =="10" ) echo "selected"; ?>>10</option>
																					<option value="11" <?php if(isset($_POST['cb_mois_'.$aCommande['commande_id']]) && $_POST['cb_mois_'.$aCommande['commande_id']] =="11" ) echo "selected"; ?>>11</option>
																					<option value="12" <?php if(isset($_POST['cb_mois_'.$aCommande['commande_id']]) && $_POST['cb_mois_'.$aCommande['commande_id']] =="12" ) echo "selected"; ?>>12</option>
																			<?php
													echo '						</select>
																			</div>
																			
																			<div class="col-sm-3"> 
																				<select class="form-control"  name="cb_annee_'.$aCommande['commande_id'].'" >'; 
																				for($i = 0; $i<10; $i++){
																					echo "<option value='".(date('y')+$i)."' ".((isset($_POST['cb_annee_'.$aCommande['commande_id']]) && $_POST['cb_annee_'.$aCommande['commande_id']] ==(date('y')+$i))? "selected" : "").">".(date('y')+$i)."</date>";
																				}
													echo '						</select>
																			</div>
																		
																		</div>
																		<div class="form-group has-feedback">
																			<label for="inputnumcontrole" class="col-sm-6 control-label">Numéro de contrôle </label>
																			<div class="col-sm-3">
																				<input type="text" class="form-control" id="inputnumcontrole" placeholder="CVV" name="cb_controle_'.$aCommande['commande_id'].'"  '. ( (isset($_POST['cb_controle_'.$aCommande['commande_id']]) ) ? 'value="'.$_POST['cb_controle_'.$aCommande['commande_id']].'"' : '' ) .'>
																				<i class="icon-shield form-control-feedback"></i>
																			</div>';
																			?>
																<div class="col-sm-3">
																	<a id="popover" 	class="btn btn-primary btn-icon action_popover" tabindex="0"  role="button"  data-trigger="focus" rel="popover" data-content="" title="Emplacement du CVV" >?</a>
																	
																</div> 
																			<?php
													echo '				</div>
																	</div>';
													if( isset($strAction) && $strAction =="deletecb")	
														echo '			<input type="hidden"  id="action_'.$aCommande['commande_id'].'" name="action"  value="deletecb"  > ';
													else
														echo '			<input type="hidden"  id="action_'.$aCommande['commande_id'].'" name="action"  value="updatecb"  > ';
													echo '				<input type="hidden"   name="idcommande"  value="'.$aCommande['commande_id'].'"  > 
																
																<div class="form-group">
																<div class="col-sm-6"> &nbsp;</div>
																<div class="col-sm-6">';
													if( isset($strAction) && $strAction =="deletecb")		
														echo '			<a href="#" id="btn_'.$aCommande['commande_id'].'" onclick="$(\'#form_cb_'.$aCommande['commande_id'].'\').submit();" class="btn btn-primary btn-fw " style="display:block;">Supprimer ma CB »</a>';
													else
														echo '			<a href="#" id="btn_'.$aCommande['commande_id'].'" onclick="$(\'#form_cb_'.$aCommande['commande_id'].'\').submit();" class="btn btn-primary btn-fw " style="display:block;">Mettre à jour ma CB <i class="icon-ok "></i></a>';
													echo'		</div></div>';
													if( !isset($strAction) || $strAction !="deletecb")
													echo '		<hr/>
																<div class="well bloc_delete_'.$aCommande['commande_id'].'">
																	<div class="form-group">
																		<div class="col-sm-12">
																			<div class="bloc_delete_'.$aCommande['commande_id'].'"></div>
																				<a href="#" onclick="supprimerCarte(\''.$aCommande['commande_id'].'\');" class="btn btn-danger btn-fw bloc_delete_'.$aCommande['commande_id'].'"><i class="icon-cancel"></i> Supprimer ma CB</a>
																			</div>
																		</div>
																		<p  class="bloc_delete_'.$aCommande['commande_id'].'">En supprimant ma CB, je mets fin à mon abonnement Bordas Soutien scolaire
																		à la fin de ma période d’engagement.</p>
																	</div>
																';?>
												</div>
												</form>
											</div>
										</div>
									</div>
<?php
// FIN BLOC POPUP MODIFICATION CB		

/************************************************************************************************/
/*									BLOC RESILIATION ABONNEMENT POPUP							*/
/************************************************************************************************/
?>
									<div id="div_resilier_<?php echo $aCommande['commande_id']; ?>" class=" modal fade" tabindex="-1" role="dialog" aria-labelledby="raModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" >Résilier mon abonnement </h4>
												</div>
												<div class="modal-body">
													<?php
													if($iDiffJour > 5){
														if(!(isset($strResiliationBloc) && $aCommande['commande_id'] == $strResiliationBloc && $bResiliation)){
															//Récupération de la date de fin de résiliation : date abo - 5J
															$strDateResiliation = $oDb->queryItem('SELECT DATE_FORMAT(DATE_ADD("'.$aCommande['date'].'", INTERVAL -5 DAY), "%d/%m/%Y")'); 
															echo'		<div class="well"><p>
																			Vous pouvez demander à résilier votre abonnement. Cette résiliation sera effective à la fin de votre période d’engagement en cours. 
																			Pour ne pas reconduire votre abonnement '.($aCommande['de_engagement'] == 1 ? "d'" : "de ").$aCommande['de_engagement'].' mois
																			'. ( $aCommande['de_engagement'] == 1 ? "supplémentaire" : "supplémentaires" ).', vous devez le résilier avant le '.$strDateResiliation.'. Ces dispositions sont précisées dans les <a href="/conditions-generales-de-vente.html" >conditions générales de vente.</a>
																		</p></div>
																		<form class="form-horizontal" role="form" id="form_resiliation_'.$aCommande['commande_id'].'" method="post">
																			
																				<div class="form-group">
																					<label class="col-sm-12 control-label" for="exampleInputEmail1">Motif de la résiliation</label>
																					<div class="col-sm-12">
																					<textarea class="form-control" rows="3" name="motif_'.$aCommande['commande_id'].'">'. ( (isset($_POST['motif_'.$aCommande['commande_id']]) ) ? $_POST['motif_'.$aCommande['commande_id']] : '' ) .'</textarea>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="col-sm-6"> &nbsp;</div>
																					<div class="col-sm-6">
																						<a href="#" class="btn btn-primary btn-fw " onclick="$(\'#form_resiliation_'.$aCommande['commande_id'].'\').submit();">Je résilie mon abonnement »</a>
																					</div>
																				</div>
																			<input type="hidden"  id="action_'.$aCommande['commande_id'].'" name="action"  value="deleteabo"  >
																			<input type="hidden"   name="idcommande"  value="'.$aCommande['commande_id'].'"  > 
																		</form>';
														}
													}else{
														echo'		<p>
																		 Vous pouvez demander à résilier votre abonnement. Toutefois vous avez dépassé la date limite pour la période en cours, 
																		 votre abonnement sera donc reconduit de '.$aCommande['de_engagement'].' mois '. ( $aCommande['de_engagement'] == 1 ? "supplémentaire" : "supplémentaires" ).'. Vous pourrez le résilier jusqu’à 5 jours 
																		 avant la fin de cette période pour qu’il ne soit pas reconduit une nouvelle fois. Ces dispositions sont précisées dans 
																		 les <a href="#"  data-toggle="modal" data-target="#modal_cgv" >conditions générales de vente</a>.
																	</p>';
													}
												?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
								<div class="col-sm-offset-2 col-sm-10 col-md-offset-0 col-md-3">
									<?php 
									$url_plateforme = "";
									
									if(isset($aCommande['code-action']) && $aCommande['code-action'] != ""){
										$url_plateforme = $oDb->queryItem('SELECT activation_plateforme FROM bor_activation WHERE bill_codeaction = "'.$aCommande['code-action'].'"');
										if(empty($url_plateforme)){
											$url_plateforme = "";
										}
									}else{
										$url_plateforme = "http://bordas-soutien-scolaire.eduplateforme.com";
									}
									
									if($url_plateforme != ""){
										?>
										<div class="bloc-bss-step-sidebar bloc-bss-1">
											<div class="bloc-bss-produit-titre text-center">Accéder à la plateforme</div>
											<div class="text-center"><a type="button" target="_blank" href="<?php echo $url_plateforme; ?>" class="btn  btn-primary ">Connexion<i class="icon-angle-right"></i></a></div>
										</div>
										<?php
									}
									?>
								</div>
							</div>
							<?php
							echo '<hr/>';
						}
					}
				}
						
/************************************************************************************************/
/*							FIN DU FOREACH DES ABONNEMENTS EN COURS								*/
/************************************************************************************************/ 
						
						if($nombre_abonnements_actifs == 0){
							echo '<p class="gras_txt"><span class="color_cible"> Vous n’avez pas d’abonnement en cours.</span></p>';
						}

/************************************************************************************************/
/*								DÉBUT DES ABONNEMENTS TERMINÉS									*/
/************************************************************************************************/
						
						$commandesTerminees = 0;
						foreach($aCommandes as $key=>$aCommande){
							$strDateAbo ="";
							if(isset($aCommande['commande_id']) && !empty($aCommande['commande_id'])){
								// $bAboEnCours = true;
								// $strDate = $aCommande['date'];
								// $strDateAbo = strtotime($strDate);
								// $strDiff  = $strDateAbo - $strDateNow;
								// $iDiffJour = round($strDiff/86400)  ;
								// if($iDiffJour < 0 )
								// $bAboEnCours = false;
								// $oDate = new DateTime( $strDate );
								// $strDateAbo = $oDate->format("d/m/Y");
								
								$bAboEnCours = true;
								$strDate = $aCommande['date'];
								$strDateAbo = strtotime($strDate);
								
								// Modif 08 03 2016 : bug sur date format string
								//$strDate = $aCommande['date'];
								//$strDateAbo = strtotime($strDate);
								if(isset($aCommande["abonnement"]['dateFinValidite']) && !empty($aCommande["abonnement"]['dateFinValidite'])){
									$date = DateTime::createFromFormat('dmY', $aCommande["abonnement"]['dateFinValidite']);
									$strDateAbo = strtotime($date->format('Y-m-d'));
									$datetime = $date->format('Y-m-d');
								}else{
									$strDate = $aCommande['date'];
									$strDateAbo = strtotime($strDate);
									$datetime = $strDate;
								}
								
								$strDiff  = $strDateAbo - $strDateNow;
								$iDiffJour = round($strDiff/86400)  ;
								if($iDiffJour < 0 )
									$bAboEnCours = false;
								$oDate = new DateTime( $datetime );
								$strDateAbo =  $oDate->format("d/m/Y");
								if(!$bAboEnCours){
									$commandesTerminees = 1;
								}
							}
						}
						if($commandesTerminees == 1){
						?>
							<h2 class="">Abonnement(s) terminé(s) :</h2></div>
						<?php
						}
						//On va parcourir chaque abonnement pour récupérer l'ensemble des infos à afficher
						foreach($aCommandes as $key=>$aCommande){

							$strDateAbo ="";
							//Récupération de la liste des enfants rattaché à cette commande 
							$strSql = "SELECT * FROM bor_commande_enfant ce INNER JOIN bor_classe c ON (c.classe_id = ce.classe_id) LEFT JOIN bor_matiere m ON (m.matiere_id = ce.matiere_id) WHERE ce.commande_id = '".mysql_real_escape_string($aCommande['commande_id'])."' ORDER BY ce.enfant_id DESC";
							$aEnfants = $oDb->queryTab($strSql); 
							
							// calcul par rapport à la date courante
							
							if(isset($aCommande['commande_id']) && !empty($aCommande['commande_id'])){
								/*$bAboEnCours = true;
								$strDate = $aCommande['date'];
								$strDateAbo = strtotime($strDate);
								$strDiff  = $strDateAbo - $strDateNow;
								$iDiffJour = round($strDiff/86400)  ;
								if($iDiffJour < 0 )
								$bAboEnCours = false;
								$oDate = new DateTime( $strDate );
								$strDateAbo =  $oDate->format("d/m/Y");*/
								
								$bAboEnCours = true;
								$strDate = $aCommande['date'];
								$strDateAbo = strtotime($strDate);
								
								// Modif 08 03 2016 : bug sur date format string
								//$strDate = $aCommande['date'];
								//$strDateAbo = strtotime($strDate);
								if(isset($aCommande["abonnement"]['dateFinValidite']) && !empty($aCommande["abonnement"]['dateFinValidite'])){
									$date = DateTime::createFromFormat('dmY', $aCommande["abonnement"]['dateFinValidite']);
									$strDateAbo = strtotime($date->format('Y-m-d'));
									$datetime = $date->format('Y-m-d');
								}else{
									$strDate = $aCommande['date'];
									$strDateAbo = strtotime($strDate);
									$datetime = $strDate;
								}
								
								$strDiff  = $strDateAbo - $strDateNow;
								$iDiffJour = round($strDiff/86400)  ;
								if($iDiffJour < 0 )
									$bAboEnCours = false;
								$oDate = new DateTime( $datetime );
								$strDateAbo =  $oDate->format("d/m/Y");

								if(!$bAboEnCours){
									?>
									<div id="abo-en-cours" class="abo-en-cours clearfix">
										<div class="col-sm-2 col-md-2" style="min-height:200px;">
											<?php
											if($aCommande["type_offre"] == "classique"){
												?>
												<img src="<?php echo $aCommande['files_path']; ?>" class="img-responsive">
												<?php
											}else{
												?>
												<img src="/uploads/formules/454.png" class="img-responsive">
												<?php
											}
											?>
										</div>
										<div class="col-sm-10 col-md-7">
										  <?php
											if($aCommande["type_offre"] == "classique"){
												echo '<h3 class="h3">'.$aCommande['formule_libelle'].' <span>valable jusqu\'au '.$strDateAbo.'</span></h3>';
												if($aCommande['formule_classe'] == "tribu")
													if(count($aEnfants) > 1)
														echo '<p class="nombre-enfant"><strong>'.count($aEnfants).' enfants</strong></p>';
											}
											else{
												echo '<h3 class="h3">Formule réussite <span>- Offre Découverte</span></h3>';
											}
										  ?>
										  <ul class="abonnement-recap">
											<?php
												/*foreach($aCommande['enfants'] as $key => $aEnfant){
													if($aCommande['formule_classe'] == "tribu")
														echo '<li class="enfant-recap"> '.$aEnfant['name'].' révise en '.(!empty($aEnfant['matiere_id'])  ? $aEnfant['matiere_titre'].' - ' : '') .$aEnfant['classe'].'</li>';
													else
														if($key == 0 )
															echo '<li class="enfant-recap"> '.$aEnfant['name'].' révise en '.(!empty($aEnfant['matiere_id'])  ? $aEnfant['matiere_titre'].' - ' : '') .$aEnfant['classe'].'</li>';
												}*/
												
												foreach($aEnfants as $aEnfant){
												$strSql = "SELECT classe_name FROM bor_classe c WHERE c.classe_id = '".$aEnfant['classe_id']."'";
												$iClasseEnfant = $oDb->queryItem($strSql);
												if($aEnfant['enfant_prenom'] == "")
													$aEnfant['enfant_prenom'] = "enfant";
												echo '<li class="enfant-recap"> '.$aEnfant['enfant_prenom'].' révise en '.(!empty($aEnfant['matiere_id'])  ? $aEnfant['matiere_titre'].' - ' : '') .(!empty($iClasseEnfant)  ? $iClasseEnfant : $aEnfant['classe']).'</li>';
											}
											?>
										  </ul>
										  <p><a href="/comment-s-abonner.html" class="btn btn-primary "> <i class="icon-arrows-cw"></i> Je me réabonne </a> </p>
										</div>
									</div>
									<hr/>
									<?php
								}
							}
						}

/************************************************************************************************/
/*										FIN DES ABONNEMENTS										*/
/************************************************************************************************/

					}else{ //Aucun abonnement pour le client
						?>
						<p class="gras_txt"><span class="color_cible"> Vous n’avez pas d’abonnement en cours.</span></p>
						<?php
					}
		?>
	</div>
  </div>
</div>
<?php

/************************************************************************************************/
/*					FIN DE LA CONDITION D'IDENTIFICATION DE L'INTERNAUTE						*/
/************************************************************************************************/

}

/************************************************************************************************/
/*								BLOC POPUP NUMÉRO DE CONTRÔLE CB								*/
/************************************************************************************************/

?>

<script>
jQuery(document).ready(function(){
	function update_matiere(id) {
		jQuery.ajax({type:"POST", data:{classe : jQuery('#'+id.id).val()}, url: "/templates/ajaxsearch_abo.php" ,dataType: 'json',	success: function(json) {
				jQuery('#matiere-'+id.id+' option').remove(); 
				jQuery('#matiere-'+id.id).append('<option value="">Choisir une matière</option>');
				jQuery.each(json, function(index, value) { 
					jQuery('#matiere-'+id.id).append('<option value="'+ json[index]['matiere_id'] +'">'+ json[index]['mc_titre'] +'</option>');
				});
			},
			error: function(){
				jQuery('#matiere-'+id.id).html('');
			}
		});
		return false;
	}

	// au chargement, si classe renseignée
	jQuery('.classe').each(function(){
		update_matiere(this);
	});

	// au changement de classe: changement des matières
	jQuery('.classe').change(function () {
		update_matiere(this);
	});
	
	// au chargement
	jQuery('.enfant_tribu').each(function () {
		change_enfant(this);
	});
	
	// au changement d'enfant dans une formule
	jQuery('.enfant_tribu').change(function () {
		change_enfant(this);
	});
});

function afficherBloc(idCommande){
	$(".resiliation").hide();
	$(".bloc_delete_"+idCommande).show();
	$("#btn_"+idCommande).html('Mettre à jour ma CB »');
	$("#message_cb_"+idCommande).html('Je renseigne les coordonnées de ma nouvelle CB.');
	$("#modif_titre_"+idCommande).html('Modifier ma CB');
	$("#action_"+idCommande).val('updatecb');
	if($('#div_cb_'+idCommande).css('display') == 'none'){
		$('#div_cb_'+idCommande).show(); 
	}else{
		$('#div_cb_'+idCommande).hide(); 
	}
}
function afficherBlocResilier(idCommande){
	$(".modif_cb").hide();		
	if($('#div_resilier_'+idCommande).css('display') == 'none'){
		$('#div_resilier_'+idCommande).show(); 
	}else{
		$('#div_resilier_'+idCommande).hide(); 
	}
}

function supprimerCarte(idCommande){
	$(".resiliation").hide();
	$(".bloc_delete_"+idCommande).hide();
	$("#btn_"+idCommande).html('Supprimer ma CB »');
	$("#message_cb_"+idCommande).html('En supprimant ma CB, je mets fin à mon abonnement Bordas Soutien Scolaire à la fin de ma période d\'engagement.');
	$("#modif_titre_"+idCommande).html('Supprimer ma CB');
	$("#action_"+idCommande).val('deletecb');

}
function change_enfant(bloc_enfant){
	res = $(bloc_enfant).val().split("-",1);
	$('#modifier-abo-'+res+' .bloc-enfant').each(function(){
		$(this).hide();
	});
	$("#bloc-enfant-"+$(bloc_enfant).val()).show();
}
</script>