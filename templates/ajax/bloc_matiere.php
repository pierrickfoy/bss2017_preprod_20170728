<?php 
// var_dump($_POST):
	define( 'ROOT', dirname( dirname( __FILE__ ) )."/..");
	$_CONST['path']["server"] = ROOT;
	
	include_once(ROOT."/lib/config.inc.php");
	include_once(ROOT."/lib/database.class.php");
	
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	$oDb->query("SET NAMES utf8"); 
	
	
	foreach($_POST['classe'] as $iIndex => $aVal){
		$iBloc = $iIndex; 
	}
	
	echo "jQuery('#error_$iBloc').html('');";
	echo "jQuery('#success_$iBloc').html('');";
	
	if(isset( $_POST['classe'][$iBloc] ) &&  $_POST['classe'][$iBloc] > 0 ){
		//Récupération du titre de la classe 
		$strSql = "SELECT classe_name FROM bor_classe WHERE classe_id = '".mysql_real_escape_string($_POST['classe'][$iBloc])."' "; 
		$strClasse = $oDb->queryItem($strSql); 
		//Récupération de la liste des date de disponibilité
		$strSql = "SELECT d.* FROM bor_datedispo d INNER JOIN bor_matiere_dispo md ON (md.datedispo_id = d.datedispo_id) WHERE  md.classe_id = '".mysql_real_escape_string($_POST['classe'][$iBloc])."' GROUP BY d.datedispo_id ORDER BY datedispo_position";
		$aDispos = $oDb->queryTab($strSql); 
		$strHtml = "";
		echo "jQuery('#modal_matiere_titre_$iBloc').html(\"".$strClasse."\");"; 	
		echo "jQuery('#modal_classe_$iBloc').val(\"".$_POST['classe'][$iBloc]."\");"; 	
		if($oDb->rows > 0 ){
			
			foreach($aDispos as $aDispo){
				$strHtml .= "<h5>".$aDispo['datedispo_date']." :</h5>";
				$strHtml .= "<ul>";
				$strSql = "SELECT * FROM bor_matiere_dispo md INNER JOIN bor_matiere m ON (m.matiere_id = md.matiere_id) WHERE md.datedispo_id = '".$aDispo['datedispo_id']."' AND md.classe_id = '".mysql_real_escape_string($_POST['classe'][$iBloc])."'";
				$aMatieres = $oDb->queryTab($strSql); 
				foreach($aMatieres as $aMatiere){
					$strHtml .= "<li> ".$aMatiere['matiere_titre']."</li>";
				}
				$strHtml .= "</ul>";
			}
			// echo $strHtml;
			echo "jQuery('#modal_matiere_body_$iBloc').show();";
			echo "jQuery('#body_matiere_$iBloc').html(\"".$strHtml."\");";
			
		}else{
			//Toutes les matières sont dispo
			echo "jQuery('#modal_matiere_body_$iBloc').hide();";
			echo "jQuery('#body_matiere_$iBloc').html(\"<p>Toutes les matières sont déjà disponible pour cette classe.</p>\");";
		}
	}else{
		//Afficher merci de saisir une classe
		echo "jQuery('#modal_matiere_body_$iBloc').hide();";
		echo "jQuery('#modal_matiere_titre_$iBloc').html(\"\");"; 	
		echo "jQuery('#body_matiere_$iBloc').html(\"<p>Veuillez sélectionner la classe de votre enfant.</p>\");";
	}
	
?>
