<?php

// V�rification de la page en https
/*
if (preg_match('/^http:\/\//', $_SERVER['SCRIPT_URI'])) {
    header('location: https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    exit();
}*/

// debug($_SESSION['delivery'], 'session delivery');
//Ecomiz 20131104 : Mise � jour de l'adresse de livraison et facturation en fonction des infos mises en base
$query = '	SELECT c.lieufactchoice , c.rue1fact as client_rue1 , c.rue1factbis as client_rue1bis, c.rue2fact as client_rue2, c.rue3fact as client_rue3, c.cpfact as client_codepostal, c.villefact as client_ville, c.idpaysfact as client_idpays,
					CONCAT_WS( " ", e.nom , e.nom_complement) as etab_lieu, e.rue as etab_rue2, e.rue_complement as etab_rue3, e.cp as etab_codepostal, e.ville as etab_ville, p.idpays as etab_idpays
			FROM `clients` AS `c`
				LEFT JOIN  `etablissement` e ON c.id_etab = e.id_etab
				LEFT JOIN `pays` p ON p.id_pays_vpc = e.id_pays_vpc
			WHERE `c`.`id`=\'' . mysql_real_escape_string( (int)$_SESSION['id'] ) . '\'
		';
$result = mysql_query( $query ) or die (mysql_error());
if( $result ):
	$aAdresse = mysql_fetch_assoc( $result );
	mysql_free_result( $result );	
endif;
if($aAdresse['lieufactchoice']==1){//Adresse �tablissement
	$_SESSION['delivery']['lieu'] = $aAdresse['etab_lieu'];
    $_SESSION['delivery']['rue1'] = '';
    $_SESSION['delivery']['rue1bis'] = '';
    $_SESSION['delivery']['rue2'] = $aAdresse['etab_rue2'];
    $_SESSION['delivery']['rue3'] = $aAdresse['etab_rue3'];		
    $_SESSION['delivery']['codepostal'] = $aAdresse['etab_codepostal'];
    $_SESSION['delivery']['ville'] = $aAdresse['etab_ville'];
    $_SESSION['delivery']['idpays'] = $aAdresse['etab_idpays'];
	//Ecomiz 20131114 : mise � jour choix_facturation 
	$_SESSION['delivery']['choix_facturation'] = 1;
}else{//Adresse client
	$_SESSION['delivery']['lieu'] = '';
    $_SESSION['delivery']['rue1'] = $aAdresse['client_rue1'];
    $_SESSION['delivery']['rue1bis'] = $aAdresse['client_rue1bis'];
    $_SESSION['delivery']['rue2'] = $aAdresse['client_rue2'];
    $_SESSION['delivery']['rue3'] = $aAdresse['client_rue3'];
    $_SESSION['delivery']['codepostal'] = $aAdresse['client_codepostal'];
    $_SESSION['delivery']['ville'] = $aAdresse['client_ville'];
    $_SESSION['delivery']['idpays'] = $aAdresse['client_idpays'];
	//Ecomiz 20131114 : mise � jour choix_facturation
	$_SESSION['delivery']['choix_facturation'] = 2 ;
}
//Fin Ecomiz 20131104

include_once('class/promocodes.class.php');

// debug($_SESSION['delivery']);

function strim_accents ($chaine)
{
	return htmlentities(strtr( $chaine, '���������������������������������������������������������������������', 'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy' ));
}


//Ecomiz 20140109 : Suite � la hausse TVA : Frais de port => 5.50 
// $portFrance = 4.4;
$portFrance = 5.5;
$portOthers = 12;
//Ecomiz 20140109 : Suite � la hausse TVA : Franco de port => 40
// $sumPortFree = 45;
$sumPortFree = 40;
$tauxReduction = 0.05;
$applyPort = false;
$phrDevisPort="les frais de ports feront l'objet d'un devis � r�ception de la commande";

if( !( !empty( $_SESSION ) and isset( $_SESSION['id'] ) ) ):
	header( 'Location: login.html' );
	exit;
endif;

if( !isset( $_SESSION['payment'] ) or $_SESSION['payment'] === null ):
	header( 'Location: order-payment.html' );
	exit;	
endif;

// 11/2011 - Refonte graphique - Reconstitution du code de la carte
if (!empty($_POST['moiscb']) && !empty($_POST['anneecb'])) {
    $_POST['datecarte'] = $_POST['moiscb'].'/'.substr($_POST['anneecb'], 2, 2);
} else {
    if (isset($_POST['datecarte'])) {
        unset($_POST['datecarte']);
    }
}
if (!empty($_POST['mp'])) {
    $_SESSION['payment'] = $_POST['mp'];
} else {
    $_SESSION['payment'] = 'CB';
}

$affichetva=true;
$isportFrance = true;
if ($_SESSION['id']>0) {
	if (isset( $_SESSION['delivery']['idpays'] ) and $_SESSION['delivery']['idpays'] != '76' ) {
		$isportFrance=false;
	}
	$cl=@mysql_fetch_assoc(mysql_query("select idpaysfact from clients where `id`='".$_SESSION['id']."'"));
	if ( ($cl) and ($cl['idpaysfact']!=76)) $affichetva=false;	
}

//$tva = 0.196;

//Ecomiz ZHZH 20111231 : changement de la tva 5,5 -> 7%
/*$tvaTypes = array(
	'19.6' => '19.6',
	'5.5' => '5.5'
);*/
$tvaTypes = array(
	'19.6' => '19.6',
	'5.5' => '5.5',
    '7' => '7'
);

$cbTypes = array(
	'Carte Bleue' => 'Carte Bleue',
	'Carte Visa' => 'Carte Visa',
	'EuroCard' => 'EuroCard',
	'MasterCard' => 'MasterCard',	
);

$civilites = array (
	'1' => 'Monsieur',
	'2' => 'Madame',
	'3' => 'Mademoiselle',
);

$civilitesXml = array (
	'1' => 'M.',
	'2' => 'MME',
	'3' => 'MLLE',
);

$numadresses = array (
	'1' => 'BIS',
	'2' => 'TER',
);

$imageDirectory = dirname( __FILE__ ) . '/images/catalogue';
$imageHttp = 'images/catalogue';

$messages = array();

// Traitements
if( !empty( $_POST ) and isset( $_POST['register'] ) ):
	$validForm = true;
	
	if( !isset( $_POST['confirm'] ) or $_POST['confirm'] === null ):	
		$validForm = false;
		$messages[] = gettext( 'Vous devez accepter les conditions g�n�rales de ventes afin de confirmer votre commande.' );
	endif;
	
	if ( $_SESSION['payment'] == 'CB' ) {
		// contr�les des param�tre de la CB
		if( !in_array( $_POST['typecarte'], array_keys( $cbTypes ) )  ) {
			$validForm = false;
			$messages[] = 'Le type de votre carte est incomplet.';
		}
		
		if ( empty( $_POST['numcarte'] ) or strlen( $_POST['numcarte'] ) > 16 or strlen( $_POST['numcarte'] ) < 13  or ! is_numeric ( $_POST['numcarte'] ) ) {
			$validForm = false;
			$messages[] = 'Le num�ro de votre carte est incorrect. Il doit comporter entre 13 et 16 chiffres.';
		}
		else {
			// v�rification de la cl� de L�hn
            if ($_CONST['TYPE_ENVIRONNEMENT'] != 'dev') {
                if ((int)substr($_POST['numcarte'],strlen($_POST['numcarte']) - 1 , 1 ) != (int)cle_luhn($_POST['numcarte'])) {
                    $validForm = false;
                    $messages[] = 'Le num�ro de votre carte est invalide.';	
                }
            }
		}
		if ( empty( $_POST['datecarte'] ) or strlen( $_POST['datecarte'] ) != '5' or substr( $_POST['datecarte'],2,1 ) != '/' or ! is_numeric ( substr( $_POST['datecarte'],0,2 ) ) or (int)substr( $_POST['datecarte'],0,2 ) <= 0 or (int)substr( $_POST['datecarte'],0,2 ) > 12 or ! is_numeric ( substr( $_POST['datecarte'],3,2 ) )  ) {
			$validForm = false;
			$messages[] = 'La date de validit� votre carte est incorrecte. Elle doit �tre au format "MM/AA".';	
		} 
		else {
			// contr�le de la date
			if ( (int)date("y") > (int)substr( $_POST['datecarte'],3,2 ) or ( (int)date("y") == (int)substr( $_POST['datecarte'],3,2 ) and (int)date("n") > (int)substr( $_POST['datecarte'],0,2 ) ) ) {
				$validForm = false;
				$messages[] = 'La date de validit� votre carte est d�pass�e.';				
			}
		}
		
		if ( empty( $_POST['codecarte'] ) or strlen( $_POST['codecarte'] ) != '3' or ! is_numeric ( $_POST['codecarte'] ) ) {
			$validForm = false;
			$messages[] = 'Le code de contr�le votre carte est incorrect. Il doit comporter 3 chiffres.';		
		}
	}
    
	if( $validForm === true ):	
		// Enregistrement de la commande
	
		// Cr�ation de la commande
	
		// Sum
		$sumBasket = 0;
		$sumBasketTTC = 0;
		$sumWeight = 0;
		$sumTva = array(
			'19.6' => '0',
			'5.5' => '0',
            '7' => '0'
		);
		$sumPort = 0;
		$sumPortHT = 0;
		$sumReduction = 0;
		$sumTotal = 0;
        $nbarticles=0;
			
		$currentBasketProducts = array();
		if( isset( $_CADDIE ) and is_a( $_CADDIE, 'caddie' ) ):
			$keys = $_CADDIE->keysProduct();
			if( !empty( $keys ) ):				
				// Produits
				$productsInit = array();
				$query = 'SELECT `p`.`idproduit` as `id`, `p`.`id_collection`, `p`.`titre`, `p`.`soustitre`, `p`.`ean13`, `p`.`isbn`,`p`.`prix_euro_ttc` as `price`, `p`.`support`  
								FROM `produits` as `p` 
								WHERE `p`.`idproduit` IN ( ' . implode( ', ', $keys ) . ' )  ORDER BY `p`.`titre` ASC
						';					
					
				$result = mysql_query( $query );
				if( $result ):
					while( $rs = mysql_fetch_assoc( $result ) ):
						$currentInfoBasketProduct = $_CADDIE->getProduct( $rs['id'] );									
						// prix						
						$sumBasketTTC += $rs['price'] * $currentInfoBasketProduct->quantity;
						//sk - 2008-03-03 - TVA - 19.6% pour les documents num�riques et 5.5% pour les livres classiques
						if (in_array($rs['support'], $numericDocuments ) ) {
							// $sumTva['19.6'] +=  $currentInfoBasketProduct->quantity * ( $rs['price']  - ( $rs['price']  / 1.196 ) ) ;
                            // Ecomiz GMV 20120111 : Changement de la TVA : 19,6 -> 7%
                            $sumTva['7'] +=  $currentInfoBasketProduct->quantity * ( $rs['price']  - ( $rs['price']  / 1.07 ) ) ;
						}
						else {
							// Remise de 5% uniquement pour les "livres"
							$sumReduction += round($currentInfoBasketProduct->quantity * $rs['price'] * $tauxReduction,2);						
							$applyPort = true;
							$sumTva['5.5'] +=  $currentInfoBasketProduct->quantity * ( $rs['price']  - ( $rs['price']  / 1.055 ) ) ;
							$nbarticles+=$currentInfoBasketProduct->quantity;				
						}						
						$productsInit[] = $rs;				
					endwhile;
					mysql_free_result( $result );
				endif;
				unset( $result, $rs, $query, $currentInfoBasketProduct );
				
				// Gestionnaire de produits
				$products = new products( $productsInit );
				unset( $productsInit );
				$currentBasketProducts = $products->getAll();
			endif;
							
			$portSurDevis=false;
			// Remise de 5%
			//$sumReduction = $sumBasketTTC * $tauxReduction;
			$sumTva['5.5'] = round( $sumTva['5.5'] - ($sumTva['5.5'] * $tauxReduction),2);
            $sumTva['19.6'] = round( $sumTva['19.6'], 2);
            $sumTva['7'] = round( $sumTva['7'], 2);
			//$sumTva['19.6'] = $sumTva['19.6'] - ($sumTva['19.6'] * $tauxReduction);
			// sk - 2008-01-11 suppression des FdP gratuits
			// sk - 2008-09-19 - Suite � la demande du client, r�application du franco de port.	
			// sk - 2008-03-03 - Ne pas appliquer les fdp pour une commande avec uniquement des produits num�riques
            // jp - 2005-05-07 - Suite � la demande du client, application du franco de port en France uniquement, ajout port sur devis si + 10 prod � l'etranger.
			if ( $applyPort === true ) {			
				if( $isportFrance ) {
					$sumPort = $portFrance;
                } else {
                    if ($nbarticles > 10) {
                        $portSurDevis=true;
                    } else {
						$sumPort = $portOthers;
					}
				}
            }
            
            // Cas d'un panier � + de 45 euro
            /*
            if (($sumPort == $portFrance) && ($sumBasketTTC >= $sumPortFree)) {
                $sumPort = 0;
            }
            */
            
            // Cas d'un code de r�duction qui annule les frais de port (coupon_type = 1)
            if (!empty($_CADDIE->codepromoType) && ($_CADDIE->codepromoType == 1)) {
                $sumPort = 0;
            }
            
            // Gestion des codes promo entrainant une remise
            if (!empty($_CADDIE->codepromoType) && ($_CADDIE->codepromoType == 2) && !empty($_CADDIE->codepromoRemise)) {
                $sumTotal -= $_CADDIE->codepromoRemise;
            }
            if (!empty($_CADDIE->codepromoType) && ($_CADDIE->codepromoType == 3) && !empty($_CADDIE->codepromoRemise)) {
                $sumTotal -= $_CADDIE->codepromoRemise;
            }
            
            if (!empty($_CADDIE->deductionRemise)) {
                $sumReduction = round($sumReduction - round($_CADDIE->deductionRemise, 2), 2);
            }
            
			// ne pas calculer les frais de port dans la TVA 19.6%			
			//$sumTva['5.5'] = $sumBasketTTC -  ( $sumBasketTTC / 1.055 );
			//$sumTva['19.6'] = $sumPort -  ( $sumPort / 1.196 );
			// Total TTC
            
            // Frais de port gratuits pour une commande >= 45 ?, apr�s d�duction de la remise et du code promo
            if (($sumPort == $portFrance) && (($sumBasketTTC - $sumReduction - $_CADDIE->codepromoRemise) >= $sumPortFree)) {
                $sumPort = 0;
            }
            
			$sumTotal = $sumBasketTTC - $sumReduction + $sumPort;
            
            if (!empty($_CADDIE->codepromoRemise)) {
                $sumTotal -= $_CADDIE->codepromoRemise;
            }
		endif;
		
		// Client
		$client = null;
		$query = '
			SELECT 
				`c`.`id`, `c`.`numcli`,`c`.`civilite`,`c`.`nom`, `c`.`prenom`,`c`.`email`, `c`.`tel`, `c`.`fax`, `c`.`lieufactchoice`, `c`.`lieufact`,  `c`.`rue1fact`,`c`.`rue1factbis`, `c`.`rue2fact`,`c`.`rue3fact`, `c`.`cpfact`, `c`.`villefact`, `c`.`idpaysfact`,
				`p`.`pays_fr` AS `pays` 
			FROM `clients` AS `c`
				LEFT OUTER JOIN `pays` AS `p` ON `p`.`idpays`=`c`.`idpaysfact`
			WHERE `c`.`id`=\'' . mysql_real_escape_string( (int)$_SESSION['id'] ) . '\'
		';
		$result = mysql_query( $query ) or die (mysql_error());
		if( $result ):
			$client = mysql_fetch_assoc( $result );
			mysql_free_result( $result );	
		endif;
		
		$orderDate = strtotime("now");
		
		// Adresse de livraison
		$query = 'SELECT `pays_fr`,`code_retz` FROM `pays` WHERE `idpays`=\'' . mysql_real_escape_string( $_SESSION['delivery']['idpays'] ) . '\'';
		$result = mysql_query( $query ) or die (mysql_error());
		$_SESSION['delivery']['pays'] = '';
		$_SESSION['delivery']['coderetz'] = '';
		if( $result ):
			$deliveryCountry = mysql_fetch_assoc( $result );
			mysql_free_result( $result );
			unset( $result );
			$_SESSION['delivery']['pays'] = $deliveryCountry['pays_fr'];
			$_SESSION['delivery']['coderetz'] = $deliveryCountry['code_retz'];
		endif;		

        // Informations du num�ro de commande
        mysql_query("lock tables `nums` write");
        $result = mysql_query( 'SELECT `prefixe`, `mini`, `nbcar` FROM `nums` WHERE `id`=\'commande\'' );
        $infosNumCommande = mysql_fetch_assoc( $result );
        $resultMax = mysql_query( 'SELECT MAX( `nocommande` ) AS `nocommande` FROM `commandes` WHERE `prefixe` = \'' . mysql_real_escape_string( $infosNumCommande['prefixe'] ) .'\'' );
		if(@mysql_num_rows($resultMax) > 0 )
			$lastNumCmd = @mysql_fetch_assoc( $resultMax );
        if ( @mysql_num_rows( $resultMax ) == 1 ) {
            if ( $infosNumCommande['mini'] > $lastNumCmd['nocommande'] + 1 ) {
                $numAct = $infosNumCommande['mini'];
            } else {
                $numAct = $lastNumCmd['nocommande'] + 1;
            }
        } else {
            $numAct = $infosNumCommande['mini'];
        }
        mysql_query("update `nums` set `mini`='".mysql_real_escape_string($numAct+1)."'");
        mysql_query("unlock tables");
        $numCommande = $infosNumCommande['prefixe'] . str_repeat( '0', $infosNumCommande['nbcar'] - strlen( $numAct ) - strlen( $infosNumCommande['prefixe'] ) ) . $numAct;

		// sk - 2009-05-11 - Frais de port : devis � la r�ception pour les commandes depuis l'�tranger (nb d'articles > 10)
		$portdevis = '0';
		if ( $sumPort == 0 and $portSurDevis === true ) {
			$portdevis = '1';
		}
		
        /*
        debug($_CADDIE, '$_CADDIE');
        debug($sumBasketTTC, '$sumBasketTTC');
        debug($sumReduction, '$sumReduction');
        debug($sumPort, '$sumPort');
        debug($sumTotal, '$sumTotal');
        die;
        */
		
		// SK - 2008-02-11 :  ancienne m�thode cr�ation de la commande + envoi email de confirmation avant l'envoi du flux � BILL
		
		/*
		
		// Commande
		$orderDate = strtotime("now");
		$query = '
			INSERT INTO `commandes`
				( `prefixe`, `nocommande`, `nbcar`, `libnocommande`, `datecommande`, `idclient`, `ht`,`tva196`,`tva55`, `ttc`, `remisettc`, `port`,`typepaiement`, `reglement`, `archive` )
			VALUES 
				( \'' . mysql_real_escape_string( $infosNumCommande['prefixe'] ) .'\', \'' . mysql_real_escape_string( $numAct ) .'\', \'' . mysql_real_escape_string( $infosNumCommande['nbcar'] ) .'\', \'' . mysql_real_escape_string( $numCommande ) .'\',  \'' . date('Y-m-d H:i:s', $orderDate  ) . '\' , \'' . mysql_real_escape_string( (int)$_SESSION['id'] ) . '\' , \'' . mysql_real_escape_string( sprintf( '%01.2F', $sumTotal - $sumTva['19.6']  ) ) . '\', \'' . mysql_real_escape_string( sprintf( '%01.2F', $sumTva['19.6'] ) ) . '\' , \'' . mysql_real_escape_string( sprintf( '%01.2F', $sumTva['5.5'] ) ) . '\',   \'' . mysql_real_escape_string( sprintf( '%01.2F', $sumTotal ) ) . '\' , \'' . mysql_real_escape_string( sprintf( '%01.2F', $sumReduction ) ) . '\' , \'' . mysql_real_escape_string( sprintf( '%01.2F', $sumPort ) ) . '\' , \'' . mysql_real_escape_string( $_SESSION['payment'] ) . '\', \'0\', \'0\' )
		';
		mysql_query( $query );
		$idCommande = mysql_insert_id();
		
			
		// Produits commandes
		if( !empty( $currentBasketProducts ) ):
			foreach( $currentBasketProducts as $basketProduct ):
				$currentInfoBasketProduct = $_CADDIE->getProduct( $basketProduct->id );
										
				$query = 'INSERT INTO `commande_produits` ( `idcommande`, `idproduit`, `name`, `tva`, `ean13`, `isbn`, `qt`, `ht`, `ttc` ) VALUES ( \'' . mysql_real_escape_string( $idCommande ) . '\', \'' . mysql_real_escape_string(  $basketProduct->id ) . '\',  \'' . mysql_real_escape_string( $basketProduct->titre ) . '\'  , \'5.5\' ,  \'' . mysql_real_escape_string( $basketProduct->ean13 ) . '\',  \'' . mysql_real_escape_string( $basketProduct->isbn ) . '\', \'' . mysql_real_escape_string( $currentInfoBasketProduct->quantity ) . '\', \'' . mysql_real_escape_string( sprintf( '%01.2F', ( $basketProduct->price * $currentInfoBasketProduct->quantity ) / 1.196 ) ) . '\', \'' . mysql_real_escape_string( sprintf( '%01.2F', ( $basketProduct->price * $currentInfoBasketProduct->quantity ) ) ) . '\' )';
				mysql_query( $query );
				unset( $query );	
			endforeach;
			
			unset( $basketProduct, $currentInfoBasketProduct );
		endif;		
		
		// Adresse de facturation
		$query = 'INSERT INTO `commande_facturation` ( `idcommande`, `civilite`, `numcli`, `nom`, `prenom`, `lieuchoice`, `lieu`, `rue1`, `rue1bis`, `rue2`, `rue3`, `codepostal`, `ville`, `idpays` ) VALUES ( \'' . mysql_real_escape_string( $idCommande ) . '\', \'' . mysql_real_escape_string( $client['civilite'] ) . '\' , \'' . mysql_real_escape_string( $client['numcli'] ) . '\' , \'' . mysql_real_escape_string( $client['nom'] ) . '\', \'' . mysql_real_escape_string( $client['prenom'] ) . '\', \'' . mysql_real_escape_string( $client['lieufactchoice'] ) . '\', \'' . mysql_real_escape_string( $client['lieufact'] ) . '\' ,  \'' . mysql_real_escape_string( $client['rue1fact'] ) . '\',  \'' . mysql_real_escape_string( $client['rue1factbis'] ) . '\' , \'' . mysql_real_escape_string( $client['rue2fact'] ) . '\', \'' . mysql_real_escape_string( $client['rue3fact'] ) . '\' , \'' . mysql_real_escape_string( $client['cpfact'] ) . '\', \'' . mysql_real_escape_string( $client['villefact'] ) . '\', \'' . mysql_real_escape_string( $client['idpaysfact'] ) . '\' )';
		mysql_query( $query );
		$idFacturation = mysql_insert_id();	
		
		// Adresse de facturation 
		$adresseFacturation = $civilites [$client['civilite'] ] . ' ' . $client['prenom'] . ' ' . $client['nom'] . chr( 10 );
		if( !empty( $client['numcli'] ) ):
			$adresseFacturation .= '(N� ' . $client['numcli'] . ')' . chr( 10 );
		endif;
		if( !empty( $client['lieufact'] ) ):
			$adresseFacturation .=  $client['lieufact'] . chr( 10 );
		endif;	
		if( !empty( $client['rue1fact'] ) ):
			$adresseFacturation .= $client['rue1fact'] ;
			if( !empty( $client['rue1factbis'] ) ):
				$adresseFacturation .= ' ' . $numadresses[$client['rue1factbis']] ;
			endif;
		$adresseFacturation .= chr( 10 );
		endif;	
		$adresseFacturation .= trim( $client['rue2fact'] . chr( 10 ) . $client['rue3fact'] ) . chr( 10 );
	
		$adresseFacturation .= trim( $client['cpfact'] . ' ' . $client['villefact'] ) . chr( 10 );
		$adresseFacturation .= $client['pays'];
		
		// Adresse de livraison
		$query = 'SELECT `pays_fr`,`code_retz` FROM `pays` WHERE `idpays`=\'' . mysql_real_escape_string( $_SESSION['delivery']['idpays'] ) . '\'';
		$result = mysql_query( $query ) or die (mysql_error());
		$_SESSION['delivery']['pays'] = '';
		$_SESSION['delivery']['coderetz'] = '';
		if( $result ):
			$deliveryCountry = mysql_fetch_assoc( $result );
			mysql_free_result( $result );
			unset( $result );
			$_SESSION['delivery']['pays'] = $deliveryCountry['pays_fr'];
			$_SESSION['delivery']['coderetz'] = $deliveryCountry['code_retz'];
		endif;
		
		$query = 'INSERT INTO `commande_livraison` ( `idcommande`, `civilite`, `numcli`, `nom`, `prenom`, `lieuchoice`, `lieu`, `rue1`, `rue1bis`, `rue2`, `rue3`, `codepostal`, `ville`, `idpays` ) VALUES ( \'' . mysql_real_escape_string( $idCommande ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['civilite'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['numcli'] ) . '\' , \'' . mysql_real_escape_string( $_SESSION['delivery']['nom'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['prenom'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['lieuchoice'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['lieu'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['rue1'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['rue1bis'] ) . '\' , \'' . mysql_real_escape_string( $_SESSION['delivery']['rue2'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['rue3'] ) . '\'  , \'' . mysql_real_escape_string( $_SESSION['delivery']['codepostal'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['ville'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['idpays'] ) . '\' )';
		mysql_query( $query );
		$idLivraison = mysql_insert_id();
		
		$adresseLivraison =  $civilites [ $_SESSION['delivery']['civilite'] ] . ' ' . $_SESSION['delivery']['prenom'] . ' ' .  $_SESSION['delivery']['nom'] . chr( 10 );
		if( !empty( $_SESSION['delivery']['numcli'] ) ):
			$adresseLivraison .= '(N� ' . $_SESSION['delivery']['numcli'] . ')' . chr( 10 );
		endif;
		if( !empty( $_SESSION['delivery']['lieu'] ) ):
			$adresseLivraison .= $_SESSION['delivery']['lieu'] . chr( 10 );
		endif;	
		if( !empty( $_SESSION['delivery']['rue1'] ) ):
			$adresseLivraison .= $_SESSION['delivery']['rue1'] ;
			if( !empty( $_SESSION['delivery']['rue1bis'] ) ):
				$adresseLivraison .= ' ' . $numadresses [$_SESSION['delivery']['rue1bis']] ;
			endif;
		$adresseLivraison .= chr( 10 );
		endif;		
		$adresseLivraison .= trim( $_SESSION['delivery']['rue2'] . chr( 10 ) .  $_SESSION['delivery']['rue3'] ) . chr( 10 );
		$adresseLivraison .= trim( $_SESSION['delivery']['codepostal'] . ' ' .  $_SESSION['delivery']['ville'] ) . chr( 10 );
		$adresseLivraison .=  $_SESSION['delivery']['pays'];
		
		// Mise a jour des idFacturation et idLivraison de la commande
		$query = 'UPDATE `commandes` SET `idfacturation`=\'' . mysql_real_escape_string( $idFacturation ) . '\', `idlivraison`=\'' . mysql_real_escape_string( $idLivraison ) . '\' WHERE `id`=\'' . mysql_real_escape_string( $idCommande ) . '\'';
		mysql_query( $query );
		
		// Email
		ob_start();
		include( ROOT . '/templates/email.order.text.php' );
		$messageText  = ob_get_clean();
		
		ob_start();
		include( ROOT . '/templates/email.order.html.php' );
		$messageHTML  = ob_get_clean();
		
		$boundary = md5( uniqid( time() ) );
		
		$headers  = 'From: "Editions Retz"<' . $emailNoReply . ">\n";
		$headers .= 'Return-Path: ' . $emailNoReply . "\n";
		$headers .= 'MIME-Version: 1.0' ."\n";
		$headers .= 'X-Mailer: PHP/' . phpversion() ."\n";
		$headers .= 'Content-Type: multipart/alternative; boundary="' . $boundary . '"' . "\n\n";
		$headers .= $messageText . "\n";
		$headers .= '--' . $boundary . "\n";
		$headers .= 'Content-Type: text/plain; charset=ISO-8859-1' ."\n";
		$headers .= 'Content-Transfer-Encoding: 8bit'. "\n\n";
		$headers .= $messageText . "\n";
		$headers .= '--' . $boundary . "\n";
		$headers .= 'Content-Type: text/HTML; charset=ISO-8859-1' ."\n";
		$headers .= 'Content-Transfer-Encoding: 8bit'. "\n\n";
		$headers .= $messageHTML . "\n";
		$headers .= '--' . $boundary . "--\n";
	
		mail( $client['email'], gettext( 'Votre commande sur Editions Retz' ), $messageText, $headers );
		
		*/
		
		// Envoi de la commande (flux Xml � Bill)
		// 2008-03-07 - Modif flux BILL avec nouveau param�trage pour prendre en compte les produits virtuels
		$codeTransaction = 'CDE';
		$nbVirtualProducts = 0;
		$nbTotalProducts = 0;
		if( !empty( $currentBasketProducts ) ){
			foreach( $currentBasketProducts as $basketProduct ){
				$nbTotalProducts++;
				if (in_array( $basketProduct->support, $numericDocuments ) ) {
					$nbVirtualProducts++;
				}
			}
		}
		if ($nbVirtualProducts > 0 and $nbVirtualProducts == $nbTotalProducts ) {
			$codeTransaction = 'CDI';
		}
			
		$orderXml = '<?xml version="1.0" encoding="UTF-8"?>';
		
		$orderXml .= '<COMMANDE>';
			$orderXml .= '<ORIGINE>RETZ</ORIGINE>';
			//$orderXml .= '<TYPECOMMANDE/>';
			$orderXml .= '<IDENTIFIANT>';	
				$orderXml .= '<IDCLIENT>' . strim_accents($_SESSION['id']) . '</IDCLIENT>';
				$orderXml .= '<NUMCOMMANDE>' . strim_accents($numCommande) . '</NUMCOMMANDE>';
                if (!empty($_CADDIE->codepromoActivated)) {
                    // Pour chaque op� commerciale via code promo, on remplit le code action avec le code promo
                    $orderXml .= '<CODEACTION>'.$_CADDIE->codepromoActivated.'</CODEACTION>';
                } else {
                    $orderXml .= '<CODEACTION>INTRTZ</CODEACTION>';
                }
				$orderXml .= '<DATECOMMANDE>' . strim_accents(date('dmY', $orderDate  ) ). '</DATECOMMANDE>';
				$orderXml .= '<CODETRANSACTION>' . strim_accents( $codeTransaction ). '</CODETRANSACTION>';
				$orderXml .= '<LOGIN/>';
				$orderXml .= '<MOTDEPASSE/>';
			$orderXml .= '</IDENTIFIANT>';	
			$orderXml .= '<CLIENT>';	
				if (isset ($client ) and (int)$client['numcli'] > 0  ) {
					$orderXml .= '<NUMCLIENT>' . sprintf( '%08d', (int)$client['numcli'] ) . '</NUMCLIENT>';	
				}
				else {
					$orderXml .= '<NUMCLIENT>P</NUMCLIENT>';
				}
				$orderXml .= '<CIVILITECLIENT>' . strim_accents($civilitesXml[$client['civilite']]) . '</CIVILITECLIENT>';
				$orderXml .= "<NOMCLIENT><![CDATA[" . strtoupper(strim_accents( $client['nom'] )) . "]]></NOMCLIENT>";
				$orderXml .= "<PRENOMCLIENT><![CDATA[" . strtoupper(strim_accents( $client['prenom'] )) . "]]></PRENOMCLIENT>";
				if ( $nbVirtualProducts > 0 ) {
					$orderXml .= '<LOGIN>' . strim_accents($_SESSION['id']) . '</LOGIN>';
				}
				$orderXml .= '<NUMETB/>';
			$orderXml .= '</CLIENT>';	
			$orderXml .= '<LIVRAISON>';
				$orderXml .= '<CIVILITELIVRAISON>' . strim_accents($civilitesXml[$_SESSION['delivery']['civilite']] ). '</CIVILITELIVRAISON>';
				$orderXml .= "<NOMLIVRAISON><![CDATA[" . strtoupper(strim_accents( $_SESSION['delivery']['nom'] )) . "]]></NOMLIVRAISON>";
				$orderXml .= "<PRENOMLIVRAISON><![CDATA[" . strtoupper(strim_accents( $_SESSION['delivery']['prenom'] )) . "]]></PRENOMLIVRAISON>";
				if ( isset ( $_SESSION['delivery'] ) and $_SESSION['delivery']['choix_facturation'] == 1) {
					// Livraison � l'�tablissement
					$orderXml .= '<LIVRETAB>Y</LIVRETAB>';
                    $orderXml .= '<TYPEETAB/>';
                    $orderXml .= '<NUMVOIE/>';
                    $orderXml .= '<BISTER/>';
                    $orderXml .= '<TYPEVOIE/>';
                    $orderXml .= "<ADR1><![CDATA[" . strtoupper(strim_accents( $_SESSION['delivery']['rue2'] )) . "]]></ADR1>";
                    $orderXml .= "<ADR2><![CDATA[" . strtoupper(strim_accents( $_SESSION['delivery']['lieu'] )) . "]]></ADR2>";
                    $orderXml .= '<ADR3><![CDATA['.strtoupper(strim_accents( $_SESSION['delivery']['rue3'] )).']]></ADR3>';
				}
				else {
					// Livraison chez le particulier
					$orderXml .= '<LIVRETAB>N</LIVRETAB>';
                    $orderXml .= '<TYPEETAB/>';
                    $orderXml .= '<NUMVOIE><![CDATA['.strtoupper(strim_accents( $_SESSION['delivery']['rue1'] )).']]></NUMVOIE>';
                    $orderXml .= '<BISTER><![CDATA['.strtoupper(strim_accents( $numadresses[$_SESSION['delivery']['rue1bis']] )).']]></BISTER>';
                    $orderXml .= "<TYPEVOIE><![CDATA[".strtoupper(strim_accents( $_SESSION['delivery']['rue2'] ))."]]></TYPEVOIE>";
                    $orderXml .= "<ADR1><![CDATA[".strtoupper(strim_accents( $_SESSION['delivery']['rue3'] ))."]]></ADR1>";
                    $orderXml .= '<ADR2/>';
                    $orderXml .= '<ADR3/>';
				}
				
				//Ecomiz 20140515 : Si pays = france, alors on formate le cp sur 5 chiffres
				if ( $_SESSION['delivery']['coderetz'] == 'M01001' )
					$_SESSION['delivery']['codepostal']   = substr_replace("00000",$_SESSION['delivery']['codepostal'] , -strlen($_SESSION['delivery']['codepostal']));
				$orderXml .= "<CPLIVRAISON><![CDATA[" .  strim_accents($_SESSION['delivery']['codepostal'])  . "]]></CPLIVRAISON>";
				$orderXml .= "<VILLELIVRAISON><![CDATA[" . strtoupper(strim_accents( $_SESSION['delivery']['ville'] )) . "]]></VILLELIVRAISON>";
				$orderXml .= '<PAYSLIVRCODE>' . strim_accents($_SESSION['delivery']['coderetz']) . '</PAYSLIVRCODE>';
				if ( !isset( $_SESSION['delivery']['coderetz'] ) or trim($_SESSION['delivery']['coderetz'] == '') ) {
					$orderXml .= '<PAYSLIVRLIBEL>' . strtoupper(strim_accents( $_SESSION['delivery']['pays'] )) . '</PAYSLIVRLIBEL>';
				}
				else {
					$orderXml .= '<PAYSLIVRLIBEL/>';
				}

				$orderXml .= '<DATELIVRAISON/>';
                
                // Gestion du code promo qui supprime les frais de port
                if (!empty($_CADDIE->codepromoType) && ($_CADDIE->codepromoType == 1)) {
                    $orderXml .= '<CODELOGISTIQUE>00</CODELOGISTIQUE>';
                } else {
                    if ( $_SESSION['delivery']['coderetz'] !== 'M01001' ) {
                        // livraison en dehors de la france				
                        $orderXml .= '<CODELOGISTIQUE>10</CODELOGISTIQUE>';
                    } else {
                        // livraison en france
						//modif 20140114 - 72 = code pour le 4.40� et franco = 45�
                        $orderXml .= '<CODELOGISTIQUE>76</CODELOGISTIQUE>';
                    }                    
                }
			$orderXml .= '</LIVRAISON>';
			$orderXml .= '<COORD>';
				$orderXml .= "<EMAIL><![CDATA[" . strim_accents($client['email']) . "]]></EMAIL>";
				$orderXml .= "<TELEPHONE><![CDATA[" . strim_accents($client['tel']) . "]]></TELEPHONE>";
				$orderXml .= "<FAX><![CDATA[" .strim_accents( $client['tel']) . "]]></FAX>";
			$orderXml .= '</COORD>';
			$orderXml .= '<CONTACTETAB1/>';
			$orderXml .= '<CONTACTETAB2/>';
			$orderXml .= '<PRODUITS>';
			// Ajout des produits
			$nbOrderProducts = 0;
			if( !empty( $currentBasketProducts ) ):
				foreach( $currentBasketProducts as $basketProduct ):
					$currentInfoBasketProduct = $_CADDIE->getProduct( $basketProduct->id );
					$orderXml .= '<PRODUIT>';
						if (in_array( $basketProduct->support, $numericDocuments ) ) {
							$orderXml .= '<TYPEPRODUIT>V</TYPEPRODUIT>';
						}
						else {
							$orderXml .= '<TYPEPRODUIT>L</TYPEPRODUIT>';
						}
						$orderXml .= '<TYPESUPPORT>' . strtoupper ( strim_accents($basketProduct->support) ) . '</TYPESUPPORT>';
						$orderXml .= '<CODEPRODUIT/>';
						$orderXml .= '<ISBN>' . strim_accents(substr ( $basketProduct->ean13, 3, 9 ))  . '</ISBN>';
						$orderXml .= '<EAN13>' . strim_accents($basketProduct->ean13) . '</EAN13>';
						$orderXml .= '<FORFAIT>N</FORFAIT>';	
						$orderXml .= '<CATEGORIE/>';	
						$orderXml .= "<TITRESOUSTITRE><![CDATA[" . strim_accents($basketProduct->titre) . ' ' .  strim_accents( $basketProduct->soustitre ) . "]]></TITRESOUSTITRE>";
						$orderXml .= '<QUANTITE>' . strim_accents($currentInfoBasketProduct->quantity ) . '</QUANTITE>';
						
                        /* D�but : Future version accept�e par Bill
                        $orderXml .= '<PRIXPROD>'.intval(strim_accents(floatval($basketProduct->price) * 100)).'</PRIXPROD>';
                        if (!empty($_CADDIE->codepromoType) && ($_CADDIE->codepromoType == 3) && !empty($_CADDIE->arrayReductionByProduct[$basketProduct->ean13])) {
                            // Ce produit b�n�ficie d'un code promo "ean" (type=3) au sein de cette commande
                            $orderXml .= '<CODEPRIX>001</CODEPRIX>';
                            $orderXml .= '<REMISE>'.intval($_CADDIE->arrayReductionByProduct[$basketProduct->ean13] * 100).'</REMISE>';
                        } else {
                            // Produit sans code promo "ean"
                            if (in_array($basketProduct->support, $numericDocuments )) {
                                $orderXml .= '<CODEPRIX>001</CODEPRIX>';
                            } else {
                                $orderXml .= '<CODEPRIX>011</CODEPRIX>';
                            }
                            $orderXml .= '<REMISE/>';
                        }
                       Fin : Future version accept�e par Bill */
                        
                        /* D�but : Version actuellement accept�e par Bill (05/12/2011) */
                        if (!empty($_CADDIE->codepromoType) && ($_CADDIE->codepromoType == 3) && !empty($_CADDIE->arrayReductionByProduct[$basketProduct->ean13])) {
                            $orderXml .= '<PRIXPROD>'.intval(((floatval($basketProduct->price) * 100) - ((floatval($basketProduct->price) * 100) * $_CADDIE->arrayReductionByProduct[$basketProduct->ean13] / 100))).'</PRIXPROD>';
                            $orderXml .= '<CODEPRIX>007</CODEPRIX>';
                        } else {
                            // Produit sans code promo "ean"
                            $orderXml .= '<PRIXPROD>'.strim_accents(floatval($basketProduct->price) * 100).'</PRIXPROD>';
                            if (in_array($basketProduct->support, $numericDocuments )) {
                                $orderXml .= '<CODEPRIX>001</CODEPRIX>';
                            } else {
                                $orderXml .= '<CODEPRIX>011</CODEPRIX>';
                            }
                        }
                        $orderXml .= '<REMISE/>';
                        /* Fin : Version actuellement accept�e par Bill */
                        
						$orderXml .= '<DATEDEBUT/>';
						$orderXml .= '<DATEFIN/>';
						$orderXml .= '<TYPEACCES/>';
					$orderXml .= '</PRODUIT>';
					
					$nbOrderProducts = $nbOrderProducts + $currentInfoBasketProduct->quantity;
				endforeach;
				unset( $basketProduct, $currentInfoBasketProduct );
			endif;	
			$orderXml .= '</PRODUITS>';
			$orderXml .= '<REGLEMENT>';
				//modification 13/09/11
				//$orderXml .= '<TOTALCOMMANDE>' . intval ( $sumTotal * 100 ) . '</TOTALCOMMANDE>';
				
            // Gestion du code promo remise sur les ebooks et la remise sur les produits
            if (!empty($_CADDIE->codepromoType) && ($_CADDIE->codepromoType == 2) && !empty($_CADDIE->codepromoRemise)) {
                $orderXml .= '<MONTANTREMISE>'.($_CADDIE->codepromoRemise * 100).'</MONTANTREMISE>';
                // $sumTotal -= $_CADDIE->codepromoRemise;
            }
            if (!empty($_CADDIE->codepromoType) && ($_CADDIE->codepromoType == 3) && !empty($_CADDIE->codepromoRemise)) {
                // $sumTotal -= $_CADDIE->codepromoRemise;
            }

            // $sumTotal = 53.685; // Pour tests
            $sumTotalBill = $sumTotal;
            if (strpos($sumTotalBill * 100, '.')) {
                // debug(TRUE);
                $sumTotalBill = intval($sumTotalBill * 100);
            } else {
                // debug(FALSE);
                $sumTotalBill = $sumTotalBill * 100;
            }
            
            $orderXml .= '<TOTALCOMMANDE>'.$sumTotalBill.'</TOTALCOMMANDE>';
				
            
            $orderXml .= '<QTETOTALE>' . $nbOrderProducts .  '</QTETOTALE>';
                
                // Gestion du code promo qui supprime les frais de port
                if (!empty($_CADDIE->codepromoType) && ($_CADDIE->codepromoType == 1)) {
                    $orderXml .= '<FRAISPORT>0</FRAISPORT>';
                } else {
                    $orderXml .= '<FRAISPORT>' . intval ( $sumPort * 100 ) . '</FRAISPORT>';
                }
                
                
                if ($_SESSION['payment'] == 'CB'  ) {
					$orderXml .= '<CODEREGLEMENT>05</CODEREGLEMENT>';
					$orderXml .= '<TYPECB>';
					if (in_array($_POST['typecarte'], array('Carte Bleue','Carte Visa') ) ){
						$orderXml .= 'CB';
					}elseif (in_array($_POST['typecarte'], array('EuroCard','MasterCard') ) )  {
						$orderXml .= 'EU';
					} 
					$orderXml .= '</TYPECB>';
					$orderXml .= '<NUMCB>' . strim_accents($_POST['numcarte']) . '</NUMCB>';
					$orderXml .= '<DATEEXPIRCB>' . strim_accents(substr ( $_POST['datecarte'], 0, 2)) . strim_accents(substr ( $_POST['datecarte'], 3, 2)) . '</DATEEXPIRCB>';
					$orderXml .= '<NUMCONTROLECB>' . strim_accents($_POST['codecarte'] ). '</NUMCONTROLECB>';
				}			
				else {
					if ($_SESSION['payment'] == 'C'  ) {
						$orderXml .= '<CODEREGLEMENT>40</CODEREGLEMENT>';
					}
					else {
						$orderXml .= '<CODEREGLEMENT>00</CODEREGLEMENT>';
					}
					$orderXml .= '<TYPECB/>';
					$orderXml .= '<NUMCB/>';
					$orderXml .= '<DATEEXPIRCB/>';
					$orderXml .= '<NUMCONTROLECB/>';
				}
			$orderXml .= '</REGLEMENT>';
		$orderXml .= '</COMMANDE>';	
		
		//var_dump(htmlspecialchars($orderXml));
 
		/* Cr�ation et envoi du Flux XML */
		$bError = false;
		
		//$xmlfile="../docs/commande_test2007-07-02-010.xml";
    	$result="";
     	$xmlDoc=new DOMDocument();
		
      	//$xmlRequest=$xmlDoc->load($xmlfile);
	  	$xmlRequest=$xmlDoc->loadXML($orderXml);
 
        if (!$xmlRequest){
			@mail('fpioche@sejer.fr','Editions Retz - Erreur lors du chargement du flux Bill', 'une erreur s\'est produite lors du chargement du flux Xml au cours de l\'envoi � Bill pour la commande : ' . $numCommande );		
       	 	@mail('studyrama18@gmail.com','Editions Retz - Erreur lors du chargement du flux Bill', 'une erreur s\'est produite lors du chargement du flux Xml au cours de l\'envoi � Bill pour la commande : ' . $numCommande );		
       	 	
			echo "fail to load xml file";
            // r�initialiser le prochain numero de commande
            /*
            $resultMax = mysql_fetch_assoc(mysql_query( 'SELECT MAX( `nocommande` ) AS `nocommande` FROM `commandes` WHERE `prefixe` = \'' . mysql_real_escape_string( $infosNumCommande['prefixe'] ) .'\'' ));
            mysql_query("update `nums` set `mini`='".mysql_real_escape_string($resultMax['nocommande'])."'");
             */
			$bError = true;
       	 	exit;
        }
        $xmlSource=$xmlDoc->saveXML();
		//Ecomiz 20131001
		mail('hz.hedizouari@gmail.com', 'Commande Retz', $xmlSource);
		 // exit;
        print_r($xmlSource);
        $contentLength=strlen($xmlSource);
        if ($_CONST['TYPE_ENVIRONNEMENT'] != 'dev') {
            $fp = fsockopen("ssl://nsxa-server.net", 443, $errno, $errstr, 30);
        }

       	if (isset($fp) && !$fp) {
            if ($_CONST['TYPE_ENVIRONNEMENT'] != 'dev') {
                //ajout envoi mail le 04/07/11
				@mail('fpioche@sejer.fr','Editions Retz - Erreur lors du chargement du flux Bill - WS inaccessible', 'commande : '.$numCommande.'  -'. $errstr ($errno) );		
                @mail('studyrama18@gmail.com','Editions Retz - Erreur lors du chargement du flux Bill', 'une erreur s\'est produite lors du chargement du flux Xml au cours de l\'envoi � Bill pour la commande : ' . $numCommande );		
       	 	
				echo "<br/>$errstr ($errno)<br />\n";
                $xml_error_send = "$errstr ($errno)";
                $bError = true;
                // Redirection vers ordererrorb.html
                header('location: http://www.editions-retz.com/ordererrorb.html');
                exit();
            } else {
                mail('fpioche@sejer.fr', 'Commande Retz', $xmlSource);
                mail('gildas@me.com', 'Commande Retz', $xmlSource);
                mail('johan.mizrahi@ecomiz.com', 'Commande Retz', $xmlSource);
                mail('hedi.zouari@ecomiz.com', 'Commande Retz', $xmlSource);
            }
       	}
		else {
            if ($_CONST['TYPE_ENVIRONNEMENT'] != 'dev') {
                fputs($fp, "POST /cdes/transfert/loadcontrolxml.asp?CA=xxxx HTTP/1.0\r\n");
                fputs($fp, "Host: nsxa-server.net\r\n");
                fputs($fp, "Content-type: text/xml\r\n");
                fputs($fp, "Content-Length: $contentLength\r\n"); // 
                fputs($fp, "Connexion: close\r\n");
                fputs($fp, "\r\n"); // all headers sent
                fputs($fp, $xmlSource); // sending xml
				
				$xmlSource = preg_replace('/<NUMCB>(.*)<\/NUMCB>/', '', $xmlSource);
				//DEBUG ECOMIZ SUR PROD POUR BUG DOUBLON DE COMMANDE
				@mail('fpioche@sejer.fr','Nouvelle Commande RETZ - commande', "HTTP REFERER :\r\n".$_SERVER['HTTP_REFERER']."\r\n\r\nSESSION : \r\n".print_r($_SESSION, true)."\r\n\r\n XML sended : $xmlSource");
				@mail('johan.mizrahi@gmail.com','Nouvelle Commande RETZ - commande', "HTTP REFERER :\r\n".$_SERVER['HTTP_REFERER']."\r\n\r\nSESSION : \r\n".print_r($_SESSION, true)."\r\n\r\n XML sended : $xmlSource");
				@mail('hz.hedizouari@gmail.com','Nouvelle Commande RETZ - commande', "HTTP REFERER :\r\n".$_SERVER['HTTP_REFERER']."\r\n\r\nSESSION : \r\n".print_r($_SESSION, true)."\r\n\r\n XML sended : $xmlSource");
					var_dump('test'); exit; 
                while (!feof($fp)) {
                   $result .= fgets($fp, 128);
                }
                fclose($fp);
            }
  			$xml_error_send = "ok";
       } 
	   
	   //avant 04/07/11 : log enregistr� ici
	   //$query = "INSERT INTO `logs` (`xml`, `error`, `date`)  VALUES ('".mysql_real_escape_string( $orderXml )."', '".mysql_real_escape_string( $xml_error_send )."', NOW())";
	   //@mysql_query( $query );
		//@mail("skubiak@alteo.fr", "Editions-Retz logs Commande", $result."\n\n\nEtat : ".$xml_error_send);
		$sPatternSeperate = '/\r?\n\r?\n/';
		$arMatchResponsePart = preg_split($sPatternSeperate, $result, 2);
		//@mail("skubiak@alteo.fr","Editions-Retz logs Commande",  trim($arMatchResponsePart[1]));
		
        // Pour tester une erreur de Bill
        // $arMatchResponsePart[1] = 5;
        
		// Gestion des codes d'erreurs
		if(isset($arMatchResponsePart[1]))
			switch ($arMatchResponsePart[1]){
				case 0: $xml_error_send = "Code : ".$arMatchResponsePart[1]." - Message : la structure du flux XML n�est pas correcte"; $bError = true; break;
				case 1: $xml_error_send = "Code : ".$arMatchResponsePart[1]." - Message : OK"; break;
				case 2: $xml_error_send = "Code : ".$arMatchResponsePart[1]." - Message : OK"; break;
				case 3: $xml_error_send = "Code : ".$arMatchResponsePart[1]." - Message : la commande est rejet�e (CODEACTION invalide)"; $bError = true; break;
				case 4: $xml_error_send = "Code : ".$arMatchResponsePart[1]." - Message : la commande est rejet�e (DOUBLON de NUMCOMMANDE)";  $bError = true; break;
				case 5: $xml_error_send = "Code : ".$arMatchResponsePart[1]." - Message : la commande est rejet�e (PAIEMENT REFUSEE / CB INVALIDE)";  $bError = true; break;
				case 6: $xml_error_send = "Code : ".$arMatchResponsePart[1]." - Message : OK"; break;
				case 7: $xml_error_send = "Code : ".$arMatchResponsePart[1]." - Message : la commande est rejet�e car l�e-mail existe d�j� en base";  $bError = true; break;
				case 8: $xml_error_send = "Code : ".$arMatchResponsePart[1]." - Message : la commande rejet�e car la banque du client refuse le paiement";  $bError = true; break;
			}
		
				
		//04/07/11 : stocker le flux en base avec suppression n� CB avant stockage
		$orderXml = preg_replace('/<NUMCB>[^<]*<\/NUMCB>/', '<NUMCB>XXX</NUMCB>', $orderXml);
		$querylogs = "INSERT INTO `logs` (`xml`, `error`, `date`)  VALUES ('".mysql_real_escape_string( $orderXml )."', '".mysql_real_escape_string( $xml_error_send )."', NOW())";
		@mysql_query( $querylogs );
		
		if($bError === true) {
			@mail('studyrama18@gmail.com','Editions Retz - R�ponse de flux Bill : Erreur', 'Bill a r�pondu  : '.$xml_error_send.'- pour la commande : ' . $numCommande );
		}
				
		// Gestion des codes d'erreur : 5 et 8
		//Modification le 04/07/11 pour ajouter les cas d'erreur 0,3,4,7
		if($_CONST['TYPE_ENVIRONNEMENT'] == "prod" &&  (trim($arMatchResponsePart[1]) == '5' or  trim($arMatchResponsePart[1]) == '8' or  trim($arMatchResponsePart[1]) == '0' or  trim($arMatchResponsePart[1]) == '3' or  trim($arMatchResponsePart[1]) == '4' or  trim($arMatchResponsePart[1]) == '7') ) {
			// SK - 2008-02-11 : pas de cr�ation de commande en cas d'erreur retourn�e par Bill
			//$query = 'UPDATE `commandes` SET `typepaiement`=\'C\' WHERE `id`=\'' . mysql_real_escape_string( (int)$idCommande ) . '\'';
			//mysql_query( $query );
			//@mail('fpioche@sejer.fr','Editions Retz - R�ponse de flux Bill : Erreur', 'Bill a r�pondu un code erreur (5 ou 8) pour la commande : ' . $numCommande );
			//04/07/11 : modification du mail			
			
            @mail('studyrama18@gmail.com','Editions Retz - R�ponse de flux Bill : Commande refus�e', 'Bill a r�pondu  : '.$xml_error_send.'- pour la commande : ' . $numCommande );
			$_CADDIE->deleteAllProducts();
            promocodes::deletePromoCodeSession();
			unset( $_SESSION['lservice'], $_SESSION['reduction'], $_SESSION['delivery'], $nbOrderProducts, $orderXml );
			unset( $_SESSION['payment'] );
            // r�initialiser le prochain numero de commande
            /*
            $resultMax = mysql_fetch_assoc(mysql_query( 'SELECT MAX( `nocommande` ) AS `nocommande` FROM `commandes` WHERE `prefixe` = \'' . mysql_real_escape_string( $infosNumCommande['prefixe'] ) .'\'' ));
            mysql_query("update `nums` set `mini`='".mysql_real_escape_string($resultMax['nocommande'])."'");
             */
            
            // Si 5 et 8 => order-cancelcb.html
            if ((trim($arMatchResponsePart[1]) == '5') || (trim($arMatchResponsePart[1]) == '8')) {
                header( 'Location: http://www.editions-retz.com/order-cancelcb.html' );
                exit;
            }
            
            // Si 0, 3, 4 et 7 => order-canceltec.html
            if ((trim($arMatchResponsePart[1]) == '0') || (trim($arMatchResponsePart[1]) == '3') || (trim($arMatchResponsePart[1]) == '4') || (trim($arMatchResponsePart[1]) == '7')) {
                header( 'Location: http://www.editions-retz.com/index.php?page=order-canceltec' );
                exit;
            }
		}
		elseif($_CONST['TYPE_ENVIRONNEMENT'] == "prod" && $bError === true){//si erreur autre que erreurs Bill	
				$_CADDIE->deleteAllProducts();
                promocodes::deletePromoCodeSession();
				unset( $_SESSION['lservice'], $_SESSION['reduction'], $_SESSION['delivery'], $nbOrderProducts, $orderXml );
				unset( $_SESSION['payment'] );
				// r�initialiser le prochain numero de commander
                /*
                    $resultMax = mysql_fetch_assoc(mysql_query( 'SELECT MAX( `nocommande` ) AS `nocommande` FROM `commandes` WHERE `prefixe` = \'' . mysql_real_escape_string( $infosNumCommande['prefixe'] ) .'\'' ));
                    mysql_query("update `nums` set `mini`='".mysql_real_escape_string($resultMax['nocommande'])."'");
                */
				@mail('studyrama18@gmail.com','Editions Retz - commande refus�e - non enregistr�e en base', 'Bill a r�pondu  : '.$xml_error_send.'- pour la commande : ' . $numCommande );
				header( 'Location: http://www.editions-retz.com/ordererrorb.html' );
				exit;	
		}
		// si Bill ne renvoie pas de code d'erreur : cration de la commande + envoi d'email de confirmation.
		else {
			//si aucune erreur : enregistrement en base
			// Commande
			$query = '
				INSERT INTO `commandes`
					( `prefixe`, `nocommande`, `nbcar`, `libnocommande`, `datecommande`, `idclient`, `ht`,`tva196`,`tva55`, `ttc`, `remisettc`, `port`,`typepaiement`, `reglement`, `archive`, `portdevis`, `numlivraison`, `tva7`)
				VALUES 
					( \'' . mysql_real_escape_string( $infosNumCommande['prefixe'] ) .'\', \'' . mysql_real_escape_string( $numAct ) .'\', \'' . mysql_real_escape_string( $infosNumCommande['nbcar'] ) .'\', \'' . mysql_real_escape_string( $numCommande ) .'\',  \'' . date('Y-m-d H:i:s', $orderDate  ) . '\' , \'' . mysql_real_escape_string( (int)$_SESSION['id'] ) . '\' , \'' . mysql_real_escape_string( sprintf( '%01.2F', $sumTotal - $sumTva['19.6'] - $sumTva['5.5'] - $sumTva['7']   ) ) . '\', \'' . mysql_real_escape_string( sprintf( '%01.2F', $sumTva['19.6'] ) ) . '\' , \'' . mysql_real_escape_string( sprintf( '%01.2F', $sumTva['5.5'] ) ) . '\',   \'' . mysql_real_escape_string( sprintf( '%01.2F', $sumTotal ) ) . '\' , \'' . mysql_real_escape_string( sprintf( '%01.2F', (!empty($_CADDIE->codepromoRemise) ? $_CADDIE->codepromoRemise : $sumReduction) ) ) . '\' , \'' . mysql_real_escape_string( sprintf( '%01.2F', $sumPort ) ) . '\' , \'' . mysql_real_escape_string( $_SESSION['payment'] ) . '\', \'0\', \'0\', \'' . mysql_real_escape_string ($portdevis) . '\', \''.(!empty($_CADDIE->codepromoActivated) ? mysql_real_escape_string($_CADDIE->codepromoActivated) : '').'\', \'' . mysql_real_escape_string( sprintf( '%01.2F', $sumTva['7'])).'\')
			';
			mysql_query( $query );
			$idCommande = mysql_insert_id();
            $orderProducts=array();
			// Produits commandes
			if( !empty( $currentBasketProducts ) ):
				foreach( $currentBasketProducts as $basketProduct ):
					$currentInfoBasketProduct = $_CADDIE->getProduct( $basketProduct->id );
					// GMV 20120111 : conservation de la tva � 5.5 + pb insertion tva 7% dans commande_produit
					$productTva = '5.5';
					// GMV 20120111 : Ajout d'une variable pour calculer la tva du produit en fonction de si 19.6 ou 7 ou 5.5
					$strTvaDivision = 1.055 ;
					if (in_array( $basketProduct->support, $numericDocuments ) ) {
						$productTva = '7';	
						//ZHZH 20111231 : Ajout d'une variable pour calculer la tva du produit en fonction de si 19.6 ou 7
                        $strTvaDivision = 1.07;
					} 				
					$query = 'INSERT INTO `commande_produits` ( `idcommande`, `idproduit`, `name`, `tva`, `ean13`, `isbn`, `qt`, `ht`, `ttc` ) 
                                                        VALUES ( \'' . mysql_real_escape_string( $idCommande ) . '\', \'' . mysql_real_escape_string(  $basketProduct->id ) . '\',  \'' . mysql_real_escape_string( $basketProduct->titre ) . '\'  , \'' . $productTva . '\' ,  \'' . mysql_real_escape_string( $basketProduct->ean13 ) . '\',  \'' . mysql_real_escape_string( $basketProduct->isbn ) . '\', \'' . mysql_real_escape_string( $currentInfoBasketProduct->quantity ) . '\', \'' . mysql_real_escape_string( sprintf( '%01.2F', ( $basketProduct->price * $currentInfoBasketProduct->quantity ) / $strTvaDivision ) ) . '\', \'' . mysql_real_escape_string( sprintf( '%01.2F', ( $basketProduct->price * $currentInfoBasketProduct->quantity ) ) ) . '\' )';
					mysql_query( $query );
					unset( $query );
					
					/*
					 * EcomiZ Update 2011-08-05
					 * AJout de la cat�gorie dans le tableau $orderProducts pour GA
					 */
                    $orderProducts[]=array(	'ref'=>$basketProduct->ean13,
                    						'name'=>$basketProduct->titre,
                    						'categorie'=>$basketProduct->support,
                    						'ttc'=>sprintf('%01.2f',$basketProduct->price),
                    						'quantity'=>$currentInfoBasketProduct->quantity);
					/*
					 * Fin EcomiZ Update 2011-08-05
					 */
					
				
				endforeach;
				unset( $basketProduct, $currentInfoBasketProduct );
			endif;		

            //$_SESSION['tabgoogle']=array('idcommande'=>$numAct,'affil'=>'commande','total'=>$sumTotal,'tva'=>round($sumTva['19.6'],2) + round($sumTva['5.5'],2) ,'port'=>$sumPort,'ville'=>$_SESSION['delivery']['ville'],'dep'=>substr($_SESSION['delivery']['codepostal'],0,2),'pays'=>$_SESSION['delivery']['pays'],'prod'=>$orderProducts);



			/*
             * EcomiZ Update - 2011-08-03
             * Ajout des tags Google analytics 
             */
            $strTagGA = '';
            if ($_CONST['TYPE_ENVIRONNEMENT'] == "prod") {
                $strTagGA .= '
                <script type="text/javascript">
                  var gaJsHost = (("https:" == document.location.protocol ) ? "https://ssl." : "http://www.");
                  document.write(unescape("%3Cscript src=\'" + gaJsHost + "google-analytics.com/ga.js\' type=\'text/javascript\'%3E%3C/script%3E"));
                </script>
                <script type="text/javascript">
                try{
                  var pageTracker = _gat._getTracker("UA-6518058-1");
                  pageTracker._trackPageview();
                  pageTracker._addTrans(
                      "'.$numCommande.'",
                      "RETZ",
                      "'.sprintf('%01.2F',$sumTotal).'",
                      "'.sprintf('%01.2f',(round($sumTva['19.6'],2) + round($sumTva['5.5'],2) + round($sumTva['7'],2))).'",
                      "'.sprintf('%01.2f',$sumPort).'",
                      "'.$_SESSION['delivery']['ville'].'",
                      "",
                      "'.$_SESSION['delivery']['pays'].'"
                    );';  
                foreach($orderProducts as $aProdInfos) {
                    $strTagGA .= 'pageTracker._addItem(
                    "'.$numCommande.'",
                    "'.$aProdInfos["ref"].'",
                    "'.$aProdInfos["name"].'",
                    "'.$aProdInfos["categorie"].'",
                    "'.$aProdInfos["ttc"].'",
                    "'.$aProdInfos["quantity"].'"
                    );';
                }
                $strTagGA .= 'pageTracker._trackTrans(); //submits transaction to the Analytics servers
                } catch(err) {}
                </script>';
            }
		
            //stockage en session pour utilisation dans order-confirmcb et order-confirmc
            $_SESSION['tags_GA'] = $strTagGA;
		
            /* Fin Google Analytics */
			
			// Adresse de facturation
			// Ancien mode, avec infos client : $query = 'INSERT INTO `commande_facturation` ( `idcommande`, `civilite`, `numcli`, `nom`, `prenom`, `lieuchoice`, `lieu`, `rue1`, `rue1bis`, `rue2`, `rue3`, `codepostal`, `ville`, `idpays` ) VALUES ( \'' . mysql_real_escape_string( $idCommande ) . '\', \'' . mysql_real_escape_string( $client['civilite'] ) . '\' , \'' . mysql_real_escape_string( $client['numcli'] ) . '\' , \'' . mysql_real_escape_string( $client['nom'] ) . '\', \'' . mysql_real_escape_string( $client['prenom'] ) . '\', \'' . mysql_real_escape_string( $client['lieufactchoice'] ) . '\', \'' . mysql_real_escape_string( $client['lieufact'] ) . '\' ,  \'' . mysql_real_escape_string( $client['rue1fact'] ) . '\',  \'' . mysql_real_escape_string( $client['rue1factbis'] ) . '\' , \'' . mysql_real_escape_string( $client['rue2fact'] ) . '\', \'' . mysql_real_escape_string( $client['rue3fact'] ) . '\' , \'' . mysql_real_escape_string( $client['cpfact'] ) . '\', \'' . mysql_real_escape_string( $client['villefact'] ) . '\', \'' . mysql_real_escape_string( $client['idpaysfact'] ) . '\' )';
            // Nouveau mode : prise en compte des infos saisies � l'�tape order-delivery
            
            $query = 'INSERT INTO `commande_facturation` ( `idcommande`, `civilite`, `numcli`, `nom`, `prenom`, `lieuchoice`, `lieu`, `rue1`, `rue1bis`, `rue2`, `rue3`, `codepostal`, `ville`, `idpays` ) VALUES ( \'' . mysql_real_escape_string( $idCommande ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['civilite'] ) . '\' , \'' . mysql_real_escape_string( $_SESSION['delivery']['numcli'] ) . '\' , \'' . mysql_real_escape_string( $_SESSION['delivery']['nom'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['prenom'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['lieuchoice'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['lieu'] ) . '\' ,  \'' . mysql_real_escape_string( $_SESSION['delivery']['rue1'] ) . '\',  \'' . mysql_real_escape_string( $_SESSION['delivery']['rue1bis'] ) . '\' , \'' . mysql_real_escape_string( $_SESSION['delivery']['rue2'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['rue3'] ) . '\' , \'' . mysql_real_escape_string( $_SESSION['delivery']['codepostal'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['ville'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['idpays'] ) . '\' )';
			mysql_query( $query );
			$idFacturation = mysql_insert_id();	
			
			// Adresse de facturation 
			$adresseFacturation = $civilites [$_SESSION['delivery']['civilite'] ] . ' ' . $_SESSION['delivery']['prenom'] . ' ' . $_SESSION['delivery']['nom'] . chr( 10 );
			if( !empty( $_SESSION['delivery']['numcli'] ) ):
				$adresseFacturation .= '(N� ' . $_SESSION['delivery']['numcli'] . ')' . chr( 10 );
			endif;
            if (isset($_SESSION['delivery']) && ($_SESSION['delivery']['choix_facturation'] == 1)) {
                // Livraison � l'�tablissement
                $adresseFacturation .= $_SESSION['delivery']['lieu'].chr(10);
                $adresseFacturation .= $_SESSION['delivery']['rue2'].' '.$_SESSION['delivery']['rue3'].chr(10);
            } else {
                // Livraison au domicile du particulier
                $rueBis = '';
                if (isset($numadresses[$_SESSION['delivery']['rue1bis']])) {
                    $rueBis = $numadresses[$_SESSION['delivery']['rue1bis']];
                }
                $adresseFacturation .= $_SESSION['delivery']['rue1'].' '.$rueBis.' '.$_SESSION['delivery']['rue2'].' '.$_SESSION['delivery']['rue3'].chr(10);
            }
            /*
			if( !empty( $_SESSION['delivery']['lieu'] ) ):
				$adresseFacturation .=  $_SESSION['delivery']['lieu'];
                if (!empty($_SESSION['delivery']['rue3'])) {
                    $adresseFacturation .= trim($_SESSION['delivery']['rue3']);
                }
                $adresseFacturation .= chr(10);
			endif;
			if( !empty( $_SESSION['delivery']['rue1'] ) ):
				$adresseFacturation .= $_SESSION['delivery']['rue1'] ;
				if( !empty( $_SESSION['delivery']['rue1bis'] ) ):
					$adresseFacturation .= ' ' . $numadresses[$_SESSION['delivery']['rue1bis']] ;
				endif;
			$adresseFacturation .= chr( 10 );
			endif;
            if (!empty($_SESSION['delivery']['rue2'])) {
                $adresseFacturation .= trim($_SESSION['delivery']['rue2']). chr(10);
            }
            */
			$adresseFacturation .= trim( $_SESSION['delivery']['codepostal'] . ' ' . $_SESSION['delivery']['ville'] ) . chr( 10 );
			$adresseFacturation .= $_SESSION['delivery']['pays'];
						
			$query = 'INSERT INTO `commande_livraison` ( `idcommande`, `civilite`, `numcli`, `nom`, `prenom`, `lieuchoice`, `lieu`, `rue1`, `rue1bis`, `rue2`, `rue3`, `codepostal`, `ville`, `idpays` ) VALUES ( \'' . mysql_real_escape_string( $idCommande ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['civilite'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['numcli'] ) . '\' , \'' . mysql_real_escape_string( $_SESSION['delivery']['nom'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['prenom'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['lieuchoice'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['lieu'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['rue1'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['rue1bis'] ) . '\' , \'' . mysql_real_escape_string( $_SESSION['delivery']['rue2'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['rue3'] ) . '\'  , \'' . mysql_real_escape_string( $_SESSION['delivery']['codepostal'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['ville'] ) . '\', \'' . mysql_real_escape_string( $_SESSION['delivery']['idpays'] ) . '\' )';
			mysql_query( $query );
			$idLivraison = mysql_insert_id();
			
			$adresseLivraison =  $civilites [ $_SESSION['delivery']['civilite'] ] . ' ' . $_SESSION['delivery']['prenom'] . ' ' .  $_SESSION['delivery']['nom'] . chr( 10 );
			if( !empty( $_SESSION['delivery']['numcli'] ) ):
				$adresseLivraison .= '(N� ' . $_SESSION['delivery']['numcli'] . ')' . chr( 10 );
			endif;
            
            if (isset($_SESSION['delivery']) && ($_SESSION['delivery']['choix_facturation'] == 1)) {
                // Livraison � l'�tablissement
                $adresseLivraison .= $_SESSION['delivery']['lieu'].chr(10);
                $adresseLivraison .= $_SESSION['delivery']['rue2'].' '.$_SESSION['delivery']['rue3'].chr(10);
            } else {
                // Livraison au domicile du particulier
                $rueBis = '';
                if (isset($numadresses[$_SESSION['delivery']['rue1bis']])) {
                    $rueBis = $numadresses[$_SESSION['delivery']['rue1bis']];
                }
                $adresseLivraison .= $_SESSION['delivery']['rue1'].' '.$rueBis.' '.$_SESSION['delivery']['rue2'].' '.$_SESSION['delivery']['rue3'].chr(10);
            }
            /*
			if( !empty( $_SESSION['delivery']['lieu'] ) ):
				$adresseLivraison .= $_SESSION['delivery']['lieu'];
                if (!empty($_SESSION['delivery']['rue3'])) {
                    $adresseLivraison .= trim($_SESSION['delivery']['rue3']);
                }
                $adresseLivraison .= chr(10);            
			endif;	
			if( !empty( $_SESSION['delivery']['rue1'] ) ):
				$adresseLivraison .= $_SESSION['delivery']['rue1'] ;
				if( !empty( $_SESSION['delivery']['rue1bis'] ) ):
					$adresseLivraison .= ' ' . $numadresses [$_SESSION['delivery']['rue1bis']] ;
				endif;
                $adresseLivraison .= chr( 10 );
			endif;
            if (!empty($_SESSION['delivery']['rue2'])) {
                $adresseLivraison .= trim($_SESSION['delivery']['rue2']).chr(10);
            }
            */
			$adresseLivraison .= trim( $_SESSION['delivery']['codepostal'] . ' ' .  $_SESSION['delivery']['ville'] ) . chr( 10 );
			$adresseLivraison .=  $_SESSION['delivery']['pays'];
			
			// Mise a jour des idFacturation et idLivraison de la commande
			$query = 'UPDATE `commandes` SET `idfacturation`=\'' . mysql_real_escape_string( $idFacturation ) . '\', `idlivraison`=\'' . mysql_real_escape_string( $idLivraison ) . '\' WHERE `id`=\'' . mysql_real_escape_string( $idCommande ) . '\'';
			mysql_query( $query );
			
            // Enregistrement du num�ro de commande en session pour la page de confirmation de commande
            $_SESSION['num_last_commande'] = $numCommande;
            
            // Email
            switch ($codeTransaction) {
                case 'CDE' :
                    ob_start();
                    include( ROOT . '/templates/email.order.text.php' );
                    $messageText  = ob_get_clean();
                    ob_start();
                    include( ROOT . '/templates/email.order.html.php' );
                    $messageHTML  = ob_get_clean();
                break;
                case 'CDI' :
                    ob_start();
                    include( ROOT . '/templates/email.ordernum.text.php' );
                    $messageText  = ob_get_clean();
                    ob_start();
                    include( ROOT . '/templates/email.ordernum.html.php' );
                    $messageHTML  = ob_get_clean();
                break;
            }
			
			$boundary = md5( uniqid( time() ) );
			
			$headers  = 'From: "Editions Retz"<' . $emailNoReply . ">\n";
			$headers .= 'Return-Path: ' . $emailNoReply . "\n";
			$headers .= 'MIME-Version: 1.0' ."\n";
			$headers .= 'X-Mailer: PHP/' . phpversion() ."\n";
			$headers .= 'Content-Type: multipart/alternative; boundary="' . $boundary . '"' . "\n\n";
			$headers .= $messageText . "\n";
			$headers .= '--' . $boundary . "\n";
			$headers .= 'Content-Type: text/plain; charset=ISO-8859-1' ."\n";
			$headers .= 'Content-Transfer-Encoding: 8bit'. "\n\n";
			$headers .= $messageText . "\n";
			$headers .= '--' . $boundary . "\n";
			$headers .= 'Content-Type: text/HTML; charset=ISO-8859-1' ."\n";
			$headers .= 'Content-Transfer-Encoding: 8bit'. "\n\n";
			$headers .= $messageHTML . "\n";
			$headers .= '--' . $boundary . "--\n";
		
			mail( $client['email'],  'Votre commande sur Editions Retz', $messageText, $headers );
			//ECOMIZ 20131001 
			mail( "hz.hedizouari@gmail.com",  'Votre commande sur Editions Retz', $messageText, $headers );
			if ($_CONST['TYPE_ENVIRONNEMENT'] == "prod")
				@mail('studyrama18@gmail.com','Editions Retz - commande OK - mail envoy� au client et enregistrer en base', 'Bill a r�pondu  : '.$xml_error_send.'- pour la commande : ' . $numCommande );
			
			
		}
        
		/* FIN Flux XML */
		//echo '<br /><p>' . $orderXml . '</p>';
		$_CADDIE->deleteAllProducts();
        promocodes::deletePromoCodeSession();
		unset( $_SESSION['lservice'], $_SESSION['reduction'], $_SESSION['delivery'], $nbOrderProducts, $orderXml );
		
		$_SESSION['idcommande'] = (int)$idCommande;
		// Paiement en ligne
		if( $_SESSION['payment'] == 'CB' ):
			// Redirection vers le Paiement en ligne
			unset( $_SESSION['payment'] );
			//header( 'Location: order-paymentcb.html' );
			header( 'Location: http://www.editions-retz.com/order-confirmcb.html' );
			exit;
		elseif( $_SESSION['payment'] == 'C' ):
		   	// Redirection vers la confirmation de paiement par ch�que
			unset( $_SESSION['payment'] );
			header( 'Location: http://www.editions-retz.com/order-confirmc.html' );
			exit;
		endif;
	endif;
endif;

// Retour
if( !empty( $_POST ) and isset( $_POST['return'] ) ):
	header( 'Location: order-payment.html' );
	exit;
endif;

// V�rification https
/*
if (strpos($_SERVER['SCRIPT_URI'], 'http://www.editions-retz.com') !== false) {
    echo "<script>document.location.href='https://www.editions-retz.com".$_SERVER['REQUEST_URI']."'; </script>";
    exit();
}*/

// Sum
$sumBasket = 0;
$sumBasketTTC = 0;
$sumTva = array(
	'19.6' => '0',
	'5.5' => '0',
    '7' => '0'
);
$sumPort = 0;
$sumPortFree = 45;
$tauxReduction = 0.05;
$sumPortHT = 0;
$sumReduction = 0;
$sumTotal = 0;

// Affichage
$currentBasketProducts = array();
if( isset( $_CADDIE ) and is_a( $_CADDIE, 'caddie' ) ):
	$keys = $_CADDIE->keysProduct();
	if( !empty( $keys ) ):
	
		// Toutes les collections
		$categoriesInit = array();
		$query = 'SELECT `id`, `collection` FROM `collections` ORDER BY `collection` ASC';
		$result = mysql_query( $query );
		if( $result ) {
			while( $rs = mysql_fetch_assoc( $result ) ) {
				$categoriesInit[] = $rs;	
			}
			mysql_free_result( $result );
		}
		unset( $result, $rs, $query );	
		
		// Produits
		$productsInit = array();
        $nbarticles=0;
		$query = 'SELECT `p`.`idproduit` as `id`, `p`.`id_collection`, `p`.`titre`, `p`.`isbn`, `p`.`ean13`,`p`.`prix_euro_ttc` as `price`, `p`.`support`  
						FROM `produits` as `p` 
						WHERE `p`.`idproduit` IN ( ' . implode( ', ', $keys ) . ' )  ORDER BY `p`.`titre` ASC
				';	
		$result = mysql_query( $query );
		if( $result ):
			while( $rs = mysql_fetch_assoc( $result ) ):
				$currentInfoBasketProduct = $_CADDIE->getProduct( $rs['id'] );
				// prix
				$sumBasketTTC += $rs['price'] * $currentInfoBasketProduct->quantity;
				//sk - 2008-03-03 - TVA - 19.6% pour les documents num�riques et 5.5% pour les livres classiques
				if (in_array($rs['support'], $numericDocuments ) ) {
					// $sumTva['19.6'] +=  $currentInfoBasketProduct->quantity * ( $rs['price']  - ( $rs['price']  / 1.196 ) ) ;
                    // Ecomiz GMV 20120111 : Changement de la TVA : 19,6 -> 7%
                    $sumTva['7'] +=  $currentInfoBasketProduct->quantity * ( $rs['price']  - ( $rs['price']  / 1.07 ) ) ;
				}
				else {
					// Remise de 5% uniquement pour les "livres"
					$sumReduction += round($currentInfoBasketProduct->quantity * $rs['price'] * $tauxReduction,2);
					$applyPort = true;
                    
                    $sumTva['5.5'] +=  $currentInfoBasketProduct->quantity * ( $rs['price']  - ( $rs['price']  / 1.055 ) ) ;
					
                    $nbarticles+=$currentInfoBasketProduct->quantity;				
				}
				$productsInit[] = $rs;				
			endwhile;
			mysql_free_result( $result );
		endif;
		unset( $result, $rs, $query, $currentInfoBasketProduct );
		
		// Gestionnaire de produits
		$products = new products( $productsInit );
		unset( $productsInit );
		$currentBasketProducts = $products->getAll();
		
		// Nettoyage du panier
		if( !empty( $currentBasketProducts ) ):
			foreach( $currentBasketProducts as $product ):
				if( $_CADDIE->getProduct( $product->id ) === null ):
					$_CADDIE->deleteProduct( $product->id );
				endif;
			endforeach;
		else:
			$_CADDIE->deleteAllProducts();
            promocodes::deletePromoCodeSession();
		endif;
	else:
		unset( $_SESSION['reduction'], $_SESSION['lservice'] );
	endif;
		
	
	// Remise de 5%
	//$sumReduction = $sumBasketTTC * $tauxReduction;
	$sumTva['5.5'] = round($sumTva['5.5'] - ( $sumTva['5.5'] * $tauxReduction), 2);
    $sumTva['19.6'] = round($sumTva['19.6'], 2);
    $sumTva['7'] = round($sumTva['7'], 2);
	// pas de remise sur les produits virtuel (taux de TVA 19.6%)
	//$sumTva['19.6'] = $sumTva['19.6'] - ($sumTva['19.6'] * $tauxReduction);
	
	// sk - 2008-01-11 suppression des FdP gratuits
	// sk - 2008-09-19 - Suite � la demande du client, r�application du franco de port.	
    // sk - 2008-03-03 - Ne pas appliquer les fdp pour une commande avec uniquement des produits num�riques
    // jp - 2005-05-07 - Suite � la demande du client, application du franco de port en France uniquement, ajout port sur devis si + 10 prod � l'etranger.
    $portSurDevis=false;
	if ( $applyPort === true ) {	
		if( $isportFrance ) {
            if ( $sumBasketTTC - $sumReduction < $sumPortFree) {
				$sumPort = $portFrance;
			}
        } else {
            if ($nbarticles>10) {
                $portSurDevis=true;
            }else {
				$sumPort = $portOthers;
			}
		}
	}	
	// ne pas calculer les frais de port dans la TVA 19.6%
	//$sumTva['5.5'] = $sumBasketTTC -  ( $sumBasketTTC / 1.055 );
	//$sumTva['19.6'] = $sumPort -  ( $sumPort / 1.196 );
	// Total TTC
	$sumTotal = $sumBasketTTC - $sumReduction + $sumPort;

	// Client
	$client = null;
	$query = '
		SELECT 
			`c`.`id`, `c`.`numcli`,`c`.`civilite`,`c`.`nom`, `c`.`prenom`,`c`.`email`, `c`.`lieufactchoice`, `c`.`lieufact`,  `c`.`rue1fact`,`c`.`rue1factbis`, `c`.`rue2fact`,`c`.`rue3fact`, `c`.`cpfact`, `c`.`villefact`, `c`.`idpaysfact`,
			`p`.`pays_fr` AS `pays` 
		FROM `clients` AS `c`
			LEFT OUTER JOIN `pays` AS `p` ON `p`.`idpays`=`c`.`idpaysfact`
		WHERE `c`.`id`=\'' . mysql_real_escape_string( (int)$_SESSION['id'] ) . '\'
	';
	$result = mysql_query( $query ) or die(mysql_error());
	if( $result ):
		$client = mysql_fetch_assoc( $result );
		mysql_free_result( $result );	
	endif;

    // R�cup�ration du nom du pays de livraison / facturation
	$query = 'SELECT `pays_fr` FROM `pays` WHERE `idpays`=\'' . mysql_real_escape_string( $_SESSION['delivery']['idpays'] ) . '\'';
	$result = mysql_query( $query );
	$_SESSION['delivery']['pays'] = '';
	if( $result ):
		$deliveryCountry = mysql_fetch_assoc( $result );
		mysql_free_result( $result );
		unset( $result );
		$_SESSION['delivery']['pays'] = $deliveryCountry['pays_fr'];
	endif;
    
	// Adresse de facturation 
	$adresseFacturation = '<span class="blue bold">' .$civilites[$_SESSION['delivery']['civilite'] ] . ' ' . $_SESSION['delivery']['prenom'] . ' ' . $_SESSION['delivery']['nom'] .'</span>'. chr( 10 );
	if( !empty( $_SESSION['delivery']['numcli'] ) ):
		$adresseFacturation .= '<br/><i>(N� ' . $_SESSION['delivery']['numcli'] . ')</i><br/>' . chr( 10 );
	endif;
    if (isset($_SESSION['delivery']) && ($_SESSION['delivery']['choix_facturation'] == 1)) {
        // Livraison � l'�tablissement
        $adresseFacturation .= $_SESSION['delivery']['lieu'].chr(10);
        $adresseFacturation .= $_SESSION['delivery']['rue2'].' '.$_SESSION['delivery']['rue3'].chr(10);
    } else {
        // Livraison au domicile du particulier
        $rueBis = '';
        if (isset($numadresses[$_SESSION['delivery']['rue1bis']])) {
            $rueBis = $numadresses[$_SESSION['delivery']['rue1bis']];
        }
        $adresseFacturation .= $_SESSION['delivery']['rue1'].' '.$rueBis.' '.$_SESSION['delivery']['rue2'].' '.$_SESSION['delivery']['rue3'].chr(10);
    }
    /*
	if( !empty( $_SESSION['delivery']['lieu'] ) ):
		$adresseFacturation .=  $_SESSION['delivery']['lieu'];
        if (!empty($_SESSION['delivery']['rue3'])) {
            $adresseFacturation .= trim($_SESSION['delivery']['rue3']);
        }
        $adresseFacturation .= chr(10);
	endif;	
	if( !empty( $_SESSION['delivery']['rue1'] ) ):
		$adresseFacturation .= $_SESSION['delivery']['rue1'] ;
		if( !empty( $_SESSION['delivery']['rue1bis'] ) ):
			$adresseFacturation .= ' ' . $numadresses[$_SESSION['delivery']['rue1bis']] ;
		endif;
	$adresseFacturation .= chr( 10 );
	endif;
    if (!empty($_SESSION['delivery']['rue2'])) {
        $adresseFacturation .= trim($_SESSION['delivery']['rue2']).chr(10);
    }
    */
	$adresseFacturation .= trim( $_SESSION['delivery']['codepostal'] . ' ' . $_SESSION['delivery']['ville'] ) . chr( 10 );
    $adresseFacturation .= $_SESSION['delivery']['pays'];

    // Adresse de livraison
	$adresseLivraison = '<span class="blue bold"> ' . $civilites  [ $_SESSION['delivery']['civilite'] ] . ' ' . $_SESSION['delivery']['prenom'] . ' ' .  $_SESSION['delivery']['nom'] .'</span>'. chr( 10 );
	if( !empty( $_SESSION['delivery']['numcli'] ) ):
		$adresseLivraison .= '<br/><i>(N� ' . $_SESSION['delivery']['numcli'] . ')</i><br/>' . chr( 10 );
	endif;
    if (isset($_SESSION['delivery']) && ($_SESSION['delivery']['choix_facturation'] == 1)) {
        // Livraison � l'�tablissement
        $adresseLivraison .= $_SESSION['delivery']['lieu'].chr(10);
        $adresseLivraison .= $_SESSION['delivery']['rue2'].' '.$_SESSION['delivery']['rue3'].chr(10);
    } else {
        // Livraison au domicile du particulier
        $rueBis = '';
        if (isset($numadresses[$_SESSION['delivery']['rue1bis']])) {
            $rueBis = $numadresses[$_SESSION['delivery']['rue1bis']];
        }
        $adresseLivraison .= $_SESSION['delivery']['rue1'].' '.$rueBis.' '.$_SESSION['delivery']['rue2'].' '.$_SESSION['delivery']['rue3'].chr(10);
    }
    /*
	if( !empty( $_SESSION['delivery']['lieu'] ) ):
		$adresseLivraison .= $_SESSION['delivery']['lieu'];
        if (!empty($_SESSION['delivery']['rue3'])) {
            $adresseLivraison .= trim($_SESSION['delivery']['rue3']);
        }
        $adresseLivraison .= chr(10);
	endif;	
	if( !empty( $_SESSION['delivery']['rue1'] ) ):
		$adresseLivraison .= $_SESSION['delivery']['rue1'];
		if( !empty( $_SESSION['delivery']['rue1bis'] ) ):
			$adresseLivraison .= ' ' . $numadresses [$_SESSION['delivery']['rue1bis']] ;
		endif;
        $adresseLivraison .= chr( 10 );
	endif;
    if (!empty($_SESSION['delivery']['rue2'])) {
        $adresseLivraison .= trim($_SESSION['delivery']['rue2']).chr(10);
    }
    */
	$adresseLivraison .= trim( $_SESSION['delivery']['codepostal'] . ' ' .  $_SESSION['delivery']['ville'] ) . chr( 10 );
	$adresseLivraison .=  $_SESSION['delivery']['pays'];
endif;

if ( !empty( $messages ) ) {
	$_POST['typecarte'] = '';
	$_POST['numcarte'] = '';
	$_POST['datecarte'] = '';
	$_POST['codecarte'] = '';			
}
?>
<!-- EcomiZ 2010-03-11: Integration des nouveaux style -->
<div id="contenu-1col"  >
<div id="colone-g-1col"  > 
<?php $oInterface->Get_ComLinks(0); ?>
<div id="breadcrump"><a href="/">Accueil</a> > <?php echo $aTemplateInfos["template_title"]; ?></div>
<div id="bloc-page">
<div id="bloc-page-titre"><h1>COMMANDE</h1></div>

<!-- CONTENU DEDIE -->
<div id="main">
            <!-- FIL D'ARIANE CMDE -->        
            <div id="fil_cmde">
            	<div id="fil_titre"><img src="images/panier/filCmdeTitre.gif" alt="Votre commande" width="178" height="31" border="0" /></div>
                <div id="fil_step1"><img src="images/panier/filCmdeStep1Off.gif" alt="Panier" width="136" height="31" border="0" /></div>
                <div id="fil_step2"><img src="images/panier/filCmdeStep2Off.gif" alt="Authentification" width="156" height="31" border="0" /></div>
                <div id="fil_step3"><img src="images/panier/filCmdeStep3Off.gif" alt="Livraison" width="156" height="31" border="0" /></div>
                <div id="fil_step4"><img src="images/panier/filCmdeStep4On.gif" alt="Paiement" width="156" height="31" border="0" /></div>
            </div>

            <?php
                if (is_array($messages)) {
                    foreach ($messages as $k => $v) {
                        echo '<div id="boite_alerte_rouge"><img src="images/panier/pictoBoiteAlerteRouge.gif" width="16" height="16" border="0" align="texttop" />'.$v.'</div>';
                    }
                }
                // debug($_SESSION['delivery'], '$_SESSION[\'delivery\']');
            ?>

            <!-- BOITES -->
                <form name="formulaireCB" method="post">
    			<input type="hidden" name="traitement" value="5">
                <input type="hidden" name="register" value="1"/>
                <input type="hidden" name="confirm" value="1"/>
                <div id="zone_facturation" style="width: 450px;">
                    <div id="boite_facturation">
                          <div id="facturation_contenu" style="width: 418px;">
                                <div id="titre">Vous payez par carte bancaire</div>
                                <div id="fil"><img src="images/panier/1x1.gif" width="1" height="1" border="0" /></div>
                                <div id="accroche">Notre module de commande en ligne est <strong>s�curis�</strong>.<br />Vos donn�es personnelles sont crypt�es et transmises directement � nos services de vente par correspondance.</div>
                                <div id="libelleChampCB" style="margin-top:15px">Type de carte</div><div id="champCB" style="width: 250px;"><input name="typecarte" type="radio" value="<?= htmlentities('Carte Bleue') ?>" /><a href="#" onclick="document.formulaireCB.typecb[0].checked=true;"><img src="images/panier/logoCB.gif" width="49" height="30" border="0" /></a>&nbsp;<input name="typecarte" type="radio" value="<?= htmlentities('Carte Visa') ?>" /><a href="#" onclick="document.formulaireCB.typecb[1].checked=true;"><img src="images/panier/logoVisa.gif" width="49" height="30" border="0" /></a>&nbsp;<input name="typecarte" type="radio" value="<?= htmlentities('MasterCard') ?>" /><a href="#" onclick="document.formulaireCB.typecb[2].checked=true;"><img src="images/panier/logoMasterCard.gif" width="49" height="30" border="0" /></a></div>
                                <div id="libelleChampCB">Num&eacute;ro de la carte</div><div id="champCB"><input name="numcarte" id="numcarte" maxlength="16" type="text" class="inputTextCBNumero" /></div>
                                <div id="libelleChampCB">Date d'expiration</div>
                        		<div id="champCB">
                                    <select name="moiscb" type="text" class="selectTextCBMois">
                                        <option value=""></option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option>
                                    </select>
                                    &nbsp;
                                    <select name="anneecb" type="text" class="selectTextCBAnnee">
                                        <option value=""></option>
                                        <?php
                                            for ($i = date('Y'); $i <= date('Y') + 10; $i++) {
                                                echo '<option value="'.$i.'">'.$i.'</option>';
                                            }
                                        ?>
                                    </select>
								</div>
                                <div id="libelleChampCB">Num&eacute;ro de contr&ocirc;le</div><div id="champCB" style="width: 280px;"><input name="codecarte" id="codecarte" maxlength="3" type="text" class="inputTextCBNumCtrl" /><span style="font-size: 11px; margin-left: 5px;">(les 3 derniers chiffres au dos de la carte)</span></div>
                                <div id="libelleChampCB"> </div><div id="champCB"><br /><input name="submit" type="image" src="images/panier/btValider.gif" onclick="return verifFormCB()" /></div>
                              </div>
                    </div>
                </div>
                </form>
                <?php
                    // Si au moins un exemplaire num�rique dans le panier, on ne propose pas le ch�que
                    $numericElmInBasket = false;
                    if (is_array($_CADDIE->caddieProduct)) {
                        $arrayNumericSupports = array('PDF � t�l�charger', 'Module e-learning','E-Pub � t�l�charger');
                        foreach ($_CADDIE->caddieProduct as $key => $value) {
                            if (in_array($value->support, $arrayNumericSupports)) {
                                $numericElmInBasket = true;
                            }
                        }
                    }
                    
                    if (!$numericElmInBasket) {
                ?>
                <form name="formulaireCHQ" method="post">
    			<input type="hidden" name="traitement" value="40"/>
                <input type="hidden" name="mp" value="C"/>
                <input type="hidden" name="register" value="1"/>
                <input type="hidden" name="confirm" value="1"/>
                <div id="zone_livraison">
                    <div id="boite_livraison">
                        <div id="livraison_contenu">
                            <div id="titre">Vous payez par ch&egrave;que</div>
                            <div id="fil"><img src="images/panier/1x1.gif" width="1" height="1" border="0" /></div>
                            <div id="accroche">Imprimez votre bon de commande, et envoyez-le par courrier, accompagn� de votre ch�que libell� � l'ordre d'<strong>Interforum</strong> � :</div>
                            <div id="adresseCheque">
                                    Relation Clients Retz<br />Autorisation n� 94089<br />77217 Avon Cedex<br />
                                <br />
                                <input name="submit" type="image" src="images/panier/btValider.gif" onClick="return verifForm(formulaire);">
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <?php
                    }
                ?>
</div>
<p>&nbsp;</p>
<!-- Fin div main -->

<?php
    /*
?>
<p style="text-align:right; font-size:10px;">Relation Clients RETZ - autorisation 94089 - 77219 AVON CEDEX 09 </p>
 <p>&nbsp;</p>
<div id="panier-option">
<p><img src="images/suivi_D.png" width="850" height="40" alt="Suivi-panier" /></p>
</div>



<?php if( !empty($messages) ) {?>
<ul class="message">
	<?php foreach( $messages as $message ): ?>
	<li style="color: red;"><?php echo htmlspecialchars( $message ); ?></li>	
	<?php endforeach; ?>
</ul>
<?php } ?>
		
<div id="inscription-compte">
<div class="formu reca">
<h4 class="titre-corpo"><?php echo gettext( 'R�capitulatif d\'adresses' ); ?></h4>
<br/>

		<p class="part2" style="border:1px solid #CCC" >
    <strong> <?php echo gettext( 'Adresse de livraison' ); ?> :</strong><br/>
			<?php echo ( $adresseLivraison ) ; ?>
	</p> 
			
		
	<p class="part1" style="border:1px solid #CCC">
      <strong><?php echo gettext( 'Adresse de facturation' ); ?> :</strong><br/>
		<?php echo ( $adresseFacturation ) ; ?>
	</p>
	<br/>

	<h4 class="titre-corpo"><?php echo gettext( 'R�capitulatif de la commande' ); ?></h4>
	<br/>
	
	<p><strong>Mode de paiement : </strong><?php if( isset($_SESSION['payment']) and $_SESSION['payment'] == 'CB' ) : echo ' par carte bancaire'; else : echo 'par ch�que'; endif;?></p>
  <div id="basket">
	
	<table cellpadding="2" class="panier" width="100%"  cellspacing="0"  >
      <thead>
			 <tr>
			<th style="width:40%" ></th>
			<th style="width:10%"></th>
			<?php if ($affichetva) {?><th style="width:10%"></th><?php }?>
			<th style="width:20%" ></th>
			<th style="width:20%" ></th>
		</tr>
			<tr class="entete" style="height:10px"> 
            <th>D�signation</th>
           <th ><abbr title="Quantit�">Quantit�</abbr></th>
           <?php if ($affichetva) {?><th class="center" style="text-align:center">TVA</th><?php }?>
          <th style="text-align:center"><abbr title="Prix unitaire">Prix unit TTC</abbr></th>
          <th  style=" text-align:center" >Montant TTC</th>
          </tr>
		</thead>
		 <tfoot>
          <?php $baseRowSpan = 4;
			if ($sumReduction > 0) :
				$baseRowSpan++;
			endif;
        	// affichage TVA 19.6
				$baseRowSpan++;?>
          <tr> 
		   <?php if ($affichetva) {?><td style="border:none"></td><?php  }?>
		  	<td colspan="2" style="border:none"></td>
            <td colspan="2" class="right price" style="padding: 5px 0 5px 0px;"><strong><?php echo gettext( 'Sous total TTC' ); ?> :</strong>&nbsp;
           <strong><?php echo htmlspecialchars( sprintf( '%01.2f', $sumBasketTTC ) ); ?> Eur</strong>
			 </td>
          </tr>
		  <?php if ($sumReduction > 0 ) {?>
          <tr> 
		   <?php if ($affichetva) {?><td style="border:none"></td><?php  }?>
		  	<td colspan="2" style="border:none"></td>
            <td colspan="2" class="right price" style="padding: 5px 0 5px 0px;"><strong><?php echo gettext( 'Remise de 5%' ); ?>  :</strong>&nbsp;
           <strong><?php echo htmlspecialchars( sprintf( '%01.2f', $sumReduction ) ); ?> Eur</strong>
			 </td>
          </tr>
		 <?php  } ?>				
		<?php if ($sumPort > 0 ) {?>	
          <tr> 
		   <?php if ($affichetva) {?><td style="border:none"></td><?php  }?>
		  	<td colspan="2" style="border:none"></td>
            <td colspan="2" class="right price" style="padding: 5px 0 5px 0px;"><strong><?php echo gettext( 'Frais de livraison TTC' ); ?>  :</strong>&nbsp;
           <strong><?php echo htmlspecialchars( sprintf( '%01.2f', $sumPort ) ); ?> Eur</strong>
			 </td>
          </tr>					
		<?php  }elseif($portSurDevis) { ?>
          <tr>
          <?php if ($affichetva) {?><td style="border:none"></td><?php  }?>
            <td colspan="4" class="right price" align="right"><strong><?php echo $phrDevisPort;?></strong></td>
            <td colspan="1"> </td>
          </tr>
          <?php } ?>
          <tr> 
		   <?php if ($affichetva) {?><td style="border:none"></td><?php  }?>
		  	<td colspan="2" style="border:none"></td>
            <td colspan="2" class="total" style="padding: 5px 0 5px 0px;"><strong><?php echo gettext( 'Total TTC' ); ?> :</strong>&nbsp;
           <strong><?php echo htmlspecialchars( sprintf( '%01.2f', $sumTotal ) ); ?> Eur</strong>
			 </td>
          </tr>
		  <?php if ($affichetva and $sumTva['19.6'] > 0 ) {?>
          <tr>
		  <?php if ($affichetva) {?><td style="border:none"></td><?php  }?>
            <td colspan="2" style="border:none"></td>
            <td colspan="2"><span >Dont TVA � 19,6% :</span> &nbsp; <span ><?php echo htmlspecialchars( sprintf( '%01.2f', $sumTva['19.6'] ) ); ?> Eur</span></td>
          </tr>
		 <?php  }?>
		 <?php if ($affichetva and $sumTva['5.5'] > 0 ) {?>
          <tr>
		  <?php if ($affichetva) {?><td style="border:none"></td><?php  }?>
            <td colspan="2" style="border:none"></td>
            <td colspan="2"><span >Dont TVA � 5,5% :</span> &nbsp; <span ><?php echo htmlspecialchars( sprintf( '%01.2f', $sumTva['5.5'] ) ); ?> Eur</span></td>
          </tr>
		 <?php  }?>		  
        </tfoot>
			<tbody>
          <?php foreach( $currentBasketProducts as $basketProduct ): ?>
          	<?php $currentInfoBasketProduct = $_CADDIE->getProduct( $basketProduct->id ); ?>
          <tr> 
            <td style="padding: 0 0 0 2px "> 
            <a href="product-<?php echo htmlspecialchars( $basketProduct->ean13 ); ?>.html"><?php echo  $basketProduct->titre; ?></a>
			<br />ISBN : <?php echo htmlspecialchars( $basketProduct->isbn ); ?>			   
            </td>
            <td class="center">
				<input type="hidden" style="width:10px" name="basket[<?php echo htmlspecialchars( $basketProduct->id ); ?>][titre]" value="<?php echo htmlspecialchars( $basketProduct->titre ); ?>" /> 
				<input type="hidden" style="width:10px" name="basket[<?php echo htmlspecialchars( $basketProduct->id ); ?>][isbn]" value="<?php echo htmlspecialchars( $currentInfoBasketProduct->isbn ); ?>" />
				<input type="hidden" style="width:10px" name="basket[<?php echo htmlspecialchars( $basketProduct->id ); ?>][quantity]" value="<?php echo htmlspecialchars( $currentInfoBasketProduct->quantity ); ?>" />  
				<?php echo htmlspecialchars( $currentInfoBasketProduct->quantity ); ?>
            </td>
            <?php if ($affichetva) {
				if (in_array( $basketProduct->support, $numericDocuments ) ) { ?>
					<td class="center">19,6 %</td>
				<?php } else { ?>
					<td class="center">5,5 %</td>
				<?php } ?>	
			<?php } ?>
            <td class="center"><?php echo htmlspecialchars( sprintf( '%01.2f', $basketProduct->price  ) ); ?> Eur</td>
            <td class="center"><?php echo htmlspecialchars( sprintf( '%01.2f', ( $basketProduct->price * $currentInfoBasketProduct->quantity ) ) ); ?> Eur</td>
          </tr> 
          <?php endforeach; ?>
        </tbody>
	</table>
	
	</div>
	</div>
	<br/><br/>
	
<div>&nbsp;</div>
	

	
<br/>
	<form  action="order-confirm.html" method="post" class="formu">
	<h4 class="titre-corpo"><?php echo gettext( 'Confirmer la commande' ); ?></h4>
<br/>
				<p>
				<input style="border: none; float: left; width: 15px; margin: 0 5px 0 0px;" type="checkbox" name="confirm" id="confirm" /> 
				<p style="width:500px; margin:0; padding:0"><?php echo gettext( 'Je d�clare avoir pris connaissance des conditions g�n�rales de vente et les acceptes' ); ?>. <a class="colorM" href="cgv.html" target="_blank"><?php echo gettext( 'Lire les conditions g�n�rales de vente' ); ?></a>.</p>
				</p>
				
				<?php if( $_SESSION['payment'] == 'CB' ): ?>
					<br />
					
				
					<h4 class="titre-corpo">Paiement par carte bancaire <img src="images/illustr/mini_carte_bleue.gif" /> : </h4>
					<br/>
				
					<p align="right" style="width:60%; margin:0; padding:0">
						<label for="typecarte" class="lbleft">Type de carte * :</label>
						<select name="typecarte" id="typecarte" class="text">
							<option value=""></option>
							<?php foreach( $cbTypes as $key => $value ): ?>
							<option value="<?php echo htmlentities( $key ); ?>"<?php if( isset( $_POST, $_POST['typecarte'] ) and $_POST['typecarte'] == $key ) echo ' selected="selected"'; ?>><?php echo htmlentities( $value ); ?></option>
							<?php endforeach; ?>		
						</select>
					<br/><br/>
						<label for="numcarte" class="lbleft">N� de la carte * :</label>
						<input type="text" name="numcarte" id="numcarte" value="<?php if( isset( $_POST, $_POST['numcarte'] ) ): echo htmlentities( $_POST['numcarte'] ); endif; ?>" class="text" maxlength="16" /> <br/><i>(entre 13 et 16 chiffres)</i>
					<br/><br/>
						<label for="datecarte" class="lbleft">Date d'expiration * :</label>
						<input type="text" name="datecarte" id="datecarte" value="<?php if( isset( $_POST, $_POST['datecarte'] ) ): echo htmlentities( $_POST['datecarte'] ); endif; ?>" class="text" maxlength="5" /><br/><i>(MM/AA)</i>
					<br/><br/>
						<label for="codecarte" class="lbleft">Code de contr�le * :</label>
						<input type="text" name="codecarte" id="codecarte" value="<?php if( isset( $_POST, $_POST['codecarte'] ) ): echo htmlentities( $_POST['codecarte'] ); endif; ?>" class="text" maxlength="3" /> <br/><i>(les 3 derniers chiffres au dos de la carte)</i><br />
						
						Je vous autorise � d�biter mon compte de la somme de <?php echo htmlspecialchars( sprintf( '%01.2f', $sumTotal ) ); ?> Eur TTC<br/>
						</p>
						
					</p>	
				<?php endif; ?>

<br/>
		<p align="right" style="width:60%; margin:0; padding:0">
			<input type="submit" name="return" class="valid" value="<?php echo gettext( 'Retour' ); ?>" />
			
      <input name="register" type="submit" value="<?php echo gettext( 'Valider l\'achat' ); ?>" class="valid" />
		</p>
	</form>
	</div>
		<p style="text-align:right; font-size:10px">Retz Relation clients - Autorisation n�94089 - 77219 Avon cedex - France</p>	

<?php unset( $basketProducts, $basketProduct ); ?>
        <?php
            */
        ?>
</div>
</div>
</div>
<script type="text/javascript">
    function verifFormCB(){
        //contr�le du type de CB :
        if ((document.formulaireCB.typecarte[0].checked == false) && (document.formulaireCB.typecarte[1].checked == false) && (document.formulaireCB.typecarte[2].checked == false))
        {
            alert("Veuillez pr�ciser le type de votre carte bancaire, merci.");
            return false;
        }

        //contr�le de la chaine de caracteres du num�ro de CB :
        if ((isNaN(document.formulaireCB.numcarte.value) == true) || document.formulaireCB.numcarte.value.length == 0)
        {
            alert("Veuillez saisir un num�ro de carte bancaire valide, merci.");
            document.formulaireCB.numcarte.focus();
            return false;
        }

        //contr�le numero d'identification de carte
        if (document.formulaireCB.codecarte.value.length != 3 || isNaN(document.formulaireCB.codecarte.value) == true)
        {
            alert("Veuillez saisir un num�ro d'identification de carte valide, merci.");
            document.formulaireCB.codecarte.focus();
            return false;
        }
    }
</script>