<?php
//Constante ROOT à la racine du répertoire contenant les sources Ecomiz
	define( 'ROOT', dirname( __FILE__ ).'./../../');	
	$_CONST['path']["server"] = ROOT;
	global $_CONST ;
	//Fichier de configuration contenant tous les accès à la base et autres constante
	include_once(ROOT."/lib/config.inc.php");
	include_once(ROOT."/lib/functions.php");
	//Classe permettant l'execution et le traitement des requetes SQL
	include_once(ROOT."/lib/database.class.php");	
	
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);	
	$oDb->query("SET NAMES utf8"); 
	LoadConfig($_CONST); //load eco_config table IN $_CONST
	
if($_POST['page'])
	{
	$id_rubrique = mysql_real_escape_string($_POST['id_rubrique']);
	$page = $_POST['page'];
	$cur_page = $page;
	$page -= 1;
	$per_page = 6;
	$previous_btn = true;
	$next_btn = true;
	$first_btn = true;
	$last_btn = true;
	// $_POST['rubriqueid'] = 1;
	$start = $page * $per_page;
	// echo $start;
	$strSQLList = "	SELECT rubrique_titre, rubrique_description, article_id, article_titre, article_soustitre, article_position, article_date_add
						   FROM bor_rubrique a
						   LEFT JOIN bor_article b ON (b.rubrique_id = a.rubrique_id)						  
						   WHERE 		b.article_actif = 1
										AND b.rubrique_id = ".$id_rubrique."
										".($id_rubrique ==  $_CONST["ID_SOUTIENT_SCOLAIRE"]  ? "ORDER BY article_position ASC": "")."
										".($id_rubrique == $_CONST["ID_ACTUALITE"] ? "ORDER BY article_date_add DESC": "")."
										 LIMIT $start, $per_page
										";
						  // var_dump($strSQLList); 
						$strresult=$oDb->queryTab($strSQLList);	
						$msg = "";
						// var_dump($strresult);
						foreach($strresult as $iKeyProjets => $aProjetsInfos){
						
							$strImgProjets = "SELECT files_path FROM 
							eco_files 
							WHERE files_table_source = 'borarticle' 
							AND files_table_id_name='article_id' 
							AND files_field_name ='article_image' 
							AND files_table_id = '".$aProjetsInfos["article_id"]."'";
								// var_dump($strImgProjets);
								$aPathImg=$oDb->queryRow($strImgProjets);
								// var_dump($aPathImg);
								$strPathImg = $aPathImg["files_path"];
									if(empty($strPathImg)){
									$strPathImg = "../uploads/articles/img_article_defaut.jpg";
									}
											$msg .= '
											<a href="'.$_CONST['URL_ACCUEIL'].'rubrique-'.strToUrl($aProjetsInfos['rubrique_titre']).'-'.$aProjetsInfos['article_id'].'-'.strToUrl($aProjetsInfos['article_titre']).'.html" style="text-decoration:none;"; >
												<h2>'.$aProjetsInfos['article_titre'].'</h2>
											</a>
											<a href="'.$_CONST['URL_ACCUEIL'].'rubrique-'.strToUrl($aProjetsInfos['rubrique_titre']).'-'.$aProjetsInfos['article_id'].'-'.strToUrl($aProjetsInfos['article_titre']).'.html" style="text-decoration:none;"; >
												<img class="flottant" src="'.$strPathImg.'" alt="'.$aProjetsInfos['article_titre'].'" >
											</a> 
											<h3 class="accroche">'.$aProjetsInfos['article_soustitre'].'</h3>
											<a href="'.$_CONST['URL_ACCUEIL'].'rubrique-'.strToUrl($aProjetsInfos['rubrique_titre']).'-'.$aProjetsInfos['article_id'].'-'.strToUrl($aProjetsInfos['article_titre']).'.html" class="article_link">»</a>
											<div class="spacer"></div>
											<div class="separateur"></div>
											';
											}
		
/* --------------------------------------------- */
$query_pag_num = "SELECT COUNT(*) AS count FROM `bor_article` a  
			WHERE a.rubrique_id =".$id_rubrique." ";
$result_pag_num = mysql_query($query_pag_num);
$row = mysql_fetch_array($result_pag_num);
$count = $row['count'];
$no_of_paginations = ceil($count / $per_page);

/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
if ($cur_page >= 7) {
    $start_loop = $cur_page - 3;
    if ($no_of_paginations > $cur_page + 3)
        $end_loop = $cur_page + 3;
    else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
        $start_loop = $no_of_paginations - 6;
        $end_loop = $no_of_paginations;
    } else {
        $end_loop = $no_of_paginations;
    }
} else {
    $start_loop = 1;
    if ($no_of_paginations > 7)
        $end_loop = 7;
    else
        $end_loop = $no_of_paginations;
}
/* ----------------------------------------------------------------------------------------------------------- */
$msg .= "<div class='pagination'><ul>";

// FOR ENABLING THE FIRST BUTTON
// if ($first_btn && $cur_page > 1) {
    // $msg .= "<li p='1' class='active'>Première</li>";
// } else if ($first_btn) {
    // $msg .= "<li p='1' class='inactive'>Première</li>";
// }

// FOR ENABLING THE PREVIOUS BUTTON
if ($previous_btn && $cur_page > 1) {
    $pre = $cur_page - 1;
    $msg .= "<li p='$pre' class='active' rel='prev'>«</li>";
} else if ($previous_btn) {
    $msg .= "<li class='inactive'  rel='prev'>«</li>";
}
for ($i = $start_loop; $i <= $end_loop; $i++) {

    if ($cur_page == $i)
        $msg .= "<li p='$i' style='z-index: 2;color: #ff6c00;cursor: default;background-color: #ffdabe;' class='active'>{$i}</li>";
    else
        $msg .= "<li p='$i' class='active'>{$i}</li>";
}

// TO ENABLE THE NEXT BUTTON
if ($next_btn && $cur_page < $no_of_paginations) {
    $nex = $cur_page + 1;
    $msg .= "<li p='$nex' class='active'  rel='next'>»</li>";
} else if ($next_btn) {
    $msg .= "<li class='inactive'  rel='next'>»</li>";
}

// TO ENABLE THE END BUTTON
// if ($last_btn && $cur_page < $no_of_paginations) {
    // $msg .= "<li p='$no_of_paginations' class='active'>Dernière</li>";
// } else if ($last_btn) {
    // $msg .= "<li p='$no_of_paginations' class='inactive'>Dernière</li>";
// }

$msg = $msg . "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
echo $msg;
}




