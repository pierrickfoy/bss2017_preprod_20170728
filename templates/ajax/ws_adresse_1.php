<?php 
ini_set('display_errors', '1');
error_reporting(E_ALL);

session_start();
$api_mdp = "eds"; 
$api_wsdl = "http://www.interforum.fr/it-vpc/services/ValidationAdresseWebService?wsdl"; 

//Variables 
$strCP = $_GET['cp'];
$strPays = $_GET['pays'];


//On vide le champ erreur 

echo "jQuery('#label_error').html('');";
echo "jQuery('#div_error').hide();";
if(!empty($strCP)){
	echo "jQuery('#bloc_ville').html('<select class=\"inputgris\" name=\"facturation_ville\" id=\"facturation_ville\"></select>');";
	echo "jQuery('#label_error').html('Le code postal renseigné ne correspond pas au pays choisi.');";
	echo "jQuery('#facturation_ville').html('<option>Choisir votre ville</option>');";
	echo "jQuery('#facturation_ville').addClass('inputgris');";
	//echo "$('#facturation_adresse').addClass('inputgris');";
	echo "jQuery('#facturation_numero_voie').addClass('inputgris');";
	echo "jQuery('#facturation_type_voie').addClass('inputgris');";
	echo "jQuery('#facturation_nom_voie').addClass('inputgris');";
	echo "jQuery('#facturation_complement').addClass('inputgris');";
	echo "jQuery('#facturation_ville').attr('disabled','disabled');";
	//echo "$('#facturation_adresse').attr('disabled','disabled');";
	echo "jQuery('#facturation_numero_voie').attr('disabled','disabled');";
	echo "jQuery('#facturation_type_voie').attr('disabled','disabled');";
	echo "jQuery('#facturation_nom_voie').attr('disabled','disabled');";
	echo "jQuery('#facturation_complement').attr('disabled','disabled');";
	// echo $strPays; 
	//Vérification par rapport à l'id du pays  : 
	if(empty($strPays)){
		echo "$('#label_error').html('Veuillez sélectionner un pays.');";
	}else{
		if(in_array($strPays, array( 1, 140, 144, 159, 106, 122, 109, 218, 227, 216))){
			if(strlen($strCP)>=4){
				if($strPays == 1){//France
					$strPlageMin = 1000 ; 
					$strPlageMax = 95999; 
				}elseif($strPays == 140){//Guadeloupe
					$strPlageMin = 97100 ; 
					$strPlageMax = 97199; 
				}elseif($strPays == 144){//Martinique
					$strPlageMin = 97200 ; 
					$strPlageMax = 97299; 
				}elseif($strPays == 159){//Guyane
					$strPlageMin = 97300 ; 
					$strPlageMax = 97399; 
				}elseif($strPays == 106){//Réunion
					$strPlageMin = 97400 ; 
					$strPlageMax = 97499; 
				}elseif($strPays == 122){//st pierre
					$strPlageMin = 97500 ; 
					$strPlageMax = 97599; 
				}elseif($strPays == 109){//Mayotte
					$strPlageMin = 97600 ; 
					$strPlageMax = 97699; 
				}elseif($strPays == 218){//Wallis
					$strPlageMin = 98600 ; 
					$strPlageMax = 98699; 
				}elseif($strPays == 227){//polynesie
					$strPlageMin = 98700 ; 
					$strPlageMax = 98799; 
				}elseif($strPays == 216){//nouvelle caledonie
					$strPlageMin = 98800 ; 
					$strPlageMax = 98899; 
				}
				$iCode = (int)$strCP ; 
				if($iCode >= $strPlageMin && $iCode <= $strPlageMax){					
					$client = new SoapClient($api_wsdl);
					$aParams = array (
										"password" => $api_mdp, 
										"codePostal" => $strCP
									);  
					$result = $client->rechercheVillesParCodePostal($aParams);  
					echo "jQuery('#div_error').hide();";
					if(isset($result->ville->string) && !empty($result->ville->string)){
						echo "jQuery('#bloc_ville').html('<select class=\"inputgris\" name=\"facturation_ville\" id=\"facturation_ville\"></select>');";
						if(count($result->ville->string) > 1){
							$strOptions =""; 
							foreach($result->ville->string as $aVille)
								$strOptions .= '<option value="'.$aVille.'" '.( ( isset($_SESSION['shipping']['VILLELIVRAISON']) && $_SESSION['shipping']['VILLELIVRAISON']== $aVille) ? 'selected' : '').'>'.$aVille.'</option>';		
							echo "jQuery('#facturation_ville').html('".$strOptions."');";
						}else{
							echo "jQuery('#facturation_ville').html('<option value=\"".$result->ville->string."\">".$result->ville->string."</option>');";
							
						}
						
						echo "jQuery('#label_error').html('');";
						echo "jQuery('#facturation_ville').removeClass('inputgris');";
						//echo "$('#facturation_adresse').removeClass('inputgris');";
						echo "jQuery('#facturation_numero_voie').removeClass('inputgris');";
						echo "jQuery('#facturation_type_voie').removeClass('inputgris');";
						echo "jQuery('#facturation_nom_voie').removeClass('inputgris');";
						echo "jQuery('#facturation_complement').removeClass('inputgris');";
						echo "jQuery('#facturation_ville').removeAttr('disabled');";
						//echo "$('#facturation_adresse').removeAttr('disabled');";
						echo "jQuery('#facturation_numero_voie').removeAttr('disabled');";
						echo "jQuery('#facturation_type_voie').removeAttr('disabled');";
						echo "jQuery('#facturation_nom_voie').removeAttr('disabled');";
						echo "jQuery('#facturation_complement').removeAttr('disabled');";
						
					}else{
						echo "jQuery('#label_error').html('');";
						echo "jQuery('#bloc_ville').html('<input type=\"text\"  name=\"facturation_ville\" id=\"facturation_ville\"/>');";
						echo "jQuery('#facturation_ville').removeClass('inputgris');";
						//echo "$('#facturation_adresse').removeClass('inputgris');";
						echo "jQuery('#facturation_numero_voie').removeClass('inputgris');";
						echo "jQuery('#facturation_type_voie').removeClass('inputgris');";
						echo "jQuery('#facturation_nom_voie').removeClass('inputgris');";
						echo "jQuery('#facturation_complement').removeClass('inputgris');";
						echo "jQuery('#facturation_ville').removeAttr('disabled');";
						//echo "$('#facturation_adresse').removeAttr('disabled');";
						echo "jQuery('#facturation_numero_voie').removeAttr('disabled');";
						echo "jQuery('#facturation_type_voie').removeAttr('disabled');";
						echo "jQuery('#facturation_nom_voie').removeAttr('disabled');";
						echo "jQuery('#facturation_complement').removeAttr('disabled');";
						
						
					}
				}else{
					echo "jQuery('#div_error').show();";
					echo "jQuery('#bloc_ville').html('<select class=\"inputgris\" name=\"facturation_ville\" id=\"facturation_ville\"></select>');";
					echo "jQuery('#label_error').html('Le code postal renseigné ne correspond pas au pays choisi.');";
					echo "jQuery('#facturation_ville').html('<option>Choisir votre ville</option>');";
					echo "jQuery('#facturation_ville').addClass('inputgris');";
					//echo "$('#facturation_adresse').addClass('inputgris');";
					echo "jQuery('#facturation_complement').addClass('inputgris');";
					echo "jQuery('#facturation_numero_voie').addClass('inputgris');";
					echo "jQuery('#facturation_type_voie').addClass('inputgris');";
					echo "jQuery('#facturation_nom_voie').addClass('inputgris');";
					echo "jQuery('#facturation_ville').attr('disabled','disabled');";
					//echo "$('#facturation_adresse').attr('disabled','disabled');";
					echo "jQuery('#facturation_numero_voie').attr('disabled','disabled');";
					echo "jQuery('#facturation_type_voie').attr('disabled','disabled');";
					echo "jQuery('#facturation_nom_voie').attr('disabled','disabled');";
					echo "jQuery('#facturation_complement').attr('disabled','disabled');";
				}
			}
		}else{ // Pays autre.
			echo "jQuery('#label_error').html('');";
			echo "jQuery('#div_error').hide();";
			echo "jQuery('#bloc_ville').html('<input type=\"text\"  style=\"width:590px;height:36px;max-width: 100%;\# name=\"facturation_ville\" id=\"facturation_ville\"/>');";
			echo "jQuery('#facturation_ville').removeClass('inputgris');";
			//echo "$('#facturation_adresse').removeClass('inputgris');";
			echo "jQuery('#facturation_numero_voie').removeClass('inputgris');";
			echo "jQuery('#facturation_type_voie').removeClass('inputgris');";
			echo "jQuery('#facturation_nom_voie').removeClass('inputgris');";
			echo "jQuery('#facturation_complement').removeClass('inputgris');";
			echo "jQuery('#facturation_ville').removeAttr('disabled');";
			//echo "$('#facturation_adresse').removeAttr('disabled');";
			echo "jQuery('#facturation_numero_voie').removeAttr('disabled');";
			echo "jQuery('#facturation_type_voie').removeAttr('disabled');";
			echo "jQuery('#facturation_nom_voie').removeAttr('disabled');";
			echo "jQuery('#facturation_complement').removeAttr('disabled');";
		}
	}
}




exit; 








// var_dump($result);


?>