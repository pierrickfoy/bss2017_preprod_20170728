<?php 
    define( 'ROOT', dirname( dirname( __FILE__ ) ));
	$_CONST['path']["server"] = ROOT;
	
	include_once(ROOT."/lib/config.inc.php");
	include_once(ROOT."/lib/database.class.php");
	
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	$where = "";
	
	if(isset($_POST['classe'])){
		$return = array();
		/*$where = 'cp.classe_id ='.mysql_real_escape_string((int)$_POST['classe'][0]);
		$where .= ' AND mc.mc_dispo = 1';
		$where .= ' AND m.matiere_actif = 1';
		$json = $oDb->queryTab("
		SELECT mc.matiere_id, m.matiere_titre as mc_titre 
		FROM bor_matiere_classe mc
		INNER JOIN bor_matiere m ON (m.matiere_id = mc.matiere_id)
		INNER JOIN bor_classe_page cp ON (cp.classe_page_id = mc.classe_id)
		WHERE $where");*/
		
		/*$json = $oDb->queryTab("
		SELECT mc.matiere_id, m.matiere_titre as mc_titre 
		FROM bor_matiere_classe mc
		INNER JOIN bor_matiere m ON (m.matiere_id = mc.matiere_id)
		INNER JOIN bor_classe_page cp ON (cp.classe_page_id = mc.classe_id)
		WHERE cp.classe_id ='".mysql_real_escape_string((int)$_POST['classe'][0])."'
		AND mc.mc_dispo = 1
		AND m.matiere_actif = 1
		AND mc.matiere_id != 12
		AND mc.matiere_id != 14
		");*/
		$json = $oDb->queryTab("
		SELECT mc.matiere_id, m.matiere_titre as mc_titre 
		FROM bor_matiere_classe mc
		INNER JOIN bor_matiere m ON (m.matiere_id = mc.matiere_id)
		INNER JOIN bor_classe cp ON (cp.classe_id = mc.classe_id)
		WHERE cp.classe_id ='".mysql_real_escape_string((int)$_POST['classe'][0])."'
		AND mc.mc_dispo = 1
		AND m.matiere_actif = 1
		");
		/*foreach($json as $key =>$couple){
			if($couple['matiere_id'] == 11){
				$json[$key]['matiere_id'] = 4;
				$json[$key]['mc_titre'] = 'Histoire-G&eacute;ographie';
			}
			if($couple['matiere_id'] == 13){
				$json[$key]['matiere_id'] = 3;
				$json[$key]['mc_titre'] = 'Physique-Chimie';
			}
		}*/
		
		
		//pour remedier au problme d'encodeage utf8
		if($json){
		$i = 0;
			foreach($json as $data){
				$return[$i]['matiere_id'] = $data['matiere_id'];
				$return[$i]['mc_titre'] = utf8_encode($data['mc_titre']);
				$i++;
			}
		}
		echo  json_encode($return);
	}
	
	else return false;