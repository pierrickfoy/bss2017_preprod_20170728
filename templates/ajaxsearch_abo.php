<?php 
    define( 'ROOT', dirname( dirname( __FILE__ ) ));
	$_CONST['path']["server"] = ROOT;
	
	include_once(ROOT."/lib/config.inc.php");
	include_once(ROOT."/lib/database.class.php");
	
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	$where = "";
	
	if(isset($_POST['classe'])){
		$return = array();
		$where = 'mc.classe_id ='.mysql_real_escape_string((int)$_POST['classe']);
		$where .= ' AND mc.mc_dispo = 1' ;
		$json = $oDb->queryTab("SELECT mc.matiere_id, m.matiere_titre as mc_titre FROM bor_matiere_classe mc INNER JOIN bor_matiere m ON (m.matiere_id = mc.matiere_id) WHERE $where");
		//pour remedier au problme d'encodeage utf8
		if($json){
		$i = 0;
			foreach($json as $data){
				$return[$i]['matiere_id'] = $data['matiere_id'];
				$return[$i]['mc_titre'] = utf8_encode($data['mc_titre']);
				$i++;
			}
		}
		echo  json_encode($return);
	}
	
	else return false;