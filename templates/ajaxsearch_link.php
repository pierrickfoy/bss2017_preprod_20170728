<?php 
    define( 'ROOT', dirname( dirname( __FILE__ ) ));
	$_CONST['path']["server"] = ROOT;
	
	include_once(ROOT."/lib/config.inc.php");
	include_once(ROOT."/lib/functions.php");
	include_once(ROOT."/lib/database.class.php");
	
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	$where = "";
	
	// cas de la classe seule
	if(isset($_POST['classe']) && $_POST['classe'] != "Classe" && isset($_POST['matiere']) && $_POST['matiere'] == ""){
		$return = array();
		$json = $oDb->queryTab("
		SELECT *
		FROM bor_classe_page
		WHERE classe_id ='".mysql_real_escape_string((int)$_POST['classe'])."'
		");
		if($json){
		$i = 0;
			foreach($json as $data){
				$return[$i]['classe_url'] = $data['classe_page_url'];
				$i++;
			}
		}
		echo  json_encode($return);
	}
	// cas classe + matiere
	else if(isset($_POST['classe']) && $_POST['classe'] != "Classe" && isset($_POST['matiere']) && $_POST['matiere'] != ""){
		$json = $oDb->queryTab("
		SELECT *
		FROM bor_classe_page, bor_matiere_page
		WHERE classe_id ='".mysql_real_escape_string((int)$_POST['classe'])."'
		AND matiere_id ='".mysql_real_escape_string((int)$_POST['matiere'])."'
		");
		if($json){
		$i = 0;
			foreach($json as $data){
				$return[$i]['classe_url'] = $data['classe_page_url'];
				$return[$i]['matiere_url'] = str_replace('/','',$data['matiere_page_url']);
				$i++;
			}
		}
		echo  json_encode($return);
	}
	// cas matiere seule
	else if(isset($_POST['classe']) && $_POST['classe'] == "Classe" && isset($_POST['matiere']) && $_POST['matiere'] != ""){
		$json = $oDb->queryTab("
		SELECT *
		FROM bor_matiere_page
		WHERE matiere_id ='".mysql_real_escape_string((int)$_POST['matiere'])."'
		");
		if($json){
		$i = 0;
			foreach($json as $data){
				$return[$i]['matiere_url'] = $data['matiere_page_url'];
				$i++;
			}
		}
		echo  json_encode($return);
	}
	
	else return false;