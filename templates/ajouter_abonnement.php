<?php
if(session_id() == "")
	session_start();
define( 'ROOTWS', dirname( __FILE__ ).'/..');
	$_CONST['path']["server"] = ROOTWS;

	include_once(ROOTWS."/lib/config.inc.php");
	include_once(ROOTWS."/lib/database.class.php");
	include_once(ROOTWS."/lib/JoForm.class.php");
	include_once(ROOTWS."/lib/functions.php");
	include_once(ROOTWS."/lib/intyMailer.php");
	include_once(ROOTWS."/lib/abo.class.php");
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	$oDb->query("SET NAMES 'utf8'"); 
	LoadConfig($_CONST); //load eco_config table IN $_CONST

	$_SESSION["langs_id"] = 1;
	$_SESSION["langs_code"] = 'fr';


/************************************************************************************************/
/*						TRAITEMENT DE L'ACTION DU POST	: AJOUTER ABONNEMENT						*/
/************************************************************************************************/

if(isset($_POST) && count($_POST)){
	
	//Récupération de l'action pour savoir dans quelle situation on se trouve 
	if(isset($_POST['action']) && !empty($_POST['action']) && isset($_POST['idcommande']) && !empty($_POST['idcommande'])){

		$strAction = $_POST['action'] ;
		$iCommandeId = $_POST['idcommande'] ;

		//Vérification que la commande est bien lié à l'utilisateur en ligne 
		$strSql = "SELECT * FROM bor_commande c WHERE c.commande_id  = '".mysql_real_escape_string($iCommandeId)."' AND c.client_id = '".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."' AND commande_statut = 1"; 
		$aCommande = $oDb->queryRow($strSql); 

		if($oDb->rows > 0 ){	
			if($strAction == "ajouterabo"){

/************************************************************************************************/
/*										SI ACTION = AJOUTER ABO	:								*/
/************************************************************************************************/

				// récupère le produit initial de la commande
				$strSql = "SELECT f.formule_classe, p.de_id_yonix, de.de_engagement FROM bor_produit p 
							INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix) 
							INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
							WHERE p.produit_ean = ".$aCommande['produit_ean']."";
				$aInfosProduitInitial = $oDb->queryRow($strSql);
				$nb_enfant = $_POST['nb_enfant'];
				$donneeManquante = 1;
				$iClasseSelect = 'ajouter-classe-'.$_POST['idcommande'];

				// récupère l'ean réussite et le nom de la classe
				if($_POST[$iClasseSelect] != ""){
					$donneeManquante = 0;
					$strSql = "SELECT p.produit_ean FROM bor_produit p 
								INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix)
								INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
								WHERE c.classe_id = '".$_POST[$iClasseSelect]."'
								AND	f.formule_classe = 'reussite'
								AND p.de_id_yonix = '".$aInfosProduitInitial['de_id_yonix']."'";
					$strEan = $oDb->queryItem($strSql);
					
					$strSql = "SELECT classe_name FROM bor_classe c
								WHERE c.classe_id = ".$_POST[$iClasseSelect];
					$strClasseName = $oDb->queryItem($strSql);
				}

/************************************************************************************************/
/*						SI ON A TOUTES LES DONNÉES : ENVOI DE L'AJOUT							*/
/************************************************************************************************/

				if($donneeManquante != 1){

					if((isset($_SESSION['user']['IDCLIENTWEB']) && $_SESSION['user']['IDCLIENTWEB'] != "") &&
					(isset($aCommande["commande_num"]) && $aCommande["commande_num"] != "" ) &&
					(isset($_POST['ajout-enfant-'.$_POST['idcommande']]) && $_POST['ajout-enfant-'.$_POST['idcommande']] != "" ) &&
					(isset($strEan) && $strEan != "" ) &&
					(isset($strClasseName) && $strClasseName != "" )
					){

						// echo '<pre>';
						// var_dump($_SESSION['user']['IDCLIENTWEB']);
						// var_dump($aCommande["commande_num"]);
						// var_dump($_POST['ajout-enfant-'.$_POST['idcommande']]);
						// var_dump($aCommande['produit_ean']);
						// var_dump($strEan);
						// var_dump($strClasseName);
						// var_dump($aCommande);
						// var_dump($aInfosProduitInitial);
						// echo '</pre>';
						

						// envoi WS nouvel enfant
						$oAbo = new aboEditis();
						$aAbo = $oAbo->ajouterEnfantAbonnement($_SESSION['user']['IDCLIENTWEB'], $aCommande["commande_num"], $_POST['ajout-enfant-'.$_POST['idcommande']], $strEan, $strClasseName);

						// si envoi WS réussi, on insert en BDD
						if($aAbo['ws'] && isset($aAbo['oReponse']) && $aAbo['oReponse'] == "OK"){
							$bUpdate = true; 

							// insertion BDD nouvel enfant
							$iCommandeId = $_POST['idcommande'] ;
							$classeId = $_POST[$iClasseSelect];
							$matiere = 0 ; // multi matiere tribu
							$formuleId = 3 ; // tribu
							$duree = $aInfosProduitInitial['de_engagement'];
							$prenom = $_POST['ajout-enfant-'.$_POST['idcommande']]; // prenom
							$eanTribu = $aCommande['produit_ean']; // ean de la commande initiale
							$eanReussite = $strEan; // ean réussite du nouvel enfant
							
							$strSql = "INSERT INTO bor_commande_enfant( 
								commande_id, 
								classe_id,
								matiere_id,
								formule_id,
								de_id,
								enfant_prenom,
								enfant_ean, 
								enfant_ean_reussite 
							) VALUES (
								'".mysql_real_escape_string($iCommandeId)."',
								'".mysql_real_escape_string($classeId)."',
								'".mysql_real_escape_string($matiere)."',
								'".mysql_real_escape_string($formuleId)."',
								'".mysql_real_escape_string($duree)."',
								'".mysql_real_escape_string($prenom)."',
								'".mysql_real_escape_string($eanTribu)."',
								'".mysql_real_escape_string($eanReussite)."'
							)"; 
							$oDb->query($strSql);

							// messages 
							$_SESSION['bValidate'] = true;
							$_SESSION['strMessageValidate'] = "L'ajout de ".$prenom." à votre abonnement est pris en compte . Votre enfant à maintenant accès à la classe ".$strClasseName.".";
						}else{
							$_SESSION['bErreur'] = true;
							if($aAbo['code'] == 99){
								$_SESSION['strErreurCarte'] = "Merci de choisir la classe de l'enfant.";
							}else{
								$_SESSION['strErreurCarte'] = "Nous ne pouvons pas prendre en compte votre demande. Veuillez nous contacter au ".$_CONST['RC_TEL'];
							}
						}
					}

				}else{
					// erreur : manque une donnée
					$_SESSION['bErreur'] = true;
					$_SESSION['strErreurCarte'] = "Nous ne pouvons pas prendre en compte votre demande. Veuillez nous contacter au ".$_CONST['RC_TEL'].".";
				}
			}
			header('Location: /gerer-mon-abonnement.html');
		}
	}
}