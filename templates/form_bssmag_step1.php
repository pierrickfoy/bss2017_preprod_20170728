<?php
// redirection vers la home si partenariat = inactif
if($_SESSION['partenaire_activation']['partenariat_actif'] == 0)
	 echo "<script>document.location.href='".$_CONST["URL2"]."';</script>";
 
unset($_SESSION['cart']); 
unset($_SESSION['shipping']); 
unset($_SESSION['reglement']); 
unset($_SESSION['products']);
unset($_SESSION['paiement']);
unset($_SESSION['nb_enfant']);
unset($_SESSION['partenaire']);
unset($_SESSION["chx_cgu"]);
unset($_SESSION["code_activation"]);

if(isset($_POST["action"]) && $_POST["action"] == "step_1_1"){    
    
    if( !isset($_POST["code_activation"]) || empty($_POST["code_activation"]) || !preg_match("/^[0-9a-zA-Z-_]{1,21}$/", $_POST['code_activation']) ){
        $_SESSION['code_activation'] = $_POST["code_activation"] ;
        $bError = true; 
        $strStep = "step_1_1"; 
        $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
        $strError .= "Le code d’activation saisi n’est pas valide. Veuillez le vérifier et le ressaisir.";
        $strError .="</span></p>";
    }else{        
        $_SESSION['code_activation'] = $_POST["code_activation"] ;
        //Appel du web service pour vérifier le code d'activation testjm-RUS
        $strUrlWS = "https://www.e-interforum.com/intra/webservices/per/get_code_status.php?cm=BRD&code_avantage=".$_SESSION['code_activation'];
        try{
            $strXML = file_get_contents($strUrlWS); 
            if(!empty($strXML)){
                $doc = new DOMDocument();
                $doc->loadXML(($strXML));
                $strCodeErreur = $doc->getElementsByTagName('CODE_ERREUR')->item(0)->nodeValue;
				//var_dump($strCodeErreur);exit;
                if($strCodeErreur === "00"){
                    $aCodeActivation["CODE_ERREUR"] = $doc->getElementsByTagName('CODE_ERREUR')->item(0)->nodeValue;
                    $aCodeActivation["CODE_AVANTAGE"] = $doc->getElementsByTagName('CODE_AVANTAGE')->item(0)->nodeValue; 
                    $aCodeActivation["MAX_ACTIVATION"] = $doc->getElementsByTagName('CODE_AVANTAGE')->item(0)->getAttribute("max_activation"); 
                    $aCodeActivation["NB_ACTIVATION"] = $doc->getElementsByTagName('CODE_AVANTAGE')->item(0)->getAttribute("nb_activation");
                    $aCodeActivation["CODE_ACTION"] = $doc->getElementsByTagName('CODE_ACTION')->item(0)->nodeValue; 
                    $aCodeActivation["ORIGINE"] = $doc->getElementsByTagName('ORIGINE')->item(0)->nodeValue; 
                    $aCodeActivation["CODE_SITE"] = $doc->getElementsByTagName('CODE_SITE')->item(0)->nodeValue; 
                    $aCodeActivation["DATE"] = $doc->getElementsByTagName('DATE')->item(0)->nodeValue; 
                    $oRessources = $doc->getElementsByTagName('RESSOURCE');
                    //Vérification que la ressource existe 
                    $aCodeActivation["PRODUIT"] = array();
                    foreach( $oRessources as $oRessource ){
                        $strEAN = $oRessource->getElementsByTagName('EAN13')->item(0)->nodeValue;
                        //$strEAN = 3133097365374;
                        $strSql = "SELECT p.*, f.formule_libelle, f.formule_classe FROM bor_produit p INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) WHERE produit_ean = '".mysql_real_escape_string($strEAN)."' LIMIT 1"; 
                        $aProduit = $oDb->queryRow($strSql);
                       if($oDb->rows > 0){
                            $aCodeActivation["PRODUIT"] = $aProduit ;
                            $aCodeActivation["PRODUITS"][] = $aProduit ; // Modif Pf : ajout pour liste effective des produits pour lister uniquement les classes liées dans step2 
                           // break; // Modif Pf
                        }
                    }
                    /*if(count($aCodeActivation["PRODUIT"]) > 0 && $aCodeActivation["MAX_ACTIVATION"] > $aCodeActivation["NB_ACTIVATION"]){
                        $_SESSION['code_activation'] = $aCodeActivation;
						//var_dump($aCodeActivation["PRODUIT"]["formule_classe"]);exit;
                        //var_dump( $_SESSION['partenaire_activation']['code_activation']);var_dump($_SESSION['partenaire_activation']['code_activation']["PRODUIT"]['formule_classe']);exit;
                        echo "<script>document.location.href='".$_CONST["URL2"]."/bss-mag/".$_SESSION['partenaire_activation']["activation_url"]."/activation-formule-".$aCodeActivation["PRODUIT"]["formule_classe"]."-etape-2.html';</script>";
                        exit;  
                    }else{
                        $bError = true; 
                        $strStep = "step_1_1"; 
                        $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
                        $strError .= "Le code d’activation saisi n’est pas valide. Veuillez le vérifier et le ressaisir.";
                        $strError .="</span></p>";
                    }*/
					if(count($aCodeActivation["PRODUIT"]) > 0){
						if(isset($aCodeActivation["MAX_ACTIVATION"]) && $aCodeActivation["MAX_ACTIVATION"] != ""){
							if($aCodeActivation["MAX_ACTIVATION"] > $aCodeActivation["NB_ACTIVATION"]){
								$_SESSION['code_activation'] = $aCodeActivation;
								echo "<script>document.location.href='".$_CONST["URL2"]."/bss-mag/".$_SESSION['partenaire_activation']["activation_url"]."/activation-formule-".$aCodeActivation["PRODUIT"]["formule_classe"]."-etape-2.html';</script>";
								exit;
							}else{ 
								$bError = true; 
								$strStep = "step_1_1"; 
								$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
								$strError .= "Le code d’activation saisi n’est pas valide. Veuillez le vérifier et le ressaisir.";
								$strError .="</span></p>";	
							}
						}else{
							$_SESSION['code_activation'] = $aCodeActivation;
							echo "<script>document.location.href='".$_CONST["URL2"]."/bss-mag/".$_SESSION['partenaire_activation']["activation_url"]."/activation-formule-".$aCodeActivation["PRODUIT"]["formule_classe"]."-etape-2.html';</script>";
							exit;
						}
                    }else{
                        $bError = true; 
                        $strStep = "step_1_1"; 
                        $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
                        $strError .= "Le code d’activation saisi n’est pas valide. Veuillez le vérifier et le ressaisir.";
                        $strError .="</span></p>";
                    }
				}else if($strCodeErreur === "-1"){
                    $bError = true; 
                    $strStep = "step_1_1"; 
                    $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
                    $strError .= "Ce code d’activation saisi n’est pas valide. Veuillez le vérifier et le ressaisir.";
                    $strError .="</span></p>";
                }else if($strCodeErreur === "-01"){
                    $bError = true; 
                    $strStep = "step_1_1"; 
                    $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
                    $strError .= "Suite à un problème technique, ce code n’a pas pu être validé lors de son passage en caisse. Afin de résoudre le problème, nous vous invitons à nous contacter au ".$_CONST['RC_TEL']." ou par mail à <a href='mailto:soutien-scolaire@bordas.tm.fr'>soutien-scolaire@bordas.tm.fr</a> (du lundi au vendredi ".$_CONST['RC_HORAIRES'].").";
                    $strError .="</span></p>";
                }else if($strCodeErreur === "-04"){
                    $bError = true; 
                    $strStep = "step_1_1"; 
                    $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
                    $strError .= "Ce code a déjà été utilisé et ne peut donc plus fonctionner.";
                    $strError .="</span></p>";
                }else if($strCodeErreur === "-02"){
                    $bError = true; 
                    $strStep = "step_1_1"; 
                    $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
                    $strError .= "Ce code n’est plus valable. Veuillez vérifier la date de validité indiquée au dos du coffret, et vous adresser, si besoin, au point de vente où vous avez fait cet achat.";
                    $strError .="</span></p>";
                }else if($strCodeErreur === "-03"){
                    $bError = true; 
                    $strStep = "step_1_1"; 
                    $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
                    $strError .= "Ce code n'est pas encore valide suite à des problèmes de droits (Les abonnements ne sont pas encore paramétrés). Afin de résoudre le problème, nous vous invitons à nous contacter au ".$_CONST['RC_TEL']." ou par mail à <a href='mailto:soutien-scolaire@bordas.tm.fr'>soutien-scolaire@bordas.tm.fr</a> (du lundi au vendredi ".$_CONST['RC_HORAIRES'].").";
                    $strError .="</span></p>";
                /*}else if($strCodeErreur === "-05"){
                    $bError = true; 
                    $strStep = "step_1_1"; 
                    $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
                    $strError .= "Ce code n'a pas été activé en caisse et ne peut donc fonctionner.";
                    $strError .="</span></p>";
                }else if($strCodeErreur === "-06"){
                    $bError = true; 
                    $strStep = "step_1_1"; 
                    $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
                    $strError .= "Ce code a déjà été utilisé et ne peut donc plus fonctionner.";
                    $strError .="</span></p>";
                }else if($strCodeErreur === "-07"){
                    $bError = true; 
                    $strStep = "step_1_1"; 
                    $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
                    $strError .= "Ce code n’est plus valable. Veuillez vérifier la date de validité indiquée au dos du coffret, et vous adresser, si besoin, au point de vente où vous avez fait cet achat.";
                    $strError .="</span></p>";*/
                }else{
                    $bError = true; 
                    $strStep = "step_1_1"; 
                    $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
                    $strError .= "Le code d’activation saisi n’est pas valide. Veuillez le vérifier et le ressaisir.";
                    $strError .="</span></p>";
                }
            }
        }catch(Exception $e){
            $bError = true; 
            $strStep = "step_1_1"; 
            $strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
            $strError .= "Un problème technique empêche la vérification de votre code d'activation.";
            $strError .="</span></p>";
        }
    }
}
?>
<div class="container">
   <div class="row">
     
      <ul id="steps" class="col-md-12">
         <li class="detail_formule"><a class="active"><strong>1.</strong>Saisie du code d'activation<span class="arrow_right"></span></a></li>
         <li class="confirmation"><a  ><span class="arrow_left"></span><strong>2.</strong>Création de mon compte <span class="arrow_right"></span></a></li>
         <li class="paiement"><a ><span class="arrow_left"></span><strong>3.</strong>Confirmation <span class="arrow_right"></span></a></li>
      </ul>
   </div>
    <div class="row">
    <div class="col-sm-12" style="text-align: center;">
        <?php 
            $strPadding = ''; 
            if(isset($_SESSION['partenaire_activation']["activation_texte"]) && !empty($_SESSION['partenaire_activation']["activation_texte"])){
                echo "<p>".$_SESSION['partenaire_activation']["activation_texte"]."</p>"; 
                $strPadding = 'style="padding-top: 0;"';
            }                
        ?>
        <!--<h2 class="activation_description" <?php echo $strPadding ; ?>>Entrez votre code d'activation pour bénéficier de l'abonnement à la plateforme de soutien scolaire</h2>-->
    </div>
   </div>
   
   <div class="detail_formule">
      <div class="title_formule">
         <h2>
			Je saisis mon code d'activation...
         </h2>
      </div>
      <div class="body_formule activation_blue_sky" style="margin-bottom:40px;">
		         
		 <?php 
		 if($strStep =="step_1_1" && $bError){
			echo ' <div class="alert alert-danger">
				'.$strError.'
			 </div>';
		 }
		 ?>
		   <div class="activation_accroche">
			<p class="bold">...pour bénéficier de l'abonnement à la plateforme de soutien scolaire</p>
		   </div>
		<?php
			echo '	<form class="form-horizontal" role="form" method="post" id="form_activation" name="form_activation"> 
                                    <div class="form-group">
                                        <div class="col-sm-1 activation_col2" style="">
                                        </div>
                                        <div class="col-sm-3 " style="">
                                            <label style="" for="inputEmail3" class="control-label activation_label">Code d\'activation * : </label>
                                        </div>
                                        <div class="col-sm-6" style="max-width: 315px ! important;">
                                            <input name="code_activation" id="code_activation" maxlength="22" type="text"  class="form-control"  placeholder="" value="'.((isset($_SESSION["code_activation"]) && !empty($_SESSION["code_activation"])) ? $_SESSION["code_activation"] : "" ).'" style="">
                                        </div>';?>
										<?php if ($_SESSION['partenaire_activation']["ou_trouver_code"] == 1 ) { 
											if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){ ?>
												<div class="col-sm-4 lien_isbn"><a data-target="#popupisbn" data-toggle="modal" href="#" onclick="ga('send', 'event','lien hypertexte', 'clic', 'Etape 1 – Bloc : Je saisis mon code d’activation – Où trouver le code d\’activation ? | BSS MAG', 4)">Où trouver le code d'activation ?</a></div>
											<?php } else {?>
												<div class="col-sm-4 lien_isbn"><a data-target="#popupisbn" data-toggle="modal" href="#">Où trouver le code d'activation ?</a></div>
											<?php } 		
										} ?>
		<?php    echo '                        </div>
						<div class="form-group">
						   <div class="activation-center" >'; 
				if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
					?>
						<a href="#" onclick="ga('send', 'event','bouton', 'clic', 'Etape 1 - Bloc : Je saisis mon code d\'activation - VALIDER | BSS MAG', 4);jQuery('#form_activation').submit();" class="btn btn-orange"  style="margin-top:25px;">Valider >></a>
					<?php
				}else{
					?>
						<a href="#" onclick="jQuery('#form_activation').submit();" class="btn btn-orange"  style="margin-top:25px;">Valider >></a>
					<?php
				}
				echo' </div>
						</div>
						<input type="hidden" name="action" value="step_1_1"/>
					</form>'; 
			
		 ?>
         
      </div>
      
      
   </div>
</div>

<!-- bloc j'ai déjà activé mon code -->

<div class="detail_formule ">
	<div class="activation_h2">
         <h2>
			J'ai déjà activé mon code lors de ma première visite
         </h2>
	</div>
      <div class="body_formule activation_blue_sky2">
		  <div class="activation_accroche">
			<p>J'accède directement à la plateforme de Soutien Scolaire</p>
		  </div>
		<div class="activation-center">
		<?php
		if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
			?>
				<a class="btn btn-blue btn-offre" target="_blank" onclick="ga('send', 'event','bouton', 'clic', 'Etape 1 - Bloc : J\’accède à la plateforme de soutien scolaire <?php echo $_SESSION['partenaire_activation']['bill_codeaction']; ?> – JE ME CONNECTE | BSS MAG', 4)" href="<?php echo $_SESSION['partenaire_activation']['activation_plateforme']; ?>">Je me connecte >></a>
			<?php
		}else{
			?>
				<a class="btn btn-blue btn-offre" target="_blank" href="<?php echo $_SESSION['partenaire_activation']['activation_plateforme']; ?>">Je me connecte >></a>
			<?php
		}
		?>
		</div>
      </div>
   </div>
</div>

<!-- Popup où trouver le code d'activation -->
 
<div class="modal fade" id="popupisbn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
		<div class="center">
			 <?php 
				$strSql = "SELECT files_path FROM  eco_files WHERE files_field_name = 'image_ou_trouver_code' AND files_table_source='bor_bssmag' AND files_table_id= '".$_SESSION['partenaire_activation']["activation_id"]."'";
				$strPathImage = $oDb->queryItem($strSql);
				if($strPathImage)
				echo '<img class="img-responsive" src="'.str_replace('../','../../',$strPathImage).'" alt="Où trouver mon code d\'activation">'; 
			?>
		</div>
      </div>
    </div>
  </div>
</div>