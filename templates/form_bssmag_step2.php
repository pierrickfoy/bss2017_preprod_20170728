<script type="text/javascript">
    jQuery.noConflict();
</script>

<?php
// echo 'test' ; 
 // error_reporting(E_ALL);
// ini_set('error_reporting', E_ALL);
 // ini_set('display_errors', '1');
//On verifie que la formule existe  

if(!isset($_SESSION['code_activation']["PRODUIT"]['formule_classe']) || empty($_SESSION['code_activation']["PRODUIT"]['formule_classe']) || !count($_SESSION['code_activation']["PRODUIT"]) ){
    echo "<script>document.location.href='".$_CONST["URL2"]."/bss-mag/".$_SESSION['partenaire_activation']["activation_url"]."/activation-formule-etape-1.html';</script>";
    exit; 
}
$bError = false ; 

$strSql = "SELECT * FROM bor_formule WHERE formule_classe = '".mysql_real_escape_string($_SESSION['code_activation']["PRODUIT"]['formule_classe'])."'"; 
$aFormule = $oDb->queryRow($strSql); 
$_SESSION['cart']['formule'] = $aFormule ; 
$strSql = "SELECT * FROM bor_duree_engagement WHERE de_id_yonix = '".mysql_real_escape_string($_SESSION['code_activation']["PRODUIT"]['de_id_yonix'])."'"; 
$aDureeEngagement = $oDb->queryRow($strSql); 
$_SESSION['cart']['dureeEngagement'] = $aDureeEngagement ; 
//var_dump($aFormule);
//var_dump($_SESSION['code_activation']["PRODUIT"]);
// $strSql = "SELECT * FROM bor_produit p INNER JOIN bor_duree_engagement d ON(d.de_id_yonix = p.de_id_yonix) INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) WHERE p.produit_ean = '".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["ean"])."'"; 
//$aProduit = $oDb->queryRow($strSql);
//var_dump($_SESSION["products"]["enfants"][0]);			
//var_dump($_SESSION['code_activation']["PRODUIT"]["produit_ean"]);
//var_dump($_SESSION['user']);
//var_dump($_CONST['shipping']['ADR1']);
			
/*GESTION DES FORMULAIRE */
//Formulaire 1 ciblee
if(isset($_POST["action"]) && $_POST["action"] == "step_1_1"){
	$bError = false ; 
	unset($_SESSION["products"]["enfants"]);
	$_SESSION["products"]["enfants"][0]["classe"] = $_POST["classe"][0]; 
	$_SESSION["products"]["enfants"][0]["D_PRENOM"] = $_POST["prenom"][0] ; 
	$_SESSION["products"]["enfants"][0]["matiere"] =  $_POST["matiere"][0] ; 
		
	//On se trouve dans une formule ciblee on doit donc vérifier que les champs enfant, classe et matière sont bien selectionné
	if(!isset($_POST["prenom"][0]) || empty($_POST["prenom"][0])){
		$bError = true; 
		$strStep = "step_1_1"; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Veuillez saisir le prénom de votre enfant.";
		$strError .="</span></p>";
	}else{
		//Initialisation du prénom de l'enfant
		if( !isset($_POST["classe"][0]) || empty($_POST["classe"][0]) ){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Veuillez saisir la classe de votre enfant.";
		$strError .="</span></p>";
		}else {
			if(   !isset($_POST["matiere"][0]) || empty($_POST["matiere"][0]) ) {
				
				$bError = true; 
				$strStep = "step_1_1"; 
				$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
				$strError .= "Veuillez saisir la matière de votre enfant.";
		$strError .="</span></p>";
			}else{
				
				//Prénom, matière et classe remplie on vérifi les donnée
				$strSql = "SELECT * FROM bor_matiere_classe mc INNER JOIN bor_classe c ON (c.classe_id = mc.classe_id) INNER JOIN bor_matiere m ON (m.matiere_id = mc.matiere_id)  WHERE mc.classe_id = '".mysql_real_escape_string($_POST["classe"][0])."' AND mc.matiere_id  = '".mysql_real_escape_string($_POST["matiere"][0])."' AND mc.mc_dispo = 1"; 
				$aInfosClasse = $oDb->queryRow($strSql); 
				if(!$oDb->rows){
					$bError = true; 
					$strStep = "step_1_1"; 
					$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
					$strError .= "Nous ne retrouvons pas la formule associée à votre sélection. Veuillez choisir un couple classe/matière valide.";
		$strError .="</span></p>";
				}else{
					$_SESSION["products"]["enfants"][0]["classe"] = $aInfosClasse["classe_id"]; 
					$_SESSION["products"]["enfants"][0]["matiere"] = $aInfosClasse["matiere_id"]; 
					
					$strStep = "step_1_2"; 
					$bStep1 = true; 
				}
			}
		}
	}	
}
//Formulaire 1 reussite
if(isset($_POST["action"]) && $_POST["action"] == "step_2_1"){
	unset($_SESSION["products"]["enfants"]);
	$_SESSION["products"]["enfants"][0]["classe"] = $_POST["classe"][0]; 
	$_SESSION["products"]["enfants"][0]["D_PRENOM"] = $_POST["prenom"][0] ; 
	//On se trouve dans une formule reussite on doit donc vérifier que les champs enfant, classe et matière sont bien selectionné
	if(!isset($_POST["prenom"][0]) || empty($_POST["prenom"][0])){
		
		$bError = true; 
		$strStep = "step_1_1"; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Veuillez saisir le prénom de votre enfant.";
		$strError .="</span></p>";
	}else{
		//Initialisation du prénom de l'enfant
		
		if( !isset($_POST["classe"][0]) || empty($_POST["classe"][0])  ){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Veuillez saisir la classe de votre enfant.";
		$strError .="</span></p>";
		}else{
			//Prénom, matière et classe remplie on vérifi les donnée
			$strSql = "SELECT * FROM bor_classe c  WHERE c.classe_id = '".mysql_real_escape_string($_POST["classe"][0])."'  LIMIT 1"; 
			$aInfosClasse = $oDb->queryRow($strSql); 
			if(!$oDb->rows){
				$bError = true; 
				$strStep = "step_1_1"; 
				$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
				$strError .= "Nous ne retrouvons pas la formule associée à votre sélection.";
		$strError .="</span></p>";
			}else{
				$_SESSION["products"]["enfants"][0]["classe"] = $_POST["classe"][0]; 
				$_SESSION["products"]["enfants"][0]["matiere"] = ''; 
				$strStep = "step_1_2"; 
				$bStep1 = true; 
			}
		}
	}	
}
//Formulaire 1 tribu
// $_POST['nbenfant']="";
// unset($_SESSION["nb_enfant"]);
// exit;
// var_dump($_POST['action']);
if(isset($_POST["action"]) && $_POST["action"] == "step_3_1"){
	unset($_SESSION["products"]["enfants"]);
	//On se trouve dans une formule reussite on doit donc vérifier que les champs enfant, classe et matière sont bien selectionné
	$iNbEnfant = $_POST['nbenfant'];
	
	
		$iPrenom = count($_POST['prenom']);
		$iClasse = count($_POST['classe']);
		if(  $iNbEnfant!= $iPrenom  ){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Veuillez saisir le prénom de vos enfants.";
		$strError .="</span></p>";
		}else if(  $iNbEnfant!= $iClasse  ){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Veuillez saisir la classe de vos enfants.";
		$strError .="</span></p>";
		}else{
			$bVerifEnfant = true;
			$bVerifClasse = true ;
			for($i = 0; $i < $iNbEnfant; $i++){
				$_SESSION["products"]["enfants"][$i]["classe"] = $_POST["classe"][$i];
				$_SESSION["products"]["enfants"][$i]["D_PRENOM"] = $_POST["prenom"][$i] ; 				
				if(empty($_POST['prenom'][$i]) )
					$bVerifEnfant = false ; 
				if( empty($_POST['classe'][$i]))
					$bVerifClasse = false ; 
			}
			if(!$bVerifEnfant || !$bVerifClasse){
				$bError = true; 
				$strStep = "step_1_1"; 
				if(!$bVerifEnfant ){
					$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
					$strError .= "Veuillez saisir le prénom de vos enfants.";
		$strError .="</span></p>";
				}if (!$bVerifClasse ){
					$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
					$strError .= "Veuillez saisir la classe de vos enfants.";
		$strError .="</span></p>";
				}
			}else{
				for($i = 0; $i < $iNbEnfant; $i++){
					$_SESSION["products"]["enfants"][$i]["D_PRENOM"] = $_POST["prenom"][$i] ; 
					$strSql = "SELECT * FROM bor_classe c  WHERE c.classe_id = '".mysql_real_escape_string($_POST["classe"][$i])."'  LIMIT 1"; 
					$aInfosClasse = $oDb->queryRow($strSql); 
					$_SESSION["products"]["enfants"][$i]["classe"] = $_POST["classe"][$i]; 
					$_SESSION["products"]["enfants"][$i]["matiere"] = ''; 
					$strStep = "step_1_2"; 
					$bStep1 = true; 
				}
			}
		}
	
}

//Formulaire 3 : je prouve que je suis résident
if(isset($_POST["action"]) && $_POST["action"] == "step_1_3"){
	if( (!isset($_POST['facturation_ville']) || empty($_POST['facturation_ville']))){
		$strStep = "step_1_3";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bVerif = false; 
		$bBlocFacturation = true;
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "La ville de facturation n'est pas valide."; 
		$strError .="</span></p>";
	}
	elseif( (!isset($_POST['facturation_numero_voie']) || empty($_POST['facturation_numero_voie']))){
		$strStep = "step_1_3";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bVerif = false; 
		$bBlocFacturation = true;
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Le numéro de voie n'est pas rempli.";
		$strError .="</span></p>"; 
	}
	elseif( !preg_match("/^[0-9A-Za-zàáâãäåçèéêëìíîïðòóôõöùúûüýÿÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÐÒÓÔÕÖÙÚÛÜÝŸ ]{1,10}$/", $_POST['facturation_numero_voie'])){
		$strStep = "step_1_3";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bVerif = false; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Le numéro de voie n'est pas valide. Il ne peut contenir que 10 caractères alphanumériques, sans ponctuation."; 
		$strError .="</span></p>";
	}
	elseif( (!isset($_POST['facturation_nom_voie']) || empty($_POST['facturation_nom_voie']))){
		$strStep = "step_1_3";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bVerif = false; 
		$bBlocFacturation = true;
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Le nom de voie n'est pas rempli.";
		$strError .="</span></p>"; 
	}
	elseif( !preg_match("/^[0-9A-Za-zàáâãäåçèéêëìíîïðòóôõöùúûüýÿÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÐÒÓÔÕÖÙÚÛÜÝŸ' ]{1,23}$/", $_POST['facturation_nom_voie'])){
		$strStep = "step_1_3";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bVerif = false; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Le nom de voie n'est pas valide. Il ne peut contenir que 22 caractères alphanumériques, sans ponctuation."; 
		$strError .="</span></p>";
	}
	elseif( (!isset($_POST['facturation_pays']) || empty($_POST['facturation_pays']))){
		$strStep = "step_1_3";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bVerif = false; 
		$bBlocFacturation = true;
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Le pays de facturation n'est pas valide."; 
		$strError .="</span></p>";
	}
	elseif( $_POST['facturation_pays'] == 1 && (!isset($_POST['facturation_cp']) || empty($_POST['facturation_cp']))){
		$strStep = "step_1_3";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bVerif = false; 
		$bBlocFacturation = true;
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Le code postal est obligatoire."; 
		$strError .="</span></p>";
	}else{
		$bStep1 = true; 
		$bStep2 = true;
		$strStep = "step_1_4";
	}
}

//Formulaire 4 Conditions générales de vente
if(isset($_POST["action"]) && $_POST["action"] == "step_1_4"){
    $_SESSION["chx_cgu"] = $_POST["chx_cgu"] ;
    if(!isset($_POST["chx_cgu"]) || empty($_POST["chx_cgu"])){
        $strStep = "step_1_4"; 
        $bStep1 = true; 
        $bStep2 = false; 
        $strErreurStep2 = "Veuillez cocher les Conditions Générales d’Utilisation pour activer votre abonnement."; 
    }else{
    
         
	//On vient de poster le formulaire numéro 2 permettant de récupérer l'id_yonix de la duree d'engagement
	$iDeIdYonix = mysql_real_escape_string($_SESSION['cart']['dureeEngagement']["de_id_yonix"]); 
        
        //$_SESSION['cart']['dureeEngagement']
	
	$iFormuleId = $_SESSION['cart']['formule']['formule_id'];
	$strFormuleIdYonix = mysql_real_escape_string($_SESSION['cart']['formule']['formule_id_yonix']);
	$strFormuleClasse = $_SESSION['cart']['formule']['formule_classe'];
   
	if($strFormuleClasse == "ciblee"){
		$strClasse = $oDb->queryItem("SELECT classe_id_yonix FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
		$strMatiere = $oDb->queryItem("SELECT matiere_id_yonix FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['matiere'])."'");
		$strDe = $oDb->queryItem("SELECT de_id FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
						
		//récupération du produit 
		$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."' AND classe_id_yonix = '".$strClasse."' AND matiere_id_yonix = '".$strMatiere."' LIMIT 1"; 
		$aProduit = $oDb->queryRow($strSql); 
		if($oDb->rows > 0 ){
			$_SESSION["products"]["enfants"][0]["ean"] = $aProduit['produit_ean'];
			$_SESSION["products"]["enfants"][0]["duree_engagement"] = $strDe ; 
			$_SESSION["products"]["enfants"][0]["ean_reussite"] = $aProduit['produit_ean'];
			$strStep = "step_1_3";
			$bStep1 = true; 
			$bStep2 = true;			
		}else{
			$strStep = "step_1_2"; 
			$bStep1 = true; 
			$bStep2 = false; 
			$strErreurStep2 = "Une erreur est survenue lors de votre sélection. Veuillez réessayer ultérieurement."; 
		}
	}else if($strFormuleClasse == "reussite"){
		$strClasse = $oDb->queryItem("SELECT classe_id_yonix FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
		$strDe = $oDb->queryItem("SELECT de_id FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
		$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."' AND classe_id_yonix = '".$strClasse."'  LIMIT 1"; 
		$aProduit = $oDb->queryRow($strSql); 
		
		if($oDb->rows > 0 ){
			$_SESSION["products"]["enfants"][0]["ean"] = $aProduit['produit_ean'];
			$_SESSION["products"]["enfants"][0]["duree_engagement"] = $strDe ; 
			$_SESSION["products"]["enfants"][0]["ean_reussite"] = $aProduit['produit_ean'];
			$strStep = "step_1_3";
			$bStep1 = true; 
			$bStep2 = true;			
		}else{
			$strStep = "step_1_2"; 
			$bStep1 = true; 
			$bStep2 = false; 
			$strErreurStep2 = "Une erreur est survenue lors de votre sélection. Veuillez réessayer ultérieurement."; 
		}
	}else if($strFormuleClasse == "tribu"){
            
            $strDe = $_SESSION['cart']['dureeEngagement']["de_id"];
            //$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."'  LIMIT 1"; 
            $aProduit = $_SESSION['code_activation']["PRODUIT"]; 
            if(count($aProduit) > 0 ){
//var_dump($_SESSION['nb_enfant']); exit; 
            
                    for($i= 0; $i<$_SESSION['nb_enfant'] ;$i++){
                            $_SESSION["products"]["enfants"][$i]["ean"] = $aProduit['produit_ean'];
                            $_SESSION["products"]["enfants"][$i]["duree_engagement"] = $strDe ; 
                            //Récupération de l'ean du produit reussite concerné
                            $strSql = "SELECT p.produit_ean FROM bor_produit p 
                                            INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix)
                                            INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
                                            WHERE c.classe_id = '".mysql_real_escape_string($_SESSION["products"]["enfants"][$i]['classe'])."'
                                                    AND	f.formule_classe = 'reussite'
                                                    AND p.de_id_yonix = '".$iDeIdYonix."'"; 
                            $strEan = $oDb->queryItem($strSql); 
                            $_SESSION["products"]["enfants"][$i]["ean_reussite"] = $strEan ; 
                    }

                    $bProduit = true; 
                    foreach( $_SESSION["products"]["enfants"] as $aEnfant){
                            $strSql = "SELECT * FROM bor_produit p INNER JOIN bor_duree_engagement d ON(d.de_id_yonix = p.de_id_yonix) INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix) WHERE c.classe_id = '".mysql_real_escape_string($aEnfant["classe"])."' AND f.formule_classe = 'reussite' AND d.de_id = '".mysql_real_escape_string($aEnfant["duree_engagement"])."' "; 
                            $aProduitEnfant = $oDb->queryRow($strSql); 
                            if(!$oDb->rows){
                                    $bProduit = false ; 
                            }
                    }

                    if($bProduit){
                            $strStep = "step_1_3";
                            $bStep1 = true; 
                            $bStep2 = true;	
                    }else{
                            $strStep = "step_1_2"; 
                            $bStep1 = true; 
                            $bStep2 = false; 
                            $strErreurStep2 = "Une erreur est survenue lors de votre sélection. Veuillez réessayer ultérieurement."; 
                    }
            }
	}
	// var_dump($_CONST['URL_ACCUEIL']);
	$bErreurOffre =false;
	if( isset($_SESSION['user']['IDCLIENT']) && $_SESSION['user']['IDCLIENT'] > 0 && $bStep2){
            //echo "<script>document.location.href='". $_CONST["URL2"]."/code-activation/".$_SESSION['partenaire_activation']["activation_url"]."/activation-formule-tribu-etape-3.html';</script>";
           // exit; 
		//$strStep = "step_1_3";
		//var_dump($_SESSION["products"]["enfants"]);
		echo "<script>document.location.href='". $_CONST["URL2"]."/bss-mag/".$_SESSION['partenaire_activation']["activation_url"]."/activation-formule-".$_SESSION['cart']['formule']['formule_classe']."-etape-3.html';</script>";
		exit; 
	}
    }
	
}
/*if(isset($_POST["action"]) && $_POST["action"] == "step_1_4"){
	echo "<script>document.location.href='". $_CONST["URL2"]."/code-activation/".$_SESSION['partenaire_activation']["activation_url"]."/activation-formule-tribu-etape-3.html';</script>";
	exit; 
}*/

//Traitement du retour de la connexion via le WS PER
if(isset($_SESSION["connexion"]) && !empty($_SESSION['connexion'])){
	if($_SESSION["connexion"] == "ok"){
            unset($_SESSION["connexion"]);
			//var_dump($_SESSION["user"]);exit;
			$strStep = "step_1_3";
            //echo "<script>document.location.href='". $_CONST["URL2"]."/code-activation/".$_SESSION['partenaire_activation']["activation_url"]."/activation-formule-tribu-etape-3.html';</script>";
			//exit; 
	}else if($_SESSION["connexion"] == "erreur_connexion"){
		$strStep = "step_1_2";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bError = true;
		$strError = "Vos identifiant et mot de passe sont incorrects !";
		$bPopupConnexion = true ; 
	}else if($_SESSION["connexion"] == "erreur_champ"){
		$strStep = "step_1_2";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bError = true;
		$strError = "Veuillez renseigner votre identifiant et mot de passe Bordas.";
		$bPopupConnexion = true ; 
	}else if($_SESSION["connexion"] == "erreur_profil"){
		$strStep = "step_1_2";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bError = true;
		$strError = "Vous devez posséder un compte Parent pour utiliser ce site. Si vous n’en avez pas, inscrivez-vous directement sur le site Bordas soutien scolaire.";
		$bPopupConnexion = true ; 
	}
	unset($_SESSION["connexion"]);
}
// unset($_SESSION["user"]);
//var_dump($strStep);
if( isset($_SESSION['mdp_forget']) && !empty($_SESSION['mdp_forget'])){
	$strStep = "step_1_2";
}

// Création de compte, retour de proxy_createAccount_activation
if( isset($_SESSION['user']['IDCLIENT']) && $_SESSION['user']['IDCLIENT'] > 0 && $strStep != "step_1_3" && $strStep != "step_1_4"){
	$strStep = "step_1_3";
}	

// ?
if( isset($_SESSION['creation_compte']) && !empty($_SESSION['creation_compte']) && $_SESSION['creation_compte'] === "ENCOURS"){
	// var_dump($_SESSION['creation_compte']);
	$strStep = "step_1_2";
	unset($_SESSION['creation_compte']);
	
}
// var_dump($strStep);
if(!isset($strStep) || empty($strStep))
	$strStep = "step_1_1"; 
// var_dump($strStep); 

?>
<div class="container">
   <div class="row">
      <ul id="steps" class="col-md-12">
         <li class="detail_formule"><a class="active"><strong>1.</strong>Saisie du code d'activation<span class="arrow_right"></span></a></li>
         <li class="confirmation"><a  class="active" ><span class=" arrow_left "></span><strong>2.</strong>Création de mon compte <span class="arrow_right"></span></a></li>
		 <li class="paiement"><a ><span class="arrow_left"></span><strong>3.</strong>Confirmation <span class="arrow_right"></span></a></li>
      </ul>
   </div>
   <div class="row">       
    <div class="col-sm-12" style="text-align: center;">         
        <h2 class="activation_formule">
            <?php
                if($aDureeEngagement['de_engagement'] == 12)
                    $strDuree = "d'1 an";
                else 
                    $strDuree = "de ".$aDureeEngagement['de_engagement']." mois";
                
                echo date_format(date(), ' jS F Y');
                
                $oDb->query("SET lc_time_names = 'fr_FR'"); 
               // *** SI existantes, les dates en BO prennent le pas sur les dates du flux ***
				
				$strCodeActionBill = $oDb->queryItem("SELECT activation_id	FROM  bor_activation WHERE bill_codeaction ='".$_SESSION['code_activation']['CODE_ACTION']."'");
				
				// date de fin
					$strDateFinBo = $oDb->queryItem("SELECT date_fin FROM  bor_formule_associee WHERE ean13 ='".$_SESSION['code_activation']['PRODUIT']['produit_ean']."' AND activation_id = '".$strCodeActionBill[0]."'");
					
					// si pas de date de fin dans le BO c'est la date du flux qui est envoyée
					if(isset($strDateFinBo) && !empty($strDateFinBo)){
						$strDateFin = $strDateFinBo;
					}else{
						if(isset($_SESSION['code_activation']["DATE"]))
							$strDateFin = $_SESSION['code_activation']["DATE"];
						else
							$strDateFin = "";
					}
					$strDateFin = $oDb->queryItem("SELECT DATE_FORMAT('".$strDateFin."', '%d %M %Y')");
					
				// date de debut
					$strDateDebutBo = $oDb->queryItem("SELECT date_debut FROM  bor_formule_associee WHERE ean13 ='".$_SESSION['code_activation']['PRODUIT']['produit_ean']."' AND activation_id = '".$strCodeActionBill[0]."'");
					
					// si pas de date de fin dans le BO c'est la date du flux qui est envoyée
					if(isset($strDateDebutBo) && !empty($strDateDebutBo)){
						$strDateDuJour = $strDateDebutBo;
						$strDateDuJour = $oDb->queryItem("SELECT DATE_FORMAT('".$strDateDuJour."', '%d %M %Y')");
					}else{
						$strDateDuJour = $oDb->queryItem("SELECT DATE_FORMAT(NOW(), '%d %M %Y')");
					}
					
                //pour une durée $strDuree
				if($aFormule["formule_id"] == 1) /* Formule ciblée */
					echo "Cet abonnement vous donne accès à une matière pour votre enfant.<br>
                        Il est valable à compter du $strDateDuJour et jusqu’au $strDateFin.";
					
				if($aFormule["formule_id"] == 2) /* Formule réussite */
					echo "Cet abonnement vous donne accès à l’ensemble des matières disponibles pour votre enfant.<br>
                        Il est valable à compter du $strDateDuJour et jusqu’au $strDateFin.";
					
				if($aFormule["formule_id"] == 3) /* Formule tribu */
					echo "Cet abonnement vous donne accès à l’ensemble des matières disponibles pour vos enfants.<br>
                        Il est valable à compter du $strDateDuJour et jusqu’au $strDateFin.";
            ?>
            
        </h2>
        <!-- <p>Pour en bénéficier, renseignez les informations ci-dessous :</p> -->
    </div>
   </div>
   <div class="detail_formule">
      <div class="title_formule">
         <h2>
            Je crée le compte de mon enfant
         </h2>
      </div>
      <div class="body_formule ">
         
		 <?php 
		if((isset($_POST['nb_enfant']) && $_POST['nb_enfant'] < 1)){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Veuillez sélectionner un nombre d’enfants.";
			$strError .= "</span></p>";
		}
		 if($strStep =="step_1_1" && $bError){
			echo ' <div class="alert alert-danger">
				'.$strError.'
			 </div>';
		 }
			
			 
			//Gestion du formulaire (3 types de formulaire possible)
			if($_SESSION['cart']['formule']['formule_classe'] == 'ciblee'){
				//Récupération de la liste des classes disponibles
				$ListClasses  = $oDb->queryTab("SELECT c.* FROM bor_classe c 
															INNER JOIN bor_matiere_classe mc ON (mc.classe_id = c.classe_id) 
															INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id) 
															INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 														
														WHERE mc.mc_dispo = 1
														GROUP BY c.classe_id
														ORDER BY n.niveau_position, cn.cn_position");
				echo '	<form class="form-horizontal" role="form" method="post" id="creer_compte_enfant" name="form_enfant" >
							<div class="form-group">
							   <label for="inputEmail3" class="col-sm-3 control-label">Prénom de mon enfant * : 
							   </label>
							   <div class="col-sm-3">
								  <input type="text" name="prenom[0]" class="form-control" id="inputEmail3" placeholder="Prénom" value="'.((isset($_SESSION["products"]["enfants"][0]["D_PRENOM"]) && !empty($_SESSION["products"]["enfants"][0]["D_PRENOM"])) ? $_SESSION["products"]["enfants"][0]["D_PRENOM"] : "" ).'">
							   </div>
							</div>
							<div class="form-group">
							   <label for="inputPassword3" class="col-sm-3 control-label">Classe * :</label>
							   <div class="col-sm-3">
								  <select name="classe[0]" id="classe"  class="classe">
									 <option value="0">Choisir une classe</option>'; 	
										if($ListClasses){ 
											foreach($ListClasses as $classe){
												echo '<option value="'.$classe['classe_id'].'" '.(( isset($_SESSION["products"]["enfants"][0]["classe"]) && $_SESSION["products"]["enfants"][0]["classe"] == $classe['classe_id']) ? 'selected' : '' ) .'>'.$classe['classe_name'].'</option>'; 
											}  
										}
								echo '</select>
							   </div>
							</div>
							<div class="form-group ">
							   <label for="inputPassword3" class="col-sm-3 control-label">Matière * :</label>
							   <div class="col-sm-3 ">
								<select name="matiere[0]" id="matiere" style="max-width: 100%;">
									 <option value="">Choisir une matière</option>'; 
									if(isset($_SESSION["products"]["enfants"][0]["classe"]) && !empty($_SESSION["products"]["enfants"][0]["classe"])){
										$where = 'mc.classe_id ='.mysql_real_escape_string((int)$_SESSION["products"]["enfants"][0]["classe"]);
										$where .= ' AND mc.mc_dispo = 1' ;
										$json = $oDb->queryTab("SELECT mc.matiere_id, m.matiere_titre  as mc_titre FROM bor_matiere_classe mc INNER JOIN bor_matiere m ON (m.matiere_id = mc.matiere_id) WHERE $where");
										foreach($json as $data){
											echo '<option value="'.$data['matiere_id'].'" '.(( isset($_SESSION["products"]["enfants"][0]["matiere"]) && $_SESSION["products"]["enfants"][0]["matiere"] == $data['matiere_id']) ? 'selected' : '' ) .'>'.$data['mc_titre'].'</option>'; 
										}
									}
						echo '	</select>
							   </div>
							   <div   style="clear: both;text-align: center;padding-top: 5px;">
								  <a href="#" class="autres_matieres_link"  data-toggle="modal" data-target="#popupmatieres_0" >Voir les matières déjà disponibles et les dates des prochaines parutions</a> 
							   </div>
							</div>
							<div class="form-group">
							   <div class="col-sm-offset-3 col-sm-6 matiere_etape_suivant">';
						if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
							?>
								 <a href="#" onclick="jQuery('#creer_compte_enfant').submit();ga('send', 'event','bouton', 'clic', 'Etape 2 – Formule CIBLEE | Bloc : Je crée le compte de mon enfant - VALIDER | BSS MAG', 4)" class="btn btn-orange col-sm-offset-4"  style="margin-top:25px;">Je valide >></a>
						<?php
						}else{
							?>
								 <a href="#" onclick="jQuery('#creer_compte_enfant').submit();" class="btn btn-orange col-sm-offset-4"  style="margin-top:25px;">Je valide >></a>
						<?php
						}
						echo '	</div>
							</div>
							<input type="hidden" name="action" value="step_1_1"/>
						</form>'; 
			
			}else if($_SESSION['cart']['formule']['formule_classe'] == 'reussite'){
				//Récupération de la liste des classes disponibles

				// Modif Pf : liste des classes qui sont uniquement dans celles des produits reçu du flux 
				foreach($_SESSION['code_activation']["PRODUITS"] as $prod){
					$list_prod[] = $prod['produit_id'];
				}
				$list_prod = implode(',', $list_prod);
				//var_dump($list_prod);
				// ci dessous ajout du AND p.produit_id IN($list_prod)
				$ListClasses  = $oDb->queryTab("SELECT c.* FROM bor_classe c 
															INNER JOIN bor_produit p ON (p.classe_id_yonix = c.classe_id_yonix) 
															INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) 
															INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id) 
															INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 		
														WHERE f.formule_classe = 'reussite'
														AND p.produit_id IN($list_prod)
														GROUP BY c.classe_id
														ORDER BY n.niveau_position, cn.cn_position"); 
				echo '	<form class="form-horizontal" role="form" method="post" id="creer_compte_enfant" name="form_enfant"> 
							<div class="form-group">
							   <label for="inputEmail3" class="col-sm-3 control-label">Prénom de mon enfant * : 
							   </label>
							   <div class="col-sm-3">
								  <input type="text" name="prenom[0]" class="form-control" id="inputEmail3" placeholder="Prénom" value="'.((isset($_SESSION["products"]["enfants"][0]["D_PRENOM"]) && !empty($_SESSION["products"]["enfants"][0]["D_PRENOM"])) ? $_SESSION["products"]["enfants"][0]["D_PRENOM"] : "" ).'">
							   </div>
							</div>
							<div class="form-group">
							   <label for="inputPassword3" class="col-sm-3 control-label">Classe * :</label>
							   <div class="col-sm-3">
								  <select name="classe[0]" id="classe"  class="classe">
									 <option value="0">Choisir une classe</option>'; 	
										if($ListClasses){ 
											foreach($ListClasses as $classe){
												echo '<option value="'.$classe['classe_id'].'" '.(( isset($_SESSION["products"]["enfants"][0]["classe"]) && $_SESSION["products"]["enfants"][0]["classe"] == $classe['classe_id']) ? 'selected' : '' ) .'>'.$classe['classe_name'].'</option>'; 
											}  
										}
								echo '</select>
							   </div>
							</div>
							<div class="row"   style="clear: both;text-align: center;padding-top: 5px;">
								 <a href="#" class="autres_matieres_link"  data-toggle="modal" data-target="#popupmatieres_0">Voir les matières déjà disponibles et les dates des prochaines parutions</a> 
								</div>
							<div class="form-group">
							   <div class="col-sm-offset-3 col-sm-6">';
						if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
							?>
								 <a href="#" onclick="jQuery('#creer_compte_enfant').submit();ga('send', 'event','bouton', 'clic', 'Etape 2 – Formule REUSSITE | Bloc : Je crée le compte de mon enfant - VALIDER | BSS MAG', 4)" class="btn btn-orange col-sm-offset-4"  style="margin-top:25px;">Je valide >></a>
						<?php
						}else{
							?>
								 <a href="#" onclick="jQuery('#creer_compte_enfant').submit()" class="btn btn-orange col-sm-offset-4"  style="margin-top:25px;">Je valide >></a>
						<?php
						}
						echo '
								</div>
							</div>
							<input type="hidden" name="action" value="step_2_1"/>
						</form>'; 
			}else if($_SESSION['cart']['formule']['formule_classe'] == 'tribu'){
				if((isset($_POST['nb_enfant']) && $_POST['nb_enfant'] > 0 && $_POST['nb_enfant'] <= 5))
					$_SESSION['nb_enfant'] = $_POST['nb_enfant'] ;
				
				echo '	<form class="form-horizontal" role="form" method="post" id="form_nb_enfant" name="form_nb_enfant">
								<div class="form-group">
								   <label for="inputPassword3" class="col-sm-3 control-label">Nombre d\'enfants * :</label>
								   <div class="col-sm-3">
										<select name="nb_enfant" id="nb_enfant">
											 <option value="0">Choisir le nombre d\'enfants</option> 	
                                                                                         <option value="1" '.(($_POST['nb_enfant'] == 1 || $_SESSION['nb_enfant'] == 1) ? 'selected' : '' ) .'>1</option> 
											 <option value="2" '.(($_POST['nb_enfant'] == 2 || $_SESSION['nb_enfant'] == 2) ? 'selected' : '' ) .'>2</option> 	
											 <option value="3" '.(($_POST['nb_enfant'] == 3 || $_SESSION['nb_enfant'] == 3) ? 'selected' : '' ) .'>3</option> 	
											 <option value="4" '.(($_POST['nb_enfant'] == 4 || $_SESSION['nb_enfant'] == 4) ? 'selected' : '' ) .'>4</option> 	
											 <option value="5" '.(($_POST['nb_enfant'] == 5 || $_SESSION['nb_enfant'] == 5) ? 'selected' : '' ) .'>5</option> 	
										</select>
								   </div>
								</div>
								
								<div class="form-group">
								   <div class="col-sm-offset-3 col-sm-6">
									  
							';
							  if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
									  ?>
										  <a href="#" onclick="ga('send', 'event','bouton', 'clic', 'Etape 2 – Formule TRIBU | Bloc : Je crée le compte de mon enfant - VALIDER | BSS MAG', 4);jQuery('#form_nb_enfant').submit();" class="btn btn-orange col-sm-offset-4">Je valide >></a>
									  <?php
							  }else{
									  ?>
										  <a href="#" onclick="jQuery('#form_nb_enfant').submit();" class="btn btn-orange col-sm-offset-4">Je valide >></a>
									  <?php
							  }
                                    echo '
								   </div>
								</div>
							</form>';
				if((isset($_POST['nb_enfant']) && $_POST['nb_enfant'] > 0 && $_POST['nb_enfant'] <= 5) || (isset($_SESSION['nb_enfant']) && $_SESSION['nb_enfant'] > 0 && $_SESSION['nb_enfant'] <= 5)){
					 
					$ListClasses  = $oDb->queryTab("SELECT c.* FROM bor_classe c 
															INNER JOIN bor_produit p ON (p.classe_id_yonix = c.classe_id_yonix) 
															INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) 
															INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id) 
															INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 		
														WHERE f.formule_classe = 'reussite' 
														GROUP BY c.classe_id
														ORDER BY n.niveau_position, cn.cn_position"); 
					echo '	<form class="form-horizontal" role="form" method="post" id="creer_compte_enfant" name="form_enfant">
								<input type="hidden" name="nbenfant" value="'.$_SESSION['nb_enfant'].'"/>';
					for($i=0; $i<$_SESSION['nb_enfant']; $i++){
						echo '
							<h3>Enfant '.($i+1).'</h3>
							<div class="form-group">
							   <label for="inputEmail3" class="col-sm-3 control-label">Prénom de mon enfant * : 
							   </label>
							   <div class="col-sm-3">
								  <input type="text" name="prenom['.$i.']" class="form-control" id="inputEmail3" placeholder="Prénom" value="'.((isset($_SESSION["products"]["enfants"][$i]["D_PRENOM"]) && !empty($_SESSION["products"]["enfants"][$i]["D_PRENOM"])) ? $_SESSION["products"]["enfants"][$i]["D_PRENOM"] : "" ).'">
							   </div>
							</div>
							<div class="form-group">
							   <label for="inputPassword3" class="col-sm-3 control-label">Classe * :</label>
							   <div class="col-sm-3">
								  <select name="classe['.$i.']" id="classe"  class="classe">
									 <option value="0">Choisir une classe</option>'; 	
										if($ListClasses){ 
											foreach($ListClasses as $classe){
												echo '<option value="'.$classe['classe_id'].'" '.(( isset($_SESSION["products"]["enfants"][$i]["classe"]) && $_SESSION["products"]["enfants"][$i]["classe"] == $classe['classe_id']) ? 'selected' : '' ) .'>'.$classe['classe_name'].'</option>'; 
											}  
										}
								echo '</select>
							   </div>
							</div>';
							echo '<div   style="clear: both;text-align: center;padding-top: 5px;">
								 <a href="#" class="autres_matieres_link"  data-toggle="modal" data-target="#popupmatieres_'.$i.'" >Voir les matières déjà disponibles et les dates des prochaines parutions</a> 
							   </div>'; 
					}
					echo '<div class="form-group">
							   <div class="col-sm-offset-3 col-sm-6">';
						if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
								?>
									<a href="#" onclick="ga('send', 'event','bouton', 'clic', 'Etape 2 – Formule TRIBU | Bloc : Je crée le compte de mon enfant - VALIDER | BSS MAG', 4);jQuery('#creer_compte_enfant').submit();" class="btn btn-orange col-sm-offset-4" style="margin-top:25px;">Je valide >></a>
								<?php
						}else{
								?>
									<a href="#" onclick="jQuery('#creer_compte_enfant').submit();" class="btn btn-orange col-sm-offset-4" style="margin-top:25px;">Étape suivante >></a>
								<?php
						}
							echo' 
							   </div>
							</div>
							<input type="hidden" name="action" value="step_3_1"/>
						</form>'; 
				}
			}
		 ?>
         
      </div>
     
			
      <!--***********************-->
      <div class="title_formule" id="compteparent"><h2>Je crée mon compte parent</h2></div>
		<?php 
			if( $strStep =="step_1_2" || $strStep =="step_1_3" ){
		?>
				<div class="body_formule " id="creer_mon_compte">    
					<p>Si vous avez déjà un compte «Bordas»,     <a  class="btn_inscription" data-toggle="modal" data-target="#inscription" id="btn_connexion">connectez-vous</a> pour passer à l’étape suivante.</p>
					<!-- Modal -->
					<?php 
					// var_dump(
					if($strStep =="step_1_3" && $bError && !$bPopupConnexion){
						echo ' <div class="alert alert-danger red">
							'.$strError.'
						 </div>';
					 }
					 ?>
					 
<div class="modal fade" id="inscription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Vous êtes déjà inscrit ?</h4>
				<?php 
					if(isset($bPopupConnexion) && $bPopupConnexion){
						echo '<div class="" id="news-warning" >
									<div class="hero-unit text-center red">
										'.$strError.'
									</div>              
								</div>';
					}
				?>
			</div>
			<div class="modal-body">
				<p>Merci de vous identifier :</p>
				<form class="form-horizontal" role="form" action="/proxy/proxy_connexion.php" method="post">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-5 control-label">Votre identifiant :</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="inputEmail3" value="" name="username">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-5 control-label">Votre mot de passe :</label>
						<div class="col-sm-7">
							<input type="password" class="form-control" id="inputPassword3"  name="password">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-5 col-sm-10">
							<button type="submit" class="btn btn-orange">Connexion »</button>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
						<div class="checkbox">
							<?php
							if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
								?>
									<a href="#" onclick="ga('send', 'event','bouton', 'clic', 'Etape 2 - Formule <?php echo $_SESSION['code_activation']["PRODUIT"]['formule_classe']; ?> | Bloc : Je crée mon compte parent - VALIDER | BSS MAG', 4);jQuery('#inscription').removeClass('in');jQuery('#inscription').hide();jQuery('.modal-backdrop').remove();" class="mot_oublie" data-toggle="modal" data-target="#motdepasseoublie" id="forget_mdp"> Identifiant ou mot de passe oublié ?</a>
							<?php
							}else{
								?>
									<a href="#" onclick="jQuery('#inscription').removeClass('in');jQuery('#inscription').hide();jQuery('.modal-backdrop').remove();" class="mot_oublie" data-toggle="modal" data-target="#motdepasseoublie" id="forget_mdp"> Identifiant ou mot de passe oublié ?</a>
							<?php
							}
							?>
						</div>
						</div>
					</div>
                                    
                                        <input type="hidden" name="redirect_after" value="<?php echo (  $_CONST['URL2'] )  ; ?>/bss-mag/<?php echo $_SESSION['partenaire_activation']["activation_url"]; ?>/activation-formule-<?php echo $_SESSION['cart']['formule']['formule_classe']; ?>-etape-2.html" >
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
		jQuery.noConflict();	
</script>

					<?php
					// var_dump('tete'); 
					include("./proxy/proxy_createAccount_bssmag.php"); 
					// var_dump('tete'); 
						
					?>
					
					<div class="formulaire_form"> 
					<p class="obligatoire"> <strong>*</strong> champs obligatoires</p>
					<p class="description_pack">
					Conformément à la loi Informatique et Libertés du 6 janvier 1978 modifiée en 2004, vous disposez d’un droit d’accès, de rectification ou d’opposition aux données personnelles vous concernant, à la Relation client BORDAS : <a href="mailto:soutien-scolaire@bordas.tm.fr">soutien-scolaire@bordas.tm.fr</a>, <?php echo $_CONST['RC_TEL']; ?>.
					</p>
					</div>    
				</div>
		<?php 
			}
		?>
	  
	   <!--***********************-->
	   <?php if ($_SESSION['partenaire_activation']["resident_actif"] == 1 ) { ?> <!-- Si le bloc je prouve que je suis résident est activé en BO -->
      <div class="title_formule" id="formresident" ><h2>Je prouve que je suis résident <?php if($_SESSION['partenaire_activation']["resident_nom"]) echo $_SESSION['partenaire_activation']["resident_nom"]; ?></h2></div>
		<?php if(  $strStep =="step_1_3" || $strStep =="step_1_4" ){?>
            <div class="body_formule "  >
				<?php 
				// var_dump(
				if($strStep =="step_1_3" && $bBlocFacturation){
					echo ' <div class="alert alert-danger red">
						'.$strError.'
					</div>';
				}
				?>
			<div class="form-horizontal" role="form">
				<form id="form_resident" method="post" >
				<input type="hidden" name="action" value="step_1_3"/>
				<div class="form-group">
					<label for="numclientparent" class="col-sm-3 control-label">Pays :</label>
					<div class="col-sm-7">
					<?php 
							//Récupération de la liste des pays 
							$strSql = "SELECT * FROM bor_pays ORDER BY pays_libelle ASC" ; 
							$aListePays = $oDb->queryTab($strSql); 
							echo "<select  style='width:590px;max-width: 100%;height:36px;' name='facturation_pays' id='facturation_pays'>"; 
							echo "<option value='0'>Choisir votre pays</option>"; 
							foreach ( $aListePays as $aPays){
									echo "<option value='".$aPays['pays_id']."' ".((isset($_POST['facturation_pays']) && $_POST['facturation_pays'] ==$aPays['pays_id'])? "selected" : "")." >".$aPays['pays_libelle']."</option>";
							}
							echo "</select>"; 
					?>
					</div>
				</div>

				<div class="form-group">
					<label for="inputcodepostal" class="col-sm-3 control-label">Code postal :</label>
					<div class="col-sm-4">
					   <input type="text" class="form-control inputgris"   disabled  id="facturation_cp" name="facturation_cp"  <?php if(isset($_POST['facturation_cp'])) echo "value='".$_POST['facturation_cp']."'"; ?> >
					</div>
				</div>

				<div class="form-group" id="div_error" style="display:none;">
					<div class="col-sm-3">
					</div>
					<div class="col-sm-6">
						<label style="color:red;" id="label_error"></label>
					</div>
				</div>

				<div class="form-group">
					<label for="inputville" class="col-sm-3 control-label">Ville :</label>
					<div class="col-sm-7" id="bloc_ville">
						<select style="width:590px;max-width: 100%;height:36px;" class="inputgris" disabled name="facturation_ville" id="facturation_ville">
							<option>Choisir votre ville</option>
						</select>
					</div>
				</div> 

				<div class="form-group">
					<label for="facturation_numero_voie" class="col-sm-3 control-label"> Adresse :</label>
					<div class="col-sm-2"  style='width:95px;padding-right:0px;'>
						<input style="width:80px;" type="text" class="form-control inputgris" disabled id="facturation_numero_voie" maxlength="10" name="facturation_numero_voie"  <?php if(isset($_POST['facturation_numero_voie'])) echo "value=\"".(stripslashes($_POST['facturation_numero_voie']))."\""; ?>>
						<p style="font-size:14px">(ex : 5 TER)</p>
					</div>
					<div class="col-sm-3" style='padding-left: 5px;padding-right: 0px;width: 240px;'>
						<?php 
						//Récupération de la liste des types de voies
						$strSql2 = "SELECT * FROM bor_adresse_postale ORDER BY ref_libelle ASC" ; 
						$aListeTypeVoie = $oDb->queryTab($strSql2); 
						echo "<select class='inputgris' disabled name='facturation_type_voie' id='facturation_type_voie' style='margin-bottom: 5px; height: 36px; line-height: 36px;'>"; 
						echo "<option value='0'>Choisir votre type de voie</option>"; 
						foreach ( $aListeTypeVoie as $aTypeVoie){
							echo "<option value='".$aTypeVoie['ref_code']."' ".((isset($_POST['facturation_type_voie']) && $_POST['facturation_type_voie'] ==$aTypeVoie['ref_code'])? "selected" : "")." >".$aTypeVoie['ref_libelle']."</option>";
						}
						echo "</select>"; 
						?>
					</div>
					<div class="col-sm-3" style='padding-left: 5px;'>
						<input type="text" class="form-control inputgris" disabled id="facturation_nom_voie" maxlength="23" name="facturation_nom_voie"  <?php if(isset($_POST['facturation_nom_voie'])) echo "value=\"".(stripslashes($_POST['facturation_nom_voie']))."\""; ?>>
						<p style="font-size:14px">(ex : MAL J DE L DE TASSIGNY)</p>
					</div>
				</div>

				<div class="form-group">
					<label for="facturation_complement" class="col-sm-3 control-label">Complément d’adresse :</label>
					<div class="col-sm-7">
					   <input style="width: 590px;max-width: 100%;" type="text" class="form-control inputgris" disabled name="facturation_complement" id="facturation_complement"  <?php if(isset($_POST['facturation_complement'])) echo "value=\"".(stripslashes($_POST['facturation_complement']))."\""; ?>>
					   <p style="font-size:14px">(Résidence, appartement, chez Mr ...)</p>
					</div>
				</div>
			
				<div style="text-align:center;">
					<!-- <a href="#" onclick="$(this).attr('disabled','disabled');$('#form_paiement').submit();" class="btn btn-orange valid_abonnement col-sm-offset-6">Je valide » </a> -->
				<?php
				if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
						?>
							<a href="#" onclick="ga('send', 'event','bouton', 'clic', 'Etape 2 - Formule <?php echo $_SESSION['code_activation']["PRODUIT"]['formule_classe']; ?> | Bloc : Je prouve que je suis résident - VALIDER', 4);jQuery('#form_resident').submit();" class="btn btn-orange col-sm-offset-4" style="margin-top:25px;">Je valide >></a>
						<?php
				}else{
						?>
							<a href="#" onclick="jQuery('#form_resident').submit();" class="btn btn-orange">Étape suivante >></a>
						<?php
				}
				?>
				
				</div>
				</form>
            </div>
			</div>    
		<?php 
			}
	   } /* Fin condition Je prouve que je suis résident actif ou non selon partenaire */
	   else
	   {
		if( $strStep == "step_1_3" || $strStep == "step_1_4" )
			$strStep = "step_1_4";
	   }
		?>
	  
	  <!--***********************-->
      <div class="title_formule" id="choixformule" ><h2>Je valide les Conditions Générales d'Utilisation</h2></div>
       <?php if( $strStep =="step_1_4" ){?>
            <div class="body_formule "  > 
                <?php
                        if(isset($strErreurStep2) && !empty($strErreurStep2)){
                                echo '<div class="alert alert-danger">
                                                <img src="/images/warning_icon.png" alt="">
                                                <a href="#" class="alert-link">'.$strErreurStep2.'</a>
                                        </div>';
                                unset($strErreurStep2); 
                        }
                ?>
                <div class="row packs_form"> 
                <p >
                    <form id="form_cgu" method="post" >
                        <input style=" margin-left: 35px;" type="checkbox" name="chx_cgu" id="chx_cgu" value="1" <?php if(isset($_SESSION["chx_cgu"]) && $_SESSION["chx_cgu"]) echo "checked";?> /> J'ai lu et j'accepte les <a href="#" data-toggle="modal" data-target="#modal_cgu">Conditions Générales d'Utilisation (CGU)</a></p>
                        <input type="hidden" name="action" value="step_1_4"/>
                        <div class="col-sm-offset-3 col-sm-6">
                            <?php
                            if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
                                    ?>
                                        <a href="#" onclick="ga('send', 'event','bouton', 'clic', 'Etape 2 - Bloc : Je valide les Conditions Générales d\'Utilisation - ÉTAPE SUIVANTE | BSS MAG', 4);jQuery('#form_cgu').submit();" class="btn btn-orange col-sm-offset-4" style="margin-top:25px;">Je valide >></a>
                                    <?php
                            }else{
                                    ?>
                                        <a href="#" onclick="jQuery('#form_cgu').submit();" class="btn btn-orange">Je valide >></a>
                                    <?php
                            }
                            ?>

                        </div>

                    </form>		
                </div>   
            </div>    
	<?php 
        }
       ?>
	  
   </div>
</div>


<?php 

//Création d'un popup pour tous les enfants 
if(isset($_SESSION['nb_enfant']) && $_SESSION['nb_enfant'] > 0 ){
	for($i=0; $i<$_SESSION['nb_enfant']; $i++){
		echo '
		<!-- Modal pop up autres matieres -->
		<div class="modal fade popupmatieres" id="popupmatieres_'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		   <div class="modal-dialog">
			  <div class="modal-content">
				 <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Liste des matières disponibles en <span id="modal_matiere_titre_'.$i.'"></span></h4>
					<div class="" id="news-warning" >
						<div class="hero-unit text-center red" id="error_'.$i.'">
						</div>              
					</div>
					<div class="" id="news-success" >
						<div class="hero-unit text-center green" style="color:green;" id="success_'.$i.'">
						</div>              
					</div>
				 </div>
					<div class="modal-body" id="body_matiere_'.$i.'">

					</div>
					<div class="modal-footer" id="modal_matiere_body_'.$i.'" style="display:none;">
						<div class="spacer"></div>
						<form method="post">
						<div class="form-group">
						   <label for="inputPassword3" class=" control-label">Si vous souhaitez être informé lorsque de nouvelles
matières sont disponibles, laissez-nous votre email : </label>
						   <div class="col-sm-10">
							  <input type="email" class="form-control" id="inputEmail_'.$i.'" required name="modal_email">
						   </div>
						   <div class="col-sm-2">
							  <input type="button"  onclick="valider_formulaire('.$i.');" class="form-control btn btn-orange" id="inputEmail3" value="ok">
							  <input type="hidden" name="action" value="save_classe"/>
							  <input type="hidden" id="modal_classe_'.$i.'" name="modal_classe" value=""/>
						   </div>
						</div>
						</form>
					</div>
			  </div>
		   </div>
		</div>';
	}
}else{
	echo '
	<div class="modal fade popupmatieres" id="popupmatieres_0" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	   <div class="modal-dialog">
		  <div class="modal-content">
			 <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Liste des matières disponibles en <span id="modal_matiere_titre_0"></span></h4>
				<div class="" id="news-warning" >
					<div class="hero-unit text-center red" id="error_0">
					</div>              
				</div>
				<div class="" id="news-success" >
					<div class="hero-unit text-center green"   style="color:green;" id="success_0">
					</div>              
				</div>
			 </div>
				<div class="modal-body" id="body_matiere_0">
				</div>
			<form method="post">
			   <div class="modal-footer" id="modal_matiere_body_0"  style="display:none;"> 
					<div class="spacer"></div>
					<div class="form-group">
					   <label for="inputPassword3" class=" control-label">Si vous souhaitez être informé lorsque de nouvelles
matières sont disponibles, laissez-nous votre email : </label>
					   <div class="col-sm-10">
						   <input type="email" class="form-control" id="inputEmail_0" required name="modal_email">
					   </div>
					   <div class="col-sm-2 valid_matiere_button">
						    <input type="button" onclick="valider_formulaire(0);" class="form-control btn btn-orange " id="inputEmail3" value="ok">
							<input type="hidden" name="action" value="save_classe"/>
							<input type="hidden" id="modal_classe_0" name="modal_classe" value=""/>
					   </div>
					</div>
				</div>
				</form>
				
		
			 
		  </div>
	   </div>
	</div>
';
}

?>











<?php 
	if(isset($bPopupConnexion) && $bPopupConnexion){
?>
		<script type="text/javascript">
			jQuery('document').ready(function(){
				jQuery('#btn_connexion').click();
			});
		</script>
<?php
	}
	if($bForget){
?>
	<script type="text/javascript">
		jQuery('document').ready(function(){
			jQuery('#forget_mdp').click();
			
		});
	</script>
<?php
	}
?>



<!-- ajax call -->
<script type="text/javascript">
	jQuery(document).ready(function(){
            jQuery("#chx_cgu").click(function(){
                if(!jQuery(this).is(":checked"))
                   jQuery('#form_cgu').submit();
            });
            
            
		jQuery('.classe').each(function(){
			jQuery.ajax({type:"POST", data:jQuery(this).serialize(), url: "/templates/ajax/bloc_matiere.php" ,dataType: 'script'	});
		});
		
		// liste classe du matiere selectionne
		jQuery('.classe').change(function () {
			// alert('test'); 
			
			jQuery.ajax({type:"POST", data: jQuery(this).serialize(), url: "/templates/ajaxsearch.php" ,dataType: 'json',	success: function(json) {
					jQuery('#matiere option').remove(); 
					jQuery('#matiere').append('<option value="">Choisir une matière</option>');
					jQuery.each(json, function(index, value) { 
						jQuery('#matiere').append('<option value="'+ json[index]['matiere_id'] +'">'+ json[index]['mc_titre'] +'</option>');
					});
				},
				error: function(){
					jQuery('#matiere').html('');
				}
			});
			
			// update_popup(0);
			jQuery.ajax({type:"POST", data: jQuery(this).serialize(), url: "/templates/ajax/bloc_matiere.php" ,dataType: 'script'	});
			
			 return false;
		 
		});	
		
	});	  
	function valider_formulaire(iPopup){
		// alert(iPopup);
		bVerif = true ; 
		jQuery('#error_'+iPopup).html(''); 
		jQuery('#success_'+iPopup).html(''); 
		strEmail = jQuery('#inputEmail_'+iPopup).val() ; 
		iClasse = jQuery('#modal_classe_'+iPopup).val() ; 
		// alert(iClasse); 
		if( strEmail == "" ){
			bVerif = false ; 
			jQuery('#error_'+iPopup).html('Veuillez saisir votre email.'); 
		}
		if( !(iClasse > 0) ){
			bVerif = false ; 
			jQuery('#error_'+iPopup).html('Veuillez saisir la classe de votre enfant.'); 
		}
		
		if(bVerif){
			// alert('ok'); 
			jQuery.ajax(
				{
					type:"POST", 
					data: 
						{
							iPopup : iPopup, 
							iClasse : iClasse, 
							strEmail : strEmail
						}, 
					url: "/templates/ajax/save_email.php" 
				}
			).done(function( msg ) {
				if(msg == 1 ) 	
					jQuery('#success_'+iPopup).html('Nous avons bien enregistré votre demande.');
				else
					jQuery('#error_'+iPopup).html('Veuillez saisir une adresse email valide.');
			});
		}
		
	}
	// function update_popup(iPopup){
		// $.ajax({type:"POST", data: $(this).serialize(), url: "/templates/ajax/bloc_matiere.php" ,dataType: 'script'	});
	// }
</script>

<script type="text/javascript">

	


jQuery(window).load(function(){
	<?php 
		if(isset($strStep) && $strStep == "step_1_4") {
	?>
			jQuery('#choixformule').ScrollTo({
				duration: 1000
			});
	<?php 
		}
		if(isset($strStep) && $strStep == "step_1_3") {
	?>
			jQuery('#formresident').ScrollTo({
				duration: 1000
			});
	<?php
		}
		if(isset($strStep) && $strStep == "step_1_2") {
	?>
			jQuery('#compteparent').ScrollTo({
				duration: 1000
			});
	<?php
		}
	?>
});
</script>

<script type='text/javascript'>
jQuery(document).ready(function(){
	jQuery("#facturation_pays").change(function(){
            jQuery('#facturation_cp').removeClass('inputgris');
            jQuery('#facturation_cp').removeAttr('disabled');
            jQuery('#facturation_cp').val('');
        });
	jQuery("#facturation_cp").keyup(function( request, response ) {		
		recherche_ville();	
	});
	recherche_ville();
});

function recherche_ville(){
	strCp =jQuery("#facturation_cp").val();
	strPays =jQuery("#facturation_pays").val();
	jQuery.ajax({
		url:  "/templates/ajax/ws_adresse_1.php",
		dataType: "script",
		data: {
			cp: strCp, 
			pays: strPays
		}
	});		
}
</script>