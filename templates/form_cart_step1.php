<script type="text/javascript">
		jQuery.noConflict();
</script>

<?php
// echo 'test' ; 
 // error_reporting(E_ALL);
// ini_set('error_reporting', E_ALL);
 // ini_set('display_errors', '1');
//On verifie que la formule existe 
$strSql = "SELECT * FROM bor_formule WHERE formule_classe = '".mysql_real_escape_string($_GET['formule'])."'"; 
$aFormule = $oDb->queryRow($strSql); 
if(!$oDb->rows){
	echo "<script>document.location.href='".$_CONST["URL2"]."/soutien-scolaire-en-ligne/abonnement.html';</script>";
	exit; 
}else{
	$_SESSION['cart']['formule'] = $aFormule ; 
	$_SESSION["products"]["enfants"][0]["formule"] = $aFormule ["formule_id"] ; 
}
$bError = false ; 

if(isset($_POST["inputpromo"]) && $_POST["inputpromo"]=="ok" && !empty($_POST["codepromo"])){
    unset($_SESSION["CODEPROMO"]);
    unset($_SESSION["products"]["enfants"][0]['CODEPROMO']);
    unset($_SESSION["products"]["enfants"][0]['REDUCTION']);
    
    $strStep = "step_1_2"; 
    $bStep1 = true; 
    //Vérification que le coupon est actif 
    $sql = "SELECT coupon_reduction , f.de_id
            FROM bor_coupon c 
                INNER JOIN bor_coupon_formule f ON (c.coupon_id = f.coupon_id) 
            WHERE c.coupon_code ='".mysql_real_escape_string($_POST["codepromo"])."' 
                AND f.formule_id ='".$_SESSION['cart']['formule']['formule_id']."'
                AND c.coupon_debut <= NOW() 
                AND c.coupon_fin >= NOW()
            "; 
    $aCoupons = $oDb->queryTab($sql);
    if($oDb->rows > 0){
        $_SESSION["CODEPROMO"]["CODE"] = mysql_real_escape_string($_POST["codepromo"]); 
        foreach($aCoupons as $aCoupon){
            $_SESSION["CODEPROMO"]["REDUCTION"][$aCoupon["de_id"]] = $aCoupon["coupon_reduction"] ;
        }
    }else{
        $strStep = "step_1_2"; 
        $bStep1 = true; 
        $bStep2 = false; 
        $strErreurStep2 = "Le code promo n'est pas valide."; 
    }
}
 
/*GESTION DES FORMULAIRE */
//Formulaire 1 ciblee
if(isset($_POST["action"]) && $_POST["action"] == "step_1_1"){
    unset($_SESSION["CODEPROMO"]);
    unset($_SESSION["products"]["enfants"][0]['CODEPROMO']);
    unset($_SESSION["products"]["enfants"][0]['REDUCTION']);  
    
   
	$bError = false ; 
	unset($_SESSION["products"]["enfants"]);
	$_SESSION["products"]["enfants"][0]["classe"] = $_POST["classe"][0]; 
	$_SESSION["products"]["enfants"][0]["D_PRENOM"] = $_POST["prenom"][0] ; 
	$_SESSION["products"]["enfants"][0]["matiere"] =  $_POST["matiere"][0] ; 
		
	//On se trouve dans une formule ciblee on doit donc vérifier que les champs enfant, classe et matière sont bien selectionné
	if(!isset($_POST["prenom"][0]) || empty($_POST["prenom"][0])){
		$bError = true; 
		$strStep = "step_1_1"; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Veuillez saisir le prénom de votre enfant.";
		$strError .="</span></p>";
	}else{
		//Initialisation du prénom de l'enfant
		if( !isset($_POST["classe"][0]) || empty($_POST["classe"][0]) ){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Veuillez saisir la classe de votre enfant.";
		$strError .="</span></p>";
		}else {
			if(   !isset($_POST["matiere"][0]) || empty($_POST["matiere"][0]) ) {
				
				$bError = true; 
				$strStep = "step_1_1"; 
				$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
				$strError .= "Veuillez saisir la matière de votre enfant.";
		$strError .="</span></p>";
			}else{
				
				//Prénom, matière et classe remplie on vérifi les donnée
				$strSql = "SELECT * FROM bor_matiere_classe mc INNER JOIN bor_classe c ON (c.classe_id = mc.classe_id) INNER JOIN bor_matiere m ON (m.matiere_id = mc.matiere_id)  WHERE mc.classe_id = '".mysql_real_escape_string($_POST["classe"][0])."' AND mc.matiere_id  = '".mysql_real_escape_string($_POST["matiere"][0])."' AND mc.mc_dispo = 1"; 
				$aInfosClasse = $oDb->queryRow($strSql); 
				if(!$oDb->rows){
					$bError = true; 
					$strStep = "step_1_1"; 
					$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
					$strError .= "Nous ne retrouvons pas la formule associée à votre sélection. Veuillez choisir un couple classe/matière valide.";
		$strError .="</span></p>";
				}else{
					$_SESSION["products"]["enfants"][0]["classe"] = $aInfosClasse["classe_id"]; 
					$_SESSION["products"]["enfants"][0]["matiere"] = $aInfosClasse["matiere_id"]; 
					
					$strStep = "step_1_2"; 
					$bStep1 = true; 
				}
			}
		}
	}	
}
//Formulaire 1 reussite
if(isset($_POST["action"]) && $_POST["action"] == "step_2_1"){
    unset($_SESSION["CODEPROMO"]);
    unset($_SESSION["products"]["enfants"][0]['CODEPROMO']);
    unset($_SESSION["products"]["enfants"][0]['REDUCTION']);  
    
	unset($_SESSION["products"]["enfants"]);
	$_SESSION["products"]["enfants"][0]["classe"] = $_POST["classe"][0]; 
	$_SESSION["products"]["enfants"][0]["D_PRENOM"] = $_POST["prenom"][0] ; 
	//On se trouve dans une formule reussite on doit donc vérifier que les champs enfant, classe et matière sont bien selectionné
	if(!isset($_POST["prenom"][0]) || empty($_POST["prenom"][0])){
		
		$bError = true; 
		$strStep = "step_1_1"; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Veuillez saisir le prénom de votre enfant.";
		$strError .="</span></p>";
	}else{
		//Initialisation du prénom de l'enfant
		
		if( !isset($_POST["classe"][0]) || empty($_POST["classe"][0])  ){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Veuillez saisir la classe de votre enfant.";
		$strError .="</span></p>";
		}else{
			//Prénom, matière et classe remplie on vérifi les donnée
			$strSql = "SELECT * FROM bor_classe c  WHERE c.classe_id = '".mysql_real_escape_string($_POST["classe"][0])."'  LIMIT 1"; 
			$aInfosClasse = $oDb->queryRow($strSql); 
			if(!$oDb->rows){
				$bError = true; 
				$strStep = "step_1_1"; 
				$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
				$strError .= "Nous ne retrouvons pas la formule associée à votre sélection.";
		$strError .="</span></p>";
			}else{
				$_SESSION["products"]["enfants"][0]["classe"] = $_POST["classe"][0]; 
				$_SESSION["products"]["enfants"][0]["matiere"] = ''; 
				$strStep = "step_1_2"; 
				$bStep1 = true; 
			}
		}
	}	
}
//Formulaire 1 tribu
// $_POST['nbenfant']="";
// unset($_SESSION["nb_enfant"]);
// exit;
// var_dump($_POST['action']);
if(isset($_POST["action"]) && $_POST["action"] == "step_3_1"){
    unset($_SESSION["CODEPROMO"]);
    unset($_SESSION["products"]["enfants"][0]['CODEPROMO']);
    unset($_SESSION["products"]["enfants"][0]['REDUCTION']);  
	unset($_SESSION["products"]["enfants"]);
	//On se trouve dans une formule reussite on doit donc vérifier que les champs enfant, classe et matière sont bien selectionné
	$iNbEnfant = $_POST['nbenfant'];
	
	
		$iPrenom = count($_POST['prenom']);
		$iClasse = count($_POST['classe']);
		if(  $iNbEnfant!= $iPrenom  ){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Veuillez saisir le prénom de vos enfants.";
		$strError .="</span></p>";
		}else if(  $iNbEnfant!= $iClasse  ){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Veuillez saisir la classe de vos enfants.";
		$strError .="</span></p>";
		}else{
			$bVerifEnfant = true;
			$bVerifClasse = true ;
			for($i = 0; $i < $iNbEnfant; $i++){
				$_SESSION["products"]["enfants"][$i]["classe"] = $_POST["classe"][$i];
				$_SESSION["products"]["enfants"][$i]["D_PRENOM"] = $_POST["prenom"][$i] ; 				
				if(empty($_POST['prenom'][$i]) )
					$bVerifEnfant = false ; 
				if( empty($_POST['classe'][$i]))
					$bVerifClasse = false ; 
			}
			if(!$bVerifEnfant || !$bVerifClasse){
				$bError = true; 
				$strStep = "step_1_1"; 
				if(!$bVerifEnfant ){
					$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
					$strError .= "Veuillez saisir le prénom de vos enfants.";
		$strError .="</span></p>";
				}if (!$bVerifClasse ){
					$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
					$strError .= "Veuillez saisir la classe de vos enfants.";
		$strError .="</span></p>";
				}
			}else{
				for($i = 0; $i < $iNbEnfant; $i++){
					$_SESSION["products"]["enfants"][$i]["D_PRENOM"] = $_POST["prenom"][$i] ; 
					$strSql = "SELECT * FROM bor_classe c  WHERE c.classe_id = '".mysql_real_escape_string($_POST["classe"][$i])."'  LIMIT 1"; 
					$aInfosClasse = $oDb->queryRow($strSql); 
					$_SESSION["products"]["enfants"][$i]["classe"] = $_POST["classe"][$i]; 
					$_SESSION["products"]["enfants"][$i]["matiere"] = ''; 
					$strStep = "step_1_2"; 
					$bStep1 = true; 
				}
			}
		}
	
}

//Formulaire 2
if(isset($_POST["action"]) && $_POST["action"] == "step_1_2"){
	//On vient de poster le formulaire numéro 2 permettant de récupérer l'id_yonix de la duree d'engagement
	$iDeIdYonix = mysql_real_escape_string($_POST['de']); 
	
	$iFormuleId = $_SESSION['cart']['formule']['formule_id'];
	$strFormuleIdYonix = mysql_real_escape_string($_SESSION['cart']['formule']['formule_id_yonix']);
	$strFormuleClasse = $_SESSION['cart']['formule']['formule_classe'];
	if($strFormuleClasse == "ciblee"){
		$strClasse = $oDb->queryItem("SELECT classe_id_yonix FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
		$strMatiere = $oDb->queryItem("SELECT matiere_id_yonix FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['matiere'])."'");
		$strDe = $oDb->queryItem("SELECT de_id FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
						
		//récupération du produit 
		$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."' AND classe_id_yonix = '".$strClasse."' AND matiere_id_yonix = '".$strMatiere."' LIMIT 1"; 
		$aProduit = $oDb->queryRow($strSql); 
		if($oDb->rows > 0 ){
                        //LOT6
                        if(isset($_SESSION["CODEPROMO"]["REDUCTION"][$strDe]) && $_SESSION["CODEPROMO"]["REDUCTION"][$strDe] > 0){
                            $_SESSION["products"]["enfants"][0]["CODEPROMO"] = $_SESSION["CODEPROMO"]["CODE"];
                            $_SESSION["products"]["enfants"][0]["REDUCTION"] = $_SESSION["CODEPROMO"]["REDUCTION"][$strDe];
                        }
                        $_SESSION["products"]["enfants"][0]["ean"] = $aProduit['produit_ean'];
                        $_SESSION["products"]["enfants"][0]["duree_engagement"] = $strDe ; 
                        $_SESSION["products"]["enfants"][0]["ean_reussite"] = $aProduit['produit_ean'];
                        $strStep = "step_1_3";
                        $bStep1 = true; 
                        $bStep2 = true;			
		}else{
			$strStep = "step_1_2"; 
			$bStep1 = true; 
			$bStep2 = false; 
			$strErreurStep2 = "Une erreur est survenue lors de votre sélection. Veuillez réessayer ultérieurement."; 
			
			$strMessage = "Produit non trouvé lors de la commande.<br><br>" ; 
			$strMessage .= "Clients<br>" ;
			$strMessage .= "- Id client web : ".$_SESSION['user']["IDCLIENTWEB"]."<br>" ;
			$strMessage .= "- Num client : ".$_SESSION['user']["NUMCLIENT"]."<br>" ;
			$strMessage .= "- Nom : ".$_SESSION['user']["NOMCLIENT"]."<br>" ;
			$strMessage .= "- Prénom : ".$_SESSION['user']["PRENOMCLIENT"]."<br>" ;
			$strMessage .= "- Email : ".$_SESSION['user']["EMAIL"]."<br>" ;
			
			// $strMessage .= print_r($_SESSION["user"], true) ;
			$strMessage .= "<br><br>"; 
			$strMessage .= "Formule<br>" ;
			$strMessage .= "- Id Yonix : ".$_SESSION['cart']["formule"]["formule_id_yonix"]."<br>" ;
			$strMessage .= "- Libellé : ".$_SESSION['cart']["formule"]["formule_libelle_yonix"]."<br>" ;
			//Récupération de la classe : 
			$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id = '".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["classe"])."' "); 
			$strMessage .= "- Classe : ".$strClasse."<br>" ;
			$strMatiere = $oDb->queryItem("SELECT matiere_titre FROM bor_matiere WHERE matiere_id = '".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["matiere"])."' "); 
			$strMessage .= "- Matière : ".$strMatiere."<br>" ;
			$strDeLibelle = $oDb->queryItem("SELECT de_libelle_yonix FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
			$strMessage .= "- Durée d'engagement : ".$strDeLibelle."<br>" ;
			
			$mail = new intyMailer(); 
			// OPTIONAL 
			$mail->set(array('charset'  , 'utf-8')); 
			$mail->set(array('priority' , 3)); 
			$mail->set(array('replyTo'  , "noreply@bordas.tm.fr")); 
			$mail->set(array('alert '    ,  true)); 
			$mail->set(array('html'     ,  true)); 
			// OPTIONAL 
			$mail->set(array('subject' , "[".$_CONST["SITE_NAME"]."] Commande : Produit non retrouvé")); 
			$mail->set(array('message' , $strMessage)); 
			$mail->set(array('sending' , 'FROM' , "Bordas Soutien Scolaire" ,"noreply@bordas.tm.fr")); 
			$aStrEmail = "hz.hedizouari@gmail.com";
			$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
			$mail->send();
			$aStrEmail = "fpioche@sejer.fr";
			$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
			$mail->send();
			
		}
	}else if($strFormuleClasse == "reussite"){
		$strClasse = $oDb->queryItem("SELECT classe_id_yonix FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
		$strDe = $oDb->queryItem("SELECT de_id FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
		$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."' AND classe_id_yonix = '".$strClasse."'  LIMIT 1"; 
		$aProduit = $oDb->queryRow($strSql); 
		if($oDb->rows > 0 ){
                        //LOT6
                        if(isset($_SESSION["CODEPROMO"]["REDUCTION"][$strDe]) && $_SESSION["CODEPROMO"]["REDUCTION"][$strDe] > 0){
                            $_SESSION["products"]["enfants"][0]["CODEPROMO"] = $_SESSION["CODEPROMO"]["CODE"];
                            $_SESSION["products"]["enfants"][0]["REDUCTION"] = $_SESSION["CODEPROMO"]["REDUCTION"][$strDe];
                        }
			$_SESSION["products"]["enfants"][0]["ean"] = $aProduit['produit_ean'];
			$_SESSION["products"]["enfants"][0]["duree_engagement"] = $strDe ; 
			$_SESSION["products"]["enfants"][0]["ean_reussite"] = $aProduit['produit_ean'];
			$strStep = "step_1_3";
			$bStep1 = true; 
			$bStep2 = true;			
		}else{
			$strStep = "step_1_2"; 
			$bStep1 = true; 
			$bStep2 = false; 
			$strErreurStep2 = "Une erreur est survenue lors de votre sélection. Veuillez réessayer ultérieurement."; 
		
			$strMessage = "Produit non trouvé lors de la commande.<br><br>" ; 
			$strMessage .= "Clients<br>" ;
			$strMessage .= "- Id client web : ".$_SESSION['user']["IDCLIENTWEB"]."<br>" ;
			$strMessage .= "- Num client : ".$_SESSION['user']["NUMCLIENT"]."<br>" ;
			$strMessage .= "- Nom : ".$_SESSION['user']["NOMCLIENT"]."<br>" ;
			$strMessage .= "- Prénom : ".$_SESSION['user']["PRENOMCLIENT"]."<br>" ;
			$strMessage .= "- Email : ".$_SESSION['user']["EMAIL"]."<br>" ;
			
			// $strMessage .= print_r($_SESSION["user"], true) ;
			$strMessage .= "<br><br>"; 
			$strMessage .= "Formule<br>" ;
			$strMessage .= "- Id Yonix : ".$_SESSION['cart']["formule"]["formule_id_yonix"]."<br>" ;
			$strMessage .= "- Libellé : ".$_SESSION['cart']["formule"]["formule_libelle_yonix"]."<br>" ;
			//Récupération de la classe : 
			$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id = '".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["classe"])."' "); 
			$strMessage .= "- Classe : ".$strClasse."<br>" ;
			$strDeLibelle = $oDb->queryItem("SELECT de_libelle_yonix FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
			$strMessage .= "- Durée d'engagement : ".$strDeLibelle."<br>" ;
			$mail = new intyMailer(); 
			// OPTIONAL 
			$mail->set(array('charset'  , 'utf-8')); 
			$mail->set(array('priority' , 3)); 
			$mail->set(array('replyTo'  , "noreply@bordas.tm.fr")); 
			$mail->set(array('alert '    ,  true)); 
			$mail->set(array('html'     ,  true)); 
			// OPTIONAL 
			$mail->set(array('subject' , "[".$_CONST["SITE_NAME"]."] Commande : Produit non retrouvé")); 
			$mail->set(array('message' , $strMessage)); 
			$mail->set(array('sending' , 'FROM' , "Bordas Soutien Scolaire" ,"noreply@bordas.tm.fr")); 
			$aStrEmail = "hz.hedizouari@gmail.com";
			$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
			$mail->send();
			$aStrEmail = "fpioche@sejer.fr";
			$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
			$mail->send();
		
		
		
		
		
		}
	}else if($strFormuleClasse == "tribu"){
		$strDe = $oDb->queryItem("SELECT de_id FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
		$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."'  LIMIT 1"; 
		$aProduit = $oDb->queryRow($strSql); 
		if($oDb->rows > 0 ){
			for($i= 0; $i<$_SESSION['nb_enfant'] ;$i++){
				$_SESSION["products"]["enfants"][$i]["ean"] = $aProduit['produit_ean'];
				$_SESSION["products"]["enfants"][$i]["duree_engagement"] = $strDe ; 
				//Récupération de l'ean du produit reussite concerné
				$strSql = "SELECT p.produit_ean FROM bor_produit p 
                                                    INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix)
                                                    INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
                                                    WHERE c.classe_id = '".mysql_real_escape_string($_SESSION["products"]["enfants"][$i]['classe'])."'
                                                            AND	f.formule_classe = 'reussite'
                                                            AND p.de_id_yonix = '".$iDeIdYonix."'"; 
				$strEan = $oDb->queryItem($strSql); 
				$_SESSION["products"]["enfants"][$i]["ean_reussite"] = $strEan ; 
			}
				
			$bProduit = true; 
			foreach( $_SESSION["products"]["enfants"] as $aEnfant){
				$strSql = "SELECT * FROM bor_produit p INNER JOIN bor_duree_engagement d ON(d.de_id_yonix = p.de_id_yonix) INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix) WHERE c.classe_id = '".mysql_real_escape_string($aEnfant["classe"])."' AND f.formule_classe = 'reussite' AND d.de_id = '".mysql_real_escape_string($aEnfant["duree_engagement"])."' "; 
				$aProduitEnfant = $oDb->queryRow($strSql); 
				if(!$oDb->rows){
					$bProduit = false ; 
				}
			}
			
			if($bProduit){
                                //LOT6
                                if(isset($_SESSION["CODEPROMO"]["REDUCTION"][$strDe]) && $_SESSION["CODEPROMO"]["REDUCTION"][$strDe] > 0){
                                    $_SESSION["products"]["enfants"][0]["CODEPROMO"] = $_SESSION["CODEPROMO"]["CODE"];
                                    $_SESSION["products"]["enfants"][0]["REDUCTION"] = $_SESSION["CODEPROMO"]["REDUCTION"][$strDe];
                                }
				$strStep = "step_1_3";
				$bStep1 = true; 
				$bStep2 = true;	
			}else{
				$strStep = "step_1_2"; 
				$bStep1 = true; 
				$bStep2 = false; 
				$strErreurStep2 = "Une erreur est survenue lors de votre sélection. Veuillez réessayer ultérieurement."; 
				
				$strMessage = "Produit non trouvé lors de la commande.<br><br>" ; 
			$strMessage .= "Clients<br>" ;
			$strMessage .= "- Id client web : ".$_SESSION['user']["IDCLIENTWEB"]."<br>" ;
			$strMessage .= "- Num client : ".$_SESSION['user']["NUMCLIENT"]."<br>" ;
			$strMessage .= "- Nom : ".$_SESSION['user']["NOMCLIENT"]."<br>" ;
			$strMessage .= "- Prénom : ".$_SESSION['user']["PRENOMCLIENT"]."<br>" ;
			$strMessage .= "- Email : ".$_SESSION['user']["EMAIL"]."<br>" ;
			
			// $strMessage .= print_r($_SESSION["user"], true) ;
			$strMessage .= "<br><br>"; 
			$strMessage .= "Formule<br>" ;
			$strMessage .= "- Id Yonix : ".$_SESSION['cart']["formule"]["formule_id_yonix"]."<br>" ;
			$strMessage .= "- Libellé : ".$_SESSION['cart']["formule"]["formule_libelle_yonix"]."<br>" ;
			//Récupération de la classe : 
			foreach( $_SESSION["products"]["enfants"] as $aEnfant){
				$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id = '".mysql_real_escape_string($aEnfant["classe"])."' "); 
				$strMessage .= "- Classe : ".$strClasse."<br>" ;			
			}
			$strDeLibelle = $oDb->queryItem("SELECT de_libelle_yonix FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
			$strMessage .= "- Durée d'engagement : ".$strDeLibelle."<br>" ;
				$mail = new intyMailer(); 
				// OPTIONAL 
				$mail->set(array('charset'  , 'utf-8')); 
				$mail->set(array('priority' , 3)); 
				$mail->set(array('replyTo'  , "noreply@bordas.tm.fr")); 
				$mail->set(array('alert '    ,  true)); 
				$mail->set(array('html'     ,  true)); 
				// OPTIONAL 
				$mail->set(array('subject' , "[".$_CONST["SITE_NAME"]."] Commande : Produit non retrouvé")); 
				$mail->set(array('message' , $strMessage)); 
				$mail->set(array('sending' , 'FROM' , "Bordas Soutien Scolaire" ,"noreply@bordas.tm.fr")); 
				$aStrEmail = "hz.hedizouari@gmail.com";
				$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
				$mail->send();
				$aStrEmail = "fpioche@sejer.fr";
				$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
				$mail->send();
			}
		}else{
			$strStep = "step_1_2"; 
			$bStep1 = true; 
			$bStep2 = false; 
			$strErreurStep2 = "Une erreur est survenue lors de votre sélection. Veuillez réessayer ultérieurement."; 
		}
	}
	
		/* Tag Universel Cart public idées*/
	if(strtolower($_CONST['TYPE_ENVIRONNEMENT']) == 'prod'){
		if($strFormuleClasse == "ciblee"){
		?>
			<script type="text/javascript"> 
				var tip = tip || []; 
				tip.push(['_setSegment', '4212', '5842', 
				/* To change >> start */ 
				'<?php echo $_SESSION["products"]["enfants"][0]["ean"]; ?>'           // Id cart - required 
				/* To change << end */ 
				]); 
				(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src =(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidee s.com/p/tip/"; 
				var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t) }if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window) 
			</script> 
		<?php
		}else if( $strFormuleClasse == "reussite"){
		?>
			<script type="text/javascript"> 
				var tip = tip || []; 
				tip.push(['_setSegment', '4212', '5842', 
				/* To change >> start */ 
				'<?php echo $_SESSION["products"]["enfants"][0]["ean"]; ?>'           // Id cart - required 
				/* To change << end */ 
				]); 
				(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src =(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidee s.com/p/tip/"; 
				var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t) }if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window) 
			</script> 
		<?php
		}else if($strFormuleClasse == "tribu"){
		?>
			<script type="text/javascript"> 
				var tip = tip || []; 
				tip.push(['_setSegment', '4212', '5842', 
				/* To change >> start */ 
				/*'<?php
				echo $_SESSION["products"]["enfants"][0]["ean"].";";
				for($i= 0; $i<$_SESSION['nb_enfant'] ;$i++){
					if($i == 0)
						echo $_SESSION["products"]["enfants"][$i]["ean_reussite"];
					else
						echo ";".$_SESSION["products"]["enfants"][$i]["ean_reussite"];
				}
				?>'           // Id cart - required */
				/* To change << end */
				/* To change >> start */ 
				'<?php echo $_SESSION["products"]["enfants"][0]["ean"]; ?>'           // Id cart - required 
				/* To change << end */ 
				]); 
				(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src =(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidee s.com/p/tip/"; 
				var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t) }if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window) 
			</script> 
		<?php
		}
	}
	/* /Tag Universel Cart public idées */
	
	// var_dump($_CONST['URL_ACCUEIL']);
	if( isset($_SESSION['user']['IDCLIENT']) && $_SESSION['user']['IDCLIENT'] > 0 && $bStep2){
		//L'utilisateur est déjà connecté => redirection vers la page paiement
		echo "<script>document.location.href='".( ($_CONST['TYPE_ENVIRONNEMENT'] !='dev' ) ? $_CONST["HTTPS"] : $_CONST["URL2"]).$_CONST['URL_ACCUEIL']."abonnement/paiement-".$_SESSION['cart']['formule']['formule_classe'].".html';</script>";
		exit; 
	}
	
}

//Traitement du retour de la connexion via le WS PER
if(isset($_SESSION["connexion"]) && !empty($_SESSION['connexion'])){
	if($_SESSION["connexion"] == "ok"){
		unset($_SESSION["connexion"]);
		echo "<script>document.location.href='".( ($_CONST['TYPE_ENVIRONNEMENT'] !='dev' ) ? $_CONST["HTTPS"] : $_CONST["URL2"]).$_CONST['URL_ACCUEIL']."abonnement/paiement-".$_SESSION['cart']['formule']['formule_classe'].".html';</script>";
		exit; 
	}else if($_SESSION["connexion"] == "erreur_connexion"){
		$strStep = "step_1_3";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bError = true;
		$strError = "Vos identifiant et mot de passe sont incorrects !";
		$bPopupConnexion = true ; 
	}else if($_SESSION["connexion"] == "erreur_champ"){
		$strStep = "step_1_3";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bError = true;
		$strError = "Veuillez renseigner votre identifiant et mot de passe Bordas.";
		$bPopupConnexion = true ; 
	}else if($_SESSION["connexion"] == "erreur_profil"){
		$strStep = "step_1_3";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bError = true;
		$strError = "Vous devez posséder un compte Parent pour utiliser ce site. Si vous n’en avez pas, inscrivez-vous directement sur le site Bordas soutien scolaire.";
		$bPopupConnexion = true ; 
	}
	unset($_SESSION["connexion"]);
}
// unset($_SESSION["user"]);
// var_dump($strStep);
if( isset($_SESSION['mdp_forget']) && !empty($_SESSION['mdp_forget'])){
	$strStep = "step_1_3";
	
	
	
	
}
if( isset($_SESSION['creation_compte']) && !empty($_SESSION['creation_compte']) && $_SESSION['creation_compte'] === "ENCOURS"){
	// var_dump($_SESSION['creation_compte']);
	$strStep = "step_1_3";
	unset($_SESSION['creation_compte']);
	
}
// var_dump($strStep);
if(!isset($strStep) || empty($strStep))
	$strStep = "step_1_1"; 
// var_dump($strStep); 
?>
<div class="container">
   <div class="row">
      <div class="col-md-3 changer_formule">
		 <?php 
			$strLien = $oDb->queryItem("SELECT templates_name FROM  eco_templates WHERE templates_id = 5"); 
		
			//HZ LOT 3 FINAL : On vérifi si partenaire : on ne propose pas de changer de formule
			if(!isset($_SESSION['partenaire']['partenaire_id']) || empty($_SESSION['partenaire']['partenaire_id']))
				echo '<a href="'.$_CONST['URL2'] . $_CONST['URL_ACCUEIL'] . strToUrl($strLien) .'.html" class="btn txt-orange"><< Changer de formule</a>';
		?>
		
         
      </div>
      <ul id="steps" class="col-md-9">
         <li class="detail_formule"><a class="active"><strong>1.</strong> Détail de ma formule<span class="arrow_right"></span></a></li>
         <li class="confirmation"><a  ><span class="arrow_left"></span><strong>2.</strong>Paiement <span class="arrow_right"></span></a></li>
         <li class="paiement"><a ><span class="arrow_left"></span><strong>3.</strong>Confirmation <span class="arrow_right"></span></a></li>
      </ul>
   </div>
   <section class="description_formule">
      <div class="picto_formule">
         <img src="/images/<?php echo $_SESSION['cart']['formule']['formule_classe']; ?>_picto.png" alt="<?php echo $_SESSION['cart']['formule']['formule_libelle']; ?>">
      </div>
      <div class="right_description">
         <h2><?php echo $_SESSION['cart']['formule']['formule_libelle']; ?></h2>
         <?php echo $_SESSION['cart']['formule']['formule_desc2']; ?>
      </div>
   </section>
   <div class="detail_formule">
      <div class="title_formule">
         <h2>
            Je crée le compte de mon enfant
         </h2>
      </div>
      <div class="body_formule ">
         
		 <?php 
		if((isset($_POST['nb_enfant']) && $_POST['nb_enfant'] < 1)){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Veuillez sélectionner un nombre d’enfants.";
			$strError .= "</span></p>";
		}
		 if($strStep =="step_1_1" && $bError){
			echo ' <div class="alert alert-danger">
				'.$strError.'
			 </div>';
		 }
			/*Gestion des message d'erreur*/
			/*<div class="alert alert-success fermer">
				<img src="../images/success_icon.png" alt="">
				<a href="#" class="alert-link">Bien fait! les champs sont bien renseignés.</a>
			 </div>
			*/
			 
			//Gestion du formulaire (3 types de formulaire possible)
			if($_SESSION['cart']['formule']['formule_classe'] == 'ciblee'){
				//Récupération de la liste des classes disponibles
				$ListClasses  = $oDb->queryTab("SELECT c.* FROM bor_classe c 
															INNER JOIN bor_matiere_classe mc ON (mc.classe_id = c.classe_id) 
															INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id) 
															INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 														
														WHERE mc.mc_dispo = 1
														GROUP BY c.classe_id
														ORDER BY n.niveau_position, cn.cn_position");
				echo '	<form class="form-horizontal" role="form" method="post" id="creer_compte_enfant" name="form_enfant" >
							<div class="form-group">
							   <label for="inputEmail3" class="col-sm-3 control-label">Prénom de mon enfant : 
							   </label>
							   <div class="col-sm-3">
								  <input type="text" name="prenom[0]" class="form-control" id="inputEmail3" placeholder="Prénom" value="'.((isset($_SESSION["products"]["enfants"][0]["D_PRENOM"]) && !empty($_SESSION["products"]["enfants"][0]["D_PRENOM"])) ? $_SESSION["products"]["enfants"][0]["D_PRENOM"] : "" ).'">
							   </div>
							</div>
							<div class="form-group">
							   <label for="inputPassword3" class="col-sm-3 control-label">Classe :</label>
							   <div class="col-sm-3">
								  <select name="classe[0]" id="classe"  class="classe">
									 <option value="0">Choisir une classe</option>'; 	
										if($ListClasses){ 
											foreach($ListClasses as $classe){
												echo '<option value="'.$classe['classe_id'].'" '.(( isset($_SESSION["products"]["enfants"][0]["classe"]) && $_SESSION["products"]["enfants"][0]["classe"] == $classe['classe_id']) ? 'selected' : '' ) .'>'.$classe['classe_name'].'</option>'; 
											}  
										}
								echo '</select>
							   </div>
							</div>
							<div class="form-group ">
							   <label for="inputPassword3" class="col-sm-3 control-label">Matière :</label>
							   <div class="col-sm-3 ">
								<select name="matiere[0]" id="matiere" style="max-width: 100%;">
									 <option value="">Choisir une matière</option>'; 
									if(isset($_SESSION["products"]["enfants"][0]["classe"]) && !empty($_SESSION["products"]["enfants"][0]["classe"])){
										$where = 'mc.classe_id ='.mysql_real_escape_string((int)$_SESSION["products"]["enfants"][0]["classe"]);
										$where .= ' AND mc.mc_dispo = 1' ;
										$json = $oDb->queryTab("SELECT mc.matiere_id, m.matiere_titre  as mc_titre FROM bor_matiere_classe mc INNER JOIN bor_matiere m ON (m.matiere_id = mc.matiere_id) WHERE $where");
										foreach($json as $data){
											echo '<option value="'.$data['matiere_id'].'" '.(( isset($_SESSION["products"]["enfants"][0]["matiere"]) && $_SESSION["products"]["enfants"][0]["matiere"] == $data['matiere_id']) ? 'selected' : '' ) .'>'.$data['mc_titre'].'</option>'; 
										}
									}
						echo '	</select>
							   </div>
							   <div   style="clear: both;text-align: center;padding-top: 5px;">
								  <a href="#" class="autres_matieres_link"  data-toggle="modal" data-target="#popupmatieres_0">Voir les matières déjà disponibles et les dates des prochaines parutions</a> 
							   </div>
							</div>
							<div class="form-group">
							   <div class="col-sm-offset-3 col-sm-6 matiere_etape_suivant">
								  <a href="#" onclick="jQuery(\'#creer_compte_enfant\').submit();" class="btn btn-orange col-sm-offset-4"  style="margin-top:25px;">Étape suivante >></a>
							   </div>
							</div>
							<input type="hidden" name="action" value="step_1_1"/>
						</form>'; 
			
			}else if($_SESSION['cart']['formule']['formule_classe'] == 'reussite'){
				//Récupération de la liste des classes disponibles
				$ListClasses  = $oDb->queryTab("SELECT c.* FROM bor_classe c 
															INNER JOIN bor_produit p ON (p.classe_id_yonix = c.classe_id_yonix) 
															INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) 
															INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id) 
															INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 		
														WHERE f.formule_classe = 'reussite' 
														GROUP BY c.classe_id
														ORDER BY n.niveau_position, cn.cn_position"); 
				echo '	<form class="form-horizontal" role="form" method="post" id="creer_compte_enfant" name="form_enfant"> 
							<div class="form-group">
							   <label for="inputEmail3" class="col-sm-3 control-label">Prénom de mon enfant : 
							   </label>
							   <div class="col-sm-3">
								  <input type="text" name="prenom[0]" class="form-control" id="inputEmail3" placeholder="Prénom" value="'.((isset($_SESSION["products"]["enfants"][0]["D_PRENOM"]) && !empty($_SESSION["products"]["enfants"][0]["D_PRENOM"])) ? $_SESSION["products"]["enfants"][0]["D_PRENOM"] : "" ).'">
							   </div>
							</div>
							<div class="form-group">
							   <label for="inputPassword3" class="col-sm-3 control-label">Classe :</label>
							   <div class="col-sm-3">
								  <select name="classe[0]" id="classe"  class="classe">
									 <option value="0">Choisir une classe</option>'; 	
										if($ListClasses){ 
											foreach($ListClasses as $classe){
												echo '<option value="'.$classe['classe_id'].'" '.(( isset($_SESSION["products"]["enfants"][0]["classe"]) && $_SESSION["products"]["enfants"][0]["classe"] == $classe['classe_id']) ? 'selected' : '' ) .'>'.$classe['classe_name'].'</option>'; 
											}  
										}
								echo '</select>
							   </div>
							</div>
							<div class="row"   style="clear: both;text-align: center;padding-top: 5px;">
								 <a href="#" class="autres_matieres_link"  data-toggle="modal" data-target="#popupmatieres_0">Voir les matières déjà disponibles et les dates des prochaines parutions</a> 
								</div>
							<div class="form-group">
							   <div class="col-sm-offset-3 col-sm-6">
								  <a href="#" onclick="jQuery(\'#creer_compte_enfant\').submit();" class="btn btn-orange col-sm-offset-4"  style="margin-top:25px;">Étape suivante >></a>
							   </div>
							</div>
							<input type="hidden" name="action" value="step_2_1"/>
						</form>'; 
			}else if($_SESSION['cart']['formule']['formule_classe'] == 'tribu'){
				if((isset($_POST['nb_enfant']) && $_POST['nb_enfant'] > 1 && $_POST['nb_enfant'] <= 5))
					$_SESSION['nb_enfant'] = $_POST['nb_enfant'] ;
				
				echo '	<form class="form-horizontal" role="form" method="post" id="form_nb_enfant" name="form_nb_enfant">
								<div class="form-group">
								   <label for="inputPassword3" class="col-sm-3 control-label">Nombre d\'enfants :</label>
								   <div class="col-sm-3">
										<select name="nb_enfant" id="nb_enfant">
											 <option value="0">Choisir le nombre d\'enfants</option> 	
											 <option value="2" '.(($_POST['nb_enfant'] == 2 || $_SESSION['nb_enfant'] == 2) ? 'selected' : '' ) .'>2</option> 	
											 <option value="3" '.(($_POST['nb_enfant'] == 3 || $_SESSION['nb_enfant'] == 3) ? 'selected' : '' ) .'>3</option> 	
											 <option value="4" '.(($_POST['nb_enfant'] == 4 || $_SESSION['nb_enfant'] == 4) ? 'selected' : '' ) .'>4</option> 	
											 <option value="5" '.(($_POST['nb_enfant'] == 5 || $_SESSION['nb_enfant'] == 5) ? 'selected' : '' ) .'>5</option> 	
										</select>
								   </div>
								</div>
								
								<div class="form-group">
								   <div class="col-sm-offset-3 col-sm-6">
									  <a href="#" onclick="jQuery(\'#form_nb_enfant\').submit();" class="btn btn-orange col-sm-offset-4">Valider >></a>
								   </div>
								</div>
							</form>';
				if((isset($_POST['nb_enfant']) && $_POST['nb_enfant'] > 1 && $_POST['nb_enfant'] <= 5) || (isset($_SESSION['nb_enfant']) && $_SESSION['nb_enfant'] > 1 && $_SESSION['nb_enfant'] <= 5)){
					 
					$ListClasses  = $oDb->queryTab("SELECT c.* FROM bor_classe c 
															INNER JOIN bor_produit p ON (p.classe_id_yonix = c.classe_id_yonix) 
															INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) 
															INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id) 
															INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 		
														WHERE f.formule_classe = 'reussite' 
														GROUP BY c.classe_id
														ORDER BY n.niveau_position, cn.cn_position"); 
					echo '	<form class="form-horizontal" role="form" method="post" id="creer_compte_enfant" name="form_enfant">
								<input type="hidden" name="nbenfant" value="'.$_SESSION['nb_enfant'].'"/>';
					for($i=0; $i<$_SESSION['nb_enfant']; $i++){
						echo '
							<h3>Enfant '.($i+1).'</h3>
							<div class="form-group">
							   <label for="inputEmail3" class="col-sm-3 control-label">Prénom de mon enfant : 
							   </label>
							   <div class="col-sm-3">
								  <input type="text" name="prenom['.$i.']" class="form-control" id="inputEmail3" placeholder="Prénom" value="'.((isset($_SESSION["products"]["enfants"][$i]["D_PRENOM"]) && !empty($_SESSION["products"]["enfants"][$i]["D_PRENOM"])) ? $_SESSION["products"]["enfants"][$i]["D_PRENOM"] : "" ).'">
							   </div>
							</div>
							<div class="form-group">
							   <label for="inputPassword3" class="col-sm-3 control-label">Classe :</label>
							   <div class="col-sm-3">
								  <select name="classe['.$i.']" id="classe"  class="classe">
									 <option value="0">Choisir une classe</option>'; 	
										if($ListClasses){ 
											foreach($ListClasses as $classe){
												echo '<option value="'.$classe['classe_id'].'" '.(( isset($_SESSION["products"]["enfants"][$i]["classe"]) && $_SESSION["products"]["enfants"][$i]["classe"] == $classe['classe_id']) ? 'selected' : '' ) .'>'.$classe['classe_name'].'</option>'; 
											}  
										}
								echo '</select>
							   </div>
							</div>';
							echo '<div   style="clear: both;text-align: center;padding-top: 5px;" >
								 <a href="#" class="autres_matieres_link"  data-toggle="modal" data-target="#popupmatieres_'.$i.'">Voir les matières déjà disponibles et les dates des prochaines parutions</a> 
							   </div>'; 
					}
					echo '<div class="form-group">
							   <div class="col-sm-offset-3 col-sm-6">
								  <a href="#" onclick="jQuery(\'#creer_compte_enfant\').submit();" class="btn btn-orange col-sm-offset-4" style="margin-top:25px;">Étape suivante >></a>
							   </div>
							</div>
							<input type="hidden" name="action" value="step_3_1"/>
						</form>'; 
				}
			}
		 ?>
         
      </div>
      <!--***********************-->
      <div class="title_formule" id="choixformule" ><h2>Je choisis ma durée d’abonnement</h2></div>
	  <?php 
		if($strStep =="step_1_2"  || $strStep =="step_1_3" ){
			// var_dump($_SESSION["products"]["enfants"][0]); 
			$iFormuleId = $_SESSION['cart']['formule']['formule_id'];
			$strFormuleIdYonix = $_SESSION['cart']['formule']['formule_id_yonix'];
			$strFormuleClasse = $_SESSION['cart']['formule']['formule_classe'];
			
			//Récupération des 3 duree d'engagement disponible
			$strSql = "SELECT * FROM bor_duree_engagement ORDER BY de_position"; 
			$aDureeEngagement = $oDb->queryTab($strSql); 
			
			
			
			
			
			
			
	  ?>
			<div class="body_formule "  > 
	<?php
		if(isset($strErreurStep2) && !empty($strErreurStep2)){
			echo '<div class="alert alert-danger">
					<img src="/images/warning_icon.png" alt="">
					<a href="#" class="alert-link">'.$strErreurStep2.'</a>
				</div>';
			unset($strErreurStep2); 
		}
	?>
				<!--<div class="alert alert-success">
					<img src="../../images/success_icon.png" alt="">
					<a href="#" class="alert-link">Bien fait! les champs sont bien renseignés.</a>
				</div> 
				<div class="alert alert-danger">
					<img src="../../images/warning_icon.png" alt="">
					<a href="#" class="alert-link">Message d’erreur pour signaler un champs non renseigné.</a>
				</div>-->
				<?php 
					//Affichage de la liste des enfants 
					if($strFormuleClasse == "ciblee"){
						//Récupération du nom de la classe et la matière
						$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
						$strMatiere = $oDb->queryItem("SELECT matiere_titre FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['matiere'])."'");
						echo '<p class="detail_abonnement">Abonnement pour '.$_SESSION["products"]["enfants"][0]['D_PRENOM'].' en classe de '.$strClasse.' en '.$strMatiere.'.</p>'; 				
					}else if( $strFormuleClasse == "reussite"){
						//Récupération du nom de la classe et la matière
						$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
						echo '<p class="detail_abonnement">Abonnement pour '.$_SESSION["products"]["enfants"][0]['D_PRENOM'].' en classe de '.$strClasse.'.</p>'; 				
					}else if($strFormuleClasse == "tribu"){
						echo '<p class="detail_abonnement">'; 
						foreach($_SESSION["products"]["enfants"] as $aEnfant){
							$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($aEnfant['classe'])."'");
							$strMatiere = $oDb->queryItem("SELECT matiere_titre FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($aEnfant['matiere'])."'");
							echo 'Abonnement pour '.$aEnfant['D_PRENOM'].' en classe de '.$strClasse.'.<br/>'; 
						}
						echo '</p>';
					}
				?>
                                <!-- LOT6 -->
				<div class="form-group" style="">
                                    <form id="form_promo" method="post" name="form_promo">
					<label for="inputPassword3" class="col-sm-5 control-label">Si vous avez un <strong>code promotionnel</strong>, saississez-le :</label>
					<div class="col-sm-3 code_promotionnel">
						<input type="text" required class="form-control" name="codepromo" id="codepromo" value="<?php if(isset($_SESSION["CODEPROMO"]["CODE"]) && !empty($_SESSION["CODEPROMO"]["CODE"])) echo $_SESSION["CODEPROMO"]["CODE"] ; ?>" >
					</div>
					<div class="col-sm-2 valid_code_promotionnel">
                                            <?php
                                                if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
                                                        ?>
                                                            <input name="inputpromo" onclick="ga('send', 'event','bouton', 'clic', '', 4);" type="submit" class="form-control btn btn-orange " id="inputpromo" value="ok" >
                                                       <?php
                                                }else{
                                                        ?>
                                                            <input type="submit" class="form-control btn btn-orange " id="inputpromo" name="inputpromo" value="ok" >
                                                        <?php
                                                }
                                            ?>
						
					</div>
                                    </form>
				</div>  
                                <?php 
                                    if(isset($_SESSION["CODEPROMO"]["REDUCTION"]) && count($_SESSION["CODEPROMO"]["REDUCTION"]) > 0)
                                        echo '<p class="choix_duree" style="font-size: 18px;   margin-bottom: 0;">Grâce au code promotionnel, vous bénéficiez d’une remise sur votre premier abonnement à Bordas Soutien scolaire :</p>'; 
                                ?>
				<p class="choix_duree">Choississez la durée qui vous convient :</p>
				<div class="row packs_form">  
					<?php 
						//Affichage de la liste des offres
						foreach ($aDureeEngagement as $aDe){
							//Pour chaque duree d'engagement, on doit récupérer le prix d'un produit 
							$strSql = "SELECT produit_prix FROM bor_produit WHERE formule_id_yonix = '".mysql_real_escape_string($strFormuleIdYonix)."' AND de_id_yonix = '".mysql_real_escape_string($aDe['de_id_yonix'])."' LIMIT 1"; 
							$fPrix = $oDb->queryItem($strSql) ; 
							$fPrixMois = round( $fPrix / $aDe['de_engagement'] , 2 ) ; 
							echo '	<form id="form_choix_de_'.$aDe['de_id_yonix'].'" method="post" >
									<input type="hidden" name="action" value="step_1_2"/>
									<input type="hidden" name="de" value="'.$aDe['de_id_yonix'].'"/>
									<div class="pack_form '.$strFormuleClasse.' col-md-4">
										<div class="title_pack">'.$aDe['de_libelle'].'<br><span>'.$fPrixMois.'€ <span> / mois</span></span> </div>
										<div class="body_pack">'; 
                                                                                // LOT6
                                                                                if(isset($_SESSION["CODEPROMO"]["REDUCTION"][$aDe['de_id']]) && $_SESSION["CODEPROMO"]["REDUCTION"][$aDe['de_id']] > 0){
                                                                                    $newPrice = number_format(round( ($fPrix - ( $fPrix * $_SESSION["CODEPROMO"]["REDUCTION"][$aDe['de_id']] / 100)), 2), 2, ',', '' ) ; 
                                                                                    $strNewPrice = str_replace('.',',',$newPrice) ."€";
                                                                                }else
                                                                                    $strNewPrice =""; 
                                                                                
										if($aDe['de_engagement'] == 3){
                                                                                    if(!empty($strNewPrice)){
                                                                                        echo '<p class="paiement_pack">Paiement en une seule fois,<br>soit<span> <strong> <strike>'.str_replace('.',',',$fPrix).'€</strike> '. $strNewPrice.'</strong> <br>les 3 premiers mois puis <br><trong>'.str_replace('.',',',$fPrix).'€</strong> tous les 3 mois</span></p>';
                                                                                    }else
											echo '<p class="paiement_pack">Paiement en une seule fois,<br>soit<span> <strong> '.str_replace('.',',',$fPrix).'€</strong> tous les 3 mois</span></p>';
                                                                                }else if ($aDe['de_engagement'] == 12){
                                                                                    if(!empty($strNewPrice)){
                                                                                        echo '<p class="paiement_pack">Paiement en une seule fois,<br>soit<span> <strong> <strike>'.str_replace('.',',',$fPrix).'€</strike></strong> <br><strong>'. $strNewPrice.'</strong> pour 1 an</span></p>';
                                                                                    }else
											echo '<p class="paiement_pack">Paiement en une seule fois,<br>soit<span> <strong> '.str_replace('.',',',$fPrix).'€</strong> pour 1 an</span></p>';
                                                                                }else{
                                                                                    if(!empty($strNewPrice)){
                                                                                        echo '<p class="paiement_pack">Paiement en une seule fois,<br>soit<span> <strong> <strike>'.str_replace('.',',',$fPrix).'€</strike> '. $strNewPrice.'</strong> <br>le premier mois puis <br><trong>'.str_replace('.',',',$fPrix).'€</strong> / mois</span></p>';
                                                                                    }else
											echo '<p class="paiement_pack">Paiement mensuel,<br>soit<span> <strong> '. $strNewPrice.' '.str_replace('.',',',$fPrix).'€</strong> / mois</p>'; 
                                                                                }
                                                                                
									echo '	<div class="center"><a href="#" onclick="jQuery(\'#form_choix_de_'.$aDe['de_id_yonix'].'\').submit();" class="btn btn-orange">Je choisis</a></div>
											<p class="description_pack">
												'.$aDe['de_condition'].'
											</p>
										</div>    
									</div>
									</form>';
						}
					?>
					
					
				</div>   
			</div>    
		<?php 
			}
		?>
			
      <!--***********************-->
      <div class="title_formule" id="compteparent"><h2>Je crée mon compte parent</h2></div>
		<?php 
			if( $strStep =="step_1_3" ){                            
		?>
				<div class="body_formule " id="creer_mon_compte">    
					<p>Inscrivez-vous pour bénéficier gratuitement d’un accès privilégié aux sites des Éditions Bordas, dont <strong>Bordas Soutien scolaire</strong></P>
					<p>Si vous avez déjà un compte «Bordas»,     <a  class="btn_inscription" data-toggle="modal" data-target="#inscription" id="btn_connexion">connectez-vous</a> pour passer à l’étape suivante.</p>
					<!-- Modal -->
					<?php 
					// var_dump(
					if($strStep =="step_1_3" && $bError && !$bPopupConnexion){
						echo ' <div class="alert alert-danger red">
							'.$strError.'
						 </div>';
					 }
					 ?>
					 
<div class="modal fade" id="inscription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Vous êtes déjà inscrit ?</h4>
				<?php 
					if(isset($bPopupConnexion) && $bPopupConnexion){
						echo '<div class="" id="news-warning" >
									<div class="hero-unit text-center red">
										'.$strError.'
									</div>              
								</div>';
					}
				?>
			</div>
			<div class="modal-body">
				<p>Merci de vous identifier :</p>
				<form class="form-horizontal" role="form" action="/proxy/proxy_connexion.php" method="post">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-5 control-label">Votre identifiant :</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="inputEmail3" value="" name="username">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-5 control-label">Votre mot de passe :</label>
						<div class="col-sm-7">
							<input type="password" class="form-control" id="inputPassword3"  name="password">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-5 col-sm-10">
							<button type="submit" class="btn btn-orange">Connexion »</button>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
						<div class="checkbox">
							<a href="#" onclick="jQuery('#inscription').removeClass('in');jQuery('#inscription').hide();jQuery('.modal-backdrop').remove();" class="mot_oublie" data-toggle="modal" data-target="#motdepasseoublie" id="forget_mdp"> Identifiant ou mot de passe oublié ?</a>
						</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
		jQuery.noConflict();	
</script>

					<?php
					// var_dump('tete'); 
					include("./proxy/proxy_createAccount_abo.php");
					// var_dump('tete'); 
						
					?>
					
					<div class="formulaire_form"> 
					<p class="obligatoire"> <strong>*</strong> champs obligatoire</p>
					<p class="description_pack">
					Conformément à la loi Informatique et Libertés du 6 janvier 1978 modifiée en 2004, vous disposez d’un droit d’accès, de rectification ou d’opposition aux données personnelles vous concernant, à la Relation client BORDAS : <a href="mailto:soutien-scolaire@bordas.tm.fr">soutien-scolaire@bordas.tm.fr</a>, <?php echo $_CONST['RC_TEL']; ?>.
					</p>
					</div>    
				</div>
		<?php 
			}
		?>
   </div>
</div>
 
 
<?php 


//Création d'un popup pour tous les enfants 
if(isset($_SESSION['nb_enfant']) && $_SESSION['nb_enfant'] > 0 ){
	for($i=0; $i<$_SESSION['nb_enfant']; $i++){
		echo '
		<!-- Modal pop up autres matieres -->
		<div class="modal fade popupmatieres" id="popupmatieres_'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		   <div class="modal-dialog">
			  <div class="modal-content">
				 <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Liste des matières disponibles en <span id="modal_matiere_titre_'.$i.'"></span></h4>
					<div class="" id="news-warning" >
						<div class="hero-unit text-center red" id="error_'.$i.'">
						</div>              
					</div>
					<div class="" id="news-success" >
						<div class="hero-unit text-center green" style="color:green;" id="success_'.$i.'">
						</div>              
					</div>
				 </div>
					<div class="modal-body" id="body_matiere_'.$i.'">

					</div>
					<div class="modal-footer" id="modal_matiere_body_'.$i.'" style="display:none;">
						<div class="spacer"></div>
						<form method="post">
						<div class="form-group">
						   <label for="inputPassword3" class=" control-label">Si vous souhaitez être informé lorsque de nouvelles
matières sont disponibles, laissez-nous votre email : </label>
						   <div class="col-sm-10">
							  <input type="email" class="form-control" id="inputEmail_'.$i.'" required name="modal_email">
						   </div>
						   <div class="col-sm-2">
							  <input type="button"  onclick="valider_formulaire('.$i.');" class="form-control btn btn-orange" id="inputEmail3" value="ok">
							  <input type="hidden" name="action" value="save_classe"/>
							  <input type="hidden" id="modal_classe_'.$i.'" name="modal_classe" value=""/>
						   </div>
						</div>
						</form>
					</div>
			  </div>
		   </div>
		</div>';
	}
}else{
	echo '
	<div class="modal fade popupmatieres" id="popupmatieres_0" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	   <div class="modal-dialog">
		  <div class="modal-content">
			 <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Liste des matières disponibles en <span id="modal_matiere_titre_0"></span></h4>
				<div class="" id="news-warning" >
					<div class="hero-unit text-center red" id="error_0">
					</div>              
				</div>
				<div class="" id="news-success" >
					<div class="hero-unit text-center green"   style="color:green;" id="success_0">
					</div>              
				</div>
			 </div>
				<div class="modal-body" id="body_matiere_0">
				</div>
			<form method="post">
			   <div class="modal-footer" id="modal_matiere_body_0"  style="display:none;"> 
					<div class="spacer"></div>
					<div class="form-group">
					   <label for="inputPassword3" class=" control-label">Si vous souhaitez être informé lorsque de nouvelles
matières sont disponibles, laissez-nous votre email : </label>
					   <div class="col-sm-10">
						   <input type="email" class="form-control" id="inputEmail_0" required name="modal_email">
					   </div>
					   <div class="col-sm-2 valid_matiere_button">
						    <input type="button" onclick="valider_formulaire(0);" class="form-control btn btn-orange " id="inputEmail3" value="ok">
							<input type="hidden" name="action" value="save_classe"/>
							<input type="hidden" id="modal_classe_0" name="modal_classe" value=""/>
					   </div>
					</div>
				</div>
				</form>
				
		
			 
		  </div>
	   </div>
	</div>
';
}

?>







<div class="modal fade" id="motdepasseoublie" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Vous avez oublié votre mot de passe ?</h4>
				<?php 
					$bPostForget = false;
					$bForget = false;
					if( isset($_SESSION['mdp_forget']) && !empty($_SESSION['mdp_forget'])){
						$bForget =true ;
						if($_SESSION['mdp_forget'] == "ok" ){
							$bPostForget = true;
							$strMessage = "Un e-mail vient de vous être envoyé. Veuillez suivre les instructions qui s'y trouvent.";
						}else if($_SESSION['mdp_forget'] == "erreur_update" ){
							$strMessageBox = "Cette adresse email n'est pas reconnue sur ce site. Si vous avez changé d'adresse email, veuillez  nous contacter." ; 
						}else if($_SESSION['mdp_forget'] == "erreur_champ" ){
							$strMessageBox = "Veuillez renseigner votre email." ;  
						}else if($_SESSION['mdp_forget'] == "erreur_technique" ){
							$strMessageBox = "Un problème technique empêche la récupération du mot de passe." ; 
						}else if($_SESSION['mdp_forget'] == "champ_incorrect" ){
							$strMessageBox = "Veuillez renseigner une adresse email valide." ; 
						}
						unset( $_SESSION['mdp_forget'] ) ;
						
					}
					if($bPostForget){
						echo '	<div class="" id="news-success" >
									<div class="hero-unit text-center green">
										'.$strMessage.'
									</div>              
								</div>';
					}else if(isset($strMessageBox) && !empty($strMessageBox)){
						echo '<div class="" id="news-warning" >
								<div class="hero-unit text-center red">
									'.$strMessageBox.'
								</div>              
							</div>';
					}
				?>
			</div>
			<div class="modal-body">
			<?php
				if(!$bPostForget){
			?>
				<form action="/proxy/proxy_mdpForget.php"  method="post" id="forgot-form">	
					
					<div class="modal-body row">
							<p>Pour retrouver l'identifiant et le mot de passe que vous avez créés lors de votre inscription sur le site des Editions Bordas ou de Bordas Soutien scolaire, merci de nous 
indiquer l'adresse e-mail saisie lors de cette inscription. Veuillez suivre les instructions qui figurent dans l'e-mail que vous allez recevoir d'ici quelques minutes.</p>
							
					</div>
					<div class="modal-body row">
					<label>Entrez l’adresse e-mail associée à votre inscription :</label><br>
							<input type="email" id="txtEmail" name="email_forgot" value="" required>
							<input type="hidden" name="redirect_after" value="<?php echo ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ). $_CONST['URL_ACCUEIL']  ; ?>abonnement/formule-<?php echo $_SESSION['cart']['formule']['formule_classe'];?>.html" >
						
					</div>
					
					<div class="modal-footer">
						<button type="submit" class="btn btn-orange">Valider »</button>
					</div>
				</form>	 
			<?php
			}
			unset($_SESSION['mdp_forget']);
			?>
			</div>
		</div>
	</div>
</div>














<?php 
	if(isset($bPopupConnexion) && $bPopupConnexion){
?>
		<script type="text/javascript">
			jQuery('document').ready(function(){
				jQuery('#btn_connexion').click();
			});
		</script>
<?php
	}
	if($bForget){
?>
	<script type="text/javascript">
		jQuery('document').ready(function(){
			jQuery('#forget_mdp').click();
			
		});
	</script>
<?php
	}
?>



<!-- ajax call -->
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.classe').each(function(){
			jQuery.ajax({type:"POST", data:jQuery(this).serialize(), url: "/templates/ajax/bloc_matiere.php" ,dataType: 'script'	});
		});
		
		// liste classe du matiere selectionne
		jQuery('.classe').change(function () {
			// alert('test'); 
			
			jQuery.ajax({type:"POST", data: jQuery(this).serialize(), url: "/templates/ajaxsearch.php" ,dataType: 'json',	success: function(json) {
					jQuery('#matiere option').remove(); 
					jQuery('#matiere').append('<option value="">Choisir une matière</option>');
					jQuery.each(json, function(index, value) { 
						jQuery('#matiere').append('<option value="'+ json[index]['matiere_id'] +'">'+ json[index]['mc_titre'] +'</option>');
					});
				},
				error: function(){
					jQuery('#matiere').html('');
				}
			});
			
			// update_popup(0);
			jQuery.ajax({type:"POST", data: jQuery(this).serialize(), url: "/templates/ajax/bloc_matiere.php" ,dataType: 'script'	});
			
			 return false;
		 
		});	
		
	});	  
	function valider_formulaire(iPopup){
		// alert(iPopup);
		bVerif = true ; 
		jQuery('#error_'+iPopup).html(''); 
		jQuery('#success_'+iPopup).html(''); 
		strEmail = jQuery('#inputEmail_'+iPopup).val() ; 
		iClasse = jQuery('#modal_classe_'+iPopup).val() ; 
		// alert(iClasse); 
		if( strEmail == "" ){
			bVerif = false ; 
			jQuery('#error_'+iPopup).html('Veuillez saisir votre email.'); 
		}
		if( !(iClasse > 0) ){
			bVerif = false ; 
			jQuery('#error_'+iPopup).html('Veuillez saisir la classe de votre enfant.'); 
		}
		
		if(bVerif){
			// alert('ok'); 
			jQuery.ajax(
				{
					type:"POST", 
					data: 
						{
							iPopup : iPopup, 
							iClasse : iClasse, 
							strEmail : strEmail
						}, 
					url: "/templates/ajax/save_email.php" 
				}
			).done(function( msg ) {
				if(msg == 1 ) 	
					jQuery('#success_'+iPopup).html('Nous avons bien enregistré votre demande.');
				else
					jQuery('#error_'+iPopup).html('Veuillez saisir une adresse email valide.');
			});
		}
		
	}
	// function update_popup(iPopup){
		// $.ajax({type:"POST", data: $(this).serialize(), url: "/templates/ajax/bloc_matiere.php" ,dataType: 'script'	});
	// }
</script>

<script type="text/javascript">

	


jQuery(window).load(function(){
	<?php 
		if(isset($strStep) && $strStep == "step_1_2") {
	?>
			jQuery('#choixformule').ScrollTo({
				duration: 1000
			});
	<?php
		}
		if(isset($strStep) && $strStep == "step_1_3") {
	?>
			jQuery('#compteparent').ScrollTo({
				duration: 1000
			});
	<?php
		}
	?>
});
</script>