<?php 
// var_dump($_SESSION['user']);
//Récupération du prix en fonction du produit 
$strSql = "SELECT * FROM bor_produit p INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix) WHERE p.produit_ean = '".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["ean"])."'"; 
$aProduct = $oDb->queryRow($strSql); 
// unset($_SESSION['user']);
// var_dump($_SESSION['user']);
if(!isset($_SESSION['cart']['formule']) || !count($_SESSION['cart']['formule'])){
	echo "<script>document.location.href='".$_CONST['URL2'] . $_CONST['URL_ACCUEIL']."abonnement.html';</script>";
	exit;
}
	


//Vérification que le produit est bien en base 
$strSql = "SELECT * FROM bor_produit p INNER JOIN bor_duree_engagement d ON(d.de_id_yonix = p.de_id_yonix) INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) WHERE p.produit_ean = '".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["ean"])."'"; 
$aProduit = $oDb->queryRow($strSql); 
$bProduit = true; 
if($aProduit["formule_classe"] == "tribu"){
	foreach( $_SESSION["products"]["enfants"] as $aEnfant){
		$strSql = "SELECT * FROM bor_produit p INNER JOIN bor_duree_engagement d ON(d.de_id_yonix = p.de_id_yonix) INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix) WHERE c.classe_id = '".mysql_real_escape_string($aEnfant["classe"])."' AND f.formule_classe = 'reussite' AND d.de_id = '".mysql_real_escape_string($aEnfant["duree_engagement"])."' "; 
		$aProduit = $oDb->queryRow($strSql); 
		if(!$oDb->rows){
			$bProduit = false ; 
		}
	}
}

// var_dump($_SESSION["user"]) ; 
// var_dump($_SESSION) ; 
 
 
if(!$bProduit ){

	$strMessage = "Produit non retrouvé sur l'étape 2 (paiement) du tunnel de commande.<br><br>" ; 
	
	$strMessage .= "Clients<br>" ;
	$strMessage .= print_r($_SESSION["user"], true) ;
	$strMessage .= "<br><br>"; 
	$strMessage .= "Formule<br>" ;
	$strMessage .= print_r($_SESSION['cart']["formule"], true) ;
	$strMessage .= "<br><br>"; 
	$strMessage .= "Liste des enfants<br>" ;
	$strMessage .= print_r($_SESSION["products"], true) ;
	$mail = new intyMailer(); 
	// OPTIONAL 
	$mail->set(array('charset'  , 'utf-8')); 
	$mail->set(array('priority' , 3)); 
	$mail->set(array('replyTo'  , "noreply@bordas.tm.fr")); 
	$mail->set(array('alert '    ,  true)); 
	$mail->set(array('html'     ,  true)); 
	// OPTIONAL 
	$mail->set(array('subject' , "[".$_CONST["SITE_NAME"]."] Commande : Produit non retrouvé")); 
	$mail->set(array('message' , $strMessage)); 
	$mail->set(array('sending' , 'FROM' , "Bordas Soutien Scolaire" ,"noreply@bordas.tm.fr")); 
	$aStrEmail = "hz.hedizouari@gmail.com";
	$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
	$mail->send();
	$aStrEmail = "hgonzalez-quijano@sejer.fr";
	$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
	$mail->send();
	$aStrEmail = "vbesakian@sejer.fr";
	$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
	$mail->send();
}
// var_dump($bProduit); 
	
//Traitement du formulaire 
$bVerif = true ;
if(isset($_POST) && count($_POST)>0 ){
	// var_dump($_POST);
	//Verificatio sur les différent champs 
	$bVerif = true ; 
	$bDonne = false;
	if( $_SESSION['products']['enfants'][0]['duree_engagement'] < 3  && (!isset($_POST['cb_name']) || empty($_POST['cb_name']))){
		$bVerif = false; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Veuillez renseigner le nom du détenteur de la carte.";
		$strError .="</span></p>";
	}
	if( (!isset($_POST['cp_type']) || empty($_POST['cp_type']))){
		$bVerif = false; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Veuillez sélectionner le type de votre carte."; 
		$strError .="</span></p>";
	}
	if( (!isset($_POST['cb_numcarte']) || empty($_POST['cb_numcarte']))){
		$bVerif = false; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Veuillez sélectionner votre n° de carte bancaire.";
		$strError .="</span></p>"; 
	}
	
	if( !preg_match("/^[0-9]{16}$/", $_POST['cb_numcarte'])){
		$bVerif = false; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Votre n° de carte n’est pas conforme, il doit contenir 16 chiffres."; 
		$strError .="</span></p>";
	}
	if(( (!isset($_POST['cb_annee']) || empty($_POST['cb_annee'])) || (!isset($_POST['cb_mois']) || empty($_POST['cb_mois'])) )){
		$bVerif = false; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Veuillez sélectionner la date d’expiration de votre CB."; 
		$strError .="</span></p>";
	}
	$strCarteDate = $_POST['cb_annee'].'-'.$_POST['cb_mois'];
	$strDate = date('y-m');
	if($strCarteDate < $strDate){
		$bVerif = false; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "La date d'expiration de la carte est dépassée."; 
		$strError .="</span></p>";
	}
	if((!isset($_POST['cb_controle']) || empty($_POST['cb_controle']))){
		$bVerif = false; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Veuillez sélectionner votre n° de contrôle."; 
		$strError .="</span></p>";
	}
	if( !preg_match("/^[0-9]{3}$/", $_POST['cb_controle'])){
		$bVerif = false; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Votre n° de contrôle n’est pas conforme, il doit contenir 3 chiffres."; 
		$strError .="</span></p>";
	}
	
	if( (!isset($_POST['cb_cgv']) || empty($_POST['cb_cgv']))){
		$bVerif = false; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Il faut accepter les Conditions Générales de Vente pour valider votre commande.";
		$strError .="</span></p>";
		if( (!isset($_POST['cb_donnee']) || empty($_POST['cb_donnee'])) &&  $_SESSION['products']['enfants'][0]['duree_engagement'] < 3  ){
			$bDonne = true;
		}
	}
	$bVerifDonnees = true;
	if( (!isset($_POST['cb_donnee']) || empty($_POST['cb_donnee'])) &&  $_SESSION['products']['enfants'][0]['duree_engagement'] < 3  ){
		$bVerif = false; 
		$bVerifDonnees = false; 
		$bDonne = true;
		// $strError = "Il faut accepter la conservation sécurisée de vos données bancaire pour valider votre commande."; 
	}
	//TVA
	//if( (isset($_POST['facturation_pays']) && !empty($_POST['facturation_pays']))){
		//Le client à choisi de saisir une adresse de facturation
		// var_dump($_POST['facturation_ville']); 
		if( (!isset($_POST['facturation_ville']) || empty($_POST['facturation_ville']))){
			$bVerif = false; 
			$bBlocFacturation = true;
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "La ville de facturation n'est pas valide."; 
			$strError .="</span></p>";
		}
		/*if( (!isset($_POST['facturation_adresse']) || empty($_POST['facturation_adresse']))){
			$bVerif = false; 
			$bBlocFacturation = true;
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "L'adresse de facturation n'est pas remplie.";
			$strError .="</span></p>"; 
		}*/
		if( (!isset($_POST['facturation_numero_voie']) || empty($_POST['facturation_numero_voie']))){
			$bVerif = false; 
			$bBlocFacturation = true;
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Le numéro de voie n'est pas rempli.";
			$strError .="</span></p>"; 
		}
		if( !preg_match("/^[0-9A-Za-zàáâãäåçèéêëìíîïðòóôõöùúûüýÿÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÐÒÓÔÕÖÙÚÛÜÝŸ ]{1,10}$/", $_POST['facturation_numero_voie'])){
			$bVerif = false; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Le numéro de voie n'est pas valide. Il ne peut contenir que 10 caractères alphanumériques, sans ponctuation."; 
			$strError .="</span></p>";
		}
		if( (!isset($_POST['facturation_nom_voie']) || empty($_POST['facturation_nom_voie']))){
			$bVerif = false; 
			$bBlocFacturation = true;
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Le nom de voie n'est pas rempli.";
			$strError .="</span></p>"; 
		}
		if( !preg_match("/^[0-9A-Za-zàáâãäåçèéêëìíîïðòóôõöùúûüýÿÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÐÒÓÔÕÖÙÚÛÜÝŸ' ]{1,23}$/", $_POST['facturation_nom_voie'])){
			$bVerif = false; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Le nom de voie n'est pas valide. Il ne peut contenir que 22 caractères alphanumériques, sans ponctuation."; 
			$strError .="</span></p>";
		}
		if( (!isset($_POST['facturation_pays']) || empty($_POST['facturation_pays']))){
			$bVerif = false; 
			$bBlocFacturation = true;
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Le pays de facturation n'est pas valide."; 
			$strError .="</span></p>";
		}
                if( $_POST['facturation_pays'] == 1 && (!isset($_POST['facturation_cp']) || empty($_POST['facturation_cp']))){
			$bVerif = false; 
			$bBlocFacturation = true;
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Le code postal est obligatoire."; 
			$strError .="</span></p>";
		}
		
	//}
	
	
		$aCbType = array ( 1 => "CB" , 2 => "EU", 3 => "CB"); 
		//Aucune erreur traitement de la commande 
		if((isset($_POST['facturation_pays']) && !empty($_POST['facturation_pays']))){
			//$_SESSION['shipping']['ADR1'] = $_POST['facturation_adresse'];
			$_SESSION['shipping']['ADR1'] = $_POST['facturation_numero_voie'].' '.$_POST['facturation_type_voie'].' '.$_POST['facturation_nom_voie'];
			$_SESSION['shipping']['ADR3'] = $_POST['facturation_complement'];
			$_SESSION['shipping']['CPLIVRAISON'] = $_POST['facturation_cp'];
			$_SESSION['shipping']['VILLELIVRAISON'] = $_POST['facturation_ville'];
			$_SESSION['shipping']['pays_id'] = $_POST['facturation_pays'];
		}
		//var_dump($_SESSION['shipping']);exit;
		// var_dump(	$_SESSION['shipping']['VILLELIVRAISON'] );
		$_SESSION['reglement']['cb_name'] = $_POST['cb_name']  ; 
		$_SESSION['reglement']['TOTALCOMMANDE'] = $aProduct['produit_prix']  ; 
		$_SESSION['reglement']['TYPECB'] = $aCbType[$_POST['cp_type']];
		$_SESSION['reglement']['NUMCB'] = $_POST['cb_numcarte'];
		$_SESSION['reglement']['MOISCB'] = $_POST['cb_mois'];
		$_SESSION['reglement']['ANNEECB'] = $_POST['cb_annee'];
		$_SESSION['reglement']['NUMCONTROLECB'] = $_POST['cb_controle'];
		
	//Vérification de la présence d'une autre commande pour le même client dans un délai de 30 seconde pour éviter les doublons
	$strSql = "SELECT commande_num FROM bor_commande WHERE client_id = '".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."' AND commande_date > ADDDATE(NOW(), INTERVAL -30 SECOND) LIMIT 1 "; 
	$strCommandeNum = $oDb->queryItem($strSql); 
	if($oDb->rows > 0 ){
		//Une commande existe pour le client donc il s'agit d'un doublon on récupère le num commande 
		$_SESSION['cart']['NUMCOMMANDE'] = $strCommandeNum;
		$_SESSION['paiement']['statut'] = "OK"; 
		echo "<script>document.location.href='".( ($_CONST['TYPE_ENVIRONNEMENT'] !='dev' ) ? $_CONST["HTTPS"] : $_CONST["URL2"]) . $_CONST['URL_ACCUEIL']."abonnement/confirmation-".$_SESSION['cart']['formule']['formule_classe'].".html';</script>";
		exit;
	}else if($bVerif){	
		
		
		$strCommandeLast = $oDb->queryRow("SELECT  commande_num  as commande_num FROM bor_commande ORDER BY commande_id DESC LIMIT 1");
		
		 
		
                
                if($strCommandeLast['commande_num']!="" ) {
                    if(strtolower($_CONST['TYPE_ENVIRONNEMENT']) == 'dev'){
			$strCommandeLast = $strCommandeLast['commande_num'];
			$strCommandeNum = "BSSTST" . sprintf("%04d",((int)str_replace("BSSTST", "", $strCommandeLast)) + 1 ); 
                    }else if(strtolower($_CONST['TYPE_ENVIRONNEMENT']) == 'preprod'){
                        $strCommandeLast = $strCommandeLast['commande_num'];
			$strCommandeNum = "BSSPRP" . sprintf("%04d",((int)str_replace("BSSPRP", "", $strCommandeLast)) + 1 ); 
                    }else if(strtolower($_CONST['TYPE_ENVIRONNEMENT']) == 'prod'){
                        $strCommandeLast = $strCommandeLast['commande_num'];
			$strCommandeNum = "BSS" . sprintf("%07d",((int)str_replace("BSS", "", $strCommandeLast)) + 1 ); 
                    }
		}
		
			
		$_SESSION['cart']['NUMCOMMANDE'] = $strCommandeNum;
		
			// echo "test"; 
			// $oAbo = new aboEditis();
			// $oAbo->initCommande();
			// $a = $oAbo->setCommande();
				
				// exit; 
		
		
		$oBill = new fluxBill();
		
		// LOT 4 FINAL
                $strAction = ""; 
                //LOT6
                $strPromo = "";
                $strReduction = "" ; 
                if(isset($_SESSION["products"]["enfants"][0]["REDUCTION"]) && !empty($_SESSION["products"]["enfants"][0]["REDUCTION"]))
                   $strReduction =  $_SESSION["products"]["enfants"][0]["REDUCTION"];
                if(isset($_SESSION["products"]["enfants"][0]["CODEPROMO"]) && !empty($_SESSION["products"]["enfants"][0]["CODEPROMO"]))
                    $strPromo = $strAction = $_SESSION["products"]["enfants"][0]["CODEPROMO"];
		else if(isset($_SESSION['partenaire']['bill_codeaction_commande']) && !empty($_SESSION['partenaire']['bill_codeaction_commande']))
                    $strAction = $_SESSION['partenaire']['bill_codeaction_commande'];
		
		$oBill->Init(false, $strAction);
		$oBill->SetClient();
		$oBill->SetShipping();
		$oBill->SetCoord();
                //LOT6
                //var_dump($strPromo); exit; 
		$oBill->SetProducts(false,$strPromo); 
		$oBill->SetReglement();
		$oBill->Close();
		
		// var_dump($oBill->strFluxXML); 
// exit; 		
		$aRetour = $oBill->SendBill(); 
		
		if(!$aRetour['erreur']){
			// Insertion dans la table commande
			$strSql = "INSERT INTO bor_commande (client_id,
												produit_ean, 
												commande_num, 
												commande_total, 
												commande_statut,
												commande_date,
                                                                                                commande_promo, 
                                                                                                commande_reduction
												) VALUES (
												'".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."',
												'".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["ean"])."',
												'".mysql_real_escape_string($_SESSION['cart']['NUMCOMMANDE'])."',
												'".mysql_real_escape_string($_SESSION['reglement']['TOTALCOMMANDE'])."',
												'1', 
												NOW(),
                                                                                                '".mysql_real_escape_string($strPromo)."',
												'".mysql_real_escape_string($strReduction)."'
												) "; 
			$oDb->query($strSql); 
			$iCommandeId = $oDb->last_insert_id; 
			$_SESSION['cart']['IDCOMMANDE'] = $iCommandeId ; 
			//Insertion dans la table enfants
			foreach ( $_SESSION["products"]["enfants"] as $aEnfants){
				$strSql = "INSERT INTO bor_commande_enfant ( commande_id, 
															 classe_id,
															 matiere_id,
															 formule_id,
															 de_id,
															 enfant_prenom,
															 enfant_ean, 
															 enfant_ean_reussite 
															) VALUES (
															 '".mysql_real_escape_string($iCommandeId)."',
															 '".mysql_real_escape_string($aEnfants['classe'])."',
															 '".mysql_real_escape_string($aEnfants['matiere'])."',
															 '".mysql_real_escape_string($_SESSION["cart"]["formule"]['formule_id'])."',
															 '".mysql_real_escape_string($aEnfants['duree_engagement'])."',
															 '".mysql_real_escape_string($aEnfants['D_PRENOM'])."',
															 '".mysql_real_escape_string($aEnfants['ean'])."',
															 '".mysql_real_escape_string($aEnfants['ean_reussite'])."'
															)"; 
				$oDb->query($strSql); 														 
			}
			
			//Insertion dans la table facture
			if(isset($_SESSION['shipping']) && count($_SESSION['shipping'])>0){
				$strSql = "INSERT INTO bor_commande_facture ( commande_id, 
															  pays_id, 
															  facture_adr1, 
															  facture_adr3, 
															  facture_ville, 
															  facture_cplivraison
															 ) VALUES (
															  '".mysql_real_escape_string($iCommandeId)."',
															  '".mysql_real_escape_string($_SESSION['shipping']['pays_id'])."',
															  '".mysql_real_escape_string($_SESSION['shipping']['ADR1'])."',
															  '".mysql_real_escape_string($_SESSION['shipping']['ADR3'])."',
															  '".mysql_real_escape_string($_SESSION['shipping']['VILLELIVRAISON'])."',
															  '".mysql_real_escape_string($_SESSION['shipping']['CPLIVRAISON'])."'
															 )";
				$oDb->query($strSql); 
			}
			
			$strSql = "UPDATE bor_client SET acces_zoe = 1 WHERE client_id ='".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."'"; 
			$oDb->query($strSql);
			//On effectu appel le ws abonnement pour les durees inférieur à 1 an (id duree engagement 1 et 2 )  
			if( $_SESSION["products"]["enfants"][0]['duree_engagement']  < 3){			
				$oAbo = new aboEditis();
				// LOT 4 FINAL LOT6 
                                $strAction = "";
                                if(isset($_SESSION['partenaire']['bill_codeaction_commande']) && !empty($_SESSION['partenaire']['bill_codeaction_commande']))
                                    $strAction = $_SESSION['partenaire']['bill_codeaction_commande'];
				$oAbo->initCommande($strAction,$strPromo);
				$a = $oAbo->setCommande();
			}
			// var_dump($a); exit; 
			$_SESSION['paiement']['statut'] = "OK"; 
			echo "<script>document.location.href='".( ($_CONST['TYPE_ENVIRONNEMENT'] !='dev' ) ? $_CONST["HTTPS"] : $_CONST["URL2"]) . $_CONST['URL_ACCUEIL']."abonnement/confirmation-".$_SESSION['cart']['formule']['formule_classe'].".html';</script>";
			exit; 
			
		}else{
			$bVerif = false ; 
			// var_dump($aRetour['code']); 
			$_SESSION['paiement']['statut'] = "KO"; 
			$_SESSION['paiement']['code'] = $aRetour['code']; 
                        
                        // HZ 20150123 : En cas d'erreur bill insertion de la commande avec statut -1, 
                        // cela permet d'accrémeter le num commande en cas de doublon pour ne pas rester bloquer sur le même num
                        $strSql = "INSERT INTO bor_commande (client_id,
                                                    produit_ean, 
                                                    commande_num, 
                                                    commande_total, 
                                                    commande_statut,
                                                    commande_date, 
                                                    commande_activation
                                                    ) VALUES (
                                                    '".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."',
                                                    '".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["ean"])."',
                                                    '".mysql_real_escape_string($_SESSION['cart']['NUMCOMMANDE'])."',
                                                    '".mysql_real_escape_string($_SESSION['reglement']['TOTALCOMMANDE'])."',
                                                    '-1', 
                                                    NOW(),
                                                    '".mysql_real_escape_string($_SESSION["code_activation"]["CODE_AVANTAGE"])."'
                                                    ) "; 
                        $oDb->query($strSql); 
                        
                        
			echo "<script>document.location.href='".( ($_CONST['TYPE_ENVIRONNEMENT'] !='dev' ) ? $_CONST["HTTPS"] : $_CONST["URL2"]) . $_CONST['URL_ACCUEIL']."abonnement/confirmation-".$_SESSION['cart']['formule']['formule_classe'].".html';</script>";
			exit; 
		}
			
	}
	
	
	
}
?>
<form method='post' id='form_paiement'>
<div class="container">

<div class="row">
<div class="col-md-3">
</div>

<ul id="steps" class="col-md-9">
 <li class="detail_formule"><a class="active" href="<?php echo  $_CONST['URL']."/soutien-scolaire-en-ligne/abonnement/formule-".$_SESSION['cart']['formule']['formule_classe'].".html"; ?>" ><strong>1.</strong> Détail de ma formule<span class="arrow_right"></span></a></li>
 <li class="confirmation"><a  class="active" ><span class=" arrow_left "></span><strong>2.</strong>Paiement <span class="arrow_right"></span></a></li>
 <li class="paiement"><a ><span class="arrow_left"></span><strong>3.</strong>Confirmation <span class="arrow_right"></span></a></li>
</ul>
</div>




<section class="description_formule">
      <div class="picto_formule">
         <img src="/images/<?php echo $_SESSION['cart']['formule']['formule_classe']; ?>_picto.png" alt="<?php echo $_SESSION['cart']['formule']['formule_libelle']; ?>">
      </div>
      <div class="right_description">
         <h2><?php echo $_SESSION['cart']['formule']['formule_libelle']; ?></h2>
         <?php echo $_SESSION['cart']['formule']['formule_desc2']; ?>
      </div>
   </section>








<div class="paiement_formule">

<div class="title_formule">
<?php 

        //LOT6
    $strNewPrice ="";
       if($aProduct['de_engagement'] == 1){
            if(isset($_SESSION["products"]["enfants"][0]["REDUCTION"]) && !empty($_SESSION["products"]["enfants"][0]["REDUCTION"])){
                $newPrice = number_format(round( ($aProduct['produit_prix'] - ( $aProduct['produit_prix'] * $_SESSION["products"]["enfants"][0]["REDUCTION"] / 100)), 2), 2, ',', '' ) ; 
                $strNewPrice = str_replace('.',',',$newPrice) ."€ pour le premier mois puis";
            }
            echo '<h2>Paiement de '.$strNewPrice.' '.$aProduct['produit_prix'].'€ par mois</h2>'; 
	}else if($aProduct['de_engagement'] == 3){
            if(isset($_SESSION["products"]["enfants"][0]["REDUCTION"]) && !empty($_SESSION["products"]["enfants"][0]["REDUCTION"])){
                $newPrice = number_format(round( ($aProduct['produit_prix'] - ( $aProduct['produit_prix'] * $_SESSION["products"]["enfants"][0]["REDUCTION"] / 100)), 2), 2, ',', '' ) ; 
                $strNewPrice = str_replace('.',',',$newPrice) ."€ pour les 3 premiers mois puis";
            }
            echo '<h2>Paiement de '.$strNewPrice.' '.$aProduct['produit_prix'].'€ tous les 3 mois</h2>'; 
	}else if($aProduct['de_engagement'] == 12){
            if(isset($_SESSION["products"]["enfants"][0]["REDUCTION"]) && !empty($_SESSION["products"]["enfants"][0]["REDUCTION"])){
                $newPrice = number_format(round( ($aProduct['produit_prix'] - ( $aProduct['produit_prix'] * $_SESSION["products"]["enfants"][0]["REDUCTION"] / 100)), 2), 2, ',', '' ) ; 
                $strNewPrice = str_replace('.',',',$newPrice) ."€ au lieu de";
            }
            echo '<h2>Paiement de '.$strNewPrice.' '.$aProduct['produit_prix'].'€ pour un an</h2>'; 
        }
	
?>

</div>


<div class="body_formule">

<?php 
	if(!$bProduit){
		echo '	<div class="alert alert-danger">
					Une erreur est survenue lors de votre commande. Veuillez réessayer ultérieurement.
				</div>';
	}else{
	?>
			<!--
			<div class="alert alert-success">
				  <img src="../../images/success_icon.png" alt="">
			  <a href="#" class="alert-link">Bien fait! les champs sont bien renseignés.</a>
			</div>
			-->
			<?php 
				if(!$bVerif ){
					echo '	<div class="alert alert-danger">
									'.$strError.'
							</div>';
				}
				
				//Affichage de la liste des enfants 
				$strFormuleClasse = $_SESSION['cart']['formule']['formule_classe'];
				if($strFormuleClasse == "ciblee"){
					//Récupération du nom de la classe et la matière
					$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
					$strMatiere = $oDb->queryItem("SELECT matiere_titre FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['matiere'])."'");
					echo '<p class="detail_abonnement">Abonnement pour '.$_SESSION["products"]["enfants"][0]['D_PRENOM'].' en classe de '.$strClasse.' en '.$strMatiere.'.</p>'; 				
				}else if( $strFormuleClasse == "reussite"){
					//Récupération du nom de la classe et la matière
					$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
					echo '<p class="detail_abonnement">Abonnement pour '.$_SESSION["products"]["enfants"][0]['D_PRENOM'].' en classe de '.$strClasse.'.</p>'; 				
				}else if($strFormuleClasse == "tribu"){
					echo '<p class="detail_abonnement">'; 
					foreach($_SESSION["products"]["enfants"] as $aEnfant){
						$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($aEnfant['classe'])."'");
						$strMatiere = $oDb->queryItem("SELECT matiere_titre FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($aEnfant['matiere'])."'");
						echo 'Abonnement pour '.$aEnfant['D_PRENOM'].' en classe de '.$strClasse.'.<br/>'; 
					}
					echo '</p>';
				}
			?>
			<h3>Paiement</h3>

			<p>Tout paiement effectué sur notre site fait l’objet d’un système de sécurisation par certificat SSL (Secure Sockets Layer) permettant le cryptage de l'information relative à vos coordonnées bancaires.</p>




			<div class="formulaire_form_paiement">

			<div class="form-horizontal form_cb_paiement " role="form">

			<div class="form_cb_paiement_content"> 


			<div class="form-horizontal  " role="form">

			  <div class="form-group nom_detenteur_cb">
				<?php 
					//Affichage de l'asterisque que si duree engagement inferieur à un an : id 3 
					if($_SESSION['products']['enfants'][0]['duree_engagement'] < 3)
						echo '<label for="inputnomdetenteur" class="col-sm-6 control-label">Nom du détenteur de la CB <strong>*</strong></label>';
					else
						echo '<label for="inputnomdetenteur" class="col-sm-6 control-label">Nom du détenteur de la CB</label>';
				?>
				
				<div class="col-sm-5">
				  <input type="text" class="form-control" id="inputnomdetenteur"  maxlength="20"  name="cb_name" <?php if(isset($_POST['cb_name'])) echo "value=\"".stripslashes($_POST['cb_name'])."\""; ?>>
			   
				</div>
			  </div>
			  
			  <div class="form_cb">
			  
			<div class="cb_formulaire_paiement">

			  <div class="form-group">
				<label for="inputytpecarte" class="col-sm-5 control-label">Type de carte</label>
				<div class="col-sm-6 checkbox btn_carte">
				   <label>
					  <input type="radio" name="cp_type" value="1" <?php if(isset($_POST['cp_type']) && $_POST['cp_type'] =="1" ) echo "checked"; ?> > <img src="/images/visa.png" alt="">
					</label>
					
					  <label>
					  <input type="radio"  name="cp_type" value="2" <?php if(isset($_POST['cp_type']) && $_POST['cp_type'] =="2" ) echo "checked"; ?> > <img src="/images/mastercard.png" alt="">
					</label>
					
					   <label>
					  <input type="radio"  name="cp_type" value="3" <?php if(isset($_POST['cp_type']) && $_POST['cp_type'] =="3" ) echo "checked"; ?>> <img src="/images/cb.png" alt="">
					</label>
				</div>
			  </div>
			  <div class="form-group">
				<label for="inputnumcarte" class="col-sm-5 control-label">Numéro de carte <strong>*</strong></label>
				<div class="col-sm-6">
				  <input type="text" class="form-control" id="inputnumcarte" name="cb_numcarte"  <?php if(isset($_POST['cb_numcarte'])) echo "value='".$_POST['cb_numcarte']."'"; ?>>
				</div>
			  </div>
			  
			  <div class="form-group">
				<label for="inputnumcarte" class="col-sm-5 control-label">Expiration <strong>*</strong></label>
				<div class="col-sm-6 cb_expiration_div">
			   <div class="col-sm-5" style="float:left;"> 
			   <select class="form-control" name="cb_mois">
			   <option ></option>
				  <option value="01" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="01" ) echo "selected"; ?>>01</option>
				  <option value="02" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="02" ) echo "selected"; ?>>02</option>
				  <option value="03" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="03" ) echo "selected"; ?>>03</option>
				  <option value="04" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="04" ) echo "selected"; ?>>04</option>
				  <option value="05" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="05" ) echo "selected"; ?>>05</option>
				  <option value="06" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="06" ) echo "selected"; ?>>06</option>
				  <option value="07" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="07" ) echo "selected"; ?>>07</option>
				  <option value="08" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="08" ) echo "selected"; ?>>08</option>
				  <option value="09" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="09" ) echo "selected"; ?>>09</option>
				  <option value="10" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="10" ) echo "selected"; ?>>10</option>
				  <option value="11" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="11" ) echo "selected"; ?>>11</option>
				  <option value="12" <?php if(isset($_POST['cb_mois']) && $_POST['cb_mois'] =="12" ) echo "selected"; ?>>12</option>
				</select>
			</div>
			  <div class="col-sm-1"  style="float:left;"> 
			/
			</div>
			  <div class="col-sm-5"  style="float:left;"> 
			<select name="cb_annee" class="form-control">
			<option ></option>
			  <?php 
				 for($i = 0; $i<10; $i++){
					echo "<option value='".(date('y')+$i)."' ".((isset($_POST['cb_annee']) && $_POST['cb_annee'] ==(date('y')+$i))? "selected" : "").">".(date('y')+$i)."</date>";
				}
			  ?>
			</select>
			</div>
				</div>
			  </div>
			  
			  
			  <div class="form-group">
				<label for="inputnumcontrole" class="col-sm-5 control-label">Numéro de contrôle <strong>*</strong></label>
				<div class="col-sm-2 numcontrol_div">
				  <input type="text" class="form-control" id="inputnumcontrole" name="cb_controle"  <?php if(isset($_POST['cb_controle'])) echo "value='".$_POST['cb_controle']."'"; ?>>
				</div>
				
				 <a href="#"  data-toggle="modal" data-target="#popupnumerocart"><img src="/images/help_numcart.png"></a>
			  </div> 
			 
			 
			 
			 
			 <div class="checkbox"><input type="checkbox" id="informationbordas" name="cb_cgv"><label for="informationbordas">J’ai lu et j’accepte les <a href="#"  data-toggle="modal" data-target="#modal_cgv" ><span style="text-decoration: underline;font-weight:bold;">Conditions Générales de Vente</span> </a>
			et les <a href="#"  data-toggle="modal" data-target="#modal_cgu" ><span style="text-decoration: underline;font-weight:bold;">Conditions Générales d’utilisation</span></a>.</label></div>
			<?php 
				if( $_SESSION['products']['enfants'][0]['duree_engagement'] < 3  ){
					echo '<div class="checkbox"><input type="checkbox" id="informationparter"  name="cb_donnee"><label for="informationparter">J’accepte la conservation sécurisée de mes données bancaires.</label></div>';
				}
			?>
			</div>
			<?php if($bDonne){ ?>
				<div class="alert alert-danger conversation_securise red">
					<img src="/images/warning_icon.png" alt="">
					
						La conservation sécurisée de vos données bancaires est nécessaire pour vous abonner 
						mensuellement. Cela vous permet d’éviter de les saisir à nouveau pour la poursuite 
						de votre abonnement. Nous vous rappelons que vous êtes libre d’interrompre 
						l’abonnement mensuel dans les conditions définies aux 
						<strong><a href="#"  data-toggle="modal" data-target="#modal_cgv" ><span style="text-decoration: underline;font-weight:bold;">conditions générales de ventes</span></a></strong>.
					
				</div>
			<?php } ?>
			 </div>
			</div>

			 </div>

			</div>

			</div>

                        <!-- TVA -->

				<div id="bloc_facturation">
					<h3>Adresse de facturation</h3>



					<div class="form-horizontal" role="form">

					<?php 
						$aCivilitesXml = array (
							'1' => 'M.',
							'2' => 'MME',
							'3' => 'MLLE',
						);
						//Nom est prénom 
						echo '<p class="col-md-offset-4">'.$aCivilitesXml[$_SESSION['user']['CIVILITECLIENT']].' '.$_SESSION['user']['PRENOMCLIENT'].' '.$_SESSION['user']['NOMCLIENT'].'</p>'; 
					?>
					  
                                        
					   
                                            

						<!-- <div class="form-group">
						<label for="facturation_adresse" class="col-sm-3 control-label"> N° et rue :</label>
						<div class="col-sm-4">
						   <input type="text" class="form-control inputgris" id="facturation_adresse" name="facturation_adresse"  <?php if(isset($_POST['facturation_adresse'])) echo "value=\"".(stripslashes($_POST['facturation_adresse']))."\""; ?>>
						</div>
					  </div> -->
                                                
                                            <div class="form-group">
                                                <label for="numclientparent" class="col-sm-3 control-label">Pays :</label>
                                                <div class="col-sm-7">
                                                <?php 
                                                        //Récupération de la liste des pays 
                                                        $strSql = "SELECT * FROM bor_pays ORDER BY pays_libelle ASC" ; 
                                                        $aListePays = $oDb->queryTab($strSql); 
                                                        echo "<select  style='width:590px;max-width: 100%;height:36px;' name='facturation_pays' id='facturation_pays'>"; 
                                                        echo "<option value='0'>Choisir votre pays</option>"; 
                                                        foreach ( $aListePays as $aPays){
                                                                echo "<option value='".$aPays['pays_id']."' ".((isset($_POST['facturation_pays']) && $_POST['facturation_pays'] ==$aPays['pays_id'])? "selected" : "")." >".$aPays['pays_libelle']."</option>";
                                                        }
                                                        echo "</select>"; 
                                                ?>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                    <label for="inputcodepostal" class="col-sm-3 control-label">Code postal :</label>
                                                    <div class="col-sm-4">
                                                       <input type="text" class="form-control inputgris"   disabled  id="facturation_cp" name="facturation_cp"  <?php if(isset($_POST['facturation_cp'])) echo "value='".$_POST['facturation_cp']."'"; ?> >
                                                    </div>

                                            </div>
                                            <div class="form-group" id="div_error" style="display:none;">
                                                    <div class="col-sm-3">
                                                    </div>
                                                    <div class="col-sm-6">
                                                    <label style="color:red;" id="label_error"></label>
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputville" class="col-sm-3 control-label">Ville :</label>
                                                <div class="col-sm-7" id="bloc_ville">
                                                   <select style="width:590px;max-width: 100%;height:36px;" class="inputgris" disabled name="facturation_ville" id="facturation_ville">
                                                                <option>Choisir votre ville</option>
                                                 </select>
                                                </div>
                                            </div> 

					  <div class="form-group">
						<label for="facturation_numero_voie" class="col-sm-3 control-label"> Adresse :</label>
						<div class="col-sm-2"  style='width:95px;padding-right:0px;'>
						   <input style="width:80px;" type="text" class="form-control inputgris" disabled id="facturation_numero_voie" maxlength="10" name="facturation_numero_voie"  <?php if(isset($_POST['facturation_numero_voie'])) echo "value=\"".(stripslashes($_POST['facturation_numero_voie']))."\""; ?>>
						   <p style="font-size:14px">(ex : 5 TER)</p>
						</div>
                                                <div class="col-sm-3" style='padding-left: 5px;padding-right: 0px;width: 240px;'>
							<?php 
							//Récupération de la liste des types de voies
							$strSql2 = "SELECT * FROM bor_adresse_postale ORDER BY ref_libelle ASC" ; 
							$aListeTypeVoie = $oDb->queryTab($strSql2); 
							echo "<select class='inputgris' disabled name='facturation_type_voie' id='facturation_type_voie' style='margin-bottom: 5px; height: 36px; line-height: 36px;'>"; 
							echo "<option value='0'>Choisir votre type de voie</option>"; 
							foreach ( $aListeTypeVoie as $aTypeVoie){
								echo "<option value='".$aTypeVoie['ref_code']."' ".((isset($_POST['facturation_type_voie']) && $_POST['facturation_type_voie'] ==$aTypeVoie['ref_code'])? "selected" : "")." >".$aTypeVoie['ref_libelle']."</option>";
							}
							echo "</select>"; 
							?>
						</div>
                                                <div class="col-sm-3" style='padding-left: 5px;'>
						   <input type="text" class="form-control inputgris" disabled id="facturation_nom_voie" maxlength="23" name="facturation_nom_voie"  <?php if(isset($_POST['facturation_nom_voie'])) echo "value=\"".(stripslashes($_POST['facturation_nom_voie']))."\""; ?>>
						   <p style="font-size:14px">(ex : MAL J DE L DE TASSIGNY)</p>
						</div>
					  </div>
						
						 <!--<div class="form-group">
						<label for="facturation_type_voie" class="col-sm-3 control-label">Type de voie :</label>
						<div class="col-sm-4" id="bloc_ville">
						   <select class="inputgris" name="facturation_type_voie" id="facturation_type_voie">
								<option>Choisir votre type de voie</option>
						 </select>
						</div>
					  </div>-->
						
					  
					  
                                            <div class="form-group">
                                                    <label for="facturation_complement" class="col-sm-3 control-label">Complément d’adresse :</label>
                                                    <div class="col-sm-7">
                                                       <input style="width: 590px;max-width: 100%;" type="text" class="form-control inputgris" disabled name="facturation_complement" id="facturation_complement"  <?php if(isset($_POST['facturation_complement'])) echo "value=\"".(stripslashes($_POST['facturation_complement']))."\""; ?>>
                                                       <p style="font-size:14px">(Résidence, appartement, chez Mr ...)</p>
                                                    </div>
                                            </div>
                                                 
                                            
                                              
                                            
					   
					   
					
					</div>
				</div> 
					<div style="text-align:center;">
						<a href="#" onclick="$(this).attr('disabled','disabled');$('#form_paiement').submit();" class="btn btn-orange valid_abonnement col-sm-offset-6">Valider mon abonnement » </a>
						<p style="margin-top:25px;">Attention, la génération de votre abonnement peut prendre quelques instants, <br>merci de bien vouloir patienter pendant le chargement de la page. </p>
					</div>
					
<?php 
		}
?>
</div>
</div>


</div>
</form>


<div class="modal fade" id="popupnumerocart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        
      </div>
      <div class="modal-body">
      
      <div class="center">
       <img class="img-responsive" src="/images/carte_photo.jpg" alt="">
       </div>
       
      </div>
     
    </div>
  </div>
</div>


<script type='text/javascript'>
$(document).ready(function(){
	$("#facturation_pays").change(function(){
            $('#facturation_cp').removeClass('inputgris');
            $('#facturation_cp').removeAttr('disabled');
            $('#facturation_cp').val('');
        });
	$("#facturation_cp").keyup(function( request, response ) {		
		recherche_ville();	
	});
	recherche_ville();
});

function recherche_ville(){
	strCp =$("#facturation_cp").val();
	strPays =$("#facturation_pays").val();
	$.ajax({
		url:  "/templates/ajax/ws_adresse.php",
		dataType: "script",
		data: {
			cp: strCp, 
			pays: strPays
		}
	});		
}
</script>
     