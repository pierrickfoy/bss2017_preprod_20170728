
<div class="container">
 <div class="row">
      <div class="col-md-3">
      </div>
     <ul id="steps" class="col-md-9">
		 <li class="detail_formule"><a class="active" ><strong>1.</strong> Détail de ma formule<span class="arrow_right"></span></a></li>
		 <li class="confirmation"><a  class="active" ><span class=" arrow_left "></span><strong>2.</strong>Paiement <span class="arrow_right"></span></a></li>
		 <li class="paiement"><a class="active" ><span class="arrow_left"></span><strong>3.</strong>Confirmation <span class="arrow_right"></span></a></li>
	</ul>
   </div>
<section class="description_formule">
  <div class="picto_formule">
	 <img src="/images/<?php echo $_SESSION['cart']['formule']['formule_classe']; ?>_picto.png" alt="<?php echo $_SESSION['cart']['formule']['formule_libelle']; ?>">
  </div>
  <div class="right_description">
	 <h2><?php echo $_SESSION['cart']['formule']['formule_libelle']; ?></h2>
	 <?php echo $_SESSION['cart']['formule']['formule_desc2']; ?>
  </div>
</section>
   
<div class="confirmation">
   <div class="title_formule">
		<?php 
			if($_SESSION['paiement']['statut'] == "OK"){
				echo '<h2>Votre abonnement a bien été enregistré.</h2>';
			}else{
				echo '<h2>Echec de la commande.</h2>';
			}
		?> 
   </div>
   <div class="body_formule">
		<?php
			$bAnalytics = false; 
			if($_SESSION['paiement']['statut'] == "OK"){
				$bAnalytics = true; 
				//Affichage de la liste des enfants 
				$strFormuleClasse = $_SESSION['cart']['formule']['formule_classe'];
				if($strFormuleClasse == "ciblee"){
					//Récupération du nom de la classe et la matière
					$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
					$strMatiere = $oDb->queryItem("SELECT matiere_titre FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['matiere'])."'");
					echo '<p class="detail_abonnement">Abonnement pour '.$_SESSION["products"]["enfants"][0]['D_PRENOM'].' en classe de '.$strClasse.' en '.$strMatiere.'.</p>'; 				
				}else if( $strFormuleClasse == "reussite"){
					//Récupération du nom de la classe et la matière
					$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
					echo '<p class="detail_abonnement">Abonnement pour '.$_SESSION["products"]["enfants"][0]['D_PRENOM'].' en classe de '.$strClasse.'.</p>'; 				
				}else if($strFormuleClasse == "tribu"){
					echo '<p class="detail_abonnement">'; 
					foreach($_SESSION["products"]["enfants"] as $aEnfant){
						$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($aEnfant['classe'])."'");
						$strMatiere = $oDb->queryItem("SELECT matiere_titre FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($aEnfant['matiere'])."'");
						echo 'Abonnement pour '.$aEnfant['D_PRENOM'].' en classe de '.$strClasse.'.<br/>'; 
					}
					echo '</p>';
				}
				
				//Récupération du prix en fonction du produit 
				$strSql = "SELECT * FROM bor_produit p INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix) WHERE p.produit_ean = '".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["ean"])."'"; 
				$aProduct = $oDb->queryRow($strSql);
				//LOT 6
                                $strNewPrice ="";
				if($aProduct['de_engagement'] == 1){
                                    if(isset($_SESSION["products"]["enfants"][0]["REDUCTION"]) && !empty($_SESSION["products"]["enfants"][0]["REDUCTION"])){
                                        $newPrice = number_format(round( ($aProduct['produit_prix'] - ( $aProduct['produit_prix'] * $_SESSION["products"]["enfants"][0]["REDUCTION"] / 100)), 2), 2, ',', '' ) ; 
                                        $strNewPrice = str_replace('.',',',$newPrice) ."€ pour le premier mois puis";
                                    }
                                    echo '<p>Paiement de <strong> '.$strNewPrice.' '.$aProduct['produit_prix'].'€ par mois</strong></p>'; 
                                }else if($aProduct['de_engagement'] == 3){
                                    if(isset($_SESSION["products"]["enfants"][0]["REDUCTION"]) && !empty($_SESSION["products"]["enfants"][0]["REDUCTION"])){
                                        $newPrice = number_format(round( ($aProduct['produit_prix'] - ( $aProduct['produit_prix'] * $_SESSION["products"]["enfants"][0]["REDUCTION"] / 100)), 2), 2, ',', '' ) ; 
                                        $strNewPrice = str_replace('.',',',$newPrice) ."€ pour les 3 premiers mois puis";
                                    }
                                    echo '<p>Paiement de <strong>'.$strNewPrice.' '.$aProduct['produit_prix'].'€ tous les 3 mois</strong></p>'; 
                                }else if($aProduct['de_engagement'] == 12){
                                    if(isset($_SESSION["products"]["enfants"][0]["REDUCTION"]) && !empty($_SESSION["products"]["enfants"][0]["REDUCTION"])){
                                        $newPrice = number_format(round( ($aProduct['produit_prix'] - ( $aProduct['produit_prix'] * $_SESSION["products"]["enfants"][0]["REDUCTION"] / 100)), 2), 2, ',', '' ) ; 
                                        $strNewPrice = str_replace('.',',',$newPrice) ."€ au lieu de";
                                    }
                                    echo '<p>Paiement de <strong>'.$strNewPrice.' '.$aProduct['produit_prix'].'€ pour un an</strong></p>';
                                }
				echo '<p>Votre abonnement porte le n°<strong>'.$_SESSION['cart']['NUMCOMMANDE'].'</strong>.</p>';
				
				/* Tags de tracking Effiliation */
				if(strtolower($_CONST['TYPE_ENVIRONNEMENT']) == 'prod'){
					$code_promo = "";
					$nouveau_client = "";		
					$donnees_client = "SELECT * FROM bor_commande c WHERE c.client_id = '".$_SESSION['user']['IDCLIENT']."'"; 
					$client = $oDb->queryTab($donnees_client);

					if(count($client) > 1){
						$nouveau_client = 0;
					}else{
						$nouveau_client = 1;
					}
					if(isset($_SESSION["products"]["enfants"][0]["CODEPROMO"])){
						$code_promo = $_SESSION["products"]["enfants"][0]["CODEPROMO"];
					} 
					// calcul montant total HT :
					$montant_ht = round((($_SESSION['reglement']['TOTALCOMMANDE']) / ($_CONST['TAUX_HT'])),2);
					echo '<img src="http://track.effiliation.com/servlet/effi.revenue?id='.$_CONST['ID_ANNONCEUR'].'&montant='.$montant_ht.'&monnaie=eu&ref='.$_SESSION['cart']['NUMCOMMANDE'].'&payment=CB&newcustomer='.$nouveau_client.'&voucher='.$code_promo.'&ref2='.$strFormuleClasse.'" width="0" height="0">';
				}
				/* --- */
				
				/* Tag Vente public idées*/
				if(strtolower($_CONST['TYPE_ENVIRONNEMENT']) == 'prod'){
					if($strFormuleClasse == "ciblee"){
					?>
					<script type="text/javascript">
    
						var tip = tip || [];
						tip.push(["_setSale","4212","581078","792a8c3e3fd08926a57da5d8a97375fb",
						/* To change >> start */
						'<?php echo $_SESSION['cart']['NUMCOMMANDE']; ?>',        // transaction ID - required
						'<?php echo $montant_ht; ?>',        // total : does not include tax and shipping - required
						''    // additional data
						/* To change << end */
						]);
						(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src=(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidees.com/p/tip/";               
						var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window)

					 </script>
					<?php
					}else if( $strFormuleClasse == "reussite"){
					?>
					<script type="text/javascript">
    
						var tip = tip || [];
						tip.push(["_setSale","4212","581080","ca61117e2b1e0c2d17a02367cd4598ce",
						/* To change >> start */
						'<?php echo $_SESSION['cart']['NUMCOMMANDE']; ?>',        // transaction ID - required
						'<?php echo $montant_ht; ?>',        // total : does not include tax and shipping - required
						''    // additional data
						/* To change << end */
						]);
						(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src=(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidees.com/p/tip/";               
						var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window)

					 </script>
					<?php
					}else if($strFormuleClasse == "tribu"){
					?>
					<script type="text/javascript">
    
						var tip = tip || [];
						tip.push(["_setSale","4212","581082","5e6eb15095c36f52e9670c3d71efab91",
						/* To change >> start */
						'<?php echo $_SESSION['cart']['NUMCOMMANDE']; ?>',        // transaction ID - required
						'<?php echo $montant_ht; ?>',        // total : does not include tax and shipping - required
						''    // additional data
						/* To change << end */
						]);
						(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src=(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidees.com/p/tip/";               
						var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window)

					 </script>
 
					<?php
					}
				}
				/* /Tag Vente public idées */
			?>
				
			  <p>Vous allez recevoir un <span class="blue_txt">email de confirmation de commande</span> avec vos codes d’accès, ceux de votre enfant et le lien 
				 pour vous connecter à la plateforme de soutien scolaire. L'envoi de cet email peut prendre quelques minutes, merci de votre patience. 
			  </p>
			  <p>Nous vous remercions de votre confiance.<br/>
				 Le service client reste à votre écoute pour toute question au<span class="blue_txt"> <?php echo $_CONST['RC_TEL']; ?></span> ou <a href="mailto:soutien-scolaire@bordas.tm.fr"><span class="blue_txt">par mail</span></a>. 
			  </p>
			<?php 
			}else{
				echo '<p>Suite à un problème technique, votre commande n’a pas pu aboutir. Votre abonnement n’est donc pas enregistré.</p>'; 
				echo '<p>Afin de finaliser votre commande, nous vous invitons à contacter notre service client par mail <a href="mailto:soutien-scolaire@bordas.tm.fr"><span class="blue_txt">soutien-scolaire@bordas.tm.fr</span></a> ou par téléphone au <span class="blue_txt">'.$_CONST['RC_TEL'].'</span> (du lundi au vendredi '.$_CONST['RC_HORAIRES'].').</p>'; 
				echo '<p>Nous vous remercions de votre confiance. </p>'; 
			}
			?>
   </div>
</div>
<?php 
// var_dump( $bAnalytics); 
	// if($bAnalytics && (  $_CONST['TYPE_ENVIRONNEMENT'] !='dev' || true) ){
	if($bAnalytics && (  $_CONST['TYPE_ENVIRONNEMENT'] !='dev' ) ){
            //LOT6 Mise à jour du code affiliation
            if(isset($_SESSION["products"]["enfants"][0]["CODEPROMO"]) && !empty($_SESSION["products"]["enfants"][0]["CODEPROMO"]))
                $strAffiliation = $_SESSION["products"]["enfants"][0]["CODEPROMO"];
            else
                $strAffiliation = "Bordas Soutien Scolaire";
		
	?>
		<!-- GA --> 
		<script>
		
			ga('require', 'ecommerce', 'ecommerce.js');
			
			ga('ecommerce:addTransaction', {
			  'id': '<?php echo $_SESSION['cart']['NUMCOMMANDE']; ?>',                     // Transaction ID. Required.
			  'affiliation': '<?php echo $strAffiliation; ?>',   // Affiliation or store name.
			  'revenue': '<?php echo $aProduct['produit_prix']; ?>',               // Grand Total.
			  'shipping': '0',                  // Shipping.
			  'tax': '10'                     // Tax.
			});
			
			ga('ecommerce:addItem', {
			  'id': '<?php echo $_SESSION['cart']['NUMCOMMANDE']; ?>',                     // Transaction ID. Required.
			  'name': '<?php echo $aProduct['produit_titre']; ?>',    // Product name. Required.
			  'sku': '<?php echo $aProduct['produit_ean']; ?>',                 // SKU/code.
			  'category': 'BOUQUET NUMERIQUE',         // Category or variation.
			  'price': '<?php echo $aProduct['produit_prix']; ?>',                 // Unit price.
			  'quantity': '1'                   // Quantity.
			});
						   
			ga('ecommerce:send');
			
			ga('ecommerce:clear');
		</script>
		<!-- /GA -->
		
		<!-- Tag Universel Thank You Page public idées -->
		<script type="text/javascript"> 
			var tip = tip || []; 
			tip.push(['_setSegment', '4212', '5843', 'Thank you Page']); 
			(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src=( document.location.protocol=="https:"?"https":"http")+"://tracking.publicidees.co m/p/tip/"; 
			var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}if (e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window) 
		</script> 
		<!-- /Tag Universel Thank You Page public idées -->
		
		
		<!-- Facebook Conversion Code for BSS-achatFB -->
		<script>(function() {
		  var _fbq = window._fbq || (window._fbq = []);
		  if (!_fbq.loaded) {
			var fbds = document.createElement('script');
			fbds.async = true;
			fbds.src = '//connect.facebook.net/en_US/fbds.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(fbds, s);
			_fbq.loaded = true;
		  }
		})();
		window._fbq = window._fbq || [];
		window._fbq.push(['track', '6018688632045', {'value':'<?php echo $aProduct['produit_prix']; ?>','currency':'EUR'}]);
		</script>
		<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6018688632045&amp;cd[value]=<?php echo $aProduct['produit_prix']; ?>&amp;cd[currency]=EUR&amp;noscript=1" /></noscript>

<?php
/* Implémentation plan de taggage - EFFILIATION */
	// confirmation de paiement
	if(isset($_GET["templates_id"]) && $_GET["templates_id"] == 27){
		if(isset($_SESSION['paiement']['statut']) && $_SESSION['paiement']['statut'] == "OK"){
			echo '<script src="https://mastertag.effiliation.com/mt660015994.js?page=sale&idp='.$_SESSION["products"]["enfants"][0]["ean"].'&montant='.$_SESSION['reglement']['TOTALCOMMANDE'].'&ref='.$_SESSION['cart']['NUMCOMMANDE'].'" async="async" ></script>';
		}
	}
/* --- */
?>

	<?php 
   }
	unset($_SESSION['cart']); 
	unset($_SESSION['shipping']); 
	unset($_SESSION['reglement']); 
	unset($_SESSION['products']);
	unset($_SESSION['paiement']);
	unset($_SESSION['nb_enfant']);
// var_dump($_SESSION['paiement']);
?>
 
