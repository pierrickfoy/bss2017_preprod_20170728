<?php
$iClasseUrl = "/".$_GET["classe_url"]."/";
//Récupération des infos de la classe
$strSql = "SELECT *
FROM bor_classe_page cp
INNER JOIN bor_classe c ON (cp.classe_id = c.classe_id)
WHERE cp.classe_page_url = '$iClasseUrl'"; 
$aClasse = $oDb->queryRow($strSql);

// redirection vers home si url ko
if(empty($aClasse)){
	echo '<SCRIPT LANGUAGE="JavaScript">
document.location.href="'.$_CONST['URL2'].'"
</SCRIPT>';
	exit;
}

$aImage  = $oDb->queryRow("SELECT ef.files_path
							FROM   eco_files ef
							WHERE ef.files_table_id = ".$aClasse['classe_page_id']." AND ef.files_field_name = 'classe_image'
							"); 
							
// Ajout 20 07 2016
// slider
$aSlider  = $oDb->queryTab("SELECT *
							FROM   bor_slider_page sp
							LEFT JOIN eco_files ef ON (ef.files_id = sp.files_id)
							WHERE sp.page_id = ".$aClasse['classe_page_id']." AND sp.tpl = 'classe_page'
							ORDER BY sp.position
							", true); 

// traitement du lien slider
$sliderLien = $aClasse['classe_page_slider_url'];
if(!$sliderLien) $sliderLien= "#";
$sliderLienBlank = $aClasse['classe_page_slider_url_blank'];
if($sliderLienBlank != NULL) $sliderLienBlank = 'target="_blank"'; else $sliderLienBlank = "";

// traitement du lien "tester"
$testerLien = $aClasse['classe_page_tester_url'];
$testerLienBlank = $aClasse['classe_page_tester_url_blank'];
if($testerLienBlank != NULL) $testerLienBlank = 'target="_blank"'; else $testerLienBlank = "";
?>

<?php include ("./breadcrumb_classique.php");?>
<?php include ("section-classe.php");?>
<?php /*include ("section-avantage-abonne.php");*/?>
<?php include ("section-matiere-disponible.php");?>
<?php include ("section-home-abonnement.php");?>
<?php /*include ("section-compatibilite.php");*/?>
<?php include ("section-temoignage.php");?>
<?php include ("section-newsletter.php");?>