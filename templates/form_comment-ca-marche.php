<!-- BREADCRUMB -->
  <div class="bss-breadcrumb" >
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h6>
            <ol class="breadcrumb">
              <li id="a" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="/index.php" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
              <li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="#" itemprop="url"> <span itemprop="title">Comment ça marche ?</span> </a></li>
            </ol>
          </h6>
        </div>
      </div>
    </div>
  </div>

<div class="bss-section bloc-section-gris-orange bss-ccm">
  <div class="container">
    <h1 id="tagline" class="h1">Comment ça marche ?</h1>
   
    <div class="row">
      <div class="col-md-8">
        <div class="bss-doc-section"> 
         <h2 id="subtitle" class="h2">Une plateforme de soutien scolaire en ligne</h2>
         <img src="/img/ccm-1.png" class="img-responsive">
          <p>Bordas Soutien scolaire propose aux élèves un véritable programme d’entraînement pour réviser à la maison, sur ordinateur ou sur tablette.</p>
          <hr>
          <h2 id="subtitle" class="h2">Des parcours d'entrainement personnalisés</h2>
         <img src="/img/ccm-2.png" class="img-responsive">
          <p>En français et en maths, des tests de positionnement nous permettent de proposer à votre enfant des exercices adaptés à son niveau : il peut ainsi progresser à son rythme !</p>
          <hr>
          <h2 id="subtitle" class="h2">Des exercices interactifs</h2>
          <img src="/img/ccm-4.png" class="img-responsive">
          <p>Votre enfant prendra plaisir à s’entraîner grâce nos exercices interactifs (quiz, glisser-déposer, relier, textes à trous, ...) de difficulté progressive.</p>
          <hr>
          <h2 id="subtitle" class="h2">Des fiches de cours illustrées</h2>
         <img src="/img/ccm-3.png" class="img-responsive">
          <p>Des diaporamas de cours clairs et illustrés d’exemples permettent de revoir tout le programme pour la classe et la matières choisies.</p>
          <hr>
          <h2 id="subtitle" class="h2">Des ressources multimédia</h2>
     <img src="/img/ccm-5.png" class="img-responsive">
          <p> Pour apprendre autrement, Bordas Soutien scolaire met des ressources variées à la disposition de votre enfant : vidéos, schémas interactifs, cartes mentales...</p>
          <hr>
          <h2 id="subtitle" class="h2">Des corrigés commentés et personnalisés</h2>
        <img src="/img/ccm-6.png" class="img-responsive">
          <p> Lorsque votre enfant réalise un exercice, nous ne nous contentons pas d’afficher la bonne réponse : la correction est accompagnée de commentaires adaptés à son erreur et de messages positifs et encourageants !</p>
          <hr>
          <h2 id="subtitle" class="h2">Un compte parents pour suivre ses résultats</h2>
          <img src="/img/ccm-7.png" class="img-responsive">
          <p>Vous pouvez accompagner votre enfant et suivre ses progrès en visualisant ses résultats à tout moment grâce à votre compte Parents !</p>
          <hr>
          <h2 id="subtitle" class="h2">Spécial Bac / Brevet</h2>
          <img src="/img/ccm-8.png" class="img-responsive">
          <p> Pour les collégiens et les lycéens, des points « Spécial Bac » et Spécial Brevet » sont prévus dans les matières d’examen : méthodes à connaître, entraînement...</p>
        </div>
      </div>
      <div class="col-md-4 hidden-xs hidden-sm">
        <div class=""> 
          <!-- 16:9 aspect ratio -->
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/9Va3KEari7I?rel=0"></iframe>
          </div>
        </div>
        <hr>
        <div class="bloc-annonce"><div class="prepend"><b class="caret"></b> publicité <b class="caret"></b></div><a href="#"><img src="http://www.bordas-soutien-scolaire.com/images/bss-wengo-300.gif" class="img-responsive"></a></div>
      </div>
      <div class="col-md-12">
        <hr>
        <p class="text-center"><a class="btn btn-md btn-primary" href="comment-sabonner">Comparer les formules d'abonnement<i class="icon-angle-right"></i></a></p>
      </div>
    </div>
  </div>
</div>
