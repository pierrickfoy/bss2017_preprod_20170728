<?php 
ini_set("soap.wsdl_cache_enabled", 0); 

header('Content-Type:text/html; charset=utf-8');
$bPost = false; 
$strMessageBox ="";
if(isset($_POST['action']) && $_POST['action'] =="submit"){
$strMessage = ''; 
$bPost = true; 
	$bOpt1 = false; 
	$bOpt2 = false; 
	if(isset($_POST['email']) && !empty($_POST['email'])){
		$strEmail = $_POST['email'] ; 
		if(is_valid_email($strEmail)){
			if(isset($_POST['option1']) && !empty($_POST['option1']))
				$bOpt1 = true; 
			if(isset($_POST['option2']) && !empty($_POST['option2']))
				$bOpt2 = true; 
			try{
				$soap_options = array ( 'login' => 'WSProspect', 'password' => 'ProspectManagementPER', 'exception' => true); 
				$client = new SoapClient("http://ws.edupole.net/prospectManagement/WSProspect.wsdl", $soap_options); 
				$params = array(
							'codeSite' => 'BRD_SOUTIEN_SCO',
							'email' => $strEmail, 
							'origin' => 'DECOUVERTE-SUR-OUVRAGE_BSS',
							'optinNewsletter' => $bOpt1,
							'optinPartner' => $bOpt2
							);
							
				$retour_ws = $client->createProspect($params); 
				if(isset($retour_ws)){
					if($retour_ws->creationStatus)
						$strMessage = "Votre adresse email a bien été enregistrée."; 
					else
						$strMessage = "Cet email a déjà été utilisé, vous ne pouvez l'utiliser qu'une seule fois.";
				// var_dump($retour_ws->creationStatus); 
				}else
					$strMessage = "Une erreur est survenue lors de votre enregistrement."; 
			}catch (Exception $e){
				$strMessage = "Une erreur est survenue lors de votre enregistrement.";
			}
		}else{
			$strMessageBox = "Veuillez renseigner une adresse email valide.";
			$bPost = false;
		}
	}else{
		$strMessageBox = "Veuillez renseigner votre email"; 
		$bPost = false;
	}
}
				
	?>
	
	
	
	
	<?php $strSQLabonnementtitle = "SELECT templates_title, templates_content
						   FROM eco_templates a
						   RIGHT JOIN eco_templates_lang b ON (a.templates_id = b.templates_id)
						   WHERE a.templates_id= ".$_CONST["ID_Abonnement"]."";
	
						$strresult=$oDb->queryRow($strSQLabonnementtitle);	
						
?>	
<div class="bss-breadcrumb">
	<div class="container">
		<div class="row">
		  <div class="col-sm-12">
			<h6>
			  <ol class="breadcrumb">
				<li id="a" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="index.php" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
				<li id="b" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="form_decouverte.php" itemprop="url"> <span itemprop="title">Offre découverte</span> </a></li>
			  </ol>
			</h6>
		  </div>
		</div>
	</div>
</div>
<div class="bss-section bloc-section-orange bss-ccm">
	<div class="container">
      <h1 id="tagline" class="h1">Offre Découverte</h1>
      <div class="row">
        <div class="center_column col-md-8">
          <p class="lead">Vous avez acheté un cahier d'entraînement ou un cahier de vacances Bordas ?</p>
          <p>Choisissez la ou les offre(s) proposée(s) avec votre livre pour profiter de vos
            avantages exclusifs :</p>
          
          <div class="row">
            <div class="col-md-6 text-center">
              <div class=" bloc-bss-1 bloc-decouverte ">
                <p class="h3">Entraînement en ligne</p>
				<a onclick="ga('send', 'event', 'Offre découverte', 'Clic offre', '40% de réduction sur votre abonnement au site de révisions');" href="/comment-s-abonner.html" class="offre"><strong>40% de réduction</strong> sur votre <br>
                abonnement au site de révisions*</a>
                <a onclick="ga('send', 'event', 'Offre découverte', 'Clic offre', '20% de réduction sur votre abonnement au site de révisions');" href="/comment-s-abonner.html" class="offre"><strong>20% de réduction</strong> sur votre <br>
                abonnement au site de révisions*</a> <a onclick="ga('send', 'event', 'Offre découverte', 'Clic offre', '1 mois d abonnement offert au site de révisions');" href="/abonnement/offre-decouverte-formule-reussite-bordas-soutien-scolaire-etape-1.html" class="offre"><strong>1 mois d'abonnement offert</strong> au <br>
                site de révisions**</a> </div>
            </div>
            <div class="col-md-6 text-center">
              <div class=" bloc-bss-1 bloc-decouverte">
                <p class="h3">Cours particuliers</p>
                <a onclick="ga('send', 'event', 'Offre découverte', 'Clic offre', '1 heure de cours offerte avec un enseignant de l éducation nationale');" href="http://www.bordas.com/avantage/bss-offre-manuel" target="_blank"><strong>1 heure de cours offerte</strong> avec <br>
                un
                enseignant de l'Éducation Nationale ***</a>  </div>
            </div>
          </div>
          <p class="legende">* Offre valable sur le site <a href="http://soutien.editions-bordas.fr">soutien.editions-bordas.fr</a> pour 1 enfant (1 classe au choix)
            pour les principales collections Bordas (<a href="http://soutien.editions-bordas.fr/docs/BSS_liste_coll_mdl.pdf">voir la liste des ouvrages concernés</a>).<br>
            <br>
            **Offre valable sur le site <a href="http://soutien.editions-bordas.fr">soutien.editions-bordas.fr</a>, sur le premier abonnement
            uniquement, sur présentation du code promotionnel précisé au dos de votre livre.<br>
            <br>
            ***Offre valable pour toute souscription au service de cours à domicile sur <a href="http://bordas.com/" target="_blank">bordas.com</a> et soumises à conditions (voir modalités sur <a href="http://bordas.com/" target="_blank">bordas.com</a>). <br>
            <br>
            Ces offres sont non cumulables, valables une seule fois, et à durée limitée (voir
            conditions indiquées au dos de votre livre). </p>
        </div>
		<aside class="right_column col-md-4">
		<div class=""> 
          <!-- 16:9 aspect ratio -->
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CItcTboz6_k?rel=0?" allowfullscreen></iframe>
          </div>
        </div>
					<?php
					 echo get_datablocks("pub");   
					?>
		</aside>
        
      </div>
    </div>
</div>