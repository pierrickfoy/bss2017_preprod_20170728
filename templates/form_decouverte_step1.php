<script type="text/javascript">
		jQuery.noConflict();
</script>

<?php
$bError = false ; 
$strSql = "SELECT * FROM bor_formule WHERE formule_classe = 'reussite'"; 
$aFormule = $oDb->queryRow($strSql); 
$_SESSION['cart']['formule'] = $aFormule ; 
$_SESSION["products"]["enfants"][0]["formule"] = $aFormule ["formule_id"] ; 

/**********************************************************************************************************************************
**************************************************** STEP 1 : ANALYSE DU ISBN *****************************************************
**********************************************************************************************************************************/
if(isset($_POST["action"]) && $_POST["action"] == "step_1_1"){
	//Vérification que ISBN et rempli par des chiffres
	if( !isset($_POST["isbn4"]) || empty($_POST["isbn4"]) || !preg_match("/^[0-9]{6}$/", $_POST['isbn4']) ){
		$_SESSION['isbn4'] = $_POST["isbn4"] ;
		$bError = true; 
		$strStep = "step_1_1"; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Vous devez saisir seulement 6 chiffres dans ce champ de saisie.";
		$strError .="</span></p>";
	}else if( !isset($_POST["isbn5"]) ||  !preg_match("/^[0-9]{1}$/", $_POST['isbn5']) ){
		$_SESSION['isbn4'] = $_POST["isbn4"] ;
		$_SESSION['isbn5'] = $_POST["isbn5"] ;
		$bError = true; 
		$strStep = "step_1_1"; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Vous devez saisir seulement 1 chiffre dans ce champ de saisie.";
		$strError .="</span></p>";
	}else{
		//Analyse du isbn
		$_SESSION['isbn4'] = $_POST["isbn4"] ;
		$_SESSION['isbn5'] = $_POST["isbn5"] ;
		$strEan = mysql_real_escape_string("978204".$_POST["isbn4"].$_POST["isbn5"]); 
		
		$strSql = "SELECT * FROM bor_ouvrage o INNER JOIN bor_offre of ON (o.ouvrage_offre_idyonix = of.offre_id_yonix) WHERE o.ouvrage_ean = '$strEan' AND o.ouvrage_actif = 1"; 
		// var_dump($strSql); 
		$aOuvrage = $oDb->queryRow($strSql); 
		if($oDb->rows > 0 ){
			$_SESSION["ouvrage"] = $aOuvrage; 
			$strStep = "step_1_2"; 
			$bStep1 = true; 
		}else{
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "L'ISBN saisi ne donne pas accès à l'offre découverte !<br>Vérifiez que l'ISBN saisi est associé à un produit BORDAS donnant un accès à l'offre découverte Bordas Soutien Scolaire au dos de votre livre";
			$strError .="</span></p>";
		}		
	}	
}


 
?>
<div class="container">
   <div class="row">
     
      <ul id="steps" class="col-md-12">
         <li class="detail_formule"><a class="active"><strong>1.</strong>Preuve d'achat du livre<span class="arrow_right"></span></a></li>
         <li class="confirmation"><a  ><span class="arrow_left"></span><strong>2.</strong>Détail de la formule <span class="arrow_right"></span></a></li>
         <li class="paiement"><a ><span class="arrow_left"></span><strong>3.</strong>Confirmation <span class="arrow_right"></span></a></li>
      </ul>
   </div>
   <section class="description_formule">
      <div class="picto_formule">
         <img src="/images/reussite_picto.png" alt="Offre Découverte - Formule Réussite">
      </div>
      <div class="right_description">
         <h1><?php echo get_template_title(); ?></h1>
         <?php echo get_template_data(); ?>
      </div>
   </section>
   <div class="detail_formule">
      <div class="title_formule">
         <h2>
			Je saisis l'ISBN de mon livre
         </h2>
      </div>
      <div class="body_formule ">
         
		 <?php 
		
		 if($strStep =="step_1_1" && $bError){
			echo ' <div class="alert alert-danger">
				'.$strError.'
			 </div>';
		 }
			echo '	<form class="form-horizontal" role="form" method="post" id="form_isbn" name="form_isbn"> 
						<div class="form-group">
						   <label for="inputEmail3" class="col-sm-3 control-label">ISBN du livre * : </label>
						   <div class="col-sm-4" style="width: 315px !important;">
							  <input disabled name="isbn1" id="isbn1"    type="text" class="form-control" value="978" style=" width: 45px;float: left;  margin-right: 10px;">
							  <input disabled name="isbn2" id="isbn2" type="text"  class="form-control"  value="2"  style=" width: 30px;  float: left;  margin-right: 10px;">
							  <input disabled name="isbn3" id="isbn3" type="text"  class="form-control"  value="04"  style="width: 45px;  float: left;  margin-right: 10px;">
							  <input name="isbn4" id="isbn4" maxlength="6" onkeyup="if(jQuery(this).val().length>=6){document.getElementById(\'isbn5\').focus()};" type="text"  class="form-control" autofocus  placeholder="XXXXXX"  value="'.((isset($_SESSION["isbn4"]) && !empty($_SESSION["isbn4"])) ? $_SESSION["isbn4"] : "" ).'" style="width: 80px;  float: left;  margin-right: 10px;">
							  <input name="isbn5" id="isbn5" maxlength="1" type="text"  class="form-control"  placeholder="X" value="'.((isset($_SESSION["isbn5"]) && !empty($_SESSION["isbn5"])) ? $_SESSION["isbn5"] : "" ).'" style="width: 30px;  float: left;  margin-right: 10px;">
						   </div>
						   <div class="col-sm-4 lien_isbn"><a href="#"  data-toggle="modal" data-target="#popupisbn">Où trouver l\'ISBN de mon livre ?</a></div>
						</div>
						<div class="form-group">
						   <div class="col-sm-offset-3 col-sm-6" style="    margin-left: 0;">'; 
				if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
					?>
						<a href="#" onclick="ga('send', 'event','bouton', 'clic', 'Etape 1 - Bloc : Je saisis l\'ISBN de mon livre - ETAPE SUIVANTE', 4);jQuery('#form_isbn').submit();" class="btn btn-orange col-sm-offset-4"  style="margin-top:25px;float: right;">Étape suivante >></a>
					<?php
				}else{
					?>
						<a href="#" onclick="jQuery('#form_isbn').submit();" class="btn btn-orange col-sm-offset-4"  style="margin-top:25px;float: right;">Étape suivante >></a>
					<?php
				}
				echo' </div>
						</div>
						<input type="hidden" name="action" value="step_1_1"/>
					</form>'; 
			
		 ?>
         
      </div>
      <!--***********************-->
      <div class="title_formule" id="choixformule" ><h2>Je réponds à une question sur mon livre</h2></div>	
      <!--***********************-->
	  <?php 
			if( $strStep =="step_1_2" || (isset($_POST['action']) && $_POST['action'] =='step_2_1') ){
				$strStep = "step_1_2";
				$bError = false; 
		?>
			<div class="body_formule " id="creer_mon_compte"> 
			<?php 
				$strPage = "motdulivre"; 
				
				// echo '<script type="text/javascript" src="http://www.edupole.net/edupole_ws/motsDuLivre/js/script.js"></script>'; 
				
				try{
					ini_set("soap.wsdl_cache_enabled",0);	
					$wsdl=  "http://ws.edupole.net/motsDuLivre/WebserviceMdl.wsdl";
					$options = array(
						'exceptions' =>0,
						'trace' => 1, 
						'wsdl_cache'=>0
					);
					$client = new SoapClient($wsdl,$options);

					$strEan = $_SESSION["ouvrage"]["ouvrage_ean"]; 
					
					$tab = array(
						array( 'isbn'=>$strEan , 'questionQuantity' => '1')
					);

					$login = 'MDL'; 
					$password = 'motsLivre2011';
					$id_origine = 'BD01';
					$id_user_site = '38'; 
				
					$post = (!empty($_POST) ? $_POST : "" ); 

					if(isset($post['mdl']) && count($post['mdl']['quizz'][$strEan])){				
						$post['mdl']['quizz'][$strEan] ; 
						foreach($post['mdl']['quizz'][$strEan] as $iK => $aVal){
							$post['mdl']['quizz'][$strEan][$iK]['response'] = trim( $aVal['response']) ; 
						}
					}				
					if(empty($_POST['mdl']['quizz'])){
						$customer_arr = $client->getAleaQuizz($login,$password,$tab,$id_origine,$id_user_site,$id_user_site,false );
						// var_dump($customer_arr); exit; 
						$strForm = $customer_arr['display']; 
						$regex1 = "/<label([^>]*)>(.*)<\/label>/"; 
						preg_match_all($regex1, $strForm, $aMatch); 
						$strQuestion = $aMatch[2][0] ; 
						$regex2 = "/\[$strEan\]\[([^>]*)\]\[response\]/"; 
						preg_match_all($regex2, $strForm, $aMatch); 
						$strCode = $aMatch[1][0] ; 
						// var_dump($strQuestion); 
						// var_dump($strForm);
						$_SESSION['ticket'] = $customer_arr["ticket"];
					}else{
						//Controle de la réponse 
						// var_dump('test'); 
						$customer_arr = $client->checkQuizz($login,$password,$post,$_SESSION['ticket'] );
						if(isset($customer_arr['authorization']) && $customer_arr['authorization']){
							$_SESSION["ouvrage"]["mdl"] = "ok"; 
							$strPage = "ouvrage"; 
							$strUrl = $_CONST["URL2"]."/abonnement/offre-decouverte-formule-reussite-bordas-soutien-scolaire-etape-2.html";
							echo "<script type='text/javascript'>location.href='$strUrl'</script>"; 
							exit;
						}else{
							$customer_arr = $client->getAleaQuizz($login,$password,$tab,$id_origine,$id_user_site,$id_user_site,false );
							// var_dump($customer_arr);
							$strForm = $customer_arr['display']; 
							// var_dump('test'); 
							$regex1 = "/<label([^>]*)>(.*)<\/label>/"; 
							preg_match_all($regex1, $strForm, $aMatch); 
							$strQuestion = $aMatch[2][0] ; 
							$regex2 = "/\[$strEan\]\[([^>]*)\]\[response\]/"; 
							preg_match_all($regex2, $strForm, $aMatch); 
							$strCode = $aMatch[1][0] ;
							$_SESSION['ticket'] = $customer_arr["ticket"];	
							$bError = true; 
						}						 
					}
				}catch (Exception $e){
					$customer_arr = $client->getAleaQuizz($login,$password,$tab,$id_origine,$id_user_site,$id_user_site,false );
					// var_dump($customer_arr);
					$strForm = $customer_arr['display']; 
					// var_dump('test'); 
					$regex1 = "/<label([^>]*)>(.*)<\/label>/"; 
					preg_match_all($regex1, $strForm, $aMatch); 
					$strQuestion = $aMatch[2][0] ; 
					$regex2 = "/\[$strEan\]\[([^>]*)\]\[response\]/"; 
					preg_match_all($regex2, $strForm, $aMatch); 
					$strCode = $aMatch[1][0] ;
					$_SESSION['ticket'] = $customer_arr["ticket"];	
					$strPage = "motdulivre"; 
				}
			
				if($strStep =="step_1_2" && $bError){
					echo ' <div class="alert alert-danger red"><img src="/images/warning_icon.png">
						Le mot saisi est incorrect.
					 </div>';
				 }
			?>
				<form class="question-podcast bs-docs-example form-horizontal" method="post">
					<div id="div_personal_information">
						<div class="fieldset" style="    height: auto;">
						<h2>Question</h2>
						<p><?php echo $_SESSION["ouvrage"]['ouvrage_titre']; ?></p>
						<ul class="field" style="padding-bottom: 0;height: 25px;">
							<li>
							<label for="lastname"  style=" width: 100%; text-align: left;"><?php echo $strQuestion; ?></label>
							</li>
						</ul>
						<ul class="field" style="padding-bottom: 0">
							<li>
							<input type="text" class="input-xlarge" name="mdl[quizz][<?php echo $strEan; ?>][<?php echo $strCode; ?>][response]" style="width: 170px;text-align: left;float: left;margin: 0;">
							<input type="hidden" name="mdl[quizz][<?php echo $strEan; ?>][<?php echo $strCode; ?>][question]" value="<?php echo $strQuestion; ?>">
							</li>
						</ul>
						</div>
					</div>
					<input type="hidden" name="action" value="step_2_1"/>
					<div class="submit" style="    margin-top: 10px;">
						<?php
						if($_CONST['TYPE_ENVIRONNEMENT'] !='dev' ){
							?>
								<input type="submit" id="global_registration_par_submit" name="global_registration_par_submit" value="Valider" class="submit_button" onclick="ga('send', 'event', 'bouton', 'clic', 'Etape 1 - Bloc : Je réponds à une question sur mon livre - VALIDER', 4);NSaccountManagement.valid_form('form_account', 'global_registration_par_form', 'main#main'); return false;">					
							<?php
						}else{
							?>
								<input type="submit" id="global_registration_par_submit" name="global_registration_par_submit" value="Valider" class="submit_button" onclick="NSaccountManagement.valid_form('form_account', 'global_registration_par_form', 'main#main'); return false;">
							<?php
						}
						?>
					
					
					</div>
				</form>
				
					
			</div>
		<?php
			}
		?>
      
   </div>
</div>
 

 <div class="modal fade" id="popupisbn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        
      </div>
      <div class="modal-body">
      
      <div class="center">
       <img class="img-responsive" src="/images/BSS_MDL-ISBN.jpg" alt="">
       </div>
       
      </div>
     
    </div>
  </div>
</div>









<?php 
	if(isset($bPopupConnexion) && $bPopupConnexion){
?>
		<script type="text/javascript">
			jQuery('document').ready(function(){
				jQuery('#btn_connexion').click();
			});
		</script>
<?php
	}
	if($bForget){
?>
	<script type="text/javascript">
		jQuery('document').ready(function(){
			jQuery('#forget_mdp').click();
			
		});
	</script>
<?php
	}
?>




<script type="text/javascript">

	


jQuery(window).load(function(){
	<?php 
		if(isset($strStep) && $strStep == "step_1_2") {
	?>
			jQuery('#choixformule').ScrollTo({
				duration: 1000
			});
	<?php
		}
		if(isset($strStep) && $strStep == "step_1_3") {
	?>
			jQuery('#compteparent').ScrollTo({
				duration: 1000
			});
	<?php
		}
	?>
});
</script>