<script type="text/javascript">
		jQuery.noConflict();
</script>

<?php
// echo 'test' ; 
 // error_reporting(E_ALL);
// ini_set('error_reporting', E_ALL);
 // ini_set('display_errors', '1');
//On verifie que la formule existe 
$strSql = "SELECT * FROM bor_formule WHERE formule_classe = '".mysql_real_escape_string($_GET['formule'])."'"; 
$aFormule = $oDb->queryRow($strSql); 
if(!isset($_SESSION["ouvrage"]["mdl"]) || empty($_SESSION["ouvrage"]["mdl"]) || $_SESSION["ouvrage"]["mdl"] != "ok"){
	echo "<script>document.location.href='".$_CONST["URL2"]."/abonnement/offre-decouverte-formule-reussite-bordas-soutien-scolaire-etape-1.html';</script>";
	exit; 
}
$bError = false ; 



 
/*GESTION DES FORMULAIRE */
//Formulaire 1 ciblee
if(isset($_POST["action"]) && $_POST["action"] == "step_1_1"){
	$bError = false ; 
	unset($_SESSION["products"]["enfants"]);
	$_SESSION["products"]["enfants"][0]["classe"] = $_POST["classe"][0]; 
	$_SESSION["products"]["enfants"][0]["D_PRENOM"] = $_POST["prenom"][0] ; 
	$_SESSION["products"]["enfants"][0]["matiere"] =  $_POST["matiere"][0] ; 
		
	//On se trouve dans une formule ciblee on doit donc vérifier que les champs enfant, classe et matière sont bien selectionné
	if(!isset($_POST["prenom"][0]) || empty($_POST["prenom"][0])){
		$bError = true; 
		$strStep = "step_1_1"; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Veuillez saisir le prénom de votre enfant.";
		$strError .="</span></p>";
	}else{
		//Initialisation du prénom de l'enfant
		if( !isset($_POST["classe"][0]) || empty($_POST["classe"][0]) ){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Veuillez saisir la classe de votre enfant.";
		$strError .="</span></p>";
		}else {
			if(   !isset($_POST["matiere"][0]) || empty($_POST["matiere"][0]) ) {
				
				$bError = true; 
				$strStep = "step_1_1"; 
				$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
				$strError .= "Veuillez saisir la matière de votre enfant.";
		$strError .="</span></p>";
			}else{
				
				//Prénom, matière et classe remplie on vérifi les donnée
				$strSql = "SELECT * FROM bor_matiere_classe mc INNER JOIN bor_classe c ON (c.classe_id = mc.classe_id) INNER JOIN bor_matiere m ON (m.matiere_id = mc.matiere_id)  WHERE mc.classe_id = '".mysql_real_escape_string($_POST["classe"][0])."' AND mc.matiere_id  = '".mysql_real_escape_string($_POST["matiere"][0])."' AND mc.mc_dispo = 1"; 
				$aInfosClasse = $oDb->queryRow($strSql); 
				if(!$oDb->rows){
					$bError = true; 
					$strStep = "step_1_1"; 
					$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
					$strError .= "Nous ne retrouvons pas la formule associée à votre sélection. Veuillez choisir un couple classe/matière valide.";
		$strError .="</span></p>";
				}else{
					$_SESSION["products"]["enfants"][0]["classe"] = $aInfosClasse["classe_id"]; 
					$_SESSION["products"]["enfants"][0]["matiere"] = $aInfosClasse["matiere_id"]; 
					
					$strStep = "step_1_2"; 
					$bStep1 = true; 
				}
			}
		}
	}	
}
//Formulaire 1 reussite
if(isset($_POST["action"]) && $_POST["action"] == "step_2_1"){
	unset($_SESSION["products"]["enfants"]);
	$_SESSION["products"]["enfants"][0]["classe"] = $_POST["classe"][0]; 
	$_SESSION["products"]["enfants"][0]["D_PRENOM"] = $_POST["prenom"][0] ; 
	//On se trouve dans une formule reussite on doit donc vérifier que les champs enfant, classe et matière sont bien selectionné
	if(!isset($_POST["prenom"][0]) || empty($_POST["prenom"][0])){
		
		$bError = true; 
		$strStep = "step_1_1"; 
		$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
		$strError .= "Veuillez saisir le prénom de votre enfant.";
		$strError .="</span></p>";
	}else{
		//Initialisation du prénom de l'enfant
		
		if( !isset($_POST["classe"][0]) || empty($_POST["classe"][0])  ){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Veuillez saisir la classe de votre enfant.";
		$strError .="</span></p>";
		}else{
			//Prénom, matière et classe remplie on vérifi les donnée
			$strSql = "SELECT * FROM bor_classe c  WHERE c.classe_id = '".mysql_real_escape_string($_POST["classe"][0])."'  LIMIT 1"; 
			$aInfosClasse = $oDb->queryRow($strSql); 
			if(!$oDb->rows){
				$bError = true; 
				$strStep = "step_1_1"; 
				$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
				$strError .= "Nous ne retrouvons pas la formule associée à votre sélection.";
		$strError .="</span></p>";
			}else{
				$_SESSION["products"]["enfants"][0]["classe"] = $_POST["classe"][0]; 
				$_SESSION["products"]["enfants"][0]["matiere"] = ''; 
				$strStep = "step_1_2"; 
				$bStep1 = true; 
			}
		}
	}	
}
//Formulaire 1 tribu
// $_POST['nbenfant']="";
// unset($_SESSION["nb_enfant"]);
// exit;
// var_dump($_POST['action']);
if(isset($_POST["action"]) && $_POST["action"] == "step_3_1"){
	unset($_SESSION["products"]["enfants"]);
	//On se trouve dans une formule reussite on doit donc vérifier que les champs enfant, classe et matière sont bien selectionné
	$iNbEnfant = $_POST['nbenfant'];
	
	
		$iPrenom = count($_POST['prenom']);
		$iClasse = count($_POST['classe']);
		if(  $iNbEnfant!= $iPrenom  ){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Veuillez saisir le prénom de vos enfants.";
		$strError .="</span></p>";
		}else if(  $iNbEnfant!= $iClasse  ){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Veuillez saisir la classe de vos enfants.";
		$strError .="</span></p>";
		}else{
			$bVerifEnfant = true;
			$bVerifClasse = true ;
			for($i = 0; $i < $iNbEnfant; $i++){
				$_SESSION["products"]["enfants"][$i]["classe"] = $_POST["classe"][$i];
				$_SESSION["products"]["enfants"][$i]["D_PRENOM"] = $_POST["prenom"][$i] ; 				
				if(empty($_POST['prenom'][$i]) )
					$bVerifEnfant = false ; 
				if( empty($_POST['classe'][$i]))
					$bVerifClasse = false ; 
			}
			if(!$bVerifEnfant || !$bVerifClasse){
				$bError = true; 
				$strStep = "step_1_1"; 
				if(!$bVerifEnfant ){
					$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
					$strError .= "Veuillez saisir le prénom de vos enfants.";
		$strError .="</span></p>";
				}if (!$bVerifClasse ){
					$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
					$strError .= "Veuillez saisir la classe de vos enfants.";
		$strError .="</span></p>";
				}
			}else{
				for($i = 0; $i < $iNbEnfant; $i++){
					$_SESSION["products"]["enfants"][$i]["D_PRENOM"] = $_POST["prenom"][$i] ; 
					$strSql = "SELECT * FROM bor_classe c  WHERE c.classe_id = '".mysql_real_escape_string($_POST["classe"][$i])."'  LIMIT 1"; 
					$aInfosClasse = $oDb->queryRow($strSql); 
					$_SESSION["products"]["enfants"][$i]["classe"] = $_POST["classe"][$i]; 
					$_SESSION["products"]["enfants"][$i]["matiere"] = ''; 
					$strStep = "step_1_2"; 
					$bStep1 = true; 
				}
			}
		}
	
}

//Formulaire 2
if(isset($_POST["action"]) && $_POST["action"] == "step_1_2"){
	//On vient de poster le formulaire numéro 2 permettant de récupérer l'id_yonix de la duree d'engagement
	$iDeIdYonix = mysql_real_escape_string($_POST['de']); 
	
	$iFormuleId = $_SESSION['cart']['formule']['formule_id'];
	$strFormuleIdYonix = mysql_real_escape_string($_SESSION['cart']['formule']['formule_id_yonix']);
	$strFormuleClasse = $_SESSION['cart']['formule']['formule_classe'];
	if($strFormuleClasse == "ciblee"){
		$strClasse = $oDb->queryItem("SELECT classe_id_yonix FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
		$strMatiere = $oDb->queryItem("SELECT matiere_id_yonix FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['matiere'])."'");
		$strDe = $oDb->queryItem("SELECT de_id FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
						
		//récupération du produit 
		$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."' AND classe_id_yonix = '".$strClasse."' AND matiere_id_yonix = '".$strMatiere."' LIMIT 1"; 
		$aProduit = $oDb->queryRow($strSql); 
		if($oDb->rows > 0 ){
			$_SESSION["products"]["enfants"][0]["ean"] = $aProduit['produit_ean'];
			$_SESSION["products"]["enfants"][0]["duree_engagement"] = $strDe ; 
			$_SESSION["products"]["enfants"][0]["ean_reussite"] = $aProduit['produit_ean'];
			$strStep = "step_1_3";
			$bStep1 = true; 
			$bStep2 = true;			
		}else{
			$strStep = "step_1_2"; 
			$bStep1 = true; 
			$bStep2 = false; 
			$strErreurStep2 = "Une erreur est survenue lors de votre sélection. Veuillez réessayer ultérieurement."; 
			
			$strMessage = "Produit non trouvé lors de la commande.<br><br>" ; 
			$strMessage .= "Clients<br>" ;
			$strMessage .= "- Id client web : ".$_SESSION['user']["IDCLIENTWEB"]."<br>" ;
			$strMessage .= "- Num client : ".$_SESSION['user']["NUMCLIENT"]."<br>" ;
			$strMessage .= "- Nom : ".$_SESSION['user']["NOMCLIENT"]."<br>" ;
			$strMessage .= "- Prénom : ".$_SESSION['user']["PRENOMCLIENT"]."<br>" ;
			$strMessage .= "- Email : ".$_SESSION['user']["EMAIL"]."<br>" ;
			
			// $strMessage .= print_r($_SESSION["user"], true) ;
			$strMessage .= "<br><br>"; 
			$strMessage .= "Formule<br>" ;
			$strMessage .= "- Id Yonix : ".$_SESSION['cart']["formule"]["formule_id_yonix"]."<br>" ;
			$strMessage .= "- Libellé : ".$_SESSION['cart']["formule"]["formule_libelle_yonix"]."<br>" ;
			//Récupération de la classe : 
			$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id = '".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["classe"])."' "); 
			$strMessage .= "- Classe : ".$strClasse."<br>" ;
			$strMatiere = $oDb->queryItem("SELECT matiere_titre FROM bor_matiere WHERE matiere_id = '".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["matiere"])."' "); 
			$strMessage .= "- Matière : ".$strMatiere."<br>" ;
			$strDeLibelle = $oDb->queryItem("SELECT de_libelle_yonix FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
			$strMessage .= "- Durée d'engagement : ".$strDeLibelle."<br>" ;
			
			$mail = new intyMailer(); 
			// OPTIONAL 
			$mail->set(array('charset'  , 'utf-8')); 
			$mail->set(array('priority' , 3)); 
			$mail->set(array('replyTo'  , "noreply@bordas.tm.fr")); 
			$mail->set(array('alert '    ,  true)); 
			$mail->set(array('html'     ,  true)); 
			// OPTIONAL 
			$mail->set(array('subject' , "[".$_CONST["SITE_NAME"]."] Commande : Produit non retrouvé")); 
			$mail->set(array('message' , $strMessage)); 
			$mail->set(array('sending' , 'FROM' , "Bordas Soutien Scolaire" ,"noreply@bordas.tm.fr")); 
			$aStrEmail = "hz.hedizouari@gmail.com";
			$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
			$mail->send();
			$aStrEmail = "hgonzalez-quijano@sejer.fr";
			$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
			$mail->send();
			$aStrEmail = "vbesakian@sejer.fr";
			$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
			$mail->send();
			
		}
	}else if($strFormuleClasse == "reussite"){
		$strClasse = $oDb->queryItem("SELECT classe_id_yonix FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
		$strDe = $oDb->queryItem("SELECT de_id FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
		$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."' AND classe_id_yonix = '".$strClasse."'  LIMIT 1"; 
		$aProduit = $oDb->queryRow($strSql); 
		if($oDb->rows > 0 ){
			$_SESSION["products"]["enfants"][0]["ean"] = $aProduit['produit_ean'];
			$_SESSION["products"]["enfants"][0]["duree_engagement"] = $strDe ; 
			$_SESSION["products"]["enfants"][0]["ean_reussite"] = $aProduit['produit_ean'];
			$strStep = "step_1_3";
			$bStep1 = true; 
			$bStep2 = true;			
		}else{
			$strStep = "step_1_2"; 
			$bStep1 = true; 
			$bStep2 = false; 
			$strErreurStep2 = "Une erreur est survenue lors de votre sélection. Veuillez réessayer ultérieurement."; 
		
			$strMessage = "Produit non trouvé lors de la commande.<br><br>" ; 
			$strMessage .= "Clients<br>" ;
			$strMessage .= "- Id client web : ".$_SESSION['user']["IDCLIENTWEB"]."<br>" ;
			$strMessage .= "- Num client : ".$_SESSION['user']["NUMCLIENT"]."<br>" ;
			$strMessage .= "- Nom : ".$_SESSION['user']["NOMCLIENT"]."<br>" ;
			$strMessage .= "- Prénom : ".$_SESSION['user']["PRENOMCLIENT"]."<br>" ;
			$strMessage .= "- Email : ".$_SESSION['user']["EMAIL"]."<br>" ;
			
			// $strMessage .= print_r($_SESSION["user"], true) ;
			$strMessage .= "<br><br>"; 
			$strMessage .= "Formule<br>" ;
			$strMessage .= "- Id Yonix : ".$_SESSION['cart']["formule"]["formule_id_yonix"]."<br>" ;
			$strMessage .= "- Libellé : ".$_SESSION['cart']["formule"]["formule_libelle_yonix"]."<br>" ;
			//Récupération de la classe : 
			$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id = '".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["classe"])."' "); 
			$strMessage .= "- Classe : ".$strClasse."<br>" ;
			$strDeLibelle = $oDb->queryItem("SELECT de_libelle_yonix FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
			$strMessage .= "- Durée d'engagement : ".$strDeLibelle."<br>" ;
			$mail = new intyMailer(); 
			// OPTIONAL 
			$mail->set(array('charset'  , 'utf-8')); 
			$mail->set(array('priority' , 3)); 
			$mail->set(array('replyTo'  , "noreply@bordas.tm.fr")); 
			$mail->set(array('alert '    ,  true)); 
			$mail->set(array('html'     ,  true)); 
			// OPTIONAL 
			$mail->set(array('subject' , "[".$_CONST["SITE_NAME"]."] Commande : Produit non retrouvé")); 
			$mail->set(array('message' , $strMessage)); 
			$mail->set(array('sending' , 'FROM' , "Bordas Soutien Scolaire" ,"noreply@bordas.tm.fr")); 
			$aStrEmail = "hz.hedizouari@gmail.com";
			$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
			$mail->send();
			$aStrEmail = "hgonzalez-quijano@sejer.fr";
			$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
			$mail->send();
			$aStrEmail = "vbesakian@sejer.fr";
			$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
			$mail->send();
		
		
		
		
		
		}
	}else if($strFormuleClasse == "tribu"){
		$strDe = $oDb->queryItem("SELECT de_id FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
		$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."'  LIMIT 1"; 
		$aProduit = $oDb->queryRow($strSql); 
		if($oDb->rows > 0 ){
		
			
			


			for($i= 0; $i<$_SESSION['nb_enfant'] ;$i++){
				$_SESSION["products"]["enfants"][$i]["ean"] = $aProduit['produit_ean'];
				$_SESSION["products"]["enfants"][$i]["duree_engagement"] = $strDe ; 
				//Récupération de l'ean du produit reussite concerné
				$strSql = "SELECT p.produit_ean FROM bor_produit p 
												INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix)
												INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
												WHERE c.classe_id = '".mysql_real_escape_string($_SESSION["products"]["enfants"][$i]['classe'])."'
													AND	f.formule_classe = 'reussite'
													AND p.de_id_yonix = '".$iDeIdYonix."'"; 
				$strEan = $oDb->queryItem($strSql); 
				$_SESSION["products"]["enfants"][$i]["ean_reussite"] = $strEan ; 
			}
				
			$bProduit = true; 
			foreach( $_SESSION["products"]["enfants"] as $aEnfant){
				$strSql = "SELECT * FROM bor_produit p INNER JOIN bor_duree_engagement d ON(d.de_id_yonix = p.de_id_yonix) INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix) WHERE c.classe_id = '".mysql_real_escape_string($aEnfant["classe"])."' AND f.formule_classe = 'reussite' AND d.de_id = '".mysql_real_escape_string($aEnfant["duree_engagement"])."' "; 
				$aProduitEnfant = $oDb->queryRow($strSql); 
				if(!$oDb->rows){
					$bProduit = false ; 
				}
			}
			
			if($bProduit){
				$strStep = "step_1_3";
				$bStep1 = true; 
				$bStep2 = true;	
			}else{
				$strStep = "step_1_2"; 
				$bStep1 = true; 
				$bStep2 = false; 
				$strErreurStep2 = "Une erreur est survenue lors de votre sélection. Veuillez réessayer ultérieurement."; 
				
				$strMessage = "Produit non trouvé lors de la commande.<br><br>" ; 
			$strMessage .= "Clients<br>" ;
			$strMessage .= "- Id client web : ".$_SESSION['user']["IDCLIENTWEB"]."<br>" ;
			$strMessage .= "- Num client : ".$_SESSION['user']["NUMCLIENT"]."<br>" ;
			$strMessage .= "- Nom : ".$_SESSION['user']["NOMCLIENT"]."<br>" ;
			$strMessage .= "- Prénom : ".$_SESSION['user']["PRENOMCLIENT"]."<br>" ;
			$strMessage .= "- Email : ".$_SESSION['user']["EMAIL"]."<br>" ;
			
			// $strMessage .= print_r($_SESSION["user"], true) ;
			$strMessage .= "<br><br>"; 
			$strMessage .= "Formule<br>" ;
			$strMessage .= "- Id Yonix : ".$_SESSION['cart']["formule"]["formule_id_yonix"]."<br>" ;
			$strMessage .= "- Libellé : ".$_SESSION['cart']["formule"]["formule_libelle_yonix"]."<br>" ;
			//Récupération de la classe : 
			foreach( $_SESSION["products"]["enfants"] as $aEnfant){
				$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id = '".mysql_real_escape_string($aEnfant["classe"])."' "); 
				$strMessage .= "- Classe : ".$strClasse."<br>" ;			
			}
			$strDeLibelle = $oDb->queryItem("SELECT de_libelle_yonix FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
			$strMessage .= "- Durée d'engagement : ".$strDeLibelle."<br>" ;
				$mail = new intyMailer(); 
				// OPTIONAL 
				$mail->set(array('charset'  , 'utf-8')); 
				$mail->set(array('priority' , 3)); 
				$mail->set(array('replyTo'  , "noreply@bordas.tm.fr")); 
				$mail->set(array('alert '    ,  true)); 
				$mail->set(array('html'     ,  true)); 
				// OPTIONAL 
				$mail->set(array('subject' , "[".$_CONST["SITE_NAME"]."] Commande : Produit non retrouvé")); 
				$mail->set(array('message' , $strMessage)); 
				$mail->set(array('sending' , 'FROM' , "Bordas Soutien Scolaire" ,"noreply@bordas.tm.fr")); 
				$aStrEmail = "hz.hedizouari@gmail.com";
				$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
				$mail->send();
				$aStrEmail = "hgonzalez-quijano@sejer.fr";
				$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
				$mail->send();
				$aStrEmail = "vbesakian@sejer.fr";
				$mail->set(array('sending' , 'TO'   , "" ,$aStrEmail)); 
				$mail->send();
			}
		}else{
			$strStep = "step_1_2"; 
			$bStep1 = true; 
			$bStep2 = false; 
			$strErreurStep2 = "Une erreur est survenue lors de votre sélection. Veuillez réessayer ultérieurement."; 
		}
	}
	// var_dump($_CONST['URL_ACCUEIL']);
	$bErreurOffre =false;
	if( isset($_SESSION['user']['IDCLIENT']) && $_SESSION['user']['IDCLIENT'] > 0 && $bStep2){
		
		$strSql = "SELECT (commande_id) FROM bor_commande c INNER JOIN bor_client cl ON (cl.client_id = c.client_id) 
				WHERE commande_total =  '' AND  cl.client_email = '".mysql_real_escape_string($_SESSION['user']['EMAIL'])."' 
				AND commande_date > ADDDATE(NOW(), INTERVAL -6 MONTH)"; 
				
		$oDb->queryRow($strSql); 
		if($oDb->rows > 0  ){
			$strStep = "step_1_3";
			$bStep1 = true; 
			$bStep2 = true;	
			$bStep3 = false;
			$bErreurOffre = true ;

		}else{			
			//L'utilisateur est déjà connecté => redirection vers la page paiement
			echo "<script>document.location.href='". $_CONST["URL2"]."/abonnement/offre-decouverte-formule-reussite-bordas-soutien-scolaire-etape-3.html';</script>";
			exit; 
		}
	}
	
}

//Traitement du retour de la connexion via le WS PER
if(isset($_SESSION["connexion"]) && !empty($_SESSION['connexion'])){
	if($_SESSION["connexion"] == "ok"){
		$strSql = "SELECT (commande_id) FROM bor_commande c INNER JOIN bor_client cl ON (cl.client_id = c.client_id) 
				WHERE cl.client_email = '".mysql_real_escape_string($_SESSION['user']['EMAIL'])."' 
				AND commande_date > ADDDATE(NOW(), INTERVAL -6 MONTH)"; 
				// var_dump($strSql); 
		$oDb->queryRow($strSql); 
			
		if($oDb->rows > 0){
			$strStep = "step_1_3";
			$bStep1 = true; 
			$bStep2 = true;	
			$bStep3 = false;
			$bErreurOffre = true ;
			
			
		}else{	
			unset($_SESSION["connexion"]);
			echo "<script>document.location.href='". $_CONST["URL2"]."/abonnement/offre-decouverte-formule-reussite-bordas-soutien-scolaire-etape-3.html';</script>";
			exit; 
		}
	}else if($_SESSION["connexion"] == "erreur_connexion"){
		$strStep = "step_1_3";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bError = true;
		$strError = "Vos identifiant et mot de passe sont incorrects !";
		$bPopupConnexion = true ; 
	}else if($_SESSION["connexion"] == "erreur_champ"){
		$strStep = "step_1_3";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bError = true;
		$strError = "Veuillez renseigner votre identifiant et mot de passe Bordas.";
		$bPopupConnexion = true ; 
	}else if($_SESSION["connexion"] == "erreur_profil"){
		$strStep = "step_1_3";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bError = true;
		$strError = "Vous devez posséder un compte Parent pour utiliser ce site. Si vous n’en avez pas, inscrivez-vous directement sur le site Bordas soutien scolaire.";
		$bPopupConnexion = true ; 
	}
	unset($_SESSION["connexion"]);
}
// unset($_SESSION["user"]);
// var_dump($strStep);
if( isset($_SESSION['mdp_forget']) && !empty($_SESSION['mdp_forget'])){
	$strStep = "step_1_3";
	
	
	
	
}
if( isset($_SESSION['creation_compte']) && !empty($_SESSION['creation_compte']) && $_SESSION['creation_compte'] === "ENCOURS"){
	// var_dump($_SESSION['creation_compte']);
	$strStep = "step_1_3";
	unset($_SESSION['creation_compte']);
	
}
// var_dump($strStep);
if(!isset($strStep) || empty($strStep))
	$strStep = "step_1_1"; 
// var_dump($strStep); 
?>
<div class="container">
   <div class="row">
     
      <ul id="steps" class="col-md-12">
         <li class="detail_formule"><a class="active"><strong>1.</strong>Preuve d'achat du livre<span class="arrow_right"></span></a></li>
         <li class="confirmation"><a  class="active" ><span class=" arrow_left "></span><strong>2.</strong>Détail de la formule <span class="arrow_right"></span></a></li>
		 <li class="paiement"><a ><span class="arrow_left"></span><strong>3.</strong>Confirmation <span class="arrow_right"></span></a></li>
      </ul>
   </div>
   <section class="description_formule">
      <div class="picto_formule">
         <img src="/images/reussite_picto.png" alt="Offre Découverte - Formule Réussite">
      </div>
      <div class="right_description">
          <h1><?php echo get_template_title(); ?></h1>
         <?php echo get_template_data(); ?>
      </div>
   </section>
   
   
   <div class="detail_formule">
      <div class="title_formule">
         <h2>
            Je crée le compte de mon enfant
         </h2>
      </div>
      <div class="body_formule ">
         
		 <?php 
		if((isset($_POST['nb_enfant']) && $_POST['nb_enfant'] < 1)){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p><img src='/images/warning_icon.png'><span class='alert-link'>"; 
			$strError .= "Veuillez sélectionner un nombre d’enfants.";
			$strError .= "</span></p>";
		}
		 if($strStep =="step_1_1" && $bError){
			echo ' <div class="alert alert-danger">
				'.$strError.'
			 </div>';
		 }
		 
				//Récupération de la liste des classes disponibles
				$ListClasses  = $oDb->queryTab("SELECT c.* FROM bor_classe c 
															INNER JOIN bor_produit p ON (p.classe_id_yonix = c.classe_id_yonix) 
															INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) 
															INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id) 
															INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 		
														WHERE f.formule_classe = 'reussite' 
														GROUP BY c.classe_id
														ORDER BY n.niveau_position, cn.cn_position"); 
				echo '	<form class="form-horizontal" role="form" method="post" id="creer_compte_enfant" name="form_enfant"> 
							<div class="form-group">
							   <label for="inputEmail3" class="col-sm-3 control-label">Prénom de mon enfant : 
							   </label>
							   <div class="col-sm-3">
								  <input type="text" name="prenom[0]" class="form-control" id="inputEmail3" placeholder="Prénom" value="'.((isset($_SESSION["products"]["enfants"][0]["D_PRENOM"]) && !empty($_SESSION["products"]["enfants"][0]["D_PRENOM"])) ? $_SESSION["products"]["enfants"][0]["D_PRENOM"] : "" ).'">
							   </div>
							</div>
							<div class="form-group">
							   <label for="inputPassword3" class="col-sm-3 control-label">Classe :</label>
							   <div class="col-sm-3">
								  <select name="classe[0]" id="classe"  class="classe">
									 <option value="0">Choisir une classe</option>'; 	
										if($ListClasses){ 
											foreach($ListClasses as $classe){
												echo '<option value="'.$classe['classe_id'].'" '.(( isset($_SESSION["products"]["enfants"][0]["classe"]) && $_SESSION["products"]["enfants"][0]["classe"] == $classe['classe_id']) ? 'selected' : '' ) .'>'.$classe['classe_name'].'</option>'; 
											}  
										}
								echo '</select>
							   </div>
							</div>
							<div class="row" style="text-align: center;">
								 Votre enfant aura accès à toutes les matières disponibles. <a href="#" class="autres_matieres_link"  data-toggle="modal" data-target="#popupmatieres_0">Voir les autres matières bientôt disponibles</a> 
								</div>
							<div class="form-group">
							   <div class="col-sm-offset-3 col-sm-6">'; 
							   if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
									?>
										<a href="#" onclick="ga('send', 'event', 'bouton', 'clic', 'Etape 2 - Bloc : Je crée le compte de mon enfant - ÉTAPE SUIVANTE', 4);jQuery('#creer_compte_enfant').submit();" class="btn btn-orange col-sm-offset-4"  style="margin-top:25px;">Étape suivante >></a>
							 
									<?php
								}else{
									?>
										<a href="#" onclick="jQuery('#creer_compte_enfant').submit();" class="btn btn-orange col-sm-offset-4"  style="margin-top:25px;">Étape suivante >></a>							 
									<?php
								}
							echo '  </div>
							</div>
							<input type="hidden" name="action" value="step_2_1"/>
						</form>'; 
			
		 ?>
         
      </div>
      <!--***********************-->
      <div class="title_formule" id="choixformule" ><h2>Je valide ma durée d’abonnement</h2></div>
	  <?php 
		if($strStep =="step_1_2"  || $strStep =="step_1_3" ){
			// var_dump($_SESSION["products"]["enfants"][0]); 
			$iFormuleId = $_SESSION['cart']['formule']['formule_id'];
			$strFormuleIdYonix = $_SESSION['cart']['formule']['formule_id_yonix'];
			$strFormuleClasse = $_SESSION['cart']['formule']['formule_classe'];
			
			//Récupération des 3 duree d'engagement disponible
			$strSql = "SELECT * FROM bor_offre ORDER BY offre_position"; 
			$aDureeEngagement = $oDb->queryTab($strSql); 
			
			
			
			
			
			
			
	  ?>
			<div class="body_formule "  > 
	<?php
		if(isset($strErreurStep2) && !empty($strErreurStep2)){
			echo '<div class="alert alert-danger">
					<img src="/images/warning_icon.png" alt="">
					<a href="#" class="alert-link">'.$strErreurStep2.'</a>
				</div>';
			unset($strErreurStep2); 
		}
	?>
				<!--<div class="alert alert-success">
					<img src="../../images/success_icon.png" alt="">
					<a href="#" class="alert-link">Bien fait! les champs sont bien renseignés.</a>
				</div> 
				<div class="alert alert-danger">
					<img src="../../images/warning_icon.png" alt="">
					<a href="#" class="alert-link">Message d’erreur pour signaler un champs non renseigné.</a>
				</div>-->
				<?php 
					//Affichage de la liste des enfants 
					if($strFormuleClasse == "ciblee"){
						//Récupération du nom de la classe et la matière
						$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
						$strMatiere = $oDb->queryItem("SELECT matiere_titre FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['matiere'])."'");
						echo '<p class="detail_abonnement">Abonnement pour '.$_SESSION["products"]["enfants"][0]['D_PRENOM'].' en classe de '.$strClasse.' en '.$strMatiere.'.</p>'; 				
					}else if( $strFormuleClasse == "reussite"){
						//Récupération du nom de la classe et la matière
						$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
						echo '<p class="detail_abonnement">Abonnement pour '.$_SESSION["products"]["enfants"][0]['D_PRENOM'].' en classe de '.$strClasse.'.</p>'; 				
					}else if($strFormuleClasse == "tribu"){
						echo '<p class="detail_abonnement">'; 
						foreach($_SESSION["products"]["enfants"] as $aEnfant){
							$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($aEnfant['classe'])."'");
							$strMatiere = $oDb->queryItem("SELECT matiere_titre FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($aEnfant['matiere'])."'");
							echo 'Abonnement pour '.$aEnfant['D_PRENOM'].' en classe de '.$strClasse.'.<br/>'; 
						}
						echo '</p>';
					}
				?>
				<div class="form-group" style="display:none;">
					<label for="inputPassword3" class="col-sm-5 control-label">Si vous avez un <strong>code promotionnel</strong>, saississez-le :</label>
					<div class="col-sm-3 code_promotionnel">
						<input type="email" class="form-control" id="inputEmail3" >
					</div>
					<div class="col-sm-2 valid_code_promotionnel">
						<input type="button" class="form-control btn btn-orange " id="inputEmail3" value="ok" >
					</div>
				</div>  
				<p class="choix_duree">Dans le cadre de cette offre découverte, la durée d’abonnement est automatiquement d’un mois</p>
				<div class="row packs_form">  
					<?php 
					
						//Affichage de la liste des offres
						foreach ($aDureeEngagement as $aDe){
							//Pour chaque duree d'engagement, on doit récupérer le prix d'un produit 
							/*$strSql = "SELECT produit_prix FROM bor_produit WHERE formule_id_yonix = '".mysql_real_escape_string($strFormuleIdYonix)."' AND de_id_yonix = '".mysql_real_escape_string($aDe['offre_id_yonix'])."' LIMIT 1"; */
							/*$fPrix = $oDb->queryItem($aDe['offre_id_yonix']) ;
							var_dump($fPrix);*/
							/*$fPrixMois = round( $fPrix / $aDe['de_engagement'] , 2 ) ; */
							echo '	<form id="form_choix_de_62726" method="post" >
									<input type="hidden" name="action" value="step_1_2"/>
									<input type="hidden" name="de" value="62726"/>
									<div class="pack_form '.$strFormuleClasse.' col-md-4" style="    margin: auto;    float: none;">
										<div class="title_pack">'.$aDe['offre_desc2'].'<br><span> GRATUIT <span> / mois</span></span> </div>
										<div class="body_pack">'; 
											echo '<p class="paiement_pack">Cette offre est <span>non cumulable avec une autre offre</span> et est <span>valable une seule fois</span>.</p>';
										if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
											?>
												<div class="center"><a href="#" onclick="ga('send', 'event', 'bouton', 'clic', 'Etape 2 - Bloc : Je valide ma durée d\'abonnement - JE CHOISIS', 4);jQuery('#form_choix_de_62726').submit();" class="btn btn-orange">Je choisis</a></div>
											<?php
										}else{
											?>
												<div class="center"><a href="#" onclick="jQuery('#form_choix_de_62726').submit();" class="btn btn-orange">Je choisis</a></div>
											<?php
										}
									echo '	
											<p class="description_pack">
												'.$aDe['offre_condition'].'
											</p>
										</div>    
									</div>
									</form>';
						}
						
					?>
					
					
				</div>   
			</div>    
		<?php 
			}
		?>
			
      <!--***********************-->
      <div class="title_formule" id="compteparent"><h2>Je crée mon compte parent</h2></div>
		<?php 
			if( $strStep =="step_1_3" ){
				if($bErreurOffre){
					echo '<div class="body_formule " id="creer_mon_compte">  '; 
						echo ' <div class="alert alert-danger red"><img src="/images/warning_icon.png">
									Vous avez déjà utilisé cette offre, vous ne pouvez plus y avoir droit.
								 </div>';
						 if($_CONST['TYPE_ENVIRONNEMENT'] !='dev' ){
							echo '<div class="" style="text-align: center;"><a href="/" onclick="ga(\'send\', \'event\', \'lien hypertexte\', \'clic\', \'Etape 2 - Bloc : Je crée mon compte parent - RETOURNER SUR LE SITE\', 4);">Retourner sur le site &gt;&gt;</a></div>'; 
						}else{
							echo '<div class="" style="text-align: center;"><a href="/">Retourner sur le site &gt;&gt;</a></div>'; 
						}
					 echo '</div>'; 
				}else{
		?>
												<div class="body_formule " id="creer_mon_compte">    
													<p>Inscrivez-vous pour bénéficier gratuitement d’un accès privilégié aux sites des Éditions Bordas, dont <strong>Bordas Soutien scolaire</strong></P>
													<p>Si vous avez déjà un compte «Bordas»,     
													<?php
													if($_CONST['TYPE_ENVIRONNEMENT'] !='dev' ){
														?>
															<a  class="btn_inscription" data-toggle="modal" data-target="#inscription" id="btn_connexion" onclick="ga('send', 'event', 'lien hypertexte', 'clic', 'Etape 2 - Bloc : Je crée mon compte parent - CONNECTEZ-VOUS', 4);">connectez-vous</a> 				
														<?php
													}else{
														?>
															<a  class="btn_inscription" data-toggle="modal" data-target="#inscription" id="btn_connexion">connectez-vous</a> 
														<?php
													}
													?>
						
													pour passer à l’étape suivante.</p>
													<!-- Modal -->
													<?php 
													// var_dump(
													if($strStep =="step_1_3" && $bError && !$bPopupConnexion){
														echo ' <div class="alert alert-danger red">
															'.$strError.'
														 </div>';
													 }
													 ?>
													 
								<div class="modal fade" id="inscription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="modal-title" id="myModalLabel">Vous êtes déjà inscrit ?</h4>
												<?php 
													if(isset($bPopupConnexion) && $bPopupConnexion){
														echo '<div class="" id="news-warning" >
																	<div class="hero-unit text-center red">
																		'.$strError.'
																	</div>              
																</div>';
													}
												?>
											</div>
											<div class="modal-body">
												<p>Merci de vous identifier :</p>
												<form class="form-horizontal" role="form" action="/proxy/proxy_connexion.php" method="post">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-5 control-label">Votre identifiant :</label>
														<div class="col-sm-7">
															<input type="text" class="form-control" id="inputEmail3" value="" name="username">
														</div>
													</div>
													<div class="form-group">
														<label for="inputPassword3" class="col-sm-5 control-label">Votre mot de passe :</label>
														<div class="col-sm-7">
															<input type="password" class="form-control" id="inputPassword3"  name="password">
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-5 col-sm-10">
															<button type="submit" class="btn btn-orange">Connexion »</button>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-offset-2 col-sm-10">
														<div class="checkbox">
															<a href="#" onclick="jQuery('#inscription').removeClass('in');jQuery('#inscription').hide();jQuery('.modal-backdrop').remove();" class="mot_oublie" data-toggle="modal" data-target="#motdepasseoublie" id="forget_mdp"> Identifiant ou mot de passe oublié ?</a>
														</div>
														</div>
													</div>
													<input type="hidden" name="redirect_after" value="<?php echo (  $_CONST['URL2'] ). $_CONST['URL_ACCUEIL']  ; ?>abonnement/offre-decouverte-formule-reussite-bordas-soutien-scolaire-etape-2.html" >
												</form>
											</div>
										</div>
									</div>
								</div>


								<script type="text/javascript">
										jQuery.noConflict();	
								</script>

													<?php
													// var_dump('tete'); 
													include("./proxy/proxy_createAccount_decouverte.php");
													// var_dump('tete'); 
														
													?>
													
													<div class="formulaire_form"> 
													<p class="obligatoire"> <strong>*</strong> champs obligatoires</p>
													<p class="description_pack">
													Conformément à la loi Informatique et Libertés du 6 janvier 1978 modifiée en 2004, vous disposez d’un droit d’accès, de rectification ou d’opposition aux données personnelles vous concernant, à la Relation client BORDAS : <a href="mailto:soutien-scolaire@bordas.tm.fr">soutien-scolaire@bordas.tm.fr</a>, <?php echo $_CONST['RC_TEL']; ?>.
													</p>
													</div>    
												</div>
		<?php 
			}
		}
		?>
   </div>
</div>
 
 
<?php 


//Création d'un popup pour tous les enfants 
if(isset($_SESSION['nb_enfant']) && $_SESSION['nb_enfant'] > 0 ){
	for($i=0; $i<$_SESSION['nb_enfant']; $i++){
		echo '
		<!-- Modal pop up autres matieres -->
		<div class="modal fade popupmatieres" id="popupmatieres_'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		   <div class="modal-dialog">
			  <div class="modal-content">
				 <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Liste des matières disponibles en <span id="modal_matiere_titre_'.$i.'"></span></h4>
					<div class="" id="news-warning" >
						<div class="hero-unit text-center red" id="error_'.$i.'">
						</div>              
					</div>
					<div class="" id="news-success" >
						<div class="hero-unit text-center green" style="color:green;" id="success_'.$i.'">
						</div>              
					</div>
				 </div>
					<div class="modal-body" id="body_matiere_'.$i.'">

					</div>
					<div class="modal-footer" id="modal_matiere_body_'.$i.'" style="display:none;">
						<div class="spacer"></div>
						<form method="post">
						<div class="form-group">
						   <label for="inputPassword3" class=" control-label">Si vous souhaitez être informé lorsque de nouvelles
matières sont disponibles, laissez-nous votre email : </label>
						   <div class="col-sm-10">
							  <input type="email" class="form-control" id="inputEmail_'.$i.'" required name="modal_email">
						   </div>
						   <div class="col-sm-2">
							  <input type="button"  onclick="valider_formulaire('.$i.');" class="form-control btn btn-orange" id="inputEmail3" value="ok">
							  <input type="hidden" name="action" value="save_classe"/>
							  <input type="hidden" id="modal_classe_'.$i.'" name="modal_classe" value=""/>
						   </div>
						</div>
						</form>
					</div>
			  </div>
		   </div>
		</div>';
	}
}else{
	echo '
	<div class="modal fade popupmatieres" id="popupmatieres_0" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	   <div class="modal-dialog">
		  <div class="modal-content">
			 <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Liste des matières disponibles en <span id="modal_matiere_titre_0"></span></h4>
				<div class="" id="news-warning" >
					<div class="hero-unit text-center red" id="error_0">
					</div>              
				</div>
				<div class="" id="news-success" >
					<div class="hero-unit text-center green"   style="color:green;" id="success_0">
					</div>              
				</div>
			 </div>
				<div class="modal-body" id="body_matiere_0">
				</div>
			<form method="post">
			   <div class="modal-footer" id="modal_matiere_body_0"  style="display:none;"> 
					<div class="spacer"></div>
					<div class="form-group">
					   <label for="inputPassword3" class=" control-label">Si vous souhaitez être informé lorsque de nouvelles
matières sont disponibles, laissez-nous votre email : </label>
					   <div class="col-sm-10">
						   <input type="email" class="form-control" id="inputEmail_0" required name="modal_email">
					   </div>
					   <div class="col-sm-2 valid_matiere_button">
						    <input type="button" onclick="valider_formulaire(0);" class="form-control btn btn-orange " id="inputEmail3" value="ok">
							<input type="hidden" name="action" value="save_classe"/>
							<input type="hidden" id="modal_classe_0" name="modal_classe" value=""/>
					   </div>
					</div>
				</div>
				</form>
				
		
			 
		  </div>
	   </div>
	</div>
';
}

?>







<div class="modal fade" id="motdepasseoublie" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Vous avez oublié votre mot de passe ?</h4>
				<?php 
					$bPostForget = false;
					$bForget = false;
					if( isset($_SESSION['mdp_forget']) && !empty($_SESSION['mdp_forget'])){
						$bForget =true ;
						if($_SESSION['mdp_forget'] == "ok" ){
							$bPostForget = true;
							$strMessage = "Un e-mail vient de vous être envoyé. Veuillez suivre les instructions qui s'y trouvent.";
						}else if($_SESSION['mdp_forget'] == "erreur_update" ){
							$strMessageBox = "Cette adresse email n'est pas reconnue sur ce site. Si vous avez changé d'adresse email, veuillez  nous contacter." ; 
						}else if($_SESSION['mdp_forget'] == "erreur_champ" ){
							$strMessageBox = "Veuillez renseigner votre email." ;  
						}else if($_SESSION['mdp_forget'] == "erreur_technique" ){
							$strMessageBox = "Un problème technique empêche la récupération du mot de passe." ; 
						}else if($_SESSION['mdp_forget'] == "champ_incorrect" ){
							$strMessageBox = "Veuillez renseigner une adresse email valide." ; 
						}
						unset( $_SESSION['mdp_forget'] ) ;
						
					}
					if($bPostForget){
						echo '	<div class="" id="news-success" >
									<div class="hero-unit text-center green">
										'.$strMessage.'
									</div>              
								</div>';
					}else if(isset($strMessageBox) && !empty($strMessageBox)){
						echo '<div class="" id="news-warning" >
								<div class="hero-unit text-center red">
									'.$strMessageBox.'
								</div>              
							</div>';
					}
				?>
			</div>
			<div class="modal-body">
			<?php
				if(!$bPostForget){
			?>
				<form action="/proxy/proxy_mdpForget.php"  method="post" id="forgot-form">	
					
					<div class="modal-body row">
							<p>Pour retrouver l'identifiant et le mot de passe que vous avez créés lors de votre inscription sur le site des Editions Bordas ou de Bordas Soutien scolaire, merci de nous 
indiquer l'adresse e-mail saisie lors de cette inscription. Veuillez suivre les instructions qui figurent dans l'e-mail que vous allez recevoir d'ici quelques minutes.</p>
							
					</div>
					<div class="modal-body row">
					<label>Entrez l’adresse e-mail associée à votre inscription :</label><br>
							<input type="email" id="txtEmail" name="email_forgot" value="" required>
							<input type="hidden" name="redirect_after" value="<?php echo (  $_CONST['URL2'] ). $_CONST['URL_ACCUEIL']  ; ?>abonnement/offre-decouverte-formule-reussite-bordas-soutien-scolaire-etape-2.html" >
						
					</div>
					
					<div class="modal-footer">
						<button type="submit" class="btn btn-orange">Valider »</button>
					</div>
				</form>	 
			<?php
			}
			unset($_SESSION['mdp_forget']);
			?>
			</div>
		</div>
	</div>
</div>














<?php 
	if(isset($bPopupConnexion) && $bPopupConnexion){
?>
		<script type="text/javascript">
			jQuery('document').ready(function(){
				jQuery('#btn_connexion').click();
			});
		</script>
<?php
	}
	if($bForget){
?>
	<script type="text/javascript">
		jQuery('document').ready(function(){
			jQuery('#forget_mdp').click();
			
		});
	</script>
<?php
	}
?>



<!-- ajax call -->
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.classe').each(function(){
			jQuery.ajax({type:"POST", data:jQuery(this).serialize(), url: "/templates/ajax/bloc_matiere.php" ,dataType: 'script'	});
		});
		
		// liste classe du matiere selectionne
		jQuery('.classe').change(function () {
			// alert('test'); 
			
			jQuery.ajax({type:"POST", data: jQuery(this).serialize(), url: "/templates/ajaxsearch.php" ,dataType: 'json',	success: function(json) {
					jQuery('#matiere option').remove(); 
					jQuery('#matiere').append('<option value="">Choisir une matière</option>');
					jQuery.each(json, function(index, value) { 
						jQuery('#matiere').append('<option value="'+ json[index]['matiere_id'] +'">'+ json[index]['mc_titre'] +'</option>');
					});
				},
				error: function(){
					jQuery('#matiere').html('');
				}
			});
			
			// update_popup(0);
			jQuery.ajax({type:"POST", data: jQuery(this).serialize(), url: "/templates/ajax/bloc_matiere.php" ,dataType: 'script'	});
			
			 return false;
		 
		});	
		
	});	  
	function valider_formulaire(iPopup){
		// alert(iPopup);
		bVerif = true ; 
		jQuery('#error_'+iPopup).html(''); 
		jQuery('#success_'+iPopup).html(''); 
		strEmail = jQuery('#inputEmail_'+iPopup).val() ; 
		iClasse = jQuery('#modal_classe_'+iPopup).val() ; 
		// alert(iClasse); 
		if( strEmail == "" ){
			bVerif = false ; 
			jQuery('#error_'+iPopup).html('Veuillez saisir votre email.'); 
		}
		if( !(iClasse > 0) ){
			bVerif = false ; 
			jQuery('#error_'+iPopup).html('Veuillez saisir la classe de votre enfant.'); 
		}
		
		if(bVerif){
			// alert('ok'); 
			jQuery.ajax(
				{
					type:"POST", 
					data: 
						{
							iPopup : iPopup, 
							iClasse : iClasse, 
							strEmail : strEmail
						}, 
					url: "/templates/ajax/save_email.php" 
				}
			).done(function( msg ) {
				if(msg == 1 ) 	
					jQuery('#success_'+iPopup).html('Nous avons bien enregistré votre demande.');
				else
					jQuery('#error_'+iPopup).html('Veuillez saisir une adresse email valide.');
			});
		}
		
	}
	// function update_popup(iPopup){
		// $.ajax({type:"POST", data: $(this).serialize(), url: "/templates/ajax/bloc_matiere.php" ,dataType: 'script'	});
	// }
</script>

<script type="text/javascript">

	


jQuery(window).load(function(){
	<?php 
		if(isset($strStep) && $strStep == "step_1_2") {
	?>
			jQuery('#choixformule').ScrollTo({
				duration: 1000
			});
	<?php
		}
		if(isset($strStep) && $strStep == "step_1_3") {
	?>
			jQuery('#compteparent').ScrollTo({
				duration: 1000
			});
	<?php
		}
	?>
});
</script>