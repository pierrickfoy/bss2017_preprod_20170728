<?php
//On verifie que la formule existe 
$strSql = "SELECT * FROM bor_formule WHERE formule_classe = '".mysql_real_escape_string($_GET['formule'])."'"; 
$aFormule = $oDb->queryRow($strSql); 
if(!$oDb->rows){
	echo "<script>document.location.href='".$_CONST["URL2"]."/soutien-scolaire-en-ligne/comment-s-abonner.html';</script>";
	exit; 
}else{
	$_SESSION['cart']['formule'] = $aFormule ; 
	$_SESSION["products"]["enfants"][0]["formule"] = $aFormule ["formule_id"] ; 
}
$bError = false ;

if($_GET['formule'] == "ciblee"){
	$ListClasses  = $oDb->queryTab("SELECT c.*
									FROM bor_classe_page cp
									INNER JOIN bor_classe c ON (c.classe_id = cp.classe_id) 
									INNER JOIN bor_produit p ON (p.classe_id_yonix = c.classe_id_yonix) 
									INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) 
									INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id)
									INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 
									GROUP BY c.classe_id
									ORDER BY n.niveau_position, cn.cn_position
									"); 
				
}else if($_GET['formule'] == "reussite"){
	$ListClasses  = $oDb->queryTab("SELECT c.*
									FROM bor_classe_page cp
									INNER JOIN bor_classe c ON (c.classe_id = cp.classe_id) 
									INNER JOIN bor_produit p ON (p.classe_id_yonix = c.classe_id_yonix) 
									INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) 
									INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id)
									INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 	
									WHERE f.formule_classe = 'reussite'
									GROUP BY c.classe_id
									ORDER BY n.niveau_position, cn.cn_position
									"); 
				
}else if($_GET['formule'] == "tribu"){
	$ListClasses  = $oDb->queryTab("SELECT c.*
									FROM bor_classe_page cp
									INNER JOIN bor_classe c ON (c.classe_id = cp.classe_id) 
									INNER JOIN bor_produit p ON (p.classe_id_yonix = c.classe_id_yonix) 
									INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) 
									INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id)
									INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id)
									WHERE f.formule_classe = 'reussite' 
									GROUP BY c.classe_id
									ORDER BY n.niveau_position, cn.cn_position
									"); 
}

$strFormuleIdYonix = mysql_real_escape_string($_SESSION['cart']['formule']['formule_id_yonix']);

/*GESTION DES FORMULAIRE */
//Formulaire 1 ciblee
if(isset($_POST["action"]) && $_POST["action"] == "step_1_1"){
	$_SESSION['de'] = $_POST['de'];
	unset($_SESSION["CODEPROMO"]);
	unset($_SESSION["products"]["enfants"][0]['CODEPROMO']);
	unset($_SESSION["products"]["enfants"][0]['REDUCTION']);  

	$bError = false ; 
	unset($_SESSION["products"]["enfants"]);
	$_SESSION["products"]["enfants"][0]["classe"] = $_POST["classe"][0]; 
	$_SESSION["products"]["enfants"][0]["D_PRENOM"] = $_POST["prenom"][0] ; 
	$_SESSION["products"]["enfants"][0]["matiere"] =  $_POST["matiere"][0] ; 
	//var_dump($_POST);

	//On se trouve dans une formule ciblee on doit donc vérifier que les champs enfant, classe et matière sont bien selectionné
	$fetch = (bool) strlen(trim($_POST["prenom"][0]));
	if(!isset($_POST["prenom"][0]) || empty($_POST["prenom"][0]) || $fetch === false){
		$bError = true; 
		$strStep = "step_1_1"; 
		
		$strError .= "Veuillez saisir le prénom de votre enfant.";
		$strError .="</p>";
	}else{
		if(strpos($_POST["prenom"][0], " ") === 0){
			$_POST["prenom"][0] = substr($_POST["prenom"][0], 1);
		}
		//Initialisation du prénom de l'enfant
		if( !isset($_POST["classe"][0]) || empty($_POST["classe"][0]) ){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "Veuillez saisir la classe de votre enfant.";
		$strError .="</p>";
		}else {
			if(!isset($_POST["matiere"][0]) || empty($_POST["matiere"][0]) ) {
				$bError = true; 
				$strStep = "step_1_1"; 
				$strError .= "Veuillez saisir la matière de votre enfant.";
		$strError .="</p>";
			}else{
				$strSql = "SELECT *
				FROM bor_matiere_classe mc
				INNER JOIN bor_matiere m ON (m.matiere_id = mc.matiere_id)
				INNER JOIN bor_classe cp ON (mc.classe_id = cp.classe_id)
				WHERE cp.classe_id = '".mysql_real_escape_string($_POST["classe"][0])."'
				AND mc.matiere_id  = '".mysql_real_escape_string($_POST["matiere"][0])."'
				AND mc.mc_dispo = 1"; 
				$aInfosClasse = $oDb->queryRow($strSql); 
				if(!$oDb->rows){
					$bError = true; 
					$strStep = "step_1_1"; 
					$strError .= "Nous ne retrouvons pas la formule associée à votre sélection. Veuillez choisir un couple classe/matière valide.";
					$strError .="</p>";
				}else{
					$_SESSION["products"]["enfants"][0]["classe"] = $aInfosClasse["classe_id"]; 
					$_SESSION["products"]["enfants"][0]["matiere"] = $aInfosClasse["matiere_id"]; 
					$strStep = "step_1_2"; 
					$bStep1 = true; 
				}
			}
		}
	}	
	$iDeIdYonix = mysql_real_escape_string($_POST['de']); 
	$iFormuleId = $_SESSION['cart']['formule']['formule_id'];
	$strFormuleIdYonix = mysql_real_escape_string($_SESSION['cart']['formule']['formule_id_yonix']);
	$strFormuleClasse = $_SESSION['cart']['formule']['formule_classe'];
	if($strFormuleClasse == "ciblee"){
		$strClasse = $oDb->queryItem("SELECT classe_id_yonix FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
		$strMatiere = $oDb->queryItem("SELECT matiere_id_yonix FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['matiere'])."'");
		$strDe = $oDb->queryItem("SELECT de_id FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
		
		//récupération du produit 
		$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."' AND classe_id_yonix = '".$strClasse."' AND matiere_id_yonix = '".$strMatiere."' LIMIT 1"; 
		$aProduit = $oDb->queryRow($strSql);
		//var_dump($oDb->rows);exit;
		if($oDb->rows > 0 ){
					$_SESSION["products"]["enfants"][0]["ean"] = $aProduit['produit_ean'];
					$_SESSION["products"]["enfants"][0]["duree_engagement"] = $strDe ; 
					$_SESSION["products"]["enfants"][0]["ean_reussite"] = $aProduit['produit_ean'];
					$strStep = "step_1_3";
					$bStep1 = true; 
					$bStep2 = true;			
		}else{
			$strStep = "step_1_2"; 
			$bStep1 = true; 
			$bStep2 = false; 
			$strErreurStep2 = "Une erreur est survenue lors de votre sélection. Veuillez réessayer ultérieurement."; 
		}
	}
}

//Formulaire 1 reussite
if(isset($_POST["action"]) && $_POST["action"] == "step_2_1"){
				$_SESSION['de'] = $_POST['de'];
    unset($_SESSION["CODEPROMO"]);
    unset($_SESSION["products"]["enfants"][0]['CODEPROMO']);
    unset($_SESSION["products"]["enfants"][0]['REDUCTION']);  
    
	unset($_SESSION["products"]["enfants"]);
	$_SESSION["products"]["enfants"][0]["classe"] = $_POST["classe"][0]; 
	$_SESSION["products"]["enfants"][0]["D_PRENOM"] = $_POST["prenom"][0] ; 
	//On se trouve dans une formule reussite on doit donc vérifier que les champs enfant, classe et matière sont bien selectionné
	$fetch = (bool) strlen(trim($_POST["prenom"][0]));
	if(!isset($_POST["prenom"][0]) || empty($_POST["prenom"][0]) || $fetch === false){
		$bError = true; 
		$strStep = "step_1_1"; 
		
		$strError .= "Veuillez saisir le prénom de votre enfant.";
		$strError .="</p>";
	}else{
		if(strpos($_POST["prenom"][0], " ") === 0){
			$_POST["prenom"][0] = substr($_POST["prenom"][0], 1);
		}
		//Initialisation du prénom de l'enfant
		
		if( !isset($_POST["classe"][0]) || empty($_POST["classe"][0])  ){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p>"; 
			$strError .= "Veuillez saisir la classe de votre enfant.";
		$strError .="</p>";
		}else{
			//Prénom, matière et classe remplie on vérifi les donnée
			$strSql = "SELECT * FROM bor_classe c  WHERE c.classe_id = '".mysql_real_escape_string($_POST["classe"][0])."'  LIMIT 1"; 
			$aInfosClasse = $oDb->queryRow($strSql); 
			if(!$oDb->rows){
				$bError = true; 
				$strStep = "step_1_1"; 
				$strError .= "<p>"; 
				$strError .= "Nous ne retrouvons pas la formule associée à votre sélection.";
		$strError .="</p>";
			}else{
				$_SESSION["products"]["enfants"][0]["classe"] = $_POST["classe"][0]; 
				$_SESSION["products"]["enfants"][0]["matiere"] = ''; 
				$strStep = "step_1_2"; 
				$bStep1 = true; 
			}
		}
	}
				$iDeIdYonix = mysql_real_escape_string($_POST['de']); 
				$strClasse = $oDb->queryItem("SELECT classe_id_yonix FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
				$strDe = $oDb->queryItem("SELECT de_id FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
				$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."' AND classe_id_yonix = '".$strClasse."'  LIMIT 1"; 
				$aProduit = $oDb->queryRow($strSql); 
		
		//var_dump($aProduit);
		if($oDb->rows > 0 ){
                       
			$_SESSION["products"]["enfants"][0]["ean"] = $aProduit['produit_ean'];
			$_SESSION["products"]["enfants"][0]["duree_engagement"] = $strDe ; 
			$_SESSION["products"]["enfants"][0]["ean_reussite"] = $aProduit['produit_ean'];
			$strStep = "step_1_3";
			$bStep1 = true; 
			$bStep2 = true;
				
		}else{
			$strStep = "step_1_2"; 
			$bStep1 = true; 
			$bStep2 = false; 
			$strErreurStep2 = "Une erreur est survenue lors de votre sélection. Veuillez réessayer ultérieurement."; 
		}
	
}

// Formulaire 1 tribu
 
if(isset($_POST["action"]) && $_POST["action"] == "step_3_1"){
	$_SESSION['de'] = $_POST['de'];
    unset($_SESSION["CODEPROMO"]);
    unset($_SESSION["products"]["enfants"][0]['CODEPROMO']);
    unset($_SESSION["products"]["enfants"][0]['REDUCTION']);  
				
	if(!isset($_SESSION['retirer'])){
					unset($_SESSION["products"]["enfants"]);
	}
	unset($_SESSION['retirer']);
	//unset($_SESSION["products"]["enfants"]);
	//On se trouve dans une formule reussite on doit donc vérifier que les champs enfant, classe et matière sont bien selectionné
	//$iNbEnfant = $_POST['nbenfant'];
	$iNbEnfant = $_SESSION['nb_enfant'];
		$iPrenom = count($_POST['prenom']);
		$iClasse = count($_POST['classe']);
		if(  $iNbEnfant!= $iPrenom  ){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p>"; 
			$strError .= "Veuillez saisir le prénom de vos enfants.";
		$strError .="</p>";
		}else if(  $iNbEnfant!= $iClasse  ){
			$bError = true; 
			$strStep = "step_1_1"; 
			$strError .= "<p>"; 
			$strError .= "Veuillez saisir la classe de vos enfants.";
		$strError .="</p>";
		}else{
			$bVerifEnfant = true;
			$bVerifClasse = true ;
			for($i = 0; $i < $iNbEnfant; $i++){
				$_SESSION["products"]["enfants"][$i]["classe"] = $_POST["classe"][$i];
				$_SESSION["products"]["enfants"][$i]["D_PRENOM"] = $_POST["prenom"][$i] ;
				
				$fetch = (bool) strlen(trim($_POST['prenom'][$i]));
				if(empty($_POST['prenom'][$i]) || $fetch === false)
					$bVerifEnfant = false ; 
				if( empty($_POST['classe'][$i]))
					$bVerifClasse = false ; 
			}
			if(!$bVerifEnfant || !$bVerifClasse){
				$bError = true; 
				$strStep = "step_1_1"; 
				if(!$bVerifEnfant ){
					$strError .= "<p>"; 
					$strError .= "Veuillez saisir le prénom de vos enfants.";
					$strError .="</p>";
				}if (!$bVerifClasse ){
					$strError .= "<p>"; 
					$strError .= "Veuillez saisir la classe de vos enfants.";
					$strError .="</p>";
				}
			}else{
				for($i = 0; $i < $iNbEnfant; $i++){
					if(strpos($_POST["prenom"][$i], " ") === 0){
						$_POST["prenom"][$i] = substr($_POST["prenom"][0], 1);
					}	
					$_SESSION["products"]["enfants"][$i]["D_PRENOM"] = $_POST["prenom"][$i] ; 
					$strSql = "SELECT * FROM bor_classe c  WHERE c.classe_id = '".mysql_real_escape_string($_POST["classe"][$i])."'  LIMIT 1"; 
					$aInfosClasse = $oDb->queryRow($strSql); 
					$_SESSION["products"]["enfants"][$i]["classe"] = $_POST["classe"][$i]; 
					$_SESSION["products"]["enfants"][$i]["matiere"] = ''; 
					$strStep = "step_1_2"; 
					$bStep1 = true;
				}
				
				$iDeIdYonix = mysql_real_escape_string($_POST['de']);
				$strFormuleIdYonix = mysql_real_escape_string($_SESSION['cart']['formule']['formule_id_yonix']);
				$strDe = $oDb->queryItem("SELECT de_id FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
				$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."'  LIMIT 1"; 
				$aProduit = $oDb->queryRow($strSql);
				
				//var_dump($aProduit);

				if($oDb->rows > 0 ){
					for($i= 0; $i<$_SESSION['nb_enfant'] ;$i++){
						$_SESSION["products"]["enfants"][$i]["ean"] = $aProduit['produit_ean'];
						$_SESSION["products"]["enfants"][$i]["duree_engagement"] = $strDe ; 
						//Récupération de l'ean du produit reussite concerné
						$strSql = "SELECT p.produit_ean FROM bor_produit p 
									INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix)
									INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
									WHERE c.classe_id = '".mysql_real_escape_string($_SESSION["products"]["enfants"][$i]['classe'])."'
									AND	f.formule_classe = 'reussite'
									AND p.de_id_yonix = '".$iDeIdYonix."'"; 
						$strEan = $oDb->queryItem($strSql); 
						$_SESSION["products"]["enfants"][$i]["ean_reussite"] = $strEan ; 
					}

					$bProduit = true; 
					foreach( $_SESSION["products"]["enfants"] as $aEnfant){
						$strSql = "SELECT *
									FROM bor_produit p
									INNER JOIN bor_duree_engagement d ON(d.de_id_yonix = p.de_id_yonix)
									INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
									INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix)
									WHERE c.classe_id = '".mysql_real_escape_string($aEnfant["classe"])."'
									AND f.formule_classe = 'reussite'
									AND d.de_id = '".mysql_real_escape_string($aEnfant["duree_engagement"])."'"; 
						$aProduitEnfant = $oDb->queryRow($strSql);

						if(!$oDb->rows){
							$bProduit = false ; 
						}
					}

					if($bProduit){
						$strStep = "step_1_3";
						$bStep1 = true; 
						$bStep2 = true;	
					}else{
						$strStep = "step_1_2"; 
						$bStep1 = true; 
						$bStep2 = false; 
						$strErreurStep2 = "Une erreur est survenue lors de votre sélection. Veuillez réessayer ultérieurement."; 
					}
				}else{
					$strStep = "step_1_2"; 
					$bStep1 = true; 
					$bStep2 = false; 
					$strErreurStep2 = "Une erreur est survenue lors de votre sélection. Veuillez réessayer ultérieurement."; 
				}
			}
		}
}

if(isset($strStep) && $strStep == "step_1_3" && $bError != true){
	

$strSql = "SELECT produit_prix FROM bor_produit WHERE produit_ean = '".$_SESSION["products"]["enfants"][0]["ean"]."'"; 
$fPrix = $oDb->queryItem($strSql) ;

	?>
	<script src="http://mastertag.effiliation.com/mt660015994.js?page=product&idp=<?php echo $_SESSION["products"]["enfants"][0]["ean"]; ?>&prix=<?php echo $fPrix; ?>" async="async">
	</script>
	<?php
	
				if(isset($_GET["formule"]) && $_GET["formule"] != ""){
								if($_CONST['TYPE_ENVIRONNEMENT'] !='dev' && $_CONST['TYPE_ENVIRONNEMENT'] !='preprod'){
						echo "<script>document.location.href='".$_CONST["HTTPS"].$_CONST['URL_ACCUEIL']."commande/bordas/etape-2-informations-personnelles-formule-".$_GET["formule"].".html';</script>";
					}else{
						echo "<script>document.location.href='".$_CONST["URL2"].$_CONST['URL_ACCUEIL']."commande/bordas/etape-2-informations-personnelles-formule-".$_GET["formule"].".html';</script>";
					}
								exit; 
				}else{
								echo "<script>document.location.href='/comment-s-abonner.html';</script>";
								exit; 
				}
			
}

//echo '<pre>';
//var_dump($_SESSION["products"]["enfants"][0]);
//var_dump($_POST);
//echo '</pre>';

// **** FORMULAIRE FORMULE CIBLÉEE

if($_GET['formule'] == "ciblee")
{
?>
<div class="bss-section bloc-section-gris bss-fiche-tunnel">
  <form action="" method="POST" class="form-horizontal" id="offre-cible">
				<input type="hidden" value="step_1_1" name="action"/>
    <div class="container">
      <div class="row">
        <div class="col-sm-3 col-md-3"> <img src="/img/icone-cible-300-violet.png" class="img-responsive"> </div>
        <div class="col-sm-9 col-md-6">
			<?php
			if(isset($strError) && $strError != ""){
			?>
			<div class="alert alert-danger" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Erreur !</strong><br>
			<?php echo $strError; ?> </div>
			<?php
			}
			?>	
          <h1 class="h1">Formule Ciblée</h1>
          <p class="h2">1 classe - 1 matière</p>
          <p class="h3">Je personnalise l'abonnement de mon enfant :</p>
          <div class="form-group">
            <label for="prenom" class="col-sm-6 control-label"><span class="obligatoire">*</span> Prénom de mon enfant</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" id="prenom" name="prenom[0]" placeholder="Prénom de mon enfant" value="<?php if(isset($_SESSION["products"]["enfants"][0]["D_PRENOM"]) && $_SESSION["products"]["enfants"][0]["D_PRENOM"] != "") echo $_SESSION["products"]["enfants"][0]["D_PRENOM"]; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="classe[0]" class="col-sm-6 control-label"><span class="obligatoire">*</span> Je choisis une classe</label>
            <div class="col-sm-6">
              <select name="classe[0]" class="form-control classe" id="classe" form="offre-cible" title="tribu-classe">
						<option value="0">Choisir une classe</option>'; 
				<?php		if($ListClasses){ 
							foreach($ListClasses as $classe){
								echo '<option value="'.$classe['classe_id'].'" '.(( isset($_SESSION["products"]["enfants"][0]["classe"]) && $_SESSION["products"]["enfants"][0]["classe"] == $classe['classe_id']) ? 'selected' : '' ) .'>'.$classe['classe_name'].'</option>'; 
							}  
						}
				?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="matiere[0]" class="col-sm-6 control-label"><span class="obligatoire">*</span> Je choisis une matière</label>
            <div class="col-sm-6">
              <select name="matiere[0]" class="form-control" id="matiere" form="offre-cible" title="tribu-matiere">
				<?php if(isset($_SESSION["products"]["enfants"][0]["matiere"])){
						$iNomMatiere = $oDb->queryRow("SELECT matiere_titre FROM bor_matiere
						WHERE matiere_id = ".$_SESSION["products"]["enfants"][0]["matiere"]." 
						");
						echo '<option value='.$_SESSION["products"]["enfants"][0]["matiere"].'>'.$iNomMatiere['matiere_titre'].'</option>';
					}
				?>
              </select>
            </div>
          </div>
           <hr>
		    <p class="text-center">
            <button type="submit" onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 1', 'F.Ciblée - Continuer');" class="btn  btn-primary btn-fw">Continuer <i class="icon-angle-circled-right"></i></button>
          </p>
            <p class="legende"><span class="obligatoire">* <strong>champ obligatoire</strong></span></p>
        </div>
        <div class="col-sm-9 col-sm-offset-3   col-md-3 col-md-offset-0">
          <div class="bloc-bss-produit bloc-bss-gris">
		<?php
		$strSql = "SELECT * FROM bor_duree_engagement ORDER BY de_position DESC"; 
		$aDureeEngagement = $oDb->queryTab($strSql);
		$iCount = count($aDureeEngagement);
		for($i=0; $i< $iCount; $i++){
				$strSql = "SELECT produit_prix FROM bor_produit WHERE formule_id_yonix = '".mysql_real_escape_string($strFormuleIdYonix)."' AND de_id_yonix = '".mysql_real_escape_string( $aDureeEngagement[$i]['de_id_yonix'])."' LIMIT 1"; 
				$fPrix = $oDb->queryItem($strSql) ;
				$fPrixMois = round( $fPrix / $aDureeEngagement[$i]['de_engagement'] , 2 ) ;
				if($i == 0){
				?>
				<div class="bloc-bss-produit-titre">Durée recommandée :</div>
				<div class="bloc-bss-produit-stitre">Prix pour une période de <span id='duree_libelle'></span></div>
				<div class="bloc-bss-produit-prix"><div id="prix_choisi" style="display:inline;">
								<?php
								//echo $fPrixMois;
								?>
												</sup></div><sup>€/mois TTC</sup></div>
				<div class="bloc-bss-duree-titre">Choisir un abonnement</div>
				<div class="bloc-bss-selection-duree">
				<div class="radio">
								<label>
												<input <?php if(isset($_SESSION['de']) && $_SESSION['de'] == $aDureeEngagement[$i]['de_id_yonix']) echo 'checked'; ?> name="de" type="radio" class="aDe" form="offre-cible" value="<?php echo $aDureeEngagement[$i]['de_id_yonix']; ?>" checked>
												<div class="option">annuel - <?php echo $fPrixMois; ?>€/mois<span style="display:none;" id='de3'>*</span></div>
												<div class="legende">(1 seul paiement de <?php echo $fPrix; ?>€)</div> 
								</label>
				</div>
				<?php
				}else if($i == 1){
				?>
				<div class="radio">
								<label> <input <?php if(isset($_SESSION['de']) && $_SESSION['de'] == $aDureeEngagement[$i]['de_id_yonix']) echo 'checked'; ?> name="de" type="radio" class="aDe" value="<?php echo $aDureeEngagement[$i]['de_id_yonix']; ?>" form="offre-cible" value="option1">
								<div class="option">trimestriel - <?php echo $fPrixMois; ?>€/mois<span style="display:none;" id='de2'>*</span></div>
								<div class="legende">(paiement trimestriel de <?php echo $fPrix; ?>€) </div> </label>
				</div>
				<?php
				}else{
				?>
				<div class="radio">
                <label> <input <?php if(isset($_SESSION['de']) && $_SESSION['de'] == $aDureeEngagement[$i]['de_id_yonix']) echo 'checked'; ?> name="de" type="radio" class="aDe" value="<?php echo $aDureeEngagement[$i]['de_id_yonix']; ?>" form="offre-cible" value="option1">
											<div class="option">mensuel - <?php echo $fPrixMois; ?>€/mois<span style="display:none;" id='de1'>*</span></div> <div class="legende">(paiement mensuel de <?php echo $fPrixMois; ?>€) </div></label>
							</div>
							<?php
							}
			}
?>
            </div>
          </div>
			<p id="legende" class="legende"></p>
          
        </div>
      </div>
    </div>
  </form>
</div>
<?php

// **** FORMULAIRE FORMULE RÉUSSITE

}else if($_GET['formule'] == "reussite"){
?>
				<div class="bss-section bloc-section-gris bss-fiche-tunnel">
  <form action="" method="POST" class="form-horizontal" id="offre-reussite" title="offre-reussite">
			<input type="hidden" value="step_2_1" name="action"/>
    <div class="container">
      <div class="row">
        <div class="col-sm-3 col-md-3"> <img src="/img/icone-reussite-300-verteau.png" class="img-responsive"> </div>
        <div class="col-sm-9 col-md-6">
			<?php
			if(isset($strError) && $strError != ""){
			?>
			<div class="alert alert-danger" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Erreur !</strong><br>
			<?php echo $strError; ?> </div>
			<?php
			}
			?>	
          <h1 class="h1">Formule Réussite</h1>
          <p class="h2">1 classe - Multi-matières</p>
          <p class="h3">Je personnalise l'abonnement de mon enfant :</p>
           <div class="form-group">
            <label for="prenom" class="col-sm-6 control-label"><span class="obligatoire">*</span> Prénom de mon enfant</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" id="prenom" name="prenom[0]" placeholder="Prénom de mon enfant" value="<?php if(isset($_SESSION["products"]["enfants"][0]["D_PRENOM"]) && $_SESSION["products"]["enfants"][0]["D_PRENOM"] != "") echo $_SESSION["products"]["enfants"][0]["D_PRENOM"]; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="classe" class="col-sm-6 control-label"><span class="obligatoire">*</span> Je choisis une classe</label>
            <div class="col-sm-6">
              <select name="classe[0]" class="form-control classe1" id="reussite-classe" form="offre-reussite" title="reussite-classe">
				<?php		if($ListClasses){ 
							foreach($ListClasses as $classe){
								echo '<option value="'.$classe['classe_id'].'" '.(( isset($_SESSION["products"]["enfants"][0]["classe"]) && $_SESSION["products"]["enfants"][0]["classe"] == $classe['classe_id']) ? 'selected' : '' ) .'>'.$classe['classe_name'].'</option>'; 
							}  
						}
				?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="reussite-matiere" class="col-sm-6 control-label">Matières disponibles</label>
            <div class="col-sm-6">
              <ul class="liste-matiere" id="liste-matiere">
                <li>Français</li>
                <li>Maths</li>
              </ul>
            </div>
          </div>
          <p class="text-center">
            <button type="submit" onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 1', 'F. Réussite - Continuer');" class="btn  btn-primary btn-fw">Continuer <i class="icon-angle-circled-right"></i></button>
          </p>
          <p class="legende"><span class="obligatoire">* <strong>champ obligatoire</strong></span></p>
          <p class="text-center"> 
            <!--<button type="button" class="btn  btn-default "><i class="icon-plus-circled"></i> Ajouter un enfant</button>
            <button type="submit" class="btn  btn-primary">Continuer <i class="icon-angle-circled-right"></i></button>--></p>
        </div>
        <div class="col-sm-9 col-sm-offset-3   col-md-3 col-md-offset-0">
          <div class="bloc-bss-produit bloc-bss-gris">
			<?php
			$strSql = "SELECT * FROM bor_duree_engagement ORDER BY de_position DESC"; 
			$aDureeEngagement = $oDb->queryTab($strSql);
			$iCount = count($aDureeEngagement);
			for($i=0; $i< $iCount; $i++){
				$strSql = "SELECT produit_prix FROM bor_produit WHERE formule_id_yonix = '".mysql_real_escape_string($strFormuleIdYonix)."' AND de_id_yonix = '".mysql_real_escape_string( $aDureeEngagement[$i]['de_id_yonix'])."' LIMIT 1"; 
				$fPrix = $oDb->queryItem($strSql) ;
				$fPrixMois = round( $fPrix / $aDureeEngagement[$i]['de_engagement'] , 2 ) ;
				if($i == 0){
				?>
				<div class="bloc-bss-produit-titre">Durée recommandée :</div>
				<div class="bloc-bss-produit-stitre">Prix pour une période de <span id='duree_libelle'></span></div>
				<div class="bloc-bss-produit-prix"><div id="prix_choisi" style="display:inline;">
				<?php
				//echo $fPrixMois;
				?>
								</sup></div><sup>€/mois TTC</sup></div>
				<div class="bloc-bss-duree-titre">Choisir un abonnement</div>
				<div class="bloc-bss-selection-duree">
				<div class="radio">
				<label>
					<input <?php if(isset($_SESSION['de']) && $_SESSION['de'] == $aDureeEngagement[$i]['de_id_yonix']) echo 'checked'; ?> name="de" type="radio" class="aDe" form="offre-reussite" value="<?php echo $aDureeEngagement[$i]['de_id_yonix']; ?>" checked>
					<div class="option">annuel - <?php echo $fPrixMois; ?>€/mois<span style="display:none;" id='de3'>*</span></div>
					<div class="legende">(1 seul paiement de <?php echo $fPrix; ?>€)</div> 
				</label>
				</div>
				<?php
				}else if($i == 1){
				?>
				<div class="radio">
					<label> <input <?php if(isset($_SESSION['de']) && $_SESSION['de'] == $aDureeEngagement[$i]['de_id_yonix']) echo 'checked'; ?> name="de" type="radio" class="aDe" value="<?php echo $aDureeEngagement[$i]['de_id_yonix']; ?>" form="offre-reussite" value="option1">
					<div class="option">trimestriel - <?php echo $fPrixMois; ?>€/mois<span style="display:none;" id='de2'>*</span></div>
					<div class="legende">(paiement trimestriel de <?php echo $fPrix; ?>€) </div> </label>
				</div>
				<?php
				}else{
				?>
				<div class="radio">
                <label> <input <?php if(isset($_SESSION['de']) && $_SESSION['de'] == $aDureeEngagement[$i]['de_id_yonix']) echo 'checked'; ?> name="de" type="radio" class="aDe" value="<?php echo $aDureeEngagement[$i]['de_id_yonix']; ?>" form="offre-reussite" value="option1">
				<div class="option">mensuel - <?php echo $fPrixMois; ?>€/mois<span style="display:none;" id='de1'>*</span></div> </label>
				<div class="legende">(paiement mensuel de <?php echo $fPrixMois; ?>€) </div></div>
				<?php
				}
			}
?>
            </div>
          </div>
			<p id="legende" class="legende"></p>
         
        </div>
      </div>
    </div>
  </form>
</div>
<?php

// **** FORMULAIRE FORMULE TRIBU

}else if($_GET['formule'] == "tribu"){
	if(!isset($_SESSION['nb_enfant']))
		$_SESSION['nb_enfant'] = 2;
	else{
		if($_SESSION['nb_enfant'] > 5)
			$_SESSION['nb_enfant'] = 5;
	}
								
							
?>
<div class="bss-section bloc-section-gris bss-fiche-tunnel">
  <form action="" method="POST" class="form-horizontal" id="offre-tribu" title="offre-tribu">
			<input type="hidden" value="step_3_1" name="action"/>
    <div class="container">
      <div class="row">
									<div class="col-sm-3 col-md-3"> <img src="/img/icone-tribu-300-vert.png" class="img-responsive"> </div>
        <div class="col-sm-9 col-md-6">
		<?php
			if(isset($strError) && $strError != ""){
			?>
			<div class="alert alert-danger" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Erreur !</strong><br>
			<?php echo $strError; ?> </div>
			<?php
			}
			?>	
		<h1 class="h1">Formule Tribu</h1>
          <p class="h2">2 à 5 classes - Multi-matières</p>
          <p class="h3">Je personnalise l'abonnement de mes enfants :</p>
										
										
<?php
// en fonction du nombre d'enfants
		for($i=0;$i<$_SESSION['nb_enfant'];$i++){
						//echo '<h1>'.$i.'</h1>';
						?>
				<div class="form-group">
            <label for="prenom" class="col-sm-6 control-label"><span class="obligatoire">*</span> Prénom de mon enfant</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" id="prenom" name="prenom[<?php echo $i; ?>]" placeholder="Prénom de mon enfant" value="<?php if(isset($_SESSION["products"]["enfants"][$i]["D_PRENOM"]) && $_SESSION["products"]["enfants"][$i]["D_PRENOM"] != "") echo $_SESSION["products"]["enfants"][$i]["D_PRENOM"]; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="classe" class="col-sm-6 control-label"><span class="obligatoire">*</span> Je choisis une classe</label>
            <div class="col-sm-6">
              <select name="classe[<?php echo $i; ?>]" class="form-control classe1" id="reussite-classe-<?php echo $i;?>" >
               	<?php		if($ListClasses){ 
							foreach($ListClasses as $classe){
								echo '<option value="'.$classe['classe_id'].'" '.(( isset($_SESSION["products"]["enfants"][$i]["classe"]) && $_SESSION["products"]["enfants"][$i]["classe"] == $classe['classe_id']) ? 'selected' : '' ) .'>'.$classe['classe_name'].'</option>'; 
							}  
						}
				?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="reussite-matiere" class="col-sm-6 control-label">Matières disponibles</label>
            <div class="col-sm-6">
              <ul class="liste-matiere" id="liste-matiere-reussite-classe-<?php echo $i;?>">
                <li>Français</li>
                <li>Maths</li>
              </ul>
            </div>
          </div>
		  <hr>
						<?php
				}
		?>
          <p class="text-center">
			<?php
			if($_SESSION['nb_enfant'] > 2){
				echo '  <button type="button" id="retirer_enfant" class="btn  btn-default "><i class="icon-minus-circled"></i> Retirer un enfant</button>  ';
			}
			if($_SESSION['nb_enfant'] < 5){
				?>
				<button type="button" onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 1', 'F.Tribu - Ajouter un enfant');" id="ajouter_enfant" class="btn  btn-default "><i class="icon-plus-circled"></i> Ajouter un enfant</button>
				<?php
			}
			?>
          </p> 
		   <p class="text-center">
            <button type="submit" onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 1', 'F.Tribu - Continuer');" class="btn  btn-primary btn-fw">Continuer <i class="icon-angle-circled-right"></i></button>
          </p>
		  <p class="legende"><span class="obligatoire">* <strong>champ obligatoire</strong></span></p>
        </div>
        <div class="col-sm-9 col-sm-offset-3   col-md-3 col-md-offset-0">
          <div class="bloc-bss-produit bloc-bss-gris">
																		<?php
												$strSql = "SELECT * FROM bor_duree_engagement ORDER BY de_position DESC"; 
												$aDureeEngagement = $oDb->queryTab($strSql);
												$iCount = count($aDureeEngagement);
												for($i=0; $i< $iCount; $i++){
																$strSql = "SELECT produit_prix FROM bor_produit WHERE formule_id_yonix = '".mysql_real_escape_string($strFormuleIdYonix)."' AND de_id_yonix = '".mysql_real_escape_string( $aDureeEngagement[$i]['de_id_yonix'])."' LIMIT 1"; 
																$fPrix = $oDb->queryItem($strSql) ;
																$fPrixMois = round( $fPrix / $aDureeEngagement[$i]['de_engagement'] , 2 ) ;
																if($i == 0){
																?>
																<div class="bloc-bss-produit-titre">Durée recommandée :</div>
																<div class="bloc-bss-produit-stitre">Prix pour une période de <span id='duree_libelle'></span></div>
																<div class="bloc-bss-produit-prix"><div id="prix_choisi" style="display:inline;">
																				<?php
																				//echo $fPrixMois;
																				?>
																								</sup></div><sup>€/mois TTC</sup></div>
																<div class="bloc-bss-duree-titre">Choisir un abonnement</div>
																<div class="bloc-bss-selection-duree">
																<div class="radio">
																				<label>
																								<input <?php if(isset($_SESSION['de']) && $_SESSION['de'] == $aDureeEngagement[$i]['de_id_yonix']) echo 'checked'; ?> name="de" type="radio" class="aDe" form="offre-tribu" value="<?php echo $aDureeEngagement[$i]['de_id_yonix']; ?>" checked>
																								<div class="option">annuel - <?php echo $fPrixMois; ?>€/mois<span style="display:none;" id='de3'>*</span></div>
																								<div class="legende">(1 seul paiement de <?php echo $fPrix; ?>€)</div> 
																				</label>
																</div>
																<?php
																}else if($i == 1){
																?>
																<div class="radio">
																				<label> <input <?php if(isset($_SESSION['de']) && $_SESSION['de'] == $aDureeEngagement[$i]['de_id_yonix']) echo 'checked'; ?> name="de" type="radio" class="aDe" value="<?php echo $aDureeEngagement[$i]['de_id_yonix']; ?>" form="offre-tribu" value="option1">
																				<div class="option">trimestriel - <?php echo $fPrixMois; ?>€/mois<span style="display:none;" id='de2'>*</span></div>
																				<div class="legende">(paiement trimestriel de <?php echo $fPrix; ?>€) </div> </label>
																</div>
																<?php
																}else{
																?>
																<div class="radio">
                <label> <input <?php if(isset($_SESSION['de']) && $_SESSION['de'] == $aDureeEngagement[$i]['de_id_yonix']) echo 'checked'; ?> name="de" type="radio" class="aDe" value="<?php echo $aDureeEngagement[$i]['de_id_yonix']; ?>" form="offre-tribu" value="option1">
																				<div class="option">mensuel - <?php echo $fPrixMois; ?>€/mois<span style="display:none;" id='de1'>*</span></div><div class="legende">(paiement mensuel de <?php echo $fPrixMois; ?>€) </div> </label>
																</div>
																<?php
																}
												}
?>
            </div>
          </div>
												<p id="legende" class="legende"></p>
        
		
        </div>
      </div>
    </div>
  </form>
</div>

<?php
}
?>
</div>

<?php
// script spécifique formule tribu avec ajout d'enfants
if($_GET['formule'] == "tribu"){
?>
<script type="text/javascript">
	jQuery(document).ready(function(){
					jQuery('#ajouter_enfant').click(function() {
								// Nombre d'enfants en session
								nb_enfants = <?php echo $_SESSION['nb_enfant']; ?>;
								var de = $('input[name=de]:checked', '#offre-tribu').val();
								//alert(de);
								var prenoms = new Array();
								var classes = new Array();
								// Pour chaque champ enfant on récupère le prénom et la classe
								for(i=0; i< nb_enfants; i++){
												//alert($("input[name='prenom\\["+i+"\\]']").val());
												prenoms.push($("input[name='prenom\\["+i+"\\]']").val());
												classes.push($("select[name='classe\\["+i+"\\]']").val());
								}
								jQuery.ajax({type:"POST", data:{nb_enfants: nb_enfants, prenoms: prenoms, classes : classes, de : de} , url: "/templates/ajaxNbEnfants.php" ,dataType: 'json',	success: function(json) {
												location.reload(true);
								}});
				});
				
				jQuery('#retirer_enfant').click(function() {
								nb_enfants = <?php echo $_SESSION['nb_enfant']; ?>;
									var de = $('input[name=de]:checked', '#offre-tribu').val();
								//alert(de);
								var prenoms = new Array();
								var classes = new Array();
								// Pour chaque champ enfant on récupère le prénom et la classe
								for(i=0; i< nb_enfants; i++){
												//alert($("input[name='prenom\\["+i+"\\]']").val());
												prenoms.push($("input[name='prenom\\["+i+"\\]']").val());
												classes.push($("select[name='classe\\["+i+"\\]']").val());
								}
								jQuery.ajax({type:"POST", data:{nb_enfants: nb_enfants, retirer : 'retirer',prenoms: prenoms, classes : classes, de : de} , url: "/templates/ajaxNbEnfants.php" ,dataType: 'json',	success: function(json) {
												//alert(nb_enfants);
												location.reload(true);
								}});
				});
				
				function update_matiere_reussite(id) {

								if(id != undefined){
								var id_brut = id.replace( /^\D+/g, '');

				}
				//alert(jQuery('#'+id).val());
    	jQuery.ajax({type:"POST", data:{ classe : jQuery('#'+id).val()}, url: "/templates/ajaxsearchTribu.php" ,dataType: 'json',	success: function(json) {
													
				jQuery('#liste-matiere-'+id+' li').remove(); 
				jQuery.each(json, function(index, value) { 
				jQuery('#liste-matiere-'+id).append('<li>'+ json[index]['mc_titre'] +'</li>');
					});
				},
				error: function(){
								//alert('erreur1');
					jQuery('#liste-matiere-'+id).html('');
				}
			});
			 return false;
}

$('.classe1').each(function() {
    //alert( this.id );
				update_matiere_reussite(this.id);
});
 
		jQuery('.classe1').change(function () {
			//alert(this.id);
		 update_matiere_reussite(this.id);
		});	
					// **** formule ciblée : update select matiere
					
						function update_matiere() {
   jQuery.ajax({type:"POST", data: jQuery('.classe').serialize(), url: "/templates/ajaxsearch.php" ,dataType: 'json',	success: function(json) {
			//jQuery('#matiere option').remove(); 
				//	jQuery('#matiere').append('<option value="">Choisir une matière</option>');
					jQuery.each(json, function(index, value) { 
						jQuery('#matiere').append('<option value="'+ json[index]['matiere_id'] +'">'+ json[index]['mc_titre'] +'</option>');
					});
				},
				error: function(){
					jQuery('#matiere').html('');
				}
			});
			 return false;
}
				// au chargement, si classe renseignée
				update_matiere();
		jQuery('.classe').each(function(){
						//alert(jQuery('#matiere').val());
			jQuery.ajax({type:"POST", data:{classe: jQuery(this).serialize(), matiere: jQuery('#matiere').val()}, url: "/templates/ajax/bloc_matiere.php" ,dataType: 'script'	});
		});
		
		// au changement de classe: changement des matières
		jQuery('.classe').change(function () {
						jQuery('#matiere option').remove();
						jQuery('#matiere').append('<option value="">Choisir une matière</option>');
				update_matiere();
		});
		
		
			<?php
		//$cgvCiblee = 
		//$cgvReussite = 
		$onClick = 'onclick="';
		$cgvTribu = $onClick."ga('send', 'event', 'Tunnel Standard', 'Etape 1', 'F.Tribu - Lien cgv');".'"';
		?>
		
		// ****** update prix fonction de durée
		function update_periode() {
				var duree = jQuery( "input:radio[name=de]:checked" ).val();
				var formule = <?php echo $_SESSION['cart']["formule"]["formule_id_yonix"];?>;
				if(duree ==  62725 ){
								jQuery('#de1').hide();
								jQuery('#de2').hide();
								jQuery('#de3').show();
								jQuery('#duree_libelle').html(' 1 an');
								jQuery('#legende').html('* Cet abonnement prend fin après la durée d\'1 an. Il n\'est pas reconduit tacitement. Pour plus d’informations, voir <a target="_blank" href="/conditions-generales-de-vente.html">les conditions générales de vente.</a>');
				}else if(duree ==  62727 ){
								jQuery('#de1').hide();
								jQuery('#de2').show();
								jQuery('#de3').hide();
								jQuery('#duree_libelle').html(' 3 mois');
								jQuery('#legende').html('* Cet abonnement est reconduit tacitement par période de 3 mois. La reconduction peut être résiliée à tout moment (jusqu\'à 5 jours avant la fin de chaque période) et sans justificatif, en vous connectant sur ce site et en vous rendant sur la page « Gérer mon abonnement ». Pour plus d’informations, voir <a target="_blank" class="cgv_link" href="/conditions-generales-de-vente.html">les conditions générales de vente</a>.');
				}else if(duree ==  62726 ){
								jQuery('#de1').show();
								jQuery('#de2').hide();
								jQuery('#de3').hide();
								jQuery('#duree_libelle').html(' 1 mois');
								jQuery('#legende').html('* Cet abonnement est reconduit tacitement chaque mois. La reconduction peut être résiliée à tout moment (jusqu\'à 5 jours avant la fin de chaque période) et sans justificatif, en vous connectant sur ce site et en vous rendant sur la page « Gérer mon abonnement ». Pour plus d’informations, voir <a target="_blank" class="cgv_link" href="/conditions-generales-de-vente.html">les conditions générales de vente</a>.');
				}
			jQuery.ajax({type:"POST", data:{duree : duree, formule : formule}, url: "/templates/ajaxUpdateDe.php" ,dataType: 'json',	success: function(json) {
				//jQuery('#matiere option').remove(); 
				//	jQuery('#matiere').append('<option value="">Choisir une matière</option>');
				//	jQuery.each(json, function(index, value) { 
			//	alert(json[1]);
						jQuery('#prix_choisi').html(json[1]);
				//	});
				},
				error: function(){
								alert('erreur')
				//	jQuery('#matiere').html('');
				}
			});
			 return false;
}
				// au chargement
				update_periode();
		
		// au changement de duree
jQuery('.aDe').change(function () {
				jQuery('.bloc-bss-produit-titre').hide();
				update_periode();	
		});
		
});
</script>
<?php
}else{
?>

<script type="text/javascript">
			
	jQuery(document).ready(function(){
					
					
		// ****** formules tribu, reussite : update liste matiere
		function update_matiere_reussite() {
    	jQuery.ajax({type:"POST", data: jQuery('.classe1').serialize(), url: "/templates/ajaxsearch.php" ,dataType: 'json',	success: function(json) {
				jQuery('#liste-matiere li').remove(); 
				jQuery.each(json, function(index, value) { 
				jQuery('#liste-matiere').append('<li>'+ json[index]['mc_titre'] +'</li>');
					});
				},
				error: function(){
					jQuery('#liste-matiere').html('');
				}
			});
			 return false;
}
 update_matiere_reussite();
		jQuery('.classe1').change(function () {
			
		 update_matiere_reussite();
		});	
					// **** formule ciblée : update select matiere
					
						function update_matiere() {
   jQuery.ajax({type:"POST", data: jQuery('.classe').serialize(), url: "/templates/ajaxsearch.php" ,dataType: 'json',	success: function(json) {
			//jQuery('#matiere option').remove(); 
				//	jQuery('#matiere').append('<option value="">Choisir une matière</option>');
					jQuery.each(json, function(index, value) { 
						jQuery('#matiere').append('<option value="'+ json[index]['matiere_id'] +'">'+ json[index]['mc_titre'] +'</option>');
					});
				},
				error: function(){
					jQuery('#matiere').html('');
				}
			});
			 return false;
}
				// au chargement, si classe renseignée
				update_matiere();
		jQuery('.classe').each(function(){
						//alert(jQuery('#matiere').val());
			jQuery.ajax({type:"POST", data:{classe: jQuery(this).serialize(), matiere: jQuery('#matiere').val()}, url: "/templates/ajax/bloc_matiere.php" ,dataType: 'script'	});
		});
		
		// au changement de classe: changement des matières
		jQuery('.classe').change(function () {
						jQuery('#matiere option').remove();
						jQuery('#matiere').append('<option value="">Choisir une matière</option>');
				update_matiere();
		});	
		
		
		// **** formule ciblée : update prix période
function update_periode() {
				var duree = jQuery( "input:radio[name=de]:checked" ).val();
				var formule = <?php echo $_SESSION['cart']["formule"]["formule_id_yonix"];?>;
				if(duree ==  62725 ){
								jQuery('#de1').hide();
								jQuery('#de2').hide();
								jQuery('#de3').show();
								jQuery('#duree_libelle').html(' 1 an');
								jQuery('#legende').html('* Cet abonnement prend fin après la durée d\'1 an. Il n\'est pas reconduit tacitement. Pour plus d’informations, voir <a target="_blank" class="cgv_link" href="/conditions-generales-de-vente.html">les conditions générales de vente.</a>');
				}else if(duree ==  62727 ){
								jQuery('#de1').hide();
								jQuery('#de2').show();
								jQuery('#de3').hide();
								jQuery('#duree_libelle').html(' 3 mois');
								jQuery('#legende').html('* Cet abonnement est reconduit tacitement par période de 3 mois. La reconduction peut être résiliée à tout moment (jusqu\'à 5 jours avant la fin de chaque période) et sans justificatif, en vous connectant sur ce site et en vous rendant sur la page « Gérer mon abonnement ». Pour plus d’informations, voir <a target="_blank" class="cgv_link" href="/conditions-generales-de-vente.html">les conditions générales de vente</a>.');
				}else if(duree ==  62726 ){
								jQuery('#de1').show();
								jQuery('#de2').hide();
								jQuery('#de3').hide();
								jQuery('#duree_libelle').html(' 1 mois');
								jQuery('#legende').html('* Cet abonnement est reconduit tacitement chaque mois. La reconduction peut être résiliée à tout moment (jusqu\'à 5 jours avant la fin de chaque période) et sans justificatif, en vous connectant sur ce site et en vous rendant sur la page « Gérer mon abonnement ». Pour plus d’informations, voir <a target="_blank" class="cgv_link" href="/conditions-generales-de-vente.html">les conditions générales de vente</a>.');
				}
				
				jQuery('.cgv_link').click(function() {
					var formule = " <?php echo $_GET['formule'];?> ";
					if(formule == " ciblee ")
						var formule_ga = "F.Ciblée";
					else if(formule == " reussite ")
						var formule_ga = "F.Réussite";
					else if(formule == " tribu ")
						var formule_ga = "F.Tribu";
					ga('send', 'event', 'Tunnel Standard', 'Etape 1', formule_ga+' - Lien cgv');
				});
			jQuery.ajax({type:"POST", data:{duree : duree, formule : formule}, url: "/templates/ajaxUpdateDe.php" ,dataType: 'json',	success: function(json) {
				//jQuery('#matiere option').remove(); 
				//	jQuery('#matiere').append('<option value="">Choisir une matière</option>');
				//	jQuery.each(json, function(index, value) { 
			//	alert(json[1]);
						jQuery('#prix_choisi').html(json[1]);
				//	});
				},
				error: function(){
								alert('erreur')
				//	jQuery('#matiere').html('');
				}
			});
			 return false;
}
				// au chargement
				update_periode();
		
		// au changement de duree
jQuery('.aDe').change(function () {
				jQuery('.bloc-bss-produit-titre').hide();
				update_periode();	
		});
});
	</script>
	
<?php
}