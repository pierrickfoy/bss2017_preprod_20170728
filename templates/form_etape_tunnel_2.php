<link href="/assets/bordas-2015/global_abonnement.css" rel="stylesheet" type="text/css">

<?php
$strFormuleIdYonix = $_SESSION['cart']['formule']['formule_id_yonix'];
if($strFormuleIdYonix != 62724)
	$_SESSION['nb_enfant'] = 1;
$_SESSION['compteur_erreur_cmd'] = "";
if( !isset($_SESSION['products']['enfants'][0]['duree_engagement'])){
	// si pas de durée récupérée en étape 1 (?) on applique durée 12 mois
	$_SESSION['products']['enfants'][0]['duree_engagement'] = 3;
}
if( isset($_SESSION['user']['IDCLIENT']) && $_SESSION['user']['IDCLIENT'] > 0){
	$strStep = "step_adresse";
}

if( isset($strStep) && $strStep == "step_adresse"){
	$iDe = $_SESSION['products']['enfants'][0]['duree_engagement'];
	$strSql = "SELECT bcf.* FROM bor_commande bc INNER JOIN bor_commande_facture bcf ON ( bcf.commande_id = bc.commande_id) WHERE client_id = ".$_SESSION['user']['IDCLIENT']." ORDER BY commande_id DESC";
	$aFacture = $oDb->queryTab($strSql);
	
	// on cherche si le client a une facture et une adresse valide, si oui : on passe directement à l'étape 3
	if(isset($aFacture[0]['pays_id']) && $aFacture[0]['pays_id'] != "" && isset($aFacture[0]['facture_adr1']) && $aFacture[0]['facture_adr1'] != "" && isset($aFacture[0]['facture_ville']) && $aFacture[0]['facture_ville'] != "" && isset($aFacture[0]['facture_cplivraison']) && $aFacture[0]['facture_cplivraison'] != "" ){
		$_SESSION['shipping']['ADR1'] = $aFacture[0]['facture_adr1'];
		$_SESSION['shipping']['ADR3'] = $aFacture[0]['facture_adr3'];
		$_SESSION['shipping']['CPLIVRAISON'] = $aFacture[0]['facture_cplivraison'];
		$_SESSION['shipping']['VILLELIVRAISON'] = $aFacture[0]['facture_ville'];
		$_SESSION['shipping']['pays_id'] = $aFacture[0]['pays_id'];
		
		// quelle formule
		$strFormuleIdYonix = $_SESSION['cart']['formule']['formule_id_yonix'];
		
		if($strFormuleIdYonix == 62724){ //formule tribu
			$iDeIdYonix = $oDb->queryItem("SELECT de_id_yonix FROM bor_duree_engagement WHERE de_id ='".$_SESSION["products"]["enfants"][0]["duree_engagement"]."'");
			$strDe = $oDb->queryItem("SELECT de_id FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
			$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."'  LIMIT 1"; 
			$aProduit = $oDb->queryRow($strSql); 
			if($oDb->rows > 0 ){
				for($i= 0; $i<$_SESSION['nb_enfant'] ;$i++){
					$_SESSION["products"]["enfants"][$i]["ean"] = $aProduit['produit_ean'];
					$_SESSION["products"]["enfants"][$i]["duree_engagement"] = $strDe ; 
					//Récupération de l'ean du produit reussite concerné
					$strSql = "SELECT p.produit_ean FROM bor_produit p 
								INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix)
								INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
								WHERE c.classe_id = '".mysql_real_escape_string($_SESSION["products"]["enfants"][$i]['classe'])."'
								AND	f.formule_classe = 'reussite'
								AND p.de_id_yonix = '".$iDeIdYonix."'"; 
					$strEan = $oDb->queryItem($strSql); 
					$_SESSION["products"]["enfants"][$i]["ean_reussite"] = $strEan ; 
				}

				$bProduit = true; 
				foreach( $_SESSION["products"]["enfants"] as $aEnfant){
					$strSql = "SELECT * FROM bor_produit p INNER JOIN bor_duree_engagement d ON(d.de_id_yonix = p.de_id_yonix) INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix) WHERE c.classe_id = '".mysql_real_escape_string($aEnfant["classe"])."' AND f.formule_classe = 'reussite' AND d.de_id = '".mysql_real_escape_string($aEnfant["duree_engagement"])."' "; 
					$aProduitEnfant = $oDb->queryRow($strSql); 
					if(!$oDb->rows){
						$bProduit = false ; 
					}
				}
			}
		}else if($strFormuleIdYonix == 62722){ //formule réussite	
			//on enregistre(écrase) les informations récupérées sur la page courante
			$iDeIdYonix = $oDb->queryItem("SELECT de_id_yonix FROM bor_duree_engagement WHERE de_id ='".$_SESSION["products"]["enfants"][0]["duree_engagement"]."'");
			$strClasse = $oDb->queryItem("SELECT classe_id_yonix FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
			$strFormuleIdYonix = $_SESSION['cart']['formule']['formule_id_yonix'];
			$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."' AND classe_id_yonix = '".$strClasse."'  LIMIT 1"; 
			$aProduit = $oDb->queryRow($strSql); 

			if($oDb->rows > 0 ){
					$_SESSION["products"]["enfants"][0]["ean"] = $aProduit['produit_ean']; 
					$_SESSION["products"]["enfants"][0]["ean_reussite"] = $aProduit['produit_ean'];
			}
		}else if($strFormuleIdYonix == 62723){ //formule ciblée
			//on enregistre(écrase) les informations récupérées sur la page courante
			$iDeIdYonix = $oDb->queryItem("SELECT de_id_yonix FROM bor_duree_engagement WHERE de_id ='".$_SESSION["products"]["enfants"][0]["duree_engagement"]."'");
			$strClasse = $oDb->queryItem("SELECT classe_id_yonix FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
			$strMatiere = $oDb->queryItem("SELECT matiere_id_yonix FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['matiere'])."'");
			$strFormuleIdYonix = $_SESSION['cart']['formule']['formule_id_yonix'];
			$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."' AND classe_id_yonix = '".$strClasse."' AND matiere_id_yonix = '".$strMatiere."' LIMIT 1"; 
			$aProduit = $oDb->queryRow($strSql); 

			if($oDb->rows > 0 ){
					$_SESSION["products"]["enfants"][0]["ean"] = $aProduit['produit_ean']; 
					$_SESSION["products"]["enfants"][0]["ean_reussite"] = $aProduit['produit_ean'];
			}
		}
		if($_CONST['TYPE_ENVIRONNEMENT'] !='dev' && $_CONST['TYPE_ENVIRONNEMENT'] !='preprod'){
			echo "<script>document.location.href='".$_CONST["HTTPS"].$_CONST['URL_ACCUEIL']."commande/bordas/etape-3-paiement-formule-".$_GET["formule"].".html';</script>";
		}else{
			echo "<script>document.location.href='".$_CONST["URL2"].$_CONST['URL_ACCUEIL']."commande/bordas/etape-3-paiement-formule-".$_GET["formule"].".html';</script>";
		}
		exit; 
	}
}

//Traitement formulaire adresse
if( isset($strStep) && $strStep == "step_adresse" && isset($_POST['validation_adresse']) && $_POST['validation_adresse'] == "valide" ){
	$strPays = $_POST['facturation_pays'];
	// traitement adresse étrangère
	/*if(!in_array($strPays, array( 1, 140, 144, 159, 106, 122, 109, 218, 227, 216))){
		if( (!isset($_POST['facturation_numero_voie_etranger']) || empty($_POST['facturation_numero_voie_etranger']))){
			$bVerif = false; 
			$bBlocFacturation = true;
			$strError .= "<p>"; 
			$strError .= "L'adresse n'est pas remplie.";
			$strError .="</p>"; 
		}
		if( !preg_match("/^[0-9A-Za-zàáâãäåçèéêëìíîïðòóôõöùúûüýÿÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÐÒÓÔÕÖÙÚÛÜÝŸ ]{1,45}$/", $_POST['facturation_numero_voie_etranger'])){
			$bVerif = false; 
			$strError .= "<p>"; 
			$strError .= "L'adresse n'est pas valide. Elle doit contenir 45 caractères alphanumériques sans ponctuation au maximum."; 
			$strError .="</p>";
		}
	// traitement adresse française
	}else{
		if( (!isset($_POST['facturation_numero_voie']) || empty($_POST['facturation_numero_voie']))){
			$bVerif = false; 
			$bBlocFacturation = true;
			$strError .= "<p>"; 
			$strError .= "Le numéro de voie n'est pas rempli.";
			$strError .="</p>"; 
		}
			if( !preg_match("/^[0-9A-Za-zàáâãäåçèéêëìíîïðòóôõöùúûüýÿÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÐÒÓÔÕÖÙÚÛÜÝŸ ]{1,10}$/", $_POST['facturation_numero_voie'])){
			$bVerif = false; 
			$strError .= "<p>"; 
			$strError .= "Le numéro de voie n'est pas valide. Il ne peut contenir que 10 caractères alphanumériques, sans ponctuation."; 
			$strError .="</p>";
		}
		if( (!isset($_POST['facturation_nom_voie']) || empty($_POST['facturation_nom_voie']))){
			$bVerif = false; 
			$bBlocFacturation = true;
			$strError .= "<p>"; 
			$strError .= "Le nom de voie n'est pas rempli.";
			$strError .="</p>"; 
		}
		if( !preg_match("/^[-0-9A-Za-zàáâãäåçèéêëìíîïðòóôõöùúûüýÿÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÐÒÓÔÕÖÙÚÛÜÝŸ' ]{1,23}$/", $_POST['facturation_nom_voie'])){
			$bVerif = false; 
			$strError .= "<p>"; 
			$strError .= "Le nom de voie n'est pas valide. Il doit contenir 22 caractères alphanumériques sans ponctuation au maximum."; 
			$strError .="</p>";
		}
	}

	if( (!isset($_POST['facturation_ville']) || empty($_POST['facturation_ville']))){
		$bVerif = false; 
		$bBlocFacturation = true;
		$strError .= "<p>"; 
		$strError .= "La ville de facturation n'est pas valide."; 
		$strError .="</p>";
	}*/
	if( (!isset($_POST['facturation_pays']) || empty($_POST['facturation_pays']))){
		$bVerif = false; 
		$bBlocFacturation = true;
		$strError .= "<p>"; 
		$strError .= "Le pays de facturation n'est pas valide."; 
		$strError .="</p>";
	}
	/*if( $_POST['facturation_pays'] == 1 && (!isset($_POST['facturation_cp']) || empty($_POST['facturation_cp']))){
		$bVerif = false; 
		$bBlocFacturation = true;
		$strError .= "<p>"; 
		$strError .= "Le code postal est obligatoire."; 
		$strError .="</p>";
	}*/
	
	// enregistre les nouvelles données de la page
	if($_POST['modification-duree'][0] != 0)
		$_SESSION['products']['enfants'][0]['duree_engagement'] = $_POST['modification-duree'][0];
	
	// quelle formule
		$strFormuleIdYonix = $_SESSION['cart']['formule']['formule_id_yonix'];
		
		if($strFormuleIdYonix == 62724){ //formule tribu
			$iDeIdYonix = $oDb->queryItem("SELECT de_id_yonix FROM bor_duree_engagement WHERE de_id ='".$_SESSION["products"]["enfants"][0]["duree_engagement"]."'");
			$strDe = $oDb->queryItem("SELECT de_id FROM bor_duree_engagement WHERE de_id_yonix ='".$iDeIdYonix."'");
			$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."'  LIMIT 1"; 
			$aProduit = $oDb->queryRow($strSql); 
			if($oDb->rows > 0 ){
				for($i= 0; $i<$_SESSION['nb_enfant'] ;$i++){
					$_SESSION["products"]["enfants"][$i]["ean"] = $aProduit['produit_ean'];
					$_SESSION["products"]["enfants"][$i]["duree_engagement"] = $strDe ; 
					//Récupération de l'ean du produit reussite concerné
					$strSql = "SELECT p.produit_ean FROM bor_produit p 
																	INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix)
																	INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
																	WHERE c.classe_id = '".mysql_real_escape_string($_SESSION["products"]["enfants"][$i]['classe'])."'
																	AND	f.formule_classe = 'reussite'
																	AND p.de_id_yonix = '".$iDeIdYonix."'"; 
					$strEan = $oDb->queryItem($strSql); 
					$_SESSION["products"]["enfants"][$i]["ean_reussite"] = $strEan ; 
				}

				$bProduit = true; 
				foreach( $_SESSION["products"]["enfants"] as $aEnfant){
					$strSql = "SELECT * FROM bor_produit p INNER JOIN bor_duree_engagement d ON(d.de_id_yonix = p.de_id_yonix) INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix) WHERE c.classe_id = '".mysql_real_escape_string($aEnfant["classe"])."' AND f.formule_classe = 'reussite' AND d.de_id = '".mysql_real_escape_string($aEnfant["duree_engagement"])."' "; 
					$aProduitEnfant = $oDb->queryRow($strSql); 
					if(!$oDb->rows){
						$bProduit = false ; 
					}
				}
			}
		}else if($strFormuleIdYonix == 62722){ //formule réussite
			//on enregistre(écrase) les informations récupérées sur la page courante
			
			$iDeIdYonix = $oDb->queryItem("SELECT de_id_yonix FROM bor_duree_engagement WHERE de_id ='".$_SESSION["products"]["enfants"][0]["duree_engagement"]."'");
			$strClasse = $oDb->queryItem("SELECT classe_id_yonix FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
			$strFormuleIdYonix = $_SESSION['cart']['formule']['formule_id_yonix'];
			$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."' AND classe_id_yonix = '".$strClasse."'  LIMIT 1"; 
			$aProduit = $oDb->queryRow($strSql); 

			if($oDb->rows > 0 ){
					$_SESSION["products"]["enfants"][0]["ean"] = $aProduit['produit_ean']; 
					$_SESSION["products"]["enfants"][0]["ean_reussite"] = $aProduit['produit_ean'];
			}
			
		}else if($strFormuleIdYonix == 62723){ //formule ciblée	
			//on enregistre(écrase) les informations récupérées sur la page courante
			$iDeIdYonix = $oDb->queryItem("SELECT de_id_yonix FROM bor_duree_engagement WHERE de_id ='".$_SESSION["products"]["enfants"][0]["duree_engagement"]."'");
			$strClasse = $oDb->queryItem("SELECT classe_id_yonix FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
			$strMatiere = $oDb->queryItem("SELECT matiere_id_yonix FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['matiere'])."'");
			$strFormuleIdYonix = $_SESSION['cart']['formule']['formule_id_yonix'];
			$strSql = "SELECT * FROM bor_produit WHERE de_id_yonix = '".$iDeIdYonix."' AND formule_id_yonix = '".$strFormuleIdYonix."' AND classe_id_yonix = '".$strClasse."' AND matiere_id_yonix = '".$strMatiere."' LIMIT 1"; 
			$aProduit = $oDb->queryRow($strSql); 

			if($oDb->rows > 0 ){
					$_SESSION["products"]["enfants"][0]["ean"] = $aProduit['produit_ean']; 
					$_SESSION["products"]["enfants"][0]["ean_reussite"] = $aProduit['produit_ean'];
			}
		}

	// tribu : enregistrement pour chaque enfant
	if($_SESSION['cart']['formule']['formule_id'] == 3){
		foreach($_POST['nom-enfant'] as $key=>$enfant){
			$_SESSION['products']['enfants'][$key]['D_PRENOM'] = $enfant;
		}
	}else{
		$_SESSION['products']['enfants'][0]['D_PRENOM'] = $_POST['nom-enfant'][0];
	}
}

// si l'adresse est validée, on passe à l'étape 3
if( isset($strStep) && $strStep == "step_adresse" && isset($_POST['validation_adresse']) && $_POST['validation_adresse'] == "valide" && !$strError ){
				if(!in_array($strPays, array( 1, 140, 144, 159, 106, 122, 109, 218, 227, 216))){
					$_SESSION['shipping']['ADR1'] = $_POST['facturation_numero_voie_etranger'];
				}else{
					if($_POST['facturation_type_voie'] == 0){
						$_SESSION['shipping']['ADR1'] = $_POST['facturation_numero_voie'].' '.$_POST['facturation_nom_voie'];
					}else{
						$_SESSION['shipping']['ADR1'] = $_POST['facturation_numero_voie'].' '.$_POST['facturation_type_voie'].' '.$_POST['facturation_nom_voie'];
					}
				}
				$_SESSION['shipping']['ADR3'] = $_POST['facturation_complement'];
				$_SESSION['shipping']['CPLIVRAISON'] = $_POST['facturation_cp'];
				$_SESSION['shipping']['VILLELIVRAISON'] = $_POST['facturation_ville'];
				$_SESSION['shipping']['pays_id'] = $_POST['facturation_pays'];
		if($_CONST['TYPE_ENVIRONNEMENT'] !='dev' && $_CONST['TYPE_ENVIRONNEMENT'] !='preprod'){
			echo "<script>document.location.href='".$_CONST["HTTPS"].$_CONST['URL_ACCUEIL']."commande/bordas/etape-3-paiement-formule-".$_GET["formule"].".html';</script>";
		}else{
			echo "<script>document.location.href='".$_CONST["URL2"].$_CONST['URL_ACCUEIL']."commande/bordas/etape-3-paiement-formule-".$_GET["formule"].".html';</script>";
		}
		exit; 
}

//Traitement du retour de la connexion via le WS PER
if(isset($_SESSION["connexion"]) && !empty($_SESSION['connexion'])){
	if($_SESSION["connexion"] == "ok"){
		unset($_SESSION["connexion"]);
		$strStep = "step_adresse";
	}else if($_SESSION["connexion"] == "erreur_connexion"){
		$strStep = "step_1_3";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bError = true;
		$strError = "Vos identifiant et mot de passe sont incorrects !";
		$bPopupConnexion = true ; 
	}else if($_SESSION["connexion"] == "erreur_champ"){
		$strStep = "step_1_3";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bError = true;
		$strError = "Veuillez renseigner votre identifiant et mot de passe Bordas.";
		$bPopupConnexion = true ; 
	}else if($_SESSION["connexion"] == "erreur_profil"){
		$strStep = "step_1_3";
		$bStep1 = true; 
		$bStep2 = true;	
		$bStep3 = false;
		$bError = true;
		$strError = "Vous devez posséder un compte Parent pour utiliser ce site. Si vous n’en avez pas, inscrivez-vous directement sur le site Bordas soutien scolaire.";
		$bPopupConnexion = true ; 
	}
	unset($_SESSION["connexion"]);
}
?>

<div class="bss-section bloc-section-gris bss-tunnel">
  <div class="container">
						
						<?php if(isset($strStep) && $strStep == "step_adresse"){
										$aCivilitesXml = array (
												'1' => 'Monsieur',
												'2' => 'Madame',
												'3' => 'Mademoiselle',
											);
						?>
					
						<div class="col-md-8" id="formulaire_adresse">
								<form action="#" method="post" class="form-horizontal clearfix" id="form-adresse" title="form-adresse">		
								<h1 class="h1">Informations personnelles</h1>
								<h2 >Mon pays de résidence :</h2>
									<p>Nous avons besoin de cette information pour appliquer le taux de TVA correspondant à votre pays de résidence.</p>
														<?php
												if(isset($strError) && $strError != ""){
												?>
												<div class="alert alert-danger" role="alert">
												<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<strong>Erreur !</strong><br>
												<?php echo $strError; ?> </div>
												<?php
												}
												?>	

        <div class="bss-creation bloc-bss-1">
          
            <div class="form-group ">
              <label for="civilite" class="col-sm-6 control-label"><?php echo $aCivilitesXml[$_SESSION['user']['CIVILITECLIENT']]; ?></label>
              <div class="col-sm-6">
                <p class="form-control-static"><?php echo $_SESSION['user']['PRENOMCLIENT'].' '.$_SESSION['user']['NOMCLIENT']; ?></p>
              </div>
            </div>
             <div class="form-group">
              <label for="pays" class="col-sm-6 control-label"><span class="obligatoire">*</span>  Pays</label>
              <div class="col-sm-6">
																				<?php 
																				//Récupération de la liste des pays 
																				$strSql = "SELECT * FROM bor_pays ORDER BY pays_libelle ASC" ; 
																				$aListePays = $oDb->queryTab($strSql); 
																				echo "<select class='form-control' title='pays' name='facturation_pays' id='facturation_pays'>"; 
																				echo "<option value='0'>Choisir votre pays</option>"; 
																				foreach ( $aListePays as $aPays){
																												echo "<option value='".$aPays['pays_id']."' ".((isset($_POST['facturation_pays']) && $_POST['facturation_pays'] ==$aPays['pays_id'])? "selected" : "")." >".$aPays['pays_libelle']."</option>";
																				}
																				echo "</select>"; 
																				?>
              </div>
            </div>
            <div class="form-group masque_adresse" style="display:none;">
              <label for="code-postal" class="col-sm-6 control-label"><span class="obligatoire">*</span> Code postal</label>
              <div class="col-sm-3">
																<input type="text" class="form-control inputgris"   disabled  id="facturation_cp" placeholder="Code postal" title="Code postal" name="facturation_cp"  <?php if(isset($_POST['facturation_cp'])) echo "value='".$_POST['facturation_cp']."'"; ?> >
														</div>
            </div>
            <div class="form-group masque_adresse" style="display:none;">
              <label for="Ville" class="col-sm-6 control-label"><span class="obligatoire">*</span> Ville</label>
              <div class="col-sm-6" id="bloc_ville">
																<select class="inputgris form-control" class="form-control" disabled name="facturation_ville" id="facturation_ville">
																				<option>Choisir votre ville</option>
																</select>
														</div>
            </div>
             <div class="form-group masque_adresse" style="display:none;">
              <label for="voie" class="col-sm-6 control-label"><span class="obligatoire">*</span> Adresse</label>
              <div class="col-sm-6">
																<input type="text" class="form-control" style="display:none;" disabled id="facturation_numero_voie_etranger" maxlength="45" placeholder="Adresse" name="facturation_numero_voie_etranger"  <?php if(isset($_POST['facturation_numero_voie_etranger'])) echo "value=\"".(stripslashes($_POST['facturation_numero_voie_etranger']))."\""; ?>>
              <div class="row">
              <div class="col-sm-4">
																<input type="text" class="form-control inputgris" disabled id="facturation_numero_voie" maxlength="10" placeholder="Numéro" title="Ex : 5 TER" name="facturation_numero_voie"  <?php if(isset($_POST['facturation_numero_voie'])) echo "value=\"".(stripslashes($_POST['facturation_numero_voie']))."\""; ?>>
														</div>
              <div class="col-sm-8">
               <?php 
																//Récupération de la liste des types de voies
																$strSql2 = "SELECT * FROM bor_adresse_postale ORDER BY ref_libelle ASC" ; 
																$aListeTypeVoie = $oDb->queryTab($strSql2); 
																echo "<select class='inputgris form-control' disabled name='facturation_type_voie' id='facturation_type_voie' >"; 
																echo "<option value='0'>Type de voie</option>"; 
																foreach ( $aListeTypeVoie as $aTypeVoie){
																	echo "<option value='".$aTypeVoie['ref_code']."' ".((isset($_POST['facturation_type_voie']) && $_POST['facturation_type_voie'] ==$aTypeVoie['ref_code'])? "selected" : "")." >".$aTypeVoie['ref_libelle']."</option>";
																}
																echo "</select>"; 
																?>
														</div>
              </div>
              </div>
          </div>
           
            <div class="form-group masque_adresse" style="display:none;">
              <label for="nom-voie" class="col-sm-6 control-label">&nbsp;</label>
              <div class="col-sm-6">

																<input type="text" class="form-control inputgris" disabled id="facturation_nom_voie" maxlength="23" placeholder="Nom de la voie" name="facturation_nom_voie"  <?php if(isset($_POST['facturation_nom_voie'])) echo "value=\"".(stripslashes($_POST['facturation_nom_voie']))."\""; ?>>
														</div>
            </div>
            <div class="form-group masque_adresse" style="display:none;">
              <label for="adresse-complementaire" class="col-sm-6 control-label">Adresse Complémentaire</label>
              <div class="col-sm-6">

																<input type="text" class="form-control inputgris" disabled name="facturation_complement" id="facturation_complement" placeholder="Adresse complémentaire" <?php if(isset($_POST['facturation_complement'])) echo "value=\"".(stripslashes($_POST['facturation_complement']))."\""; ?>>
														</div>
            </div>
            <hr>
            <p class="legende"><span class="obligatoire">* <strong>champ obligatoire</strong></span></p>
        </div>
								<input type="hidden" name="validation_adresse" value="valide"/>
							</form>
						</div>
					
						
						<?php }else{?>
   
      <div class="col-md-8" id="formulaires_identification">
											<?php
												if(isset($strError) && $strError != ""){
												?>
												<div class="alert alert-danger" role="alert">
												<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<strong>Erreur !</strong><br>
												<?php echo $strError; ?> </div>
												<?php
												}
												?>	
        <h1 class="h1">Informations personnelles</h1>
        <h2 >Vous avez déjà un compte ?</h2>
        <div class="bss-connexion bloc-bss-1">
												<div class="modal-body">
            <h3>Identifiez-vous</h3>
												<form class="form-horizontal" id="form_identification" role="form" action="/proxy/proxy_connexion.php" method="post">
												 <input type="hidden" name="redirect_after" value="<?php echo $_SERVER['REQUEST_URI'];?>">
																<div class="form-group">
              <label for="inputEmail3" class="sr-only">Identifiant</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" id="inputEmail3" value="" name="username" placeholder="Identifiant">
              </div>
              <label for="inputPassword3" class="sr-only">Mot de passe</label>
              <div class="col-sm-6">
                <input type="password" class="form-control" id="inputPassword3" name="password" placeholder="Mot de passe">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6">	
				<a href="#" onclick="jQuery('#inscription').removeClass('in');jQuery('#inscription').hide();jQuery('.modal-backdrop').remove();" class="mot_oublie" data-toggle="modal" data-target="#motdepasseoublie" id="forget_mdp">Mot de passe oublié ? </a>
				</div>
              <div class="col-sm-6">
				<a href="#"
				<?php
				if($_SESSION['cart']['formule']['formule_id'] == 1){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 2', 'F.Ciblée - Je m identifie');" <?php
				}else if($_SESSION['cart']['formule']['formule_id'] == 2){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 2', 'F.Réussite - Je m identifie');" <?php
				}else if($_SESSION['cart']['formule']['formule_id'] == 3){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 2', 'F.Tribu - Je m identifie');" <?php
				}
				?>
				id="submit_form_identification" class="btn btn-fw  btn-primary"> Je m'identifie <i class="icon-angle-circled-right"></i></a>
              </div>
            </div>
		  </form>
        </div>
        </div>
        <hr>
        <h2>Vous n'avez pas de compte ?</h2>
        
								<div class="row">
        <div class="bss-creation bloc-bss-1">	
		<h3>Créez-le maintenant !</h3>
			<?php
			include("./proxy/proxy_createAccount_abo.php");
			?>

           
          
        </div>
      </div>
      </div>
						<?php } ?>

      <div class="col-md-4 bss-step-sidebar">
        <div class="bloc-bss-step-sidebar bloc-bss-gris">
          <div class="bloc-bss-produit-titre">Ma Formule</div>
          <ul>
            <li><?php echo $_SESSION['cart']['formule']['formule_libelle'];?></li>
												<li><?php 
																if($_SESSION['cart']['formule']['formule_id'] == 1)
																				echo "1 classe - 1 matière";
																else if($_SESSION['cart']['formule']['formule_id'] == 2)
																				echo "1 classe - 1 Multi-matières";
																else if($_SESSION['cart']['formule']['formule_id'] == 3)
																				echo count($_SESSION["products"]["enfants"])." classes - Multi-matières";
																?>
												
												</li>
          </ul>
          <div class="text-right"><a href="<?php echo $_CONST['URL_ACCUEIL']; ?>comment-s-abonner.html"
		 <?php
				if($_SESSION['cart']['formule']['formule_id'] == 1){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 2', 'F.Ciblée - Changer de formule');" <?php
				}else if($_SESSION['cart']['formule']['formule_id'] == 2){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 2', 'F.Réussite - Changer de formule');" <?php
				}else if($_SESSION['cart']['formule']['formule_id'] == 3){
					?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 2', 'F.Tribu - Changer de formule');" <?php
				}
				?>
		  class="btn btn-sm btn-default " type="button">Changer de formule</a></div>
        </div>
       
          <div class="bloc-bss-step-sidebar bloc-bss-gris">
															<?php
																$strFormuleIdYonix = $_SESSION['cart']['formule']['formule_id_yonix'];
																$iDe = $_SESSION['products']['enfants'][0]['duree_engagement'];
																$strSql = "SELECT * FROM bor_duree_engagement WHERE de_id = ".$iDe.""; 
																$aDureeEngagement = $oDb->queryTab($strSql);
																
																$strSql = "SELECT produit_prix FROM bor_produit WHERE formule_id_yonix = '".mysql_real_escape_string($strFormuleIdYonix)."' AND de_id_yonix = '".mysql_real_escape_string( $aDureeEngagement[0]['de_id_yonix'])."' LIMIT 1"; 
																$fPrix = $oDb->queryItem($strSql) ;
															
																$fPrixMois = round( $fPrix / $aDureeEngagement[0]['de_engagement'] , 2 ) ;
																?>
            <div class="bloc-bss-produit-titre">Prix de mon abonnement :</div>
            <div class="bloc-bss-produit-stitre">Pour une durée de <span class='duree_libelle'> <?php echo $aDureeEngagement[0]['de_libelle']; ?></span> :</div>
            <div class="bloc-bss-produit-prix"><span>À partir de </span><div style='display:inline;' id='prix_choisi'>
																				</sup></div><sup><span>€/mois</span></sup></div>
            <div class="legende text-center" id="ligne_prix_annee">(soit <span id='prix_annee'><?php echo $fPrix; ?></span>€ pour <span class='duree_libelle'> <?php echo $aDureeEngagement[0]['de_libelle']; ?></span>)</div>
            <div class="form-group">
              <label for="modification-duree" class="sr-only">Modifier la durée</label>
              <select name="modification-duree[0]" class="form-control" form="form-adresse" id="modification-duree" title="modification-duree">
																		<option value='0' selected>Modifier la durée</option>
																		<?php
															$strSql = "SELECT * FROM bor_duree_engagement";
															$aDureeEngagement = $oDb->queryTab($strSql);
															foreach(	$aDureeEngagement as $autre_duree){
																			echo '<option value="'.$autre_duree['de_id'].'" ';
																			
				if($_SESSION['cart']['formule']['formule_id'] == 1){
					if($autre_duree['de_id'] == 1){
						?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 2', 'F.Ciblée - Modifier la durée - 1 mois');" <?php
					}else if($autre_duree['de_id'] == 2){
						?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 2', 'F.Ciblée - Modifier la durée - 3 mois');" <?php
					}else if($autre_duree['de_id'] == 3){
						?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 2', 'F.Ciblée - Modifier la durée - 12 mois');" <?php
					}
				}else if($_SESSION['cart']['formule']['formule_id'] == 2){
					if($autre_duree['de_id'] == 1){
						?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 2', 'F.Réussite - Modifier la durée - 1 mois');" <?php
					}else if($autre_duree['de_id'] == 2){
						?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 2', 'F.Réussite - Modifier la durée - 3 mois');" <?php
					}else if($autre_duree['de_id'] == 3){
						?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 2', 'F.Réussite - Modifier la durée - 12 mois');" <?php
					}
				}else if($_SESSION['cart']['formule']['formule_id'] == 3){
					if($autre_duree['de_id'] == 1){
						?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 2', 'F.Tribu - Modifier la durée - 1 mois');" <?php
					}else if($autre_duree['de_id'] == 2){
						?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 2', 'F.Tribu - Modifier la durée - 3 mois');" <?php
					}else if($autre_duree['de_id'] == 3){
						?> onclick="ga('send', 'event', 'Tunnel Standard', 'Etape 2', 'F.Tribu - Modifier la durée - 12 mois');" <?php
					}
				}
																			echo '>'.$autre_duree['de_libelle'].'</option>';
															}
															?>

              </select>
            </div>
          </div>
          <div class="bloc-bss-step-sidebar bloc-bss-gris">
            <div class="bloc-bss-produit-titre">Mon abonnement pour :</div>
												<?php 
																foreach($_SESSION["products"]["enfants"] as $key=>$enfant){
																			
																				?>
																				<div class="form-group">
																						<label for="nom-enfant" class="control-panel">Nom de l'enfant</label>
																							<input name="nom-enfant[<?php echo $key; ?>]" type="text" class="form-control" id="nom-enfant[<?php echo $key; ?>]" form="form-adresse" placeholder="Nom de l'enfant" title="Nom de l'enfant" value="<?php echo $enfant['D_PRENOM']; ?>">
																				</div>
																				<label for="classe" class="control-panel">
																							Classe :
																				</label>
																				<ul class="liste-matiere">
																				<?php
																					$iNomClasse = $oDb->queryRow("SELECT classe_name
																					FROM bor_classe				
																					WHERE classe_id = ".$_SESSION["products"]["enfants"][$key]["classe"]."");
																					echo '<li>'.$iNomClasse['classe_name'].'</li>';
																				?>
																				</ul>

																				<label for="matiere" class="control-panel">
																								<?php
																								if($_SESSION['cart']['formule']['formule_id'] == 1)
																												echo 'Matière :';
																								else
																												echo 'Matières :';
																								?>
																				</label>
																				<ul class="liste-matiere">
																				<?php
																				// ciblee : une seule matière
																				if($_SESSION['cart']['formule']['formule_id'] == 1){
																					$iNomMatiere = $oDb->queryRow("SELECT matiere_titre
																					FROM bor_matiere
																					WHERE matiere_id = ".$_SESSION["products"]["enfants"][0]["matiere"]."");
																					echo '<li>'.$iNomMatiere['matiere_titre'].'</li>';
																				// réussite : plusieurs matières associées à une classe
																				}else{
																					
																					$aMatieres = $oDb->queryTab("
																					SELECT mc.matiere_id, m.matiere_titre as mc_titre
																					FROM bor_matiere_classe mc
																					INNER JOIN bor_matiere m ON (m.matiere_id = mc.matiere_id)
																					INNER JOIN bor_classe_page cp ON (mc.classe_id = cp.classe_page_id)
																					WHERE cp.classe_page_id =".mysql_real_escape_string((int)$_SESSION["products"]["enfants"][$key]["classe"])."
																					AND mc.mc_dispo = 1");
																					
																					foreach($aMatieres as $iMatiere){
																									echo '<li>'.$iMatiere['mc_titre'].'</li>';
																					}
																				}
																				?>
																				</ul>
																				<hr>
																				<?php
																}
																if( isset($strStep) && $strStep == "step_adresse"){
												?>

								<a href="#" onclick="$('#form-adresse').submit();" class="btn btn-fw  btn-primary"> Continuer <i class="icon-angle-circled-right"></i></a>

											<?php
																}
											?>
          </div>
      </div>
</form>
    </div>
  </div>
</div>
</div>

<script type="text/javascript">
				// changement de la durée
jQuery(document).ready(function(){
	//if(jQuery("#global_registration_par_submit").val() == "")
	//	jQuery("#global_registration_par_submit").val('Valider');

	jQuery("body").animate({"scrollTop":0},"slow");
	jQuery('#submit_form_identification').click(function() {
				
					// enregistre les nouvelles données de la page
					//alert(jQuery('#nom-enfant').val());
					var modification_duree;
					var prenoms = new Array();
					var nb_enfants = <?php if(isset($_SESSION['nb_enfant']) && $_SESSION['nb_enfant'] != "") echo $_SESSION['nb_enfant']; else echo 1; ?>;
					for(i=0; i< nb_enfants; i++){
												//alert($("input[name='prenom\\["+i+"\\]']").val());
												prenoms.push(jQuery("input[name='nom-enfant\\["+i+"\\]']").val());
					}
					if(jQuery('#modification-duree').val() != 0){
									modification_duree = jQuery('#modification-duree').val();
					}else{
									modification_duree = <?php echo $_SESSION['products']['enfants'][0]['duree_engagement']; ?>
									}
				jQuery.ajax({type:"POST", data:{nb_enfants: nb_enfants, prenoms: prenoms, modification_duree: modification_duree} , url: "/templates/ajaxUpdateName.php" ,dataType: 'json',	success: function(json) {
												//alert('ok');
												modification_duree;
				}});

				jQuery('#form_identification').submit();
					
	});
				
				
function updatePrice(duree) {
				
				if(duree == 1){
								jQuery('#ligne_prix_annee').hide();
				}else{
								jQuery('#ligne_prix_annee').show();
				}
				var formule = <?php echo $_SESSION['cart']["formule"]["formule_id_yonix"];?>;
				jQuery.ajax({type:"POST", data:{duree : duree, formule : formule}, url: "/templates/ajaxUpdatePrice.php" ,dataType: 'json',	success: function(json) {
								jQuery('#prix_choisi').html(json[1]);
								jQuery('#prix_annee').html(json[2]);
								jQuery('.duree_libelle').each(function(i, obj) {
												jQuery( this ).html(json[3]);
								});
				},
				error: function(){
								alert('erreur');				
				//	jQuery('#matiere').html('');
				}
				});
			 return false;
}
// au chargement
var duree = <?php echo $_SESSION['products']['enfants'][0]['duree_engagement']; ?>;
updatePrice(duree);

// au changement de durée
jQuery('#modification-duree').change(function () {
		var duree = jQuery( "select#modification-duree option:selected").val();
		if(duree != 0 ){
						updatePrice(duree);
		}
				
});	
		
		
	});
	
// ****

jQuery(document).ready(function(){
	jQuery('.masque_adresse').hide();
	
				if(jQuery("#facturation_pays").val() != 0){
								
								jQuery('#facturation_cp').removeClass('inputgris');
								jQuery('#facturation_cp').removeAttr('disabled');
								if(jQuery.inArray( jQuery("#facturation_pays").val(), [ "1", "140", "144", "159", "106", "122", "109", "218", "227", "216" ] ) == "-1")
								{
										jQuery('#facturation_type_voie').hide();
										jQuery('#facturation_numero_voie').hide();
										jQuery('#facturation_nom_voie').hide();
										jQuery('#facturation_numero_voie_etranger').show();
										jQuery('#facturation_numero_voie_etranger').removeAttr('disabled');
										jQuery('#facturation_numero_voie_etranger').removeClass('inputgris');
										jQuery('#bloc_ville').html('<input type=\"text\" class=\"form-control\" placeholder=\"Ville\" name=\"facturation_ville\" id=\"facturation_ville\"/>');
										jQuery('#facturation_ville').removeClass('inputgris');
										jQuery('#facturation_numero_voie').removeClass('inputgris');
										jQuery('#facturation_type_voie').removeClass('inputgris');
										jQuery('#facturation_nom_voie').removeClass('inputgris');
										jQuery('#facturation_complement').removeClass('inputgris');
										jQuery('#facturation_ville').removeAttr('disabled');
										//jQuery('#facturation_numero_voie').removeAttr('disabled');
										//jQuery('#facturation_type_voie').removeAttr('disabled');
										jQuery('#facturation_nom_voie').removeAttr('disabled');
										jQuery('#facturation_complement').removeAttr('disabled');
								}else{
										jQuery('#facturation_numero_voie_etranger').hide();
										jQuery('#facturation_type_voie').show();
										jQuery('#facturation_numero_voie').show();
										jQuery('#bloc_ville').html('<select class=\"inputgris form-control\" placeholder=\"Ville\" name=\"facturation_ville\" id=\"facturation_ville\"></select>');
										jQuery('#facturation_ville').addClass('inputgris');
										jQuery('#facturation_ville').html('<option>Choisir votre ville</option>');
										jQuery('#facturation_numero_voie').addClass('inputgris');
										jQuery('#facturation_type_voie').addClass('inputgris');
										jQuery('#facturation_nom_voie').addClass('inputgris');
										jQuery('#facturation_complement').addClass('inputgris');
										jQuery('#facturation_ville').attr('disabled','disabled');
										jQuery('#facturation_numero_voie').attr('disabled','disabled');
										jQuery('#facturation_type_voie').attr('disabled','disabled');
										jQuery('#facturation_nom_voie').attr('disabled','disabled');
										jQuery('#facturation_complement').attr('disabled','disabled');
								}
				}
				
	jQuery("#facturation_pays").change(function(){
		jQuery('#facturation_cp').removeClass('inputgris');
		jQuery('#facturation_cp').removeAttr('disabled');
		jQuery('#facturation_cp').val('');
		/*if(jQuery("#facturation_pays").val() != 1)*/
		if(jQuery.inArray( jQuery("#facturation_pays").val(), [ "1", "140", "144", "159", "106", "122", "109", "218", "227", "216" ] ) == "-1")
		{
				jQuery('#facturation_type_voie').hide();
				jQuery('#facturation_numero_voie').hide();
				jQuery('#facturation_nom_voie').hide();
				jQuery('#facturation_numero_voie_etranger').removeAttr('disabled');
				jQuery('#facturation_numero_voie_etranger').removeAttr('inputgris');
				jQuery('#facturation_numero_voie_etranger').show();
				jQuery('#bloc_ville').html('<input type=\"text\" class=\"form-control\" placeholder=\"Ville\" name=\"facturation_ville\" id=\"facturation_ville\"/>');
				jQuery('#facturation_ville').removeClass('inputgris');
				jQuery('#facturation_numero_voie').removeClass('inputgris');
				jQuery('#facturation_type_voie').removeClass('inputgris');
				jQuery('#facturation_nom_voie').removeClass('inputgris');
				jQuery('#facturation_complement').removeClass('inputgris');
				jQuery('#facturation_ville').removeAttr('disabled');
				//jQuery('#facturation_numero_voie').removeAttr('disabled');
				//jQuery('#facturation_type_voie').removeAttr('disabled');
				jQuery('#facturation_nom_voie').removeAttr('disabled');
				jQuery('#facturation_complement').removeAttr('disabled');
		}else{
				jQuery('#facturation_numero_voie_etranger').hide();
				jQuery('#facturation_type_voie').show();
				jQuery('#facturation_numero_voie').show();
				jQuery('#facturation_nom_voie').show();
				jQuery('#bloc_ville').html('<select class=\"inputgris form-control\" placeholder=\"Ville\" name=\"facturation_ville\" id=\"facturation_ville\"></select>');
				jQuery('#facturation_ville').addClass('inputgris');
				jQuery('#facturation_ville').html('<option>Choisir votre ville</option>');
				jQuery('#facturation_numero_voie').addClass('inputgris');
				jQuery('#facturation_type_voie').addClass('inputgris');
				jQuery('#facturation_nom_voie').addClass('inputgris');
				jQuery('#facturation_complement').addClass('inputgris');
				jQuery('#facturation_ville').attr('disabled','disabled');
				jQuery('#facturation_numero_voie').attr('disabled','disabled');
				jQuery('#facturation_type_voie').attr('disabled','disabled');
				jQuery('#facturation_nom_voie').attr('disabled','disabled');
				jQuery('#facturation_complement').attr('disabled','disabled');
		}
	});
	
	jQuery("#facturation_cp").keyup(function( request, response ) {		
		recherche_ville();	
	});
	recherche_ville();
});

/*jQuery(document).ready(function(){
	jQuery("#facturation_pays").change(function(){
            jQuery('#facturation_cp').removeClass('inputgris');
            jQuery('#facturation_cp').removeAttr('disabled');
            jQuery('#facturation_cp').val('');
        });
	jQuery("#facturation_cp").keyup(function( request, response ) {		
		recherche_ville();	
	});
	recherche_ville();
});*/

function recherche_ville(){
	strCp =jQuery("#facturation_cp").val();
	strPays =jQuery("#facturation_pays").val();
	jQuery.ajax({
		url:  "/templates/ajax/ws_adresse.php",
		dataType: "script",
		data: {
			cp: strCp, 
			pays: strPays
		}
	});		
}
</script>