<?php
$iDe = $_SESSION['products']['enfants'][0]['duree_engagement'];
$strSql = "SELECT * FROM bor_duree_engagement WHERE de_id = ".$iDe.""; 
$aDureeEngagement = $oDb->queryTab($strSql);

$aImage  = $oDb->queryRow("SELECT ef.files_path, f.* , p.produit_prix , de.de_engagement , (produit_prix / de_engagement) as price 
						FROM   bor_produit p 
						INNER JOIN bor_formule f  ON (p.formule_id_yonix = f.formule_id_yonix) 
						INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix)
						INNER JOIN eco_files ef ON (ef.files_table_id = f.formule_id AND ef.files_field_name = 'formule_visuel')
						WHERE de_engagement = 12 
						AND f.formule_id = ".$_SESSION['cart']['formule']['formule_id']."
						GROUP BY f.formule_id 
						ORDER BY formule_position "); 
?>

<div class="bss-section bloc-section-gris bss-tunnel">
  <div class="container">
    <div class="row">
      <div class="col-md-3">

	 <img src="/<?php echo $aImage['files_path']; ?>" class="img-responsive">
	   </div>
      <div class="col-md-9">
        <h1 class="h1">Confirmation</h1>
        <h2 class="text-success">Votre abonnement à bien été pris en compte</h2>
        <p class="lead">Nous vous remercions de votre confiance</p>
        <p class="numero-commande bg-success">Numéro de votre commande : <strong><?php echo $_SESSION['cart']['NUMCOMMANDE']; ?></strong></p>
        <p class="texte">Vous allez recevoir dans quelques minutes un mail de confirmation de commande.<br>
          À réception de ce mail votre abonnement sera activé et vous pourrez accéder à la plateforme de soutien scolaire en ligne Bordas. <strong>Si vous ne recevez pas ce mail, merci de vérifier s'il ne se trouve pas dans votre dossier "Courrier indésirable".</strong><br>
          Le service client reste à votre écoute pour toute question par mail ou par téléphone. </p>
        <hr>
        <div class="bss-recap-commande bloc-bss-1">
          <h3 class="h3">Information de facturation</h3>
          <ul class="paiement-recap">
            <li class="numero-commande">Numéro de votre commande : <span class="numero-commande"><strong><?php echo $_SESSION['cart']['NUMCOMMANDE']; ?></strong></span></li>
            <li class="infos-persos">Vos informations personnelles :<strong>
																		<?php $aCivilitesXml = array (
																				'1' => 'Monsieur',
																				'2' => 'Madame',
																				'3' => 'Mademoiselle',
																			);
																	echo $aCivilitesXml[$_SESSION['user']['CIVILITECLIENT']].' '.$_SESSION['user']['PRENOMCLIENT'].' '.$_SESSION['user']['NOMCLIENT']; ?>
																
																</strong></li>
            <li class="adresse-facturation">Votre adresse :<strong> <?php echo $_SESSION["shipping"]['ADR1'].' '.$_SESSION["shipping"]['ADR3'].' '.$_SESSION["shipping"]['CPLIVRAISON'].' '.$_SESSION["shipping"]['VILLELIVRAISON'];?></strong></li>
          </ul>
        </div>
        <div class="bss-recap-commande bloc-bss-1">
          <h3 class="h3">Mode de paiement</h3>
          <ul class="paiement-recap">
            <li class="type-paiement">Type de paiement : <span class="numero-commande"><strong>Carte Bancaire</strong></span></li>
          </ul>
        </div>
        <div class="bss-recap-commande bloc-bss-1">
          <h3 class="h3">Votre abonnement</h3>
          <ul class="paiement-recap">
            <li class="type-formule">Votre formule : <span class="numero-commande"><strong><?php echo $_SESSION['cart']['formule']['formule_libelle'];?></strong></span></li>
            <li class="duree-engagement">Votre durée d'engagement :<strong> <?php echo $aDureeEngagement[0]['de_libelle']; ?></strong></li>
												
												<?php
																$strFormuleClasse = $_SESSION['cart']['formule']['formule_classe'];
																if($strFormuleClasse == "ciblee"){
																				
																				//Récupération du nom de la classe et la matière
																				$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
																				$strMatiere = $oDb->queryItem("SELECT matiere_titre FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['matiere'])."'");
																				//echo '<p class="detail_abonnement">Abonnement pour '.$_SESSION["products"]["enfants"][0]['D_PRENOM'].' en classe de '.$strClasse.' en '.$strMatiere.'.</p>'; 				
																				echo'<li class="nom-enfant-1">Prénom de l\'enfant :<strong> '.$_SESSION["products"]["enfants"][0]['D_PRENOM'].'</strong></li>
																								<li class="classe-enfant-1">Classe : <strong>'.$strClasse.'</strong></li>
																								<li class="matiere-enfant-1">Matière : <strong>'.$strMatiere.'</strong></li>
																								<li class="separateur"><hr></li>';
																				
																				
																}else if( $strFormuleClasse == "reussite"){
																				
																				//Récupération du nom de la classe et des matières
																				$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($_SESSION["products"]["enfants"][0]['classe'])."'");
																				//echo '<p class="detail_abonnement">Abonnement pour '.$_SESSION["products"]["enfants"][0]['D_PRENOM'].' en classe de '.$strClasse.'.</p>';
																				echo'<li class="nom-enfant-1">Prénom de l\'enfant :<strong> '.$_SESSION["products"]["enfants"][0]['D_PRENOM'].'</strong></li>
																								<li class="classe-enfant-1">Classe : <strong>'.$strClasse.'</strong></li>
																								<li class="matiere-enfant-1">Matières : <strong>';
																				$aMatieres = $oDb->queryTab("SELECT mc.matiere_id, m.matiere_titre as mc_titre FROM bor_matiere_classe mc INNER JOIN bor_matiere m ON (m.matiere_id = mc.matiere_id) WHERE mc.classe_id =".mysql_real_escape_string((int)$_SESSION["products"]["enfants"][0]["classe"])." AND mc.mc_dispo = 1");
																								foreach($aMatieres as $key=>$iMatiere){
																												if($key == 0)
																																echo $iMatiere['mc_titre'];
																												else
																																echo ', '.$iMatiere['mc_titre'];
																								}
																				echo '</strong></li><li class="separateur"><hr></li>';
															
																}else if($strFormuleClasse == "tribu"){
																				
																				echo '<p class="detail_abonnement">'; 
																				foreach($_SESSION["products"]["enfants"] as $aEnfant){
																								$strClasse = $oDb->queryItem("SELECT classe_name FROM bor_classe WHERE classe_id ='".mysql_real_escape_string($aEnfant['classe'])."'");
																								//$strMatiere = $oDb->queryItem("SELECT matiere_titre FROM bor_matiere WHERE matiere_id ='".mysql_real_escape_string($aEnfant['matiere'])."'");
																								//echo 'Abonnement pour '.$aEnfant['D_PRENOM'].' en classe de '.$strClasse.'.<br/>';
																								echo'<li class="nom-enfant-1">Prénom de l\'enfant :<strong> '.$aEnfant['D_PRENOM'].'</strong></li>
																												<li class="classe-enfant-1">Classe : <strong>'.$strClasse.'</strong></li>
																												<li class="matiere-enfant-1">Matières : <strong>';
																								$aMatieres = $oDb->queryTab("SELECT mc.matiere_id, m.matiere_titre as mc_titre FROM bor_matiere_classe mc INNER JOIN bor_matiere m ON (m.matiere_id = mc.matiere_id) WHERE mc.classe_id =".mysql_real_escape_string((int)$aEnfant['classe'])." AND mc.mc_dispo = 1");
																												foreach($aMatieres as $key=>$iMatiere){
																																if($key == 0)
																																				echo $iMatiere['mc_titre'];
																																else
																																				echo ', '.$iMatiere['mc_titre'];
																												}
																								echo '</strong></li><li class="separateur"><hr></li>';
																				}
																				echo '</p>';
																}
												?>
												
            <li class="limite-dispo">Disponible jusqu'au : <strong>
			<?php
			$today = date("d-m-Y");
			$date = new DateTime($today.'+ '.$aDureeEngagement[0]['de_engagement'].' month');
			echo $date->format('d/m/Y');
			?></strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<?php 
//Récupération du prix en fonction du produit 
$strSql = "SELECT * FROM bor_produit p INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix) WHERE p.produit_ean = '".mysql_real_escape_string($_SESSION["products"]["enfants"][0]["ean"])."'"; 
$aProduct = $oDb->queryRow($strSql);
	if($_CONST['TYPE_ENVIRONNEMENT'] !='dev'){
            //LOT6 Mise à jour du code affiliation
            if(isset($_SESSION["products"]["enfants"][0]["CODEPROMO"]) && !empty($_SESSION["products"]["enfants"][0]["CODEPROMO"]))
                $strAffiliation = $_SESSION["products"]["enfants"][0]["CODEPROMO"];
            else
                $strAffiliation = "Bordas Soutien Scolaire";
		
	?>

<!-- Facebook fbq track Purchase -->
<script>
	/*fbq('track', 'Purchase', {value: '<?php echo str_replace(",",".",$_SESSION['reglement']['TOTALCOMMANDE']); ?>', currency: 'EUR'});*/
</script>
<!-- /Facebook fbq track Purchase -->

<!-- GA --> 
		<script>
		
			ga('require', 'ecommerce', 'ecommerce.js');
			
			ga('ecommerce:addTransaction', {
			  'id': '<?php echo $_SESSION['cart']['NUMCOMMANDE']; ?>',                     // Transaction ID. Required.
			  'affiliation': '<?php echo $strAffiliation; ?>',   // Affiliation or store name.
			  'revenue': '<?php echo $aProduct['produit_prix']; ?>',               // Grand Total.
			  'shipping': '0',                  // Shipping.
			  'tax': '10'                     // Tax.
			});
			
			ga('ecommerce:addItem', {
			  'id': '<?php echo $_SESSION['cart']['NUMCOMMANDE']; ?>',                     // Transaction ID. Required.
			  'name': '<?php echo $aProduct['produit_titre']; ?>',    // Product name. Required.
			  'sku': '<?php echo $aProduct['produit_ean']; ?>',                 // SKU/code.
			  'category': 'BOUQUET NUMERIQUE',         // Category or variation.
			  'price': '<?php echo $aProduct['produit_prix']; ?>',                 // Unit price.
			  'quantity': '1'                   // Quantity.
			});
						   
			ga('ecommerce:send');
			
			ga('ecommerce:clear');
		</script>
		<!-- /GA -->
	<?php }
	
	/* Tag Universel Cart public idées*/
	if(strtolower($_CONST['TYPE_ENVIRONNEMENT']) != 'dev'){
		$strFormuleClasse = $_SESSION['cart']['formule']['formule_classe'];
		if($strFormuleClasse == "ciblee"){
		?>
			<script type="text/javascript"> 
				var tip = tip || []; 
				tip.push(['_setSegment', '4212', '5842', 
				/* To change >> start */ 
				'<?php echo $_SESSION["products"]["enfants"][0]["ean"]; ?>'           // Id cart - required 
				/* To change << end */ 
				]); 
				(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src =(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidees.com/p/tip/"; 
				var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t) }if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window) 
			</script> 
		<?php
		}else if( $strFormuleClasse == "reussite"){
		?>
			<script type="text/javascript"> 
				var tip = tip || []; 
				tip.push(['_setSegment', '4212', '5842', 
				/* To change >> start */ 
				'<?php echo $_SESSION["products"]["enfants"][0]["ean"]; ?>'           // Id cart - required 
				/* To change << end */ 
				]); 
				(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src =(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidees.com/p/tip/"; 
				var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t) }if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window) 
			</script> 
		<?php
		}else if($strFormuleClasse == "tribu"){
		?>
			<script type="text/javascript"> 
				var tip = tip || []; 
				tip.push(['_setSegment', '4212', '5842', 
				/* To change >> start */ 
				/*'<?php
				echo $_SESSION["products"]["enfants"][0]["ean"].";";
				for($i= 0; $i<$_SESSION['nb_enfant'] ;$i++){
					if($i == 0)
						echo $_SESSION["products"]["enfants"][$i]["ean_reussite"];
					else
						echo ";".$_SESSION["products"]["enfants"][$i]["ean_reussite"];
				}
				?>'           // Id cart - required */
				/* To change << end */
				/* To change >> start */ 
				'<?php echo $_SESSION["products"]["enfants"][0]["ean"]; ?>'           // Id cart - required 
				/* To change << end */ 
				]); 
				(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src =(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidees.com/p/tip/"; 
				var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t) }if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window) 
			</script> 
		<?php
		}
	}
	/* /Tag Universel Cart public idées */
	
	/* Tag Public idées Thank you Page */
	?>
	<script type="text/javascript">

	var tip = tip || [];

	tip.push(['_setSegment', '4212', '5843', 'Thank you Page']);

	(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src=(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidees.com/p/tip/";
	var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window)

	</script>
	<?php 
	/* /Tag Public idées Thank you Page */
	
	/* Implémentation plan de taggage - EFFILIATION */
		// confirmation de paiement
		//if(isset($_GET["templates_id"]) && $_GET["templates_id"] == 27){
			if(isset($_SESSION['paiement']['statut']) && $_SESSION['paiement']['statut'] == "OK"){
				echo '<script src="https://mastertag.effiliation.com/mt660015994.js?page=sale&idp='.$_SESSION["products"]["enfants"][0]["ean"].'&montant='.$_SESSION['reglement']['TOTALCOMMANDE'].'&ref='.$_SESSION['cart']['NUMCOMMANDE'].'" async="async" ></script>';
			}
		//}
	/* --- */
	
	// 17 08 : Ajout pixel de tracking email AFFILIAE
	$montant_ht = round((($_SESSION['reglement']['TOTALCOMMANDE']) / ($_CONST['TAUX_HT'])),2);
	// modifié le 2017 07 25 : 
	?>
	<iframe src="https://lb.affilae.com/?key=5919583be8facec24c8b4573-591957ea06add76072b0f8cb&id=<?php echo $_SESSION['cart']['NUMCOMMANDE']; ?>&amount=<?php echo $montant_ht; ?>&payment=online" frameborder="0" width="1" height="1"></iframe>
	<?php
	
	/* Tags de tracking Effiliation */
				if(strtolower($_CONST['TYPE_ENVIRONNEMENT']) != 'dev'){
					$code_promo = "";
					$nouveau_client = "";		
					$donnees_client = "SELECT * FROM bor_commande c WHERE c.client_id = '".$_SESSION['user']['IDCLIENT']."'"; 
					$client = $oDb->queryTab($donnees_client);

					if(count($client) > 1){
						$nouveau_client = 0;
					}else{
						$nouveau_client = 1;
					}
					if(isset($_SESSION["products"]["enfants"][0]["CODEPROMO"])){
						$code_promo = $_SESSION["products"]["enfants"][0]["CODEPROMO"];
					} 
					// calcul montant total HT :
					$montant_ht = round((($_SESSION['reglement']['TOTALCOMMANDE']) / ($_CONST['TAUX_HT'])),2);
					echo '<img src="http://track.effiliation.com/servlet/effi.revenue?id='.$_CONST['ID_ANNONCEUR'].'&montant='.$montant_ht.'&monnaie=eu&ref='.$_SESSION['cart']['NUMCOMMANDE'].'&payment=CB&newcustomer='.$nouveau_client.'&voucher='.$code_promo.'&ref2='.$strFormuleClasse.'" width="0" height="0">';
				}
				/* --- */
				
				/* Tag Vente public idées*/
				if(strtolower($_CONST['TYPE_ENVIRONNEMENT']) != 'dev'){
					if($strFormuleClasse == "ciblee"){
					?>
					<script type="text/javascript">
    
						var tip = tip || [];
						tip.push(["_setSale","4212","581078","792a8c3e3fd08926a57da5d8a97375fb",
						/* To change >> start */
						'<?php echo $_SESSION['cart']['NUMCOMMANDE']; ?>',        // transaction ID - required
						'<?php echo $montant_ht; ?>',        // total : does not include tax and shipping - required
						''    // additional data
						/* To change << end */
						]);
						(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src=(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidees.com/p/tip/";               
						var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window)

					 </script>
					<?php
					}else if( $strFormuleClasse == "reussite"){
					?>
					<script type="text/javascript">
    
						var tip = tip || [];
						tip.push(["_setSale","4212","581080","ca61117e2b1e0c2d17a02367cd4598ce",
						/* To change >> start */
						'<?php echo $_SESSION['cart']['NUMCOMMANDE']; ?>',        // transaction ID - required
						'<?php echo $montant_ht; ?>',        // total : does not include tax and shipping - required
						''    // additional data
						/* To change << end */
						]);
						(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src=(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidees.com/p/tip/";               
						var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window)

					 </script>
					<?php
					}else if($strFormuleClasse == "tribu"){
					?>
					<script type="text/javascript">
    
						var tip = tip || [];
						tip.push(["_setSale","4212","581082","5e6eb15095c36f52e9670c3d71efab91",
						/* To change >> start */
						'<?php echo $_SESSION['cart']['NUMCOMMANDE']; ?>',        // transaction ID - required
						'<?php echo $montant_ht; ?>',        // total : does not include tax and shipping - required
						''    // additional data
						/* To change << end */
						]);
						(function(e){function t(){var e=document.createElement("script");e.type="text/javascript";e.async=true;e.src=(document.location.protocol=="https:"?"https":"http")+"://tracking.publicidees.com/p/tip/";               
						var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}if(e.addEventListener){e.addEventListener("load",t,false)}else if(e.attachEvent){e.attachEvent("onload",t)}})(window)

					 </script>
 
					<?php
					}
				}
				/* /Tag Vente public idées */

	

unset($_SESSION['cart']); 
unset($_SESSION['shipping']); 
unset($_SESSION['reglement']); 
unset($_SESSION['products']);
unset($_SESSION['paiement']);
unset($_SESSION['nb_enfant']);
?>
