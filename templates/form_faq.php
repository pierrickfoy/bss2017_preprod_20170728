<!-- BREADCRUMB -->
<div class="bss-breadcrumb" >
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h6>
          <ol class="breadcrumb">
            <li id="a" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="index.php" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
            <li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="faq.php" itemprop="url"> <span itemprop="title">FAQ</span> </a></li>
          </ol>
        </h6>
      </div>
    </div>
  </div>
</div>
<div class="bss-section bloc-section-orange bss-ccm">
  <div class="container">
    <h1 id="tagline" class="h1">FAQ</h1>
    <div class="row">
      <div class="col-md-8">
          <?php  
            $Groups = $oDb->queryTab("SELECT * FROM bor_faq_groupe WHERE faq_groupe_actif=1", true);
            
            foreach ($Groups as $Group) {
                echo '<h2 id="subtitle" class="h2">'.$Group['faq_groupe_titre'].'</h2>';
                echo '<div class="panel-group" id="accordion-faq-info" role="tablist" aria-multiselectable="true">';
                $Questions = $oDb->queryTab("SELECT * FROM bor_faq WHERE faq_groupe_id=".$Group['faq_groupe_id']." AND faq_actif=1", true);
               foreach ($Questions as $Question) {
                echo '<div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="info-headingOne">
                          <h4 class="panel-title"> <a class="collapsed" ole="button" data-toggle="collapse" data-parent="#accordion-faq-info" href="#info-collapse'.$Question["faq_id"].'" aria-expanded="false" aria-controls="info-collapse'.$Question["faq_id"].'"> '.$Question["faq_question"].' </a> </h4>
                        </div>
                        <div id="info-collapse'.$Question["faq_id"].'" class="panel-collapse collapse " role="tabpanel" aria-labelledby="info-headingOne">
                          <div class="panel-body"> 
                            <p>'.$Question["faq_reponse"].'</p>
                          </div>
                        </div>
                      </div>';
               }
               echo '</div><hr>';
            }
          ?>
        
      </div>
      <div class="col-md-4 hidden-xs hidden-sm">
        <div class=""> 
          <!-- 16:9 aspect ratio -->
           <div class="embed-responsive embed-responsive-16by9"><iframe src="https://www.youtube.com/embed/CItcTboz6_k?rel=0" width="320" height="240"></iframe></div>

        </div>
        <hr>
        <?php /*echo get_datablocks("pub");*/  ?>
      </div>
      <div class="col-md-12">
        <hr>
        <p class="text-center"><a class="btn btn-md btn-primary" href="/comment-s-abonner.html">Je découvre toutes les formules d'abonnement<i class="icon-angle-right"></i></a></p>
      </div>
    </div>
  </div>
</div>