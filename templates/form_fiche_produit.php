<?php
$iProduit = $_GET["produit"];
//Récupération des infos
$aProduit  = $oDb->queryRow("SELECT ef.files_path, f.* , p.produit_prix , de.de_engagement , (produit_prix / de_engagement) as price 
							FROM bor_produit p 
							INNER JOIN bor_formule f  ON (p.formule_id_yonix = f.formule_id_yonix) 
							INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix)
							INNER JOIN eco_files ef ON (ef.files_table_id = f.formule_id AND ef.files_field_name = 'formule_visuel')
							WHERE de_engagement = 12 
							AND f.formule_classe = '".$iProduit."'
							GROUP BY f.formule_id 
							ORDER BY formule_position ");
//var_dump($aProduit);

// traitement du lien "tester"
$testerLien = $aProduit['formule_tester_url'];
$testerLienBlank = $aProduit['formule_tester_url_blank'];
if($testerLienBlank != NULL) $testerLienBlank = 'target="_blank"'; else $testerLienBlank = "";
?>

<?php include ("./breadcrumb_classique.php");?>
<?php include ("section-fiche-produit.php");?>
<?php /*include ("section-test-demo.php");*/ ?>
<?php include ("section-voir-video.php");?>
<?php include ("section-bss-autres-produits.php");?>