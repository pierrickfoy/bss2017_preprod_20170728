<link href="/assets/bordas-2015/global_abonnement.css" rel="stylesheet" type="text/css">

<?php
// mise en session pour redirection après identification
$_SESSION['strUrlToRedirect'] = (isset($_SERVER['HTTPS'])) ? 'https://' . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" : 'http://' . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if(!isset($_SESSION['user']['IDCLIENTWEB']) || empty($_SESSION['user']['IDCLIENTWEB'])){
	$strLien = $oDb->queryItem("SELECT templates_name FROM  eco_templates WHERE templates_id = 34"); 
	$strProvenance = $_CONST['URL2'].$_CONST['URL_ACCUEIL'].strToUrl($strLien).'.html' ;
	echo "<script>document.location.href='".$strProvenance."';</script>";
	exit;
}
$_SESSION['strUrlToRedirect'] = "";
?>

<script type="text/javascript">
		jQuery.noConflict();	
</script>

<?php include('./breadcrumb_classique.php');?>

<!--	<section class="description">
		<div class="container">
			<h1><?php echo get_template_title(); ?></h1>
			<div><?php echo get_template_data(); ?></div>
		</div>
	</section>-->

<div class="bss-section bloc-section-bleu bss-gestion-abonnement">
  <div class="container">
    <div class="row">
    <div class="col-md-12">
      <h1 class="h1"><?php echo get_template_title(); ?></h1>
      <h2 class=""><?php echo get_template_data(); ?></h2></div>

	<div class="container">
		<div class="separateur"></div>
		 <div class="row">
			<div class="center_column col-md-8">
				<?php
					include("./proxy/proxy_updateAccount_bordas.php");
				?>
				<div class="formulaire_form"> 
					<p class="obligatoire"> <strong>*</strong> champs obligatoire</p>
					<p class="description_pack">
						Conformément à la loi Informatique et Libertés du 6 janvier 1978 modifiée en 2004, vous disposez d’un droit d’accès, de rectification ou d’opposition aux données personnelles vous concernant, à la Relation client BORDAS : <a href="mailto:soutien-scolaire@bordas.tm.fr">soutien-scolaire@bordas.tm.fr</a>, 01 72 36 40 91.
					</p>
				</div> 
			</div>


		</div>
	</div>
								
				</div>
  </div>
</div>