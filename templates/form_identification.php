<?php 
	if(isset($_SESSION['user']['IDCLIENT']) && $_SESSION['user']['IDCLIENT']  > 0 ){
		$strLien = $oDb->queryItem("SELECT templates_name FROM  eco_templates WHERE templates_id =30"); 
		$strProvenance = ( $_CONST['TYPE_ENVIRONNEMENT'] != "prod" ? $_CONST['URL2'] : $_CONST['HTTPS'] ).$_CONST['URL_ACCUEIL'].strToUrl($strLien).'.html' ;
		echo "<script>document.location.href='".$strProvenance."';</script>";
		exit;
	}
?>

<?php include('./breadcrumb_classique.php');?>
  
  <div class="bss-section bloc-section-orange bss-ccm">
  <div class="container">
    <h1 id="tagline" class="h1"><?php echo get_template_title(); ?></h1>
    <div class="col-md-8 col-md-offset-2 ">
    <div class="bss-connexion bloc-bss-1">
								<?php 
								if (isset($_SESSION["connexion_identification"]) && !empty($_SESSION["connexion_identification"])){
									if ($_SESSION["connexion_identification"] == "erreur_connexion")
										$strMessage = "Vos identifiant et mot de passe sont incorrects !" ; 
									else if ($_SESSION["connexion_identification"] == "erreur_champ")
										$strMessage = "Veuillez renseigner un identifiant et un mot de passe." ; 
									else if ($_SESSION["connexion_identification"] == "erreur_profil")
										$strMessage = "Vous devez posséder un compte Parent pour utiliser ce site. Si vous n’en avez pas, inscrivez-vous directement sur le site Bordas soutien scolaire." ; 
								
									echo '<div class="modal-header">
											<h2>'.$strMessage.'</h2>
										</div>';
									unset($_SESSION["connexion_identification"]); 
								}
							?>
								
          <form  method="post" id="feedback-form" action="/proxy/proxy_connexion.php" class="form-horizontal clearfix" id="authentification" title="authentification">								  
            <h2 class="text-center">Connectez-vous à votre compte</h2>
            <div class="form-group">
              <label for="username" class="sr-only">Identifiant</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" id="username" name="username" placeholder="Identifiant">
              </div>
              <label for="password" class="sr-only">Mot de passe</label>
              <div class="col-sm-6">
                <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe">
              </div>
            </div>
												
            <div class="form-group">
              <div class="col-sm-6">
																		<?php 
																				$strLien = $oDb->queryItem("SELECT templates_name FROM  eco_templates WHERE templates_id =36"); 
																				$strProvenance = $_CONST['URL2'].$_CONST['URL_ACCUEIL'].strToUrl($strLien).'.html' ;
																				echo '<a href="'.$strProvenance.'" class="btn btn-link">Mot de passe oublié ?</a>';
																			?>
														</div>
              <div class="col-sm-6">
																<input type='hidden' name='action' value='submit'/>
																<input type='hidden' name='page' value='identification'/>
																<input type='hidden' name='redirect_after' value='<?php if(isset($_SESSION['strUrlToRedirect'])) echo $_SESSION['strUrlToRedirect'];  else echo '/identification.html' ; ?>'/>
                <button type="submit" id="btnValidate" name="action" class="btn btn-primary btn-fw">Je m'identifie</button>
              </div>
            </div>
          </form>
        </div>
     </div>
  </div>
</div>