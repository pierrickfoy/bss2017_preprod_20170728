<?php
$iMatiereUrl = "/".$_GET["matiere_url"]."/";
//Récupération des infos de la classe
$strSql = "SELECT *
FROM bor_matiere_page cp
INNER JOIN bor_matiere c ON (cp.matiere_id = c.matiere_id)
WHERE cp.matiere_page_url = '$iMatiereUrl'"; 
$aMatiere = $oDb->queryRow($strSql);

// redirection vers home si url ko
if(empty($aMatiere)){
	echo '<SCRIPT LANGUAGE="JavaScript">
document.location.href="'.$_CONST['URL2'].'"
</SCRIPT>';
	exit;
}
	

$aImage  = $oDb->queryRow("SELECT ef.files_path
							FROM   eco_files ef
							WHERE ef.files_table_id = ".$aMatiere['matiere_page_id']." AND ef.files_field_name = 'matiere_page_image'
							");
							
// Ajout 20 07 2016
// slider
$aSlider  = $oDb->queryTab("SELECT *
							FROM   bor_slider_page sp
							LEFT JOIN eco_files ef ON (ef.files_id = sp.files_id)
							WHERE sp.page_id = ".$aMatiere['matiere_page_id']." AND sp.tpl = 'matiere_page'
							ORDER BY sp.position
							", true); 

// traitement du lien slider
$sliderLien = $aMatiere['matiere_page_slider_url'];
if(!$sliderLien) $sliderLien= "#";
$sliderLienBlank = $aMatiere['matiere_page_slider_url_blank'];
if($sliderLienBlank != NULL) $sliderLienBlank = 'target="_blank"'; else $sliderLienBlank = "";

// traitement du lien "tester"
$testerLien = $aMatiere['matiere_page_tester_url'];
$testerLienBlank = $aMatiere['matiere_page_tester_url_blank'];
if($testerLienBlank != NULL) $testerLienBlank = 'target="_blank"'; else $testerLienBlank = "";

//masquer la demo
$aMatiere['matiere_page_URL_CGS'] = "";
?>

<?php include ("./breadcrumb_classique.php");?>
<?php include 'section-matiere.php';?>
<?php /*include 'section-avantage-abonne.php';*/?>
<?php include 'section-classe-disponible.php';?>
<?php include 'section-home-abonnement.php';?>
<?php /*include 'section-compatibilite.php';*/?>
<?php include 'section-temoignage.php';?>
<?php /*include 'section-notion-matiere-relatif.php';*/?>
<?php include 'section-newsletter.php';?>