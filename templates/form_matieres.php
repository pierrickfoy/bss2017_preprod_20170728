<section class="description">
	<div class="container">
		<h1><?php echo get_template_title(); ?></h1>
		<div><?php echo get_template_data(); ?></div>
		<?php 
			include_once("./templates/sidebar/form_btn_abo.php");
		?>
	</div>
</section>


<div class="container">

<div class="separateur"></div>

  <div class="row">
  
    <div class="center_column col-md-8">

<div class='titre_h2'>Choisissez votre matière :</div>

<?php 
//Récupération de la liste des matières
$strSql = "SELECT m.*, f.files_path FROM bor_matiere m LEFT JOIN eco_files f ON (f.files_field_name = 'matiere_image' AND f.files_table_id = m.matiere_id ) WHERE matiere_actif = 1 ORDER BY matiere_position"; 
$aMatieres = $oDb->queryTab($strSql); 
foreach($aMatieres as $aMatiere){
	echo '
<div class="matiere_block">
	<h3>'.$aMatiere['matiere_titre'].'</h3>
	<div class="photo_cp">
		<img class="img-responsive" src="'.$aMatiere['files_path'].'" alt="'.$aMatiere['matiere_titre'].'">
	</div>

	<div class="description_cp">
		<p>
			'.$aMatiere['matiere_html'].'
		</p>
		
	'; 
	
	//Récupération des classes 
	$strSql = "SELECT mc.mc_titre, mc.mc_id ,c.classe_name FROM bor_classe c INNER JOIN bor_matiere_classe mc ON (mc.classe_id = c.classe_id) WHERE mc.matiere_id = ".$aMatiere['matiere_id']." AND mc.mc_actif =1 ORDER BY c.classe_id"; 
	$aClasses = $oDb->queryTab($strSql); 
	if($oDb->rows > 0 ){
		echo '<label>Choisissez votre classe :</label>
				<select onchange="location.href=this.value;">
				<option >...</option>'; 
				
		foreach ($aClasses as $aClasse ){
			echo "<option value='".$_CONST['URL_ACCUEIL'].'matiere-'.strToUrl($aClasse['mc_titre']).'-'.$aClasse['mc_id'].".html'>".$aClasse['classe_name']."</option>"; 
		}
		echo '	
		</select>'; 
	}
	echo '
	</div>
</div>


<div class="spacer"></div>'; 
}



?>










    </div>
    
    
    
    
    <aside class="right_column col-md-4">
			<?php 
			include_once("./templates/sidebar/form_sidebar_youtube.php");
			include_once("./templates/sidebar/form_sidebar_wengo.php");
			include_once("./templates/sidebar/form_sidebar_publicite.php");
			?>
			
		</aside>
  </div>
</div>