<?php 
if(isset($_SESSION['confirmation']) && !empty($_SESSION['confirmation'])){
	//Traitement de la réponse 
	if($_SESSION['confirmation'] == "ok" ){
		$bPost = true;
		$strMessage = "Votre mot de passe a été modifié.";
	}else if($_SESSION['confirmation'] == "erreur_password" ){
		$strMessageBox = "La confirmation de votre mot de passe n'est pas valide !" ; 
	}else if($_SESSION['confirmation'] == "erreur_champ" ){
		$strMessageBox = "Veuillez renseigner tous les champs ci-dessous." ; 
	}else if($_SESSION['confirmation'] == "erreur_update" ){
		$strMessageBox = "La récupération de mot de passe a échoué. Vérifiez votre identifiant et référez-vous aux informations figurant dans l'e-mail que vous avez reçu lors de votre demande de récupération de mot de passe." ; 
	}else if($_SESSION['confirmation'] == "erreur_technique" ){
		$strMessageBox = "Un problème technique empêche la récupération du mot de passe." ; 
	}
	unset( $_SESSION['confirmation'] ) ;
}

?>

<?php include('./breadcrumb_classique.php');?>

<div class="bss-section bloc-section-orange bss-ccm">
  <div class="container">
    <h1 id="tagline" class="h1"><?php echo get_template_title(); ?></h1>
    <div class="row">
   
    <div class="col-md-8 col-md-offset-2 ">
    <div class="bss-connexion bloc-bss-1">
							 	<?php 
						if($bPost){
					?>
						
								  <div class="alert alert-success">
									 <p><?php echo $strMessage ; ?></p>
								  </div>
							
					<?php 
						}else{
						
							if(!empty($strMessageBox)){
									?>
										
												  <div class="alert alert-danger">
													 <p><?php echo $strMessageBox ; ?></p>
												  </div>
											
									<?php 
								}
					?>
								  <form  method="post" class="form-horizontal clearfix" id="feedback-form" action="/proxy/proxy_mdpForget.php">	
            <h2 class="text-center"><?php echo get_template_data(); ?></h2>
            <div class="form-group">
              <label for="identifiant" class="sr-only">Identifiant du compte :</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" id="txtLogin" name="login" value="" required placeholder="Mon login">
              </div>
            </div>
												
												<div class="form-group">
              <label for="identifiant" class="sr-only">Nouveau mot de passe :</label>
              <div class="col-sm-12">
                <input type="password" class="form-control" id="txtLogin" name="password" value="" required placeholder="Nouveau mot de passe">
              </div>
            </div>
												
												<div class="form-group">
              <label for="identifiant" class="sr-only">Confirmation du nouveau mot de passe :</label>
              <div class="col-sm-12">
                <input type="password" class="form-control" id="txtLogin" name="confirm_pasword" value="" required placeholder="Confirmation mot de passe">
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
																		<div class="modal-footer">
																<input type='hidden' name='action' value='generate_password'/>
																<input type='hidden' name='ticket' value='<?php echo $_GET['ticket']; ?>'/>
                <button type="submit" onclick="$('#feedback-form').submit();" id="btnValidate" name='action' class="btn btn-primary btn-fw">Valider<i class="icon-angle-right"></i></button>
              </div>
              </div>
            </div>
          </form>
								<?php 
					}
					?>
        </div>
     </div>
     </div>
  </div>
</div>