<?php 
	if(isset($_SESSION['user']['IDCLIENT']) && $_SESSION['user']['IDCLIENT']  > 0 ){
		$strLien = $oDb->queryItem("SELECT templates_name FROM  eco_templates WHERE templates_id =31"); 
		$strProvenance = $_CONST['URL'].$_CONST['URL_ACCUEIL'].strToUrl($strLien).'.html' ;
		echo "<script>document.location.href='".$strProvenance."';</script>";
		exit;
	}
?>

<?php include('./breadcrumb_classique.php');?>

<div class="bss-section bloc-section-orange bss-ccm">
  <div class="container">
    <h1 id="tagline" class="h1"><?php echo get_template_title(); ?> ?</h1>
    <div class="row">
   
    <div class="col-md-8 col-md-offset-2 ">
    <div class="bss-connexion bloc-bss-1">
								<?php 
								if (isset($_SESSION["mdp_forget_motdepasse"]) && !empty($_SESSION["mdp_forget_motdepasse"])){
									if ($_SESSION["mdp_forget_motdepasse"] == "ok")
										$strMessage = "Un e-mail vient de vous être envoyé. Veuillez suivre les instructions qui s'y trouvent." ; 
									else if ($_SESSION["mdp_forget_motdepasse"] == "erreur_update")
										$strMessage = "Cette adresse email n'est pas reconnue sur ce site. Si vous avez changé d'adresse email, merci de nous contacter." ; 
									else if ($_SESSION["mdp_forget_motdepasse"] == "erreur_technique")
										$strMessage = "Un problème technique empêche la récupération du mot de passe." ; 
								
									echo '<div class="modal-header">
											<h2>'.$strMessage.'</h2>
										</div>';
									// unset($_SESSION["mdp_forget_motdepasse"]); 
								}
								?>
								  <form  method="post" class="form-horizontal clearfix" id="feedback-form" action="/proxy/proxy_mdpForget.php">	
            <h2 class="text-center">Entrez l'adresse e-mail associé à votre inscription</h2>
            <div class="form-group">
              <label for="identifiant" class="sr-only">Entrez l'adressse e-mail associé à votre inscription :</label>
              <div class="col-sm-12">
                <input type="email" class="form-control" id="email" name="email_forgot" value="" required placeholder="Mon adresse email">
              </div>
             
            </div>
            <div class="form-group">
              <div class="col-sm-12">
																<input type='hidden' name='page' value='motdepasse'/>
																<input type='hidden' name='redirect_after' value='/soutien-scolaire-en-ligne/vous-avez-oublie-votre-mot-de-passe.html'/>
                <button type="submit" id="btnValidate" name='action' class="btn btn-primary btn-fw">Valider<i class="icon-angle-right"></i></button>
              </div>
            </div>
          </form>
        </div>
     </div>
     </div>
  </div>
</div>