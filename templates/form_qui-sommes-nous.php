<!-- BREADCRUMB -->
  <div class="bss-breadcrumb" >
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h6>
            <ol class="breadcrumb">
              <li id="a" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="/index.php" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
              <li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="#" itemprop="url"> <span itemprop="title">Qui sommes-nous ?</span> </a></li>
            </ol>
          </h6>
        </div>
      </div>
    </div>
  </div>

<div class="bss-section bloc-section-gris-orange bss-qsn">
  <div class="container">
    <h1 id="tagline" class="h1">Qui sommes-nous ?</h1>
    
    <?php echo get_template_data(); ?>
  </div>
</div>
