<?php
$iLimite = 11 ;
//var_dump(	$iLimite);exit;
	
	if(isset($_GET['id_rubrique'])){
	
					// si une rubrique est selectionnée
					if($_GET['id_rubrique'] != 0){
								$iRubriqueId = mysql_real_escape_string($_GET['id_rubrique']); 

								//Récupération de la page
								if(!isset($_GET['page']) || empty($_GET['page']))
									$iPage = 1 ; 
								else
									$iPage = mysql_real_escape_string($_GET['page']); 
								
								//Début du compteur
								$iStart = ($iPage-1) * $iLimite ; 

								//On compte le nombre d'éléments 		
								$strSQLList = "	SELECT count(*) 
															FROM	bor_article a
															INNER JOIN bor_rubrique r ON (r.rubrique_id = a.rubrique_id)
															WHERE 	a.article_actif = 1
															AND r.rubrique_id = ".$iRubriqueId;
								$iCount=$oDb->queryItem($strSQLList);
								if($iCount > $iLimite)
									$iNbPage = ceil($iCount / $iLimite); 


								$strSQLList = "	SELECT rubrique_titre, rubrique_description, article_id, article_titre, article_soustitre, article_position, DATE_FORMAT(article_date_add,'%d/%m/%Y') as date_add, DATE_FORMAT(article_date_publication,'%d/%m/%Y') as date, files_path
															FROM	bor_article a
															INNER JOIN bor_rubrique r ON (r.rubrique_id = a.rubrique_id)		
															LEFT JOIN eco_files f ON (f.files_table_id =  a.article_id AND files_field_name ='article_image' AND files_table_id_name='article_id' AND files_table_source = 'borarticle' )
															WHERE 		a.article_actif = 1
																AND r.rubrique_id = ".$iRubriqueId."
																ORDER BY date DESC
																	LIMIT $iStart, $iLimite
																";
								$aArticles=$oDb->queryTab($strSQLList);


								$strSQLList = "	SELECT rubrique_titre, rubrique_description
															FROM bor_rubrique
															WHERE rubrique_id = ".$_GET['id_rubrique']."";

								$strresult=$oDb->queryRow($strSQLList);
								
					// si aucune rubrique selectionnée = page générale, pas de tri des actus
					}else{
								//Récupération de la page
								if(!isset($_GET['page']) || empty($_GET['page']))
									$iPage = 1 ; 
								else
									$iPage = mysql_real_escape_string($_GET['page']); 

								//Début du compteur
								$iStart = ($iPage-1) * $iLimite ; 

								//On compte le nombre d'éléments 		
								$strSQLList = "	SELECT count(*) 
															FROM	bor_article a
															INNER JOIN bor_rubrique r ON (r.rubrique_id = a.rubrique_id)
															WHERE 	a.article_actif = 1";
								$iCount=$oDb->queryItem($strSQLList);
								if($iCount > $iLimite)
									$iNbPage = ceil($iCount / $iLimite); 


								$strSQLList = "	SELECT rubrique_titre, rubrique_description, article_id, article_titre, article_soustitre, article_position,DATE_FORMAT(article_date_add,'%d/%m/%Y') as date_add, DATE_FORMAT(article_date_publication,'%d/%m/%Y') as date, files_path
															FROM	bor_article a
															INNER JOIN bor_rubrique r ON (r.rubrique_id = a.rubrique_id)		
															LEFT JOIN eco_files f ON (f.files_table_id =  a.article_id AND files_field_name ='article_image' AND files_table_id_name='article_id' AND files_table_source = 'borarticle' )
															WHERE 		a.article_actif = 1
																ORDER BY article_date_publication DESC
																	LIMIT $iStart, $iLimite
																";
								$aArticles=$oDb->queryTab($strSQLList);


								$strSQLList = "	SELECT rubrique_titre, rubrique_description
															FROM bor_rubrique";
								$strresult=$oDb->queryRow($strSQLList);
									
					}
		?>
<!-- BREADCRUMB -->
  <div class="bss-breadcrumb" >
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h6>
            <ol class="breadcrumb">
              <li id="a" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="<?php echo $_CONST['URL_ACCUEIL'];?>" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
              <li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="<?php echo $_CONST['URL_ACCUEIL'];?>actualite/" itemprop="url"> <span itemprop="title">Actualité</span> </a></li>
              
														<?php
														if(isset($iRubriqueId)){
																			echo '<li id="3" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="b"> <a href="#" itemprop="url"> <span itemprop="title">'.$strresult['rubrique_titre'].'</span> </a></li>';
														}
													  ?>
            </ol>
          </h6>
        </div>
      </div>
    </div>
  </div>
<div class="bss-section bloc-section-orange bss-actualite">
  <div class="container">
								<?php
								if($_GET['id_rubrique'] != 0){
								?>
												<h1 id="tagline" class="h1"><?php echo $strresult['rubrique_titre'];?></h1>
												<h2 id="tagline" class="h2"><?php echo $strresult['rubrique_description'];?></h2>
								<?php
								}else{
								?>
												<h1 id="tagline" class="h1">Actualités Bordas Soutien Scolaire</h1>
												<h2 id="tagline" class="h2">Toute l'actualité du service d'entraînement en ligne de Bordas Soutien scolaire.</h2>
								<?php
								}
								?>
    <div id="articles" class="row">
								
		<?php						foreach ($aArticles as $key=>$aArticle){
						//var_dump($aArticle);
								if(!empty($aArticle["files_path"]))
									$strPathImg = $aArticle["files_path"];
								else
									$strPathImg = "../uploads/articles/img_article_defaut.jpg";

								$lien = $_CONST['URL_ACCUEIL'].'actualite/'.strToUrl($aArticle['rubrique_titre']).'/'.$aArticle['article_id'].'-'.strToUrl($aArticle['article_titre']).'.html';
				?>
      <div class="item col-sm-6 col-md-4 hidden-xs">
        <div class="bloc-bss-actu">
												<div class="visuel-actu"><a href="<?php echo $lien; ?>"><div style="max-height:114px;"><img style="width:100%;" src="/<?php echo $strPathImg; ?>" class="img-responsive"></a></div></div>
          <div class="info-actu "><h2><a href="<?php echo $lien; ?>"><?php echo $aArticle['article_titre']; ?></a></h2>
          <div class="tag-actu "><span class="rubrique"><?php echo $aArticle['rubrique_titre']; ?></span><div class="pull-right"><?php echo $aArticle['date']; ?></div></div>
          <div class="excerpt"><?php echo $aArticle['article_soustitre']; ?></div></div>
        </div>
      </div>
				<?php							
				if($key == 4){
								include_once("./templates/sidebar/form_sidebar_wengo.php");
				}
		}		
				?>
								
    </div>
    <nav class="text-center">
								<ul class="pagination ">
								<?php
								if($_GET['id_rubrique'] != 0){
												if($iNbPage > 1){
																//if($iRubriqueId ==  $_CONST["ID_SOUTIENT_SCOLAIRE"])
																				//$strLien = $oDb->queryItem("SELECT rubrique_titre FROM bor_rubrique WHERE rubrique_id = ".$_CONST['ID_SOUTIENT_SCOLAIRE']);
																//else if ($iRubriqueId == $_CONST["ID_ACTUALITE"])
																				$strLien = $oDb->queryItem("SELECT rubrique_titre FROM bor_rubrique WHERE rubrique_id = ".$_GET['id_rubrique']);

																	//Bouton précédent
																if($iPage == 1){
																				echo '<li class="disabled">
																													<a href="#" >
																																	<span>&laquo;</span>
																													</a>
																									</li>';
																}else{
																	if($iPage==2){
																						echo '<li p='.($iPage-1).' rel="prev">
																							<a href="'.$_CONST['URL_ACCUEIL'].'actualite/'.strToUrl($strLien).'/" aria-label="Previous">
																									<span aria-hidden="true">&laquo;</span>
																							</a>
																					</li>';
																			}else{
																										echo '<li p='.($iPage-1).' rel="prev">
																							<a href="'.$_CONST['URL_ACCUEIL'].'actualite/'.strToUrl($strLien).'/page='.($iPage-1).'" aria-label="Previous">
																									<span aria-hidden="true">&laquo;</span>
																							</a>
																					</li>';
																			}
																}
																//Boucle
																for($i=1; $i<= $iNbPage ; $i++){
																	if($i==$iPage){
																				echo '		<li p='.$i.' class="active"><a href="#">'.$i.'</a></li>';
																	}else if($i==1){
																				echo '		<li p='.$i.'><a href="'.$_CONST['URL_ACCUEIL'].'actualite/'.strToUrl($strLien).'/">'.$i.'</a></li>';
																	}else{
																				echo '		<li p='.$i.'><a href="'.$_CONST['URL_ACCUEIL'].'actualite/'.strToUrl($strLien).'/page='.($i).'">'.$i.'</a></li>';
																	}
																}

																//Bouton suivant
																if($iPage == $iNbPage){
																				echo '<li class="disabled">
																							<a href="#" >
																									<span >&raquo;</span>
																							</a>
																					</li>';
																}else{
																						echo '<li p='.($iPage+1).' rel="next">
																							<a href="'.$_CONST['URL_ACCUEIL'].'actualite/'.strToUrl($strLien).'/page='.($iPage+1).'">
																									<span aria-hidden="true">&raquo;</span>
																							</a>
																					</li>';
																}
												}
								}else{
												if($iNbPage > 1){
																if($iRubriqueId ==  $_CONST["ID_SOUTIENT_SCOLAIRE"])
																				$strLien = $oDb->queryItem("SELECT rubrique_titre FROM bor_rubrique WHERE rubrique_id = ".$_CONST['ID_SOUTIENT_SCOLAIRE']);
																else if ($iRubriqueId == $_CONST["ID_ACTUALITE"])
																				$strLien = $oDb->queryItem("SELECT rubrique_titre FROM bor_rubrique WHERE rubrique_id = ".$_CONST['ID_ACTUALITE']);

																	//Bouton précédent
																if($iPage == 1){
																				echo '<li class="disabled">
																													<a href="#" >
																																	<span>&laquo;</span>
																													</a>
																									</li>';
																}else{
																	if($iPage==2){
																						echo '<li p='.($iPage-1).' rel="prev">
																							<a href="'.$_CONST['URL_ACCUEIL'].'actualite/" aria-label="Previous">
																									<span aria-hidden="true">&laquo;</span>
																							</a>
																					</li>';
																			}else{
																										echo '<li p='.($iPage-1).' rel="prev">
																							<a href="'.$_CONST['URL_ACCUEIL'].'actualite/page='.($iPage-1).'" aria-label="Previous">
																									<span aria-hidden="true">&laquo;</span>
																							</a>
																					</li>';
																			}
																}
																//Boucle
																for($i=1; $i<= $iNbPage ; $i++){
																	if($i==$iPage){
																				echo '		<li p='.$i.' class="active"><a href="#">'.$i.'</a></li>';
																	}else if($i==1){
																				echo '		<li p='.$i.'><a href="'.$_CONST['URL_ACCUEIL'].'actualite/">'.$i.'</a></li>';
																	}else{
																				echo '		<li p='.$i.'><a href="'.$_CONST['URL_ACCUEIL'].'actualite/page='.($i).'">'.$i.'</a></li>';
																	}
																}

																//Bouton suivant
																if($iPage == $iNbPage){
																				echo '<li class="disabled">
																							<a href="#" >
																									<span >&raquo;</span>
																							</a>
																					</li>';
																}else{
																						echo '<li p='.($iPage+1).' rel="next">
																							<a href="'.$_CONST['URL_ACCUEIL'].'actualite/page='.($iPage+1).'">
																									<span aria-hidden="true">&raquo;</span>
																							</a>
																					</li>';
																}
												}
								}
				?>
								</ul>
				</nav>
  </div>
</div>



<?php
						
		/*echo'
				<input type="hidden"  name="id_rubrique" id="id_rubrique" value="'.$_GET['id_rubrique'].'">
			<div class="bss-section bloc-section-orange bss-actualite">
				<div class="container">
					<section class="description">
						<h1>'.$strresult['rubrique_titre'].'</h1>
						<div>'.$strresult['rubrique_description'].'</div>'; 
						//include_once("./templates/sidebar/form_btn_abo.php");
		echo'		</section>
				</div>
						
				<div class="container">
					<div class="separateur"></div>
					<div class="row">						
						<div id="containerpg" class="center_column col-md-8">
							<div class="data">'; 
				
							foreach ($aArticles as $aArticle){
								if(!empty($aArticle["files_path"]))
									$strPathImg = $aArticle["files_path"];
								else
									$strPathImg = "../uploads/articles/img_article_defaut.jpg";
								
								echo '	<a href="'.$_CONST['URL_ACCUEIL'].'rubrique-'.strToUrl($aArticle['rubrique_titre']).'-'.$aArticle['article_id'].'-'.strToUrl($aArticle['article_titre']).'.html" style="text-decoration:none;"; >
											<h2>'.$aArticle['article_titre'].'</h2>
										</a>
										<a href="'.$_CONST['URL_ACCUEIL'].'rubrique-'.strToUrl($aArticle['rubrique_titre']).'-'.$aArticle['article_id'].'-'.strToUrl($aArticle['article_titre']).'.html" style="text-decoration:none;"; >
											<img class="flottant" src="'.$strPathImg.'" alt="'.$aArticle['article_titre'].'" >
										</a> 
										<h3 class="accroche">'.$aArticle['article_soustitre'].'</h3>
										<a href="'.$_CONST['URL_ACCUEIL'].'rubrique-'.strToUrl($aArticle['rubrique_titre']).'-'.$aArticle['article_id'].'-'.strToUrl(aArticle).'.html" class="article_link">»</a>
										<div class="spacer"></div>
										<div class="separateur"></div>
									';
							}		
							
		echo '				</div>
		
							'; */
							/*if($iNbPage > 1){
								if($iRubriqueId ==  $_CONST["ID_SOUTIENT_SCOLAIRE"])
									$strLien = $oDb->queryItem("SELECT rubrique_titre FROM bor_rubrique WHERE rubrique_id = ".$_CONST['ID_SOUTIENT_SCOLAIRE']);
								else if ($iRubriqueId == $_CONST["ID_ACTUALITE"])
									$strLien = $oDb->queryItem("SELECT rubrique_titre FROM bor_rubrique WHERE rubrique_id = ".$_CONST['ID_ACTUALITE']);
						
								// echo '<li  '. ( (isset($_GET["id_rubrique"]) && $_GET["id_rubrique"] == $_CONST['ID_SOUTIENT_SCOLAIRE']) ? "class='active menubtn'" : "class='menubtn'" ).'><a href="'.$_CONST['URL_ACCUEIL'].'rubrique-'.strToUrl($strLien).'.html">'.$strLien.'</a></li>';
						
						
								echo '<div class="pagination"><ul>'; 
									//Bouton précédent
									if($iPage == 1)
										echo "<li class='inactive' rel='prev'>«</li>";
									else{
										if($iPage==2)
											echo "<li p='".($iPage-1)."' class='active' rel='prev'><a href='".$_CONST['URL_ACCUEIL'].'rubrique-'.strToUrl($strLien).".html'>«</a></li>";
										else
											echo "<li p='".($iPage-1)."' class='active' rel='prev'><a href='".$_CONST['URL_ACCUEIL'].'rubrique-'.strToUrl($strLien)."-".($iPage-1).".html'>«</a></li>";
									}
									//Boucle
									for($i=1; $i<= $iNbPage ; $i++){
										if($i==$iPage)
											echo "<li p='$i' style='z-index: 2;color: #ff6c00;cursor: default;background-color: #ffdabe;' class='active'>{$i}</li>"; 
										else if($i==1)
											echo "<li p='$i' class='active'><a href='".$_CONST['URL_ACCUEIL'].'rubrique-'.strToUrl($strLien).".html'>{$i}</a></li>"; 
										else
											echo "<li p='$i' class='active'><a href='".$_CONST['URL_ACCUEIL'].'rubrique-'.strToUrl($strLien)."-".($i).".html'>{$i}</a></li>"; 
											
										
									}
									
									//Bouton suivant
									if($iPage == $iNbPage)
										echo "<li class='inactive' rel='next'>»</li>";
									else
										echo "<li p='".($iPage+1)."' class='active' rel='next'><a href='".$_CONST['URL_ACCUEIL'].'rubrique-'.strToUrl($strLien)."-".($iPage+1).".html'>»</a></li>";
									
									
										
								echo '</ul></div>'; 
							}*/
	/*	echo '				
						</div>
						
						<aside class="right_column col-md-4">';
						//	include_once("./templates/sidebar/form_sidebar_abonnement.php");
						//	include_once("./templates/sidebar/form_sidebar_youtube.php");
						//	include_once("./templates/sidebar/form_sidebar_wengo.php");
						//	include_once("./templates/sidebar/form_sidebar_presse.php");
		echo '			</aside>
					</div>
				</div>
			';	*/
	}				
	else
	echo "<script>document.location.href='/';</script>";

?>