<div class="bss-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h6>
          <ol class="breadcrumb">
            <li id="a" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="/" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
            <li id="b" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="#" itemprop="url"> <span itemprop="title"><?php echo get_template_title($_GET['templates_id']); ?></span> </a></li>
          </ol>
        </h6>
      </div>
    </div>
  </div>
</div>
<div class="bss-section bloc-section-orange bss-ccm">
<div class="container">
	
		 <div class="row">
			<div class="center_column col-md-8">
			<section class="description">
			<h1><?php echo get_template_title($_GET['templates_id']); ?></h1>
			<div><?php echo get_template_data($_GET['templates_id']); ?></div>
			</section>
			</div>		
						
	
	
			
			<aside class="right_column col-md-4">
					<?php include_once("./templates/sidebar/form_sidebar_youtube.php");
					include_once("./templates/sidebar/form_sidebar_wengo.php");
					include_once("./templates/sidebar/form_sidebar_publicite.php");
					?>
			</aside>
		</div>
	</div>
	</div>
