<?php
if($_GET['type'] == "plan"){
	?>
<div class="bss-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h6>
          <ol class="breadcrumb">
            <li id="a" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="/" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
            <li id="b" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="#" itemprop="url"> <span itemprop="title"><?php echo get_template_title($_GET['templates_id']); ?></span> </a></li>
          </ol>
        </h6>
      </div>
    </div>
  </div>
</div>
<div class="bss-section bloc-section-orange bss-ccm">
	<div class="container">
      <div class="row">
        <div class="center_column col-md-12">
          <section class="description">
            <h1><?php echo get_template_title($_GET['templates_id']); ?></h1>
            <div></div>
          </section>
          <ul class="site-map-list">
            
			<?php
			$strSql = "SELECT niveau_id, niveau_titre FROM bor_niveau WHERE niveau_actif = 1 ORDER BY niveau_position"; 
			$aNiveaux = $oDb->queryTab($strSql);
			foreach($aNiveaux as $aNiveau){
			?>
			<ul>
              <li><?php echo $aNiveau['niveau_titre']; ?> </li>
              
				<?php
				$strSql = "SELECT *, cp.classe_page_id as ccc
				FROM bor_classe_page cp
				INNER JOIN bor_classe c ON (cp.classe_id = c.classe_id)
				INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id)
				WHERE cp.classe_page_actif = 1
				AND cn.niveau_id = ".$aNiveau['niveau_id']."
				GROUP BY classe_name
				ORDER BY cn_position ";
				$aClasses = $oDb->queryTab($strSql);
				foreach($aClasses as $iR => $aClasse){
				?>
				<ul>
                <li><a href="<?php echo $aClasse['classe_page_url'];?>"><?php echo $aClasse['classe_name']; ?></a></li>
				
				<?php
				//var_dump($aClasse['ccc']);
				$aMatiere = $oDb->queryTab("SELECT *
				FROM bor_matiere_page cmp, bor_matiere_classe_edito mc, bor_matiere m
				WHERE mc.matiere_id = cmp.matiere_id
				AND m.matiere_id = cmp.matiere_id
				AND mc.classe_id = ".$aClasse['ccc']."
				AND cmp.matiere_page_actif = 1");
				foreach($aMatiere as $matiere){
					$lien_classe_matiere = $aClasse['classe_page_url'].strToUrl($matiere['matiere_titre'])."/";
					?>
                <ul>
                  <li><a href="<?php echo $lien_classe_matiere; ?>"><?php echo $matiere['matiere_titre']; ?></a></li>
                  <ul>
				  <?php
					$strSql = "SELECT *
					FROM bor_chapitres c
					WHERE c.chapitre_classe_id = ".$aClasse['classe_id']."
					AND c.chapitre_matiere_id = ".$matiere['matiere_id']."
					AND c.chapitre_status = 1"; 
					$aChapitre = $oDb->queryTab($strSql);
					foreach($aChapitre as $chapitre){
						?>
						<li><a href="<?php echo strtolower($chapitre['chapitre_alias']);?>"><?php echo $chapitre['chapitre_h1'];?></a></li>
						<ul>
						<?php
						$strSql = "SELECT *
						FROM bor_notions n
						WHERE n.chapitre_id = ".$chapitre['chapitre_id']."
						AND n.notions_status = 1"; 
						$aNotions = $oDb->queryTab($strSql);
						foreach($aNotions as $notion){
							if(isset($notion['notions_url_seo']) && $notion['notions_url_seo'] != "")
								$lien_notion = strtolower($notion['notions_url_seo']);
							else{
								$lien_notion = "#";
							}
							?>
							<li><a href="<?php echo $lien_notion; ?>"><?php echo $notion['notions_h1'];?></a></li>
							<?php
						}
						?>
						</ul>
						<?php								
						}
					?>
                  </ul>
                  </ul>
				  <?php
					}
				?>
                </ul>
				<?php 
			}
			?>
                
         </ul>
		<?php
	}
	?>
              
              
            <li> Matière </li>
           <?php
					$strSql = "SELECT *
					FROM bor_matiere_page cp
					INNER JOIN bor_matiere c ON (cp.matiere_id = c.matiere_id)
					"; 
					$listeMatiere = $oDb->queryTab($strSql);
					foreach($listeMatiere as $mat)
					{
					?>
						<ul>
							<li><a href="<?php echo $mat['matiere_page_url']; ?>"><?php echo $mat['matiere_titre']; ?></a></li>
						</ul>
					<?php
					}
				?>
          </ul>
        </div>
      </div>
    </div>
	
	<!-- - -->
	
	

</div>

	<?php
}else{
?>
	
	<div class="container">
	
		 <div class="row">
			<div class="center_column col-md-8">
			<section class="description">
			<h1><?php echo get_template_title($_GET['templates_id']); ?></h1>
			<div><?php echo get_template_data($_GET['templates_id']); ?></div>
			</section>
			</div>
			<aside class="right_column col-md-4">
					<?php include_once("./templates/sidebar/form_sidebar_youtube.php");
					include_once("./templates/sidebar/form_sidebar_wengo.php");
					include_once("./templates/sidebar/form_sidebar_publicite.php");
					?>
			</aside>
		</div>
	</div>
	
<?php
}
?>