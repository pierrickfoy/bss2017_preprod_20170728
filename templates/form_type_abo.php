<div class="bss-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h6>
          <ol class="breadcrumb">
            <li id="a" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="/" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
            <li id="b" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="#" itemprop="url"> <span itemprop="title"><?php echo get_template_title($_GET['templates_id']); ?></span> </a></li>
          </ol>
        </h6>
      </div>
    </div>
  </div>
</div>

<div class="bss-section bloc-section-orange bss-ccm">
  <div class="container">
    <h1 id="tagline" class="h1"><?php echo get_template_title($_GET['templates_id']); ?></h1>
    <div class="row">
     <?php echo get_template_data($_GET['templates_id']); ?>
        <hr>
     <?php /*echo get_datablocks("pub");*/ ?>    
    </div>
      <div class="col-md-12">
        <hr>
        <p class="text-center"><a class="btn btn-md btn-primary" href="/comment-s-abonner.html">Je découvre toutes les formules d'abonnement<i class="icon-angle-right"></i></a></p>
      </div>
    </div>
  </div>
</div>