<?php
if(isset($_POST["classe"][0])){
	$_SESSION["products"]["enfants"][0]["classe"] = $_POST["classe"][0];
}
if(isset($_POST['direction'])){
	// direction vers fiche produit/tunnel commande avec infos.
	$_SESSION["products"]["enfants"][0]["classe"] = $_POST["classe"][0]; 
	$_SESSION["products"]["enfants"][0]["matiere"] =  $_POST["matiere"];
	
	if(isset($_POST['plus_dinfo']) && $_POST['plus_dinfo'] == 'plus_dinfo'){
		echo "<script>document.location.href='".$_CONST["URL2"].$_CONST['URL_ACCUEIL']."produit/abonnement-formule-".$_POST["direction"].".html';</script>";
	}else{
		if($_CONST['TYPE_ENVIRONNEMENT'] == 'dev' || $_CONST['TYPE_ENVIRONNEMENT'] == 'preprod'){
			echo "<script>document.location.href='".$_CONST["URL2"].$_CONST['URL_ACCUEIL']."commande/bordas/etape-1-formule-".$_POST["direction"].".html';</script>";
		}else{
			echo "<script>document.location.href='".$_CONST["HTTPS"].$_CONST['URL_ACCUEIL']."commande/bordas/etape-1-formule-".$_POST["direction"].".html';</script>";
		}
	}
	
	exit; 
}

$aCiblee  = $oDb->queryRow("SELECT ef.files_path, f.* , p.produit_prix , de.de_engagement , (produit_prix / de_engagement) as price 
							FROM  bor_produit p 
							INNER JOIN bor_formule f  ON (p.formule_id_yonix = f.formule_id_yonix) 
							INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix)
							INNER JOIN eco_files ef ON (ef.files_table_id = f.formule_id AND ef.files_field_name = 'formule_visuel')
							WHERE de_engagement = 12 
							AND f.formule_id = 1
							GROUP BY f.formule_id 
							ORDER BY formule_position ");

if(isset($_POST['classe_id']) && $_POST['classe_id'] != ""){
	$classeSelected = $_POST['classe_id'];
}else if(isset($_SESSION["products"]["enfants"][0]["classe"]) && $_SESSION["products"]["enfants"][0]["classe"] != ""){
	$classeSelected = $_SESSION["products"]["enfants"][0]["classe"];
}else if(isset($_POST["classe"][0]) && $_POST["classe"][0] != ""){
	$classeSelected = $_POST["classe"][0];
}else{
	$classeSelected = "";
}
?>
<div class="bss-section bloc-section-bleu bss-home-abonnement">
  <div class="container">
    <p id="tagline" class="h1">Nos tarifs de soutien scolaire en ligne</p>
    <h2 id="subtitle" class="h2">3 formules d'abonnement souples, à partir de
								<?php
												$prixCiblee = str_replace('.',',',round($aCiblee['price'],2));
												echo $prixCiblee;
								?>
								€ par mois</h2>
    <div class="row">
      <div class="col-sm-4">
        <div class="bloc-bss-formule bloc-bss-violet">
          <form method="POST" action="" class="form-horizontal form-cible">
            <input type="hidden" value="ciblee" name="direction"/>
			<div class="h2  text-center"><?php echo $aCiblee['formule_libelle_yonix'] ?></div>
            <div class="icone text-center">
				<img src="<?php echo $aCiblee['files_path']; ?>">
			</div>
            <p class="info  text-center">1 enfant / 1 matière</p>
            <div class="form-group">
              <div class="col-md-12">
			  <?php
				/*$ListClasses  = $oDb->queryTab("
								SELECT *
								FROM bor_classe c 
								INNER JOIN bor_matiere_classe mc ON (mc.classe_id = c.classe_id) 
								INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id) 
								INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 														
								WHERE mc.mc_dispo = 1
								GROUP BY c.classe_id
								ORDER BY n.niveau_position, cn.cn_position");*/
				$ListClasses  = $oDb->queryTab("SELECT c.*
								FROM bor_classe_page cp
								INNER JOIN bor_classe c ON (c.classe_id = cp.classe_id) 
								INNER JOIN bor_produit p ON (p.classe_id_yonix = c.classe_id_yonix) 
								INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) 
								INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id)
								INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 
								GROUP BY c.classe_id
								ORDER BY n.niveau_position, cn.cn_position
								"); 
			  ?>
                <select name="classe[0]" class="form-control classe" id="cible-classe" placeholder="Classe" >
					<option value="Classe" selected="selected">Classe</option>
					<?php
					/*if($ListClasses){ 
						foreach($ListClasses as $classe){
							echo '<option value="'.$classe['classe_id'].'" '.((( isset($_SESSION["products"]["enfants"][0]["classe"]) && $_SESSION["products"]["enfants"][0]["classe"] == $classe['classe_id']) || (isset($_POST["classe"][0]) && $_POST["classe"][0] == $classe['classe_id'])) ? 'selected' : '' ) .'>'.$classe['classe_name'].'</option>'; 
						}  
					}*/
					if($ListClasses){ 
							foreach($ListClasses as $classe){
								echo '<option value="'.$classe['classe_id'].'" '.
								/*(( isset($_SESSION["products"]["enfants"][0]["classe"]) && $_SESSION["products"]["enfants"][0]["classe"] == $classe['classe_id']) ? 'selected' : '' ) */
								(((isset($classeSelected)
								&& $classeSelected == $classe['classe_id'])
								)
								? 'selected' : '' )
								.'>'.$classe['classe_name'].'</option>'; 
							}  
						}
					?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12">
                <select  name="matiere" class="form-control" id="matiere" placeholder="Matiere" >
					<option value="Matière" selected="selected">Matière</option>
                </select>
              </div>
            </div>
            
            <p class="tarif  text-center"><span>À partir de</span>
				<?php
					$prix = str_replace('.',',',round($aCiblee['price'],2));
					$prix_explode = explode(',',$prix); echo $prix_explode[0].'<sup>,'.$prix_explode[1];
				?>
            	€<span>/mois</span></p>
			<p class="bouton  text-center">
              <button type="submit" class="btn  btn-fw btn-ciblee ">J'abonne mon enfant<i class="icon-angle-right"></i> </button>
            </p>
           <?php /*<p class="lead">Avec cette formule, votre enfant se concentre sur la matière dans laquelle il est en difficulté ou souhaite progresser.</p>*/ ?>
			<p class="lead"><?php echo $aCiblee['formule_accroche_yonix']; ?></p>
            
            <?php /*<p class="bouton  text-center"><button type="submit" name="plus_dinfo" value="plus_dinfo" class="btn btn-ciblee-outline btn-fw">+ d'informations</button></p>*/ ?>
          </form>
        </div>
      </div>
								
<?php 
$aReussite  = $oDb->queryRow("SELECT ef.files_path,   f.* , p.produit_prix , de.de_engagement , (produit_prix / de_engagement) as price 
								FROM   bor_produit p 
								INNER JOIN bor_formule f  ON (p.formule_id_yonix = f.formule_id_yonix) 
								INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix)
								INNER JOIN eco_files ef ON (ef.files_table_id = f.formule_id AND ef.files_field_name = 'formule_visuel')
								WHERE de_engagement = 12 
								AND f.formule_id = 2
								GROUP BY f.formule_id 
								ORDER BY formule_position "); 
?>
								
      <div class="col-sm-4">
        <div class="bloc-bss-formule bloc-bss-vert-deau">
           <form method="POST" action="" class="form-horizontal form-cible">
			<input type="hidden" value="reussite" name="direction"/>
            <div class="h2  text-center"><?php echo $aReussite['formule_libelle_yonix'] ?></div>
            <div class="icone text-center">
				<img src="<?php echo $aReussite['files_path']; ?>">
			</div>
            <p class="info  text-center">1 enfant / Multi-matières</p>
            <div class="form-group">
             <div class="col-md-12">
                <select  name="classe[0]" class="form-control" id="cible-classe" placeholder="Classe" >
                  
                  <option value="Classe" selected="selected">Classe</option>
					<?php
						/*$ListClasses2  = $oDb->queryTab("SELECT c.* FROM bor_classe c 
									INNER JOIN bor_matiere_classe mc ON (mc.classe_id = c.classe_id) 
									INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id) 
									INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 														
								WHERE mc.mc_dispo = 1
								GROUP BY c.classe_id
								ORDER BY n.niveau_position, cn.cn_position");*/
						$ListClasses2  = $oDb->queryTab("SELECT c.*
								FROM bor_classe_page cp
								INNER JOIN bor_classe c ON (c.classe_id = cp.classe_id) 
								INNER JOIN bor_produit p ON (p.classe_id_yonix = c.classe_id_yonix) 
								INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) 
								INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id)
								INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id)
								GROUP BY c.classe_id
								ORDER BY n.niveau_position, cn.cn_position
								"); 
						
						/*if($ListClasses2){
							foreach($ListClasses2 as $classe){
								echo '<option value="'.$classe['classe_id'].'" '.((( isset($_SESSION["products"]["enfants"][0]["classe"]) && $_SESSION["products"]["enfants"][0]["classe"] == $classe['classe_id']) || (isset($_POST["classe"][0]) && $_POST["classe"][0] == $classe['classe_id'])) ? 'selected' : '' ) .'>'.$classe['classe_name'].'</option>'; 
							}
						}*/
						if($ListClasses){ 
							foreach($ListClasses as $classe){
								echo '<option value="'.$classe['classe_id'].'" '.
								/*(( isset($_SESSION["products"]["enfants"][0]["classe"]) && $_SESSION["products"]["enfants"][0]["classe"] == $classe['classe_id']) ? 'selected' : '' ) */
								(((isset($classeSelected)
								&& $classeSelected == $classe['classe_id'])
								)
								? 'selected' : '' )
								.'>'.$classe['classe_name'].'</option>'; 
							}  
						}
					?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12">
                <label for="reussite-matiere" class="col-md-6 control-label hidden-xs hidden-sm hidden-md hidden-lg">&nbsp;</label>
                <p class="form-control-static text-center">Multi-matières</p>
              </div>
            </div>
            <p class="tarif  text-center"><span>À partir de </span>
			<?php
				$prixReussite = str_replace('.',',',round($aReussite['price'],2));
				$prix_explode = explode(',',$prixReussite); echo $prix_explode[0].'<sup>,'.$prix_explode[1];
			?>
				€<span>/mois</span></sup></p>
			 <p class="bouton  text-center">
              <button type="submit" class="btn btn-fw btn-reussite ">J'abonne mon enfant<i class="icon-angle-right"></i> </button>
            </p>
            <?php /* <p class="lead">Avec cette formule, votre enfant accède à toutes les matières de sa classe, et s'entraîne selon son rythme et ses besoins.</p> */ ?>
            <p class="lead"><?php echo $aReussite['formule_accroche_yonix']; ?></p>
           
            <?php /*<p class="bouton  text-center"><button type="submit" name="plus_dinfo" value="plus_dinfo" class="btn btn-reussite-outline btn-fw">+ d'informations</button></p>*/ ?>

          </form>
        </div>
      </div>
								
<?php 
$aTribu  = $oDb->queryRow("SELECT ef.files_path,  f.* , p.produit_prix , de.de_engagement , (produit_prix / de_engagement) as price 
							FROM   bor_produit p 
							INNER JOIN bor_formule f  ON (p.formule_id_yonix = f.formule_id_yonix) 
							INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix)
							INNER JOIN eco_files ef ON (ef.files_table_id = f.formule_id AND ef.files_field_name = 'formule_visuel')
							WHERE de_engagement = 12 
							AND f.formule_id = 3
							GROUP BY f.formule_id 
							ORDER BY formule_position "); 
?>
      <div class="col-sm-4">
        <div class="bloc-bss-formule bloc-bss-vert">
           <form method="POST" action="" class="form-horizontal form-cible">
				<input type="hidden" value="tribu" name="direction"/>
				<div class="h2  text-center"><?php echo $aTribu['formule_libelle_yonix'] ?></div>
            <div class="icone text-center">
				<img src="<?php echo $aTribu['files_path']; ?>">
			</div>
            <p class="info  text-center">2 à 5 enfants / Multi-matières</p>
            <div class="form-group">
              <label for="classe" class="col-md-6 control-label tribu-label">Un de mes enfants est en</label>
              <div class="col-md-6">
                <select  name="classe[0]" class="form-control" id="tribu-classe" placeholder="Classe" >
					<option value="Classe" selected="selected">Classe</option>
					<?php
						/*$ListClasses3  = $oDb->queryTab("SELECT c.* FROM bor_classe c 
									INNER JOIN bor_matiere_classe mc ON (mc.classe_id = c.classe_id) 
									INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id) 
									INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 														
								WHERE mc.mc_dispo = 1
								GROUP BY c.classe_id
								ORDER BY n.niveau_position, cn.cn_position");*/
						$ListClasses3  = $oDb->queryTab("SELECT c.*
								FROM bor_classe_page cp
								INNER JOIN bor_classe c ON (c.classe_id = cp.classe_id) 
								INNER JOIN bor_produit p ON (p.classe_id_yonix = c.classe_id_yonix) 
								INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix) 
								INNER JOIN bor_classe_niveau cn ON (cn.classe_id = c.classe_id)
								INNER JOIN bor_niveau n ON (n.niveau_id = cn.niveau_id) 	
								GROUP BY c.classe_id
								ORDER BY n.niveau_position, cn.cn_position
								");
						
						/*if($ListClasses3){ 
							foreach($ListClasses3 as $classe){
								echo '<option value="'.$classe['classe_id'].'" '.((( isset($_SESSION["products"]["enfants"][0]["classe"]) && $_SESSION["products"]["enfants"][0]["classe"] == $classe['classe_id']) || (isset($_POST["classe"][0]) && $_POST["classe"][0] == $classe['classe_id'])) ? 'selected' : '' ) .'>'.$classe['classe_name'].'</option>'; 
							}  
						}*/
						if($ListClasses){ 
							foreach($ListClasses as $classe){
								echo '<option value="'.$classe['classe_id'].'" '.
								/*(( isset($_SESSION["products"]["enfants"][0]["classe"]) && $_SESSION["products"]["enfants"][0]["classe"] == $classe['classe_id']) ? 'selected' : '' ) */
								(((isset($classeSelected)
								&& $classeSelected == $classe['classe_id'])
								)
								? 'selected' : '' )
								.'>'.$classe['classe_name'].'</option>'; 
							}  
						}
					?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12">
                <label for="tribu-matiere" class="col-md-6 control-label hidden-xs hidden-sm hidden-md hidden-lg">&nbsp;</label>
                <p class="form-control-static text-center">Multi-matières</p>
              </div>
            </div>
            <p class="tarif  text-center"><span>À partir de </span>
			<?php
				$prixTribu = str_replace('.',',',round($aTribu['price'],2));
				$prix_explode = explode(',',$prixTribu); echo $prix_explode[0].'<sup>,'.$prix_explode[1];
			?>
			€<span>/mois</span></p>
			 <p class="bouton  text-center">
              <button type="submit" class="btn btn-fw  btn-tribu ">J’abonne mes enfants<i class="icon-angle-right"></i> </button>
            </p>
            <?php /*  <p class="lead">Cette formule permet d'abonner plusieurs enfants à un tarif avantageux. Chaque enfant peut accéder à toutes les matières.</p>*/ ?>
			<p class="lead"><?php echo $aTribu['formule_accroche_yonix']; ?></p>
           
            <?php /*<p class="bouton  text-center"><button type="submit" name="plus_dinfo" value="plus_dinfo" class="btn btn-tribu-outline btn-fw">+ d'informations</button></p>*/ ?>

          </form>
        </div>
      </div>
    </div>
    <?php echo get_template_data(); ?>
  </div>
</div>
<?php
	if(isset($_POST["matiere_id"]) && $_POST["matiere_id"] == 11 || $_POST["matiere_id"] == 12)
		$matiere_selected = 4;
	else if(isset($_POST["matiere_id"]) && $_POST["matiere_id"] == 13 || $_POST["matiere_id"] == 14)
		$matiere_selected = 3;
	else
		$matiere_selected = $_POST["matiere_id"];
?>
<script type="text/javascript">
				// chargement des matières en fonction de la classe
	jQuery(document).ready(function(){
		
		
	function update_matiere() {
   jQuery.ajax({type:"POST", data: jQuery('.classe').serialize(), url: "/templates/ajaxsearch.php" ,dataType: 'json',	success: function(json) {
			jQuery('#matiere option').remove(); 
					jQuery('#matiere').append('<option value="">Choisir une matière</option>');
					jQuery.each(json, function(index, value) { 
						jQuery('#matiere').append('<option value="'+ json[index]['matiere_id'] +'">'+ json[index]['mc_titre'] +'</option>');
					});
					$('#matiere option[value="<?php if(isset($matiere_selected) && $matiere_selected != "" ) echo $matiere_selected; ?>"]').prop('selected', true);
				},
				error: function(){
					jQuery('#matiere').html('');
				}
			});
			 return false;
}
				// au chargement, si classe renseignée
		update_matiere();
		jQuery('.classe').each(function(){
			jQuery.ajax({type:"POST", data:jQuery(this).serialize(), url: "/templates/ajax/bloc_matiere.php" ,dataType: 'script'	});
		});
		
		// au changement de classe: changement des matières
		jQuery('.classe').change(function () {
				update_matiere();
				
			/*jQuery.ajax({type:"POST", data: jQuery(this).serialize(), url: "/templates/ajaxsearch.php" ,dataType: 'json',	success: function(json) {
					jQuery('#matiere option').remove(); 
					jQuery('#matiere').append('<option value="">Choisir une matière</option>');
					jQuery.each(json, function(index, value) { 
						jQuery('#matiere').append('<option value="'+ json[index]['matiere_id'] +'">'+ json[index]['mc_titre'] +'</option>');
					});
				},
				error: function(){
					jQuery('#matiere').html('');
				}
			});
			// update_popup(0);
			jQuery.ajax({type:"POST", data: jQuery(this).serialize(), url: "/templates/ajax/bloc_matiere.php" ,dataType: 'script'	});
			 return false;*/
		});	
		
	});
	</script>