
<div class="bss-section bloc-section-gris-bleu bss-autres-produits">
  <div class="container">
    <p  class="h1">Les autres offres disponibles :</p>
    <div class="row">
	<?php
	$strSql = "SELECT *
	FROM bor_formule f, eco_files ef
	WHERE f.formule_classe != '$iProduit'
	AND ef.files_table_id = f.formule_id
	AND ef.files_field_name = 'formule_visuel'"; 
	$aFormule = $oDb->queryTab($strSql);
	//var_dump($aFormule);
	foreach($aFormule as $key=>$formule){
	?>
      <div class="col-md-3">
		<p class=" h3 text-center"><?php echo $formule['formule_libelle']; ?></p>
        <p class="text-center"><a 
		
		<?php
				if($iProduit == "ciblee"){
					if($formule['formule_classe'] == "ciblee"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Ciblée', 'Information', 'Formule ciblée - Bloc Autres offres');" <?php
					}else if($formule['formule_classe'] == "reussite"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Ciblée', 'Information', 'Formule réussite - Bloc Autres offres');" <?php
					}else if($formule['formule_classe'] == "tribu"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Ciblée', 'Information', 'Formule tribu - Bloc Autres offres');" <?php
					}
					
				}else if($iProduit == "reussite"){
					if($formule['formule_classe'] == "ciblee"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Réussite', 'Information', 'Formule ciblée - Bloc Autres offres');" <?php
					}else if($formule['formule_classe'] == "reussite"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Réussite', 'Information', 'Formule réussite - Bloc Autres offres');" <?php
					}else if($formule['formule_classe'] == "tribu"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Réussite', 'Information', 'Formule tribu - Bloc Autres offres');" <?php
					}
					
					
				}else if($iProduit == "tribu"){
					if($formule['formule_classe'] == "ciblee"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Tribu', 'Information', 'Formule ciblée - Bloc Autres offres');" <?php
					}else if($formule['formule_classe'] == "reussite"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Tribu', 'Information', 'Formule réussite - Bloc Autres offres');" <?php
					}else if($formule['formule_classe'] == "tribu"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Tribu', 'Information', 'Formule tribu - Bloc Autres offres');" <?php
					}
					
					
				}
		?> 
		 class="image-autres-produits" href="/produit/abonnement-formule-<?php echo $formule['formule_classe']; ?>.html"><img src="/<?php echo $formule['files_path']; ?>" class="img-responsive"> </a></p>
        <p class="text-center"><a 
		<?php
				if($iProduit == "ciblee"){
					if($formule['formule_classe'] == "ciblee"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Ciblée', 'Information', 'Formule ciblée - Bloc Autres offres');" <?php
					}else if($formule['formule_classe'] == "reussite"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Ciblée', 'Information', 'Formule réussite - Bloc Autres offres');" <?php
					}else if($formule['formule_classe'] == "tribu"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Ciblée', 'Information', 'Formule tribu - Bloc Autres offres');" <?php
					}
					
				}else if($iProduit == "reussite"){
					if($formule['formule_classe'] == "ciblee"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Réussite', 'Information', 'Formule ciblée - Bloc Autres offres');" <?php
					}else if($formule['formule_classe'] == "reussite"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Réussite', 'Information', 'Formule réussite - Bloc Autres offres');" <?php
					}else if($formule['formule_classe'] == "tribu"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Réussite', 'Information', 'Formule tribu - Bloc Autres offres');" <?php
					}
					
					
				}else if($iProduit == "tribu"){
					if($formule['formule_classe'] == "ciblee"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Tribu', 'Information', 'Formule ciblée - Bloc Autres offres');" <?php
					}else if($formule['formule_classe'] == "reussite"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Tribu', 'Information', 'Formule réussite - Bloc Autres offres');" <?php
					}else if($formule['formule_classe'] == "tribu"){
						?> onclick="ga('send', 'event', 'Fiche produit F.Tribu', 'Information', 'Formule tribu - Bloc Autres offres');" <?php
					}
					
					
				}
		?> 
		 class="lien-autres-produits" href="/produit/abonnement-formule-<?php echo $formule['formule_classe']; ?>.html"><?php echo $formule['formule_soustitre']; ?></a></p>
      </div>
    <?php
	}
	?>
    </div>
  </div>
</div>
