<!--NOUVELLE SECTION AVEC CAROUSEL ET BLOC-->

<div class="container classe-carousel">
  <div class="row">
    <div class=" col-md-8  ">
	<?php
	if(isset($aSlider) && $aSlider != "" && !empty($aSlider)){
	?>
      <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
		<?php
		if(count($aSlider) > 1){
			
		?>
        <ol class="carousel-indicators">
		 <?php
		  foreach($aSlider as $key => $slider){
			?>
          <li data-target="#myCarousel" data-slide-to="<?php echo $key; if($key == 0) echo 'class="active"'; ?>" ></li>
		  <?php
		  }
		  ?>
        </ol>
		<?php
		}
		?>

        <div class="carousel-inner" role="listbox">
          <!--LOOP ITEM SLIDER-->
		  <?php
		  foreach($aSlider as $key => $slider){
			  $titre= "";
			  // si vide, affiche par défaut
			  $soustitre = "Conforme aux programmes – Rédigé par des enseignants";
			  if(isset($slider['titre']) && $slider['titre'] != "")
				  $titre = $slider['titre'];
			  if(isset($slider['sous_titre']) && $slider['sous_titre'] != "")
				  $soustitre = $slider['sous_titre'];
			?>
			 <div class="item  <?php if($key == 0) echo 'active'; ?> "  data-slide-number="<?php echo $key;?>"  > 
			<a href="<?php echo $sliderLien; ?>" <?php echo $sliderLienBlank;?> > <img src="/<?php echo $slider['files_path']; ?>" class="img-responsive" border="0" alt="<?php echo $titre; ?>" title="<?php echo $titre; ?>">
            <div class="container">
              <div class="carousel-caption">
			  <?php 
				  echo '<h2>'.$titre.'</h2>';
				  echo '<p>'.$soustitre.'</p>';
			  ?>
              </div>
            </div>
            </a> 
         </div>

			<?php
		  }
		  ?>

            <!--END LOOP ITEM SLIDER-->
        </div>
		<?php
		if(count($aSlider) > 1){
			
		?>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <i class="icon-angle-left glyphicon-chevron-right"></i> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <i class="icon-angle-right glyphicon-chevron-right"></i> </a>
		<?php
		}
		?>
	</div>
	
	<?php
	}
	?>

    </div>
    <div class=" col-md-4  ">
      <div class="well prix-appel ">
        <p class="h3"><span class="partir">A&nbsp;partir&nbsp;de</span><span class="prix">3,99&nbsp;€</span>&nbsp;<span class="mois">/&nbsp;mois</span></p>
        <!--<a class="btn btn-lg btn-primary btn-fw btn-abonne" href="#">Je m'abonne &gt;</a>-->
		<form action="/comment-s-abonner.html" method="post">
				<button type="submit" class="btn btn-lg btn-abonne btn-primary btn-fw btn-vertical" >Je m'abonne &gt;</button>
				<input type="hidden" name="classe_id" value="<?php echo $aClasse['classe_id']; ?>">
				<input type="hidden" name="matiere_id" value="<?php echo $aMatiere['matiere_id']; ?>">
		</form>
		</div>

		<?php if(isset($testerLien) && $testerLien != ""){?>
			<a class="btn btn-lg  btn-link  btn-fw btn-test" href="<?php echo $testerLien;?>" <?php echo $testerLienBlank;?>>Tester une leçon<i class="icon-angle-right"></i></a>
		<?php
		}?>

    </div>
  </div>
</div>
<!--FIN NOUVELLE SECTION AVEC CAROUSEL ET BLOC-->