<div class="bss-section bloc-section-bleu bss-chapitre">
  <div class="container">
    <div class="row">
      <div class=" text-center">
        <h1 id="tagline" class="h1"><?php echo $aChapitre['chapitre_h1'];?></h1>
      </div>
      <div class="col-sm-12 col-md-12 ">
		<!-- Accroche -->
        <?php echo $aChapitre['chapitre_accroche']; ?>
		<!-- /Accroche -->
		<!-- Contenu -->
        <?php echo $aChapitre['chapitre_content']; ?>
		<!-- /Contenu -->
      </div>
		<!-- Pub -->
	  <?php							
		/*include_once("./templates/sidebar/form_sidebar_wengo.php");*/
	  ?>
		<!-- /Pub -->
    </div>
  </div>
</div>
