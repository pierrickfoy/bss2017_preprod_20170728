<div class="bss-section bloc-section-orange bss-matiere">
  <div class="container">
    <p id="tagline" class="h1">Les classes disponibles</p>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-demo">
		   <?php
			/*$strSql = "SELECT *
			FROM bor_classe_page cmp, bor_matiere_classe mc, bor_classe m, eco_files ef
			WHERE mc.classe_id = cmp.classe_page_id
			AND m.classe_id = cmp.classe_page_id
			AND mc.matiere_id = ".$aMatiere['matiere_id']."
			AND ef.files_table_id = m.classe_id
			AND ef.files_field_name = 'classe_image'";*/
			$strSql = "SELECT *
			FROM bor_classe_page cmp, bor_classe m, bor_matiere_classe_edito mc
			WHERE mc.classe_id = cmp.classe_page_id
			AND m.classe_id = cmp.classe_id
			AND mc.matiere_id = ".$aMatiere['matiere_id']."
			AND cmp.classe_page_actif = 1";
			$aClasse = $oDb->queryTab($strSql);
			foreach($aClasse as $key=>$classe){
				$lien_classe_matiere = $classe['classe_page_url'].strToUrl($aMatiere['matiere_titre'])."/";
			?>
            <div class="item-matiere " role="tab" id="">
              <div class="bloc-matiere-item">
                
                <div class="col-xs-11 col-sm-11 col-md-5 no-padding">
                
                  <h2 class=""><a href="<?php echo $lien_classe_matiere; ?>">Soutien scolaire en ligne en <?php echo $aMatiere['matiere_titre']; ?> en <?php echo $classe['classe_name']; ?> </a></h2>
                </div>
				
				<ul class="list-inline col-sm-4 col-md-5 no-padding hidden-xs hidden-sm">
					<?php
					$liste_type = explode(',',$classe['mc_page_type']);
					foreach($liste_type as $type){
						if($type == 1) echo '<li class="type-1"><i class="icon-book"></i><span>Cours</span></li>';
						else if($type == 2) echo '<li class="type-2"><i class="icon-docs"></i><span>Exercices</span></li>';
						else if($type == 3) echo '<li class="type-3"><i class="icon-doc-text"></i><span>Corrigés</span></li>';
						else if($type == 4) echo '<li class="type-4"><i class="icon-coverflow"></i><span>Animations</span></li>';
						else if($type == 5) echo '<li class="type-5"><i class="icon-movie"></i><span>Vidéos</span></li>';
						else if($type == 6) echo '<li class="type-6"><i class="icon-stopwatch"></i><span>Examen</span></li>';
					}
					?>
				</ul>
                
                
              </div>
            </div>
			
			<?php
			}
			?>
          </div>
          <hr>
		  <?php if($aMatiere['matiere_page_URL_CGS'] != ""){?>
          <p id="tagline" class="h1">Tester Bordas Soutien scolaire</p>
          <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="<?php echo $aMatiere['matiere_page_URL_CGS']; ?>"></iframe>
          </div>
		  <?php
		  }
		  ?>
        </div>
      </div>
    </div>
  </div>
</div>
