<div class="bss-section  bloc-section-bleu bss-comparez-tarif" id="compare-tarif">
  <div class="container">
  <style>#tagcomparer {
    display: block;
    position: relative;
    top: -175px;
    visibility: hidden;
}</style>
<a class="anchor" id="tagcomparer"></a>
    <p class="h1">Aide scolaire en ligne : Comparez tous les prix</p>
    
    <?php
    $aPriceTab  = $oDb->queryTab(" SELECT f.formule_id, de_engagement, round(produit_prix / de_engagement, 2) as price "
            . "                         FROM bor_produit p "
            . "                         INNER JOIN bor_formule f ON (p.formule_id_yonix = f.formule_id_yonix) "
            . "                         INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix) "
            . "                         GROUP BY f.formule_id, de_engagement "
            . "                         ORDER BY formule_position"); 
    foreach($aPriceTab as $Tmp) {
        $aPrices[$Tmp["formule_id"]][$Tmp["de_engagement"]] = str_replace('.', ',', $Tmp["price"]);
    }
    ?>
    <div class="">
      <div class="col-xs-12 col-sm-offset-4 col-sm-8 text-center">
        <div class="row">
          <div class="col-xs-4 my_planHeader my_plan1">
            <div class="my_planTitle"><a href="<?php echo $_CONST["HTTPS"].$_CONST['URL_ACCUEIL']."commande/bordas/etape-1-formule-ciblee.html"; ?>">Formule Ciblée >></a></div>
         
           
          </div>
          <div class="col-xs-4 my_planHeader my_plan2">
            <div class="my_planTitle"><a href="<?php echo $_CONST["HTTPS"].$_CONST['URL_ACCUEIL']."commande/bordas/etape-1-formule-reussite.html"; ?>">Formule Réussite >></a></div>
            
          </div>
          <div class="col-xs-4 my_planHeader my_plan3">
            <div class="my_planTitle"><a href="<?php echo $_CONST["HTTPS"].$_CONST['URL_ACCUEIL']."commande/bordas/etape-1-formule-tribu.html"; ?>">Formule Tribu >></a></div>
            
          </div>
        </div>
      </div>
    </div>
    <div class=" my_featureRow">
      <div class="col-xs-12 col-sm-4 my_feature"> Abonnement mensuel</div>
      <div class="col-xs-12 col-sm-8 text-center">
        <div class="row">
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan1">  <strong><?php echo $aPrices[1][1];?> €</strong>/mois </div>
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan2">  <strong><?php echo $aPrices[2][1];?> €</strong>/mois </div>
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan3">  <strong><?php echo $aPrices[3][1];?> €</strong>/mois </div>
        </div>
      </div>
    </div>
    <div class=" my_featureRow">
      <div class="col-xs-12 col-sm-4 my_feature"> Abonnement trimestriel</div>
      <div class="col-xs-12 col-sm-8 text-center">
        <div class="row">
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan1">  <strong><?php echo $aPrices[1][3];?> €</strong>/mois </div>
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan2">  <strong><?php echo $aPrices[2][3];?> €</strong>/mois </div>
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan3">  <strong><?php echo $aPrices[3][3];?> €</strong>/mois </div>
        </div>
      </div>
    </div>
    <div class=" my_featureRow">
      <div class="col-xs-12 col-sm-4 my_feature"> Abonnement annuel</div>
      <div class="col-xs-12 col-sm-8 text-center">
        <div class="row">
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan1"> <strong><?php echo $aPrices[1][12];?> €</strong>/mois </div>
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan2"> <strong><?php echo $aPrices[2][12];?> €</strong>/mois </div>
          <div class="col-xs-4 col-sm-4 my_planFeature my_plan3"> <strong><?php echo $aPrices[3][12];?> €</strong>/mois </div>
        </div>
      </div>
    </div>
  </div>
</div>
