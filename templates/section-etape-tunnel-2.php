<div class="bss-section bloc-section-gris bss-tunnel">
  <div class="container">
   <form action="tunnel-2-adresse.php"  class="form-horizontal clearfix" id="creation-compte" title="creation-compte"> <div class="row">
      <div class="col-md-8">
        <h1 class="h1">Informations personnelles</h1>
        <h2 >Vous avez déjà un compte ?</h2>
        <div class="bss-connexion bloc-bss-1">
         
            <h3>Identifiez-vous</h3>
            <div class="form-group">
              <label for="identifiant" class="sr-only">Identifiant</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" id="identifiant" form="creation-compte" placeholder="Identifiant">
              </div>
              <label for="motdepasse" class="sr-only">Mot de passe</label>
              <div class="col-sm-6">
                <input type="password" class="form-control" id="motdepasse" form="creation-compte" placeholder="Mot de passe">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6"> <a href="mot-de-passe-oublie" class="btn btn-link">Mot de passe oublié ? </a></div>
              <div class="col-sm-6">
                <button type="submit" class="btn btn-primary btn-fw">Je m'identifie</button>
              </div>
            </div>
          
        </div>
        <hr>
        <h2>Vous n'avez pas de compte ?</h2>
        <h3>Créez le maintenant !</h3>
        <div class="bss-creation bloc-bss-1">
          
            <h4>Mes coordonnées</h4>
            <div class="form-group ">
              <label for="numero-client" class="col-sm-6 control-label">Numéro client</label>
              <div class="col-sm-6">
                <input name="numero-client" type="text" class="form-control" id="numero-client" form="creation-compte" placeholder="Numéro client" title="Numero client">
              </div>
            </div>
            <div class="form-group ">
              <label for="civilite" class="col-sm-6 control-label"><span class="obligatoire">*</span> Civilité</label>
              <div class="col-sm-6">
                <label class="radio-inline">
                  <input name="civilite" type="radio" id="Mme" form="creation-compte" value="Mme">
                  Mme </label>
                <label class="radio-inline">
                  <input name="civilite" type="radio" id="Mr" form="creation-compte" value="Mr">
                  Mr </label>
              </div>
            </div>
            <div class="form-group has-error" >
              <label for="votre-nom" class="col-sm-6 control-label"><span class="obligatoire">*</span> Votre nom</label>
              <div class="col-sm-6">
                <input name="votre-nom" type="text" class="form-control" id="votre-nom" form="creation-compte" placeholder="Votre nom" title="Votre nom">
              </div>
            </div>
            <div class="form-group">
              <label for="votre-prenom" class="col-sm-6 control-label"><span class="obligatoire">*</span> Votre prénom</label>
              <div class="col-sm-6">
                <input name="votre-prenom" type="text" class="form-control" id="votre-prenom" form="creation-compte" placeholder="Votre prénom" title="Votre prénom">
              </div>
            </div>
            <div class="form-group">
              <label for="votre-email" class="col-sm-6 control-label"><span class="obligatoire">*</span> Email</label>
              <div class="col-sm-6">
                <input name="votre-email" type="text" class="form-control" id="votre-email" form="creation-compte" placeholder="Votre email" title="Votre email">
              </div>
            </div>
            <div class="form-group">
              <label for="confirmation-email" class="col-sm-6 control-label"><span class="obligatoire">*</span> Confirmation email</label>
              <div class="col-sm-6">
                <input name="confirmation-email" type="text" class="form-control" id="confirmation-email" form="creation-compte" placeholder="Confirmation email" title="Confirmation email">
              </div>
            </div>
            <hr>
            <h4>Je choisis mes identifiants</h4>
            <div class="form-group">
              <label for="creation-identifiant" class="col-sm-6 control-label"><span class="obligatoire">*</span> Identifiant</label>
              <div class="col-sm-6">
                <input name="creation-identifiant" type="text" class="form-control" id="creation-identifiant" form="creation-compte" placeholder="Identifiant" title="Identifiant">
              </div>
            </div>
            <div class="form-group">
              <label for="creation-mot-de-passe" class="col-sm-6 control-label"><span class="obligatoire">*</span> Mot de passe</label>
              <div class="col-sm-6">
                <input name="creation-mot-de-passe" type="password" class="form-control" id="creation-mot-de-passe" form="creation-compte" placeholder="Mot de passe" title="Mot de passe">
              </div>
            </div>
            <div class="form-group">
              <label for="confirm-mot-de-passe" class="col-sm-6 control-label"><span class="obligatoire">*</span> Confirmation du mot de passe</label>
              <div class="col-sm-6">
                <input name="confirm-mot-de-passe" type="password" class="form-control" id="confirm-mot-de-passe" form="creation-compte" placeholder="Confirmation du mot de passe" title="Confirmation du mot de passe">
              </div>
            </div>
            <hr>
            
           
            <div class="checkbox">
              <label>
                <input type="checkbox" form="creation-compte" title="recevoir des informations de Bordas" value="recevoir-info">
               Je souhaite recevoir des informations de Bordas. </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" form="creation-compte" title="recevoir des informations des partenaires de Bordas" value="recevoir-partenaire">
               J’accepte de recevoir des informations des partenaires de Bordas. </label>
            </div>
            <hr>
            <p class="well legende"><span class="obligatoire">* <strong>champ obligatoire</strong></span><br>
Conformément à la loi Informatique et Libertés du 6 janvier 1978 modifiée en 2004, vous disposez d’un droit d’accès, de rectification ou d’opposition aux données personnelles vous concernant, à la Relation client BORDAS : <a href="mailto:soutien-scolaire@bordas.tm.fr">soutien-scolaire@bordas.tm.fr</a>, <?php echo $_CONST['RC_TEL']; ?>. </p>
           
            <div class="form-group">
              <div class="col-sm-6"> &nbsp;</div>
              <div class="col-sm-6">
                <button type="submit" class="btn btn-primary btn-fw">Je crée mon compte</button>
              </div>
            </div>
           
          
        </div>
      </div>
      <div class="col-md-4 bss-step-sidebar">
        <div class="bloc-bss-step-sidebar bloc-bss-gris">
          <div class="bloc-bss-produit-titre">Ma Formule</div>
          <ul>
            <li>Formule Tribu</li>
            <li>2 classes / Multi-matières</li>
          </ul>
          <div class="text-right"><a href="tunnel-1.php" class="btn btn-sm btn-default " type="button">Changer de formule</a></div>
        </div>
       
          <div class="bloc-bss-step-sidebar bloc-bss-gris">
            <div class="bloc-bss-produit-titre">Prix de mon abonnement :</div>
            <div class="bloc-bss-produit-stitre">Pour une durée de 1 an :</div>
            <div class="bloc-bss-produit-prix"><span>A partir de </span>99<sup>,99 €</sup></div>
            <div class="legende text-center">(soit 99<sup>,99 €</sup> pour l'année)</div>
            <div class="form-group">
              <label for="modification-duree" class="sr-only">Modifier la durée</label>
              <select name="modification-duree" class="form-control" id="modification-duree" form="creation-compte" title="modification-duree">
                <option>Modifier la durée</option>
                <option>Durée 1 mois</option>
                <option>Durée 3 mois</option>
              </select>
            </div>
          </div>
          <div class="bloc-bss-step-sidebar bloc-bss-gris">
            <div class="bloc-bss-produit-titre">Mon abonnement pour :</div>
            <div class="form-group">
              <label for="nom-enfant" class="control-panel">Nom de l'enfant</label>
               <input name="nom-enfant" type="text" class="form-control" id="nom-enfant" form="creation-compte" placeholder="Nom de l'enfant" title="Nom de l'enfant" value="Léa">
            </div>
            <label for="matiere" class="control-panel">Matières :</label>
            <ul class="liste-matiere">
              <li>Histoire Géographie</li>
              <li>Mathématiques</li>
            </ul>
            <hr>
            
            <div class="form-group">
              <label for="nom-enfant2" class="control-panel">Nom de l'enfant</label>
              <input name="nom-enfant2" type="text" class="form-control" id="nom-enfant2" form="creation-compte" placeholder="Nom de l'enfant" title="Nom de l'enfant" value="Julien">
            </div>
            <label for="matiere" class="control-panel">Matières :</label>
            <ul class="liste-matiere">
              <li>Histoire Géographie</li>
              <li>Mathématiques</li>
            </ul>
          </div>
       
      </div>
    </div>
    
    </form>
  </div>
</div>
