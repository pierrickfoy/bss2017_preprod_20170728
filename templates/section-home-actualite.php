<?php 
$strAccueil = "SELECT rubrique_titre, rubrique_description, article_id, article_titre, article_soustitre, article_position, files_path, DATE_FORMAT(article_date_publication,'%d/%m/%Y') as date
						   FROM bor_rubrique a
						   LEFT JOIN bor_article b ON (b.rubrique_id = a.rubrique_id)
						   RIGHT JOIN eco_files c ON (b.article_id = c.files_table_id)
						   WHERE c.files_table_source = 'borarticle' 
										AND c.files_table_id_name='article_id' 
										AND c.files_field_name ='article_image'
										AND b.article_actif = 1
										ORDER BY article_date_publication DESC
								LIMIT 1"; 
$accueil = $oDb->queryTab($strAccueil);

 $strImgProjets = "SELECT files_path FROM 
							eco_files 
							WHERE files_table_source = 'borarticle' 
							AND files_table_id_name='article_id' 
							AND files_field_name ='article_image' 
							AND files_table_id = '".mysql_real_escape_string($accueil[0]["article_id"])."'";
								// var_dump($strImgProjets);
								$aPathImg=$oDb->queryRow($strImgProjets);
								// var_dump($aPathImg);
								$strPathImg = $aPathImg["files_path"];
									if(empty($strPathImg)){
									$strPathImg = "../uploads/articles/img_article_defaut.jpg";
									}
// var_dump($accueil);
?>
		
<div class="bss-section bloc-section-gris bss-actualite">
  <div class="container"> 
    <div id="articles" class="row">
      <div class="col-sm-6 col-md-8">
        <div class="row">
												
												<?php 
												foreach($accueil as $iKey => $STRaccueil){
														$lien = $_CONST['URL_ACCUEIL'].'actualite/'.strToUrl($STRaccueil['rubrique_titre']).'/'.$STRaccueil['article_id'].'-'.strToUrl($STRaccueil['article_titre']).'.html';		
												echo ' <div class="item col-sm-12 col-md-6">
														<div class="bloc-bss-actu home">
																<div class="visuel-actu">	<a href="'.$lien.'" style="text-decoration:none;";><div style="max-height:114px;"><img style="width:100%;" src="'.$strPathImg.'" class="img-responsive" border="0"></div></a></div>
																<div class="info-actu ">
																		<h3 class="titre-actu"><a href="'.$lien.'" style="text-decoration:none;";>'.$STRaccueil['article_titre'].'</a> </h3>
																		<div class="tag-actu "><span class="rubrique">'. $STRaccueil['rubrique_titre'].'</span><div class="pull-right">'. $STRaccueil['date'].'</div></div>
																		<div class="excerpt">'.$STRaccueil['article_soustitre'].'</div>
																		<hr class="separator">
																			<div class="toute-actu text-center"><a href="'.$_CONST['URL_ACCUEIL'].'actualite/" class="btn  btn-fw btn-primary  " onclick="ga(\'send\', \'event\', \'Home\', \'Information\', \'Lire toute l actualité\');" >Lire toute l\'actualité<i class="icon-angle-right"></i> </a></div>
																		</div>
														</div>
												</div>';
												}
												?>
          <div class="item  col-sm-12 col-md-6">
            <div class="bloc-bss-actu home">
              <div class="visuel-actu"><a href="actualite-single.php"><img src="/img/750x400.jpg" class="img-responsive"></a></div>
                <div class="info-actu ">
              <h3 class="titre-actu"><a href="actualite-single.php">Vous cherchez un professeur pour des cours particuliers ?</a></h3>
              <div class="excerpt">Bordas Soutien scolaire vous propose également un service de cours à domicile assuré par des enseignants de l'Éducation Nationale, disponibles partout en France.</div>
              <hr class="separator">
              <div class="toute-actu text-center"><a href="https://www.bordas.com" target="_blank" class="btn  btn-fw btn-primary  " onclick="ga('send', 'event', 'Home', 'Information', 'Trouver un professeur Bordas.com');">Trouver un professeur<i class="icon-angle-right"></i> </a></div>  </div>
            </div>
          </div>
        </div>
      </div>
    
  	<?php							
								include_once("./templates/sidebar/form_sidebar_wengo.php");
				?>
				
				</div>
  </div>
</div>
</div>
