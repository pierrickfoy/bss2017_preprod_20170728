
<div class="bss-section  bloc-section-gris bss-home-qsn">
  <div class="container">
    <p id="tagline" class="h1">Qui sommes-nous ?</p>
    <h2 id="subtitle" class="h2">Un programme d’aide scolaire en ligne et d’entrainement personnalisé </h2>
    <div class="row">
    
   
      <div class="col-sm-3">
        <div class=" bloc-bss-1">
          <div class="icone text-center"><img src="/img/icone-multisupport.png"></div>
       
          <p class="texte  text-center"><strong>Site de soutien scolaire</strong> pour s’entraîner sur ordinateur et tablette du CP à la Terminale</p>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="bloc-bss-1">
          <div class="icone text-center"><img src="/img/icone-smiley.png"></div>
          <p class="texte text-center"><strong>Fiches de révision, exercices interactifs, quiz, et vidéo</strong> pour donner le goût d’apprendre !</p>
         
        </div>
      </div>
      <div class="col-sm-3">
        <div class="bloc-bss-1">
          <div class="icone text-center"><img src="/img/icone-24.png"></div>
          <p class="texte  text-center"><strong>Service disponible 24h/24 et 7j/7</strong>, avec ou sans connexion Internet</p>
          
        </div>
      </div>
      <div class="col-sm-3">
        <div class="bloc-bss-1">
          <div class="icone text-center"><img src="/img/icone-enseignant.png"></div>
          <p class="texte  text-center"><strong>Conçu par des enseignants</strong> et <strong>conforme aux programmes</strong> de l’Éducation nationale</p>
          
        </div>
      </div>
    </div>
    <hr>
    <div class="text-center"><a onclick="ga('send', 'event', 'Home', 'Information', 'Découvrir les engagements Bordas');" class="btn btn-md btn-primary" href="/qui-sommes-nous.html">Découvrir les engagements Bordas<i class="icon-angle-right"></i></a> </div>
  </div>
</div>
