<div class="bss-section bloc-section-orange bss-matiere">
  <div class="container">
    <p id="tagline" class="h1">Les matières disponibles</p>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-demo">
            <?php
			$strSql = "SELECT *
			FROM bor_matiere_page cmp, bor_matiere_classe_edito mc, bor_matiere m
			LEFT JOIN eco_files ef ON (ef.files_table_id = m.matiere_id AND ef.files_field_name = 'matiere_image')
			WHERE mc.matiere_id = cmp.matiere_id
			AND m.matiere_id = cmp.matiere_id
			AND mc.classe_id = ".$aClasse['classe_page_id']."
			AND cmp.matiere_page_actif = 1"; 
			$aMatiere = $oDb->queryTab($strSql);
			/*echo '<pre>';
			var_dump($aMatiere);
			echo '</pre>';*/
			foreach($aMatiere as $key=>$matiere){
				
				// Ajout 21 07 2016 : masquer la demo
				$matiere['matiere_page_URL_CGS'] = "";
				// /ajout
				
				$lien_classe_matiere = $aClasse['classe_page_url'].strToUrl($matiere['matiere_titre'])."/";
			?>
				<div class="item-matiere " role="tab" id="matiere-<?php echo $key; ?>">
				  <div class="bloc-matiere-item">
					<div class="col-xs-11 col-sm-11 col-md-5 no-padding">
					<?php if($matiere['files_path'] != ""){?>
					<a href="/<?php echo strtolower($lien_classe_matiere); ?>" class="align-left"><img src="/<?php echo $matiere['files_path']; ?>" class="img-responsive"></a>
					<?php } ?>
					  <h2 class=""><a href="<?php echo $lien_classe_matiere; ?>">Soutien scolaire en ligne en <?php echo $matiere['matiere_titre']; ?>  en <?php echo $aClasse['classe_name'];?> </a></h2>
					</div>
					<ul class="list-inline col-sm-4 col-md-5 no-padding hidden-xs hidden-sm">
						<?php
						$liste_type = explode(',',$matiere['mc_page_type']);
						foreach($liste_type as $type){
							if($type == 1) echo '<li class="type-1"><i class="icon-book"></i><span>Cours</span></li>';
							else if($type == 2) echo '<li class="type-2"><i class="icon-docs"></i><span>Exercices</span></li>';
							else if($type == 3) echo '<li class="type-3"><i class="icon-doc-text"></i><span>Corrigés</span></li>';
							else if($type == 4) echo '<li class="type-4"><i class="icon-coverflow"></i><span>Animations</span></li>';
							else if($type == 5) echo '<li class="type-5"><i class="icon-movie"></i><span>Vidéos</span></li>';
							else if($type == 6) echo '<li class="type-6"><i class="icon-stopwatch"></i><span>Examen</span></li>';
						}
						?>
					</ul>
					<?php if($matiere['matiere_page_URL_CGS'] != ""){?>
					<div class="col-xs-1 col-sm-1 col-md-2 no-padding text-center"> <a  class="btn  btn-primary  hidden-md hidden-lg" role="button" data-toggle="collapse" data-parent="#accordion" href="#demo-<?php echo $key; ?>" aria-expanded="true" aria-controls="demo-<?php echo $key; ?>">></a> <a  class="btn   btn-primary hidden-xs hidden-sm btn-fw" role="button" data-toggle="collapse" data-parent="#accordion" href="#demo-<?php echo $key; ?>" aria-expanded="true" aria-controls="demo-<?php echo $key; ?>">Voir une démo<i class="icon-angle-right"></i></a> </div>
					<?php
					}
					?>
				  </div>
				</div>
			<?php if($matiere['matiere_page_URL_CGS'] != ""){?>	
			 <div id="demo-<?php echo $key; ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="<?php echo $matiere['matiere_page_URL_CGS']; ?>"></iframe>
                </div>
              </div>
            </div>	
			<?php
				}
			}
			?>
			
			
			
			
          </div>
		 
        </div>
      </div>
    </div>
  </div>
</div>
