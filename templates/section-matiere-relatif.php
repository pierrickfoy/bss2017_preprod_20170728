<div class="bss-section bloc-section-bleu bss-matiere">
  <div class="container">
    <p id="tagline" class="h1">Voir également les autres matières en <?php echo $aClasse['classe_name']; ?></p>
	<?php
	$strSql = "SELECT *
			FROM bor_matiere_page cmp, bor_matiere_classe_edito mc, bor_matiere m, eco_files ef
			WHERE mc.matiere_id = cmp.matiere_id
			AND m.matiere_id = cmp.matiere_id
			AND mc.classe_id = ".$aClasse['classe_page_id']."
			AND ef.files_table_id = m.matiere_id
			AND ef.files_field_name = 'matiere_image'
			AND mc.matiere_id != ".$aMatiere['matiere_id']."
			AND cmp.matiere_page_actif = 1"; 
	$aMatiereRelatif = $oDb->queryTab($strSql);
	//var_dump($aMatiereRelatif);
	foreach($aMatiereRelatif as $key=>$mc){
		$lien_classe_matiere = $aClasse['classe_page_url'].strToUrl($mc['matiere_titre'])."/";
	?>
		<div class="item-matiere " role="tab" id="<?php echo $key; ?>">
		  <div class="bloc-matiere-item">
			<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
			<a href="/<?php echo strtolower($lien_classe_matiere); ?>" class="align-left item-relatif-left"><img src="/img/icone-matiere.jpg" class="img-responsive"></a>
			  <h2 class=""><a href="<?php echo strtolower($lien_classe_matiere); ?>">Soutien scolaire en ligne en <?php echo $mc['matiere_titre']; ?> en <?php echo $aClasse['classe_name']; ?> </a></h2>
			</div>
		  </div>
		</div>
    <?php
	}
	?>
  </div>
</div>
