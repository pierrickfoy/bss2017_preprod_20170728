<div class="bss-section bloc-section-bleu bss-classe">
  <div class="container">
    <div class="row">
      <div class=" text-left">
        <h1 id="tagline" class="h1 text-center"><?php echo $aMatiere['matiere_page_titre'];?></h1>
        <?php /*if(isset($aImage['files_path']) && $aImage['files_path'] != ""){?>
			<img src="/<?php echo $aImage['files_path'];?>" class="img-responsive" border="0">
		<?php }else{ ?>
			<img src="/img/montage-classe.png" class="img-responsive" border="0">
		<?php } */
		 include ("section-carousel.php");?>
       <!-- Accroche -->
	  <div class=" col-md-12 ">
        <p class="lead">
		<?php
			echo $aMatiere['matiere_page_accroche'];
		?>
		</p>
      </div>
	    <!-- /Accroche -->
	  <!-- Description -->
      <div class="col-sm-12 col-md-12 ">
        <?php
			echo $aMatiere['matiere_page_desc'];
		?>
		<div class="bss-share" style="text-align:left;">
          <ul class="list-inline" style="padding-left: 0px;">
            <li><a href="<?php echo 'https://www.facebook.com/sharer/sharer.php?u='.$_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>" target="_blank" class="share-facebook"><i class="icon-facebook"></i></a><span>Partager</span></li>
            <li><a href="<?php echo 'https://twitter.com/intent/tweet/?url='.$_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'].'&text=Soutien Éditions Bordas : Pour la réussite de vos enfants, du CP à la Terminale &via=@BordasSoutien'; ?>" target="_blank" class="share-twitter"><i class="icon-twitter"></i></a><span>Twitter</span></li>
            <li><a href="<?php echo 'https://plus.google.com/share?url='.$_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'].'&hl=fr'; ?>" target="_blank" class="share-google"><i class="icon-gplus"></i></a><span>Partager</span></li>
          </ul>
        </div>
      </div>
	  <!-- /Description -->
		<!-- Pub -->
	  <?php							
		/*include_once("./templates/sidebar/form_sidebar_wengo.php");*/
	  ?>
	  <!-- /Pub -->

    </div>
  </div>
</div>
