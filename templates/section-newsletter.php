<div class="bss-section bloc-section-bleu bss-newsletter">
<a class="anchor" id="bloc_newsletters" style="display: block;
    position: relative;
    top: -250px;
    visibility: hidden;"></a>
  <div class="container">
    <div class="row">
      <div class=" col-sm-12 col-md-6">
        <div class="media">
          <div class="media-left"> <img src="/img/icone-newsletter-bleu.png" > </div>
          <div class="media-body">
            <div class="h1  ">Vous souhaitez ...</div>
            <ul>
              <li>Recevoir notre documentation ?</li>
              <li>Bénéficier de nos offres spéciales ?</li>
              <li>Être tenu informé de nos actualités ?</li>
            </ul>
          </div>
        </div>
      </div>
      <div class=" col-sm-12 col-md-6">
          <?php
			// sécurisation formulaire I :
			// variable en session
			$form = "newsletter";
			if(!isset($_SESSION[$form.'_secure_session']) || $_SESSION[$form.'_secure_session'] == ""){
				$secure_session = md5(uniqid(microtime(), true));
				$_SESSION[$form.'_secure_session'] = $secure_session;
			}
			
			// sécurisation formulaire II et III:
			// ajout des input hidden modifiés en js : secure, secure_copy, secure_empty (voir <form> + JS)

            if (!empty($_POST['newsletter_email'])) {
				if (
				(($_POST['secure'] != 'secureValue')
				|| ($_POST['secure_copy'] != 'secureValue'))
				|| ($_POST['secure_empty'] != '')
				|| !isset($_SESSION[$form.'_secure_session'])
				|| !isset($_POST['secure_session'])
				|| $_SESSION[$form.'_secure_session'] !== $_POST['secure_session']
				){
					echo $strMessage = "<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de votre enregistrement.</div>"; 
				}else{
					try{
				
						$soap_options = array ( 'login' => 'WSProspect', 'password' => 'ProspectManagementPER', 'exception' => true); 
						$client = new SoapClient("http://ws.edupole.net/prospectManagement/WSProspect.wsdl", $soap_options); 
						$params = array(
												'codeSite' => 'BRD_SOUTIEN_SCO',
												'email' => $_POST['newsletter_email'], 
												'origin' => 'SABONNER-L1_BSS',
												'optinNewsletter' => 1
												);

						$retour_ws = $client->createProspect($params); 

						if(isset($retour_ws)){
								if($retour_ws->creationStatus)
										$strMessage = "<div class='alert alert-success' role='alert'>Votre adresse email a bien été enregistrée.</div>"; 
								else
										$strMessage = "<div class='alert alert-warning' role='alert'>Cet email a déjà été utilisé, vous ne pouvez l'utiliser qu'une seule fois.</div>";
						 
						}else
							$strMessage = "<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de votre enregistrement.</div>"; 
					}catch (Exception $e){
							$strMessage = "<div class='alert alert-danger' role='alert'>Votre e-mail est invalide, veuillez le saisir à nouveau.</div>";
					}
					echo $strMessage;
				}
            } else {
                echo '<form class="text-right" id="form-newsletter" action="#bloc_newsletters" name="formnewsletter" method="POST">
                  <div class="input-group  ">
                    <input name="newsletter_email" type="text" class="form-control " placeholder="Mon adresse email">

					<input type="hidden" id="secure" name="secure" value="secureValue">
					<input type="hidden" id="secure_copy" name="secure_copy" value="">
					<input type="hidden" id="secure_empty" name="secure_empty" value="">

					<input type="hidden" name="secure_session" value="'.$_SESSION[$form.'_secure_session'].'" />

                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button" id="submit_button_nl" >Je m\'inscris à la newsletter</button>
                    </span> </div>
                  <!-- /input-group -->
                 </form>';
                }
          ?>
      </div>
	   <script>
		$( '#submit_button_nl' ).click(function() {
			$( '#secure_copy' ).val($( '#secure' ).val());
			ga('send', 'event', 'Prospects', 'Information', 'Je m inscris à la newsletter');
			document.formnewsletter.submit();
		});
	  </script>
      
    </div>
  </div>
</div>
