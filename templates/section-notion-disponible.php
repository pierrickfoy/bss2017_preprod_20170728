<?php
$strSql = "SELECT *
			FROM bor_notions n
			WHERE n.chapitre_id = ".$aChapitre['chapitre_id']."
			AND n.notions_status = 1"
			;
			$aNotion = $oDb->queryTab($strSql);
			/*echo '<pre>';
			var_dump($aNotion);
			echo '</pre>';*/
if(!empty($aNotion)){
?>

<div class="bss-section bloc-section-orange bss-notion">
  <div class="container">
    <p id="tagline" class="h1">Les leçons disponibles</p>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-demo">
             <?php
			
			foreach($aNotion as $key=>$notion){
				if(isset($notion['notions_url_seo']) && $notion['notions_url_seo'] != "")
					$lien_notion = strtolower($notion['notions_url_seo']);
				else{
					//$lien_notion = $notion['notions_id']."-".strToUrl($notion['notions_h1'])."/";
					$lien_notion = "#";
				}
			?>
            <div class="item-matiere " role="tab" id="">
              <div class="bloc-matiere-item">
                
                <div class="col-xs-12 col-sm-12 col-md-12 ">
                  <h2 class=""><a href="<?php echo $lien_notion; ?>"><?php echo $notion['notions_h1']; ?></a></h2>
                </div>
              </div>
            </div>
            <?php
			}
			?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
}
?>
