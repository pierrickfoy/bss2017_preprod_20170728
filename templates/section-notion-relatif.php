<?php
//var_dump($aChapitre);
   $strSql = "SELECT *
			FROM bor_notions n
			WHERE n.chapitre_id = ".$aChapitre['chapitre_id']."
			AND n.notions_id != ".$aNotion['notions_id']."
			AND n.notions_status = 1
			";
	$aNotionRelatif = $oDb->queryTab($strSql);
	
	if(!empty($aNotionRelatif)){
		?>
		<div class="bss-section bloc-section-bleu bss-notion">
		  <div class="container">
			<p id="tagline" class="h1">Les autres notions abordées dans le chapitre <?php echo $aChapitre['chapitre_h1'];?></p>
		<?php
		
	
		foreach($aNotionRelatif as $key=>$notion){
			//$lien_notion = $aChapitre['chapitre_alias'].$notion['notions_id']."-".strToUrl($notion['notions_h1'])."/";
			if(isset($notion['notions_url_seo']) && $notion['notions_url_seo'] != "")
				$lien_notion = $notion['notions_url_seo'];
			else{
				//$lien_notion = $notion['notions_id']."-".strToUrl($notion['notions_h1'])."/";
				$lien_notion = "#";
			}
	   ?>
		<div class="bloc-bss-1 item-relatif">
		  <div class="item-relatif-body">
			<h3 class="item-relatif-heading"><a href="<?php echo $lien_notion;?>"><?php echo $notion['notions_h1'];?></a></h3>
		  </div>
		</div>
		<?php
		}
		?>
		  </div>
		</div>
		<?php
	}
	?>

