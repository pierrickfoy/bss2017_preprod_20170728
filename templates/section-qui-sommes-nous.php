<div class="bss-section bloc-section-gris-orange bss-qsn">
  <div class="container">
    <h1 id="tagline" class="h1">Qui sommes-nous ?</h1>
    
    <div class="row">
      <div class="col-md-8"><h2 id="subtitle" class="h2">Les Éditions Bordas</h2>
        <div class="bss-doc-section"> <img src="img/logo-bordas.jpg" class="img-responsive">
          <p><a href="http://www.bordas.com">Les éditions Bordas</a> ont été fondées en 1946 et sont spécialisées dans l'accompagnement des élèves, de la maternelle au lycée. Elles publient des manuels scolaires, mais aussi des ouvrages parascolaires (cahiers de soutien, cahiers de vacances, …)</p>
          <p>Fortes de cette histoire, et grâce à leur expérience en sciences cognitives, les éditions Bordas vous proposent aujourd'hui un site d'assistance scolaire et d'aide aux devoirs 100% numérique : Bordas Soutien scolaire.</p>
          <hr>
          <h2 id="subtitle" class="h2">Bordas Soutien scolaire</h2>
           <img src="img/bss.jpg" class="img-responsive">
          <p>En apprenant son cours et en réalisant les exercices sur son ordinateur ou sur sa tablette, votre enfant prendra plaisir à réviser et à progresser. Nos activités interactives et ludiques lui permettront de développer son autonomie (tous les exercices sont auto-corrigés), sa motivation et sa confiance en lui. Grâce au mode hors connexion  il pourra, s'entraîner en toute sécurité, sans être tenté de surfer sur Internet, pour une concentration optimale.</p>
          <hr>
          <h2 id="subtitle" class="h2">Notre savoir faire pédagogique</h2>
          <img src="img/savoir-faire.jpg" class="img-responsive">
          <p>Tous les contenus de Bordas Soutien scolaire sont conçus par des enseignants et sont en parfaite conformité avec le programme scolaire de l'Education nationale. </p>
          <p>Avec Bordas Soutien scolaire, votre enfant aura toutes les chances de réussir dans sa scolarité !</p>
          <hr>
          <h2 id="subtitle" class="h2">Le numérique au service de la réussite de votre enfant</h2>
          <img src="img/numerique.jpg" class="img-responsive">
          <p>Les activités interactives et ludiques permettent à chaque enfant de développer sa motivation, son autonomie et sa confiance en soi. Grâce au parcours d’entraînement personnalisé, chaque enfant progresse à son rythme.</p>
          <hr>
          <h2 id="subtitle" class="h2">En plus : les cours particuliers</h2>
          <img src="img/cours-particulier.jpg" class="img-responsive">
          <p>Bordas Soutien Scolaire propose également un service complémentaire de cours particuliers assurés par des professeurs de l'Education nationale. Selon votre lieu de résidence, nous trouvons les professeurs disponibles à proximité, et vous ne vous inscrivez que quand vous avez trouvé un professeur. Plus d'informations sur <a href="http://www.bordas.com">www.bordas.com</a></p>
          
        </div>
      </div>
      <div class="col-md-4 hidden-xs hidden-sm">
        <div class=""> 
          <!-- 16:9 aspect ratio -->
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/5LaIfS7P60s?rel=0"></iframe>
          </div>
        </div>
        <hr>
        <div class="bloc-annonce"><div class="prepend"><b class="caret"></b> publicité <b class="caret"></b></div><a href="#"><img src="http://www.bordas-soutien-scolaire.com/images/bss-wengo-300.gif" class="img-responsive"></a></div>
      </div>
      <div class="col-md-12">
        <hr>
       <p class="text-center"><a class="btn btn-md btn-primary" href="comment-sabonner">Comparer les formules d'abonnement<i class="icon-angle-right"></i></a></p>
      </div>
    </div>
  </div>
</div>
