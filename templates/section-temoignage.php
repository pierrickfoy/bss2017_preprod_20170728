<?php
if(isset($_POST['post_avis'])){
	//var_dump($_POST);
	if(isset($_POST["prenom-avis"]) && $_POST["prenom-avis"] != "" && isset($_POST["nom-avis"]) && $_POST["nom-avis"] != "" && isset($_POST["desc-avis"]) && $_POST["desc-avis"] != ""){
		//var_dump($_POST);
		$strSql = "INSERT INTO bor_avis(
				avis_id,
				avis_actif, 
				avis_nom, 
				avis_prenom, 
				avis_description,
				avis_date_add
				) VALUES (
				'',
				'0',
				'".mysql_real_escape_string($_POST["nom-avis"])."',
				'".mysql_real_escape_string($_POST["prenom-avis"])."',
				'".mysql_real_escape_string($_POST["desc-avis"])."',
				NOW()
				)"; 
		$oDb->query($strSql);
			$retour_avis = 'ok';
			$retour_msg = "Votre avis a bien été déposé.";
	}else{
			$retour_avis = 'ko';
			$retour_msg = "Votre avis n'a pas été déposé. Veuillez vérifier les champs et recommencer.";
	};
}
?>
<div class="bss-section bloc-section-violet bss-temoin">
  <style>#temoignages {
    display: block;
    position: relative;
    top: -175px;
    visibility: hidden;
}</style>
<a class="anchor" id="temoignages"></a>
  <div class="container">
    <div class="row">
		<?php
		if(isset($retour_avis) && $retour_avis == "ok"){
		?>
		<div class="alert alert-success" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong>Merci !</strong><br>
		<?php echo $retour_msg; ?> </div>
		<?php
		}else if(isset($retour_avis) && $retour_avis == "ko"){
		?>
		<div class="alert alert-info" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong>Erreur !</strong><br>
		<?php echo $retour_msg; ?> </div>
		<?php
		}
		?>
      <p id="tagline" class="h1_temoin" >Les derniers avis</p>
	  <?php
		$strSql = "SELECT *, DATE_FORMAT(avis_date_add, '%d/%m/%Y') as date
			FROM bor_avis
			WHERE avis_actif = 1
			ORDER BY avis_date_add DESC
			"; 
		$aAvis = $oDb->queryTab($strSql);
		//var_dump($aAvis);
		?>
      <div id="carousel-temoins" class="owl-carousel owl-theme">
		<?php
		foreach($aAvis as $avis){
		?>
			<div class="item active">
			  <div class="texte-temoin">
				<p><?php echo $avis['avis_description']; ?><em class="auteur"><?php echo $avis['avis_prenom']." ".$avis['date']; ?></em></p>
			  </div>
			</div>
		<?php
		}
		?>
      </div>
      <div class="avis text-center"><a href="#" class="btn  btn-info" data-toggle="modal" data-target="#avisModal"><i class='icon-chat'></i> Donnez votre Avis</a></div>
    </div>
  </div>
</div>

<!-- Modal Donnez son avis-->
<div class="modal fade" id="avisModal" tabindex="-1" role="dialog" aria-labelledby="avisModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="cbModalLabel">Donnez votre avis</h4>
      </div>
      <div class="modal-body">
        <form action="#temoignages"  method="post" class="form-horizontal clearfix" id="form-donner-avis" title="Donnez votre avis">
            <div class="form-group">
              <label for="prenom-avis" class="col-sm-12 control-label">Votre prénom</label>
              <div class="col-sm-12">
                <input name="prenom-avis" type="text" class="form-control" id="prenom-avis" form="form-donner-avis" placeholder="Votre prénom" title="Votre prénom" value="">
              </div>
            </div>
            <div class="form-group">
              <label for="nom-avis" class="col-sm-12 control-label">1ère lettre de votre nom</label>
              <div class="col-sm-12">
                <input name="nom-avis" type="text" class="form-control" id="nom-avis" form="form-donner-avis" placeholder="1ère lettre de votre nom" title="1ère lettre de votre nom" value="">
              </div>
            </div>
            <div class="form-group  ">
              <label for="motif" class="col-sm-12 control-label">Votre avis</label>
              <div class="col-sm-12">
                <textarea rows="5" class="form-control" name="desc-avis" form="form-donner-avis"></textarea>
              </div> 
           </div>
			<div class="form-group">
              <div class="col-sm-12 text-center">
				<input type="hidden" name="post_avis" value="post_avis">
                <button type="submit" class="btn btn-primary ">Poster votre avis<i class="icon-ok "></i></button>
              </div>
            </div>
          
            
        </form>
      </div>
     
    </div>
  </div>
</div>