<?php
if($aProduit['formule_URL_CGS'] != ""){
?>
<div class="bss-section  bloc-section-gris-bleu bss-test-produit" id="test-demo">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p class="h1">Tester Bordas Soutien scolaire</p>
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="<?php echo $aProduit['formule_URL_CGS'];?>"></iframe>
        </div>
        <hr>
        <p class="text-center"><a class="btn btn-md  btn-primary ancre a-form-submit"  
		<?php
				if($iProduit == "ciblee"){
					?> onclick="ga('send', 'event', 'Fiche produit F.Ciblée', 'Commande', 'Je m'abonne - Bloc Tester Bordas');" <?php
				}else if($iProduit == "reussite"){
					?> onclick="ga('send', 'event', 'Fiche produit F.Réussite', 'Commande', 'Je m'abonne - Bloc Tester Bordas');" <?php
				}else if($iProduit == "tribu"){
					?> onclick="ga('send', 'event', 'Fiche produit F.Tribu', 'Commande', 'Je m'abonne - Bloc Tester Bordas');" <?php
				}
				?>
		 >Je m'abonne<i class="icon-angle-right"></i></a></p>
      </div>
    </div>
  </div>
</div>
<?php
}
?>