
<div class="bss-section bloc-section-gris-orange bss-video-produit" id="voir-video">
  <div class="container">
    <div class="row">
    <div class="col-md-12">
        <p class="h1">Voir la vidéo de démonstration</p></div>
      <div class="col-md-8 col-md-offset-2 ">
      
        <!-- 16:9 aspect ratio -->
        <div class="embed-responsive embed-responsive-16by9">
          <iframe 
		  <?php
				if($iProduit == "ciblee"){
					?> onclick="ga('send', 'event', 'Fiche produit F.Ciblée', 'Information', 'Vignette vidéo - Bloc Voir la vidéo');" <?php
				}else if($iProduit == "reussite"){
					?> onclick="ga('send', 'event', 'Fiche produit F.Réussite', 'Information', 'Vignette vidéo - Bloc Voir la vidéo');" <?php
				}else if($iProduit == "tribu"){
					?> onclick="ga('send', 'event', 'Fiche produit F.Tribu', 'Information', 'Vignette vidéo - Bloc Voir la vidéo');" <?php
				}
				?>
		   class="embed-responsive-item" src="<?php echo $aProduit['formule_URL_youtube'];?>"></iframe>
        </div>
        <hr>
        <p class="text-center"><a class="btn btn-md  btn-primary ancre a-form-submit"  
		<?php
				if($iProduit == "ciblee"){
					?> onclick="ga('send', 'event', 'Fiche produit F.Ciblée', 'Commande', 'Je m'abonne - Bloc Voir la vidéo');" <?php
				}else if($iProduit == "reussite"){
					?> onclick="ga('send', 'event', 'Fiche produit F.Réussite', 'Commande', 'Je m'abonne - Bloc Voir la vidéo');" <?php
				}else if($iProduit == "tribu"){
					?> onclick="ga('send', 'event', 'Fiche produit F.Tribu', 'Commande', 'Je m'abonne - Bloc Voir la vidéo');" <?php
				}
				?> 
		 >Je m'abonne<i class="icon-angle-right"></i></a></p>
      </div>
    </div>
  </div>
</div>