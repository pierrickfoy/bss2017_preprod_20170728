<?php
if(session_id() == "")
		session_start();
define( 'ROOTWS', dirname( __FILE__ ).'/..');
	$_CONST['path']["server"] = ROOTWS;
		
	include_once(ROOTWS."/lib/config.inc.php");
	include_once(ROOTWS."/lib/database.class.php");
	include_once(ROOTWS."/lib/JoForm.class.php");
	include_once(ROOTWS."/lib/functions.php");
	include_once(ROOTWS."/lib/intyMailer.php");
	include_once(ROOTWS."/lib/abo.class.php");
	$oDb = new database($_CONST["db"]["name"], $_CONST["db"]["user"], $_CONST["db"]["passwd"], $_CONST["db"]["host"]);
	$oDb->query("SET NAMES 'utf8'"); 
	LoadConfig($_CONST); //load eco_config table IN $_CONST
	
	$_SESSION["langs_id"] = 1;
	$_SESSION["langs_code"] = 'fr';


/************************************************************************************************/
/*						TRAITEMENT DE L'ACTION DU POST	: UPDATE ABONNEMENT						*/
/************************************************************************************************/

if(isset($_POST) && count($_POST)){
	
	//Récupération de l'action pour savoir dans quelle situation on se trouve 
	
	if(isset($_POST['action']) && !empty($_POST['action']) && isset($_POST['idcommande']) && !empty($_POST['idcommande'])){

		$strAction = $_POST['action'] ; // 3 type d'action : updatecb , deletecb, deleteabo
		$iCommandeId = $_POST['idcommande'] ;
		
		//Vérification que la commande est bien lié à l'utilisateur en ligne 

		$strSql = "SELECT * FROM bor_commande c WHERE c.commande_id  = '".mysql_real_escape_string($iCommandeId)."' AND c.client_id = '".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."' AND commande_statut = 1"; 
		$aCommande = $oDb->queryRow($strSql); 

		if($oDb->rows > 0 ){	
			if($strAction == "updateabo"){
				
/************************************************************************************************/
/*										SI ACTION = UPDATE	:									*/
/************************************************************************************************/

				$strSql = "SELECT f.formule_classe, p.de_id_yonix FROM bor_produit p 
							INNER JOIN bor_duree_engagement de ON (de.de_id_yonix = p.de_id_yonix) 
							INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
							WHERE p.produit_ean = ".$aCommande['produit_ean']."";
				$aInfosProduitInitial = $oDb->queryRow($strSql);
				$nb_enfant = $_POST['nb_enfant'];
				$donneeManquante = 1;
				
				// ****** RÉUSSITE
				
				if($aInfosProduitInitial['formule_classe'] == "reussite"){
					$iClasseSelect = 'classe-'.$_POST['idcommande'].'-'.$nb_enfant;
					if($_POST[$iClasseSelect] != ""){
						$donneeManquante = 0;
						$strSql = "SELECT p.produit_ean FROM bor_produit p 
									INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix)
									INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
									WHERE c.classe_id = '".$_POST[$iClasseSelect]."'
									AND	f.formule_classe = 'reussite'
									AND p.de_id_yonix = '".$aInfosProduitInitial['de_id_yonix']."'";
						$strEan = $oDb->queryItem($strSql);
					}
					
				// ****** CIBLÉE
				
				}else if($aInfosProduitInitial['formule_classe'] == "ciblee"){
					$iClasseSelect = 'ciblee-classe-'.$_POST['idcommande'].'-'.$nb_enfant;
					$iMatiereSelect = 'matiere-ciblee-classe-'.$_POST['idcommande'].'-'.$nb_enfant;
					if($_POST[$iClasseSelect] != "" && $_POST[$iMatiereSelect] != ""){
						$donneeManquante = 0;
						$strSql = "SELECT p.produit_ean FROM bor_produit p 
									INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix)
									INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
									INNER JOIN bor_matiere m ON (m.matiere_id_yonix = p.matiere_id_yonix)
									WHERE c.classe_id = '".$_POST[$iClasseSelect]."'
									AND	f.formule_classe = '".$aInfosProduitInitial['formule_classe']."'
									AND p.de_id_yonix = '".$aInfosProduitInitial['de_id_yonix']."'
									AND m.matiere_id = '".$_POST[$iMatiereSelect]."'";
						$strEan = $oDb->queryItem($strSql);
					}
					
				// ****** TRIBU
				
				}else if($aInfosProduitInitial['formule_classe'] == "tribu"){
					$iClasseSelect = 'classe-'.$_POST['idcommande'].'-'.$_POST['nb_enfant'];
					if($_POST[$iClasseSelect] != ""){
						$donneeManquante = 0;
						$strSql = "SELECT p.produit_ean FROM bor_produit p 
									INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix)
									INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
									WHERE c.classe_id = '".$_POST[$iClasseSelect]."'
									AND	f.formule_classe = 'reussite'
									AND p.de_id_yonix = '".$aInfosProduitInitial['de_id_yonix']."'";
						$strEan = $oDb->queryItem($strSql);
					}
				}
				
/************************************************************************************************/
/*						SI ON A TOUTES LES DONNÉES : ENVOI DE L'UPDATE							*/
/************************************************************************************************/
				
				if($donneeManquante != 1){
					
					$refProduitInitiale = $aCommande['produit_ean'];
					$refProduitRemplacant = $strEan;
					
					// ****** TRIBU
					
					if($aInfosProduitInitial['formule_classe'] == "tribu"){
						$strSql = "SELECT * FROM bor_commande_enfant ce INNER JOIN bor_commande c ON (c.commande_id = ce.commande_id) WHERE ce.commande_id = '".mysql_real_escape_string($aCommande['commande_id'])."' AND c.client_id='".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."' AND commande_statut = 1 ORDER BY enfant_id DESC";
						$aCmd = $oDb->queryTab($strSql); 
						
						foreach($aCmd as $key => $enfant){
							$aStrEan_tribu_old[$key] = $enfant['enfant_ean_reussite'];
							$id_enfant[$key] = $enfant['enfant_id'];
							$prenom_enfant[$key] = $enfant['enfant_prenom'];
							$id_commande = $enfant['commande_id'];
							$num_commande = $enfant['commande_num'];
						}
						
						$iCount = intval($_POST['nb_enfant']);
							if($_POST["classe-".$aCommande['commande_id']."-".$_POST['nb_enfant']] != "0"){
								$strSql = "	SELECT p.produit_ean FROM bor_produit p 
											INNER JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix)
											INNER JOIN bor_formule f ON (f.formule_id_yonix = p.formule_id_yonix)
											WHERE c.classe_id = '".$_POST["classe-".$aCommande['commande_id']."-".$_POST['nb_enfant']]."'
											AND	f.formule_classe = 'reussite'
											AND p.de_id_yonix = '".$aInfosProduitInitial['de_id_yonix']."'";
								$aStrEan_tribu_new = $oDb->queryItem($strSql);
							}else{
								$aStrEan_tribu_new = "";
							}
						
						// ANCIEN EAN ET NOUVEAU EAN POUR L'ENFANT SÉLECTIONNÉ
						
						$iStrEan_tribu_new = $aStrEan_tribu_new;
						$iStrEan_tribu_old = $aStrEan_tribu_old[$_POST['nb_enfant']];

						$oAbo = new aboEditis();
						$key = 	$_POST['nb_enfant'];
						
							$refRemplacante = "";
							$refInitiale = $aStrEan_tribu_old[$key];
							$refRemplacante = $iStrEan_tribu_new;
							$idEnfant = $id_enfant[$key];
							$prenomEnfant = $prenom_enfant[$key];
							
							if($refRemplacante != "" && ($refInitiale != $refRemplacante)){
								$aAbo = $oAbo->changerClasseAbonnement($_SESSION['user']['IDCLIENTWEB'], "", $refInitiale, $refRemplacante, $id_commande, $num_commande, $prenomEnfant);
								
								if($aAbo['ws'] && isset($aAbo['oReponse']) && $aAbo['oReponse'] == "OK"){
									$bUpdate = true; 
									
									$strSql = "	SELECT classe_id FROM bor_produit p
												LEFT JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix)
												WHERE p.produit_ean = '".$refRemplacante."'
												";
									$strInfoNewProduct = $oDb->queryRow($strSql);
									
									$oDb->query("UPDATE bor_commande_enfant SET enfant_ean_reussite = '".$refRemplacante."', classe_id = '".$strInfoNewProduct['classe_id']."' WHERE commande_id = '".$id_commande."' AND enfant_id='".$idEnfant."'");
									
									$strSql = "	SELECT classe_name FROM bor_classe c
										LEFT JOIN bor_produit p ON (c.classe_id_yonix = p.classe_id_yonix)
										WHERE p.produit_ean = '".$refProduitRemplacant."'
										";
									$classeName = $oDb->queryItem($strSql);
									
									$_SESSION['bValidate'] = true;
									$_SESSION['strMessageValidate'] = "Votre changement de classe est pris en compte pour ".$prenomEnfant.". Votre enfant à maintenant accès à la classe ".$classeName.".";
								}else{
									$_SESSION['bErreur'] = true;
									$_SESSION['strErreurCarte'] = "Nous ne pouvons pas prendre en compte votre demande. Veuillez nous contacter au ".$_CONST['RC_TEL'];
								}
							}

					// ****** CIBLÉE / RÉUSSITE
					
					}else{
						$strSql = "SELECT * FROM bor_commande_enfant ce INNER JOIN bor_commande c ON (c.commande_id = ce.commande_id) WHERE ce.commande_id = '".mysql_real_escape_string($aCommande['commande_id'])."' AND c.client_id='".mysql_real_escape_string($_SESSION['user']['IDCLIENT'])."' AND commande_statut = 1 ORDER BY enfant_id DESC";
						$aCmd = $oDb->queryRow($strSql);
						$prenomEnfant = $aCmd['enfant_prenom'];
						$oAbo = new aboEditis();
						$aAbo = $oAbo->changerClasseAbonnement($_SESSION['user']['IDCLIENTWEB'], $aCommande['commande_abo_id'], $refProduitInitiale, $refProduitRemplacant, $_POST['idcommande'],$aCommande['commande_num'], $prenomEnfant);
						if($aAbo['ws'] && isset($aAbo['oReponse']) && $aAbo['oReponse'] == "OK"){
							$bUpdate = true; 
							if($aInfosProduitInitial['formule_classe'] == "reussite" || $aInfosProduitInitial['formule_classe'] == "tribu" ){
								$oDb->query("UPDATE bor_commande SET produit_ean = '".$refProduitRemplacant."' WHERE commande_id = '".$_POST['idcommande']."'");
								$strSql = 	"SELECT classe_id FROM bor_produit p
											LEFT JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix)
											WHERE p.produit_ean = '".$refProduitRemplacant."'
											";
								$strInfoNewProduct = $oDb->queryRow($strSql);
								
								$oDb->query("UPDATE bor_commande_enfant SET enfant_ean = '".$refProduitRemplacant."', enfant_ean_reussite = '".$refProduitRemplacant."', classe_id = '".$strInfoNewProduct['classe_id']."' WHERE commande_id = '".$_POST['idcommande']."'");
								if($aInfosProduitInitial['formule_classe'] == "reussite"){
									$oDb->query("UPDATE bor_commande SET produit_ean = '".$refProduitRemplacant."' WHERE commande_id = '".$_POST['idcommande']."'");
								}
							}else{
								$strSql = "	SELECT classe_id, matiere_id FROM bor_produit p
											LEFT JOIN bor_classe c ON (c.classe_id_yonix = p.classe_id_yonix)
											LEFT JOIN bor_matiere m ON (m.matiere_id_yonix = p.matiere_id_yonix)											
											WHERE p.produit_ean = '".$refProduitRemplacant."'
											";
								$strInfoNewProduct = $oDb->queryRow($strSql);
								
								$oDb->query("UPDATE bor_commande_enfant SET enfant_ean = '".$refProduitRemplacant."', enfant_ean_reussite = '".$refProduitRemplacant."', classe_id = '".$strInfoNewProduct['classe_id']."', matiere_id = '".$strInfoNewProduct['matiere_id']."' WHERE commande_id = '".$_POST['idcommande']."'");
								$oDb->query("UPDATE bor_commande SET produit_ean = '".$refProduitRemplacant."' WHERE commande_id = '".$_POST['idcommande']."'");
							}
							
							$strSql = "	SELECT classe_name FROM bor_classe c
										LEFT JOIN bor_produit p ON (c.classe_id_yonix = p.classe_id_yonix)
										WHERE p.produit_ean = '".$refProduitRemplacant."'
										";
							$classeName = $oDb->queryItem($strSql);
								
							$_SESSION['bValidate'] = true;
							$_SESSION['strMessageValidate'] = "Votre changement de classe est pris en compte. Votre enfant à maintenant accès à la classe ".$classeName.".";
						}else{
							$_SESSION['bErreur'] = true;
							$_SESSION['strErreurCarte'] = " Votre changement de classe n’est pas pris en compte. Merci de nous contacter par téléphone au ".$_CONST['RC_TEL']." ou par mail : soutien-scolaire@bordas.tm.fr";
						}
					}
				}else{
					// erreur : manque une donnée
					$_SESSION['bErreur'] = true;
					$_SESSION['strErreurCarte'] = "Nous ne pouvons pas prendre en compte votre demande. Veuillez nous contacter au ".$_CONST['RC_TEL'].".";
				}
			}
			header('Location: /gerer-mon-abonnement.html');
		}
	}
}