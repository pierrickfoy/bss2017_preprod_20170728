<!doctype html>
<html lang="fr" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<title>BSS TUNNEL 4</title>
<link rel="canonical" href="[canonical]" />
<meta name="description" content="[description]" />
<meta name="author" content="[author]" />
<meta property="og:locale" content="fr_FR" />
<meta property="og:type" content="website" />
<meta property="og:title" content="[title]" />
<meta property="og:description" content="[description]" />
<meta property="og:url" content="[URL]" />
<meta property="og:site_name" content="[site_name]" />
<meta property="og:image" content="[image]" />
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:description" content="[description]"/>
<meta name="twitter:title" content="[title]"/>
<meta name="twitter:site" content="[site]"/>
<meta name="twitter:domain" content="[domain]"/>
<meta name="twitter:image:src" content="[image]" />
<!--STYLES-->
<?php include 'styles.php';?>
</head>
<!-- /!\ SI L'ABONNE EST IDENTIFIE AJOUTER LA CLASSE "logged-in" AU BODY /!\ -->
<body class="tunnel logged-in" id="tunnel-4">

<!-- HEADER -->
<header class="bss-header">
  <?php include 'header-logon.php';?>
</header>
<div class="main animsition"> 
  
  <!-- BREADCRUMB -->
  <div class="bss-breadcrumb" >
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h6>
            <ol class="breadcrumb">
              <li id="a" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemref="b"> <a href="index.php" itemprop="url"> <span itemprop="title">Accueil</span> </a></li>
              <li id="b" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" itemref="c"> <a href="#" itemprop="url"> <span itemprop="title">Tunnel</span> </a></li>
              <li id="c" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child"> <a href="tunnel-4.php" itemprop="url"> <span itemprop="title">Confirmation</span> </a></li>
            </ol>
          </h6>
        </div>
      </div>
    </div>
  </div>
  <!-- STEP COMMANDE -->
  <div class="bss-section bloc-section-blanc bss-step" >
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-3"> <a href="tunnel-1.php" class="text-center step-bloc"><span>1</span> Commande</a> </div>
        <div class="col-xs-12 col-sm-3"> <a href="tunnel-2.php" class="text-center step-bloc  "><span>2</span> Informations personnelles</a> </div>
        <div class="col-xs-12 col-sm-3"> <a href="tunnel-3.php" class="text-center step-bloc "><span>3</span> Paiement</a> </div>
        <div class="col-xs-12 col-sm-3"> <a href="tunnel-4.php" class="text-center step-bloc actif"><span>4</span> Confirmation</a> </div>
      </div>
    </div>
  </div>
  <?php include 'template/section-etape-tunnel-4.php';?>
</div>
<!--FOOTER-->
<?php include 'footer.php';?>
<!--SCRIPTS-->
<?php include 'scripts.php';?>
</body>
</html>
